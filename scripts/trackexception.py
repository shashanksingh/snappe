#! /usr/bin/python

# To change this template, choose Tools | Templates
# and open the template in the editor.

__author__="shashank"
__date__ ="$Dec 10, 2011 2:11:39 AM$"

import os
import sys
import math
import time
import re
import hashlib
import datetime
import threading
import cStringIO
import traceback

from functools import partial

import collections

import sendmail

class ExceptionTracker:

    REPORT_RECIPIENT_EMAILS = ["shashank.sunny.singh@gmail.com"]

    REPORT_TEXT_TEMPLATE = '''
A new exception has occurred %(error_message)s:
    Machine: %(machine)s
    Total occurences in last %(history_days)d days: %(total_occurences)d
    Total occurences today: %(today_occurences)d
    Frequency (last %(frequency_window)d exceptions): %(frequency).2f seconds/exception
    Message: %(error_message)s
    Stack Trace:%(stack_trace)s
'''

    REPORT_HTML_TEMPLATE = '''
        <h3>%(machine)s: %(error_message)s</h3>
        <table cellspacing="2" cellpadding="2">
            <tr>
                <td>Machine</td>
                <td>%(machine)s</td>
            </tr>
            <tr>
                <td>Total occurences in last %(history_days)d days</td>
                <td>%(total_occurences)d</td>
            </tr>
            <tr>
                <td>Total occurences today</td>
                <td>%(today_occurences)d</td>
            </tr>
            <tr>
                <td>Frequency (last %(frequency_window)d exceptions)</td>
                <td>%(frequency).2f seconds/exception</td>
            </tr>
            <tr>
                <td>Message</td>
                <td>%(error_message)s</td>
            </tr>
        </table>
        <h5>Stack Trace</h5>
        <pre>%(stack_trace)s</pre>
    '''

    def __init__(self, machine_name, log_file_dir, track_history_days):
        self.machine_name = machine_name
        self.log_file_dir = log_file_dir
        self.track_history_days = track_history_days

        self.signature_count = collections.defaultdict(int)
        self.recent_times = collections.defaultdict(list)

        self.pending_report_emails = {}
        self.pending_report_lock = threading.Lock()
        self.send_email_thread = threading.Thread(target=self.send_reports)
        self.send_email_thread.setDaemon(True)
        self.send_email_thread.start()


    def start(self):
        #load history
        for timestamp, stack_trace, signature in self.get_past_exceptions(self.track_history_days):
            self.update_stats(timestamp, stack_trace, signature)

        print "parsed past exceptions: %d days, %d distinct exceptions"%(self.track_history_days, len(self.signature_count))

        for timestamp, stack_trace, signature in self.get_today_exceptions():
            self.update_stats(timestamp, stack_trace, signature)
            self.process_new_exception(timestamp, stack_trace, signature)


    def update_stats(self, timestamp, stack_trace, signature):
        self.signature_count[signature] += 1

        #remove timestamp from day before if any and append this one
        ts_list = self.recent_times[signature]
        today_timestamp = self.day_to_timestamp()
        ts_list = [ts for ts in ts_list if ts >= today_timestamp]
        ts_list.append(timestamp)
        self.recent_times[signature] = ts_list

    def process_new_exception(self, timestamp, stack_trace, signature):
        total_occurence_count = self.signature_count[signature]
        if self.should_report_exception(total_occurence_count):
            self.report_exception(stack_trace, signature)

    def should_report_exception(self, occurence_count):
        #count in 1, 5, 10, 20, 40, 80, 160, ...
        return occurence_count == 1 or (occurence_count%5==0 and math.modf(math.log(occurence_count/5, 2))[0] == 0)

    def report_exception(self, stack_trace, signature):
        to_email = self.REPORT_RECIPIENT_EMAILS
        subject = "New exception on %s"%(self.machine_name)

        today_occurence_ts = self.recent_times[signature]
        today_occurences = len(today_occurence_ts)

        if today_occurences == 1:
            frequency_window = 1
            frequency = -1.0
        else:
            frequency_window = max(int(0.1 * today_occurences), min(5, today_occurences))
            frequency = (today_occurence_ts[-1] - today_occurence_ts[-frequency_window]) * 1.0/frequency_window


        message_stack_split = stack_trace.split('\n', 1)
        if len(message_stack_split) == 1:
            error_message = stack = stack_trace
        else:
            error_message, stack = message_stack_split

        message_params = {
            'machine': self.machine_name,
            'history_days': self.track_history_days,
            'total_occurences': self.signature_count[signature],
            'today_occurences': today_occurences,
            'frequency': frequency,
            'frequency_window': frequency_window,
            'error_message': error_message,
            'stack_trace': stack
        }

        print today_occurence_ts
        print stack_trace
        print message_params

        html_message = self.REPORT_HTML_TEMPLATE%message_params

        self.pending_report_lock.acquire()
        try:
            self.pending_report_emails[signature] = html_message
        finally:
            self.pending_report_lock.release()



    def send_reports(self):
        SLEEP_DURATION_SECONDS = 5 * 60

        #sleep a little at a time to ensure interruptability
        while True:
            for i in range(SLEEP_DURATION_SECONDS):
                time.sleep(1)

            print "mail sending thread woke up!"

            reports = []
            self.pending_report_lock.acquire()
            try:
                reports = self.pending_report_emails.values()
                self.pending_report_emails.clear()
            finally:
                self.pending_report_lock.release()

            if len(reports) == 0:
                continue

            subject = "%d New Exceptions on %s"%(len(reports), self.machine_name)
            message = "\n".join(['<div style="border-bottom: 2px solid #cecece">%s</div>'%(m) for m in reports])
            sendmail.mail(self.REPORT_RECIPIENT_EMAILS, subject, None, message)

            print "mail sending thread dispatched %d error reports"%(len(reports))

    def tail(self, fh, start_at_end=True):
        if start_at_end:
            # Go to the end of the file
            fh.seek(0,2)

        while True:
             try:
                 line = fh.readline()
             except ValueError, e:
                 if e.message == "I/O operation on closed file":
                     break
                 raise e

             if not line:
                 time.sleep(0.1)    # Sleep briefly
                 continue
             yield line

    def day_to_timestamp(self, day=None):
        if day is None:
            day = datetime.date.today()
        return int(time.mktime(day.timetuple()))

    def get_past_exceptions(self, days):
        one_day = datetime.timedelta(days=1)
        curr_date = datetime.date.today() - days * one_day

        for i in range(days):
            log_file_path = self.get_log_file_path(curr_date)
            if os.path.exists(log_file_path):
                timestamp = self.day_to_timestamp(curr_date)
                fh = open(log_file_path)
                try:
                    for stack_trace, signature in self.extract_stack_traces(fh):
                        yield timestamp, stack_trace, signature
                finally:
                    fh.close()
            else:
                print "log file not found at: %s"%(log_file_path)

            curr_date += one_day

    def get_today_exceptions(self):
        while True:
            log_file_path = self.get_log_file_path(datetime.date.today())
            fh = open(log_file_path)

            rollover_thread = threading.Thread(target=partial(self.close_file_on_tomorrow, fh))
            rollover_thread.setDaemon(True)
            rollover_thread.start()

            try:
                line_gen = self.tail(fh, False)
                for stack_trace, signature in self.extract_stack_traces(line_gen):
                    yield int(time.time()), stack_trace, signature
            finally:
                if not fh.closed:
                    fh.close()

    def close_file_on_tomorrow(self, fh, extra_wait_seconds=1800):
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        tomorrow_seconds = int(time.mktime(tomorrow.timetuple()))

        close_on = tomorrow_seconds + extra_wait_seconds

        #loop to make this interruptible
        while True:
            if int(time.time()) < close_on:
                time.sleep(1)
            else:
                fh.close()
                break


    @classmethod
    def get_stack_traces(cls, log_str):
        return re.findall(r"SEVERE:.+?\n(.+?)(?:\Z|\n\S)", log_str, re.M|re.S)

    @classmethod
    def get_exception_signature(cls, stack_trace):
        #we remove error message, it can vary with each instance of the exception
        #but we keep the class of the exception
        lines = stack_trace.split('\n')
        first_line = lines[0]
        error_name = first_line.split(':')[0]

        lines = [error_name] + [l for l in lines[1:] if l[:3] == "\tat"]
        signature = hashlib.sha224("\n".join(lines)).hexdigest()

        return signature

    def extract_stack_traces(self, log_gen):
        buffer = []
        last_ts = None

        for curr_line in log_gen:
            curr_ts = int(time.time())
            if last_ts is None or curr_ts - last_ts >= 5:
                log_buffer = "".join(buffer)
                stack_traces = self.get_stack_traces(log_buffer)
                for stack_trace in stack_traces:
                    yield stack_trace, self.get_exception_signature(stack_trace)

                buffer = [curr_line]
                last_ts = curr_ts

            else:
                buffer.append(curr_line)

        log_buffer = "".join(buffer)
        stack_traces = self.get_stack_traces(log_buffer)
        for stack_trace in stack_traces:
            yield stack_trace, self.get_exception_signature(stack_trace)

    def get_log_file_path(self, day=None):
        if day is None:
            day = datetime.date.today()
        return os.path.join(self.log_file_dir, 'localhost.%s.log')%(str(day))


if __name__ == "__main__":
    machine_name = sys.argv[1]
    log_dir = sys.argv[2]
    history_days = int(sys.argv[3])

    while True:
        try:
            tracker = ExceptionTracker(machine_name, log_dir, history_days)
            tracker.start()
        except Exception, e:
            print "exception in exception tracker!"

            exc_info = str(e) + "\n" + "".join(traceback.format_tb(sys.exc_info()[2]))

            to = ExceptionTracker.REPORT_RECIPIENT_EMAILS
            subject = "Error in exception tracker on %s"%(machine_name)
            text = "Exception tracker on %s has encountered an exception and will auto-restart:\n%s"%(machine_name, exc_info)

            sendmail.mail(to, subject, text)
            time.sleep(60 * 60)


