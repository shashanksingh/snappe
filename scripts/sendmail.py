#! /usr/bin/python

# To change this template, choose Tools | Templates
# and open the template in the editor.

__author__="shashank"
__date__ ="$Dec 10, 2011 5:55:50 AM$"

#src: http://kutuma.blogspot.com/2007/08/sending-emails-via-gmail-with-python.html

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders
import os

gmail_user = "snappe.notifications@gmail.com"
gmail_pwd = "snappenotifications"

def mail(to, subject, text, html=None, attach=None):
   msg = MIMEMultipart()

   if type(to) in (str, unicode):
       to = [to]

   msg['From'] = gmail_user
   msg['To'] = to[0]
   msg['Subject'] = subject



   if html is not None:
       msg.attach(MIMEText(html, 'html'))
   else:
       msg.attach(MIMEText(text, 'plain'))

   if attach:
       part = MIMEBase('application', 'octet-stream')
       part.set_payload(open(attach, 'rb').read())
       Encoders.encode_base64(part)
       part.add_header('Content-Disposition',
               'attachment; filename="%s"' % os.path.basename(attach))
       msg.attach(part)

   mailServer = smtplib.SMTP("smtp.gmail.com", 587)
   mailServer.ehlo()
   mailServer.starttls()
   mailServer.ehlo()
   mailServer.login(gmail_user, gmail_pwd)
   mailServer.sendmail(gmail_user, to, msg.as_string())
   # Should be mailServer.quit(), but that crashes...
   mailServer.close()

