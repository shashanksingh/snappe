<%@page import="pe.snap.exceptions.APIException.APIExceptionType"%>
<%@page import="pe.snap.exceptions.APIException"%>
<%@page import="pe.snap.infra.DBManager"%>
<%@page import="org.neo4j.graphdb.Transaction"%>
<%@page import="pe.snap.infra.PhotoManager.PhotoSize"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.io.FileUtils"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@page import="pe.snap.infra.PhotoManager"%>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.model.*"%>

<%
String hash = request.getParameter("h");
if(hash == null) {
    response.sendError(HttpServletResponse.SC_NOT_FOUND);
    return;
}

PhotoSize size = PhotoSize.STANDARD;
if(request.getParameter("thumb") != null) {
    size = PhotoSize.THUMB;
}
else if(RequestUtil.isDownloadRequest(request)) {
    size = PhotoSize.FULL;    
}

//TODO:better protection of photos
Transaction tx = DBManager.getTransaction();
try {
    User user = null;
    if(size == PhotoSize.FULL) {
        //this will throw an exception if not an authed user
        //we want to protect our full sized images
        user = RequestUtil.getUser(request);
    }
    tx.success();
}
catch(APIException apie) {
    //degrade to standard size if not authorized
    if(apie.getAPIExceptionType() == APIExceptionType.ACCESS_TOKEN_REQUIRED) {
        size = PhotoSize.STANDARD;
    }
    else {
        throw apie;
    }
}
finally {
    tx.finish();
}


if(!PhotoManager.hasPhoto(hash, size)) {
    response.sendError(HttpServletResponse.SC_NOT_FOUND);
    return;
}

String path = PhotoManager.getFilePath(hash, size);

response.setContentType("image/jpeg");
FileUtils.copyFile(new File(path), response.getOutputStream());


/*
if(size == PhotoSize.FULL) {
    CreditsManager.decrementUserCredits(user);
}
*/
%>