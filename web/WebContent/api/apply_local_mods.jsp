<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="org.json.*" %>
<%@ page import="pe.snap.model.*" %>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.api.PhotoOrganizer"%>
<%
	
    Logger logger = (Logger)request.getAttribute(Constants.REQUEST_LOGGER_ATTR);
    User user = (User)request.getAttribute(Constants.REQUEST_USER_ATTR);
    String deviceID = (String)request.getSession().getAttribute(Constants.SESSION_DEVICE_ID_ATTR);
    
    Util.log("got request user object: %s, deviceID: %s", user, deviceID);
    
    if(request.getParameterMap().size() == 0) {
        Analytics.log(deviceID, "photo_organizer_no_mods");
        return;
    }
    
    JSONArray newPhotos = new JSONArray(RequestUtil.getParameter(request, "add"));
    JSONObject deleteDescriptor = new JSONObject(RequestUtil.getParameter(request, "delete"));
    JSONArray modifiedAlbums = new JSONArray(RequestUtil.getParameter(request, "modify"));
    
    long startTime = System.currentTimeMillis();
    PhotoOrganizer.addNewPhotos(user, deviceID, newPhotos);
    long currentTime = System.currentTimeMillis();
    Analytics.log(deviceID, "photo_organizer_add_new_photos", "time_taken", currentTime - startTime);
    
    startTime = System.currentTimeMillis();
    PhotoOrganizer.removeDeletedPhotos(user, deleteDescriptor);
    currentTime = System.currentTimeMillis();
    Analytics.log(deviceID, "photo_organizer_remove_deleted_photos", "time_taken", currentTime - startTime);
    
    startTime = System.currentTimeMillis();
    PhotoOrganizer.updateModifiedAlbums(user, modifiedAlbums);
    currentTime = System.currentTimeMillis();
    Analytics.log(deviceID, "photo_organizer_update_modified_albums", "time_taken", currentTime - startTime);
%>