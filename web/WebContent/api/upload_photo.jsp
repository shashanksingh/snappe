<%@page import="pe.snap.model.*"%>
<%@page import="org.apache.commons.fileupload.util.Streams"%>
<%@page import="pe.snap.exceptions.APIException.APIExceptionType"%>
<%@page import="pe.snap.exceptions.APIException"%>
<%@page import="pe.snap.infra.*"%>
<%@page import="pe.snap.util.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.InputStream"%>
<%@page import="org.apache.commons.fileupload.*" %>
<%@page import="org.apache.commons.fileupload.servlet.*" %>
<%@page import="org.apache.commons.fileupload.disk.*" %>

<%
User user = (User)request.getAttribute(Constants.REQUEST_USER_ATTR);
int availableCredits = CreditsManager.getUserCurrentCredits(user).credits;
if(availableCredits <= 0) {
    throw new APIException(APIExceptionType.INSUFFICIENT_CREDITS, "not enough credits");
}

ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
List<FileItem> items = upload.parseRequest(request);

String hash = null;
InputStream is = null;

for(FileItem item : items) {
    if (!item.isFormField()) {
    	is = item.getInputStream();
    }
    else if(item.getFieldName().equals("hash")) {
        hash = item.getString("UTF-8");    
    }
}

if(hash == null) {
	throw new APIException(APIExceptionType.MISSING_PARAMETER, "missing parameter 'hash'");    
}
else if(is == null) {
	throw new APIException(APIExceptionType.MISSING_PARAMETER, "missing file data");    
}

PhotoManager.savePhoto(hash, is);

CreditsManager.decrementUserCredits(user);

List<Photo> photosWithHash = Photo.getByHash(hash);
for(Photo photo : photosWithHash) {
    photo.updateURLs();
}
%>