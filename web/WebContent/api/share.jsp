<%@page import="pe.snap.model.Album.PhotoSource"%>
<%@page import="pe.snap.infra.PhotoManager.PhotoSize"%>
<%@page import="pe.snap.services.NotificationService"%>
<%@page import="pe.snap.services.EmailShareService"%>
<%@page import="pe.snap.infra.PhotoManager"%>
<%@ page import="java.util.*" %>
<%@page import="pe.snap.model.Notification.NotificationType"%>
<%@page import="pe.snap.exceptions.APIException.APIExceptionType"%>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="org.json.*" %>
<%@ page import="pe.snap.model.*" %>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.exceptions.*"%>
<%

Logger logger = (Logger)request.getAttribute(Constants.REQUEST_LOGGER_ATTR);
User user = (User)request.getAttribute(Constants.REQUEST_USER_ATTR);
String deviceID = (String)request.getSession().getAttribute(Constants.SESSION_DEVICE_ID_ATTR);

String action = RequestUtil.getParameter(request, "action", new String[]{"get_upload_status", "share", "unshare", "update_share_state", "email_share", "pull", "push", "sync", "add_photos"});

Util.log("share: %s, %s, %s", request.getMethod(), user.getId(), action);

if(action.equals("get_upload_status")) {
    if(request.getParameter("aid") != null) {
        Album album = Album.get(RequestUtil.getParameter(request, "aid"));
        List<Photo> photos = album.getPhotos();
        Set<String> nonSavedFileHashes = new HashSet<String>();
        for(Photo photo : photos) {
            if(!PhotoManager.hasPhoto(photo.getHash(), PhotoSize.FULL)) {
                nonSavedFileHashes.add(photo.getHash());
            }
        }
        
        JSONArray rv = new JSONArray(nonSavedFileHashes);
        out.print(rv.toString());    
    }
    else if(request.getParameter("hashes") != null) {
        JSONArray hashes = new JSONArray(request.getParameter("hashes"));
        
        Set<String> hashesNotFound = new HashSet<String>();
        for(int i = 0; i<hashes.length(); i++) {
            String hash = hashes.getString(i);
            if(!PhotoManager.hasPhoto(hash, PhotoSize.FULL)) {
                hashesNotFound.add(hash);
            }
        }
        
        JSONArray rv = new JSONArray(hashesNotFound);
        out.print(rv.toString());
    }
	
}
else if(action.equals("share")) {
	Album albumToShare = Album.get(RequestUtil.getParameter(request, "aid"));
    User shareWithUser = User.get(RequestUtil.getParameter(request, "uid"));
    String appRequestID = RequestUtil.getOptionalParameter(request, "app_request_id");
    
    System.out.println("shareWithUser: " + shareWithUser + "shareWithUser id: " + RequestUtil.getParameter(request, "uid")); 

    if(!user.ownsAlbum(albumToShare)) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not own the album to be shared");
    }
    if(!user.isFriendsWith(shareWithUser)) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "albums can be shared only with your friends");    
    }
    
    albumToShare.shareWithUser(shareWithUser);

    JSONObject notificationData = new JSONObject();
    notificationData.put("from_id", user.getId());
    notificationData.put("from_first_name", user.getFirstName());
    notificationData.put("from_last_name", user.getLastName());
    notificationData.put("from_full_name", user.getName());
    notificationData.put("album_id", albumToShare.getId());
    notificationData.put("album_name", albumToShare.getName());
    notificationData.put("thumbnail_url", albumToShare.getThumbnailURL());

    Notification notification = new Notification(NotificationType.ALBUM_SHARED, notificationData);
    shareWithUser.addNotification(notification);
    
     //client did not send a facebook notification to the user
    if(appRequestID == null) {
        Util.log("client did not send a FB notification to the user");
        if(shareWithUser.isRegisteredUser()) {
            Util.log("sending FB notification to registered user: %s", shareWithUser.getId());
            FacebookManager.sendAlbumShareNotification(user, shareWithUser, albumToShare);
        }
        else {
            Util.log("sending email notification to non-registered user: %s", shareWithUser.getId());
            EmailShareService.submit(user, shareWithUser, albumToShare);
        }
    }
    Util.log("client did send a FB notification to the user, appRequestID: %s", appRequestID);
     
    String message = String.format("%s shared an album with you", user.getName());
    NotificationService.sendNotification(shareWithUser, message);
}
else if(action.equals("unshare")) {
    Album albumToUnShare = Album.get(RequestUtil.getParameter(request, "aid"));
    User sharedWithUser = User.get(RequestUtil.getParameter(request, "uid"));
    
    Util.log("unshareWithUser: " + sharedWithUser + "shareWithUser id: " + RequestUtil.getParameter(request, "uid")); 

    if(!user.ownsAlbum(albumToUnShare)) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not own the album to be unshared");
    }
    if(!user.isFriendsWith(sharedWithUser)) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "albums can be shared only with your friends");    
    }
    
    albumToUnShare.unshareWithUser(sharedWithUser);
}
else if(action.equals("update_share_state")) {
    Album albumToUpdate = Album.get(RequestUtil.getParameter(request, "aid"));
    if(!user.ownsAlbum(albumToUpdate)) {//not allowing sharing of friends' albums for now
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not own the album to be shared");
    }
    
    String changesParameter = RequestUtil.getParameter(request, "changes");
    Util.log("changesParameter: %s", changesParameter);
    
    JSONArray changeDescriptors = new JSONArray(changesParameter);
    
    Util.log("update_share_state: " + albumToUpdate.getId() + ": " + changeDescriptors.toString(3));
    
    for(int i=0; i<changeDescriptors.length(); i++) {
        JSONObject changeDescriptor = changeDescriptors.getJSONObject(i);
        String uid = changeDescriptor.getString("uid");
        User friend = User.get(uid);
        if(!user.isFriendsWith(friend)) {
            throw new APIException(APIExceptionType.NOT_AUTHORIZED, "albums can be shared only with your friends");    
        }
        
        
        if(changeDescriptor.has("share")) {
            boolean isShared = changeDescriptor.getBoolean("share");
            if(isShared) {
                if(!albumToUpdate.isSharedWithUser(friend)) {
                    albumToUpdate.shareWithUser(friend);
                }
            }
            else {
                if(albumToUpdate.isSharedWithUser(friend)) {
                    albumToUpdate.unshareWithUser(friend);
                }
            }
        }
        
        if(changeDescriptor.has("allow_upload")) {
            boolean isAllowedUpload = changeDescriptor.getBoolean("allow_upload");
            if(isAllowedUpload) {
                if(!albumToUpdate.isUserAllowedUploadAccess(friend)) {
                    albumToUpdate.allowUploadAccess(friend);
                }
            }
            else {
                if(albumToUpdate.isUserAllowedUploadAccess(friend)) {
                    albumToUpdate.disallowUploadAccess(friend);
                }
            }
        }
    }
}
else if(action.equals("email_share")) {
    Album albumToShare = Album.get(RequestUtil.getParameter(request, "aid"));
    if(!user.ownsAlbum(albumToShare) && !user.isFriendsWith(albumToShare.getOwner())) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not have the permission to share the album");
    }
    
    JSONArray descriptors = new JSONArray(request.getParameter("descriptors"));
    
    Util.log("email_share: " + albumToShare.getId() + ": " + descriptors.toString(3));
    
    for(int i=0; i<descriptors.length(); i++) {
        JSONObject descriptor = descriptors.getJSONObject(i);
        String recipientName = descriptor.getString("name");
        String recipientEmail = descriptor.getString("email");
        
        if(descriptor.has("share")) {
            boolean isShared = descriptor.getBoolean("share");
            if(!isShared) {
                continue;
            }
        }
        
        boolean allowUpload = descriptor.optBoolean("allow_upload", false);
        if(allowUpload && !user.canAllowUploadToAlbum(albumToShare)) {
            throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not have the permission to allow upload to the album");
        }
        
        EmailShareService.submit(recipientEmail, recipientName, albumToShare, user, allowUpload);
    }
    
}

else if(action.equals("pull")) {
    Album sourceAlbum = Album.get(RequestUtil.getParameter(request, "source_album_id"));
    Album destinationAlbum = Album.get(RequestUtil.getParameter(request, "destination_album_id"));
    
    Util.log("pull: aid: %s -> %s(%s)", 
            RequestUtil.getParameter(request, "source_album_id"),
            RequestUtil.getParameter(request, "destination_album_id"),
            user.getId());
    
    if(sourceAlbum == null) {
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, "invalid source_album_id");
    }
    if(destinationAlbum == null) {
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, "invalid destination_album_id");
    }
    
    if(!user.ownsAlbum(destinationAlbum)) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not own destination album");
    }
    
    //source album could be own facebook album
    if(!(user.ownsAlbum(sourceAlbum) || (sourceAlbum.isFacebookAlbum() && sourceAlbum.getOwner().isFriendsWith(user)) || sourceAlbum.isSharedWithUser(user))) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not have permission to access source album");
    }
    
    destinationAlbum.pullAlbum(sourceAlbum);
    
    //note: don't disclose destination album info, might not be shared
    User friend = sourceAlbum.getOwner();
    if(!friend.is(user)) {
        JSONObject notificationData = new JSONObject();
        notificationData.put("from_id", user.getId());
        notificationData.put("from_first_name", user.getFirstName());
        notificationData.put("from_last_name", user.getLastName());
        notificationData.put("from_full_name", user.getName());
        notificationData.put("source_album_id", sourceAlbum.getId());
        notificationData.put("source_album_name", sourceAlbum.getName());
        notificationData.put("thumbnail_url", sourceAlbum.getThumbnailURL());

        Notification notification = new Notification(NotificationType.ALBUM_PULLED, notificationData);
        friend.addNotification(notification);
        
        String message = String.format("%s pulled your album %s into their album", user.getName(), sourceAlbum.getName());
        NotificationService.sendNotification(friend, message);
    }
}
else if(action.equals("push")) {
    Album sourceAlbum = Album.get(RequestUtil.getParameter(request, "source_album_id"));
    Album destinationAlbum = Album.get(RequestUtil.getParameter(request, "destination_album_id"));
    
    Util.log("push: aid: %s -> %s(%s)", 
            RequestUtil.getParameter(request, "source_album_id"),
            RequestUtil.getParameter(request, "destination_album_id"),
            user.getId());
    
    if(sourceAlbum == null) {
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, "invalid source_album_id");
    }
    if(destinationAlbum == null) {
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, "invalid destination_album_id");
    }
    
    if(!user.ownsAlbum(sourceAlbum)) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not own source album");
    }
    
    if(!user.canUploadToAlbum(destinationAlbum)) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not have permission to upload to destination album");
    }
    
    sourceAlbum.pushToAlbum(destinationAlbum);
    
    //note: don't disclose source album info, might not be shared
    User friend = destinationAlbum.getOwner();
    if(!friend.is(user)) {
        JSONObject notificationData = new JSONObject();
        notificationData.put("from_id", user.getId());
        notificationData.put("from_first_name", user.getFirstName());
        notificationData.put("from_last_name", user.getLastName());
        notificationData.put("from_full_name", user.getName());
        notificationData.put("destination_album_id", destinationAlbum.getId());
        notificationData.put("destination_album_name", destinationAlbum.getName());
        notificationData.put("thumbnail_url", destinationAlbum.getThumbnailURL());

        Notification notification = new Notification(NotificationType.ALBUM_PUSHED, notificationData);
        friend.addNotification(notification);
        
        String message = String.format("%s pushed their album to %s", user.getName(), destinationAlbum.getName());
        NotificationService.sendNotification(friend, message);
    }
    
}
else if(action.equals("sync")) {
    
    Album localAlbum = Album.get(RequestUtil.getParameter(request, "local_album_id"));
    Album remoteAlbum = Album.get(RequestUtil.getParameter(request, "remote_album_id"));
    
    Util.log("sync: aid: %s -> %s(%s)", 
            RequestUtil.getParameter(request, "local_album_id"),
            RequestUtil.getParameter(request, "remote_album_id"),
            user.getId());
    
    if(localAlbum == null) {
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, "invalid local_album_id");
    }
    if(remoteAlbum == null) {
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, "invalid remote_album_id");
    }
    if(!user.ownsAlbum(localAlbum)) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not own local album");
    }
    if(!user.canUploadToAlbum(remoteAlbum)) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not have permission to upload to remote album");
    }
    
    localAlbum.syncWithAlbum(remoteAlbum);
    
    User friend = remoteAlbum.getOwner();
    if(!friend.is(user)) {
        JSONObject notificationData = new JSONObject();
        notificationData.put("from_id", user.getId());
        notificationData.put("from_first_name", user.getFirstName());
        notificationData.put("from_last_name", user.getLastName());
        notificationData.put("from_full_name", user.getName());
        notificationData.put("remote_album_id", remoteAlbum.getId());
        notificationData.put("remote_album_name", remoteAlbum.getName());
        notificationData.put("thumbnail_url", remoteAlbum.getThumbnailURL());

        Notification notification = new Notification(NotificationType.ALBUMS_SYNCED, notificationData);
        friend.addNotification(notification);
        
        String message = String.format("%s synced their album with your album %s", user.getName(), remoteAlbum.getName());
        NotificationService.sendNotification(friend, message);
    }
}

else if(action.equals("add_photos")) {
    Album recipientAlbum = Album.get(RequestUtil.getParameter(request, "aid"));
    JSONArray photoIDs = new JSONArray(RequestUtil.getParameter(request, "pids"));
    
    Util.log("add_photos: aid: %s - %s", 
            RequestUtil.getParameter(request, "aid"),
            RequestUtil.getParameter(request, "pids"),
            user.getId());
    
    if(recipientAlbum == null) {
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, "invalid aid");
    }
    if(!user.canAddPhotosToAlbum(recipientAlbum)) {
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, "user does not have permission to add photos to recipient album");
    }
    
    List<Photo> photos = new ArrayList<Photo>();
    for(int i=0; i<photoIDs.length(); i++) {
        Photo photo = Photo.get(photoIDs.getString(i));
        photos.add(photo);
    }
    Photo.addToAlbum(recipientAlbum, user, photos, PhotoSource.ADDED_PHOTOS, true);
}



%>