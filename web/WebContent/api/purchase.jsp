<%@page import="pe.snap.exceptions.APIException.APIExceptionType"%>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="org.json.*" %>
<%@ page import="pe.snap.model.*" %>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.exceptions.*"%>
<%

Logger logger = (Logger)request.getAttribute(Constants.REQUEST_LOGGER_ATTR);
User user = (User)request.getAttribute(Constants.REQUEST_USER_ATTR);
String deviceID = (String)request.getSession().getAttribute(Constants.SESSION_DEVICE_ID_ATTR);

String action = RequestUtil.getParameter(request, "action", new String[]{"verify_purchase"});
if(action.equals("verify_purchase")) {
    String receipt = request.getParameter("receipt");
    if(receipt == null) {
        throw new APIException(APIExceptionType.MISSING_PARAMETER, "missing parameter 'receipt'");
    }
    boolean verified = CreditsManager.verifyReceipt(receipt, user);
    
    JSONObject rv = new JSONObject();
    rv.put("valid", verified ? 1 : 0);
    out.print(rv.toString());
}
%>