<%@page import="pe.snap.exceptions.APIException.APIExceptionType"%>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="org.json.*"%>
<%@ page import="pe.snap.model.*" %>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.exceptions.*"%>
<%
Logger logger = (Logger)request.getAttribute(Constants.REQUEST_LOGGER_ATTR);
User user = (User)request.getAttribute(Constants.REQUEST_USER_ATTR);
String deviceID = (String)request.getSession().getAttribute(Constants.SESSION_DEVICE_ID_ATTR);

String pidJSONString = RequestUtil.getParameter(request, "pid");
JSONArray pidArray = new JSONArray(pidJSONString);

for(int i=0; i<pidArray.length(); i++) {
    String pid = pidArray.getString(i);
    
    Photo photo = Photo.get(pid);
    if(photo == null) {
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, String.format("invalid pid %s", pid));
    }

    if(photo.isDeleted()) {
        Util.log("photo %s reported_missing is already deleted", photo.getId());
        continue;
    }

    Album album = photo.getAlbum();
    User albumOwner = album.getOwner();
    if(!user.getId().equals(albumOwner.getId()) && !user.isFriendsWith(albumOwner)) {
        String message = String.format("user or friends don't own photo %s", pid);
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, message);
    }

    if(album.isFacebookAlbum()) {
        FacebookManager.deleteFacebookPhotoIfDeletedOnFacebook(user.getFacebookAccessToken(), photo);
    }
    else {
        Util.log("404 on internal photo link: %s (%s)", photo.getId(), photo.getURL());
    }    
}

%>