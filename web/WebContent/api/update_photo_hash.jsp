<%@ page trimDirectiveWhitespaces="true"%>
<%@page import="java.util.*"%>
<%@page import="pe.snap.model.*"%>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.infra.*"%>
<%@page import="org.json.*"%>
<%
User user = (User)request.getAttribute(Constants.REQUEST_USER_ATTR);

JSONArray hashes = new JSONArray(RequestUtil.getParameter(request, "hashes"));
for(int i=0; i<hashes.length(); i++) {
    JSONObject hashObj = hashes.getJSONObject(i);
    String id = hashObj.getString("id");
    String hash = hashObj.optString("hash", null);
    String pHash = hashObj.optString("pHash", null);
    
    Photo p = Photo.get(id);
    if(p == null) {
        Util.log("error while updating photo hash, no photo with id: %s", id);
    }
    else {
        p.updateHashes(hash, pHash);
    }
}

%>