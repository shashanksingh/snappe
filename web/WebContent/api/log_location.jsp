<%@page import="pe.snap.services.LocationLoggingService"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@page import="pe.snap.model.*"%>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.infra.*"%>
<%@page import="org.json.*"%>
<%
User user = (User)request.getAttribute(Constants.REQUEST_USER_ATTR);
JSONArray locations = new JSONArray(RequestUtil.getParameter(request, "locations"));   
for(int i=0; i<locations.length(); i++) {
    JSONObject locationObj = locations.getJSONObject(i);
    double latitude = locationObj.getDouble("latitude");
    double longitude = locationObj.getDouble("longitude");
    double timestamp = locationObj.getDouble("timestamp");
    
    Util.log("loggging loc: %s, %f, %f, %f", user.getId(), latitude, longitude, timestamp);
    LocationLoggingService.submit(user, latitude, longitude, (int)timestamp);
}
%>