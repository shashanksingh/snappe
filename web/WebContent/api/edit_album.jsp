<%@page import="pe.snap.services.NotificationService"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="org.json.*" %>
<%@ page import="pe.snap.model.*" %>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.exceptions.*"%>
<%@page import="pe.snap.exceptions.APIException.*"%>
<%@page import="pe.snap.model.Notification.NotificationType"%>
<%
Logger logger = (Logger)request.getAttribute(Constants.REQUEST_LOGGER_ATTR);
User user = (User)request.getAttribute(Constants.REQUEST_USER_ATTR);

String action = RequestUtil.getParameter(request, "action", new String[]{
            "mark_photo_private", "mark_photo_public", "update_album", "change_allow_upload_access"
});

if(action.equals("mark_photo_private") || action.equals("mark_photo_public")) {
    boolean isPrivate = action.equals("mark_photo_private");
    JSONArray photoIDs = new JSONArray(RequestUtil.getParameter(request, "pid"));
    
    for(int i = 0; i<photoIDs.length(); i++) {
        String photoID = photoIDs.getString(i);
        
        Photo photo =  Photo.get(photoID);
        if(photo == null) {
            logger.warn("marking photo private, no photo with id: " + photoID);
            throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, String.format("invalid pid %s", photoID));
        }
        
        String ownerId = photo.getAlbum().getOwner().getId(); 
        if(!ownerId.equals(user.getId())) {
            logger.warn("attempt to change private state of other's photo: userId: " + user.getId() + ", photoId: " + photoID + ", action: " + action);
            throw new APIException(APIExceptionType.NOT_AUTHORIZED, "you can modify only those photos that you own");
        }
        
        photo.setPrivate(isPrivate);    
    }
    
}

else if(action.equals("update_album")) {
    String albumID = RequestUtil.getParameter(request, "aid");
    String property = RequestUtil.getParameter(request, "prop");
    String newValue = RequestUtil.getParameter(request, "val");
    
    Album album = Album.get(albumID);
    if(album != null && !user.ownsAlbum(album)) {
    	throw new APIException(APIExceptionType.NOT_AUTHORIZED, String.format("you can update only those albums that you own"));
    }
    
    if(album == null) {
    	logger.warn("updating album, no album with id: " + albumID);
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, String.format("invalid aid %s", albumID));
    }
    
    if(property.equals("name")) {
        album.setName(newValue);    
    }
    else if(property.equals("description")) {
        album.setDescription(newValue);      
    }
    else {
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, String.format("invalid album property %s", property));
    }
}

else if(action.equals("change_allow_upload_access")) {
    String albumID = RequestUtil.getParameter(request, "aid");
    String userID = RequestUtil.getParameter(request, "uid");
    boolean allow = RequestUtil.getParameter(request, "allow").equals("1");
    
    Album album = Album.get(albumID);
    User friend = User.get(userID);
    
    if(album == null) {
        logger.warn("change_allow_upload_access, no album with id: " + albumID);
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, String.format("invalid aid %s", albumID));
    }
    if(friend == null) {
        logger.warn("change_allow_upload_access, no user with id: " + userID);
        throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, String.format("invalid uid %s", userID));
    }
    if(!user.isFriendsWith(friend)) {
        logger.warn(String.format("change_allow_upload_access, %s is not a friend of current user: %s", userID, user.getId()));
        throw new APIException(APIExceptionType.NOT_AUTHORIZED, String.format("%s is not a friend", userID));
    }
    
    if(allow) {
        album.allowUploadAccess(friend); 
        JSONObject notificationData = new JSONObject();
        notificationData.put("from_id", user.getId());
        notificationData.put("from_first_name", user.getFirstName());
        notificationData.put("from_last_name", user.getLastName());
        notificationData.put("from_full_name", user.getName());
        notificationData.put("album_id", album.getId());
        notificationData.put("album_name", album.getName());
        notificationData.put("thumbnail_url", album.getThumbnailURL());

        Notification notification = new Notification(NotificationType.ADDED_AS_UPLOADER, notificationData);
        friend.addNotification(notification); 
        
        String message = String.format("%s added you as uploader to their album", user.getName());
        NotificationService.sendNotification(friend, message);
    }
    else {
        album.disallowUploadAccess(friend);
    }
    
}


%>