<%@page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="org.json.*" %>
<%@ page import="pe.snap.model.*" %>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.exceptions.*"%>
<%
Logger logger = (Logger)request.getAttribute(Constants.REQUEST_LOGGER_ATTR);
User user = (User)request.getAttribute(Constants.REQUEST_USER_ATTR);

String action = RequestUtil.getParameter(request, "action", new String[]{"get", "clear", "mark_shown"});
if(action.equals("get")) {
    JSONArray rv = new JSONArray();
    List<Notification> notifications = user.getPendingNotifications();
    for(Notification notification : notifications) {
        rv.put(notification.toJSON());   
    }
    out.print(rv.toString());
}
else if(action.equals("clear")) {
    user.clearAllNotifications();
}
else if(action.equals("mark_shown")) {
	JSONArray notificationIDs = new JSONArray(request.getParameter("id"));
    Set<String> shownNotifications = new HashSet<String>();
    for(int i=0; i<notificationIDs.length(); i++) {
        shownNotifications.add(notificationIDs.getString(i));   
    }
    user.clearNotifications(shownNotifications);
}

%>