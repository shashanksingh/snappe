<%@page import="pe.snap.services.ImageResizeService"%>
<%@page import="com.drew.metadata.exif.ExifSubIFDDirectory"%>
<%@page import="com.drew.metadata.Metadata"%>
<%@page import="pe.snap.model.*"%>
<%@page import="org.apache.commons.fileupload.util.Streams"%>
<%@page import="pe.snap.exceptions.APIException.APIExceptionType"%>
<%@page import="pe.snap.exceptions.APIException"%>
<%@page import="pe.snap.infra.*"%>
<%@page import="pe.snap.util.*"%>
<%@page import="java.util.*"%>
<%@page import="org.json.*"%>
<%@page import="java.io.InputStream"%>
<%@page import="org.apache.commons.fileupload.*" %>
<%@page import="org.apache.commons.fileupload.servlet.*" %>
<%@page import="org.apache.commons.fileupload.disk.*" %>
<%@page import="pe.snap.services.EmailShareService"%>
<%@page import="org.neo4j.graphdb.Transaction"%>

<%
if(request.getMethod().toUpperCase().equals("OPTIONS")) {
    response.setHeader("Access-Control-Allow-Origin", "*");
    return;
}


ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
List<FileItem> items = upload.parseRequest(request);

InputStream is = null;
String contentType = null;
String accessToken = null;
String albumID = null;

for(FileItem item : items) {
    if (!item.isFormField()) {
        is = item.getInputStream();
        contentType = item.getContentType();
    }
    else if("access_token".equals(item.getFieldName())) {
        accessToken = item.getString("UTF-8");
    }
    else if("aid".equals(item.getFieldName())) {
        albumID = item.getString("UTF-8");
    }
}


boolean allowedAccess = EmailShareService.isValidAccessToken(albumID, accessToken);
if(!allowedAccess) {
    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "access_token expired");
    return;
}

if(is == null) {
    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "missing file data");
    return;
}

if(!PhotoManager.isAcceptableContentType(contentType)) {
    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "unacceptable content-type: " + contentType);
}

PhotoMetadata metadata = PhotoManager.savePhoto(is, contentType);

Photo photo = null;
Transaction tx = DBManager.getTransaction();
try {
    photo = new Photo(metadata);
    tx.success();
}
finally {
    tx.finish();
}


Map<String, String> uploadedPhotoHashes = (Map<String, String>)session.getAttribute(Constants.SESSION_UPLOADED_PHOTO_HASHES);
if(uploadedPhotoHashes == null) {
    uploadedPhotoHashes = new HashMap<String, String>();
    session.setAttribute(Constants.SESSION_UPLOADED_PHOTO_HASHES, uploadedPhotoHashes);
}
uploadedPhotoHashes.put(metadata.hash, photo.getId());

//CreditsManager.decrementUserCredits(user);

JSONObject rv = new JSONObject();
JSONObject fileJSON = new JSONObject();
fileJSON.put("name", String.format("%s.jpg", metadata.hash));
fileJSON.put("size", 0);
fileJSON.put("url", photo.getURL());
fileJSON.put("thumbnail_url", photo.getThumbnailURL());
fileJSON.put("delete_url", "");
fileJSON.put("delete_type", "DELETE");
fileJSON.put("hash", metadata.hash);


JSONArray fileJSONList = new JSONArray();
fileJSONList.put(fileJSON);
rv.put("files", fileJSONList);

response.setContentType("application/json");
out.println(rv.toString());
%>