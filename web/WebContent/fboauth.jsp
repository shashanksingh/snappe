<%@page import="org.json.*"%>
<html>
    <head>
        <script type="text/javascript">
            if(window.top.location != window.location) {
                window.top.location.href = document.location.href;
            }
        </script>
        <link href="/static/css/main.css" rel="stylesheet" type="text/css" />
        <link href="/static/css/download.css" rel="stylesheet" type="text/css" />
        <link href="/static/css/common.css" rel="stylesheet" type="text/css" />
        <link href="/static/js/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="/static/js/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/static/js/util.js"></script>
        <script type="text/javascript" src="/static/js/analytics.js"></script>
        
        <script type="text/javascript">
            var FB_APP_ID = "413004668729952";
            
            function fbLogin() {
                FB.login(function(response){
                    if(window.console) {
                        console.log(response);
                    }
                    if (response.status === 'connected') {
                        var accessToken = response.authResponse.accessToken;
                        var redirectURL = window.location.href;
                        redirectURL = window.util.ensureURLParameter(redirectURL, "access_token", accessToken);
                        window.location.href = redirectURL;
                    }
                    else {
                        $("#auth_required_popup").modal();
                    }
                });
            }
            
            $(function(){
                $(".fb_login_button").click(function(e){
                    e.preventDefault();
                    fbLogin();
                });
                $("#auth_required_popup_ok_button").click(function(e){
                    e.preventDefault();
                    $("#auth_required_popup").modal('hide');
                });
            });
        </script>
    </head>
    <body>
        <div id="box_1">
            <div id="branding">
              <div id="branding_container">
                  <ul>
                      <li>
                          <a href="//snap.pe"><img id="app_icon" src="/static/images/snappe_icon_medium.png"/></a>
                      </li>
                      <li>
                          <span id="app_name"><a href="//snap.pe">Snappe</a></span>    
                      </li>
                      <li class="clearfix"></li>
                  </ul>
              </div>
              <div class="clearfix"></div>
            </div>
        </div>
        <div class="content_box">
            <div class="fb_login_box">
                <%
                    JSONObject fbReqeust = (JSONObject)request.getAttribute("fb_request");
                    if(fbReqeust != null) {
                        JSONObject data = fbReqeust.getJSONObject("data"); 
                        String senderID = data.getString("from_uid");
                        String senderName = data.getString("from_name");
                        String albumName = data.getString("album_name");
                %>
                <p class="request_info">
                    <img src="http://graph.facebook.com/<%=senderID%>/picture">
                    <span><%=senderName%> has shared their album <b><%=albumName%></b> with you</span>
                </p>
                <%}%>
                
                <p id="contact_main_message">Please authenticate yourself with Facebook to view the album</p>
                
                <p>
                    <a class="btn btn-large btn-success fb_login_button" href="#">Authenticate with Facebook</a>
                </p>
                
            </div>
        </div>
        
        <div id="auth_required_popup" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Authentication Required</h3>
            </div>
            <div class="modal-body">
                <p>In order to prevent unauthorized access to your friend's photos we need to verify your identity. Please connect with Facebook to do so.</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-primary" id="auth_required_popup_ok_button">OK</a>
            </div>
        </div>
        
        <div id="fb-root"></div>
        <script>
          window.fbAsyncInit = function() {
              // init the FB JS SDK
              FB.init({
                  appId      : FB_APP_ID,
                  channelUrl : '//snap.pe/fbchannel.html',
                  status     : true,
                  xfbml      : true
              });  
          };
        
          // Load the SDK asynchronously
          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/all.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>
    </body>
</html>