<%@page import="pe.snap.services.EmailShareService"%>
<%@page import="java.util.concurrent.ExecutionException"%>
<%@page import="pe.snap.util.RequestUtil"%>
<%@page import="org.json.*"%>
<%@page import="pe.snap.model.*"%>
<%@page import="pe.snap.util.*"%>
<%!
public static JSONArray getSlideshowJSON(Album album) throws ExecutionException, InterruptedException, JSONException {
    JSONArray rv = new JSONArray();
    for(Photo photo : album.getSortedAvailableNonPrivatePhotos()) {
        
        JSONObject slideJSON = new JSONObject();
        rv.put(slideJSON);
        
        slideJSON.put("image", photo.getURL());
        slideJSON.put("thumb", photo.getThumbnailURL());
        slideJSON.put("big", photo.getURL());
        
    }
    return rv;
}
%>
<%
String albumID = RequestUtil.getParameter(request, "aid");
String accessToken = RequestUtil.getParameter(request, "access_token");

boolean allowedAccess = EmailShareService.isValidAccessToken(albumID, accessToken);
if(!allowedAccess) {
    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "access_token expired");
    return;
}

boolean allowUpload = EmailShareService.isUploadAllowed(accessToken); 

Album album = Album.get(albumID);
if(album == null) {
    response.sendError(HttpServletResponse.SC_NOT_FOUND);
    return;
}
User owner = album.getOwner();

String ownerName = owner.getName();
String albumName = album.getName();
String downloadLink = EmailShareService.getAlbumZipDownloadLink(albumID, accessToken);
JSONArray slideshowJSON = getSlideshowJSON(album);

JSONObject rv = new JSONObject();
rv.put("ownerName", ownerName);
rv.put("albumName", albumName);
rv.put("downloadLink", downloadLink);
rv.put("allowUpload", allowUpload);
rv.put("slideshowData", slideshowJSON);


%>
<%=rv.toString()%>
