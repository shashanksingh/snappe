//dependency: jquery
var MIXPANEL_API_KEY = "8cb2e9c9cb5b552e8771c0ff659929e4";

function browserFromUA() {
    var BROWSER_REGEX_IE = /msie ([^;]*);/;
    var BROWSER_REGEX_FF = "firefox/(\\d\\d?\\.\\d)";
    
    if(!String.prototype.contains) {
        String.prototype.contains = function(substr) {
            return this.indexOf(substr) >= 0;
        }
    }
    
    var ag = window.navigator.userAgent;
    ag = ag.toLowerCase();
    
    var browser = "UNKNOWN";
    
    if(ag.contains("msie")) {
        //trying to see through compatibility mode
        if (ag.contains("trident/4.0")) {
            return "IE 8.0";
        }
        else if (ag.contains("trident/5.0")) {
            return "IE 9.0";
        }
        else if (ag.contains("trident/6.0")) {
            return "IE 10.0";
        }
        
        var match = ag.match(BROWSER_REGEX_IE);
        if(match) {
            return "IE " + match[1]
        }
        return "IE";
    }
    if(ag.contains("opera")) {
        return "OP";
    }
    if(ag.contains("chrome")) {
        return "CR";
    }
    if(ag.contains("firefox")) {
        var match = ag.match(BROWSER_REGEX_FF);
        if(match) {
            return "FF " + match[1];
        }
        return "FF";
    }
    if((ag.contains("safari") && ag.contains("version")) || ag.contains("mobile safari")) {
        return "SF";
    }
    if(ag.contains("iphone;")) {
        return "iPhone";
    }
    if(ag.contains("ipad;")) {
        return "iPad";
    }
    if (ag.contains("libwww-perl") || ag.contains("web spider")
            || ag.contains("infoaxe./nutch") || ag.contains("googlebot/")
            || ag.contains("httpclient")) 
    {
        return "Spider";
    }
    if (ag.contains("blackberry")) {
        return "Blackberry";
    }
    if (ag.contains("browserng") || ag.contains("nokia")) {
        return "Nokia";
    }
    if (ag.contains("pantech")) {
        return "Pantech";
    }
    if (ag.contains("dolfin/")) {
        return "Dolfin";
    }

    return "UNKNOWN";
    
}

function browserAndVersionFromUA() {
    var browser = browserFromUA();
    var version = "";
    var brArr = browser.split(" ");
    if (brArr.length > 1) {
        browser = brArr[0];
        version = brArr[1];
    }
    return [browser, version];
}

function osFromUA() {
    if(!String.prototype.contains) {
        String.prototype.contains = function(substr) {
            return this.indexOf(substr) >= 0;
        }
    }
    
    var ag = window.navigator.userAgent;
    ag = ag.toLowerCase();
    
    if (ag.contains("windows 3.11"))
        return "Win16";
    if (ag.contains("windows 95"))
        return "Win95";
    if (ag.contains("windows 98") || ag.contains("win98"))
        return "Win98";
    if (ag.contains("windows 2000") || ag.contains("windows nt 5.0")
            || ag.contains("windows nt 5.01"))
        return "Windows 2000";
    if (ag.contains("windows xp") || ag.contains("windows nt 5.1"))
        return "Windows XP";
    if (ag.contains("windows server 2003") || ag.contains("windows nt 5.2"))
        return "Windows NT 5.2";
    if (ag.contains("windows vista") || ag.contains("windows nt 6.0"))
        return "Windows Vista";
    if (ag.contains("windows 7") || ag.contains("windows nt 6.1"))
        return "Windows 7";
    if (ag.contains("windows 8") || ag.contains("windows nt 6.2"))
        return "Windows 8";
    if (ag.contains("windows nt 4.0") || ag.contains("windows nt"))
        return "Win NT";
    if (ag.contains("windows me"))
        return "Windows ME";
    if (ag.contains("windows ce"))
        return "Windows CE";
    if (ag.contains("ipod;")) {
        return "mob_iPod";
    }
    if (ag.contains("iphone;")) {
        return "mob_iPhone";
    }
    if (ag.contains("ipad;")) {
        return "tab_iPad";
    }
    if (ag.contains("mac os"))
        return "Mac OS";
    if (ag.contains("symbianos") || ag.contains("series 60")
            || ag.contains("series60")) {
        return "mob_symbian";
    }
    if (ag.contains("windows phone")) {
        return "mob_windows";
    }
    if (ag.contains("j2me")) {
        return "mob_J2ME";
    }
    if (ag.contains("blackberry")) {
        return "mob_Blackberry";
    }
    if (ag.contains("android")) {
        if (ag.contains("mobile")) {
            return "mob_Android";
        }
        else {
            return "tab_Android";
        }
    }
    if (ag.contains("nokia")) {
        return "mob_Nokia";
    }
    if (ag.contains("bada/") || ag.contains("samsung")) {
        return "mob_Samsung";
    }
    if (ag.contains("sonyericsson")) {
        return "mob_SonyEricsson";
    }

    if (ag.contains("libwww-perl") || ag.contains("web spider")
            || ag.contains("infoaxe./nutch") || ag.contains("googlebot/")
            || ag.contains("httpclient")) {
        return "Spider";
    }

    if (ag.contains("open bsd"))
        return "OpenBSD";
    if (ag.contains("sun os") || ag.contains("sunos"))
        return "SunOS";
    if (ag.contains("linux"))
        return "Linux";

    return "UNKNOWN";
}

function decodeURLParam(paramVal) {
    if(paramVal === null) return null;
    return unescape(paramVal.replace(/\+/g, " "));
}

function getURLParameter(paramName, url) {
    if(url === undefined) {
        url =  window.location.href;
    }

    paramName = paramName.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+paramName+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec(url);

    var rv = null;
    if(results != null) {
        rv = results[1];
    }

    return decodeURLParam(rv);
}

function setupMixpanel() {
    (function(c,a){window.mixpanel=a;var b,d,h,e;b=c.createElement("script");
    b.type="text/javascript";b.async=!0;b.src=("https:"===c.location.protocol?"https:":"http:")+
    '//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';d=c.getElementsByTagName("script")[0];
    d.parentNode.insertBefore(b,d);a._i=[];a.init=function(b,c,f){function d(a,b){
    var c=b.split(".");2==c.length&&(a=a[c[0]],b=c[1]);a[b]=function(){a.push([b].concat(
    Array.prototype.slice.call(arguments,0)))}}var g=a;"undefined"!==typeof f?g=a[f]=[]:
    f="mixpanel";g.people=g.people||[];h=['disable','track','track_pageview','track_links',
    'track_forms','register','register_once','unregister','identify','alias','name_tag','set_config',
    'people.set','people.set_once','people.increment','people.track_charge','people.append'];
    for(e=0;e<h.length;e++)d(g,h[e]);a._i.push([b,c,f])};a.__SV=1.2;})(document,window.mixpanel||[]);
    mixpanel.init(MIXPANEL_API_KEY);
}

function track(event, properties) {
    
    var brArr = browserAndVersionFromUA();
    var browser = brArr[0];
    var version = brArr[1];
    var os = osFromUA();
    
    var source = getURLParameter("s");
    
    var props = {
        "browser_version": version,
        "group_br": browser,
        "group_br_v": browser+" "+version,
        "group_os": os,
        "group_os_br": os+" - "+browser,
        "group_os_br_v" : os+" - "+browser+" "+version,
        "source": source 
    };
    
    if(properties) {
        for(var k in properties) {
            props[k] = properties[k];
        }    
    }
    
    mixpanel.track(event, props);    
}

setupMixpanel();

