if(!window.pe) {
    window.pe = {};
}
if(!pe.snap) {
    pe.snap = {};
}

pe.snap.Util = function() {
    
};

pe.snap.Util.prototype = {
    ensureURLParameter: function(url, paramName, paramValue) {
        var paramAlreadyExists = url.match(new RegExp("([?|&])" + paramName + "="));
        if(paramAlreadyExists) {
            return this.replaceURLParameter(url, paramName, paramValue);   
        }
        else {
            return this.addURLParameter(url, paramName, paramValue);
        }
    },
    replaceURLParameter: function(url, paramName, paramValue) {
        paramValue = this.encodeQueryParameter(paramValue);
        return url.replace(new RegExp("([?|&])" + paramName + "=.*?(&|$|#)","i"), '$1' + paramName + "=" + paramValue + '$2');    
    },
    addURLParameter: function(url, paramName, paramValue) {
        paramName = this.encodeQueryParameter(paramName);
        paramValue = this.encodeQueryParameter(paramValue);
        
        var ref = "";
        var hashIndex = url.indexOf("#"); 
        if(hashIndex >= 0) {
            ref = url.substring(hashIndex);
            url = url.substring(0, hashIndex);
        }
        
        if(url.indexOf('?') < 0) {
            url += "?" + paramName + "=" + paramValue;
        }
        else {
            url += "&" + paramName + "=" + paramValue;
        }
        
        return url + ref;
    },
    encodeQueryParameter: function(val) {
        var rv = encodeURIComponent(val);
        return rv.replace(/%2B/, '+');
    },
};

if(!window.util) {
    window.util = new pe.snap.Util();
}