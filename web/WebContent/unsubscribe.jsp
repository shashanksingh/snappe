<%@page import="pe.snap.services.EmailService"%>
<%
String emailID = request.getParameter("id");
if(emailID == null) {
    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "missing 'id'");
    return;
}

String recipientEmail = EmailService.unsubscribe(emailID);
if(recipientEmail == null) {
    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "bad 'id'");
    return;    
}

%>
<html>
    <head>
    </head>
    <body>
        <p><i><%=recipientEmail%></i> has been successfully unsubscribed from Snappe emails</p>
        <a href="http://snap.pe">Go To Home Page</a>
    </body>
</html>