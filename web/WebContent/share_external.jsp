<%@page import="pe.snap.model.Album.PhotoSource"%>
<%@page import="pe.snap.model.*"%>
<%@page import="java.util.*"%>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.services.EmailShareService"%>
<%@page import="org.neo4j.graphdb.Transaction"%>
<%@page import="pe.snap.infra.*"%>
<%@page import="org.json.*"%>
<%
String[] hashes = request.getParameterValues("hash");
if(hashes == null) {
    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "missing hash parameter");
    return;
}

String albumID = request.getParameter("aid");
Album album = Album.get(albumID);
if(album == null) {
    response.sendError(HttpServletResponse.SC_NOT_FOUND);
    return;
}

String accessToken = request.getParameter("access_token");

boolean allowedAccess = EmailShareService.isValidAccessToken(albumID, accessToken);
if(!allowedAccess) {
    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "access_token expired");
    return;
}

boolean allowedUpload = EmailShareService.isUploadAllowed(accessToken);
if(!allowedUpload) {
    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "upload operation not permitted");
    return;
}

Map<String, String> uploadedPhotoHashToID = (Map<String, String>)session.getAttribute(Constants.SESSION_UPLOADED_PHOTO_HASHES);
if(uploadedPhotoHashToID == null) {
    uploadedPhotoHashToID = new HashMap<String, String>();
}

List<String> hashesToShare = new ArrayList<String>();
for(String hash :  hashes) {
    if(!uploadedPhotoHashToID.containsKey(hash)) {
        Util.log("photo %s not uploaded in this session by this user", hash);
    }
    else {
        hashesToShare.add(hash);
    }
}

Transaction tx = DBManager.getTransaction();
try {
    List<Photo> photos = new ArrayList<Photo>();
    for(String hash : hashesToShare) {
        String id = uploadedPhotoHashToID.get(hash);
        Photo photo = Photo.get(id);
        photos.add(photo);
    }
    
    JSONObject recipient = EmailShareService.getRecipient(accessToken);
    String sharerUID = recipient.optString("uid", null);
    
    if(sharerUID == null) {
        String recipientName = recipient.getString("email");
        Photo.addToAlbum(album, recipientName, photos, PhotoSource.EMAIL, false);    
    }
    else {
        //all user friends are already in the graph
        //only user friends can be the ones adding photos (emails is taken care
        //of above) so we always have a user object for an authenticated FB user
        User sharer = User.get(sharerUID);
        Photo.addToAlbum(album, sharer, photos, PhotoSource.ADDED_PHOTOS, false);
    }
    
    tx.success();
}
finally {
    tx.finish();
}


%>