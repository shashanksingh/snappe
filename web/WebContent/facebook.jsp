<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.json.*"%>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.services.*"%>
<%!
private static void redirectToHomeOnError(HttpServletRequest request, HttpServletResponse response) throws IOException {
    String redirectURL = Util.ensureURLParameters(Constants.getServerURL(), "s", "fber");
    response.sendRedirect(redirectURL);
}
private static void redirectToHomeOnInvite(HttpServletRequest request, HttpServletResponse response) throws IOException {
    String redirectURL = Util.ensureURLParameters(Constants.getServerURL(), "s", "fbinvite");
    response.sendRedirect(redirectURL);
}
%>

<%
String clickingThroughUserID = null;
String fbAccessToken = null;

String code = request.getParameter("code");
//fucking FB code verification does not work, it always returns
//"Error validating verification code. Please make sure your r
//edirect_uri is identical to the one you used in the OAuth dialog request"
//no idea what redirect_url will work
//users will have to re-authorize every time
//https://developers.facebook.com/docs/facebook-login/login-flow-for-web-no-jssdk/
//https://developers.facebook.com/bugs/118684588311937
if(false && code != null) {
    fbAccessToken = FacebookManager.getAccessTokenForCode(code);
}
else {
    fbAccessToken = request.getParameter("access_token");    
}

if(fbAccessToken != null) {
    clickingThroughUserID = FacebookManager.verifyAccessToken(fbAccessToken);
    if(clickingThroughUserID == null) {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "invalid access_token");
        return;
    }
    
    session.setAttribute(Constants.SESSION_FB_TOKEN_ATTR, fbAccessToken);
}
else {
    String signedRequestString = request.getParameter("signed_request");
    if(signedRequestString != null) {
        SignedRequest signedRequest = new SignedRequest(signedRequestString);
        if(!signedRequest.verify(Constants.FACEBOOK_APP_SECRET)) {
            Util.log("facebook.jsp, signed_request verification failed");
            redirectToHomeOnError(request, response);
            return;
        }

        JSONObject params = signedRequest.getData();
        Util.log("facebook.jsp request signed_request params: %s", params.toString(3));
        
        clickingThroughUserID = params.optString("user_id", null);
        if(clickingThroughUserID == null) {
            Util.log("facebook.jsp, missing user_id in signed request");
        }
    }
    else {
        Util.log("facebook.jsp, signed_request absent");
        
    }    
}

if(clickingThroughUserID != null) {
    session.setAttribute(Constants.SESSION_USERID_ATTR, clickingThroughUserID);
    //sessions get lost in redirection, use cookie as a fallback for now
    Util.setCookie(response, Constants.SESSION_USERID_ATTR, clickingThroughUserID, Constants.getServerDomain());
}


String requestIDsParams = request.getParameter("request_ids");
if(requestIDsParams == null) {
    String redirectURL = Util.ensureURLParameters(Constants.getServerURL(), "s", "fb", "u", clickingThroughUserID);
    Util.log("redirecting to: %s", redirectURL);
    response.sendRedirect(redirectURL);
    return;    
}

List<String> requestIDs = Arrays.asList(requestIDsParams.split(","));

JSONArray appRequests = FacebookManager.getRequests(requestIDs);
Util.log("facebook.jsp, appRequests: %s", appRequests.toString(2));

List<JSONObject> appRequestsList = new ArrayList<JSONObject>();
for(int i=0; i<appRequests.length(); i++) {
    JSONObject appRequest = appRequests.getJSONObject(i);
    if(!appRequest.has("recipient_uid") || appRequest.getString("recipient_uid").equals("0")) {
        appRequest.put("recipient_uid", clickingThroughUserID == null ? "" : clickingThroughUserID);
    }
    appRequestsList.add(appRequest);
}

Collections.sort(appRequestsList, new Comparator<JSONObject>(){
    public int compare(JSONObject o1, JSONObject o2) {
        try {
            return (int)(o2.getJSONObject("data").getDouble("ts") - o1.getJSONObject("data").getDouble("ts"));    
        }
        catch(JSONException e) {
            return 0;
        }
                 
    }
});

//TODO:show the clicking through user a list of requests
JSONObject latestRequest = appRequestsList.get(0);
String type = latestRequest.getJSONObject("data").optString("type");
if("friend_invite".equals(type)) {
    Util.log("friend invite: " + latestRequest.toString(3));
    redirectToHomeOnInvite(request, response);
    return;
}

if(clickingThroughUserID != null) {
    
    String accessToken = EmailShareService.getOrCreateAccessTokenForVerifiedFacebookRequest(latestRequest);
    String albumID = latestRequest.getJSONObject("data").getString("album_id");

    //because we are using appspot as https proxy, cookies won't work
    String redirectURL = EmailShareService.getSharedAlbumLink(albumID, accessToken);
    redirectURL = Util.ensureURLParameters(redirectURL, "u", clickingThroughUserID);
    
    response.sendRedirect(redirectURL);
}
else {
    //extra redirection required only until we get an HTTPS url of our own
    //(in place of using appspot)
    if(request.getParameter("redir") == null) {
        String redirectURL = String.format("%s/facebook/?%s&redir=1", Constants.getServerURL(), request.getQueryString());
        response.sendRedirect(redirectURL);
        return;
    }
    
    RequestDispatcher rd = request.getRequestDispatcher("fboauth.jsp");
    request.setAttribute("fb_request", latestRequest);
    rd.include(request, response);
}
%>