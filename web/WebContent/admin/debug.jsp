<%@page import="pe.snap.infra.AdminOperations"%>
<%@page import="org.json.JSONArray"%>
<%@page import="pe.snap.infra.PhotoManager.PhotoSize"%>
<%@page import="pe.snap.infra.PhotoManager"%>
<%@page import="pe.snap.imagehash.ImagePHash"%>
<%@page import="pe.snap.infra.DBManager"%>
<%@page import="org.neo4j.graphdb.Transaction"%>
<%@page import="pe.snap.model.*"%>
<%@page import="pe.snap.util.*"%>
<%@page import="pe.snap.services.http.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.opencv.core.*"%>
<%@page import="com.googlecode.javacv.cpp.opencv_core.*"%>
<%!
public static String DEFAULT_UID = "654175641";
%>

<%
String q = request.getParameter("q");
String u = request.getParameter("u");
if(u == null) {
    u = DEFAULT_UID;
}
int t = 0;
try {
    t = Integer.parseInt(request.getParameter("t"));
}
catch(NumberFormatException nfe) {
    
}
if("app_access_token".equals(q)) {
    response.setContentType("text/plain");
    out.print(FacebookManager.getAppAccessToken());
}
else if("graph".equals(q)) {
    
    response.setContentType("application/json");
    out.print(User.get(u).getGraphChanges(t).toString(3));
}
else if("phash".equals(q)) {
    String hash = request.getParameter("h");
    if(hash != null) {
        String fullPath = PhotoManager.getFilePath(hash, PhotoSize.FULL);
        String standardPath = PhotoManager.getFilePath(hash, PhotoSize.STANDARD);
        
        String fullPHash = ImagePHash.getHash(new FileInputStream(new File(fullPath)));
        String standardPHash = ImagePHash.getHash(new FileInputStream(new File(standardPath)));
        int distance = ImagePHash.getHashDistance(Long.parseLong(fullPHash.substring(1), 16), Long.parseLong(standardPHash.substring(1), 16));
        
        response.setContentType("text/plain");
        out.println("fullPHash: " + fullPHash);
        out.println("standardPHash: " + standardPHash);
        out.println("distance: " + distance);    
    }
    else {
        String url = request.getParameter("url");
        String h = ImagePHash.getHash(HTTPService.get(url).getData());
        response.setContentType("text/plain");
        out.println(url + " : " + h);
    }
}
else if("photos".equals(q)) {
    response.setContentType("application/json");
    String albumID = request.getParameter("aid");
    Album album = Album.get(albumID);
    for(Photo photo : album.getPhotos()) {
        out.println(photo.getChangeSet(0).toString(3));
    }
}
else if("photo".equals(q)) {
    response.setContentType("application/json");
    String hash = request.getParameter("h");
    if(hash != null) {
        JSONArray rv = new JSONArray();
        for(Photo photo : Photo.getByHash(hash)) {
            out.print("{\"id\" : \"" + photo.getId() + "\"}");
            rv.put(photo.getChangeSet(0).toString(3));
        }
        out.print(rv.toString(3));    
    }
    else {
        String id = request.getParameter("id");
        Photo photo = Photo.get(id);
        out.print("{\"id\" : \"" + photo.getId() + "\"}");
        out.print(photo.getChangeSet(0).toString(3));
    }
}
else if("photo_album".equals(q)) {
    response.setContentType("application/json");
    String id = request.getParameter("id");
    Photo photo = Photo.get(id);
    Album album = photo.getAlbum();
    out.print(album.getChangeSet(0).toString(3));
}
else if("album".equals(q)) {
    response.setContentType("application/json");
    String id = request.getParameter("id");
    Album album = Album.get(id);
    out.print(album.getChangeSet(0).toString(3));
}
else if("delete_album".equals(q)) {
    String aid = request.getParameter("aid");
    Album album = Album.get(aid);
    
    Transaction tx = DBManager.getTransaction();
    try {
        AdminOperations.deleteAlbum(album);
        tx.success();
    }
    finally {
        tx.finish();
    }
    
}
else if("show_session".equals(q)) {
    Enumeration<String> keys = session.getAttributeNames();
    
    response.setContentType("text/plain");
    out.println("session attributes");
    
    while(keys.hasMoreElements()) {
        String key = keys.nextElement();
        Object value = session.getAttribute(key);
        
        out.println(String.format("%30s: %s", key, value == null ? "null" : value.toString()));
    }
}
else if("list_sorted_available_non_private_photos".equals(q)) {
    response.setContentType("text/plain");
    
    String aid = request.getParameter("aid");
    Album album = Album.get(aid);
    out.println(String.format("album: %s\n", album.getName()));
    
    for(Photo photo : album.getSortedPhotos()) {
        out.println(photo.getChangeSet(0) + "\n");    
    }
}
else {
    User user = User.get(u);
    String accessToken = user.getFacebookAccessToken();
    String accessTokenOwnerUserID = FacebookManager.verifyAccessToken(accessToken);
    
    Transaction tx = DBManager.getTransaction();
    try {
        FacebookGraphUpdater.update(user, t);
        tx.success();
    }
    finally {
        tx.finish();
    }
    out.print(String.format("access token: %s, uid: %s", accessToken, accessTokenOwnerUserID));
}
%>