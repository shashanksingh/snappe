<%@page import="pe.snap.util.Util"%>
<%@page isErrorPage="true" %>
<%@page import="org.json.JSONObject"%>
<%@page import="java.net.URI"%>
<%
response.setCharacterEncoding("UTF-8");

URI uri = new URI(request.getRequestURI());
String path = uri.getPath(); 
if(true || path != null && path.startsWith("/api")) {
    response.setContentType("application/json");
    JSONObject errorJSON = new JSONObject();
    errorJSON.put("code", -1);
    errorJSON.put("message", "unexpected error");
    errorJSON.put("success", false);
    out.print(errorJSON.toString(2));
}
else {
    Util.log("non api exception");
    out.print(exception.getLocalizedMessage());        
}
%>