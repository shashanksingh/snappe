package pe.snap.servlets;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;

import pe.snap.model.Album;
import pe.snap.services.AlbumZipService;
import pe.snap.services.EmailShareService;
import pe.snap.util.Util;

public class AlbumZipDownloadServlet extends HttpServlet {
    
    private static final long serialVersionUID = 6020623170546429718L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        String albumID = request.getParameter("aid");
        String accessToken = request.getParameter("access_token");        
        
        try {
            boolean allowedAccess = EmailShareService.isValidAccessToken(albumID, accessToken);
            if(!allowedAccess) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "access_token expired");
                return;
            }
            
            String filePath = AlbumZipService.getFilePath(albumID);
            File file = new File(filePath);
            if(!file.exists()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            
            String albumName = "album";
            Album album = Album.get(albumID);
            if(album != null) {
                albumName = album.getName();
            }
            
            response.setContentType("application/x-download"); 
            response.setHeader("Content-Disposition", String.format("attachment; filename=%s.zip", albumName));
            response.setContentLength((int)file.length());
            
            FileUtils.copyFile(file, response.getOutputStream());
        }
        catch(Throwable t) {
            try {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);    
            }
            catch(IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
