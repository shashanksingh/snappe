package pe.snap.exceptions;

import javax.servlet.http.HttpServletResponse;

public class APIException extends Exception {
	private static final long serialVersionUID = 1L;
	
	private final APIExceptionType type;
	private final String message;
	
	public enum APIExceptionType {
		ACCESS_TOKEN_REQUIRED(1, HttpServletResponse.SC_BAD_REQUEST),
		ACCESS_TOKEN_AUTH_FAILED(2, HttpServletResponse.SC_UNAUTHORIZED),
		MISSING_PARAMETER(3, HttpServletResponse.SC_BAD_REQUEST),
		NOT_AUTHORIZED(4, HttpServletResponse.SC_BAD_REQUEST),
		INVALID_PARAMETER_VALUE(5, HttpServletResponse.SC_BAD_REQUEST),
		FRIEND_ACCESS_TOKEN_EXPIRED(6, HttpServletResponse.SC_UNAUTHORIZED),
		INSUFFICIENT_CREDITS(7, HttpServletResponse.SC_UNAUTHORIZED);;
		
		int apiErrorCode;
		int httpErrorCode;
		
		APIExceptionType(int code, int httpErrorCode) {
			this.apiErrorCode = code;
			this.httpErrorCode = httpErrorCode;
		}
	}
	
	public APIException(APIExceptionType type, String message) {
		this.type = type;
		this.message = message;
	}
	
	public int getAPIErrorCode() {
		return this.type.apiErrorCode;
	}
	
	public int getHTTPErrorCode() {
		return this.type.httpErrorCode;
	}
	
	public APIExceptionType getAPIExceptionType() {
	    return this.type;
	}
	
	@Override
	public String getMessage() {
		return this.message;
	}
	
	@Override
	public String getLocalizedMessage() {
		return this.getMessage();
	}
}
