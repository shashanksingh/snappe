package pe.snap.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pe.snap.exceptions.APIException;
import pe.snap.model.Location;
import pe.snap.model.Tag;
import pe.snap.model.User;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.util.FacebookManager;
import pe.snap.util.Util;

public class UserFacebookLocationUpdaterService {
    
    private static final int BATCH_SIZE = 50;
    
    private static final Logger logger = Logger.getLogger(UserFacebookLocationUpdaterService.class);
    private static ExecutorService processor;
    
    public static synchronized void start() {
        if(processor == null) {
            //TODO: limit the number of threads
            processor = Executors.newCachedThreadPool();
        }
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    public static void submit(final List<User> users, final String accessToken, final int modifiedSince) {
        processor.submit(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                List<List<User>> batches = Util.split(users, BATCH_SIZE);
                for(List<User> batch : batches) {
                    Util.log("updating location for a batch of %d users", batch.size());
                    updateUserLocations(batch, accessToken, modifiedSince);
                    Util.log("successfully updated location for a batch of %d users", batch.size());
                }
                return null;
            }
        });
    }
    
    private static void updateUserLocations(List<User> users, String accessToken, int modifiedSince) throws UnsupportedEncodingException, IOException, JSONException, HTTPServiceException, InterruptedException, ExecutionException, APIException {
        if(users.isEmpty()) {
            return;    
        }
        
        final String Q_TEMPLATE = String.format("SELECT page_id, timestamp, latitude, longitude, author_uid, tagged_uids FROM location_post WHERE (%%s) AND timestamp >= %d", modifiedSince);
        
        Map<String, User> userIdToUserMap = new HashMap<String, User>();
        Map<String, Location> locationIdToLocationMap = new HashMap<String, Location>();
        
        
        List<String> perUserConditions = new ArrayList<String>();
        for(User user : users) {
            String uid = user.getId();
            userIdToUserMap.put(uid, user);
            
            String perUserCondition = String.format("author_uid = %s OR %s IN tagged_uids", uid, uid);
            perUserConditions.add(perUserCondition);
        }
        
        String q = String.format(Q_TEMPLATE, StringUtils.join(perUserConditions, " OR "));
        
        JSONArray taggedLocations = FacebookGraphService.executeQuery(accessToken, q);
        
        FacebookManager.translateFBLocationObjectsToLocal(taggedLocations);
        
        for(int i=0; i<taggedLocations.length(); i++) {
            JSONObject taggedLocationJSON = taggedLocations.getJSONObject(i);
            String locationId = taggedLocationJSON.getString("id");
            
            //stupid FB guys return an empty array for no tags, object
            //otherwise
            Object taggedUserIdsObj = taggedLocationJSON.get("tagged_uids");
            JSONArray taggedUserIds = null; 
            if(taggedUserIdsObj instanceof JSONObject) {
                JSONObject taggedUserIdsJSONObj = (JSONObject)taggedUserIdsObj; 
                taggedUserIds = taggedUserIdsJSONObj.toJSONArray(taggedUserIdsJSONObj.names());
            }
            else {
                taggedUserIds = (JSONArray)taggedUserIdsObj;
            }
            
            taggedUserIds.put(taggedLocationJSON.get("author_uid"));
            
            Location taggedLocation = locationIdToLocationMap.get(locationId);
            if(taggedLocation == null) {
                taggedLocation = Location.createOrUpdate(taggedLocationJSON);
                if(taggedLocation != null) {
                    locationIdToLocationMap.put(locationId, taggedLocation);    
                }
            }
            
            for(int j=0; j<taggedUserIds.length(); j++) {
                String taggedUserId = taggedUserIds.getString(j);
                User taggedUser = userIdToUserMap.get(taggedUserId);
                if(taggedUser != null && taggedLocation != null) {
                    //we want to worry about location of friends only
                    Tag.createOrUpdate(taggedUser, taggedLocation, taggedLocationJSON.getInt("timestamp"));
                }
            }
        }
    }
}
