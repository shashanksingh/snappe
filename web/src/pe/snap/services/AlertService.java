package pe.snap.services;

public class AlertService {
    private static final String[] ALERT_NOTIFICATION_EMAIL_LIST = new String[]{
        "shashank.sunny.singh@gmail.com"
    };
    
    public static void sendErrorAlert(String title, String message) {
        sendErrorAlert(title, message, null);
    }
    
    public static void sendErrorAlert(String title, String message, String html) {
        for(String email : ALERT_NOTIFICATION_EMAIL_LIST) {
            try {
                EmailService.sendEmail(email, "Snappe Error Alert: " + title, message, html);    
            }
            catch(Throwable t) {
                t.printStackTrace(System.err);
            }
        }
    }
}
