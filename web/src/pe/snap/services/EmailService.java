package pe.snap.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

import pe.snap.infra.ConnectionPool;
import pe.snap.model.Album;
import pe.snap.model.Photo;
import pe.snap.model.User;
import pe.snap.util.Analytics;
import pe.snap.util.Util;

public class EmailService {
    private static final int MAX_PHOTOS_PER_ALBUM  = 3;
    
    private static final String SMTP_HOST = "smtp.sendgrid.net";
    private static final String USERNAME = "snappe";
    private static final String PASSWORD = "mysendgridpwd";
    private static final String FROM_EMAIL_ADDRESS = "notifications@snap.pe";
    private static final String FROM_NAME = "Snappe";
    
    private static final String UNSUB_LINK_TEMPLATE = "http://snap.pe/unsubscribe.jsp?id=%s";
    
    private static Session mailSession;
    
    
    static {
        
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_HOST);
        props.put("mail.smtp.port", 587);
        props.put("mail.smtp.auth", "true");
        
        Authenticator auth = new SMTPAuthenticator();
        mailSession = Session.getInstance(props, auth);
        
    }
    
    private static enum EmailTemplate {
        ALBUM_SHARED_BY_EMAIL    
    };
    
    private static ExecutorService processor;
    
    public static synchronized void start() {
        if(processor == null) {
            processor = Executors.newSingleThreadExecutor();
        }
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    public static void sendAlbumShareByEmailNotification(final String recipientEmail, String recipientName, Album sharedAlbum, User sharer, String accessToken) throws SQLException {
        String fromName = getPrettyName(sharer.getName(), "A Snappe User");
        
        final Map<String, String> templateParams = new HashMap<String, String>();
        templateParams.put("to_name", getPrettyName(recipientName, recipientEmail));
        templateParams.put("from_name", fromName);
        templateParams.put("album_name", sharedAlbum.getName());
        
        templateParams.put("album_link", EmailShareService.getSharedAlbumLink(sharedAlbum.getId().toString(), accessToken));
        
        final String subject = String.format("%s Shared a Snappe Album with you", fromName);
        
        int i=0;
        for(Photo photo : sharedAlbum.getSortedAvailableNonPrivatePhotos()) {
            if(i >= MAX_PHOTOS_PER_ALBUM) {
                break;
            }
            
            String photoURL = photo.getURL();
            String key = String.format("photo_url_%d", i++);
            templateParams.put(key, photoURL);
        }
        
        final String emailID = insertEmailRecord(sharer.getId(), recipientEmail, EmailTemplate.ALBUM_SHARED_BY_EMAIL, templateParams);
        
        processor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    sendEmail(emailID, recipientEmail, subject, EmailTemplate.ALBUM_SHARED_BY_EMAIL, templateParams);
                } catch (IOException e) {
                    Util.log("warning: error in sending email: %s, %s, %s", subject, EmailTemplate.ALBUM_SHARED_BY_EMAIL, templateParams);
                    e.printStackTrace();
                } catch (MessagingException e) {
                    Util.log("warning: error in sending email: %s, %s, %s", subject, EmailTemplate.ALBUM_SHARED_BY_EMAIL, templateParams);
                    e.printStackTrace();
                }
                catch(Throwable t) {
                    Util.log("warning: error in sending email: %s, %s, %s", subject, EmailTemplate.ALBUM_SHARED_BY_EMAIL, templateParams);
                    t.printStackTrace();
                }
            }
        });
    }
    
    private static String getPrettyName(String name, String defaultName) {
        return StringUtils.defaultIfEmpty(name, defaultName);
    }
    
    public static String getTemplate(EmailTemplate template, boolean html) throws IOException {
        String suffix = html ? "html" : "txt";
        
        String path = String.format("emailtemplates/%s.%s", template.toString().toLowerCase(), suffix);
        InputStream is = EmailService.class.getResourceAsStream(path);
        return IOUtils.toString(is, "UTF-8");
    }
    
    private static void sendEmail(String emailID, String recipientEmail, String subject, EmailTemplate template, Map<String, String> templateParams) throws IOException, MessagingException, SQLException {
        
        if(templateParams.get("unsub_link") == null) {
            String unsubLink = String.format(UNSUB_LINK_TEMPLATE, Util.encodeURIComponent(emailID));
            templateParams.put("unsub_link", unsubLink);    
        }
        
        String textTemplateString = getTemplate(template, false);
        String filledTextTemplate = Util.printf(textTemplateString, templateParams);
        
        String htmlTemplateString = getTemplate(template, true);
        String filledHTMLTemplate = Util.printf(htmlTemplateString, templateParams);
        
        sendEmail(recipientEmail, subject, filledTextTemplate, filledHTMLTemplate);
        
        Analytics.serverLog("email_sent", "to", recipientEmail, "subject", subject, "template", template);
    }
    
    public static void sendEmail(String recipientEmail, String subject, String text, String html) throws MessagingException, UnsupportedEncodingException, SQLException {
        if(text == null && html == null) {
            throw new IllegalArgumentException("at least one of text and html should be non-null");
        }
        
        if(isUnsubscribedEmail(recipientEmail)) {
            Util.log("ignoring email %s to %s because the recipient is unsubscribed", subject, recipientEmail);
            return;
        }
        
        // uncomment for debugging infos to stdout
        //mailSession.setDebug(true);
        
        Transport transport = mailSession.getTransport();
        MimeMessage message = new MimeMessage(mailSession);
        Multipart multipart = new MimeMultipart("alternative");

        if(text != null) {
            BodyPart textPart = new MimeBodyPart();
            textPart.setText(text);
            multipart.addBodyPart(textPart);
        }

        if(html != null) {
            BodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(html, "text/html; charset=utf-8");
            multipart.addBodyPart(htmlPart);
        }        

        message.setContent(multipart);
        message.setFrom(new InternetAddress(FROM_EMAIL_ADDRESS, FROM_NAME));
        message.setSubject(subject);
        message.addRecipient(Message.RecipientType.TO,
             new InternetAddress(recipientEmail));

        transport.connect();
        transport.sendMessage(message,
            message.getRecipients(Message.RecipientType.TO));
        transport.close();

    }
    
    private static class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(USERNAME, PASSWORD);
        }
    }
    
    private static String insertEmailRecord(String fromUID, String toEmail, EmailTemplate type, Map<String, String> params) throws SQLException {
        String emailID = Util.getUUID();
        String paramsJSON = new JSONObject(params).toString();
        
        String q = "INSERT INTO emails (email_id, from_uid, to_email, email_type, params) VALUES (?, ?, ?, ?, ?)";
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, emailID);
            ps.setString(2, fromUID);
            ps.setString(3, toEmail);
            ps.setString(4, type.toString());
            ps.setString(5, paramsJSON);
            
            ps.executeUpdate();
            return emailID;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static boolean isUnsubscribedEmail(String email) throws SQLException {
        String q = "SELECT COUNT(1) AS matches FROM unsubscribed_emails JOIN emails ON (unsubscribed_emails.email_id = emails.email_id) WHERE to_email = ?";
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, email);
            
            ResultSet rs = ps.executeQuery();
            rs.next();
            
            int matchCount = rs.getInt("matches");
            return matchCount > 0;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static String getRecipientEmailForEmailID(String emailID) throws SQLException {
        String q = "SELECT to_email FROM emails WHERE email_id = ?";
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, emailID);
            
            ResultSet rs = ps.executeQuery();
            if(!rs.next()) {
                return null;
            }
            return rs.getString("to_email");
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static String unsubscribe(String emailID) throws SQLException {
        String recipientEmail = getRecipientEmailForEmailID(emailID);
        if(recipientEmail == null) {
            return null;
        }
        
        String q = "INSERT INTO unsubscribed_emails (email_id) VALUES (?)";
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, emailID);
            
            ps.executeUpdate();
            
            return recipientEmail;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static void main(String[] args) throws Exception {
        sendEmail(Util.getUUID(), "shashank@infoaxe.com", "Test", EmailTemplate.ALBUM_SHARED_BY_EMAIL, new HashMap<String, String>());
    }

}
