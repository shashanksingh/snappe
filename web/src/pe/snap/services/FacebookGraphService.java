package pe.snap.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.Transaction;

import pe.snap.exceptions.APIException;
import pe.snap.exceptions.APIException.APIExceptionType;
import pe.snap.infra.DBManager;
import pe.snap.model.User;
import pe.snap.services.http.HTTPService;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.services.http.HTTPServiceResponse;
import pe.snap.util.FacebookGraphUpdater;
import pe.snap.util.FacebookManager;
import pe.snap.util.Util;

public class FacebookGraphService {
    private static final String FQL_END_POINT = "https://graph.facebook.com/fql";
    
    private static final Logger logger = Logger.getLogger(FacebookGraphService.class);
    private static ExecutorService processor;
    
    public static synchronized void start() {
        if(processor == null) {
            //TODO: limit the number of threads
            processor = Executors.newCachedThreadPool();
        }
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    public static Future<Map<String, JSONArray>> executeMultiQueryAsync(final String accessToken, final Map<String, String> queries) {
        return processor.submit(new Callable<Map<String, JSONArray>>() {

            @Override
            public Map<String, JSONArray> call() throws Exception {
                logger.debug("queries: " + new JSONObject(queries).toString());
                return executeMultiQuery(accessToken, queries);
            }
        });
    }
    
    public static Future<Long> updateUserGraph(final User user, final int modifiedSince) throws InterruptedException, ExecutionException {
        final String id = user.getId();
        Callable<Long> callable = new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                Transaction tx = DBManager.getTransaction();
                try {
                    long startTime = System.currentTimeMillis();
                    FacebookGraphUpdater.update(User.get(id), modifiedSince);
                    tx.success();
                    return System.currentTimeMillis() - startTime;    
                }
                finally {
                    tx.finish();
                }
                    
            }
        };
        return processor.submit(callable);
    }
    
    public static Future<Object[]> getLocationForCoordinates(final List<double[]> coordinatesList, final String accessToken) {
        return processor.submit(new Callable<Object[]>() {
            @Override
            public Object[] call() throws Exception {
                long startTime = System.currentTimeMillis();
                JSONArray locations = new JSONArray();
                try {
                    locations = FacebookManager.getFacebookLocationForCoordinates(coordinatesList, accessToken);
                }
                catch(Throwable t) {
                    ExceptionReportingService.submit(t);
                }
                
                long endTime = System.currentTimeMillis();
                return new Object[]{locations, endTime - startTime};
            }
        });
    }
    
    public static Map<String, JSONArray> executeMultiQuery(String accessToken, Map<String, String> queries) throws UnsupportedEncodingException, IOException, JSONException, HTTPServiceException, APIException {
        
        Map<String, JSONArray> rv = new HashMap<String, JSONArray>();
        
        String queriesParam = new JSONObject(queries).toString();
        String responseData = doFQLQuery(accessToken, queriesParam);
        
        int rows = 0;
        JSONArray results = new JSONObject(responseData).getJSONArray("data");
        for(int i=0; i<results.length(); i++) {
            JSONObject result = results.getJSONObject(i);
            String name = result.getString("name");
            JSONArray resultSet = result.getJSONArray("fql_result_set");
            rows += resultSet.length();
            rv.put(name, resultSet);
        }
        
        logger.debug("total rows: " + rows);
        return rv;
    }
    
    protected static String doFQLQuery(String accessToken, String queryParam) throws HTTPServiceException, UnsupportedEncodingException, IOException, APIException {
        return doFQLQuery(accessToken, queryParam, 0);
    }
    
    protected static String doFQLQuery(String accessToken, String queryParam, int attempt) throws HTTPServiceException, UnsupportedEncodingException, IOException, APIException {
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("locale", "en_US");
        params.put("access_token", accessToken);
        params.put("q", queryParam);
        
        HTTPServiceResponse response = HTTPService.get(FQL_END_POINT, params);
        if(!response.isSuccess()) {
            String message = response.getStatusMessage();
            int code = response.getStatusCode();
            
            String responseData = response.getDataAsString();
            Util.log("FQL Error:access_token: %s\nFQL: %s\nError:%s", accessToken, queryParam, responseData);
            
            try {
                JSONObject responseJSON = new JSONObject(responseData);
                if(responseJSON.has("error")) {
                    JSONObject errorJSON = responseJSON.getJSONObject("error");
                    
                    message = errorJSON.getString("message");
                    code = errorJSON.getInt("code");
                    if(code == 1) {
                        Util.log("OAuthException code 1 happened");
                        if(attempt > 0) {
                            Util.log("already a retry, not-retrying");
                        }
                        else {
                            return doFQLQuery(accessToken, queryParam, 1);
                        }
                    }
                }
                else {
                    Util.log("facebook returned error without an error field, response for url: %s:\n%s", queryParam, responseJSON.toString(3));
                    message = responseJSON.getString("error_msg");
                    code = responseJSON.getInt("error_code");
                }
            }
            catch(JSONException ex) {
                ex.printStackTrace(System.err);
                ExceptionReportingService.submit(ex);
            }
            
            if(code == 190) {
                Util.log("bad access token: %s", accessToken);
                throw new APIException(APIExceptionType.ACCESS_TOKEN_AUTH_FAILED, message);
            }
            //Util.log(response.getDataAsString());
            throw new HTTPServiceException(code, message);
        }
        
        String rv = response.getDataAsString();
        //Util.log("%s", rv);
        return rv;
    }
    
    public static JSONArray executeQuery(String accessToken, String query) throws JSONException, UnsupportedEncodingException, IOException, HTTPServiceException, APIException {
        String responseData = doFQLQuery(accessToken, query);
        JSONArray results = new JSONObject(responseData).getJSONArray("data");
        return results;
    }
    
    public static String getINFragment(List<String> ids) {
        List<String> quoted = new ArrayList<String>();
        for(String id : ids) {
            quoted.add(String.format("'%s'", id));
        }
        return StringUtils.join(quoted, ',');
    }
}
