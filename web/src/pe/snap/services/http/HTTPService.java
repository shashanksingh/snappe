package pe.snap.services.http;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import pe.snap.util.Util;


public class HTTPService {

  private static final int MAX_TOTAL_CONNECTIONS = 100;
  private static final int MAX_CONNECTIONS_PER_ROUTE = 50;
  
  //just to ensure that on a long running system, resources are freed eventually
  public static final int GLOBAL_SOCKET_TIMEOUT = 60 * 60 * 1000;
  public static final int GLOBAL_CONNECTION_TIMEOUT = 1 * 60 * 1000;
  
  private static HttpClient client;
  
  //@TODO: linkedblocking queue can potentially grow infinitely
  private static ExecutorService asyncFetchService;

  public static void init() {
	  
	  if(client != null) return;
	  
      SchemeRegistry schemeRegistry = new SchemeRegistry();
      schemeRegistry.register(new Scheme("http", 80, PlainSocketFactory
          .getSocketFactory()));
      schemeRegistry.register(new Scheme("https", 443, SSLSocketFactory
          .getSocketFactory()));

      ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager(schemeRegistry);
      cm.setMaxTotal(MAX_TOTAL_CONNECTIONS);
      cm.setDefaultMaxPerRoute(MAX_CONNECTIONS_PER_ROUTE);

      HttpParams params = new BasicHttpParams();
      HttpConnectionParams.setConnectionTimeout(params, GLOBAL_CONNECTION_TIMEOUT);
      HttpConnectionParams.setSoTimeout(params, GLOBAL_SOCKET_TIMEOUT);
      
      client = new DefaultHttpClient(cm, params);
      
      asyncFetchService = new ThreadPoolExecutor(5, 20, 10 * 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

  }
  
  public static void destroy() {
    if(client != null) {
      client.getConnectionManager().shutdown();  
    }
    if(asyncFetchService != null) {
    	asyncFetchService.shutdown();
    }
  }
  
  public static HTTPServiceResponse get(String url) {
	  return get(url, -1);
  }
  
  public static HTTPServiceResponse get(String url, int timeoutMillis) {
	  HttpGet get = new HttpGet(url);
	  setRequestParamsTimeout(get.getParams(), timeoutMillis);
	  return execute(get);
  }
  
  public static HTTPServiceResponse get(String url, Map<String, String> params) {
	  url = addParamsToUrl(url, params);
	  return get(url);
  }
  
  public static Future<HTTPServiceResponse> getAsync(final String url) throws InterruptedException, ExecutionException {
	  return getAsync(url, -1);
  }
  
  public static Future<HTTPServiceResponse> getAsync(final String url, final int timeoutMillis) throws InterruptedException, ExecutionException {
	  return asyncFetchService.submit(new Callable<HTTPServiceResponse>() {

		@Override
		public HTTPServiceResponse call() throws Exception {
		    try {
		        return HTTPService.get(url, timeoutMillis);
            }
            catch(Exception t) {
                t.printStackTrace(System.err);
                throw t;
            }
		}
		  
	  });
  }
  
  public static Future<HTTPServiceResponse> getAsync(String url, Map<String, String> params) throws InterruptedException, ExecutionException {
	  url = addParamsToUrl(url, params);
	  return getAsync(url);
  }
  
  private static String addParamsToUrl(String url, Map<String, String> params) {
	  List<String> paramPairs = new ArrayList<String>();
	  for (Entry<String, String> param : params.entrySet()) {
		  paramPairs.add(String.format("%s=%s", Util.encodeURIComponent(param.getKey()), Util.encodeURIComponent(param.getValue())));
	  }
	    	    
	  url = url + "?" + StringUtils.join(paramPairs, '&');
	  return url;
  }
  
  public static HTTPServiceResponse post(String url, Map<String, String> params) throws UnsupportedEncodingException {
	  return post(url, params, -1);
  }
  
  public static HTTPServiceResponse post(String url, Map<String, String> params, int timeoutMillis) throws UnsupportedEncodingException {
	  return post(url, params, new HashMap<String, String>(), timeoutMillis);
  }
  
  public static HTTPServiceResponse post(String url, Map<String, String> params, Map<String, String> headers) throws UnsupportedEncodingException {
	  return post(url, params, headers, -1);
  }
  
  public static HTTPServiceResponse post(String url, Map<String, String> params, Map<String, String> headers, int timeoutMillis) throws UnsupportedEncodingException {
	  HttpPost httpPost = new HttpPost(url);
	  setRequestParamsTimeout(httpPost.getParams(), timeoutMillis);
	  
	  List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	  for(String k : params.keySet()) {
		  String v = params.get(k);
		  nameValuePairs.add(new BasicNameValuePair(k, v));  
	  }
      
      httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
      
      for(String headerName : headers.keySet()) {
    	  httpPost.setHeader(headerName, headers.get(headerName));
      }
      
	  return execute(httpPost);
  }
  
  public static HTTPServiceResponse post(String url, String body) throws UnsupportedEncodingException {
      return post(url, body, new HashMap<String, String>(), -1);
  }
  
  public static HTTPServiceResponse post(String url, String body, int timeoutMillis) throws UnsupportedEncodingException {
      return post(url, body, new HashMap<String, String>(), timeoutMillis);
  }
  
  public static HTTPServiceResponse post(String url, String body, Map<String, String> headers) throws UnsupportedEncodingException {
      return post(url, body, headers, -1);
  }
  
  public static HTTPServiceResponse postMultipart(String url, Map<String, String> params, Map<String, File> files, int timeoutMillis) throws UnsupportedEncodingException {
      HttpPost httpPost = new HttpPost(url);
      setRequestParamsTimeout(httpPost.getParams(), timeoutMillis);
      
      MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
      for(String key : params.keySet()) {
          String value = params.get(key);
          entity.addPart(key, new StringBody(value));    
      }
      
      for(String key : files.keySet()) {
          File file = files.get(key);
          //TODO: get correct mime, it could be a png
          FileBody fileBody = new FileBody(file, "image/jpeg");
          entity.addPart(key, fileBody);    
      }

      httpPost.setEntity(entity);
      return execute(httpPost);
  }
  
  public static HTTPServiceResponse post(String url, String body, Map<String, String> headers, int timeoutMillis) throws UnsupportedEncodingException {
      HttpPost httpPost = new HttpPost(url);
      setRequestParamsTimeout(httpPost.getParams(), timeoutMillis);
      
      httpPost.setEntity(new StringEntity(body, "UTF-8"));
      
      for(String headerName : headers.keySet()) {
    	  httpPost.setHeader(headerName, headers.get(headerName));
      }
      
      return execute(httpPost);
  }
  
  public static Future<HTTPServiceResponse> postAsync(final String url, final Map<String, String> params) throws InterruptedException, ExecutionException {
	  return postAsync(url, params, -1);
  }
  
  public static Future<HTTPServiceResponse> postAsync(final String url, final Map<String, String> params, final int timeoutMillis) throws InterruptedException, ExecutionException {
	  return asyncFetchService.submit(new Callable<HTTPServiceResponse>() {

		@Override
		public HTTPServiceResponse call() throws Exception {
			try {
			    return HTTPService.post(url, params, timeoutMillis);
			}
			catch(Exception t) {
			    t.printStackTrace(System.err);
			    throw t;
			}
		}
	  });
  }
  
  public static Future<HTTPServiceResponse> postAsync(final String url, final String body, final Map<String, String> headers) throws UnsupportedEncodingException {
      return postAsync(url, body, headers, -1);
  }
  
  public static Future<HTTPServiceResponse> postAsync(final String url, final String body, final Map<String, String> headers, final int timeoutMillis) throws UnsupportedEncodingException {
      return asyncFetchService.submit(new Callable<HTTPServiceResponse>() {

		@Override
		public HTTPServiceResponse call() throws Exception {
		    try {
		        return HTTPService.post(url, body, headers, timeoutMillis);
            }
            catch(Exception t) {
                t.printStackTrace(System.err);
                throw t;
            }
		}
    	  	  
	  });
  }
  
  private static HTTPServiceResponse execute(HttpRequestBase request) {
      
      try { 
          HTTPServiceResponse rv = null;
          HttpResponse response = client.execute(request, new BasicHttpContext());
          
          Map<String, String> headers = new HashMap<String, String>();
          Header[] responseHeaders = response.getAllHeaders();
          for(Header responseHeader : responseHeaders) {
        	  headers.put(responseHeader.getName(), responseHeader.getValue());
          }
          
          StatusLine statusLine = response.getStatusLine();
          
          int statusCode = statusLine.getStatusCode();
          boolean success = statusCode == HttpStatus.SC_OK;
          
          String statusMessage = response.getStatusLine().getReasonPhrase();
          
          HttpEntity entity = response.getEntity();
          if(entity != null) {
        	  Header contentTypeHeader = entity.getContentType();
        	  String contentType = contentTypeHeader != null ? contentTypeHeader.getValue() : null;
              
              InputStream inputStream = entity.getContent();
              byte[] data = IOUtils.toByteArray(inputStream);
              rv = new HTTPServiceResponse(success, statusCode, statusMessage, contentType, data, headers);
          }
          
          EntityUtils.consume(entity);
          
          return rv;
          
      }
      catch(Exception ex) {
    	  ex.printStackTrace();
          request.abort();      
      }
      
      return null;
               
  }
  
  private static void setRequestParamsTimeout(HttpParams params, int timeoutMillis) {
	  HttpConnectionParams.setSoTimeout(params, timeoutMillis < 0 ? GLOBAL_SOCKET_TIMEOUT : timeoutMillis);
	  HttpConnectionParams.setConnectionTimeout(params, timeoutMillis < 0 ? GLOBAL_CONNECTION_TIMEOUT : timeoutMillis);
  }

}