package pe.snap.services.http;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public class HTTPServiceResponse {
    private final boolean success;
    private final int statusCode;
    private final String statusMessage;
    private final String contentType;
    private final byte[] data;
    private Map<String, String> headers;
    
    public HTTPServiceResponse(boolean success, int statusCode, String statusMessage, String contentType, byte[] data, Map<String, String> headers) {
        this.success = success;
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.contentType = contentType;
        this.data = data;
        this.headers = headers;
    }
    
    public byte[] getData() {
        return this.data;
    }
    
    public String getDataAsString() throws UnsupportedEncodingException, IOException {
        //@TODO: take encoding as argument
        return new String(getData(), "UTF-8");
    }

    public boolean isSuccess() {
      return success;
    }

    public int getStatusCode() {
      return statusCode;
    }

    public String getStatusMessage() {
      return statusMessage;
    }
    
    public String getContentType() {
      return contentType;
    }
    
    public Map<String, String> getHeaders() {
    	return headers;
    }

}
