package pe.snap.services.http;

import java.io.IOException;

public class HTTPServiceException extends Exception {	
	private static final long serialVersionUID = -2167619443592026227L;
	
	private int statusCode = 0;
	private String errorMessage = null;
	
	public HTTPServiceException(int statusCode, String errorMessage) {
		this.statusCode = statusCode;
		this.errorMessage = errorMessage;
	}
	
	public HTTPServiceException(HTTPServiceResponse response) {
		this.statusCode = response.getStatusCode();
		
		String responseData = null;
		try {
			responseData = response.getDataAsString();	
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
		this.errorMessage = responseData != null && !responseData.isEmpty() ? responseData : response.getStatusMessage();
		
	}
	
	public String toString() {
		return String.format("%d: %s", this.statusCode, this.errorMessage);
	}
	
	public int getStatusCode() {
		return this.statusCode;
	}
	
	public String getErrorMessage() {
		return this.errorMessage;
	}
	
	public String getMessage() {
		return this.errorMessage;
	}
}
