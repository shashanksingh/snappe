package pe.snap.services;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;

import pe.snap.util.Util;

public class ImageResizeService {
    
    public static final int THUMB_SIZE = 125;
    public static final int STANDARD_SIZE = 960;
    
    private static final int MAX_THREADS = 5;
    
    private static ExecutorService processor;
    
    public static synchronized void start() {
        if(processor == null) {
            processor = Executors.newFixedThreadPool(MAX_THREADS);
        }
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    public static void submit(final String srcPath, final String standardSizePath, final String thumbPath) {
        processor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    Util.log("creating thumbnail for %s at %s, %s", srcPath, standardSizePath, thumbPath);
                    ImageResizeService.process(srcPath, standardSizePath, thumbPath);
                    Util.log("successfully created thumbnail for %s at %s, %s", srcPath, standardSizePath, thumbPath);
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                    ExceptionReportingService.submit(e);
                }
            }
        });
    }
    
    public static void process(String srcPath, String standardSizePath, String thumbPath) throws IOException {
        BufferedImage img = ImageIO.read(new File(srcPath));
        
        int minDimension = Math.min(img.getWidth(), img.getHeight());
        int thumbSize = Math.min(THUMB_SIZE, minDimension);
        int standardSize = Math.min(STANDARD_SIZE, minDimension);
        
        BufferedImage thumbnail = Scalr.resize(img, Method.BALANCED, thumbSize, Scalr.OP_ANTIALIAS, Scalr.OP_BRIGHTER);
        ImageIO.write(thumbnail, "JPEG", new File(thumbPath));
        
        BufferedImage standard = Scalr.resize(img, Method.BALANCED, standardSize, Scalr.OP_ANTIALIAS, Scalr.OP_BRIGHTER);
        ImageIO.write(standard, "JPEG", new File(standardSizePath));
    }
    

}
