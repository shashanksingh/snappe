package pe.snap.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONException;

import pe.snap.infra.ConnectionPool;
import pe.snap.model.User;
import pe.snap.util.Constants;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;

public class NotificationService {
    private static final String NOTIFICATION_SOUND_NAME = "mallert 006.wav";
    
    private static ApnsService service;
    
    public static synchronized void start() {
        if(service != null) {
            return;
        }
        
        service = APNS.newService().
            withCert(Constants.getAPNSCertificatePath(), Constants.APNS_CERT_PWD)
            .asNonBlocking()
            .withSandboxDestination()
            .build();
        
        service.start();
    }
    
    public static synchronized void stop() {
        if(service != null) {
            service.stop();
        }
    }
    
    public static void sendNotification(User recipient, String messageBody) throws SQLException, JSONException {
        String payload = APNS.newPayload()
            .badge(recipient.getPendingNotifications().size())
            .alertBody(messageBody)
            .sound(NOTIFICATION_SOUND_NAME)
            .shrinkBody("...")
            .build();
        
        Set<String> tokens = getUserAPNSTokens(recipient);
        for(String token : tokens) {
            service.push(token, payload);
        }
    }
    
    private static Set<String> getUserAPNSTokens(User user) throws SQLException {
        String q = "SELECT DISTINCT token FROM user_devices JOIN device_apns_tokens ON (user_devices.device_id = device_apns_tokens.device_id) WHERE user_devices.uid = ?";
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, user.getId().toString());
            
            ResultSet rs = ps.executeQuery();
            Set<String> rv = new HashSet<String>();
            while(rs.next()) {
                String productID = rs.getString("token");
                rv.add(productID);
            }
            return rv;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }

}
