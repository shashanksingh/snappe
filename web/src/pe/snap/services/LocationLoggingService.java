package pe.snap.services;

import org.apache.log4j.Logger;

import pe.snap.model.User;
import pe.snap.util.Util;

public class LocationLoggingService {
    
    private static final Logger logger = Logger.getLogger("geoLocationLogger");
    
    public static synchronized void start() {
    }
    
    public static synchronized void stop() {
    }
    
    public static void submit(final User user, final double latitude, final double longitude, final int timestamp) {
        logger.info(String.format("%d %s %f %f %d", Util.getCurrentTimestamp(), user.getId(), latitude, longitude, timestamp));
    }
}