package pe.snap.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.neo4j.graphdb.Transaction;

import pe.snap.infra.DBManager;
import pe.snap.model.User;
import pe.snap.services.http.HTTPService;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.services.http.HTTPServiceResponse;
import pe.snap.util.Constants;
import pe.snap.util.Util;

public class FacebookTokenExtensionService {
    
    private static ExecutorService processor;
    
    public static synchronized void start() {
        if(processor == null) {
            processor = Executors.newSingleThreadExecutor();
        }
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    public static void submit(final User user, final String accessToken) {
        
        processor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    Object[] accessTokenInfo = getExtendedAccessTokenInfo(accessToken);
                    
                    Transaction tx = DBManager.getTransaction();
                    try {
                        String extendedAccessToken = (String)accessTokenInfo[0];
                        int expirationDelta = (Integer)accessTokenInfo[1];
                        
                        long expirationTime = Util.getCurrentTimestamp() + expirationDelta;
                        user.setExtendedFacebookAccessToken(extendedAccessToken, expirationTime);
                        tx.success();
                    }
                    finally {
                        tx.finish();
                    }    
                }
                catch(HTTPServiceException ex) {
                    ex.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                
            }
        });
    }
    
    private static Object[] getExtendedAccessTokenInfo(String accessToken) throws HTTPServiceException, UnsupportedEncodingException, IOException {
        String url = "https://graph.facebook.com/oauth/access_token?client_id=%s&client_secret=%s&grant_type=fb_exchange_token&fb_exchange_token=%s";
        url = String.format(url, Constants.FACEBOOK_APP_ID, Constants.FACEBOOK_APP_SECRET, accessToken);
        
        HTTPServiceResponse res = HTTPService.get(url);
        if(!res.isSuccess()) {
            throw new HTTPServiceException(res);
        }
        
        String data = res.getDataAsString();
        String[] kvPairs = data.split("&");
        
        String extendedAccessToken = kvPairs[0].split("=")[1];
        int expirationDelta = Integer.parseInt(kvPairs[1].split("=")[1]);
        return new Object[]{extendedAccessToken, expirationDelta};
         
    }
}

