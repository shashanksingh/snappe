package pe.snap.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.protocol.HttpService;

import pe.snap.infra.PhotoManager;
import pe.snap.infra.PhotoManager.PhotoSize;
import pe.snap.model.Album;
import pe.snap.model.Photo;
import pe.snap.services.http.HTTPService;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.services.http.HTTPServiceResponse;
import pe.snap.util.Constants;
import pe.snap.util.Util;

public class AlbumZipService {
    private static final int PHOTO_DOWNLOAD_TIMEOUT_MILLIS = 5 * 1000;
    
    private static ExecutorService processor;
    
    public static String getFilePath(String albumID) {
        return String.format("%s%s%s%s%s.zip", Constants.getDataRootPath(), File.separator, "snappe_album_archives", File.separator, albumID);
    }
    
    private static String getZipEntryName(int index, String photoURL) {
        String extension = "png";
        if(photoURL.endsWith(".jpg")) {
            extension = "jpg";
        }
        else if(photoURL.endsWith(".jpeg")) {
            extension = "jpeg";
        }
        
        return String.format("%d.%s", index, extension);
    }
    
    public static synchronized void start() {
        if(processor == null) {
            processor = Executors.newSingleThreadExecutor();
        }
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    public static void submit(final Album album) {
        if(hasZip(album.getId().toString())) {
            return;
        }
        
        processor.submit(new Runnable(){
            @Override
            public void run() {
                try {
                    zipAlbum(album);
                } catch (IOException e) {
                    Util.log("error in zipping album %s", album.getId());
                    e.printStackTrace();
                } catch (HTTPServiceException e) {
                    Util.log("error in zipping album %s", album.getId());
                    e.printStackTrace();
                }
            }
        });
        
    }
    
    public static boolean hasZip(String albumID) {
        String filePath = getFilePath(albumID);
        return Util.fileExists(filePath);
    }
    
    private static void zipAlbum(Album album) throws IOException, HTTPServiceException {
        String filePath = getFilePath(album.getId().toString());
        //re-checking, but a race condition still exists
        if(Util.fileExists(filePath)) {
            return;
        }
        
        FileOutputStream fo = new FileOutputStream(filePath);
        ZipOutputStream zo = new ZipOutputStream(fo);
        boolean noEntries = true;
        
        int index = 0;
        
        try {
            for(Photo photo : album.getSortedAvailableNonPrivatePhotos()) {
                
                Util.log("going ahead with zipping photo: %s", photo.getId());
                String url = photo.getURL();
                String hash = photo.getHash();
                
                String photoURI = url;
                boolean localFile = false;
                if(url.contains("snap.pe") && hash != null && PhotoManager.hasPhoto(hash, PhotoSize.FULL)) {
                    photoURI =  PhotoManager.getFilePath(hash, PhotoSize.FULL);
                    localFile = true;
                }
                
                String zipEntryName = getZipEntryName(index, photoURI);
                zo.putNextEntry(new ZipEntry(zipEntryName));
                if(localFile) {
                    FileUtils.copyFile(new File(photoURI), zo);    
                }
                else {
                    HTTPServiceResponse response = HTTPService.get(photoURI, PHOTO_DOWNLOAD_TIMEOUT_MILLIS);
                    if(!response.isSuccess()) {
                        Util.log("AlbumZipService: error in getting photo from url: %s, %d, %s", photoURI, response.getStatusCode(), response.getStatusMessage());
                    }
                    else {
                        IOUtils.write(response.getData(), zo);    
                    }
                }
                
                zo.closeEntry();
                
                index++;
                
                noEntries = false;
                
            }
        }
        finally {
            if(!noEntries) {
                if(zo != null) {
                    zo.close();
                }    
            }
            else {
                if(fo != null) {
                    fo.close();
                }
                //remove the empty zip file
                Util.deleteFileIfExists(filePath);
            }
            
        }
        
        
        
    }

}
