package pe.snap.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONException;
import org.json.JSONObject;

import pe.snap.infra.ConnectionPool;
import pe.snap.model.Album;
import pe.snap.model.User;
import pe.snap.util.Constants;
import pe.snap.util.Util;

public class EmailShareService {
    private static final long ACCESS_TOKEN_VALIDITY_SECONDS = 30 * 86400;//30 days
    
    private static ExecutorService processor;
    
    public static synchronized void start() {
        if(processor == null) {
            processor = Executors.newSingleThreadExecutor();
        }
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    private static void submit(String recipientEmail, String recipientName, String recipientUserID, Album sharedAlbum, User sharer, boolean allowUpload) throws SQLException {
        AlbumZipService.submit(sharedAlbum);
        
        String accessToken = generateNewAccessToken(sharedAlbum, sharer.getId(), recipientUserID, recipientEmail,  allowUpload);
        EmailService.sendAlbumShareByEmailNotification(recipientEmail, recipientName, sharedAlbum, sharer, accessToken);
    }
    
    public static void submit(String recipientEmail, String recipientName, Album sharedAlbum, User sharer, boolean allowUpload) throws SQLException {
        submit(recipientEmail, recipientName, null, sharedAlbum, sharer, allowUpload);
    }
    
    public static void submit(User sharer, User recipient, Album sharedAlbum) throws SQLException {
        String email = String.format("%s@facebook.com", recipient.getId());
        //allow upload is useless here, it is dynamically checked for known users
        submit(email, recipient.getName(), sharedAlbum, sharer, false);
    }
    
    public static String generateNewAccessToken(Album album, User recipient, User sharer) throws SQLException {
        return generateNewAccessToken(album, sharer.getId(), recipient.getId(), null, false);
    }
    
    public static String generateNewAccessToken(Album album, String sharerID, String recipientUserID, String recipientEmail, boolean allowUpload) throws SQLException {
        String q = "INSERT INTO album_access_tokens (access_token, album_id, sharer_uid, recipient_uid, recipient_email, allow_upload) VALUES (?, ?, ?, ?, ?, ?)";
        
        String accessToken = Util.getUUID();
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, accessToken);
            ps.setString(2, album.getId().toString());
            ps.setString(3, sharerID);
            ps.setString(4, recipientUserID);
            ps.setString(5, recipientEmail);
            ps.setBoolean(6, allowUpload);
            
            ps.executeUpdate();
            return accessToken;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static boolean isValidAccessToken(String albumID, String accessToken) throws SQLException {
        String q = "SELECT * FROM album_access_tokens WHERE album_id = ? AND access_token = ? AND TIMESTAMPDIFF(SECOND, ts, CURRENT_TIMESTAMP) < ? LIMIT 1";
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, albumID);
            ps.setString(2, accessToken);
            ps.setLong(3, ACCESS_TOKEN_VALIDITY_SECONDS);
            
            ResultSet rs = ps.executeQuery();
            return rs.next();
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static boolean isUploadAllowed(String accessToken) throws SQLException, InterruptedException, ExecutionException {
        String q = "SELECT album_id, recipient_uid, allow_upload FROM album_access_tokens WHERE access_token = ?";
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, accessToken);
            
            ResultSet rs = ps.executeQuery();
            //allow upload is set iff it was an external share with allowed uplaod
            //at the time of sharing, for internal sharing it is false and we
            //check dynamically
            if(!rs.next()) {
                return false;
            }
            
            boolean allowUpload = rs.getBoolean("allow_upload");
            if(allowUpload) {
                return true;
            }
            
            String albumID = rs.getString("album_id");
            String recipientUserID = rs.getString("recipient_uid");
            
            Album album = Album.get(albumID);
            if(album == null) {
                Util.log("album not found for album_id %s", albumID);
                return false;
            }
            
            if(recipientUserID == null) {
                return false;    
            }
            
            User recipient = User.get(recipientUserID);
            if(recipient == null) {
                Util.log("recipient not found for recipient_uid %s", recipientUserID);
                return false;
            }
            
            return recipient.canUploadToAlbum(album);
            
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static String getAlbumZipDownloadLink(String albumID, String accessToken) {
        return String.format("%s/download?aid=%s&access_token=%s", Constants.getServerURL(), albumID, accessToken);
    }
    
    public static String getSharedAlbumLink(String albumID, String accessToken) {
        return String.format("%s/download.html?aid=%s&access_token=%s", 
                Constants.getServerURL(), 
                Util.encodeURIComponent(albumID), 
                Util.encodeURIComponent(accessToken));
    }
    
    public static JSONObject getRecipient(String accessToken) throws SQLException, InterruptedException, ExecutionException, JSONException {
        String q = "SELECT recipient_uid, recipient_email, sharer_uid FROM album_access_tokens WHERE access_token = ?";
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, accessToken);
            
            ResultSet rs = ps.executeQuery();
            if(!rs.next()) {
                return null;
            }
            
            String recipientUID = rs.getString("recipient_uid");
            String recipientEmail = rs.getString("recipient_email");
            
            JSONObject rv = new JSONObject();
            if(recipientUID != null) {
                rv.put("uid", recipientUID);
            }
            if(recipientEmail != null) {
                rv.put("email", recipientEmail);
            }
            
            return rv;
            
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static String createAccessTokenForVerifiedFacebookRequest(JSONObject request) throws SQLException, JSONException {
        String accessToken = Util.getUUID();
        
        JSONObject data = request.getJSONObject("data");
        String albumID = data.getString("album_id");
        String sharerUID = request.optString("sender_uid");
        String recipientUID = request.getString("recipient_uid");
        String fbRequestID = request.getString("request_id");
        
        String q = "INSERT INTO album_access_tokens (access_token, album_id, sharer_uid, recipient_uid, fb_request_id, allow_upload) VALUES (?, ?, ?, ?, ?, ?)";
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, accessToken);
            ps.setString(2, albumID);
            ps.setString(3, sharerUID);
            ps.setString(4, recipientUID);
            ps.setString(5, fbRequestID);
            ps.setBoolean(6, false);
            
            ps.executeUpdate();
            
            return accessToken;
            
            
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static String getOrCreateAccessTokenForVerifiedFacebookRequest(JSONObject request) throws SQLException, JSONException {
        String requestID = request.getString("request_id");
        
        String q = "SELECT access_token FROM album_access_tokens WHERE fb_request_id = ?";
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, requestID);
            
            ResultSet rs = ps.executeQuery();
            if(!rs.next()) {
                return createAccessTokenForVerifiedFacebookRequest(request);
            }
            
            String accessToken = rs.getString("access_token");
            return accessToken;
            
            
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }

}
