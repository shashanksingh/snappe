package pe.snap.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.JSONException;
import org.json.JSONObject;

import pe.snap.util.Util;

public class ExceptionReportingService {
    
    private static ScheduledExecutorService processor;
    private static LinkedBlockingQueue<ExceptionReport> queue = new LinkedBlockingQueue<ExceptionReport>();
    
    private static String machine = "Snappe";
    private static String emailTemplate;
    
    private static Map<String, Integer> signatureToEternalCount = new HashMap<String, Integer>();
    private static Map<String, List<Long>> signatureToRecentTimestamp = new HashMap<String, List<Long>>();
    
    
    private ExceptionReportingService() {
        
    }
    
    public static synchronized void start() {
        initTemplate();
        initProcessor();
    }
    
    public static synchronized void stop() {
        if(processor != null) {
            processor.shutdownNow();
        }
    }
    
    public static void submit(final Throwable t) {
        Util.log("new exception queued:\n%s", ExceptionUtils.getStackTrace(t));
        queue.add(new ExceptionReport(t));
    }
    
    private static void initTemplate() {
        InputStream is = EmailService.class.getResourceAsStream("emailtemplates/exception_tracker_email.html");
        try {
            emailTemplate = IOUtils.toString(is, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private static void initProcessor() {
        if(processor == null) {
            processor = Executors.newSingleThreadScheduledExecutor();
            processor.scheduleWithFixedDelay(new Runnable() {
                
                @Override
                public void run() {
                    try {
                        List<ExceptionReport> q = new ArrayList<ExceptionReport>();
                        queue.drainTo(q);
                        Util.log("got %d exceptions to log", q.size());
                        processExceptions(q);    
                    }
                    catch(Throwable t) {
                        String message = String.format("<pre>%s</pre>", ExceptionUtils.getFullStackTrace(t));
                        AlertService.sendErrorAlert("Error in ExceptionReportingService", message);
                    }
                    
                }
                
            }, 10, 5, TimeUnit.MINUTES);
            
        }
    }
    
    private static void processExceptions(List<ExceptionReport> reports) {
        List<String> reportTexts = new ArrayList<String>();
        for(ExceptionReport report : reports) {
            String reportText = processException(report);
            if(reportText != null) {
                reportTexts.add(reportText);
            }
        }
        
        if(!reportTexts.isEmpty()) {
            String subject = String.format("%d New Exceptions on %s", reportTexts.size(), machine);
            String body = StringUtils.join(reportTexts, "<hr>");
            AlertService.sendErrorAlert(subject, "", body);
        }
    }
    
    private static String processException(ExceptionReport report) {
        String signature = getExceptionSignature(report.t);
        
        Integer eternalCount = signatureToEternalCount.get(signature);
        if(eternalCount == null) {
            eternalCount = 0;
        }
        signatureToEternalCount.put(signature, eternalCount + 1);
        
        List<Long> recentTimestamps = signatureToRecentTimestamp.get(signature);
        if(recentTimestamps == null) {
            recentTimestamps = new LinkedList<Long>();
        }
        else {
            long aDayAgo = report.timestamp - 86400 * 1000;
            while(!recentTimestamps.isEmpty() && recentTimestamps.get(0) < aDayAgo) {
                recentTimestamps.remove(0);
            }
        }
        recentTimestamps.add(System.currentTimeMillis());
        
        int occurrencesInLast24Hours = recentTimestamps.size();
        int occurrencesSinceBigBang = signatureToEternalCount.get(signature);
        
        if(Util.isPowerOfTwo(occurrencesInLast24Hours) || Util.isPowerOfTwo(occurrencesSinceBigBang)) {
            String message = report.t.getLocalizedMessage();
            String stackTrace = ExceptionUtils.getFullStackTrace(report.t);
            String reportText = generateReportText(message, stackTrace, occurrencesInLast24Hours, occurrencesSinceBigBang);
            return reportText;
        }
        
        return null;
    }
    
    private static String getExceptionSignature(Throwable t) {
        StringBuffer signatureParts = new StringBuffer();
        for(StackTraceElement ste : t.getStackTrace()) {
            String fileName = ste.getFileName();
            int lineNumber = ste.getLineNumber();
            
            signatureParts.append(fileName);
            signatureParts.append(lineNumber);
        }
        
        return DigestUtils.md5Hex(signatureParts.toString());
    }
    
    private static String generateReportText(String message, String stackTrace, int occurrencesInLast24Hours, int occurrencesSinceBigBang) {
        Map<String, String> templateParams = new HashMap<String, String>();
        templateParams.put("machine", machine);
        templateParams.put("message", message);
        templateParams.put("total_occurences", Integer.toString(occurrencesSinceBigBang));
        templateParams.put("today_occurences", Integer.toString(occurrencesSinceBigBang));
        templateParams.put("error_message", message);
        templateParams.put("stack_trace", stackTrace);
        
        String emailHTML = Util.printf(emailTemplate, templateParams);
        return emailHTML;
    }
}

class ExceptionReport {
    public Throwable t;
    public long timestamp;
    
    public ExceptionReport(Throwable t) {
        this.t = t;
        this.timestamp = System.currentTimeMillis();
    }
}