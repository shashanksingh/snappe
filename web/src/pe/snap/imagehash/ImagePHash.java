package pe.snap.imagehash;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;

import pe.snap.services.http.HTTPService;
import pe.snap.util.Util;

import com.googlecode.javacv.cpp.opencv_core;
import com.googlecode.javacv.cpp.opencv_core.CvArr;
import com.googlecode.javacv.cpp.opencv_core.CvMat;
import com.googlecode.javacv.cpp.opencv_core.CvPoint;
import com.googlecode.javacv.cpp.opencv_core.CvRect;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_imgproc;


public class ImagePHash {

	private static final int SIZE = 32;
	private static final int SMALLER_SIZE = 8;
	private static final int SIMILARITY_DISTANCE_THRESHOLD = 20;
	
	public static int getHashDistance(long hash1, long hash2) {
		int count = 0;
		long xor = hash1 ^ hash2;
		while(xor > 0) {
			xor &= xor - 1;
			count++;
		}
		return count;
	}
	
	public static int getHashDistance(String hash1Hex, String hash2Hex) {
	    String h1 = new BigInteger(hash1Hex, 16).toString(2);
	    String h2 = new BigInteger(hash2Hex, 16).toString(2);
	    
	    
	    if(h1.length() > h2.length()) {
	        h2 = StringUtils.leftPad(h2, h1.length(), '0');
	    }
	    else if(h2.length() > h1.length()) {
	        h1 = StringUtils.leftPad(h1, h2.length(), '0');
	    }
	    
	    int distance = 0;
	    for(int i=h1.length() - 1; i >= 0; i--) {
	        if(h1.charAt(i) != h2.charAt(i)) {
	            distance++;
	        }
	    }
	    
	    return distance;
	}
	
	public static boolean areSimilar(String hash1, String hash2) {
	    int distance = getHashDistance(hash1, hash2);
	    boolean rv = distance <= SIMILARITY_DISTANCE_THRESHOLD;
	    if(rv) {
	        Util.log("hashes %s & %s found similar with distance %d", hash1, hash2, distance);
	    }
	    return rv;
	}
	
	public static String getHash(byte[] data) throws IOException {
	    return getHash(new ByteArrayInputStream(data));
	}
	
	public static String getHash(InputStream is) throws IOException {
	    long startTime = System.currentTimeMillis();
	    
        BufferedImage buffIm = ImageIO.read(is);
        
        IplImage iplImage = IplImage.createFrom(buffIm);
        if(iplImage.nChannels() == 4) {
            throw new IllegalArgumentException("wrong image type; 4 channels");
        }
        
        opencv_imgproc.cvCvtColor(iplImage, iplImage, opencv_imgproc.CV_BGR2YCrCb);
        
        CvMat mat = iplImage.asCvMat();
        
        CvMat Y = opencv_core.cvCreateMat(mat.rows(), mat.cols(), opencv_core.CV_8UC1);
        CvMat crcb = opencv_core.cvCreateMat(mat.rows(), mat.cols(), opencv_core.CV_8UC2);
        opencv_core.cvMixChannels(new CvArr[]{mat}, 1, new CvArr[]{Y, crcb}, 2, new int[]{0,0, 1,1, 2,2}, 3);
        
        CvMat floatMat = opencv_core.cvCreateMat(Y.rows(), Y.cols(), opencv_core.CV_32FC1);
        opencv_core.cvConvert(Y, floatMat);
        Y = floatMat;
        
        CvMat meanFilter = opencv_core.cvCreateMat(7, 7, opencv_core.CV_32F);
        for(int i=0; i<7; i++) {
            for(int j=0; j<7; j++) {
                meanFilter.put(i, j, 1.0);
            }
        }
        
        opencv_imgproc.filter2D(Y, Y, -1, meanFilter, new CvPoint(-1, -1), 0, opencv_imgproc.BORDER_DEFAULT);
        
        CvMat smallMat = opencv_core.cvCreateMat(SIZE, SIZE, opencv_core.CV_32FC1);
        opencv_imgproc.cvResize(Y, smallMat);
        Y = smallMat;
        
	    opencv_core.cvDCT(Y, Y, opencv_core.CV_DXT_FORWARD);
	    
	    CvMat cropped = opencv_core.cvCreateMat(SMALLER_SIZE, SMALLER_SIZE, opencv_core.CV_32FC1);
	    CvRect croppedArea = opencv_core.cvRect(1, 1, SMALLER_SIZE, SMALLER_SIZE);
        cropped = opencv_core.cvGetSubRect(Y, cropped, croppedArea);
	    
        Y = cropped;
        
	    float[] pixelArray = new float[(int)Y.total()];
	    int k =0;
	    for(int y=0; y < Y.rows(); y++) {
	        for(int x=0; x<Y.cols(); x++) {
	            pixelArray[k++] = (float)Y.get(y, x);
	        }
	    }
	    
	    float median = Util.getMedian(pixelArray);
	    
        BigInteger h = BigInteger.ZERO;
        
        for (int i=0;i< SMALLER_SIZE * SMALLER_SIZE; i++){
            float current = pixelArray[i];
            if (current > median)
                h = h.setBit(i);
        }
        
        long timeTaken = System.currentTimeMillis() - startTime;
        Util.log("phash time: %d", timeTaken);
        
        return h.toString(16);
        
	}
	
	private static void printArray(float[] array) {
	    Float[] objArr = new Float[array.length];
         for(int i=0; i<array.length; i++) {
            objArr[i] = array[i];
        }
         
        System.out.println(StringUtils.join(objArr, ",\t"));
    }
	
	public static void main(String[] args) throws Exception {
	    Util.log("similar: %b", areSimilar("5aa592914d5a5ab5", "5aa5d2914d5a5aa5"));
	    if(true) return;
	    
	    HTTPService.init();
	    String h2 = getHash(HTTPService.get("http://snap.pe/p_t.jpg").getData());
	    System.out.println("h2 " + h2);
	    HTTPService.destroy();
	}

}
