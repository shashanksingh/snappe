package pe.snap.util;

public class TimingLogger {
    private final long startTime;
    
    public TimingLogger() {
        this.startTime = System.currentTimeMillis();       
    }
    
    public void l() {
        long delta = System.currentTimeMillis() - this.startTime;
        
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        if(stack.length >= 3) {
            System.out.println(String.format("%d - %s", delta, stack[2].toString()));
        }
    }
}
