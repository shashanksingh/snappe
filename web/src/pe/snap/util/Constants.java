package pe.snap.util;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

public class Constants {
    public static final String FACEBOOK_APP_ID = "413004668729952";
    public static final String FACEBOOK_APP_SECRET = "15a3757e718cf16f077c2960688e4fa8";
    
    public static final String FACEBOOK_PAGE_ID = "147077755451211";
    
    public static final String SNAPPE_TYPE_KEY = "snappe_object_type";
    public static final String SNAPPE_CREATION_TIMESTAMP_KEY = "snappe_creation_time";
    
    public static final String SESSION_USERID_ATTR = "userid";
    public static final String SESSION_FB_TOKEN_ATTR = "fb_access_token";
    public static final String SESSION_FB_LAST_UPDATE_ATTEMPT_TIME_ATTR = "fb_sync_attempt_time";
    public static final String SESSION_APNS_TOKEN_ATTR = "apns_token";
    public static final String SESSION_DEVICE_ID_ATTR = "device_id";
    public static final String SESSION_UPLOADED_PHOTO_HASHES = "uploaded_photo_hashes";
    
    public static final String FB_ACCESS_TOKEN_HEADER_NAME = "X-FacebookAccessToken";
    public static final String LAST_SYNC_TIME_HEADER_NAME = "X-LastSyncTime";
    public static final String DEVICE_ID_HEADER_NAME = "X-DeviceID";
    public static final String PHOTO_DOWNLOAD_REQUEST_HEADER_NAME = "X-Download";
    public static final String APNS_TOKEN_HEADER_NAME = "X-APNSToken";
    public static final String FORCE_FB_REFRESH_HEADER = "X-ForceFBRefresh";
    
    public static final String REQUEST_USER_ATTR = "user";
	public static final String REQUEST_LOGGER_ATTR = "logger";
	public static final String REQUEST_START_TIME = "request_start_time";
	public static final String FB_GRAPH_UPDATE_CALL_HANDLE = "fb_graph_update_call_handle";
	
	public static final String APNS_CERT_PWD = "my.snappe.apns.cert.pwd";
	
	private static Boolean isProduction = null;
	
	
	public static boolean isProduction() {
	    if(isProduction != null) {
	        return isProduction;
	    }
	    
	    try {
	        isProduction = InetAddress.getLocalHost().getHostName().startsWith("domU");
	        return isProduction;
	    }
	    catch(UnknownHostException uhe) {
	        Util.logAdminWarning("error in detecting host name, assuming production", uhe);
	    }
	    //default
	    return true;
	}
	
	public static String getServerURL() {
	    return isProduction() ? "http://snap.pe" : "http://localhost:8080";
	}
	
	public static String getServerDomain() throws MalformedURLException {
	    URL url = new URL(getServerURL());
	    return url.getHost();
	}
	
	public static String getDBPath() {
	    return isProduction() ? "/vol/data/snappedb" : "/Users/shashank/temp/snappedb";
	}
	
	public static String getDataRootPath() {
	    return isProduction() ? "/vol/data" : "/Users/shashank/snappe_data";
	}
	
	public static String getAPNSCertificatePath() {
	    return "/vol/conf/snappe_apns_cert.p12";
	}
}
