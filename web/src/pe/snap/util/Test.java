package pe.snap.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONException;
import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.PropertyContainer;

import pe.snap.infra.DBManager;
import pe.snap.model.TrackedPropertyContainer;
import pe.snap.model.User;
import pe.snap.services.http.HTTPService;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.services.http.HTTPServiceResponse;

public class Test {
    private static void registerShutdownHook( final GraphDatabaseService graphDb)
    {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running example before it's completed)
        Runtime.getRuntime().addShutdownHook( new Thread()
        {
            @Override
            public void run()
            {
                graphDb.shutdown();
                HTTPService.destroy();
            }
        } );
    }
    
    
	private static void start() {
		HTTPService.init();
		DBManager.startDB();
		registerShutdownHook(DBManager.getDB());
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException, IOException, JSONException, HTTPServiceException {
		start();
		
		long startTime = System.currentTimeMillis();
		
		String facebookId = "654175641";
		String facebookAccessToken = "AAACEdEose0cBAJZCa0lWNDSmZAC8QMm9mQaROvZBsqj7pIaO7Muw6vxnJaxFxRfc4fdOVgtvZCKw7Ni3K2m59yroQRPedMysXLhlNcOXDgZDZD";
		
		//FacebookManager.updateUserSubGraph(facebookId, facebookAccessToken);
		
		//User me = User.get(facebookId);
		//System.out.println(me.getChangeSet(System.currentTimeMillis() + 1000).toString());
		ExecutionEngine engine = new ExecutionEngine(DBManager.getDB());
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("start_node_id", facebookId);
		
		String allFriendsQuery = "START n=node:node_auto_index(id={start_node_id}) MATCH n-[friend_rel:FRIEND]->friend RETURN friend";
		String allAlbumsQuery = "START n=node:node_auto_index(id={start_node_id}) MATCH n-[friend_rel:FRIEND]->friend-[owns_album_rel:USER_OWNS_ALBUM]->album RETURN DISTINCT album";
		String allPhotosQuery = "START n=node:node_auto_index(id={start_node_id}) MATCH n-[friend_rel:FRIEND]->friend-[owns_album_rel:USER_OWNS_ALBUM]->album<-[photo_in_album_rel:PHOTO_IN_ALBUM]-photo RETURN DISTINCT photo";
		String allAtOnceQuery = "START n=node:node_auto_index(id={start_node_id}) MATCH n-[friend_rel:FRIEND]->friend-[owns_album_rel:USER_OWNS_ALBUM]->album<-[photo_in_album_rel:PHOTO_IN_ALBUM]-photo RETURN distinct friend_rel, friend, owns_album_rel, album, photo_in_album_rel, photo";
		
		String[] queries = new String[]{allFriendsQuery, allAlbumsQuery, allPhotosQuery, allAtOnceQuery};
		for(String q : queries) {
			ExecutionResult result = engine.execute(q, params);
			int count = 0;
			for ( Map<String, Object> row : result )
			{
			    for ( Entry<String, Object> column : row.entrySet() )
			    {
			        count++;
			        new TrackedPropertyContainer((PropertyContainer)column.getValue()).getChangeSet(0);
			    }
			    
			}
			System.out.println(count + " done in " + (System.currentTimeMillis() - startTime));	
		}
		
		System.out.println("done in " + (System.currentTimeMillis() - startTime));
	}
}
