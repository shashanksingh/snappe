package pe.snap.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pe.snap.exceptions.APIException;
import pe.snap.exceptions.APIException.APIExceptionType;
import pe.snap.infra.ConnectionPool;
import pe.snap.model.Notification;
import pe.snap.model.User;
import pe.snap.services.ExceptionReportingService;
import pe.snap.services.FacebookGraphService;
import pe.snap.services.http.HTTPServiceException;
import sun.rmi.runtime.NewThreadAction;


public class RequestUtil {
	private static final int FB_GRAPH_UPDATE_MIN_INTERVAL = 60 * 60;
	
	private static final Logger logger = Logger.getLogger(RequestUtil.class);
	
	public static User getUser(HttpServletRequest request) throws APIException, UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, InterruptedException, ExecutionException, SQLException {
	    User user = null;
        HttpSession session = request.getSession();
        
        String userId = (String)session.getAttribute(Constants.SESSION_USERID_ATTR);
        if(userId == null) {
            verifyRequest(request);
        }
        
        userId = (String)session.getAttribute(Constants.SESSION_USERID_ATTR);
        String accessToken = (String)session.getAttribute(Constants.SESSION_FB_TOKEN_ATTR);
        
        user = User.getOrCreateFromFacebook(userId, accessToken);
        
        updateDeviceID(user, request);
        updateAPNSToken(request);
        
        request.setAttribute(Constants.REQUEST_USER_ATTR, user);
        
        return user;
	}
	
	
	public static String getRequestDeviceID(HttpServletRequest request) {
        return request.getHeader(Constants.DEVICE_ID_HEADER_NAME);
	}
	
	private static String getSessionDeviceID(HttpServletRequest request) {
        return (String)request.getSession().getAttribute(Constants.SESSION_DEVICE_ID_ATTR);
    }
	
	public static String getRequestAPNSToken(HttpServletRequest request) {
        return request.getHeader(Constants.APNS_TOKEN_HEADER_NAME);
    }
    
    private static String getSessionAPNSToken(HttpServletRequest request) {
        return (String)request.getSession().getAttribute(Constants.SESSION_APNS_TOKEN_ATTR);
    }
	
	private static int getLastSyncHeader(HttpServletRequest request) {
	    String lastSyncTimeParameter = request.getHeader(Constants.LAST_SYNC_TIME_HEADER_NAME);
        int lastSyncTime = lastSyncTimeParameter == null ? 0 : Integer.parseInt(lastSyncTimeParameter);
        return lastSyncTime;
	}
	
	public static Future<Long> updateUserFacebookSubGraph(HttpServletRequest request) throws UnsupportedEncodingException, APIException, JSONException, IOException, HTTPServiceException, InterruptedException, ExecutionException, SQLException, TimeoutException {
	    return updateUserFacebookSubGraph(request, false);
	}
	public static Future<Long> updateUserFacebookSubGraph(HttpServletRequest request, boolean force) throws UnsupportedEncodingException, APIException, JSONException, IOException, HTTPServiceException, InterruptedException, ExecutionException, SQLException, TimeoutException {
	    
	    HttpSession session = request.getSession();
	    User user = getUser(request);
	    
	    Util.log("here1");
	    if(!force && request.getHeader(Constants.FORCE_FB_REFRESH_HEADER) != null) {Util.log("here2");
	        force = true;
	    }
	    
	    Integer lastFBSyncAttemptTime = getIntPropertyFromSession(session, Constants.SESSION_FB_LAST_UPDATE_ATTEMPT_TIME_ATTR, 0);
	    
	    int secondsSinceLastFBSyncAttempt = Util.secondsSince(lastFBSyncAttemptTime); 
        if(force || secondsSinceLastFBSyncAttempt > FB_GRAPH_UPDATE_MIN_INTERVAL) {
            Util.log("here3");
            int startTime = (int)Util.getCurrentTimestamp();
            session.setAttribute(Constants.SESSION_FB_LAST_UPDATE_ATTEMPT_TIME_ATTR, startTime);
            
            int lastSyncTime = getLastSyncHeader(request);
            try {
                //we are not using futures for the time being
                FacebookGraphUpdater.update(user, user.getLastSuccessfulFacebookSyncTime());
                
                user.setLastSuccessfulFacebookSyncTime(startTime);
            }
            catch(Throwable t) {Util.log("here4");
                ExceptionReportingService.submit(t);
                
                //if the nature of update is background, ignore failures
                if(!force && lastSyncTime > 0) {
                    t.printStackTrace(System.err);
                }
                else {
                    throw t;
                }
            }
            
            Util.log("here5");
            return null;
        }
        else {
            Util.log("FB sync attempt was at %d, last successful attempt was %d seconds ago, skipping this time", lastFBSyncAttemptTime, Util.secondsSince(user.getLastSuccessfulFacebookSyncTime()));
        }
	    
	    return null;
	}
	
	public static JSONObject getUserGraphChangeData(HttpServletRequest request) throws UnsupportedEncodingException, APIException, JSONException, IOException, HTTPServiceException, InterruptedException, ExecutionException, SQLException {
	    User user = getUser(request);
	    int lastSyncTime = getLastSyncHeader(request);
	    
	    JSONArray graphChanges = user.getGraphChanges(lastSyncTime);
	    
	    logger.debug(graphChanges.toString(3));
	    
	    JSONObject rv = new JSONObject();
	    rv.put("changes", graphChanges);
	    return rv;
	}
	
	public static JSONArray getUserPendingNotifications(HttpServletRequest request) throws JSONException, UnsupportedEncodingException, APIException, IOException, HTTPServiceException, InterruptedException, ExecutionException, SQLException {
		User user = getUser(request);
		List<Notification> notifications = user.getPendingNotifications();
		Util.log("getUserPendingNotifications: %d", notifications.size());
		JSONArray rv = new JSONArray();
		for(Notification notification : notifications) {
			rv.put(notification.toJSON());
		}
		return rv;
	}
	
	public static JSONObject getUserCredits(HttpServletRequest request) throws UnsupportedEncodingException, APIException, JSONException, IOException, HTTPServiceException, InterruptedException, ExecutionException, SQLException {
	    User user = getUser(request);
	    return CreditsManager.getUserCurrentCredits(user).toJSON();
	}
	
	public static Logger getLogger(HttpServletRequest request) {
        String requestUri = request.getRequestURI();
        String jspName = requestUri.substring(requestUri.lastIndexOf('/'));
        return Logger.getLogger(jspName);
    }
	
	private static void verifyRequest(HttpServletRequest request) throws APIException, UnsupportedEncodingException, JSONException, IOException, HTTPServiceException {
		HttpSession session = request.getSession();
		String facebookAccessToken = request.getHeader(Constants.FB_ACCESS_TOKEN_HEADER_NAME);
		if(facebookAccessToken == null) {
			throw new APIException(APIExceptionType.ACCESS_TOKEN_REQUIRED, "access_token_required");
		}
		else {
			String userId = FacebookManager.verifyAccessToken(facebookAccessToken);
			if(userId == null) {
				throw new APIException(APIExceptionType.ACCESS_TOKEN_AUTH_FAILED, "invalid access token");
			}
			else {
				session.setAttribute(Constants.SESSION_USERID_ATTR, userId);
				session.setAttribute(Constants.SESSION_FB_TOKEN_ATTR, facebookAccessToken);
			}
		}
	}
	
	public static String getParameter(HttpServletRequest request, String parameterName) throws APIException {
	    return getParameter(request, parameterName, null);	
	}
	
	public static String getParameter(HttpServletRequest request, String parameterName, String[] validValues) throws APIException {
	    String value = request.getParameter(parameterName);
	    if(value == null) {
	    	throw new APIException(APIExceptionType.MISSING_PARAMETER, String.format("missing parameter '%s'", parameterName));
	    }
	    if(validValues == null) return value;
	    
	    for(String validValue : validValues) {
	    	if(value.equals(validValue)) return value;
	    }
	    throw new APIException(APIExceptionType.INVALID_PARAMETER_VALUE, String.format("invalid value '%s' for parameter '%s', should be one of %s", value, parameterName, StringUtils.join(validValues, ", ")));
	}
	
	public static String getOptionalParameter(HttpServletRequest request, String parameterName) throws APIException {
		return request.getParameter(parameterName);
	}
	
	public static void unsetAccessTokenForCurrentUserIfAvailable(HttpServletRequest request) {
	    Util.log("attempting clearing of expired accesstoken for current user");
	    User user = (User)request.getAttribute(Constants.REQUEST_USER_ATTR);
	    if(user != null) {
	        Util.log("clearing expired accesstoken for current user: %s", user.getId());
	        user.unsetAccessToken();
	        Util.log("cleared expired accesstoken for current user: %s", user.getId());
	    }
	    
	    request.getSession().invalidate();
	}
	
	public static boolean isDownloadRequest(HttpServletRequest request) {
	    boolean rv = request.getHeader(Constants.PHOTO_DOWNLOAD_REQUEST_HEADER_NAME) != null;
	    if(rv) {
	        return true;
	    }
	    
	    rv = request.getParameter("full") != null;
	    return rv;
	}
	
	private static String updateDeviceID(User user, HttpServletRequest request) throws SQLException {
	    String newDeviceID = getRequestDeviceID(request);
	    String sessionDeviceID = getSessionDeviceID(request);
	    
	    if(!newDeviceID.equals(sessionDeviceID)) {
	        updateDeviceIDInDatabase(user, newDeviceID);
	        request.getSession().setAttribute(Constants.SESSION_DEVICE_ID_ATTR, newDeviceID);
	    }
	    
	    return newDeviceID;
	}
	
	private static String updateAPNSToken(HttpServletRequest request) throws SQLException {
	    String newAPNSToken = getRequestAPNSToken(request);
	    if(newAPNSToken == null) {
	        return null;
	    }
	    
	    String sessionAPNSToken = getSessionAPNSToken(request);
	    if(!newAPNSToken.equals(sessionAPNSToken)) {
	        updateAPNSTokenInDatabase(getRequestDeviceID(request), newAPNSToken);
	        request.getSession().setAttribute(Constants.SESSION_APNS_TOKEN_ATTR, newAPNSToken);
	    }
	    
	    return newAPNSToken;
	   
	}
	
	private static void updateDeviceIDInDatabase(User user, String deviceID) throws SQLException {
        String q = "INSERT INTO user_devices (uid, device_id, last_access) VALUES (?, ?, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE last_access = CURRENT_TIMESTAMP";
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, user.getId().toString());
            ps.setString(2, deviceID);
            
            ps.executeUpdate();
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
	}
	
	private static void updateAPNSTokenInDatabase(String deviceID, String token) throws SQLException {
        String q = "INSERT INTO device_apns_tokens (device_id, token) VALUES (?, ?) ON DUPLICATE KEY UPDATE token = VALUES(token)";
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, deviceID);
            ps.setString(2, token);
            
            ps.executeUpdate();
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
	}
	
	private static int getIntPropertyFromSession(HttpSession session, String propertyName, int defaultValue) {
	    Object value = session.getAttribute(propertyName);
	    if(value == null) {
	        return defaultValue;
	    }
	    return (Integer)value;
	}
	
}
