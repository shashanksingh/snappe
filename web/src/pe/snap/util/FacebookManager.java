package pe.snap.util;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pe.snap.exceptions.APIException;
import pe.snap.exceptions.APIException.APIExceptionType;
import pe.snap.model.Album;
import pe.snap.model.Album.PhotoSource;
import pe.snap.model.Location;
import pe.snap.model.Photo;
import pe.snap.model.Tag;
import pe.snap.model.User;
import pe.snap.services.EmailShareService;
import pe.snap.services.FacebookGraphService;
import pe.snap.services.http.HTTPService;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.services.http.HTTPServiceResponse;


public class FacebookManager {
	
	private static final int GEO_LOCATION_FIND_WINDOW = 100;
	private static final int PHOTO_DOWNLOAD_CONNECT_TIMEOUT = 2000;
	private static final int PHOTO_DOWNLOAD_READ_TIMEOUT = 2000;
	private static final int PHOTO_UPLOAD_TIMETOUT = 5000;
	
	private static String appAccessToken; 
	
	private static final Logger logger = Logger.getLogger(FacebookManager.class);
	
	public static String getAppAccessToken() throws HTTPServiceException, UnsupportedEncodingException, IOException {
	    if(appAccessToken == null) {
	        String url = String.format("https://graph.facebook.com/oauth/access_token?client_id=%s&client_secret=%s&grant_type=client_credentials", Constants.FACEBOOK_APP_ID, Constants.FACEBOOK_APP_SECRET);
	        HTTPServiceResponse res = HTTPService.get(url);
	        if(!res.isSuccess()) {
	            throw new HTTPServiceException(res);
	        }
	        
	        String data = res.getDataAsString();
	        appAccessToken = data.split("access_token=")[1];
	    }
	    return appAccessToken;
	}

	public static JSONObject getUser(String facebookId, String accessToken) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, APIException {
		String query = String.format("SELECT %s FROM user WHERE uid = %s", StringUtils.join(User.FQL_FIELDS, ","), facebookId);
		JSONArray fqlRows = FacebookGraphService.executeQuery(accessToken, query);
		if(fqlRows.length() == 0) {
			return null;
		}
		return fqlRows.getJSONObject(0);
	}
	
	public static String verifyAccessToken(String accessToken) throws JSONException, UnsupportedEncodingException, IOException, HTTPServiceException, APIException {
		String query = String.format("SELECT uid FROM user WHERE uid = me()");
		JSONArray fqlRows = FacebookGraphService.executeQuery(accessToken, query);
		if(fqlRows.length() == 0) {
			return null;
		}
		return fqlRows.getJSONObject(0).getString("uid");
	}
	
	private static String getLocationFQLQuery(double latitude, double longitude) {
	    return String.format("SELECT page_id,name,description,latitude,longitude FROM place WHERE distance(latitude,longitude,\"%f\",\"%f\")<%d ORDER BY distance(latitude,longitude,\"%f\",\"%f\") ASC LIMIT 1", latitude, longitude, GEO_LOCATION_FIND_WINDOW, latitude, longitude);
	}
	
	//get the closest city
	public static JSONObject getFacebookLocationForCoordinates(double latitude, double longitude, String accessToken) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, APIException {
	    String query = getLocationFQLQuery(latitude, longitude);
	    
	    JSONArray fqlRows = FacebookGraphService.executeQuery(accessToken, query);
	    translateFBLocationObjectsToLocal(fqlRows);
	    
	    if(fqlRows.length() == 0) {
            return null;
        }
	    return fqlRows.getJSONObject(0);
	}
	
	public static void translateFBLocationObjectsToLocal(JSONArray fqlRows) throws JSONException {
        for(int i=0; i<fqlRows.length(); i++) {
            if(fqlRows.isNull(i)) {
                continue;
            }
            
            JSONObject fqlRow = fqlRows.getJSONObject(i);
            String pageId = fqlRow.getString("page_id");
            fqlRow.remove("page_id");
            fqlRow.put("id", pageId);
        }
    }
	
	public static JSONArray getFacebookLocationForCoordinates(List<double[]> coordinatesList, String accessToken) throws UnsupportedEncodingException, IOException, JSONException, HTTPServiceException, APIException {
	    //SO says row limit on FQL query is about 5k but we are doing a multiquery
	    //which is a get request, we don't want the query string to be too long
	    final int BATCH_SIZE = 50;
	    
	    JSONArray rv = new JSONArray();
	    
	    Map<String, JSONArray> fqlRows = new HashMap<String, JSONArray>();
	    
	    int count = -1;
	    for(List<double[]> chunk : Util.split(coordinatesList, BATCH_SIZE)) {
	        Map<String, String> queries = new HashMap<String, String>();
	        for(double[] coordinates : chunk) {
	            count++;
	            
	            String query = getLocationFQLQuery(coordinates[0], coordinates[1]);
	            queries.put(Integer.toString(count), query);
	        }
	        
	        Map<String, JSONArray> thisChunkRows = FacebookGraphService.executeMultiQuery(accessToken, queries);
	        fqlRows.putAll(thisChunkRows);
	    }
	    
	    for(int i=0; i<coordinatesList.size(); i++) {
	        String key = Integer.toString(i);
	        JSONArray rows = fqlRows.get(key);
	        if(rows == null || rows.length() == 0) {
	            rv.put(JSONObject.NULL);
	        }
	        else {
	            rv.put(rows.getJSONObject(0));
	        }
	    }
	    
	    translateFBLocationObjectsToLocal(rv);
	    return rv;
	}
	
	//TODO: batch this as suggested here:
	//http://stackoverflow.com/questions/5498302/multiple-photos-upload-on-facebook
	public static void uploadPhotoToAlbum(Photo photo, Album album, Object/*User|String*/ sharer) throws IOException, HTTPServiceException, JSONException, APIException {
	    if(photo.isPrivate() || !photo.isImageAvailable()) {
	        return;
	    }
	    
	    URL photoURL = new URL(photo.getURL());
	    Album srcAlbum = photo.getAlbum();
	    
	    String uploadURL = String.format("https://graph.facebook.com/%s/photos", album.getFBObjectId());
	    
	    File tempFile = File.createTempFile("snappe", "tmp");
	    try {
	        FileUtils.copyURLToFile(photoURL, tempFile, PHOTO_DOWNLOAD_CONNECT_TIMEOUT, PHOTO_DOWNLOAD_READ_TIMEOUT);
	        
	        Map<String, String> graphAPIParams = new HashMap<String, String>();
	        graphAPIParams.put("access_token", album.getOwner().getFacebookAccessToken());
	        
	        Location loc = photo.getLocation();
	        if(loc == null && srcAlbum != null) {
	            loc = srcAlbum.getLocation();
	        }
	        if(loc != null) {
	            graphAPIParams.put("place", loc.getId().toString());    
	        }
	        
	        String message = null;
	        if(sharer != null && sharer instanceof User) {
	            User sharerUser = (User)sharer;
	            message = String.format("added by %s via %s", sharerUser.getName(), Constants.getServerURL());
	        }
	        else if(sharer instanceof String) {
	            String sharerNameOrEmail = (String)sharer;
	            if(sharerNameOrEmail.indexOf('@') > 0) {
	                String sharerEmail = Util.redactEmail(sharerNameOrEmail);
	                message = String.format("added by %s via %s", sharerEmail, Constants.getServerURL());
	            }
	            else {
	                String sharerName = sharerNameOrEmail;
	                message = String.format("added by %s via %s", sharerName, Constants.getServerURL());
	            }
	        }
	        
	        if(message != null) {
	            graphAPIParams.put("message", message);
	        }
	        
	        Map<String, File> files = new HashMap<String, File>();
	        files.put("image", tempFile);
	        
	        HTTPServiceResponse response = HTTPService.postMultipart(uploadURL, graphAPIParams, files, PHOTO_UPLOAD_TIMETOUT);
	        if(!response.isSuccess()) {
	            //http://developers.facebook.com/blog/post/2011/05/13/how-to--handle-expired-access-tokens/
	            if(response.getStatusCode() == 400) {
	                String errorData = response.getDataAsString();
	                JSONObject errorJSON = new JSONObject(errorData);
	                if(errorJSON.get("type").equals("OAuthException")) {
	                    throw new APIException(APIExceptionType.FRIEND_ACCESS_TOKEN_EXPIRED, "Friend's authorization for Facebook Upload has expired");
	                }
	            }
	            throw new HTTPServiceException(response);
	        }
	        else {
	            Util.log("photo upload response: %s", response.getDataAsString());
	        }
	    }
	    finally {
	        if(!tempFile.delete()) {
	            tempFile.deleteOnExit();
	        }
	    }
	}
	
	public static void uploadToAlbum(Album source, Album dest, Object/*User|String*/ sharer) throws IOException, HTTPServiceException, JSONException, APIException {
	    //no checks to be done here (permissions etc.) they are already done 
	    //upstream
	    
	    dest.ensureHashes();
	    List<Photo> srcPhotos = source.getSortedAvailableNonPrivatePhotos();
	    List<Photo> newSrcPhotos = dest.getNonSimilarPhotos(srcPhotos);
	    for(Photo photo : newSrcPhotos) {
	        uploadPhotoToAlbum(photo, dest, sharer);
	    }
	}
	
	public static void deleteFacebookPhotoIfDeletedOnFacebook(String accessToken, Photo photo) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, APIException {
	    String query = "SELECT object_id FROM photo WHERE object_id = '%s' OR pid = '%s'";
	    query = String.format(query, photo.getId().toString(), photo.getId().toString());
	    
	    JSONArray rows = FacebookGraphService.executeQuery(accessToken, query);
	    if(rows.length() == 0) {
	        Util.log("deleted photo %s because it was deleted on facebook", photo.getId());
	        
	        //extract album before deleting
	        Album album = photo.getAlbum();
	        photo.delete();
	        
	        deleteAlbumIfDeletedOnFacebook(accessToken, album);
	    }
	}
	
	private static void deleteAlbumIfDeletedOnFacebook(String accessToken, Album album) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, APIException {
	    String query = "SELECT object_id FROM album WHERE object_id = '%s' OR aid = '%s'";
	    
	    Util.log("deleteAlbumIfDeletedOnFacebook album: %s, albumID: %s", album, album == null ? null : album.getId());
	    
        query = String.format(query, album.getId().toString(), album.getId().toString());
        
        JSONArray rows = FacebookGraphService.executeQuery(accessToken, query);
        if(rows.length() == 0) {
            Util.log("deleted album %s because it was deleted on facebook", album.getId());
            album.delete();
            Analytics.serverLog("album_delete_on_fb_delete", "ablum", album.getId());
        }
	}
	
	public static void sendAlbumShareNotification(User sender, User recipient, Album sharedAlbum) throws SQLException, InterruptedException, ExecutionException, MalformedURLException {
	    String albumDownloadLink = EmailShareService.getSharedAlbumLink(sharedAlbum.getId().toString(), EmailShareService.generateNewAccessToken(sharedAlbum, recipient, sender));
	    String href = new URL(albumDownloadLink).getFile();
	    
	    String ref = "album_share";
	    
	    String albumName = StringUtils.abbreviate(sharedAlbum.getName(), 50);
	    String template = String.format("@[%s] shared album '%s' with you.", sender.getId().toString(), albumName);
	    
	    
	    Map<String, String> parameters = new HashMap<String, String>();
	    parameters.put("href", href);
	    parameters.put("ref", ref);
	    parameters.put("template", template);
	    
	    parameters.put("access_token", sender.getFacebookAccessToken());
	    
	    String url = String.format("https://graph.facebook.com/%s/notifications", recipient.getId().toString());
	    
	    try {
	        Util.log("sent FB notification: %s", new JSONObject(parameters).toString(3));    
	    }
	    catch(Exception e) {
	        e.printStackTrace();
	    }
	    
	    HTTPService.postAsync(url, parameters);
	    
	    Analytics.serverLog("album_share_notification_fb", "from", sender.getId(), "to", recipient.getId(), "album", sharedAlbum.getId());
	    
	}
	
	public static JSONArray getRequests(List<String> requestIDs) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, APIException {
	    String query = "SELECT request_id, sender_uid, recipient_uid, data FROM apprequest WHERE request_id IN (%s) AND app_id = '%s'";
	    query = String.format(query, FacebookGraphService.getINFragment(requestIDs), Constants.FACEBOOK_APP_ID);
	    JSONArray rv = FacebookGraphService.executeQuery(getAppAccessToken(), query);
	    
	    //data parameter is string, needs to parsed as JSONObject
	    for(int i=0; i<rv.length(); i++) {
	        JSONObject row = rv.getJSONObject(i);
	        row.put("data", new JSONObject(row.optString("data", "{}")));
	    }
	    return rv;
	}
	
	public static String getAccessTokenForCode(String code) throws UnsupportedEncodingException, JSONException, IOException {
	    String url = "https://graph.facebook.com/oauth/access_token?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s";
	    url = String.format(url, Util.encodeURIComponent(Constants.FACEBOOK_APP_ID), 
	            Util.encodeURIComponent("https://snappeapp.appspot.com/facebook/"),
	            Util.encodeURIComponent(Constants.FACEBOOK_APP_SECRET), 
	            Util.encodeURIComponent(code));
	    
	    System.out.println(url);
	    
	    String data = HTTPService.get(url).getDataAsString();
	    JSONObject response = new JSONObject(data);
	    
	    System.out.println(data);
	    
	    if(response.optString("error") != null) {
	        Util.log("error in getAccessTokenForCode: %s", data);
	        return null;
	    }
	    
	    return response.getString("access_token");
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException, IOException, JSONException, HTTPServiceException, APIException {
	    HTTPService.init();
	    //Util.log("%s", verifyAccessToken("AAACEdEose0cBALm4eBP0xrsK3tBdh7Ds7fabPbC2zkQybKnaUzd0fVT9qhPtCSjJktcwSTnO6KuoXxbp4ibhTMj5ZAa2bM9OvEfveuQZDZD"));
	    
	    //System.out.println(String.format("res: %s", getRequests(Arrays.asList(new String[]{"552630691446697"})).toString(3)));
	    System.out.println(getAccessTokenForCode("AQD14U_HpjQ59JzlCa4zPqqKLleEmyrb3QSY4uGDt9Us0OhkrmAkPL0n2i7S_mCKbQVDjjjtfmWRUYL7hI-Dcu16MZoyv-Y771kiI-Ij30dHC86NM4o2hNF8J5lj9h5I_HSsUSMK5M2OTvdNy4G8Xu0vGkiRVSB_4P6D9EzMGjXcN2nz2phF-jwYjJr7dCmhERgw1kWBkH4JQdUO24IF0GUifDkZJdZZLzMCsryHNjRDuDjblgZfopmv7BNeQ1__FJ_GQtjXBlfLtLmpFV3xMDAt3lLyN4T-NlkO93uPIqtYymYVrmNYH-8peioapXMGraF-aw0vFn3dGXbOMb2JR1hH"));
	    HTTPService.destroy();
	}

}
