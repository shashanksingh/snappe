package pe.snap.util;

import java.io.File;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.PropertyContainer;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;

import pe.snap.model.Album;
import pe.snap.model.Location;
import pe.snap.model.Photo;
import pe.snap.model.SnappeNode;
import pe.snap.model.SnappeRelationship;
import pe.snap.model.Tag;
import pe.snap.model.TrackedPropertyContainer;
import pe.snap.model.User;
import pe.snap.services.http.HTTPService;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.services.http.HTTPServiceResponse;

public class Util {
    private static DateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
	private static final Logger logger = Logger.getLogger(Util.class);
	
	public static long getCurrentTimestamp() {
		return System.currentTimeMillis()/1000;
	}
	
	public static int secondsSince(int referenceTime) {
	    return (int)getCurrentTimestamp() - referenceTime;
	}
	
	public static String nameInterval(int startTime, int endTime) {
		logger.debug("startTime: " + startTime);
		logger.debug("endTime: " + endTime);
		
		long startMillis = ((long)startTime) * 1000;
		long endMillis = ((long)endTime) * 1000;
		
		DateTime startDateTime = new DateTime(startMillis);
		DateTime endDateTime = new DateTime(endMillis);
		
		Period p = new Period(startDateTime, endDateTime);
		if(p.getDays() == 0) {
			DateTimeFormatter fmt = DateTimeFormat.forPattern("dd MMM yyyy");
			return fmt.print(startDateTime.getMillis());
		}
		
		DateTimeFormatter fmt = DateTimeFormat.forPattern("MMM yyyy");
		return fmt.print(startDateTime.getMillis());
	}
	
	public static String printf(String template, Map<String, String> values) {
        Pattern pattern = Pattern.compile("\\{(.+?)\\}");
        Matcher matcher = pattern.matcher(template);

        StringBuilder builder = new StringBuilder();
        int i = 0;
        while (matcher.find()) {
            String replacement = values.get(matcher.group(1));
            builder.append(template.substring(i, matcher.start()));
            if (replacement == null)
                builder.append(matcher.group(0));
            else
                builder.append(replacement);
            i = matcher.end();
        }
        builder.append(template.substring(i, template.length()));
        return builder.toString();
    }

	
	public static String encodeURIComponent(String component) {
		if(component == null) return null;
		String result = null;

		try {
			result = URLEncoder.encode(component, "UTF-8")
					.replaceAll("\\%28", "(").replaceAll("\\%29", ")")
					.replaceAll("\\+", "%20").replaceAll("\\%27", "'")
					.replaceAll("\\%21", "!").replaceAll("\\%7E", "~");
		} catch (UnsupportedEncodingException e) {
			result = component;
		}

		return result;
	}
	
	public static boolean fileExists(String path) {
		File f = new File(path);
		return f.exists();
	}
	
	public static void log(String format, Object...params) {
	    String m = String.format("%s: %s", sdf.format(new Date()), String.format(format, params));
	    //System.out.println(m);
	    logger.debug(m);
    }
	
	public static Relationship getLastToLastRelationship(Path path) {
		int length = path.length();
		if(length < 2) {
			return null;
		}
		int i=-1;
		for(Relationship r : path.relationships()) {
			i++;
			if(i == length - 2) {
				return r;
			}
		}
		return null;
	}
	
	public static Node getLastToLastNode(Path path) {
        int length = path.length();
        if(length < 1) {
            return null;
        }
        int i=-1;
        for(Node n : path.nodes()) {
            i++;
            if(i == length - 1) {
                return n;
            }
        }
        return null;
    }
	
	public static String getNodeType(Node n) {
	    return (String)n.getProperty(Constants.SNAPPE_TYPE_KEY);
	}
	
	public static String getNodeId(Node n) {
	    //Util.log("node id type: %s", n.getProperty(TrackedPropertyContainer.ID_KEY).getClass());
	    return (String)n.getProperty(TrackedPropertyContainer.ID_KEY);
	}
	
	public static String getRelationshipId(Relationship r) {
        return (String)r.getProperty(TrackedPropertyContainer.ID_KEY);
    }
	
	public static Object parseObjectID(String strID) {
        try {
            return Long.parseLong(strID);
        }
        catch(NumberFormatException nfe) {
            return strID;
        }
    }
	
	public static <T extends Object> List<List<T>> split(List<T> list, int targetSize) {
        List<List<T>> lists = new ArrayList<List<T>>();
        for (int i = 0; i < list.size(); i += targetSize) {
            lists.add(list.subList(i, Math.min(i + targetSize, list.size())));
        }
        return lists;
    }
	
	public static void deleteFileIfExists(String path) {
	    File f = new File(path);
	    if(f.exists()) {
	        f.delete();
	    }
	}
	
	public static void logAdminWarning(String message) {
	    logAdminWarning(message, null);    
	}
	
	public static void logAdminWarning(String message, Throwable t) {
	    if(t == null) {
	        t = new Exception();
	    }
	    String strackTrace = ExceptionUtils.getFullStackTrace(t);
	    
	    Util.log("%s:\n%s", message, strackTrace);
	}
	
	public static float getMedian(float[] values) {
	    values = values.clone();
	    
	    Arrays.sort(values);
	    
	    int midIndex = (int)Math.floor(values.length * 0.5);
	    if(values.length % 2 != 0) {
	        return values[midIndex];
	    }
	    return values[(int)((midIndex + midIndex + 1) * 0.5)];
	}
	
	public static float getAverage(float[] values) {
	    float sum = 0.0f;
	    for(float f : values) {
	        sum += f;
	    }
	    return sum/values.length;
	}
	
	public static String getUUID() {
	    return UUID.randomUUID().toString();
	}
	
	public static JSONObject getChangeSet(PropertyContainer pc, long ts) throws JSONException {
	    String type = (String)pc.getProperty(Constants.SNAPPE_TYPE_KEY);
	    if(type.equals("user")) {
            return User.get((Node)pc).getChangeSet(ts);
        }
        else if(type.equals("album")) {
            return Album.get((Node)pc).getChangeSet(ts);
        }
        else if(type.equals("photo")) {
            return Photo.get((Node)pc).getChangeSet(ts);
        }
        else if(type.equals("location")) {
            return Location.get((Node)pc).getChangeSet(ts);
        }
        else if(type.equals("tag")) {
            return Tag.get((Node)pc).getChangeSet(ts);
        }
        else if(type.equals("relationship")) {
            return SnappeRelationship.get((Relationship)pc).getChangeSet(ts);
        }
        else {
            throw new AssertionError("unexpected node type: " + type);
        }
	}
	
	
	//cool: http://stackoverflow.com/questions/5082314/power-of-2-formula-help
	public static boolean isPowerOfTwo(int n) {
	    return 2*n == (n ^ (n-1)) + 1;
	}
	
	public static String ensureURLParameters(String baseURL, String... params) throws MalformedURLException {
	    if(params.length%2 != 0) {
	        throw new IllegalArgumentException("params should have even number of elements");
	    }
	    
	    Map<String, String> newParams = new HashMap<String, String>();
	    
	    for(int i=0; i<params.length; i+=2) {
	        String k = params[i];
	        String v = params[i + 1];
	        newParams.put(k, v);   
	    }
	    
	    URL url = new URL(baseURL);
	    
	    String query = url.getQuery();
	    
	    if(query != null) {
	        for(String kv : query.split("&")) {
	            String[] parts = kv.split("=");
	            String k = parts[0];
	            String v = parts.length > 1 ? parts[1] : "";
	            if(!newParams.containsKey(k)) {
	                newParams.put(k, v);
	            }
	        }    
	    }
	    
	    List<String> kvPairs = new ArrayList<String>();
	    for(String k : newParams.keySet()) {
	        String v = newParams.get(k);
	        kvPairs.add(String.format("%s=%s", Util.encodeURIComponent(k), Util.encodeURIComponent(v)));
	    }
	    
	    query = StringUtils.join(kvPairs, "&");
	    
	    String rv = String.format("%s://%s%s?%s", url.getProtocol(), url.getAuthority(), url.getPath(), query);
	    
	    String ref = url.getRef();
	    if(ref != null && !ref.isEmpty()) {
	        rv += "#" + ref;
	    }
	    
	    return rv;
	    
	}
	
	public static String redactEmail(String email) {
	    String[] parts = email.split("@");
	    if(parts.length != 2) {
	        return null;
	    }
	    
	    if(parts[0].length() <= 2) {
	        return String.format("hidden@%s", parts[1]);
	    }
	    return String.format("%s..@%s", parts[0].substring(0, 2), parts[1]);
	}
	
	public static void setCookie(HttpServletResponse response, String name, String value, String domain) {
        Cookie cookie = new Cookie (name, value);
        cookie.setMaxAge(Integer.MAX_VALUE);
        cookie.setDomain(domain);
        cookie.setPath("/");
        
        response.addCookie(cookie);
    }
	
	public static String getCookie(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals(name)) {
                    return c.getValue();
                }
            }
        }
        return null;
    }
	
	public static void main(String[] args) throws MalformedURLException {
	    System.out.println(new URL("http://snap.pe").getHost());
		System.out.println(ensureURLParameters("http://snap.pe/gf.html?a=b&", "ref", "fb", "uid", "234"));
	}

}
