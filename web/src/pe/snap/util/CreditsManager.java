package pe.snap.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import pe.snap.infra.ConnectionPool;
import pe.snap.model.User;
import pe.snap.services.AlertService;
import pe.snap.services.http.HTTPService;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.services.http.HTTPServiceResponse;

public class CreditsManager {
    private static final int AUTO_RESET_CREDITS = 30;
    protected static final int AUTO_RESET_DAYS = 30;
    private static final int INFINITY_CREDITS = 10000;
    
    private static final String PURCHASE_VERIFICATION_PRODUCTION = "https://buy.itunes.apple.com/verifyReceipt";
    private static final String PURCHASE_VERIFICATION_SANDBOX = "https://sandbox.itunes.apple.com/verifyReceipt";
    
    private static final String[] PRODUCTS = {
        "pe.snap.products.non_consumables.remove_ads",
        "pe.snap.products.consumables.300_credits",
        "pe.snap.products.consumables.3000_credits",
        "pe.snap.products.non_consumables.unlimited_credits_2"
    };
    
    
    private static final Logger logger = Logger.getLogger(CreditsManager.class);
    
    public static class CreditInfo {
        
        public int credits;
        public int lastAutoResetTime;
        
        public CreditInfo(int credits, int lastAutoResetTime) {
            this.credits = credits;
            this.lastAutoResetTime = lastAutoResetTime;
        }
        
        public int getDaysSinceLastAutoReset() {
            if(lastAutoResetTime < 0) {
                return -1;
            }
            
            int currentTime = (int)(System.currentTimeMillis()/1000);
            //no need for precise time arithmetic, this will do fine
            return (currentTime - lastAutoResetTime)/(86400);
        }
        
        public JSONObject toJSON() throws JSONException {
            JSONObject rv = new JSONObject();
            rv.put("credits", this.credits);
            rv.put("next_reset", lastAutoResetTime + (86400 * AUTO_RESET_DAYS));
            return rv;
        }
    }

    public static CreditInfo getUserCurrentCredits(User user) throws SQLException {
        CreditInfo creditInfo = getUserCurrentCreditsFromDatabase(user);
        if(creditInfo == null) {
            autoResetCredits(user, AUTO_RESET_CREDITS);
            return new CreditInfo(AUTO_RESET_CREDITS, (int)(System.currentTimeMillis()/1000));
        }
        
        if(creditInfo.credits >= AUTO_RESET_CREDITS) {
            return creditInfo;    
        }
        
        int daysSinceLastAutoReset = creditInfo.getDaysSinceLastAutoReset();
        if(daysSinceLastAutoReset >= AUTO_RESET_DAYS) {
            autoResetCredits(user, AUTO_RESET_CREDITS);
            return new CreditInfo(AUTO_RESET_CREDITS, (int)(System.currentTimeMillis()/1000));
        }
        
        return creditInfo;
    }
    
    public static void decrementUserCredits(User user) throws SQLException {
        String q = "UPDATE user_credits SET credits = IF(credits >= ?, credits, credits - 1) WHERE uid = ?";
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setInt(1, INFINITY_CREDITS);
            ps.setString(2, user.getId().toString());
            
            ps.executeUpdate();
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static CreditInfo getUserCurrentCreditsFromDatabase(User user) throws SQLException {
        String q = "SELECT credits, last_auto_reset FROM user_credits WHERE uid = ?";
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, user.getId().toString());
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                int credits = rs.getInt("credits");
                
                int lastAutoResetTime = -1;
                Timestamp lastAutoResetTimestamp = rs.getTimestamp("last_auto_reset");
                if(lastAutoResetTimestamp != null) {
                    lastAutoResetTime = (int)(lastAutoResetTimestamp.getTime()/1000);
                }
                
                return new CreditInfo(credits, lastAutoResetTime);
            }
            return null;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static Set<String> getUserPurchasedProducts(User user) throws SQLException {
        String q = "SELECT product_id FROM user_products WHERE uid = ?";
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, user.getId().toString());
            
            ResultSet rs = ps.executeQuery();
            Set<String> rv = new HashSet<String>();
            while(rs.next()) {
                String productID = rs.getString("product_id");
                rv.add(productID);
            }
            return rv;
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static void autoResetCredits(User user, int autoCredits) throws SQLException {
        String q = "INSERT INTO user_credits (uid, credits, last_auto_reset) VALUES (?, ?, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE credits = VALUES(credits), last_auto_reset = VALUES(last_auto_reset)";
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, user.getId().toString());
            ps.setInt(2, autoCredits);
            
            ps.executeUpdate();
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static void updateOnProductPurchase(User user, String purchasedProductID) throws SQLException {
        updateCreditsOnProductPurchase(user, purchasedProductID);
        updatePurchasedProductsOnPurchase(user, purchasedProductID);
    }
    
    private static void updateCreditsOnProductPurchase(User user, String purchasedProductID) throws SQLException {
        int credits = getCreditsForProduct(purchasedProductID);
        
        String q = "INSERT INTO user_credits (uid, credits) VALUES (?, credits + ?) ON DUPLICATE KEY UPDATE credits = credits + VALUES(credits)";
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, user.getId().toString());
            ps.setInt(2, credits);
            
            ps.executeUpdate();
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    private static void updatePurchasedProductsOnPurchase(User user, String purchasedProductID) throws SQLException {
        String q = "INSERT IGNORE INTO user_products (uid, product_id) VALUES (?, ?)";
        
        Connection con = ConnectionPool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, user.getId().toString());
            ps.setString(2, purchasedProductID);
            
            ps.executeUpdate();
        }
        finally {
            if(con != null) {
                con.close();
            }
        }
    }
    
    public static boolean verifyReceipt(String receipt, User user) throws JSONException, HTTPServiceException, IOException, SQLException {
        JSONObject postJSON = new JSONObject();
        postJSON.put("receipt-data", receipt);
        
        String verificationEndPoint = user.isTester() ? PURCHASE_VERIFICATION_SANDBOX : PURCHASE_VERIFICATION_PRODUCTION;
        HTTPServiceResponse response = HTTPService.post(verificationEndPoint, postJSON.toString());
        if(!response.isSuccess()) {
            throw new HTTPServiceException(response);
        }
        
        JSONObject responseJSON = new JSONObject(response.getDataAsString());
        if(responseJSON.getInt("status") != 0) {
            logger.error(String.format("Purchase receipt verification failed: %s", receipt));
            AlertService.sendErrorAlert("Purchase receipt verification failed", 
                    String.format("<p>Receipt: %s</p>", receipt));
            return false;
        }
        
        JSONObject receiptJSON = responseJSON.getJSONObject("receipt");
        updateOnVerifiedPurchase(user, receiptJSON);
        
        return true;
    }
    
    private static void updateOnVerifiedPurchase(User user, JSONObject purchaseReceipt) throws JSONException, SQLException {
        String productID = purchaseReceipt.getString("product_id");
        
        logger.info(String.format("verified transaction: %s", purchaseReceipt.toString()));
        Util.log(String.format("verified transaction: %s", purchaseReceipt.toString()));

        int purchasedIndex = ArrayUtils.indexOf(PRODUCTS, productID);
        if(purchasedIndex == -1) {
            logger.error(String.format("Unknown Purchased Product: %s(%s)", productID, purchaseReceipt.toString(3)));
            AlertService.sendErrorAlert("Unknown Purchased Product", 
                    String.format("<p>Purchase Receipt: %s</p>", purchaseReceipt.toString(3)));
            return;
        }
        
        Set<String> alreadyPurchased = getUserPurchasedProducts(user);
        if(productID.indexOf(".non_consumables.") >= 0 && alreadyPurchased.contains(productID)) {
            logger.error(String.format("Possible Repurchase of Product:\nCurrent purchase: %s\nAlready purchased:%s\nPurchase Receipt:%s", productID, StringUtils.join(alreadyPurchased, ','), purchaseReceipt.toString(3)));
            AlertService.sendErrorAlert("Possible Repurchase of Product", 
                    String.format("<p>Current Purchase: %s</p><p>Already Purchased: %s</p><p>Purchase Receipt: %s</p>", productID, StringUtils.join(alreadyPurchased, ','), purchaseReceipt.toString(3)));
            return;
        }
        
        updateOnProductPurchase(user, productID);
        
    }
    
    private static int getCreditsForProduct(String productID) {
        if(productID.equals("pe.snap.products.consumables.300_credits")) {
            return 300;
        }
        if(productID.equals("pe.snap.products.consumables.3000_credits")) {
            return 3000;
        }
        if(productID.equals("pe.snap.products.non_consumables.unlimited_credits_2")) {
            return INFINITY_CREDITS;
        }
        return -1;
    }
}