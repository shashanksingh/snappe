package pe.snap.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import pe.snap.model.User;
import pe.snap.services.ExceptionReportingService;
import pe.snap.services.http.HTTPService;

public class Analytics {
    
    private static final String API_TOKEN = "8cb2e9c9cb5b552e8771c0ff659929e4";
    private static final String API_URL = "http://api.mixpanel.com/track/";
    
    private static final String SERVER_DUMMY_DEVICE_ID = "snappe";
    
    public static void log(String deviceID, String event, Map<String, String> props) {
        try {
            Map<String, String> params = getParams(deviceID, event, props);
            HTTPService.getAsync(API_URL, params);
            
        } catch (Exception e) {
            e.printStackTrace(System.err);
            ExceptionReportingService.submit(e);
        }
    }
    
    public static void log(String deviceID, String event, Object... params) {
        Object[] paramsArray = params;
        if(paramsArray.length%2!=0) {
            throw new IllegalArgumentException("the number of vararg parameters should be even");
        }
        
        Map<String, String> props = new HashMap<String, String>();
        for(int i=0; i<params.length; i+=2) {
            String key = params[i].toString();
            String val = params[i + 1].toString();
            
            props.put(key, val);
        }
        
        log(deviceID, event, props);
    }
    
    public static void log(HttpServletRequest request, String event, Object... params) {
        String deviceID = RequestUtil.getRequestDeviceID(request);
        log(deviceID, event, params);
    }
    
    public static void serverLog(String event, Object... params) {
        log(SERVER_DUMMY_DEVICE_ID, event, params);
    }
    
    private static Map<String, String> getParams(String deviceID, String event, Map<String, String> props) throws JSONException {
        JSONObject jsonParams = new JSONObject();
        jsonParams.put("event", event);
        
        JSONObject jsonProperties = new JSONObject();
        jsonProperties.put("token", API_TOKEN);
        jsonProperties.put("distinct_id", deviceID);
        for (Entry<String, String> p : props.entrySet()) {
            jsonProperties.put(p.getKey(), p.getValue());
        }
        
        jsonProperties.put("time", Util.getCurrentTimestamp());
        
        jsonParams.put("properties", jsonProperties);
        
        String data = jsonParams.toString();
        data = new String(Base64.encodeBase64(data.getBytes()));
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("data", data);
        return params;
    }

}
