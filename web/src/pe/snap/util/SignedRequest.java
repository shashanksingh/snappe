//https://code.google.com/p/play-fbgraph/source/browse/src/play/modules/facebook/SignedRequest.java
package pe.snap.util;

import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

public class SignedRequest {

    private String encodedSig;
    private String encodedData;
    private String sig;
    private JSONObject data;

    public SignedRequest(String value) throws JSONException {
        String[] splits = value.split(Pattern.quote("."));
        encodedSig = splits[0];
        sig = base64UrlDecode(encodedSig);
        encodedData = splits[1];
        data = new JSONObject(base64UrlDecode(encodedData));
    }

    public boolean verify(String secret) throws JSONException {
        boolean sigVerified = false;
        String algorithm = data.getString("algorithm");
        if ("HMAC-SHA256".equals(algorithm)) {
            try {
                SecretKeySpec key = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
                Mac hmac = Mac.getInstance("HmacSHA256");
                hmac.init(key);
                String expectedSig = new String(hmac.doFinal(encodedData.getBytes("UTF-8")));
                if (expectedSig.equals(sig)) {
                    sigVerified = true;
                }
            } catch (Exception ex) {
            }
        }
        return sigVerified;
    }

    public String getSignature() {
        return sig;
    }

    public JSONObject getData() {
        return data;
    }

    private String base64UrlDecode(String encoded) {
        Base64 base64 = new Base64(true); // URL-safe mode
        return new String(base64.decode(encoded));
    }
    
    public static void main(String[] args) throws JSONException {
        SignedRequest signedRequest = new SignedRequest("AQALd5TdK1mwgQOY9OdTRIsaLAaRSJe4l1wHepfN0-CoOJYOUcW2atRhRuHURlxxMHieufO0fsH1mJfGMR00ZW19urcWGQk2fwvWLp4nFRKmzuFfnCW-X53_QfyYDjrDjfrgV_M5Akh6c_33spRjRrX1bv4kKNdAF8D1Kuvnh5jYKVjqG9UWCfnNNHSJW4nc-x7J4bFh4VcHooFfGfHZ3yEJmc20-zqpO0FblPKD2UP5-V-XeXXOndGLgb5n-sGzaak14_yPAuU0-HH2M4fH1BQhCGtbYmYBtRc-Cl4QIMz7ZFa7ZKQZOmFH6Og5j706dfk");
        System.out.println(signedRequest.data);
        System.out.println(signedRequest.encodedData);
        System.out.println(signedRequest.sig);
        System.out.println(signedRequest.encodedSig);
    }
}