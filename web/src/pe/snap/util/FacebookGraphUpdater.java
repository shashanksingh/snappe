package pe.snap.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pe.snap.exceptions.APIException;
import pe.snap.model.Album;
import pe.snap.model.Location;
import pe.snap.model.Photo;
import pe.snap.model.Tag;
import pe.snap.model.User;
import pe.snap.model.Album.PhotoSource;
import pe.snap.services.FacebookGraphService;
import pe.snap.services.UserFacebookLocationUpdaterService;
import pe.snap.services.http.HTTPServiceException;

public class FacebookGraphUpdater {
    private static final Set<String> IGNORE_ALBUM_WITH_NAME = new HashSet<String>(Arrays.asList(new String[]{"Cover Photos", "Profile Pictures", "Wall Photos"}));
    private static final int MAX_USER_ALBUMS_PER_USER = 10;
    private static final int MAX_FRIEND_ALBUMS_PER_USER = 20;
    private static final int MAX_FRIEND_ALBUMS_PER_NEW_FRIEND = 5;
    private static final int MAX_FRIEND_ALBUMS_FOR_NEW_FRIENDS = 10;
    private static final int MAX_LOCATION_AGE_SECONDS = 6 * 30 * 24 * 60 * 60;//6 months
    
    private static final int FQL_TIMEOUT = 30;
    
    private static final Logger logger = Logger.getLogger(FacebookGraphUpdater.class);
    
    public static User update(User user, int modifiedSince) throws UnsupportedEncodingException, IOException, JSONException, HTTPServiceException, InterruptedException, ExecutionException, APIException, TimeoutException {
        
        long startTime = System.currentTimeMillis();
        boolean success = false;
        
        try {
            Map<String, JSONArray> changes = getGraphChanges(user, modifiedSince);
            
            Util.log("FB graph changes fetch (FB/HTTP part only) for uid: %s, since: %d finished in %d", 
                    user.getId(), modifiedSince, System.currentTimeMillis() - startTime);
            
            User rv = update(user, modifiedSince, changes);
            success = true;
            return rv;
        }
        catch(HTTPServiceException httpse) {
            //silently ignore unexpected error that we are seeing if this
            //is not the first attempt to get user's data
            if(httpse.getStatusCode() == 1 && modifiedSince > 0) {
                return user;
            }
            throw httpse;
        }
        finally {
            Util.log("FB graph changes updated for uid: %s, since: %d finished in %d, success: %b", 
                    user.getId(), modifiedSince, System.currentTimeMillis() - startTime, success);
        }
    }
    
    private static List<String> getAllFacebookFriends(User user) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, APIException {
        String q = "SELECT uid2 FROM friend WHERE uid1 = me()";
        JSONArray friendIDs = FacebookGraphService.executeQuery(user.getFacebookAccessToken(), q);
        List<String> rv = new ArrayList<String>();
        for(int i=0; i<friendIDs.length(); i++) {
            rv.add(friendIDs.getJSONObject(i).getString("uid2"));
        }
        return rv;
        
    }
    
    private static Map<String, String> getNewFriendChangeQueries(User user, List<String> newFriendUserIDs) {
        Map<String, String> queries = new HashMap<String, String>();
        
        String uidINFragment = FacebookGraphService.getINFragment(newFriendUserIDs);
        
        String userInfoQuery = String.format("SELECT %s FROM user WHERE uid IN (%s)", StringUtils.join(User.FQL_FIELDS, ",") , uidINFragment);
        String friendAlbumInfoQuery = String.format("SELECT %s FROM album WHERE type IN ('normal') AND owner IN (SELECT uid FROM #user_info) ORDER BY created DESC LIMIT %d", StringUtils.join(Album.FQL_FIELDS, ","), Math.min(MAX_FRIEND_ALBUMS_FOR_NEW_FRIENDS, MAX_FRIEND_ALBUMS_PER_NEW_FRIEND * newFriendUserIDs.size()));
        String photoInfoQuery = String.format("SELECT %s FROM photo WHERE album_object_id IN (SELECT object_id FROM #friend_album_info)", StringUtils.join(Photo.FQL_FIELDS, ","));
        String locationInfoQuery = String.format("SELECT page_id, name, description, latitude, longitude FROM place WHERE page_id IN (SELECT current_location FROM user WHERE uid IN (SELECT uid FROM #user_info)) OR page_id IN (SELECT place_id FROM #photo_info)");
        
        
        queries.put("user_info", userInfoQuery);
        queries.put("friend_album_info", friendAlbumInfoQuery);
        queries.put("photo_info", photoInfoQuery);
        queries.put("user_and_photo_location", locationInfoQuery);
        
        return queries;
        
    }
    
    private static Map<String, String> getAllChangeSinceLastSyncQueries(User user, int modifiedSince) {
        Map<String, String> queries = new HashMap<String, String>();
        
        String friendIDsQuery = "SELECT uid2 FROM friend WHERE uid1 = me()";
        String userInfoQuery = String.format("SELECT %s FROM user WHERE (uid = me() OR uid IN (SELECT uid2 FROM #friend_ids)) AND (profile_update_time >= %d)", StringUtils.join(User.FQL_FIELDS, ","), modifiedSince);
        String userAlbumInfoQuery = String.format("SELECT %s FROM album WHERE owner = me() AND visible != 'custom' AND modified >= %d ORDER BY created DESC LIMIT %d", StringUtils.join(Album.FQL_FIELDS, ","), modifiedSince, MAX_USER_ALBUMS_PER_USER);
        String friendAlbumInfoQuery = String.format("SELECT %s FROM album WHERE type IN ('normal') AND owner IN (SELECT uid2 FROM #friend_ids) AND (modified >= %d) ORDER BY created DESC LIMIT %d", StringUtils.join(Album.FQL_FIELDS, ","), modifiedSince, MAX_FRIEND_ALBUMS_PER_USER);
        String photoInfoQuery = String.format("SELECT %s FROM photo WHERE (album_object_id IN (SELECT object_id FROM #user_album_info) OR album_object_id IN (SELECT object_id FROM #friend_album_info)) AND (modified >= %d)", StringUtils.join(Photo.FQL_FIELDS, ","), modifiedSince);
        String locationInfoQuery = String.format("SELECT page_id, name, description, latitude, longitude FROM place WHERE page_id IN (SELECT current_location FROM user WHERE uid IN (SELECT uid FROM #user_info)) OR page_id IN (SELECT place_id FROM #photo_info)");
        String userTaggedLocationsInfoQuery = String.format("SELECT page_id, timestamp, latitude, longitude FROM location_post WHERE (author_uid = %s OR %s IN tagged_uids) AND timestamp >= %d", user.getId(), user.getId(), getTagModifiedSinceLimit(modifiedSince));
        
        queries.put("friend_ids", friendIDsQuery);
        queries.put("user_info", userInfoQuery);
        queries.put("user_album_info", userAlbumInfoQuery);
        queries.put("friend_album_info", friendAlbumInfoQuery);
        queries.put("photo_info", photoInfoQuery);
        queries.put("user_and_photo_location", locationInfoQuery);
        queries.put("user_tagged_locations", userTaggedLocationsInfoQuery);
        
        return queries;
    }
    
    
    
    private static Map<String, JSONArray> getGraphChanges(User user, int modifiedSince) throws UnsupportedEncodingException, IOException, JSONException, HTTPServiceException, InterruptedException, ExecutionException, APIException, TimeoutException {
        
        List<String> existingFriendIDs = getUserFriendIDs(user);
        
        List<String> newFriendIDs = getAllFacebookFriends(user);
        newFriendIDs.removeAll(existingFriendIDs);
        boolean newFriendsFound = !newFriendIDs.isEmpty();
                
        List<String> existingUserIds = new ArrayList<String>(existingFriendIDs);
        existingUserIds.add(user.getId());
        
        Map<String, String> newFriendsQueries = getNewFriendChangeQueries(user, newFriendIDs);
        Map<String, String> changesSinceLastSyncQueries = getAllChangeSinceLastSyncQueries(user, modifiedSince);
        
        Future<Map<String, JSONArray>> newFriendChangesFuture = null;
        Future<Map<String, JSONArray>> sinceLastSyncChangesFuture = null;
        
        Map<String, JSONArray> newFriendChanges = null;
        Map<String, JSONArray> sinceLastSyncChanges = null;
        
        if(newFriendsFound && modifiedSince > 0) {
            newFriendChangesFuture = FacebookGraphService.executeMultiQueryAsync(user.getFacebookAccessToken(), newFriendsQueries);    
        }
        sinceLastSyncChangesFuture = FacebookGraphService.executeMultiQueryAsync(user.getFacebookAccessToken(), changesSinceLastSyncQueries);
        
        if(newFriendChangesFuture != null) {
            newFriendChanges = newFriendChangesFuture.get(FQL_TIMEOUT, TimeUnit.SECONDS);    
        }
        sinceLastSyncChanges = sinceLastSyncChangesFuture.get(FQL_TIMEOUT, TimeUnit.SECONDS);
        
        //note that this call modifies sinceLastSyncChanges as well
        Map<String, JSONArray> changes = mergeChangeSets(newFriendChanges, sinceLastSyncChanges);
        return changes;
        
    }
    
    private static User update(User user, int modifiedSince, Map<String, JSONArray> graphJSONData) throws JSONException, InterruptedException, ExecutionException, IOException, HTTPServiceException, APIException {
        Map<String, User> users = new HashMap<String, User>();
        Map<String, Album> albums = new HashMap<String, Album>();
        Map<String, JSONObject> locations = new HashMap<String, JSONObject>();
        
        JSONArray locationData = graphJSONData.get("user_and_photo_location");
        FacebookManager.translateFBLocationObjectsToLocal(locationData);
        
        for(int i=0; i<locationData.length(); i++) {
            JSONObject location = locationData.getJSONObject(i);
            locations.put(location.getString("id"), location);
        }
        
        JSONArray meAndFriends = graphJSONData.get("user_info");
        User me = null;
        
        for(int i=0; i<meAndFriends.length(); i++) {
            JSONObject uJSON = meAndFriends.getJSONObject(i);
            User u = User.createOrUpdate(uJSON);
            if(u.getId().equals(user.getId())) {
                me = u;
            }
            users.put(u.getId(), u);
            
            JSONObject currentLocationJSON = uJSON.optJSONObject("current_location");
            if(currentLocationJSON != null) {
                String currentLocationId = currentLocationJSON.optString("id", null);
                if(currentLocationId == null) {
                    logger.warn(String.format("missing id in location FB json: %s", currentLocationJSON.toString()));
                    continue;
                }
                
                if(locations.get(currentLocationId) == null) {
                    Util.log("missing location for location id: %s", currentLocationId);
                }
                else {
                    Location currentLocation = Location.createOrUpdate(locations.get(currentLocationId));
                    if(currentLocation != null) {
                        u.setCurrentLocation(currentLocation);    
                    }    
                }
            }
        }
        
        if(me == null) {
            me = user;
        }
        
        for(User u : users.values()) {
            if(u != me) {
                me.setIsFriendsWith(u); 
            }
        }
        JSONArray mineAndFriendAlbums = new JSONArray();
        JSONArray myAlbums = graphJSONData.get("user_album_info");
        JSONArray friendAlbums = graphJSONData.get("friend_album_info");
        
        for(int i=0; i<myAlbums.length(); i++) {
            JSONObject album = myAlbums.getJSONObject(i);
            String albumName = album.getString("name");
            if(IGNORE_ALBUM_WITH_NAME.contains(albumName)) continue;
            mineAndFriendAlbums.put(album);
        }
        for(int i=0; i<friendAlbums.length(); i++) {
            JSONObject album = friendAlbums.getJSONObject(i);
            String albumName = album.getString("name");
            if(IGNORE_ALBUM_WITH_NAME.contains(albumName)) continue;
            mineAndFriendAlbums.put(album);
        }
        
        for(int i=0; i<mineAndFriendAlbums.length(); i++) {
            JSONObject aJSON = mineAndFriendAlbums.getJSONObject(i);
            Album a = Album.createOrUpdate(aJSON);
            
            //owner might have added a new album, but might himself be already
            //seen so not a part of this update
            String ownerId = aJSON.getString("owner");
            User owner = users.get(ownerId);
            if(owner == null) {
                owner = User.get(ownerId);
            }
            owner.setOwnsAlbum(a);
            albums.put(a.getId(), a);
        }
        
        int photosWithLocation = 0;
        
        JSONArray mineAndFriendPhotos = graphJSONData.get("photo_info");
        
        for(int i=0; i<mineAndFriendPhotos.length(); i++) {
            JSONObject pJSON = mineAndFriendPhotos.getJSONObject(i);
            Photo p = Photo.createOrUpdate(pJSON);
            Album album = albums.get(pJSON.get("album_object_id"));
            //we ignore some of the fetched albums, check is necessary
            if(album != null) {
                album.setOwnsPhoto(p, PhotoSource.FACEBOOK);    
            }
            
            String placeId = pJSON.getString("place_id");
            if(placeId != null && !placeId.equals("null")) {
                Location photoLocation = Location.createOrUpdate(locations.get(placeId));
                if(photoLocation == null) {
                    continue;
                }
                
                photosWithLocation++;
                p.setLocation(photoLocation);
            }
        }
        
        JSONArray myTaggedLocations = graphJSONData.get("user_tagged_locations");
        FacebookManager.translateFBLocationObjectsToLocal(myTaggedLocations);
        
        for(int i=0; i<myTaggedLocations.length(); i++) {
            JSONObject locationTagObject = myTaggedLocations.getJSONObject(i);
            Location currentLocation = Location.createOrUpdate(locationTagObject);
            if(currentLocation != null) {
                Tag.createOrUpdate(me, currentLocation, locationTagObject.getInt("timestamp"));
            }
        }
        
        UserFacebookLocationUpdaterService.submit(new ArrayList<User>(users.values()), 
                user.getFacebookAccessToken(), 
                getTagModifiedSinceLimit(modifiedSince));
        
        return me;
    }
    
    private static int getTagModifiedSinceLimit(int modifiedSince) {
        return (int)Math.max((System.currentTimeMillis()/1000) - MAX_LOCATION_AGE_SECONDS, modifiedSince);
    }
    
    private static List<String> getUserFriendIDs(User user) {
        List<User> friends = user.getFriends();
        List<String> currentFriendIds = new ArrayList<String>();
        for(User friend : friends) {
            currentFriendIds.add(friend.getId());
        }
        return currentFriendIds;
    }
    
    private static Map<String, JSONArray> mergeChangeSets(Map<String, JSONArray> newFriends, Map<String, JSONArray> sinceLastSync) throws JSONException {
        if(newFriends == null || newFriends.isEmpty()) {
            return sinceLastSync;
        }
        
        for(String query : newFriends.keySet()) {
            JSONArray newFriendChanges = newFriends.get(query);
            if(newFriendChanges != null) {
                JSONArray sinceLastSyncChanges = sinceLastSync.get(query);
                if(sinceLastSyncChanges != null) {
                    for(int i=0; i<newFriendChanges.length(); i++) {
                        JSONObject newFriendChange = newFriendChanges.getJSONObject(i);
                        //TODO: de-dup
                        sinceLastSyncChanges.put(newFriendChange);
                    }
                }
            }
        }
        
        return sinceLastSync;
    }
}


