package pe.snap.infra;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import pe.snap.imagehash.ImagePHash;
import pe.snap.infra.PhotoManager.PhotoSize;
import pe.snap.services.ImageResizeService;
import pe.snap.util.Analytics;
import pe.snap.util.Util;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;

public class PhotoMetadata {
    public int timestamp;
    public int width;
    public int height;
    public int thumbWidth;
    public int thumbHeight;
    public String hash;
    public String pHash;
    
    private PhotoMetadata() {}
    
    //TODO: iterate through exif directories to find tag and make it work for FB photos
    //TODO:set pHash as well
    //TODO: what if the image was resized but exif not updated?
    //TODO: the thumb might be smaller if original image is smaller than
    public static PhotoMetadata get(String hash) throws ImageProcessingException, IOException {
        PhotoMetadata rv = new PhotoMetadata();
        rv.hash = hash;
        
        String standardPath = PhotoManager.getFilePath(hash, PhotoSize.STANDARD);
        String pHash = ImagePHash.getHash(FileUtils.openInputStream(new File(standardPath)));
        rv.pHash = pHash;
        
        String filePath = PhotoManager.getFilePath(hash, PhotoSize.FULL);
        File file = new File(filePath);
        Metadata metadata = ImageMetadataReader.readMetadata(file);
        
        ExifSubIFDDirectory directory = metadata.getDirectory(ExifSubIFDDirectory.class);
        
        try {
            Date date = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
            rv.timestamp = (int)(date.getTime()/1000);    
        }
        catch(Throwable t) {
            rv.timestamp = (int)Util.getCurrentTimestamp();
            Util.log("photo_timestamp_extraction_failed, reason: %s", t.toString());
            Analytics.serverLog("photo_timestamp_extraction_failed", "reason", t.toString());
        }
        
        try {
            rv.width = directory.getInt(ExifSubIFDDirectory.TAG_EXIF_IMAGE_WIDTH);
            rv.height = directory.getInt(ExifSubIFDDirectory.TAG_EXIF_IMAGE_HEIGHT);    
        }
        catch(Throwable t) {
            rv.width = ImageResizeService.STANDARD_SIZE;
            rv.height = ImageResizeService.STANDARD_SIZE;
            Analytics.serverLog("photo_dimension_extraction_failed", "reason", t.toString());
        }
        
        rv.thumbWidth = rv.thumbHeight = ImageResizeService.THUMB_SIZE;
        
        return rv;
    }
}
