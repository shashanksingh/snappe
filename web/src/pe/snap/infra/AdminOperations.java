package pe.snap.infra;

import java.util.concurrent.ExecutionException;

import org.neo4j.graphdb.Relationship;

import pe.snap.model.Album;
import pe.snap.model.Photo;

public class AdminOperations {
    
    public static void deleteAlbum(Album album) throws InterruptedException, ExecutionException {
        for(Photo photo : album.getPhotos()) {
            deletePhoto(photo);
        }
        for(Relationship r : album.getWrappedNode().getWrappedNode().getRelationships()) {
            r.delete();
        }
        //album.getWrappedNode().getWrappedNode().delete();
    }
    
    public static void deletePhoto(Photo photo) {
        for(Relationship r : photo.getWrappedNode().getWrappedNode().getRelationships()) {
            r.delete();    
        }
        photo.getWrappedNode().getWrappedNode().delete();
    }

}
