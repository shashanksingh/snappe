package pe.snap.infra;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

public class ConnectionPool {
	static DataSource localDatasource;
	
	private static DataSource createLocalhostDataSource() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/snappe");
		dataSource.setUsername("snappe");
		dataSource.setPassword("ohlookhowrandomthispasswordis");
		dataSource.setValidationQuery("SELECT 1 AS dbcp_connection_test");
		dataSource.setTestOnBorrow(true);
		dataSource.setMaxActive(1000);
		dataSource.setInitialSize(10);
		return dataSource;
	}
	
	public static synchronized void init() throws ClassNotFoundException {
		if(localDatasource != null) return;
		localDatasource = createLocalhostDataSource();
	}
	
	public static Connection getConnection() throws SQLException {
		return localDatasource.getConnection();
	}
}
