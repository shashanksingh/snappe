package pe.snap.infra;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.jasper.JasperException;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.neo4j.graphdb.Transaction;

import pe.snap.exceptions.APIException;
import pe.snap.exceptions.APIException.APIExceptionType;
import pe.snap.services.ExceptionReportingService;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.util.Analytics;
import pe.snap.util.Constants;
import pe.snap.util.RequestUtil;
import pe.snap.util.Util;

public class APIFilter implements Filter {
	private static final int DEFAULT_API_ERROR_CODE = -1;
	private static final String DEFAULT_ERROR_MESSAGE = "unexpected error";
	private static final int DEFAUT_HTTP_ERROR_CODE = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
	private static final String RESPONSE_CONTENT_TYPE = "application/json";
	
    private static final Logger logger = Logger.getLogger(APIFilter.class);
    
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)resp;
		
		long startTime = Util.getCurrentTimestamp();
		request.setAttribute(Constants.REQUEST_START_TIME, startTime);
		
		try {
			
		    APIRequestWrapper wrappedRequest;
		    APIResponseWrapper wrappedResponse;
		    
		    wrappedRequest = new APIRequestWrapper(request);
			wrappedResponse = new APIResponseWrapper(response);
			
			Transaction tx = DBManager.getTransaction();
			try {
			    filterChain.doFilter(wrappedRequest, wrappedResponse);
			    tx.success();
			}
			finally {
			    tx.finish();
			}
			
			response.setContentType(RESPONSE_CONTENT_TYPE);
			response.setCharacterEncoding("UTF-8");
			
			tx = DBManager.getTransaction();
            try {
                handleSuccess(request, response, wrappedResponse.getData());
                tx.success();
            }
            finally {
                tx.finish();
            }
		}
		catch(Throwable t) {
			logger.error("error in serving API request", t);
			t.printStackTrace(System.err);
			
			Transaction tx = DBManager.getTransaction();
			try {
			    handleError(request, response, t);
                tx.success();
            }
            finally {
                tx.finish();
            }	
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
	
	private void handleSuccess(HttpServletRequest request, HttpServletResponse response, byte[] data) throws JSONException, IOException, APIException, HTTPServiceException, InterruptedException, ExecutionException, SQLException {
		
	    Future<Long> fbUpdateCallHandle = (Future<Long>)request.getAttribute(Constants.FB_GRAPH_UPDATE_CALL_HANDLE);
	    if(fbUpdateCallHandle != null) {
	        long timeTaken = fbUpdateCallHandle.get();
	        Analytics.log(request, "fb_update_user_sub_graph", "time_taken", timeTaken);    
	    }
	    
		JSONObject successJSON = new JSONObject();
		successJSON.put("start_time", request.getAttribute(Constants.REQUEST_START_TIME));
		successJSON.put("end_time", Util.getCurrentTimestamp());
		successJSON.put("success", true);
		
		if(data.length > 0) {
			String requestResponseData = new String(data, "UTF-8");
			JSONTokener tokener = new JSONTokener(requestResponseData);
			Object responseJSONData = tokener.nextValue();
			successJSON.put("data", responseJSONData);	
		}
		
		JSONObject graphChangeData = RequestUtil.getUserGraphChangeData(request);
		successJSON.put("graphChangeData", graphChangeData);
		//successJSON.put("graphChangeData", new JSONObject());
		
		JSONArray pendingNotifications = RequestUtil.getUserPendingNotifications(request);
		successJSON.put("notifications", pendingNotifications);
		
		JSONObject creditsJSON = RequestUtil.getUserCredits(request);
		successJSON.put("credits", creditsJSON);
		
		//Util.log("response: %s", successJSON.toString(2));
		response.getWriter().print(successJSON.toString());
	}
	
	public static void handleError(HttpServletRequest request, HttpServletResponse response, Throwable t) throws IOException {
	    ExceptionReportingService.submit(t);
	    
	    response.reset();
	    
	    response.setContentType(RESPONSE_CONTENT_TYPE);
		response.setCharacterEncoding("UTF-8");
		
	    int code = DEFAULT_API_ERROR_CODE;
	    String message = DEFAULT_ERROR_MESSAGE;
	    int httpErrorCode = DEFAUT_HTTP_ERROR_CODE;
	    
	    
	    if(t instanceof JasperException) {
	        t = ((JasperException)t).getRootCause();
	        if(t instanceof ServletException) {
	            t = ((ServletException)t).getRootCause();
	        }
	    }
	    
	    if(t instanceof APIException) {
	    	APIException ex = (APIException)t;
	    	code = ex.getAPIErrorCode();
    		message = ex.getLocalizedMessage();
    		httpErrorCode = ex.getHTTPErrorCode();
    		
    		if(ex.getAPIExceptionType() == APIExceptionType.ACCESS_TOKEN_AUTH_FAILED) {
    		    RequestUtil.unsetAccessTokenForCurrentUserIfAvailable(request);    
    		}
	    }
	    
	    JSONObject errorJSON = new JSONObject();
	    try {
	    	errorJSON.put("code", code);
	    	errorJSON.put("message", message);
	    	errorJSON.put("success", false);
	    	errorJSON.put("start_time", request.getAttribute(Constants.REQUEST_START_TIME));
	    	errorJSON.put("end_time", Util.getCurrentTimestamp());
	    	response.setStatus(httpErrorCode);
	    	response.getWriter().print(errorJSON.toString(2));
	    }
	    catch(JSONException jsonException) {
	    	Util.log("JSON Exception");
	    	jsonException.printStackTrace(System.err);
	    	response.sendError(DEFAUT_HTTP_ERROR_CODE, DEFAULT_ERROR_MESSAGE);
	    }
	    catch(Throwable th) {
			logger.error("error in serving API request", th);
			th.printStackTrace(System.err);
		}
	}
	
	public static void main(String[] args) {
	}
}

class APIRequestWrapper extends HttpServletRequestWrapper {
	private static final Logger logger = Logger.getLogger(APIRequestWrapper.class);
	
	public APIRequestWrapper(HttpServletRequest request) throws UnsupportedEncodingException, APIException, JSONException, IOException, HTTPServiceException, InterruptedException, ExecutionException, SQLException, TimeoutException {
		super(request);
		
		Transaction tx = DBManager.getTransaction();
		try {
		    RequestUtil.getUser(request);
		    tx.success();
		}
		finally {
		    tx.finish();
		}
		
		Logger servletLogger = RequestUtil.getLogger(request);
        request.setAttribute(Constants.REQUEST_LOGGER_ATTR, servletLogger);
		
		tx = DBManager.getTransaction();
        try {
            Future<Long> fbUpdateCallHandle = RequestUtil.updateUserFacebookSubGraph(request);
            if(fbUpdateCallHandle != null) {
                request.setAttribute(Constants.FB_GRAPH_UPDATE_CALL_HANDLE, fbUpdateCallHandle);    
            }
            tx.success();
        }
        finally {
            tx.finish();
        }
		
	}
}

class APIResponseWrapper extends HttpServletResponseWrapper {
	
	private final ByteArrayOutputStream output;
	private APIServletOutputStream placeHolderOutput;
	private PrintWriter printWriter;

	public APIResponseWrapper(HttpServletResponse response) {
		super(response);
		output = new ByteArrayOutputStream();
	}
	
	@Override
	public ServletOutputStream getOutputStream() {
		if(placeHolderOutput == null) {
			placeHolderOutput = new APIServletOutputStream(output);
		}
		return placeHolderOutput;
	}
	
	@Override
	public PrintWriter getWriter() throws UnsupportedEncodingException {
		if(printWriter == null) {
			printWriter = new PrintWriter(new OutputStreamWriter(getOutputStream(), "UTF-8"), true);
		}
	    return printWriter;
	}
	
	@Override
	public void flushBuffer() throws IOException {
		if(printWriter != null) {
			printWriter.flush();
		}
		else if(placeHolderOutput != null) {
			placeHolderOutput.flush();
		}
		super.flushBuffer();
	}
	
	public byte[] getData() throws IOException {
		this.flushBuffer();
		return output.toByteArray();
	}
}

class APIServletOutputStream extends ServletOutputStream {

	private BufferedOutputStream stream;

	public APIServletOutputStream(OutputStream output) {
		stream = new BufferedOutputStream(output);
	}

	public void write(int b) throws IOException {
		stream.write(b);
	}

	public void write(byte[] b) throws IOException {
		stream.write(b);
	}

	public void write(byte[] b, int off, int len) throws IOException {
		stream.write(b, off, len);
	}
	
	@Override
	public void flush() throws IOException {
		stream.flush();
		super.flush();
	}
	
	@Override
	public void close() throws IOException {
		stream.close();
		super.close();
	}

}
