package pe.snap.infra;

import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSetting;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.graphdb.index.UniqueFactory;
import org.neo4j.graphdb.index.UniqueFactory.UniqueNodeFactory;
import org.neo4j.graphdb.index.UniqueFactory.UniqueRelationshipFactory;
import org.neo4j.kernel.GraphDatabaseAPI;
import org.neo4j.server.WrappingNeoServerBootstrapper;
import org.neo4j.server.configuration.Configurator;
import org.neo4j.server.configuration.ServerConfigurator;

import pe.snap.model.Photo;
import pe.snap.model.SnappeNode.UniquenessType;
import pe.snap.model.TrackedPropertyContainer;
import pe.snap.model.User;
import pe.snap.util.Constants;
import pe.snap.util.Util;
import scala.actors.threadpool.Arrays;


public class DBManager {
	
	private static GraphDatabaseAPI db;
	private static WrappingNeoServerBootstrapper srv;
	
	private static final String[] INDEXED_PROEPRTIES = new String[]{
		TrackedPropertyContainer.ID_KEY,
		Constants.SNAPPE_TYPE_KEY,
		User.FB_ACCESS_TOKEN_KEY,
		Photo.PHOTO_HASH_KEY,
		Photo.PHOTO_THUMB_SRC_KEY
	};
	
	public static void startDB() {
		db = (GraphDatabaseAPI)new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(Constants.getDBPath()).
	    setConfig(GraphDatabaseSettings.node_keys_indexable, StringUtils.join(INDEXED_PROEPRTIES, ',')).
	    setConfig(GraphDatabaseSettings.relationship_keys_indexable, TrackedPropertyContainer.ID_KEY).
	    setConfig(GraphDatabaseSettings.node_auto_indexing, GraphDatabaseSetting.TRUE).
	    setConfig(GraphDatabaseSettings.relationship_auto_indexing, GraphDatabaseSetting.TRUE ).
	    newGraphDatabase();
		
		ServerConfigurator config = new ServerConfigurator(db);
		config.configuration().setProperty(Configurator.WEBSERVER_PORT_PROPERTY_KEY, 7575);
		config.configuration().setProperty(Configurator.WEBSERVER_ADDRESS_PROPERTY_KEY, "0.0.0.0");
		        
		srv = new WrappingNeoServerBootstrapper(db, config);
		srv.start();
		
	}
	
	public static void stopDB() {
	    if(srv != null) {
	        srv.stop();    
	    }
	    if(db != null) {
	        db.shutdown();    
	    }
	}
	
	public static GraphDatabaseService getDB() {
		return db;
	}
	
	public static Transaction getTransaction() {
		return db.beginTx();
	}
	
	public static ExecutionEngine getExecutionEngine() {
		return new ExecutionEngine(db);
	}
	
	public static Node getOrCreateNode(final String id) {
        UniqueFactory<Node> uniqueNodeFactory = new UniqueNodeFactory(db, "snappe_global_unique_node_index"){
            @Override
            protected void initialize(Node created, Map<String, Object> properties){
                created.setProperty(TrackedPropertyContainer.ID_KEY, id);
                created.setProperty(Constants.SNAPPE_CREATION_TIMESTAMP_KEY, Util.getCurrentTimestamp());
            }
        };
        Node rv = (Node) uniqueNodeFactory.getOrCreate(TrackedPropertyContainer.ID_KEY, id);
        return rv;
    }

    public static Relationship getOrCreateRelationship(final Node startNode, final Node endNode, final RelationshipType type, UniquenessType uniqueness) {
        UniqueFactory<Relationship> uniqueRelationshipFactory = new UniqueRelationshipFactory(db, "snappe_global_unique_relationship_index"){

            @Override
            protected void initialize(Relationship created, Map<String, Object> properties){
                created.setProperty(TrackedPropertyContainer.ID_KEY, properties.get(TrackedPropertyContainer.ID_KEY));
                created.setProperty(Constants.SNAPPE_CREATION_TIMESTAMP_KEY, Util.getCurrentTimestamp());
            }

            @Override
            protected Relationship create(Map<String, Object> properties) {
                return startNode.createRelationshipTo(endNode, type);
            }
        };
        
        String startID = startNode.getProperty(TrackedPropertyContainer.ID_KEY).toString();
        String endID = endNode.getProperty(TrackedPropertyContainer.ID_KEY).toString();
        
        String key = null;
        if(uniqueness == UniquenessType.DIRECTIONAL) {
            key = String.format("%s%s%s", startID, type.toString(), endID);
        }
        else if(uniqueness == UniquenessType.NON_DIRECTIONAL) {
            String[] keys = {startID, endID};
            Arrays.sort(keys);
            key = String.format("%s%s%s", keys[0], type.toString(), keys[1]);
        }
        else {
            key = UUID.randomUUID().toString();
        }
        
        Relationship rv = uniqueRelationshipFactory.getOrCreate(TrackedPropertyContainer.ID_KEY, key);
        return rv;
    }
}
