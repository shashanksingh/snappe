package pe.snap.infra;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.PropertyConfigurator;
import org.opencv.core.Core;

import pe.snap.services.AlbumZipService;
import pe.snap.services.AlertService;
import pe.snap.services.EmailService;
import pe.snap.services.EmailShareService;
import pe.snap.services.ExceptionReportingService;
import pe.snap.services.FacebookGraphService;
import pe.snap.services.FacebookTokenExtensionService;
import pe.snap.services.ImageResizeService;
import pe.snap.services.LocationLoggingService;
import pe.snap.services.NotificationService;
import pe.snap.services.UserFacebookLocationUpdaterService;
import pe.snap.services.http.HTTPService;


public class ServerStartListener implements ServletContextListener {
    
    private static void reportException(Throwable t) {
        AlertService.sendErrorAlert("Error in ServerStartListener", ExceptionUtils.getFullStackTrace(t));
    }

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	    
		try {
			DBManager.stopDB();	
		}
		catch(Throwable t) {
			reportException(t);
		}
		
		try {
			HTTPService.destroy();
		}
		catch(Throwable t) {
			reportException(t);
		}
		
		try {
            EmailShareService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            AlbumZipService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            EmailService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            FacebookTokenExtensionService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            ImageResizeService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            LocationLoggingService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            NotificationService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            FacebookGraphService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            UserFacebookLocationUpdaterService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            ExceptionReportingService.stop();
        }
        catch(Throwable t) {
            reportException(t);
        }
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
	    try {
	        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);    
	    }
	    catch(Throwable t) {
	        reportException(t);
	    }
	    
	    try {
			ConnectionPool.init();
		}
		catch(Throwable t) {
			reportException(t);
		}
		
		try {
			HTTPService.init();
		}
		catch(Throwable t) {
			reportException(t);
		}
		
		try {
			DBManager.startDB();
		}
		catch(Throwable t) {
			reportException(t);
		}
		
		try {
            EmailShareService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            AlbumZipService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            EmailService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            FacebookTokenExtensionService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            ImageResizeService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            LocationLoggingService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            NotificationService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            FacebookGraphService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            UserFacebookLocationUpdaterService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
        
        try {
            ExceptionReportingService.start();
        }
        catch(Throwable t) {
            reportException(t);
        }
		
	}

}
