package pe.snap.infra;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.JSONException;
import org.json.JSONObject;

import pe.snap.services.AlertService;
import pe.snap.services.ExceptionReportingService;
import pe.snap.util.Util;

public class ExceptionTrackingFilter implements Filter {

    private static String excludePattern;
    
    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException 
    {
        try {
            chain.doFilter(req, res);
        }
        catch(Throwable t) {
            try {
                processException(t, (HttpServletRequest)req);
            }
            catch(Throwable tt) {
                AlertService.sendErrorAlert("Error in ExceptionTrackingFilter", ExceptionUtils.getFullStackTrace(tt));
            }
            throw t;
        }
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        excludePattern = config.getInitParameter("exclude-url-pattern");
    }
    
    private void processException(Throwable t, HttpServletRequest request) throws JSONException {
        Util.log("got exception, %s", excludePattern);
        
        if(excludePattern != null) {
            HttpServletRequest httpRequest = request;
            String requestURL = httpRequest.getRequestURL().toString();
            Util.log("req url: %s", requestURL);
            if(!requestURL.matches(excludePattern)) {
                Util.log("reprting ex: %s", t.getLocalizedMessage());
                ExceptionReportingService.submit(t);
            }
        }
        else {
            ExceptionReportingService.submit(t);
        }
        
        //TODO: do it async
        logException(request, t);
        
    }
    
    private static void logException(HttpServletRequest request, Throwable t) throws JSONException {
        Util.log("Exception in processing request:");
        
        String contentType = request.getContentType();
        if(contentType != null && contentType.indexOf("image") >= 0) {
            Util.log("<skipping request parameters for content type:%s>", contentType);    
        }
        else {
            Map<String, String[]> params = request.getParameterMap(); 
            JSONObject paramsJSON = new JSONObject(params);
            Util.log("%s\n", paramsJSON.toString(2));
        }
        
        Util.log("%s", ExceptionUtils.getFullStackTrace(t));
    }
}
