package pe.snap.infra;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifIFD0Directory;

import pe.snap.services.ImageResizeService;
import pe.snap.util.Constants;
import pe.snap.util.Util;

public class PhotoManager {
    private static final int HASH_CONTENT_SIZE = 1000;
    
    public static enum PhotoSize {
        FULL, STANDARD, THUMB    
    };
    
    //@TODO: add better parsing
    private static String getExtensionFromContentType(String contentType) {
        if(contentType == null) {
            return null;
        }
        
        if(contentType.contains("jpg") || contentType.contains("jpeg")) {
            return "jpg";
        }
        return null;
    }
    
	public static String getFilePath(String photoHash, PhotoSize size) {
	    String suffix = "";
	    if(size == PhotoSize.THUMB) {
            suffix = "_t";
        }
        else if(size == PhotoSize.STANDARD) {
            suffix = "_s";
        }
		return String.format("%s%s%s%s%s%s.jpg", Constants.getDataRootPath(), File.separator, "snappe_photos", File.separator, photoHash, suffix);
	}
	
	public static void savePhoto(String photoHash, InputStream imageDataStream) throws IOException {
		String fullScalePath = getFilePath(photoHash, PhotoSize.FULL);
		String standardPath = getFilePath(photoHash, PhotoSize.STANDARD);
		String thumbPath = getFilePath(photoHash, PhotoSize.THUMB);
		
	    FileUtils.copyInputStreamToFile(imageDataStream, new File(fullScalePath));
	    ImageResizeService.process(fullScalePath, standardPath, thumbPath);
	}
	
	public static PhotoMetadata savePhoto(InputStream imageDataStream, String contentType) throws NoSuchAlgorithmException, IOException, ImageProcessingException, DigestException {
	    
	    File temp = File.createTempFile("uploaded_photo_temp", getExtensionFromContentType(contentType));
	    temp.deleteOnExit();
	    
	    FileUtils.copyInputStreamToFile(imageDataStream, temp);
	    
	    String photoHash = getHash(temp);
	    
	    String fullScalePath = getFilePath(photoHash, PhotoSize.FULL);
        String standardPath = getFilePath(photoHash, PhotoSize.STANDARD);
        String thumbPath = getFilePath(photoHash, PhotoSize.THUMB);
        
        FileUtils.copyFile(temp, new File(fullScalePath));
        ImageResizeService.process(fullScalePath, standardPath, thumbPath);
        
        PhotoMetadata rv = PhotoMetadata.get(photoHash);
        
        return rv;
	}
	
	public static boolean isAcceptableContentType(String contentType) {
	    return getExtensionFromContentType(contentType) != null;
	}
	
	public static boolean hasPhoto(String photoHash, PhotoSize size) {
		String filePath = getFilePath(photoHash, size);
		return Util.fileExists(filePath);
	}
	
	public static String getStandardPhotoURL(String hash) {
	    return getPhotoURL(hash, PhotoSize.STANDARD);
	}
	
	public static String getFullPhotoURL(String hash) {
	    return getPhotoURL(hash, PhotoSize.FULL);
	}
	
	public static String getPhotoURL(String hash, PhotoSize size) {
	    String suffix = "";
	    if(size == PhotoSize.THUMB) {
	        suffix = "?thumb";
	    }
	    else if(size == PhotoSize.FULL) {
	        suffix = "?full";
	    }
	    return String.format("%s/static/photos/%s%s", Constants.getServerURL(), hash, suffix);	
	}
	
	public static String getThumbnailURL(String hash) {
	    return getPhotoURL(hash, PhotoSize.THUMB);
	}
	
	public static void main(String[] args) throws Exception {
	    Metadata metadata = ImageMetadataReader.readMetadata(new File("/Users/shashank/Desktop/test.jpg"));
	    for (Directory directory : metadata.getDirectories()) {
	        for (Tag tag : directory.getTags()) {
	            System.out.println(tag);
	        }
	    }
	    
	    ExifIFD0Directory directory = metadata.getDirectory(ExifIFD0Directory.class);
	    System.out.println("metadata " + directory);
	}
	
	private static String getHash(File f) throws IOException, NoSuchAlgorithmException, DigestException {
	    byte[] bytes = FileUtils.readFileToByteArray(f);
	    
	    MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(bytes, Math.max(0, bytes.length - HASH_CONTENT_SIZE), HASH_CONTENT_SIZE);
	    
	    return Hex.encodeHexString(md.digest());
	}
	
	public static void updateHash(String existingHash, String updatedHash) throws IOException {
	    for(PhotoSize size : new PhotoSize[]{PhotoSize.FULL, PhotoSize.STANDARD, PhotoSize.THUMB}) {
	        String existingPath = getFilePath(existingHash, size);
	        if(!Util.fileExists(existingPath)) {
	            continue;
	        }
	        
	        String updatedPath = getFilePath(updatedHash, size);
	        
	        FileUtils.copyFile(new File(existingPath), new File(updatedPath));
	    }
	}
}
