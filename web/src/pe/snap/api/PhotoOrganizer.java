package pe.snap.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pe.snap.exceptions.APIException;
import pe.snap.infra.PhotoManager;
import pe.snap.infra.PhotoManager.PhotoSize;
import pe.snap.model.*;
import pe.snap.model.Album.PhotoSource;
import pe.snap.services.FacebookGraphService;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.util.Analytics;
import pe.snap.util.FacebookManager;
import pe.snap.util.Util;

public class PhotoOrganizer {
	
	public static void organize(String deviceID, List<Album> albums, List<Photo> photos) throws InterruptedException, ExecutionException {
		TreeMap<Integer, Album> startTimesToAlbums = new TreeMap<Integer, Album>();
		TreeMap<Integer, Album> endTimesToAlbums = new TreeMap<Integer, Album>();
		Map<Album, Integer> albumToStartTime = new HashMap<Album, Integer>();
		Map<Album, Integer> albumToEndTime = new HashMap<Album, Integer>();
		
		for(Album album : albums) {
			int oldestTime = album.getOldestPhotoTime();
			int newestTime = album.getNewestPhotoTime();
			
			if(oldestTime >= 0 && newestTime >= 0) {
				startTimesToAlbums.put(oldestTime, album);
			    endTimesToAlbums.put(newestTime, album);
			    albumToStartTime.put(album, oldestTime);
			    albumToEndTime.put(album, newestTime);	
			}
		}
		
		for(Photo photo : photos) {
		    int clickedOn = photo.getCreatedOn();
		    Util.log("clickedOn: %d", clickedOn);
		    
		    Set<Album> startedBefore = new HashSet<Album>(startTimesToAlbums.subMap(clickedOn - 86400, clickedOn + 86400).values());
		    Set<Album> endedAfter = new HashSet<Album>(startTimesToAlbums.subMap(clickedOn - 86400, clickedOn + 86400).values());
		    
		    startedBefore.retainAll(endedAfter);
		    Set<Album> candidateAlbums = startedBefore;
		    
		    if(candidateAlbums.size() == 0) {
		    	Album album = new Album(deviceID);
		    	album.setIsAutoCreatedAlbum();
		    	albums.add(album);
		    	album.setOwnsPhoto(photo, PhotoSource.DEVICE);
		    	
		    	startTimesToAlbums.put(clickedOn, album);
		    	endTimesToAlbums.put(clickedOn, album);
		    	albumToStartTime.put(album, clickedOn);
		    	albumToEndTime.put(album, clickedOn);
		    }
		    else {
		    	Album album = candidateAlbums.iterator().next();
		    	album.setOwnsPhoto(photo, PhotoSource.DEVICE);
		    	
		    	int startTime = albumToStartTime.get(album);
		    	if(clickedOn < startTime) {
		    		albumToStartTime.put(album, clickedOn);
		    		startTimesToAlbums.remove(startTime);
		    		startTimesToAlbums.put(clickedOn, album);
		    	}
		    	else {
		    		int endTime = albumToEndTime.get(album);
		    		if(clickedOn > endTime) {
		    			albumToEndTime.put(album, clickedOn);
			    		endTimesToAlbums.remove(endTime);
			    		endTimesToAlbums.put(clickedOn, album);
		    		}
		    	}
		    }
		}
		
		for(Album album : albums) {
			if(album.isDeviceAlbum() && album.isAutoCreatedAlbum()) {
				album.autoGenerateName();	
			}	
		}
	}
	
	public static void addNewPhotos(User user, String deviceID, JSONArray newPhotos) throws JSONException, UnsupportedEncodingException, IOException, HTTPServiceException, InterruptedException, ExecutionException, APIException {
	    //process all locations in batch
        List<double[]> photoCoordinates = new ArrayList<double[]>();
        Set<String> distinctCoordinates = new HashSet<String>();
        
        for(int i=0; i<newPhotos.length(); i++) {
            JSONObject albumJSON = newPhotos.getJSONObject(i);
            JSONArray photoJSONArray = albumJSON.getJSONArray("photos");
            for(int j=0; j<photoJSONArray.length(); j++) {
                JSONObject photoJSON = photoJSONArray.getJSONObject(j);
                double latitude = photoJSON.getDouble("latitude");
                double longitude = photoJSON.getDouble("longitude");
                if(latitude != 0 || longitude != 0) {
                    String key = String.format("%f-%f", latitude, longitude);
                    if(!distinctCoordinates.contains(key)) {
                        photoCoordinates.add(new double[]{latitude, longitude});
                        distinctCoordinates.add(key);    
                    }
                    
                }
            }
        }
        
        long addNewPhotosStartTime = System.currentTimeMillis();
        
        Future<Object[]> coordinatesCallHandle = FacebookGraphService.getLocationForCoordinates(photoCoordinates, user.getFacebookAccessToken());
        
        List<Photo> defaultAlbumPhotos = new ArrayList<Photo>();
        Set<String> existingAutoAlbumPhotoHashes = new HashSet<String>();
        
        List<Album> existingAlbums = user.getAlbums();
        Map<Object, Album> existingAlbumDevicePersitentIdToAlbum = new HashMap<Object, Album>();
        Map<String, Album> existingAutoCreatedAlbumNameToAlbum = new HashMap<String, Album>();
        for(Album existingAlbum : existingAlbums) {
            String devicePersistentID = existingAlbum.getDevicePersistentID();
            if(devicePersistentID != null) {
                existingAlbumDevicePersitentIdToAlbum.put(devicePersistentID, existingAlbum);    
            }
            //TODO:duplicate names possible?
            if(existingAlbum.isAutoCreatedAlbum()) {
                existingAutoCreatedAlbumNameToAlbum.put(existingAlbum.getName(), existingAlbum);
                for(Photo photo : existingAlbum.getPhotos()) {
                    existingAutoAlbumPhotoHashes.add(photo.getHash());
                }
            }
        }
        
        JSONArray albumsJSONArray = newPhotos;
        Util.log("albumsJSONArray:\n%s", albumsJSONArray.toString(2));
        
               
        Map<String, List<Photo>> coordinatesToPhoto = new HashMap<String, List<Photo>>();
        
        for(int i=0; i<albumsJSONArray.length(); i++) {
            JSONObject albumJSON = albumsJSONArray.getJSONObject(i);
            String deviceAlbumType = albumJSON.getString("type");
            String devicePersistentID = albumJSON.getString("devicePersistentId");
            
            
            if(!deviceAlbumType.equals("default") && !existingAlbumDevicePersitentIdToAlbum.containsKey(devicePersistentID)) {
                String albumName = albumJSON.getString("name");
                if(albumName != null && existingAutoCreatedAlbumNameToAlbum.containsKey(albumName)) {
                    //a new album has been created on the device with the same name
                    //as an existing auto created album. this could either be the
                    //user manually creating it, or new photos being downloaded
                    //into an auto created album which had hitherto only a
                    //logical existence. we merge the auto created album into
                    //the device album
                    
                    Album existingAutoAlbumWithSameName = existingAutoCreatedAlbumNameToAlbum.get(albumName);
                    Util.log("merging and destroying album %s (%s) because a new namesake device album was created", albumName, existingAutoAlbumWithSameName.getId());
                    
                    existingAlbumDevicePersitentIdToAlbum.put(devicePersistentID, existingAutoAlbumWithSameName);
                    existingAutoCreatedAlbumNameToAlbum.remove(albumName);
                    
                    existingAutoAlbumWithSameName.setDevicePersistentID(devicePersistentID);
                    existingAutoAlbumWithSameName.unsetIsAutoCreatedAlbum();
                    
                    Util.log("merged and destroyed album %s (%s) because a new namesake device album was created", albumName, existingAutoAlbumWithSameName.getId());
                }
                else {
                    String id = albumJSON.getString("id");
                    Album album = new Album(id, deviceID);
                    user.setOwnsAlbum(album);
                    existingAlbumDevicePersitentIdToAlbum.put(devicePersistentID, album);
                }
            }
            
            Album album = existingAlbumDevicePersitentIdToAlbum.get(devicePersistentID);
            if(album != null) {
                album.setName(albumJSON.getString("name"));
                album.setDescription(albumJSON.getString("description"));
                album.setDevicePersistentID(devicePersistentID);
            }
            
            Map<String, Photo> existingPhotoHashToPhoto = new HashMap<String, Photo>();
            if(album != null) {
                List<Photo> existingPhotos = album.getPhotos();
                for(Photo existingPhoto : existingPhotos) {
                    String hash = existingPhoto.getHash();
                    existingPhotoHashToPhoto.put(hash, existingPhoto);
                }    
            }
            
            JSONArray photoJSONArray = albumJSON.getJSONArray("photos");
            for(int j=0; j<photoJSONArray.length(); j++) {
                JSONObject photoJSON = photoJSONArray.getJSONObject(j);
                String hash = photoJSON.getString("hash");
                
                //a default album photo with an existing hash is useless
                //it will end up in the same auto created album
                //(im)possible caveat: photo timestamp changes without changing
                //hash
                if(album == null && existingAutoAlbumPhotoHashes.contains(hash)) {
                    continue;
                }
                
                if(!existingPhotoHashToPhoto.containsKey(hash)) {
                    Photo photo = new Photo();
                    existingPhotoHashToPhoto.put(hash, photo);
                }
                
                Photo photo = existingPhotoHashToPhoto.get(hash);
                
                int clickedOn = photoJSON.getInt("clickedOn");
                double latitude = photoJSON.getDouble("latitude");
                double longitude = photoJSON.getDouble("longitude");
                int width = photoJSON.getInt("width");
                int height = photoJSON.getInt("height");
                int thumbWidth = photoJSON.getInt("thumbWidth");
                int thumbHeight = photoJSON.getInt("thumbHeight");
                String pHash = photoJSON.getString("pHash");
                
                photo.setHash(hash);
                photo.setPHash(pHash);
                photo.setCreatedOn(clickedOn);
                photo.setWidth(width);
                photo.setHeight(height);
                photo.setThumbWidth(thumbWidth);
                photo.setThumbHeight(thumbHeight);
                
                
                
                if(latitude != 0 || longitude != 0) {
                    String key = String.format("%f-%f", latitude, longitude);
                    List<Photo> coordinatePhotos = coordinatesToPhoto.get(key);
                    if(coordinatePhotos == null) {
                        coordinatePhotos = new ArrayList<Photo>();
                        coordinatesToPhoto.put(key, coordinatePhotos);
                    }
                    coordinatePhotos.add(photo);
                }
                
                photo.updateURLs();
                
                if(album == null) {
                    defaultAlbumPhotos.add(photo);
                }
                else {
                    //Util.log("adding photo with id %s to album with id %s", photo.getId(), album.getId());
                    album.setOwnsPhoto(photo, PhotoSource.DEVICE);
                }
            }
        }
        
        Util.log("going to get location for %d photos", coordinatesToPhoto.size());
        
        
        List<Album> autoCreatedAlbums = new ArrayList<Album>();
        for(Album album : existingAlbums) {
            if(album.isAutoCreatedAlbum()) {
                autoCreatedAlbums.add(album);
            }
        }
        
        Set<Album> exisitingAutoAlbums = new HashSet<Album>(autoCreatedAlbums);
        
        PhotoOrganizer.organize(deviceID, autoCreatedAlbums, defaultAlbumPhotos);
        for(Album album: autoCreatedAlbums) {
            if(!exisitingAutoAlbums.contains(album)) {
                user.setOwnsAlbum(album);    
            }
        }
        
        long addNewPhotosEndTime = System.currentTimeMillis();
        Analytics.log(deviceID, "photo_organizer_add_new_photos_time", "time_taken", addNewPhotosEndTime - addNewPhotosStartTime);
        
        
        Object[] coordinateLocationResponse = coordinatesCallHandle.get();
        JSONArray locations = (JSONArray)coordinateLocationResponse[0];
        long timeTaken = (Long)coordinateLocationResponse[1];
        
        Analytics.log(deviceID, "photo_organizer_get_fb_location_for_coords", "time_taken", timeTaken);
        
        for(int i=0; i<locations.length(); i++) {
            if(!locations.isNull(i)) {
                JSONObject facebookLocation = locations.getJSONObject(i);
                double[] coords = photoCoordinates.get(i);
                String key = String.format("%f-%f", coords[0], coords[1]);
                List<Photo> photos = coordinatesToPhoto.get(key);
                Location location = Location.createOrUpdate(facebookLocation);
                //we might ignore some new photos so we need to check if photos
                //is null
                if(location != null && photos != null) {
                    for(Photo photo : photos) {
                        photo.setLocation(location);    
                    }       
                }
            }
        }
	}
	
	public static void removeDeletedPhotos(User user, JSONObject deleteDescriptor) throws JSONException, InterruptedException, ExecutionException {
	    Util.log("photo delete descriptor: %s: %s", user.getId(), deleteDescriptor.toString(3));
	    //TODO: check that the user has permission to delete!
	    Iterator<String> types = deleteDescriptor.keys();
	    
	    while(types.hasNext()) {
	        String type = types.next();
	        JSONArray ids = deleteDescriptor.getJSONArray(type);
	        for(int i=0; i<ids.length(); i++) {
	            String id = ids.getString(i);
	            if(type.equals("album")) {
	                Album album = Album.get(id);
	                if(album == null) {
	                    Util.log("attemp to delete non-existent album with id: %s", id);
	                }
	                else {
	                    album.delete();
	                }    
	            }
	            else if(type.equals("photo")) {
	                Photo photo = Photo.get(id);
	                if(photo == null) {
	                    Util.log("attemp to delete non-existent photo with id: %s", id);
	                }
	                else {
	                    photo.delete();
	                }    
	            }
	            else if(type.equals("user")) {
	                User u = User.get(id);
	                if(u == null) {
	                    Util.log("attemp to delete non-existent photo with id: %s", id);
	                }
	                else {
	                    u.delete();
	                }    
	            }
	            
	        }
	    }
	    
	    removeEmptyAutoCreatedAlbums(user);
	    
	}
	
	private static void removeEmptyAutoCreatedAlbums(User user) {
	    for(Album album : user.getAlbums()) {
	        if(album.isAutoCreatedAlbum() && album.isEmptyForUser(user)) {
	            Util.log("deleting auto created album: %s, %s", album.getName(), album.getDevicePersistentID());
	            album.delete();
	        }
	    }
	}
	
	public static void updateModifiedAlbums(User user, JSONArray modifyDescriptor) throws JSONException, InterruptedException, ExecutionException {
	    for(int i=0; i<modifyDescriptor.length(); i++) {
            JSONObject albumJSON = modifyDescriptor.getJSONObject(i);
            String deviceAlbumType = albumJSON.getString("type");
            if(deviceAlbumType.equals("default")) {
                continue;
            }
            
            String devicePersistentID = albumJSON.getString("devicePersistentId");
            
            String id = albumJSON.getString("id");
            Album album = Album.get(id);
            if(album == null) {
                Util.log("warning: updateModifiedAlbums for non-existing album id: %s", id);
                continue;
            }
            
            //updating only name and description for now
            String name = albumJSON.getString("name");
            String description = albumJSON.getString("description");
            
            album.setName(name);
            album.setDescription(description);
	    }
	}

}
