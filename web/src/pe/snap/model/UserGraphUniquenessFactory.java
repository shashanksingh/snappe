package pe.snap.model;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.traversal.TraversalBranch;
import org.neo4j.graphdb.traversal.UniquenessFactory;
import org.neo4j.graphdb.traversal.UniquenessFilter;

public class UserGraphUniquenessFactory implements UniquenessFactory {

    @Override
    public UniquenessFilter create(Object optionalParameter) {
        return new UserGraphUniquenessFilter();
    }
}


class UserGraphUniquenessFilter implements UniquenessFilter {
    private final Set<Long> incoming = new HashSet<Long>();
    private final Set<Long> outgoing = new HashSet<Long>();
    
    @Override
    public boolean checkFirst(TraversalBranch branch) {
        return check(branch);
    }

    @Override
    public boolean check(TraversalBranch branch) {
        Relationship r = branch.lastRelationship();
        if(r == null) {
            return true;
        }
        
        long id = r.getId();
        
        boolean isOutgoing = r.getEndNode().getId() == branch.endNode().getId();
        if(isOutgoing) {
            return outgoing.add(id); 
        }
        return incoming.add(id);
    }
    
}
