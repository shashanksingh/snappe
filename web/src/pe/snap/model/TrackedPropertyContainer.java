package pe.snap.model;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.PropertyContainer;

import pe.snap.infra.DBManager;
import pe.snap.util.Constants;
import pe.snap.util.Util;

public class TrackedPropertyContainer {
	
	public static final String ID_KEY = "id";
	private static final String PRIVATE_PROPERTY_PREFIX = "_";
	private static final String META_PROPERTY_SUFFIX = "__";
	private static final String MODIFICATION_TIME_SUFFIX = "__mod_on" + META_PROPERTY_SUFFIX;
	private static final String IS_DELETED_SUFFIX = "__is_deleted" + META_PROPERTY_SUFFIX;
	
	private static final Logger logger = Logger.getLogger(TrackedPropertyContainer.class);
	
	private PropertyContainer wrappedContainer;
	
	public TrackedPropertyContainer(PropertyContainer wrappedContainer) {
		this.wrappedContainer = wrappedContainer;
	}
	
	public String getID() {
	    return (String)this.getProperty(ID_KEY);
	}
	
	public void delete() {
	    if(this.hasProperty(IS_DELETED_SUFFIX)) {
	        this.touchSelf();
	        return;
	    }
		/*for(String propertyName : this.wrappedContainer.getPropertyKeys()) {
			this.wrappedContainer.removeProperty(propertyName);
		}*/
		this.setProperty(IS_DELETED_SUFFIX, true);
	}
	
	public void undelete() {
	    if(!this.hasProperty(IS_DELETED_SUFFIX)) {
	        return;
	    }
	    this.removeProperty(IS_DELETED_SUFFIX);
	}
	
	public static Node clone(Node n) throws InterruptedException, ExecutionException {
		TrackedPropertyContainer existingContainer = new TrackedPropertyContainer(n);
		
		Node newNode = DBManager.getOrCreateNode(UUID.randomUUID().toString());
		TrackedPropertyContainer newContainer = new TrackedPropertyContainer(newNode);
		//@TODO: respect deletes
    	for(String name : n.getPropertyKeys()) {
    		if(name.endsWith(META_PROPERTY_SUFFIX) || name.equals(ID_KEY)) {
    		    continue;
    		}
    		Object value = existingContainer.getProperty(name);
    		newContainer.setProperty(name, value);
    	}
    	
		return newNode;
	}
	
	protected String getId() {
	    return (String)this.getProperty(ID_KEY);	
	}
	
	protected PropertyContainer getWrappedContainer() {
		return this.wrappedContainer;
	}
	
	protected Object getProperty(String propertyName) {
		return this.wrappedContainer.getProperty(propertyName);
	}
	
	protected Object getProperty(String propertyName, Object defaultValue) {
		return this.wrappedContainer.getProperty(propertyName, defaultValue);
	}
	
	protected void setProperty(String propertyName, Object propertyValue) {
	    if(propertyName == null) {
	        Util.log("attempt to set property with null name");
	        new Exception().printStackTrace(System.err);
	        return;
	    }
	    if(propertyValue == null) {
	        Util.log("attempt to set property with null value: %s", propertyName);
            new Exception().printStackTrace(System.err);
            return;
	    }
	    if(propertyValue == JSONObject.NULL) {
            Util.log("attempt to set property with JSONObject.NULL: %s", propertyName);
            new Exception().printStackTrace(System.err);
            return;
        }
	    
		if(isMetaProperty(propertyName)) {
			this.wrappedContainer.setProperty(propertyName, propertyValue);
			this.touchSelf();
		}
		else {
			//Object oldVal = this.wrappedContainer.hasProperty(propertyName) ? this.wrappedContainer.getProperty(propertyName) : null;
			/*
			if(oldVal == null || !oldVal.equals(propertyValue)) {
				this.touchProperty(propertyName);	
			}*/
			this.touchProperty(propertyName);
			this.wrappedContainer.setProperty(propertyName, propertyValue);
			this.wrappedContainer.setProperty(propertyName + IS_DELETED_SUFFIX, false);	
		}
	}
	
	protected void removeProperty(String propertyName) {
		if(!this.hasProperty(propertyName)) return;
		
		this.wrappedContainer.removeProperty(propertyName);
		
		if(!isMetaProperty(propertyName)) {
			this.touchProperty(propertyName);
			this.wrappedContainer.setProperty(propertyName + IS_DELETED_SUFFIX, true);		
		}
		
		if(propertyName.equals(IS_DELETED_SUFFIX)) {
		    this.touchSelf();
		}
	}
	
	protected boolean hasProperty(String propertyName) {
		return this.wrappedContainer.hasProperty(propertyName);
	}
	
	protected boolean hasBeenModifiedSince(long timestamp) {
	    Object modTime = this.wrappedContainer.getProperty(MODIFICATION_TIME_SUFFIX);
	    if(modTime instanceof Long) {
	        return (Long)modTime >= timestamp;
	    }
	    else {
	        return (Integer)modTime >= timestamp;
	    }
	}
	
	protected long getLastModificationTime() {
	    long modificationTime = (Long)this.wrappedContainer.getProperty(MODIFICATION_TIME_SUFFIX, -1);
	    return modificationTime;
	}
	
	protected boolean hasPropertyBeenModifiedSince(String propertyName, long timestamp) {
	    Object modTime = this.wrappedContainer.getProperty(propertyName + MODIFICATION_TIME_SUFFIX, -1);
        if(modTime instanceof Long) {
            return (Long)modTime >= timestamp;
        }
        else {
            return (Integer)modTime >= timestamp;
        }
    }
	
	public boolean isDeleted() {
		return this.hasProperty(IS_DELETED_SUFFIX);
	}
	
	public long getCreationTime() {
		return (Long)this.getProperty(Constants.SNAPPE_CREATION_TIMESTAMP_KEY);
	}
	
	public boolean isNew(long startTime) {
	    return this.getCreationTime() >= startTime;
	}
	
	public JSONObject getChangeSet(long modifiedSince) throws JSONException {
		if(!this.hasBeenModifiedSince(modifiedSince)) {
			//an empty object for change set indicates no change
			return new JSONObject();
		}
		
		JSONObject thisChanges = new JSONObject();
    	for(String propertyName : this.wrappedContainer.getPropertyKeys()) {
    		if(propertyName.startsWith(PRIVATE_PROPERTY_PREFIX)) continue;
    		if(propertyName.endsWith(MODIFICATION_TIME_SUFFIX)) {
    			long value = (Long)this.wrappedContainer.getProperty(propertyName);
    			if(value > modifiedSince) {
    				String modifiedPropName = propertyName.substring(0, propertyName.length() - MODIFICATION_TIME_SUFFIX.length());
    				if(!modifiedPropName.isEmpty()) {
    					//the property might have been deleted, a null entry
    					//for a property in the changeset indicates that
    					//it has been deleted since last sync
    					Object propVal = this.hasProperty(propertyName) ? this.getProperty(modifiedPropName) : null; 
    				    thisChanges.put(modifiedPropName, propVal);
    				}
    			}
    		}
    	}
    	thisChanges.put(MODIFICATION_TIME_SUFFIX, this.wrappedContainer.getProperty(MODIFICATION_TIME_SUFFIX));
    	if(this.isDeleted()) {
    	    thisChanges.put(IS_DELETED_SUFFIX, true);
    	}
    	return thisChanges;
	}
	
	private void touchProperty(String propertyName) {
		this.wrappedContainer.setProperty(propertyName + MODIFICATION_TIME_SUFFIX, Util.getCurrentTimestamp());
		this.touchSelf();
	}
	
	private void touchSelf() {
		this.wrappedContainer.setProperty(MODIFICATION_TIME_SUFFIX, Util.getCurrentTimestamp());
	}
	
	private static boolean isMetaProperty(String propertyName) {
		return propertyName.endsWith(META_PROPERTY_SUFFIX);
	}


}
