package pe.snap.model;

import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.PropertyContainer;

public class BaseSnappeObject {
	
	protected TrackedPropertyContainer wrappedContainer;
	
	public BaseSnappeObject(TrackedPropertyContainer wrappedContainer) {
		this.wrappedContainer = wrappedContainer;
	}
	
	protected PropertyContainer getWrappedContainer() {
		return this.wrappedContainer.getWrappedContainer();
	}

	protected Object getProperty(String propertyName) {
		return this.wrappedContainer.getProperty(propertyName);
	}
	
	protected Object getProperty(String propertyName, Object defaultValue) {
		return this.wrappedContainer.getProperty(propertyName, defaultValue);
	}
	
	protected void setProperty(String propertyName, Object propertyValue) {
		this.wrappedContainer.setProperty(propertyName, propertyValue);
	}
	
	protected void removeProperty(String propertyName) {
        this.wrappedContainer.removeProperty(propertyName);
    }
	
	protected boolean hasProperty(String propertyName) {
		return this.wrappedContainer.hasProperty(propertyName);
	}
	
	public JSONObject getChangeSet(long modifiedSince) throws JSONException {
		return this.wrappedContainer.getChangeSet(modifiedSince);
	}
}
