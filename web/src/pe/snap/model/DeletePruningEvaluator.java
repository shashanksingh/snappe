package pe.snap.model;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;

public class DeletePruningEvaluator implements Evaluator {

    private static final DeletePruningEvaluator sharedInstance = new DeletePruningEvaluator();
    
    private DeletePruningEvaluator() {
    }
    
    public static DeletePruningEvaluator get() {
        return sharedInstance;
    }
    
     @Override
    public Evaluation evaluate(Path path) {
        Relationship r = path.lastRelationship();
        if(r != null) {
            TrackedPropertyContainer trackedR = new TrackedPropertyContainer(r);
            if(trackedR.isDeleted()) {
                return Evaluation.EXCLUDE_AND_PRUNE;
            }
            
            Node n = r.getEndNode();
            TrackedPropertyContainer trackedN = new TrackedPropertyContainer(n);
            if(trackedN.isDeleted()) {
                return Evaluation.EXCLUDE_AND_PRUNE;
            }    
        }
        
        return Evaluation.INCLUDE_AND_CONTINUE;
    }
    
}
