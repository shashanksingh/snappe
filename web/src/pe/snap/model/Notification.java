package pe.snap.model;

import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import pe.snap.util.Util;

public class Notification {
	private final String id;
	private final NotificationType type;
	private final long timestamp;
	private final JSONObject data;
	private boolean isPending;
	
	public enum NotificationType {
	    ALBUM_SHARED, ADDED_AS_UPLOADER, ALBUMS_SYNCED, ALBUM_PULLED, ALBUM_PUSHED, PHOTOS_PUSHED 	
	};
	
	public Notification(NotificationType type, JSONObject data) {
		this.id = UUID.randomUUID().toString();
	    this.type = type;
	    this.timestamp = Util.getCurrentTimestamp();
	    this.data = data;
	    this.isPending = true;
	}
	
	private Notification(String id, NotificationType type, long timestamp, boolean isPending, JSONObject data) {
		this.id = id;
	    this.type = type;
	    this.timestamp = timestamp;
	    this.data = data;
	    this.isPending = isPending;
	}
	
	public String getID() {
		return this.id;
	}
	
	public void markSeen() {
		this.isPending = false;
	}
	
	public boolean isPending() {
		return this.isPending;
	}
	
	public JSONObject toJSON() throws JSONException {
		JSONObject rv = new JSONObject();
		rv.put("id", this.id);
		rv.put("type", this.type == null ? "UNKNOWN" : this.type.toString());
		rv.put("timestamp", timestamp);
		rv.put("isPending", isPending);
		rv.put("data", this.data);
		return rv;
	}
	
	public static Notification fromJSON(JSONObject json) throws JSONException {
		String id = json.getString("id");
		NotificationType type = getType(json.getString("type"));
		long timestamp = json.getLong("timestamp");
		boolean isPending = json.getBoolean("isPending");
		JSONObject data = new JSONObject(json.getString("data"));
		
		return new Notification(id, type, timestamp, isPending, data);
	}
	
	private static NotificationType getType(String notificationType) {
		if(notificationType.equals("ALBUM_SHARED")) {
			return NotificationType.ALBUM_SHARED;
		}
		if(notificationType.equals("ADDED_AS_UPLOADER")) {
            return NotificationType.ADDED_AS_UPLOADER;
        }
		if(notificationType.equals("ALBUMS_SYNCED")) {
            return NotificationType.ALBUMS_SYNCED;
        }
		if(notificationType.equals("ALBUM_PULLED")) {
            return NotificationType.ALBUM_PULLED;
        }
		if(notificationType.equals("ALBUM_PUSHED")) {
            return NotificationType.ALBUM_PUSHED;
        }
		if(notificationType.equals("PHOTOS_PUSHED")) {
            return NotificationType.PHOTOS_PUSHED;
        }
		return null;
	}
}
