package pe.snap.model;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import pe.snap.model.SnappeNode.UniquenessType;
import pe.snap.model.SnappeRelationship.SnappeRelationshipType;
import pe.snap.model.User.PrivacyLevel;
import pe.snap.util.Constants;

public class Tag {
    private static final String NODE_TYPE = "tag";
    public static final String TIMESTAMP_PROPERTY_KEY = "timestamp";
    private static final int LOCATION_PRESENCE_ACCURACY_WINDOW_SECONDS = 6 * 60 * 60;
    
    private SnappeNode wrappedNode;
    
    public static Tag get(String id) throws InterruptedException, ExecutionException {
        SnappeNode wrappedNode = SnappeNode.get(id);
        if(wrappedNode == null) return null;
        
        Tag rv = new Tag(wrappedNode);
        return rv;
    }
    
    public static Tag get(Node node) {
        SnappeNode wrappedNode = SnappeNode.get(node);
        return new Tag(wrappedNode);
    }
    
    public static Tag createOrUpdate(User user, Location location, int timestamp) throws JSONException, IOException, InterruptedException, ExecutionException {
        Tag rv = user.getTagOnLocationOn(location, timestamp, LOCATION_PRESENCE_ACCURACY_WINDOW_SECONDS);
        if(rv == null) {
            SnappeNode wrappedNode = SnappeNode.create(NODE_TYPE);
            rv = new Tag(wrappedNode);
            rv.setTimestamp(timestamp);
            rv.setLocation(location);
            rv.addUser(user);
        }
        int averageTimestamp = rv.getTimestamp()/2 + timestamp/2;
        rv.setTimestamp(averageTimestamp);
        
        return rv;
    }
    
    private Tag(SnappeNode node) {
        this.wrappedNode = node;
    }
    
    public SnappeNode getWrappedNode() {
        return this.wrappedNode;
    }
    
    public String getId() {
        return (String)this.wrappedNode.getProperty("id");
    }
    
    public void addUser(User user) throws InterruptedException, ExecutionException {
        user.getWrappedNode().createRelationshipTo(this.wrappedNode, SnappeRelationshipType.USER_TAG, UniquenessType.NON_DIRECTIONAL);
    }
    
    public void setLocation(Location location) throws InterruptedException, ExecutionException {
        this.wrappedNode.createRelationshipTo(location.getWrappedNode(), SnappeRelationshipType.TAG_LOCATION, UniquenessType.NON_DIRECTIONAL);
    }
    
    public int getTimestamp() {
        return (Integer)this.wrappedNode.getProperty(TIMESTAMP_PROPERTY_KEY);
    }
    
    private void setTimestamp(int timestamp) {
        this.wrappedNode.setProperty(TIMESTAMP_PROPERTY_KEY, timestamp);
    }
    
    public JSONObject getChangeSet(long modifiedSince) throws JSONException {
        JSONObject rv = this.wrappedNode.getChangeSet(modifiedSince);
        if(rv.length() > 0) {
            rv.put(TrackedPropertyContainer.ID_KEY, this.getId());
            rv.put(Constants.SNAPPE_TYPE_KEY, this.getWrappedNode().getProperty(Constants.SNAPPE_TYPE_KEY));
        }
        return rv;
    }
    
    public JSONArray getChanges(long since, PrivacyLevel privacy) throws JSONException {
        JSONArray rv = new JSONArray();
        
        Node node = this.getWrappedNode().getWrappedNode();
        
        //gather changes from all locations (there could be deleted old locations)
        SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.TAG_LOCATION, Direction.OUTGOING, since, privacy);
        
        JSONObject selfChanges = this.getChangeSet(since);
        if(selfChanges.length() > 0) {
            rv.put(selfChanges);
        }
        
        return rv;
    }
    
    public void delete() {
        Node n = this.getWrappedNode().getWrappedNode();
        
        for(Relationship r : n.getRelationships()) {
            SnappeRelationship snappeR = SnappeRelationship.get(r);
            if(snappeR.isDeleted()) {
                continue;
            }
            
            if(r.isType(SnappeRelationshipType.USER_OWNS_ALBUM)) {
                Node otherNode = r.getOtherNode(n);
                Album userAlbum = Album.get(otherNode);
                userAlbum.delete();
                
            }
            else if(r.isType(SnappeRelationshipType.USER_TAG)) {
                Node otherNode = r.getOtherNode(n);
                Tag userTag = Tag.get(otherNode);
                userTag.delete();
            }
            
            snappeR.delete();
        }
        
        SnappeNode snappeN = SnappeNode.get(n);
        snappeN.delete();
    }
}
