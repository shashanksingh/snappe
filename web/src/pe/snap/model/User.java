package pe.snap.model;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.PropertyContainer;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;

import pe.snap.exceptions.APIException;
import pe.snap.model.SnappeNode.UniquenessType;
import pe.snap.model.SnappeRelationship.SnappeRelationshipType;
import pe.snap.services.FacebookTokenExtensionService;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.util.Constants;
import pe.snap.util.FacebookManager;
import pe.snap.util.TimingLogger;
import pe.snap.util.Util;


public class User {
    private static final String NODE_TYPE = "user";
    
	public static final String FB_ACCESS_TOKEN_KEY = "fb_access_token";
	public static final String FB_ACCESS_TOKEN_EXPIRATION_TIME_KEY = "fb_access_token_expiration_time";
	private static final String IS_REGISTERED_USER_KEY = "is_registered_user";
	private static final String LAST_SUCCESSFUL_FB_SYNC_TIME_KEY = "last_successful_fb_sync_key";
	
	
	//TODO: why not make this 60 days?
    private static final long EXTEND_MIN_INTERVAL_SECONDS = 1 * 86400;
	
	private static final int MAX_NOTIFICATIONS = 50;
	private static final String NOTIFICATION_PROPERTY_KEY = "_notifications";
	
	public static final String[] FQL_FIELDS = new String[]{
	    "uid", "name", "first_name", "middle_name", "last_name", "sex", "current_location", "locale"	
	};
	
	public static enum PrivacyLevel {
	    OWNER, FRIEND    
	};
	
	private static final Logger logger = Logger.getLogger(User.class);
	
	private SnappeNode wrappedNode;
	
	public static User get(String id) throws InterruptedException, ExecutionException {
		SnappeNode wrappedNode = SnappeNode.get(id);
		if(wrappedNode == null) {
			return null;
		}
		User rv = new User(wrappedNode);
        return rv;
	}
	
	public static User getOrCreate(String id) throws InterruptedException, ExecutionException {
	    SnappeNode wrappedNode = SnappeNode.getOrCreate(id, NODE_TYPE);
	    return new User(wrappedNode);
	}
	
	public static User get(Node node) {
		SnappeNode wrappedNode = SnappeNode.get(node);
		return new User(wrappedNode);
	}
	
	public static User getOrCreateFromFacebook(String id, String facebookAccessToken) throws UnsupportedEncodingException, JSONException, IOException, HTTPServiceException, InterruptedException, ExecutionException, APIException {
	    User rv = User.getOrCreate(id);
		if(!rv.hasFacebookToken()) {//a newly created node will not have access token
			JSONObject userJSON = FacebookManager.getUser(id, facebookAccessToken);
			rv = User.createOrUpdate(userJSON);
			
			if(facebookAccessToken != null) {
			    rv.setIsRegisteredUser();    
			}
		}
		rv.setFacebookAccessToken(facebookAccessToken);
		return rv;
	}
	
	public static User createOrUpdate(JSONObject fbJSON) throws JSONException, InterruptedException, ExecutionException {
		SnappeNode wrappedNode = SnappeNode.getOrCreate(fbJSON.getString("uid"), NODE_TYPE);
		User rv = new User(wrappedNode);
		
		for(String fqlField : FQL_FIELDS) {
			
			if(fqlField.equals("current_location")) {
				JSONObject locationJSON = fbJSON.optJSONObject(fqlField);
				if(locationJSON != null) {
					Location loc = Location.createOrUpdate(locationJSON);
					if(loc != null) {
						rv.setCurrentLocation(loc);
					}		
				}
			}
			else {
				wrappedNode.setProperty(fqlField, fbJSON.get(fqlField));
			}
		}
		
		if(!wrappedNode.hasProperty(NOTIFICATION_PROPERTY_KEY)) {
			wrappedNode.setProperty(NOTIFICATION_PROPERTY_KEY, new JSONArray().toString());	
		}
		
		return rv;
		
	}
	
	private User(SnappeNode node) {
	    this.wrappedNode = node;
	}
	
	public SnappeNode getWrappedNode() {
		return this.wrappedNode;
	}
	
	public String getId() {
		return this.wrappedNode.getId();
	}
	
	public boolean isRegisteredUser() {
		return (Boolean)this.wrappedNode.getProperty(IS_REGISTERED_USER_KEY, false);
	}
	
	public void setIsRegisteredUser() {
	    this.wrappedNode.setProperty(IS_REGISTERED_USER_KEY, true);
	}
	
	public int getLastSuccessfulFacebookSyncTime() {
	    return (Integer)this.wrappedNode.getProperty(LAST_SUCCESSFUL_FB_SYNC_TIME_KEY, 0);
	}
	
	public void setLastSuccessfulFacebookSyncTime(int syncInitiationTime) {
        this.wrappedNode.setProperty(LAST_SUCCESSFUL_FB_SYNC_TIME_KEY, syncInitiationTime);
    }
	
	public String getFirstName() {
		return (String)this.wrappedNode.getProperty("first_name");
	}
	
	public String getLastName() {
		return (String)this.wrappedNode.getProperty("last_name");
	}
	
	public String getName() {
		return (String)this.wrappedNode.getProperty("name");
	}
	
	public String getLocale() {
		return (String)this.wrappedNode.getProperty("locale");
	}
	
	public String getFacebookAccessToken() {
		return (String)this.wrappedNode.getProperty(FB_ACCESS_TOKEN_KEY);
	}
		
	public boolean hasFacebookToken() {
		return this.wrappedNode.hasProperty(FB_ACCESS_TOKEN_KEY);
	}
	
	public void setFacebookAccessToken(String facebookAccessToken) {
	    if(this.getAccessTokenExpirationTime() > Util.getCurrentTimestamp()) {
            Util.log("user %s already has a non-expired access token, current expiration time: %s", this.getId(), this.getAccessTokenExpirationTime());
            return;
        }
	    //set short lived access token in case extended token fetch fails
		this.wrappedNode.setProperty(FB_ACCESS_TOKEN_KEY, facebookAccessToken);
		FacebookTokenExtensionService.submit(this, facebookAccessToken);
	}
	
	//meant to be used only by FacebookTokenExtensionService
	public void setExtendedFacebookAccessToken(String extendedFacebookAccessToken, long expirationTime) {
	    this.wrappedNode.setProperty(FB_ACCESS_TOKEN_KEY, extendedFacebookAccessToken);
	    this.setAccessTokenExpirationTime(expirationTime);
	}
	
	public void setAccessTokenExpirationTime(long expirationTime) {
	    this.wrappedNode.wrappedContainer.setProperty(FB_ACCESS_TOKEN_EXPIRATION_TIME_KEY, expirationTime);
	}
	
	public long getAccessTokenExpirationTime() {
        return (Long)this.wrappedNode.wrappedContainer.getProperty(FB_ACCESS_TOKEN_EXPIRATION_TIME_KEY, -1L);
    }
	
	public void unsetAccessToken() {
	    this.setAccessTokenExpirationTime(-1L);
	    this.wrappedNode.removeProperty(FB_ACCESS_TOKEN_KEY);
	}
	
	public void setIsFriendsWith(User friend) throws InterruptedException, ExecutionException {
		this.wrappedNode.createRelationshipTo(friend.wrappedNode, SnappeRelationshipType.FRIEND, UniquenessType.NON_DIRECTIONAL);
	}
	
	public void setOwnsAlbum(Album album) throws InterruptedException, ExecutionException {
		this.wrappedNode.createRelationshipTo(album.getWrappedNode(), SnappeRelationshipType.USER_OWNS_ALBUM, UniquenessType.NON_DIRECTIONAL);
	}
	
	public Location getCurrentLocation() {
	    for(Relationship r : this.wrappedNode.getWrappedNode().getRelationships(SnappeRelationshipType.USER_CURRENT_LOCATION)) {
	        return Location.get(r.getEndNode());
	    }
	    return null;
	}
	
	public void setCurrentLocation(Location location) throws InterruptedException, ExecutionException {
		this.wrappedNode.createRelationshipTo(location.getWrappedNode(), SnappeRelationshipType.USER_CURRENT_LOCATION, UniquenessType.NON_DIRECTIONAL);
	}
	
    public Tag getTagOnLocationOn(final Location location, final int timestamp, final int timeWindow) {
        TraversalDescription traversal = Traversal.description()
                .depthFirst()
                .relationships(SnappeRelationshipType.USER_TAG, Direction.OUTGOING)
                .relationships(SnappeRelationshipType.TAG_LOCATION, Direction.OUTGOING)
                .evaluator(Evaluators.atDepth(2))
                .evaluator(DeletePruningEvaluator.get())
                .evaluator(new Evaluator() {
                    @Override
                    public Evaluation evaluate(Path path) {
                        if (path.length() == 2) {
                            Location taggedLocation = Location.get(path.endNode());
                            if (taggedLocation.getId().equals(location.getId())) {
                                if(timestamp < 0) {
                                    return Evaluation.INCLUDE_AND_PRUNE; 
                                }
                                
                                Tag tag = Tag.get(Util.getLastToLastNode(path));
                                if(Math.abs(timestamp - tag.getTimestamp()) <= timeWindow) {
                                    return Evaluation.INCLUDE_AND_PRUNE;    
                                }
                                
                            }
                            return Evaluation.EXCLUDE_AND_PRUNE;
                        }
                        return Evaluation.INCLUDE_AND_CONTINUE;
                    }
                });
        Traverser traverser = traversal.traverse(this.wrappedNode.getWrappedNode());
        if(!traverser.iterator().hasNext()) {
            return null;
        }
        
        Path path = traverser.iterator().next();
        Node tagNode = path.lastRelationship().getStartNode();
        
        String nodeType = Util.getNodeType(tagNode);
        if(!"tag".equals(nodeType)) {
            throw new AssertionError("incorrect node type for Tag node: " + nodeType);
        }
        return Tag.get(path.lastRelationship().getStartNode());
    }
	
	public JSONObject getChangeSet(long modifiedSince) throws JSONException {
	    JSONObject rv = this.wrappedNode.getChangeSet(modifiedSince);
        if(rv.length() > 0) {
            rv.put(TrackedPropertyContainer.ID_KEY, this.getId());
            rv.put(Constants.SNAPPE_TYPE_KEY, this.getWrappedNode().getProperty(Constants.SNAPPE_TYPE_KEY));
        }
        return rv;
	}
	
	private LinkedList<Notification> getNotifications() throws JSONException {
		JSONArray notificationJSONArray = new JSONArray((String)this.wrappedNode.getProperty(NOTIFICATION_PROPERTY_KEY)); 
		LinkedList<Notification> notifications = new LinkedList<Notification>();
		for(int i=0; i<notificationJSONArray.length(); i++) {
			JSONObject notificationJSON = notificationJSONArray.getJSONObject(i);
			notifications.add(Notification.fromJSON(notificationJSON));
		}
	    return notifications;
	}
	
	private void setNotifications(LinkedList<Notification> notifications) throws JSONException {
		trimNotifications(notifications);
		JSONArray notificationJSONArray = new JSONArray();
		for(Notification notification : notifications) {
			notificationJSONArray.put(notification.toJSON());
		}
		this.wrappedNode.setProperty(NOTIFICATION_PROPERTY_KEY, notificationJSONArray.toString());
	}
	
	private void trimNotifications(LinkedList<Notification> notifications) {
		while(notifications.size() >= MAX_NOTIFICATIONS) {
			notifications.remove();
		}
	}
	
	public void addNotification(Notification notification) throws JSONException {
		LinkedList<Notification> notifications = getNotifications();
		notifications.add(notification);
		setNotifications(notifications);
		if(!this.isRegisteredUser()) {
			//send facebook message to non registered user
		}
		
		try {
			Util.log("sent notification to " + getName() + "\n" + notification.toJSON().toString(3));	
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public List<Notification> getPendingNotifications() throws JSONException {
		LinkedList<Notification> notifications = getNotifications();
		List<Notification> rv = new ArrayList<Notification>();
		for(int i=notifications.size() - 1; i>=0; i--) {
			Notification notification = notifications.get(i);
			if(!notification.isPending()) break;
			rv.add(notification);
		}
		return rv;
	}
	
	public void clearNotifications(Set<String> notificationIDs) throws JSONException {
		LinkedList<Notification> notifications = getNotifications();
		for(Notification notification : notifications) {
			if(notificationIDs != null && notificationIDs.contains(notification.getID())) {
				notification.markSeen();	
			}
		}
		setNotifications(notifications);
	}
	
	public void clearAllNotifications() throws JSONException {
		clearNotifications(null);
	}
	
	public boolean isFriendsWith(User possibleFriend) {
		for(User friend : this.getFriends()) {
		    if(friend.getId().equals(possibleFriend.getId())) {
		        return true;
		    }
		}
		return false;
		
	}
	
	public List<User> getFriends() {
	    List<User> rv = new ArrayList<User>();
	    Iterable<Relationship> rels = this.getWrappedNode().getWrappedNode().getRelationships(SnappeRelationshipType.FRIEND);
	    for(Relationship rel : rels) {
	        User u1 = User.get(rel.getStartNode());
	        User u2 = User.get(rel.getEndNode());
	        
	        if(u1.getId().equals(this.getId())) {
	            rv.add(u2);    
	        }
	        else {
	            rv.add(u1);
	        }
	        
	    }
	    return rv;
	}
	
	public JSONArray getGraphChanges(final int modifiedSince) throws JSONException {
	    long startTime = System.currentTimeMillis();
	    Util.log("getGraphChanges: %d", modifiedSince);
	    
	    final TimingLogger tl = new TimingLogger();
	    /*
	    UserGraphChangeEvaluator evaluator = new UserGraphChangeEvaluator(this, modifiedSince);
	    
		TraversalDescription traversal = Traversal.description().depthFirst()
		    .relationships(SnappeRelationshipType.FRIEND)
		    .relationships(SnappeRelationshipType.USER_OWNS_ALBUM)
		    .relationships(SnappeRelationshipType.ALBUM_SHARED_PUSHED)
		    .relationships(SnappeRelationshipType.CAN_UPLOAD_TO_ALBUM)
		    .relationships(SnappeRelationshipType.USER_CURRENT_LOCATION, Direction.OUTGOING)
		    .relationships(SnappeRelationshipType.USER_TAG, Direction.OUTGOING)
		    .relationships(SnappeRelationshipType.PHOTO_IN_ALBUM, Direction.INCOMING)
		    .relationships(SnappeRelationshipType.PHOTO_LOCATION, Direction.OUTGOING)
		    .relationships(SnappeRelationshipType.TAG_LOCATION, Direction.OUTGOING)
		    .uniqueness(new UserGraphUniquenessFactory())
		    .evaluator(evaluator);
		
		Traverser traverser = traversal.traverse(this.wrappedNode.getWrappedNode());
		
		tl.l();
        JSONArray rv = getChangesFromTraverse(traverser, modifiedSince);
        tl.l();
        */
	    tl.l();
	    JSONArray rv = this.getChanges(modifiedSince, PrivacyLevel.OWNER);
	    tl.l();
	    //de-dup by choosing the biggest changeset for each id
	    HashMap<String, JSONObject> idToChangeSet = new HashMap<String, JSONObject>();
	    HashMap<String, Integer> idToMaxChangeSetSize = new HashMap<String, Integer>();
	    
	    for(int i=0; i<rv.length(); i++) {
	        JSONObject change = rv.getJSONObject(i);
	        
	        String id = change.getString(TrackedPropertyContainer.ID_KEY);
	        
	        Integer currentMaxSize = idToMaxChangeSetSize.get(id);
	        if(currentMaxSize == null || currentMaxSize < change.length()) {
	            idToChangeSet.put(id, change);
	            idToMaxChangeSetSize.put(id, change.length());
	        }
	    }
	    tl.l();
	    rv = new JSONArray();
	    for(JSONObject change : idToChangeSet.values()) {
	        rv.put(change);
	    }
	    tl.l();
        Util.log("fastest changes count: %d", rv.length());
        
        Util.log("getGraphChanges time: %dms", System.currentTimeMillis() - startTime);
		return rv;
	}
	
	public JSONArray getChanges(long since, PrivacyLevel privacy) throws JSONException {
	    JSONArray rv = new JSONArray();
	    
	    Node node = this.getWrappedNode().getWrappedNode();
	    
	    if(privacy == PrivacyLevel.OWNER) {
	        //gather changes all friends are willing to give
	        SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.FRIEND, Direction.BOTH, since, PrivacyLevel.FRIEND);
	        //gather all changes from albums shared with you
	        SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.ALBUM_SHARED_PUSHED, Direction.INCOMING, since, PrivacyLevel.FRIEND);
	        //gather all changes from albums you are allowed to upload to
	        SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.CAN_UPLOAD_TO_ALBUM, Direction.OUTGOING, since, PrivacyLevel.FRIEND);
	    }
	    
	    //gather changes all own albums are willing to give
	    SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.USER_OWNS_ALBUM, Direction.OUTGOING, since, privacy);
	    
	    //get the user's current location
	    SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.USER_CURRENT_LOCATION, Direction.OUTGOING, since, privacy);
	    
	    //gather all user tags
	    SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.USER_TAG, Direction.OUTGOING, since, privacy);
	    
	    JSONObject selfChanges = this.getChangeSet(since);
	    if(selfChanges.length() > 0) {
	        rv.put(selfChanges);
	    }
	    
	    return rv;
	}
	
	private JSONArray getChangesFromTraverse(Traverser traverser, int modifiedSince) throws JSONException {
	    Set<String> containersWithExistingPathTo = new HashSet<String>();
	    Map<String, TrackedPropertyContainer> idToModifiedContainer = new HashMap<String, TrackedPropertyContainer>();
	    
	    Iterator<Path> pathIterator = traverser.iterator();
	    while(pathIterator.hasNext()) {
	        Path path = pathIterator.next();
	        
            Iterator<PropertyContainer> it = path.iterator();
            
            boolean pathIsNew = false;
            
            while(it.hasNext()) {
                PropertyContainer pc = it.next();
                
                TrackedPropertyContainer tpc = new TrackedPropertyContainer(pc);
                
                String id = tpc.getId();
                long modTime = tpc.getLastModificationTime();
                
                if(!pathIsNew) {
                    boolean modified = modTime >= modifiedSince;
                    if(modified) {
                        idToModifiedContainer.put(id, tpc);
                    }
                    else {
                        //there is an existing path to this so it was seen
                        //before and it has not changed since, so NOOP
                    }
                    containersWithExistingPathTo.add(id);
                }
                else if(!containersWithExistingPathTo.contains(id)) {
                    //this path is new and there is no other existing path 
                    idToModifiedContainer.put(id, tpc);
                }
                
                if(pc instanceof Relationship) {
                    long creationTime = tpc.getCreationTime();
                    
                    if(!pathIsNew && creationTime >= modifiedSince) {
                        pathIsNew = true;
                    }
                }
            }
	    }
	    
        JSONArray rv = new JSONArray();
        
        Util.log("idToModifiedContainer length: %d", idToModifiedContainer.size());
        
        for(String id : idToModifiedContainer.keySet()) {
            
            TrackedPropertyContainer tpc = idToModifiedContainer.get(id);
            //if there is an existing path to the container get only those
            //changes that happened since we last checked, get all the changes
            //since big bang otherwise
            long getModSince = containersWithExistingPathTo.contains(id) ? modifiedSince : 0;
            
            JSONObject changes = Util.getChangeSet(tpc.getWrappedContainer(), getModSince);
            if(changes.length() > 0) {
                //TODO: for rels check if either node is new
                //check old code
                //do we even need that check?
                
                String nodeType = (String)tpc.getProperty(Constants.SNAPPE_TYPE_KEY);
                
                changes.put(TrackedPropertyContainer.ID_KEY, id);
                changes.put(Constants.SNAPPE_TYPE_KEY, nodeType);
                
                rv.put(changes);
            }
        }
        
        return rv;
	}
	
	public List<Album> getAlbums() {
		Traverser traverser = Traversal.description().breadthFirst()
	        .relationships(SnappeRelationshipType.USER_OWNS_ALBUM, Direction.OUTGOING)
	        .evaluator(Evaluators.atDepth(1))
	        .evaluator(DeletePruningEvaluator.get())
	        .traverse(this.wrappedNode.getWrappedNode());
		
		List<Album> rv = new ArrayList<Album>();
		for(Node albumNode : traverser.nodes()) {
			rv.add(Album.get(albumNode));
		}
		return rv;
	}
	
	public List<Photo> getPhotos() {
		Traverser traverser = Traversal.description().depthFirst()
	        .relationships(SnappeRelationshipType.USER_OWNS_ALBUM)
	        .relationships(SnappeRelationshipType.PHOTO_IN_ALBUM)
	        .uniqueness(Uniqueness.NODE_GLOBAL)
		    .uniqueness(Uniqueness.RELATIONSHIP_GLOBAL)
		    .evaluator(Evaluators.atDepth(2))
		    .evaluator(DeletePruningEvaluator.get())
		    .traverse(this.wrappedNode.getWrappedNode());
		
		List<Photo> rv = new ArrayList<Photo>();
		for(Node photoNode : traverser.nodes()) {
			rv.add(Photo.get(photoNode));
		}
		
		return rv;
	}
	
    public boolean ownsAlbum(Album album) {
        return album.getOwner().getId().equals(this.getId());
	}
    
    public boolean canUploadToAlbum(Album album) {
        if(this.ownsAlbum(album)) {
            //can't uplaod to one's own device albums
            //may be in the future if we handle multi device
            //scenarios
            return album.isFacebookAlbum();
        }
        return album.isUserAllowedUploadAccess(this);
    }
    
    public boolean canAddPhotosToAlbum(Album album) {
        return this.ownsAlbum(album) || album.isUserAllowedUploadAccess(this);
    }
    
    public boolean canAllowUploadToAlbum(Album album) {
        if(this.ownsAlbum(album)) {
            //can't uplaod to one's own device albums
            //may be in the future if we handle multi device
            //scenarios
            return true;
        }
        return album.isUserAllowedUploadAccess(this);
    }
    
    public void delete() {
        Node n = this.getWrappedNode().getWrappedNode();
        
        for(Relationship r : n.getRelationships()) {
            SnappeRelationship snappeR = SnappeRelationship.get(r);
            if(snappeR.isDeleted()) {
                continue;
            }
            
            if(r.isType(SnappeRelationshipType.USER_OWNS_ALBUM)) {
                Node otherNode = r.getOtherNode(n);
                Album userAlbum = Album.get(otherNode);
                userAlbum.delete();
                
            }
            else if(r.isType(SnappeRelationshipType.USER_TAG)) {
                Node otherNode = r.getOtherNode(n);
                Tag userTag = Tag.get(otherNode);
                userTag.delete();
            }
            
            
            snappeR.delete();
        }
        
        SnappeNode snappeN = SnappeNode.get(n);
        snappeN.delete();
    }
    
    public boolean is(User u) {
        return this.wrappedNode.is(u.wrappedNode);
    }
    
    public boolean isTester() {
        String id = this.getId();
        return id.equals("654175641") || id.equals("100004431692208") || id.equals("100005398141386");
    }
	
}