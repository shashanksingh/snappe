package pe.snap.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import pe.snap.infra.DBManager;
import pe.snap.model.SnappeRelationship.SnappeRelationshipType;
import pe.snap.model.User.PrivacyLevel;
import pe.snap.util.Constants;
import pe.snap.util.Util;

public class SnappeNode extends BaseSnappeObject {
	
	private static final Logger logger = Logger.getLogger(SnappeNode.class);
	
	public enum UniquenessType {
	    NONE, DIRECTIONAL, NON_DIRECTIONAL    
	};
    
    public static SnappeNode getOrCreate(String id, String type) throws InterruptedException, ExecutionException {
		return get(id, type);
	}
    
    public static SnappeNode get(String id) throws InterruptedException, ExecutionException {
    	return get(id, null);
    }
    
    public static List<SnappeNode> getByNonUniqueProperty(String property, Object value) {
    	Iterator<Node> nodes = DBManager.getDB().index().getNodeAutoIndexer().getAutoIndex().get(property, value);
    	List<SnappeNode> rv = new ArrayList<SnappeNode>();
    	while(nodes.hasNext()) {
    		TrackedPropertyContainer wrapped = new TrackedPropertyContainer(nodes.next());
        	rv.add(new SnappeNode(wrapped));
    	}
    	return rv;
    }
    
    public static SnappeNode create(String type) throws InterruptedException, ExecutionException {
    	return get(UUID.randomUUID().toString(), type);
    }
    
    public static SnappeNode get(Node node) {
    	TrackedPropertyContainer wrapped = new TrackedPropertyContainer(node);
    	return new SnappeNode(wrapped);
    }
    
    private static SnappeNode get(String id, String type) throws InterruptedException, ExecutionException {
		Node n = DBManager.getDB().index().getNodeAutoIndexer().getAutoIndex().get(TrackedPropertyContainer.ID_KEY, id).getSingle();
		if(n == null && type == null) return null;
		
		if(n == null) {
            n = DBManager.getOrCreateNode(id);
        }
        
        TrackedPropertyContainer wrapped = new TrackedPropertyContainer(n);
        if(type != null) {
            wrapped.setProperty(Constants.SNAPPE_TYPE_KEY, type);   
        }
        SnappeNode rv = new SnappeNode(wrapped);
        
        return rv;
	}
    
    private SnappeNode(TrackedPropertyContainer wrappedNode) {
		super(wrappedNode);
	}
    
    public SnappeNode cloneNode() throws InterruptedException, ExecutionException {
    	Node newNode = TrackedPropertyContainer.clone(this.getWrappedNode());
		SnappeNode rv = SnappeNode.get(newNode);
		return rv;
	}
    
    public String getId() {
    	return (String)this.wrappedContainer.getId();
    }
    
    public String getType() {
    	return (String)getProperty(Constants.SNAPPE_TYPE_KEY);
    }

	public Node getWrappedNode() {
		return (Node)super.getWrappedContainer();
	}
	
	public SnappeRelationship createRelationshipTo(SnappeNode otherNode, SnappeRelationshipType type, UniquenessType uniqueness) throws InterruptedException, ExecutionException {
	    SnappeRelationship rv = SnappeRelationship.create(this, otherNode, type, uniqueness);
	    if(rv.isDeleted()) {
	        rv.undelete();
	    }
	    return rv;
	}
	
	public JSONObject getChangeSet(long modifiedSince) throws JSONException {
		return this.wrappedContainer.getChangeSet(modifiedSince);
	}
	
	public void delete() {
	    this.wrappedContainer.delete();
	}
	
	public void undelete() {
        this.wrappedContainer.undelete();
    }
	
	public boolean isDeleted() {
	    return this.wrappedContainer.isDeleted();
	}
	
	protected boolean hasRelationshipOfTypeWithNode(SnappeRelationshipType relType, Node node) {
        SnappeRelationship r = getRelationshipWithNodeOfType(node, relType);
        return r != null && !r.isDeleted();
    }
    
    protected void deleteRelationWithNodeOfType(Node node, SnappeRelationshipType relType) {
        SnappeRelationship r = getRelationshipWithNodeOfType(node, relType);
        if(r != null) {
            r.delete();
        }
    }
    
    protected SnappeRelationship getRelationshipWithNodeOfType(Node node, SnappeRelationshipType relType) {
        Node n = this.getWrappedNode();
        for(Relationship r : n.getRelationships(relType)) {
            Node otherNode = r.getOtherNode(n);
            if(Util.getNodeId(node).equals(Util.getNodeId(otherNode))){
                SnappeRelationship snappeR = SnappeRelationship.get(r);
                return snappeR;
            }
        }
        return null;
    }
    
    protected boolean is(SnappeNode n) {
        return this.getId().equals(n.getId());
    }
    
    public static void gatherChanges(JSONArray changeBucket, Node root, SnappeRelationshipType relType, Direction direction, long since, PrivacyLevel privacy) throws JSONException {
        
        Iterable<Relationship> rels;
        rels = root.getRelationships(relType, direction);
        
        for(Relationship rel : rels) {
            
            long modificationsSince = since;
            TrackedPropertyContainer tpcR = new TrackedPropertyContainer(rel);
            if(tpcR.isNew(since)) {
                modificationsSince = 0;
            }
            
            JSONArray changes = null;
            if(relType == SnappeRelationshipType.FRIEND) {
                User u1 = User.get(rel.getStartNode());
                User u2 = User.get(rel.getEndNode());
                
                User friend = u1.getId().equals(Util.getNodeId(root)) ? u2 : u1;
                
                changes = friend.getChanges(modificationsSince, privacy);
                
            }
            else if(relType == SnappeRelationshipType.ALBUM_SHARED_PUSHED) {
                Album album = Album.get(rel.getStartNode());
                if(direction == Direction.INCOMING) {//looking from user
                    changes = album.getChanges(modificationsSince, privacy);    
                }
                else {
                    changes = new JSONArray();
                }
            }
            else if(relType == SnappeRelationshipType.CAN_UPLOAD_TO_ALBUM) {
                if(direction == Direction.OUTGOING) {//looking from user
                    Album album = Album.get(rel.getEndNode());
                    changes = album.getChanges(modificationsSince, privacy);
                }
                else {
                    changes = new JSONArray();
                }
            }
            else if(relType == SnappeRelationshipType.USER_OWNS_ALBUM) {
                if(direction == Direction.OUTGOING) {
                    Album album = Album.get(rel.getEndNode());
                    if(privacy == PrivacyLevel.OWNER || album.isFacebookAlbum()) {
                        changes = album.getChanges(modificationsSince, privacy);    
                    }    
                }
                else {//looking for album, user should already be known, just get the rel
                    changes = new JSONArray();
                }
            }
            else if(relType == SnappeRelationshipType.PHOTO_IN_ALBUM) {
                Photo photo = Photo.get(rel.getStartNode());
                if(privacy == PrivacyLevel.OWNER) {
                    changes = photo.getChanges(modificationsSince, privacy);
                }
                else {
                    boolean isPrivate = photo.isPrivate();
                    
                    boolean hasPrivacyChanged = photo.hasPrivacyChangedSince(modificationsSince);
                    if(!hasPrivacyChanged) {//this means that the property has not been touched since
                        if(!isPrivate) {
                            changes = photo.getChanges(modificationsSince, privacy);
                        }
                    }
                    else {
                        if(!isPrivate) {
                            modificationsSince = 0;
                            changes = photo.getChanges(modificationsSince, privacy);
                        }
                        else {
                            JSONObject deleteDescriptor = new JSONObject();
                            deleteDescriptor.put("__is_deleted__", true);
                            deleteDescriptor.put(TrackedPropertyContainer.ID_KEY, photo.getId());
                            deleteDescriptor.put(Constants.SNAPPE_TYPE_KEY, photo.getWrappedNode().getProperty(Constants.SNAPPE_TYPE_KEY));
                            
                            changes = new JSONArray();
                            changes.put(deleteDescriptor);
                        }    
                    }
                    
                }
            }
            else if(relType == SnappeRelationshipType.USER_TAG) {
                Tag tag = Tag.get(rel.getEndNode());
                changes = tag.getChanges(modificationsSince, privacy);
            }
            else if(relType == SnappeRelationshipType.TAG_LOCATION 
                    || relType == SnappeRelationshipType.PHOTO_LOCATION
                    || relType == SnappeRelationshipType.USER_CURRENT_LOCATION
                    || relType == SnappeRelationshipType.ALBUM_LOCATION)
            {
                Location location = Location.get(rel.getEndNode());
                changes = new JSONArray();
                
                JSONObject c = location.getChangeSet(modificationsSince);
                if(c.length() > 0) {
                    changes.put(c);    
                }
            }
            else {
                Util.logAdminWarning("unknown rel type when getting user graph changes " + relType);
            }
            
            if(changes != null) {
                for(int i=0; i<changes.length(); i++) {
                    JSONObject change = changes.getJSONObject(i);
                    changeBucket.put(change);
                }
                
                JSONObject relChange = SnappeRelationship.get(rel).getChangeSet(modificationsSince);
                if(relChange.length() != 0) {
                    changeBucket.put(relChange);    
                }
            }
            
        }
    }

}
