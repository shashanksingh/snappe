package pe.snap.model;

import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.Node;

import pe.snap.util.Constants;

public class Location {
	private static final String NODE_TYPE = "location";
    private SnappeNode wrappedNode;
    
    public static final String[] FQL_FIELDS = new String[]{
	    "id", "zip", "name", "city", "state", "country", "latitude", "longitude"
	};
    
    private static final Logger logger = Logger.getLogger(Location.class);
	
	public static Location get(String id) throws InterruptedException, ExecutionException {
		SnappeNode wrappedNode = SnappeNode.get(id);
		if(wrappedNode == null) return null;
		
		Location rv = new Location(wrappedNode);
        return rv;
	}
	
	public static Location get(Node node) {
        SnappeNode wrappedNode = SnappeNode.get(node);
        return new Location(wrappedNode);
    }
	
	public static Location createOrUpdate(JSONObject fbJSON) throws JSONException, InterruptedException, ExecutionException {
	    if(fbJSON == null) {
	        logger.warn("null fbJSON passed to Location.createOrUpdate");
	        return null;
	    }
		String id = fbJSON.optString("id", null);
		if(id == null) {
		    logger.warn(String.format("missing id in location FB json: %s", fbJSON.toString()));
		    return null;
		}
		
		//id = "flo_" + id;
		
		SnappeNode wrappedNode = SnappeNode.getOrCreate(id, NODE_TYPE);
		for(String fqlField : FQL_FIELDS) {
			Object value = fqlField.endsWith("id") ? fbJSON.optString(fqlField, null) : fbJSON.opt(fqlField);
			if(value != null) {
				wrappedNode.setProperty(fqlField, value);	
			}
		}
		
		return new Location(wrappedNode);
	}
	
	private Location(SnappeNode node) {
	    this.wrappedNode = node;
	}
	
	public SnappeNode getWrappedNode() {
		return this.wrappedNode;
	}
	
	public String getId() {
		return (String)this.wrappedNode.getProperty("id");
	}
	
	public double getLatitude() {
	    return (Double)this.wrappedNode.getProperty("latitude");
	}
	
	public double getLongitude() {
        return (Double)this.wrappedNode.getProperty("longitude");
    }
	
	public String getName() {
	    return (String)this.wrappedNode.getProperty("name", null);
	}
	
	public JSONObject getChangeSet(long modifiedSince) throws JSONException {
	    JSONObject rv = this.wrappedNode.getChangeSet(modifiedSince);
        if(rv.length() > 0) {
            rv.put(TrackedPropertyContainer.ID_KEY, this.getId());
            rv.put(Constants.SNAPPE_TYPE_KEY, this.getWrappedNode().getProperty(Constants.SNAPPE_TYPE_KEY));
        }
        return rv;
    }
}
