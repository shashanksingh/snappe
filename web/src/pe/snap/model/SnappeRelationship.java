package pe.snap.model;

import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;


import pe.snap.infra.DBManager;
import pe.snap.model.SnappeNode.UniquenessType;
import pe.snap.util.Constants;
import pe.snap.util.Util;

public class SnappeRelationship extends BaseSnappeObject {
	private static final String SNAPPE_TYPE = "relationship";
	
	public enum SnappeRelationshipType implements RelationshipType {
	    FRIEND, USER_OWNS_ALBUM, PHOTO_IN_ALBUM, 
	    USER_CURRENT_LOCATION, PHOTO_LOCATION, ALBUM_LOCATION, 
	    ALBUM_SHARED_PUSHED, USER_TAG, TAG_LOCATION, CAN_UPLOAD_TO_ALBUM
	};
	
    public static SnappeRelationship get(String id) {
        Relationship r = DBManager.getDB().index().getRelationshipAutoIndexer().getAutoIndex().get(TrackedPropertyContainer.ID_KEY, id).getSingle();
        if(r == null) {
            return null;
        }
        return new SnappeRelationship(new TrackedPropertyContainer(r));
    }
    
    public static SnappeRelationship get(Relationship r) {
    	TrackedPropertyContainer wrapped = new TrackedPropertyContainer(r);
    	return new SnappeRelationship(wrapped);
    }
	
	public static SnappeRelationship create(SnappeNode startNode, SnappeNode endNode, SnappeRelationshipType type, UniquenessType uniqueness) throws InterruptedException, ExecutionException {
		Relationship r = DBManager.getOrCreateRelationship(startNode.getWrappedNode(), endNode.getWrappedNode(), type, uniqueness);
	 	TrackedPropertyContainer wrapped = new TrackedPropertyContainer(r);
	 	wrapped.setProperty(Constants.SNAPPE_TYPE_KEY, SNAPPE_TYPE);
	 	
	    return new SnappeRelationship(wrapped);
	}
	
	public SnappeRelationship(TrackedPropertyContainer wrappedRelationship) {
		super(wrappedRelationship);
	}
	
	public Relationship getWrappedRelationship() {
		return (Relationship)super.getWrappedContainer();
	}
	
	public JSONObject getChangeSet(long modifiedSince) throws JSONException {
		JSONObject changeSet = this.wrappedContainer.getChangeSet(modifiedSince);
		if(changeSet.length() == 0) {
		    return changeSet;
		}
		
		Relationship r = (Relationship) this.wrappedContainer.getWrappedContainer();
		
		
		SnappeNode startNode = SnappeNode.get(r.getStartNode());
		SnappeNode endNode = SnappeNode.get(r.getEndNode());
		
		changeSet.put("start_node", startNode.getId());
		changeSet.put("end_node", endNode.getId());
		changeSet.put("relationship_type", r.getType().toString());
		
		changeSet.put(TrackedPropertyContainer.ID_KEY, this.wrappedContainer.getId());
		changeSet.put(Constants.SNAPPE_TYPE_KEY, this.wrappedContainer.getProperty(Constants.SNAPPE_TYPE_KEY));
        
		return changeSet;
	}
	
	public void delete() {
	    Util.log("deleting relationship");
	    new Exception().printStackTrace(System.err);
	    
	    this.wrappedContainer.delete();
	}
	
	public void undelete() {
        this.wrappedContainer.undelete();
    }
	
	public boolean isDeleted() {
	    return this.wrappedContainer.isDeleted();
	}
}
