package pe.snap.model;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.PropertyContainer;

import pe.snap.util.Util;
import scala.actors.threadpool.Arrays;

public abstract class BasePropertyContainer<T extends PropertyContainer> {
	private static final String ID_KEY = "id";
	private static final String MODIFICATION_TIME_SUFFIX = "__mod_on__";
	private static final String IS_DELETED_SUFFIX = "__is_deleted__";
	
	private T wrappedContainer;

	
	
	@SuppressWarnings("unchecked")
	private static final Set<String> indexedProperties = new HashSet<String>(Arrays.asList(new String[]{
	    ID_KEY		
	}));
	
	private BasePropertyContainer() {
	}
	
	public String getId() {
		return this.getProperty(ID_KEY);
	}
	
	public T getWrappedContainer() {
		return this.wrappedContainer;
	}
	
	protected static Set<String> getIndexedProperties() {
	    return indexedProperties;
	}	
	
	protected String getProperty(String propertyName) {
		return this.wrappedContainer.getProperty(propertyName).toString();
	}
	
	protected void setProperty(String propertyName, Object propertyValue) {
		this.wrappedContainer.setProperty(propertyName, propertyValue);
		this.touchProperty(propertyName);
		this.wrappedContainer.setProperty(propertyName + IS_DELETED_SUFFIX, false);
	}
	
	protected void removeProperty(String propertyName) {
		this.wrappedContainer.removeProperty(propertyName);
		this.touchProperty(propertyName);
		this.wrappedContainer.setProperty(propertyName + IS_DELETED_SUFFIX, true);
	}
	
	protected boolean hasProperty(String propertyName) {
		return this.wrappedContainer.hasProperty(propertyName);
	}
	
	private void touchProperty(String propertyName) {
		this.wrappedContainer.setProperty(propertyName + MODIFICATION_TIME_SUFFIX, Util.getCurrentTimestamp());
	}
	
	@Override
	public int hashCode() {
		return wrappedContainer.hashCode();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object o) {
		return o instanceof BasePropertyContainer<?> && wrappedContainer.equals(((BasePropertyContainer<T>)o).getWrappedContainer());
	}
}
