package pe.snap.model;

import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;

import pe.snap.model.SnappeRelationship.SnappeRelationshipType;
import pe.snap.util.Util;

public class UserGraphChangeEvaluator implements Evaluator {
    private User user;
    public UserGraphChangeEvaluator(User user, long modifiedSince) {
        this.user = user;
    }
    
    
    @Override
    public Evaluation evaluate(Path path) {
        //Util.log("%s", path.toString());
        if(path.length() == 0) {
            return Evaluation.INCLUDE_AND_CONTINUE;    
        }
        
        Relationship r = path.lastRelationship();
        //no FOF
        if(r.isType(SnappeRelationshipType.FRIEND)) {
            User u1 = User.get(r.getStartNode());
            User u2 = User.get(r.getEndNode());
            
            if(!(u1.is(user) || u2.is(user))) {
                return Evaluation.EXCLUDE_AND_PRUNE;    
            }
        }
        //no non-FB album of friends
        //no restriction on "friend of album" (to cover explicit shares)
        if(r.isType(SnappeRelationshipType.USER_OWNS_ALBUM)) {
            String lastNodeType = Util.getNodeType(path.endNode());
            //user->album
            if("album".equals(lastNodeType)) {
                Album album = Album.get(path.endNode());
                User owner = User.get(path.lastRelationship().getStartNode());
                if(!(owner.is(user) || album.isFacebookAlbum())) {
                    return Evaluation.EXCLUDE_AND_PRUNE;
                }
            }
        }
        //only pushed to me or my pushed album
        if(r.isType(SnappeRelationshipType.ALBUM_SHARED_PUSHED)) {
            User pushedTo = User.get(r.getEndNode());
            Album pushed = Album.get(r.getStartNode());
            
            boolean pushedToSelf = pushedTo.is(user);
            boolean pushedAlbumSelfOwned = pushed.getOwner().is(user);
            
            if(!(pushedToSelf || pushedAlbumSelfOwned)) {
                return Evaluation.EXCLUDE_AND_PRUNE;    
            }
            
        }
        if(r.isType(SnappeRelationshipType.CAN_UPLOAD_TO_ALBUM)) {
            User uploader = User.get(r.getStartNode());
            Album album = Album.get(r.getEndNode());
            
            boolean uploaderSelf = uploader.is(user);
            boolean albumSelfOwned = album.getOwner().is(user);
            if(!(uploaderSelf || albumSelfOwned)) {
                return Evaluation.EXCLUDE_AND_PRUNE;
            }
            
        }
        if(r.isType(SnappeRelationshipType.PHOTO_IN_ALBUM)) {
            //hide others' private photos
            Photo photo = Photo.get(r.getStartNode());
            Album album = Album.get(r.getEndNode());
            if(photo.isPrivate() && !album.getOwner().is(user)) {
                return Evaluation.EXCLUDE_AND_PRUNE;
            }
        }
        
        return Evaluation.INCLUDE_AND_CONTINUE;
    }
}
