package pe.snap.model;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;

import pe.snap.exceptions.APIException;
import pe.snap.imagehash.ImagePHash;
import pe.snap.infra.DBManager;
import pe.snap.model.SnappeNode.UniquenessType;
import pe.snap.model.SnappeRelationship.SnappeRelationshipType;
import pe.snap.model.User.PrivacyLevel;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.util.Analytics;
import pe.snap.util.Constants;
import pe.snap.util.Util;
import scala.actors.threadpool.Arrays;

public class Album {
	private static final String NODE_TYPE = "album";
	private static final String DEVICE_ID_KEY = "device_id";
	private static final String DEVICE_PERSISTENT_ID_KEY = "device_persistent_id";
	private static final String ALBUM_CREATION_SOURCE_PROPERTY_KEY = "album_creation_source";
	private static final String PHOTO_SOURCE_PROPERTY_KEY = "album_photo_source";
	
    private SnappeNode wrappedNode;
    
    public enum PhotoSource {
        DEVICE, FACEBOOK, EMAIL, PULLED_ALBUM, PUSHED_ALBUM, ADDED_PHOTOS    
    };
	
	public static final String[] FQL_FIELDS = new String[]{
	    "aid", "object_id", "owner", "name", "description", "created", "modified", "location", "type"	
	};
	
	public Album(String deviceID) throws InterruptedException, ExecutionException {
	    this.wrappedNode = SnappeNode.create(NODE_TYPE);
	    this.wrappedNode.setProperty(DEVICE_ID_KEY, deviceID);
	}
	
	public Album(String id, String deviceID) throws InterruptedException, ExecutionException {
	    this.wrappedNode = SnappeNode.getOrCreate(id, NODE_TYPE);
	    this.wrappedNode.setProperty(DEVICE_ID_KEY, deviceID);
	}
	
	public static Album get(String id) throws InterruptedException, ExecutionException {
		SnappeNode wrappedNode = SnappeNode.get(id);
		if(wrappedNode == null) return null;
		return new Album(wrappedNode);
	}
	
	public static Album get(Node node) {
		SnappeNode wrappedNode = SnappeNode.get(node);
		return new Album(wrappedNode);
	}
	
	public static Album createOrUpdate(JSONObject fbJSON) throws JSONException, InterruptedException, ExecutionException {
	    
	    String id = fbJSON.getString("object_id");
	    
		SnappeNode wrappedNode = SnappeNode.getOrCreate(id, NODE_TYPE);
		for(String fqlField : FQL_FIELDS) {
		    wrappedNode.setProperty(fqlField, fqlField.endsWith("id") ? fbJSON.getString(fqlField) : fbJSON.get(fqlField));
		}
		wrappedNode.setProperty(DEVICE_ID_KEY, "facebook");
		return new Album(wrappedNode);
	}
	
	private Album(SnappeNode node) {
	    this.wrappedNode = node;
	}
	
	public SnappeNode getWrappedNode() {
		return this.wrappedNode;
	}
	
	public String getId() {
		return this.wrappedNode.getId();
	}
	
	public String getFBObjectId() {
	    return (String)this.wrappedNode.getProperty("object_id", null);
	}
	
	public int getModifiedOn() {
		if(!this.wrappedNode.hasProperty("modified")) return -1;
		return (Integer)this.wrappedNode.getProperty("modified");
	}
	
	public void setModifiedOn(int modifiedOn) {
		this.wrappedNode.setProperty("modified", modifiedOn);
	}
	
	public void setDescription(String description) {
		this.wrappedNode.setProperty("description", description);
	}
	
	public String getDescription() {
		return (String)this.wrappedNode.getProperty("description", "");
	}
	
	public void setName(String name) {
		this.wrappedNode.setProperty("name", name);
	}
	
	public String getName() {
		return (String)this.wrappedNode.getProperty("name", null); 
	}
	
	public void setLocation(Location location) throws InterruptedException, ExecutionException {
	    this.wrappedNode.createRelationshipTo(location.getWrappedNode(), SnappeRelationshipType.ALBUM_LOCATION, UniquenessType.NON_DIRECTIONAL);
    }
	
	public Location getLocation() {
        Iterable<Relationship> rels = this.wrappedNode.getWrappedNode().getRelationships(Direction.OUTGOING, SnappeRelationshipType.ALBUM_LOCATION);
        for(Relationship r : rels) {
            return Location.get(r.getEndNode());
        }
        return null;
    }
	
	public void autoGenerateName() {
	    String currentName = this.getName();
	    if(currentName != null && !currentName.isEmpty()) {
	        return;
	    }
	    
	    String locationName = null;
		for(Photo photo : this.getPhotos()) {
		    Location loc = photo.getLocation();
		    if(loc != null) {
		        locationName = loc.getName();
		        if(locationName != null) {
		            break;
		        }
		    }
		}
		
		
		
	    int minTime = getOldestPhotoTime();
        int maxTime = getOldestPhotoTime();
	    String intervalName = Util.nameInterval(minTime, maxTime);    
		
	    String name = locationName == null ? intervalName : String.format("%s - %s", locationName, intervalName);
		this.setName(name);
	}
	
	public JSONObject getChangeSet(long modifiedSince) throws JSONException {
		JSONObject rv = this.wrappedNode.getChangeSet(modifiedSince);
		if(rv.length() > 0) {
		    rv.put(TrackedPropertyContainer.ID_KEY, this.getId());
            rv.put(Constants.SNAPPE_TYPE_KEY, this.getWrappedNode().getProperty(Constants.SNAPPE_TYPE_KEY));
		}
		return rv;
	}
	
	public JSONArray getChanges(long since, PrivacyLevel privacy) throws JSONException {
        JSONArray rv = new JSONArray();
        
        Node node = this.getWrappedNode().getWrappedNode();
        
        if(privacy == PrivacyLevel.OWNER) {
            //add just the relationship to the friend whom my album is shared
            //with. since albums can be shared only with friends, the friend
            //must already be captured
            SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.ALBUM_SHARED_PUSHED, Direction.OUTGOING, since, privacy);
            SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.CAN_UPLOAD_TO_ALBUM, Direction.INCOMING, since, privacy);
        }
        
        //gather changes from all photos
        SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.PHOTO_IN_ALBUM, Direction.INCOMING, since, privacy);
        
        //get albums' location
        SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.ALBUM_LOCATION, Direction.OUTGOING, since, privacy);
        
        if(privacy == PrivacyLevel.FRIEND) {
            //get album owner relationship only
            SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.USER_OWNS_ALBUM, Direction.INCOMING, since, privacy);
        }
        
        JSONObject selfChanges = this.getChangeSet(since);
        if(selfChanges.length() > 0) {
            rv.put(selfChanges);
        }
        
        return rv;
	}
	
	public boolean setOwnsPhoto(Photo photo, PhotoSource source) throws InterruptedException, ExecutionException {
	    Album currentAlbum = photo.getAlbum();
	    if(currentAlbum != null && currentAlbum.is(this)) {
	        //TODO: do we need to update source? Does it ever happen?
	        Util.log("skipping Album.setOwnsPhoto: %s already owns %s(%s)", currentAlbum.getId(), photo.getId(), photo.getHash());
	        return false;
	    }
	    
	    //remove any connection between the photo and it's existing parent album
	    for(Relationship r : photo.getWrappedNode().getWrappedNode().getRelationships(SnappeRelationshipType.PHOTO_IN_ALBUM)) {
	        SnappeRelationship snappeR = SnappeRelationship.get(r);
	        snappeR.delete();
	    }
	    
		SnappeRelationship rel = photo.getWrappedNode().createRelationshipTo(this.wrappedNode, SnappeRelationshipType.PHOTO_IN_ALBUM, UniquenessType.NON_DIRECTIONAL);
		rel.setProperty(PHOTO_SOURCE_PROPERTY_KEY, source.toString());
		
		int photoTime = photo.getCreatedOn();
		if(getModifiedOn() < photoTime) {
			this.setModifiedOn(photoTime);
		}
		
		return true;
	}
	
	public boolean isDeviceAlbum() {
		return !"facebook".equals(this.wrappedNode.getProperty(DEVICE_ID_KEY));
	}
	
	public boolean isFacebookAlbum() {
	    return !this.isDeviceAlbum();
	}
	
	public String getDeviceID() {
		return (String)this.wrappedNode.getProperty(DEVICE_ID_KEY);
	}
	
	public void setDeviceID(String deviceID) {
		this.wrappedNode.setProperty(DEVICE_ID_KEY, deviceID);
	}
	
	public String getDevicePersistentID() {
	    return (String)this.wrappedNode.getProperty(DEVICE_PERSISTENT_ID_KEY, null);
	}
	
	public void setDevicePersistentID(String devicePersistentID) {
	    this.wrappedNode.setProperty(DEVICE_PERSISTENT_ID_KEY, devicePersistentID);
	}
	
	public void setIsAutoCreatedAlbum() {
		this.wrappedNode.setProperty(ALBUM_CREATION_SOURCE_PROPERTY_KEY, "auto");
	}
	
	public boolean isAutoCreatedAlbum() {
		return this.wrappedNode.getProperty(ALBUM_CREATION_SOURCE_PROPERTY_KEY, "").equals("auto");
	}
	
	public void unsetIsAutoCreatedAlbum() {
	    this.wrappedNode.setProperty(ALBUM_CREATION_SOURCE_PROPERTY_KEY, "device");
	}
    
    public void allowUploadAccess(User u) throws InterruptedException, ExecutionException {
        u.getWrappedNode().createRelationshipTo(this.getWrappedNode(), SnappeRelationshipType.CAN_UPLOAD_TO_ALBUM, UniquenessType.NON_DIRECTIONAL);
    }
    
    public void disallowUploadAccess(User u) {
        this.wrappedNode.deleteRelationWithNodeOfType(u.getWrappedNode().getWrappedNode(), SnappeRelationshipType.CAN_UPLOAD_TO_ALBUM);
    }
    
    public boolean isUserAllowedUploadAccess(User u) {
        return this.wrappedNode.hasRelationshipOfTypeWithNode(SnappeRelationshipType.CAN_UPLOAD_TO_ALBUM, u.getWrappedNode().getWrappedNode());
    }
	
	public void shareWithUser(User u) throws InterruptedException, ExecutionException {
		this.wrappedNode.createRelationshipTo(u.getWrappedNode(), SnappeRelationshipType.ALBUM_SHARED_PUSHED, UniquenessType.NON_DIRECTIONAL);
	}
	
	public void unshareWithUser(User u) {
	    this.wrappedNode.deleteRelationWithNodeOfType(u.getWrappedNode().getWrappedNode(), SnappeRelationshipType.ALBUM_SHARED_PUSHED);
	}
	
	public boolean isSharedWithUser(User u) {
        return this.wrappedNode.hasRelationshipOfTypeWithNode(SnappeRelationshipType.ALBUM_SHARED_PUSHED, u.getWrappedNode().getWrappedNode());
    }
	
	public List<User> getSharedWithUsers() {
	    Traverser traverser = Traversal.description().depthFirst()
	            .relationships(SnappeRelationshipType.ALBUM_SHARED_PUSHED, Direction.OUTGOING)
	            .uniqueness(Uniqueness.NODE_GLOBAL)
	            .uniqueness(Uniqueness.RELATIONSHIP_GLOBAL)
	            .evaluator(Evaluators.atDepth(1))
	            .evaluator(DeletePruningEvaluator.get())
	            .traverse(this.wrappedNode.getWrappedNode());
	        
	        List<User> rv = new ArrayList<User>();
	        for (Node n : traverser.nodes()) {
	            User sharedWithUser = User.get(n);
	            rv.add(sharedWithUser);
	        }
	        
	        return rv;
	}
	
	//before calling this always check that this album's owner has permission
	//to read + write to otherAlbum
	public void pullAlbum(Album source) throws InterruptedException, ExecutionException, JSONException, SQLException, IOException, HTTPServiceException, APIException {
	    this.pullPhotos(source);
	}
	
	public void pushToAlbum(Album destination) throws InterruptedException, ExecutionException, JSONException, SQLException, IOException, HTTPServiceException, APIException {
        this.pushPhotos(destination);
    }
    
    public void syncWithAlbum(Album otherAlbum) throws InterruptedException, ExecutionException, JSONException, SQLException, IOException, HTTPServiceException, APIException {
        this.pullAlbum(otherAlbum);
        this.pushToAlbum(otherAlbum);
    }
		
	public int getOldestPhotoTime() {
		List<Photo> sortedPhotos = this.getSortedPhotos();
		if(sortedPhotos.size() == 0) {
		    return -1;
		}
		return sortedPhotos.get(0).getCreatedOn();
		
		
	}
	
	public int getNewestPhotoTime() {
	    List<Photo> sortedPhotos = this.getSortedPhotos();
        if(sortedPhotos.size() == 0) {
            return -1;
        }
        return sortedPhotos.get(sortedPhotos.size() - 1).getCreatedOn();
	}
	
	public User getOwner() {
	    Iterable<Relationship> it = this.wrappedNode.getWrappedNode().getRelationships(SnappeRelationshipType.USER_OWNS_ALBUM);
	    for(Relationship r : it) {
	        return User.get(r.getStartNode());
	    }
	    return null;
	}
	
	public List<Photo> getPhotos() {
		Traverser traverser = Traversal.description().depthFirst()
		    .relationships(SnappeRelationshipType.PHOTO_IN_ALBUM, Direction.INCOMING)
		    .uniqueness(Uniqueness.NODE_GLOBAL)
		    .uniqueness(Uniqueness.RELATIONSHIP_GLOBAL)
		    .evaluator(Evaluators.atDepth(1))
		    .evaluator(DeletePruningEvaluator.get())
		    .traverse(this.wrappedNode.getWrappedNode());
		
		List<Photo> rv = new ArrayList<Photo>();
		for (Node n : traverser.nodes()) {
			Photo photo = Photo.get(n);
			rv.add(photo);
		}
		
		return rv;
	}
	
	public List<Photo> getPhotos(boolean sorted, boolean imageAvailable, boolean nonPrivate) {
	    List<Photo> photos = this.getPhotos();
	    
	    List<Photo> rv = new ArrayList<Photo>();
	    for(Photo photo : photos) {
	        if(nonPrivate && photo.isPrivate()) {
	            continue;
	        }
	        if(imageAvailable && !photo.isImageAvailable()) {
	            continue;
	        }
	        
	        rv.add(photo);
	    }
	    
	    if(sorted) {
	        Collections.sort(rv, new Comparator<Photo>() {
	            @Override
	            public int compare(Photo o1, Photo o2) {
	                return o1.getCreatedOn() - o2.getCreatedOn();
	            }
	        });    
	    }
	    
	    return rv;
	}
	
	public List<Photo> getSortedPhotos() {
	    return getPhotos(true, false, false);
	}
	
	public List<Photo> getSortedAvailableNonPrivatePhotos() {
        return getPhotos(true, true, true);
    }
	
	public List<Photo> getNonPrivatePhotos() {
        return getPhotos(false, false, true);
    }
	
	
	
	public String getThumbnailURL() {
		for(Photo photo : this.getPhotos()) {
			String thumbURL = photo.getThumbnailURL();
			if(thumbURL != null) {
				return thumbURL;
			}
		}
		return null;
	}
	
	public void delete() {
	    Node n = this.getWrappedNode().getWrappedNode();
	    
	    for(Relationship r : n.getRelationships()) {
	        SnappeRelationship snappeR = SnappeRelationship.get(r);
	        if(snappeR.isDeleted()) {
	            continue;
	        }
	        
	        if(r.isType(SnappeRelationshipType.PHOTO_IN_ALBUM)) {
	            Node otherNode = r.getOtherNode(n);
	            Photo albumPhoto = Photo.get(otherNode);
	            albumPhoto.delete();
	        }
	        
	        
            snappeR.delete();
	    }
	    
	    SnappeNode snappeN = SnappeNode.get(n);
        snappeN.delete();
	}
	
	public boolean hasPhotoWithHash(String hash) {
	    for(Photo photo : this.getPhotos()) {
	        if(hash.equals(photo.getHash())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	public void ensureHashes() throws IOException {
	    Transaction tx = DBManager.getTransaction();
	    try {
	        for(Photo photo : this.getPhotos()) {
	            photo.ensureHashes();
	        }
	        tx.success();
	    }
	    finally {
	        tx.finish();
	    }
	}
	
	public Set<String> getHashes() {
        Set<String> rv = new HashSet<String>();
        for(Photo photo : this.getPhotos()) {
            String hash = photo.getHash();
            if(hash != null) {
                rv.add(hash);
            }
        }
        return rv;
    }
	
	public Set<String> getPHashes() {
	    Set<String> rv = new HashSet<String>();
	    for(Photo photo : this.getPhotos()) {
	        String pHash = photo.getPHash();
	        if(pHash != null) {
	            rv.add(pHash);
	        }
	    }
	    return rv;
	}
	
	public List<Photo> getNonSimilarPhotos(List<Photo> photos) {
	    Set<String> hashes = this.getHashes();
	    Set<String> pHashes = this.getPHashes();
	    
	    List<Photo> rv = new ArrayList<Photo>();
	    for(Photo photo : photos) {
	        String hash = photo.getHash();
	        if(hash != null && hashes.contains(hash)) {
	            continue;
	        }
	        
	        String pHash = photo.getPHash();
	        if(pHash != null && isSimilarToAny(pHash, pHashes)) {
	            continue;
	        }
	        
	        rv.add(photo);
	    }
	    return rv;
	}
	
	public boolean hasSimilarPhoto(Photo needle) {
	    List<Photo> list = new ArrayList<Photo>();
	    list.add(needle);
	    return this.getNonSimilarPhotos(list).isEmpty();    
	}
	
	private static boolean isSimilarToAny(String needle, Set<String> haystack) {
	    for(String hay : haystack) {
	        if(ImagePHash.areSimilar(hay, needle)) {
	            return true;
	        }
	    }
	    return false;
	}
	
	public boolean isEmptyForUser(User user) {
	    boolean isUserOwner = user.ownsAlbum(this);
	    
	    for(Photo photo : this.getPhotos()) {
	        //for non-owner private photo is as good as non-existent
	        if(!isUserOwner && photo.isPrivate()) {
	            continue;
	        }
	        
	        if(!photo.getWrappedNode().isDeleted()) {
	            return false;
	        }
	    }
	    
	    return true;
	}
	    
    private void pushPhotos(Album destination) throws InterruptedException, ExecutionException, JSONException, SQLException, IOException, HTTPServiceException, APIException {
        List<Photo> photosToAdd = new ArrayList<Photo>();
        for(Photo photo : this.getNonPrivatePhotos()) {
            if(!photo.isImageAvailable()) {
                String message = String.format("photo to push has no available image: %s", photo.getId());
                Util.logAdminWarning(message);
            }
            else {
                photosToAdd.add(photo);    
            }
        }
        
        if(!photosToAdd.isEmpty()) {
            Photo.addToAlbum(destination, this.getOwner(), photosToAdd, PhotoSource.PUSHED_ALBUM, true);
        }
    }
    
    private void pullPhotos(Album source) throws InterruptedException, ExecutionException, JSONException, SQLException, IOException, HTTPServiceException, APIException {
        List<Photo> photosToAdd = new ArrayList<Photo>();
        for(Photo photo : source.getNonPrivatePhotos()) {
            if(!photo.isImageAvailable()) {
                String message = String.format("photo to pull has no available image: %s", photo.getId());
                Util.logAdminWarning(message);
            }
            else {
                photosToAdd.add(photo);    
            }
        }
        
        if(!photosToAdd.isEmpty()) {
            Photo.addToAlbum(this, this.getOwner(), photosToAdd, PhotoSource.PULLED_ALBUM, true);
        }
    }
    
    public boolean is(Album album) {
        return this.wrappedNode.is(album.wrappedNode);
    }
    
	
}
