package pe.snap.model;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;

import pe.snap.exceptions.APIException;
import pe.snap.imagehash.ImagePHash;
import pe.snap.infra.PhotoManager;
import pe.snap.infra.PhotoManager.PhotoSize;
import pe.snap.infra.PhotoMetadata;
import pe.snap.model.Album.PhotoSource;
import pe.snap.model.Notification.NotificationType;
import pe.snap.model.SnappeNode.UniquenessType;
import pe.snap.model.SnappeRelationship.SnappeRelationshipType;
import pe.snap.model.User.PrivacyLevel;
import pe.snap.services.NotificationService;
import pe.snap.services.http.HTTPService;
import pe.snap.services.http.HTTPServiceException;
import pe.snap.services.http.HTTPServiceResponse;
import pe.snap.util.Constants;
import pe.snap.util.FacebookManager;
import pe.snap.util.Util;


public class Photo {
	private static final String NODE_TYPE = "photo";
	
    private SnappeNode wrappedNode;
	
	public static final String[] FQL_FIELDS = new String[]{
	    "pid", "aid", "owner", "caption", "src_big", "src_big_width", 
	    "src_big_height", "src", "src_width", "src_height", "src_small",
	    "src_small_width", "src_small_height", "created", "modified",
	    "place_id", "object_id", "album_object_id"
	};

	public static final String PHOTO_HASH_KEY = "hash";
	public static final String PHOTO_PHASH_KEY = "phash";
	public static final String PHOTO_PRIVATE_KEY = "snappe_private";
	public static final String PHOTO_THUMB_SRC_KEY = "src_small";
	
	public Photo() throws InterruptedException, ExecutionException {
	    this.wrappedNode = SnappeNode.create(NODE_TYPE);	
	}
	
	public Photo(PhotoMetadata metadata) throws InterruptedException, ExecutionException {
	    String standardPhotoURL = PhotoManager.getStandardPhotoURL(metadata.hash);
	    String fullPhotoURL = PhotoManager.getFullPhotoURL(metadata.hash);
	    String thumbURL = PhotoManager.getThumbnailURL(metadata.hash);
	    
	    this.wrappedNode = SnappeNode.create(NODE_TYPE);
	    
	    this.setHash(metadata.hash);
	    this.setPHash(metadata.pHash);
	    
	    this.setStandardURL(standardPhotoURL);
	    this.setFullURL(fullPhotoURL);
	    this.setThumbnailURL(thumbURL);
	    
	    this.setCreatedOn(metadata.timestamp);
	    
	    this.setWidth(metadata.width);
	    this.setHeight(metadata.height);
	    this.setThumbWidth(metadata.thumbWidth);
	    this.setThumbHeight(metadata.thumbHeight);
	    
	}
	public static Photo get(String id) throws InterruptedException, ExecutionException {
		SnappeNode wrappedNode = SnappeNode.get(id);
		if(wrappedNode == null) return null;
		
		Photo rv = new Photo(wrappedNode);
        return rv;
	}
	
	public static Photo get(Node node) {
		SnappeNode wrappedNode = SnappeNode.get(node);
		return new Photo(wrappedNode);
	}
	
	public static List<Photo> getByThumbURL(String thumbURL) {
		List<Photo> rv = new ArrayList<Photo>();
		List<SnappeNode> nodes = SnappeNode.getByNonUniqueProperty(PHOTO_THUMB_SRC_KEY, thumbURL);
		for(SnappeNode node : nodes) {
		    if(!node.isDeleted()) {
		        rv.add(Photo.get(node.getWrappedNode()));    
		    }
		}
		return rv;
	}
	
	public static List<Photo> getByHash(String hash) {
        List<Photo> rv = new ArrayList<Photo>();
        List<SnappeNode> nodes = SnappeNode.getByNonUniqueProperty(PHOTO_HASH_KEY, hash);
        for(SnappeNode node : nodes) {
            if(!node.isDeleted()) {
                rv.add(Photo.get(node.getWrappedNode()));    
            }
        }
        return rv;
    }
	
	public static Photo createOrUpdate(JSONObject fbJSON) throws JSONException, IOException, InterruptedException, ExecutionException {
	    String id = fbJSON.getString("object_id");
	    
	    
		SnappeNode wrappedNode = SnappeNode.getOrCreate(id, NODE_TYPE);
		for(String fqlField : FQL_FIELDS) {
		    Object val = fqlField.endsWith("id") ? fbJSON.getString(fqlField) : fbJSON.get(fqlField);
		    if(val == JSONObject.NULL) {
		        continue;
		    }
		    wrappedNode.setProperty(fqlField, val);
		}
		
		Photo p = new Photo(wrappedNode);
		//p.setPHash(p.calculatePHash());
		return p;
	}
	
	private Photo(SnappeNode wrappedNode) {
		this.wrappedNode = wrappedNode;
	}
	
	public SnappeNode getWrappedNode() {
		return this.wrappedNode;
	}
	
	public String getId() {
		return (String)this.wrappedNode.getId();
	}
	
	public JSONObject getChangeSet(long modifiedSince) throws JSONException {
	    JSONObject rv = this.wrappedNode.getChangeSet(modifiedSince);
        if(rv.length() > 0) {
            rv.put(TrackedPropertyContainer.ID_KEY, this.getId());
            rv.put(Constants.SNAPPE_TYPE_KEY, this.getWrappedNode().getProperty(Constants.SNAPPE_TYPE_KEY));
        }
        return rv;
	}
	
	public JSONArray getChanges(long since, PrivacyLevel privacy) throws JSONException {
        JSONArray rv = new JSONArray();
        
        Node node = this.getWrappedNode().getWrappedNode();
        
        //gather changes from all locations (there could be deleted old locations)
        SnappeNode.gatherChanges(rv, node, SnappeRelationshipType.PHOTO_LOCATION, Direction.OUTGOING, since, privacy);
        
        JSONObject selfChanges = this.getChangeSet(since);
        if(selfChanges.length() > 0) {
            rv.put(selfChanges);
        }
        
        return rv;
	}
	
	public void setCreatedOn(int timestamp) {
		this.wrappedNode.setProperty("created", timestamp);
	}
	
	public int getCreatedOn() {
		return (Integer)this.wrappedNode.getProperty("created");
	}
	
	public void setLocation(Location location) throws InterruptedException, ExecutionException {
        this.wrappedNode.createRelationshipTo(location.getWrappedNode(), SnappeRelationshipType.PHOTO_LOCATION, UniquenessType.NON_DIRECTIONAL);
    }
	
	public Location getLocation() {
	    Iterable<Relationship> rels = this.wrappedNode.getWrappedNode().getRelationships(Direction.OUTGOING, SnappeRelationshipType.PHOTO_LOCATION);
	    for(Relationship r : rels) {
	        return Location.get(r.getEndNode());
	    }
	    return null;
	}
	
	public void setHash(String hash) {
		this.wrappedNode.setProperty(PHOTO_HASH_KEY, hash);
	}
	
	public String getHash() {
	    return (String)this.wrappedNode.getProperty(PHOTO_HASH_KEY, null);
	}
	
	public void setPHash(String pHash) {
		this.wrappedNode.setProperty(PHOTO_PHASH_KEY, pHash);
	}
	
	public String getPHash() {
		return (String)this.wrappedNode.getProperty(PHOTO_PHASH_KEY, null);
	}
	
	public void setWidth(int width) {
	    this.wrappedNode.setProperty("src_big_width", width);
	}
	
	public void setHeight(int height) {
        this.wrappedNode.setProperty("src_big_height", height);
    }
	
	public void setThumbWidth(int thumbWidth) {
		this.wrappedNode.setProperty("src_small_width", thumbWidth);
	}
	
	public void setThumbHeight(int thumbHeight) {
		this.wrappedNode.setProperty("src_small_height", thumbHeight);
	}
	
	public void setFullURL(String url) {
	    this.wrappedNode.setProperty("src_big", url);
	}
	
	public void setStandardURL(String url) {
        this.wrappedNode.setProperty("src", url);
    }
	
	public void setThumbnailURL(String thumbURL) {
		this.wrappedNode.setProperty("src_small", thumbURL);
	}
	
	public String getFullURL() {
        return (String)this.wrappedNode.getProperty("src_big", null);
    }
	
	public String getStandardURL() {
	    String rv = (String)this.wrappedNode.getProperty("src", null);
	    //facebook standard size photos are of hardly any use
	    if(rv == null || rv.indexOf("snap.pe") < 0) {
	        return this.getFullURL();
	    }
	    return rv;
	}
	
	public String getThumbnailURL() {
		return (String)this.wrappedNode.getProperty("src_small", null);
	}
	
	public String getURL() {
	    //FB photos might have one type of url missing, we start searching
	    //with standard size
		String rv = this.getStandardURL();
		if(rv == null || rv.indexOf("snap.pe") < 0) {
		    rv = this.getFullURL();
		}
		if(rv == null) {
		    rv = this.getThumbnailURL();
		}
		return rv;
	}
	
	public void updateURLs() {
	    String hash = this.getHash();
	    if(hash != null && PhotoManager.hasPhoto(hash, PhotoSize.STANDARD)) {
            this.setStandardURL(PhotoManager.getStandardPhotoURL(hash));
            this.setFullURL(PhotoManager.getFullPhotoURL(hash));
            this.setThumbnailURL(PhotoManager.getThumbnailURL(hash));
	    }
	}
	
	public boolean isPrivate() {
		return (Boolean)this.wrappedNode.getProperty(PHOTO_PRIVATE_KEY, false);
	}
	
	public void setPrivate(boolean isPrivate) {
		this.wrappedNode.setProperty(PHOTO_PRIVATE_KEY, isPrivate);
	}
	
	public boolean hasPrivacyChangedSince(long timestamp) {
	    TrackedPropertyContainer tpc = new TrackedPropertyContainer(this.wrappedNode.getWrappedContainer());
	    return tpc.hasPropertyBeenModifiedSince(PHOTO_PRIVATE_KEY, timestamp);
	}
	
	public void ensureHashes() throws IOException {
	    if(this.getHash() != null && this.getPHash() != null) {
	        return;
	    }
	    
		String src = this.getURL();
		if(src == null) {
		    Util.log("warning:: ensureHashes: src is null for photo with id: %s", this.getId());
		    return;
		}
		
		HTTPServiceResponse response = HTTPService.get(src);
		byte[] imgData = response.getData();
		String pHash = ImagePHash.getHash(imgData);
		String hash = DigestUtils.md5Hex(imgData);
		
		Util.log("updating hash & phash for url: %s -> %s, %s", src, hash, pHash);
		
		
		this.setHash(hash);
		this.setPHash(pHash);
	}
	
	public void updateHashes(String hash, String pHash) throws IOException {
	    String currentHash = this.getHash();
        if(currentHash != null) {
            PhotoManager.updateHash(currentHash, hash);    
        }
        
        if(hash != null) {
            this.setHash(hash);
            Util.log("successfully update hash of photo %s to %s", this.getId(), hash);
        }
        if(pHash != null) {
            this.setPHash(pHash);
            Util.log("successfully update phash of photo %s to %s", this.getId(), pHash);
        }
        
        String thumbURL = this.getThumbnailURL();
        if(thumbURL != null) {
            List<Photo> photosWithSameURL = Photo.getByThumbURL(thumbURL);
            for(Photo photo : photosWithSameURL) {
                if(hash != null) {
                    photo.setHash(hash);
                    Util.log("successfully update hash of photo %s with url %s to %s", photo.getId(), thumbURL, hash);
                }
                if(pHash != null) {
                    photo.setPHash(pHash);
                    Util.log("successfully update phash of photo %s with url %s to %s", photo.getId(), thumbURL, pHash);
                }
            }
        }
        
	}
	
	public Photo cloneNode() throws InterruptedException, ExecutionException {
	    SnappeNode n = this.wrappedNode.cloneNode();
	    Photo rv = new Photo(n);
		for(Relationship r : this.getWrappedNode().getWrappedNode().getRelationships()) {
		    if(r.isType(SnappeRelationshipType.PHOTO_LOCATION)) {
		        Location location = Location.get(r.getOtherNode(this.getWrappedNode().getWrappedNode()));
		        rv.setLocation(location);
		    }
		    //add any other rel we want to preserve
		}
		return rv; 
	}
	
	public Album getAlbum() {
		Traverser traverser = Traversal.description().depthFirst()
	        .relationships(SnappeRelationshipType.PHOTO_IN_ALBUM, Direction.OUTGOING)
	        .uniqueness(Uniqueness.NODE_GLOBAL)
		    .uniqueness(Uniqueness.RELATIONSHIP_GLOBAL)
		    .evaluator(Evaluators.atDepth(1))
		    .evaluator(DeletePruningEvaluator.get())
		    .traverse(this.wrappedNode.getWrappedNode());
		Iterator<Node> it = traverser.nodes().iterator();
		if(it.hasNext()) return Album.get(it.next()); 
		return null;
	}
	
	public void delete() {
	    Util.log("deleting photo: %s", this.getId());
	    new Exception().printStackTrace(System.err);
	    
        Node n = this.getWrappedNode().getWrappedNode();
        
        for(Relationship r : n.getRelationships()) {
            SnappeRelationship snappeR = SnappeRelationship.get(r);
            if(snappeR.isDeleted()) {
                continue;
            }
            snappeR.delete();
        }
        
        SnappeNode snappeN = SnappeNode.get(n);
        snappeN.delete();
	}
	
	public boolean isDeleted() {
	    return this.wrappedNode.isDeleted();
	}
	
	public boolean isImageAvailable() {
	    return this.getURL() != null;
	}
	
	public boolean addCloneToAlbum(Album album, PhotoSource source, Object/*User|String*/ sharer) throws InterruptedException, ExecutionException, IOException, HTTPServiceException, JSONException, APIException {
	    if(!album.hasSimilarPhoto(this)) {
	        if(album.isFacebookAlbum()) {
	            FacebookManager.uploadPhotoToAlbum(this, album, sharer);
	            return true;
	        }
	        else {
	            Photo clone = this.cloneNode();
	            return album.setOwnsPhoto(clone, source);    
	        }
	    }
	    return false;
	}
	
	public boolean addToAlbum(Album album, PhotoSource source, Object/*User|String*/ sharer) throws InterruptedException, ExecutionException, IOException, HTTPServiceException, JSONException, APIException {
        if(!album.hasSimilarPhoto(this)) {
            if(album.isFacebookAlbum()) {
                FacebookManager.uploadPhotoToAlbum(this, album, sharer);
                return true;
            }
            else {
                return album.setOwnsPhoto(this, source);    
            }
            
        }
        return false;
    }
	
	public static void addToAlbum(Album album, Object/*User|String*/ sharer, List<Photo> photos, PhotoSource source, boolean clone) throws InterruptedException, ExecutionException, JSONException, SQLException, IOException, HTTPServiceException, APIException {
        
	    album.ensureHashes();
	    
	    int addedCount = 0;
        for(Photo photo : photos) {
            boolean added = false;
            if(clone) {
                added = photo.addCloneToAlbum(album, source, sharer);
            }
            else {
                added = photo.addToAlbum(album, source, sharer);    
            }
            
            if(added) {
                addedCount++;
            }
        }
        
        if(addedCount > 0) {
            List<User> notificationRecipients = new ArrayList<User>();
            if(source != PhotoSource.PULLED_ALBUM) {
                //puller already knows, owner doesn't need to know, no need to notify
                notificationRecipients.add(album.getOwner());
            }
            
            notificationRecipients.addAll(album.getSharedWithUsers());
            
            boolean sharerIsUser = sharer instanceof User;
            String sharerName = sharerIsUser ? ((User)sharer).getName() : (String)sharer;
            
            for(User receiver : notificationRecipients) {
                if(sharerIsUser) {
                    if(sharer != null && receiver.is((User)sharer)) {
                        continue;
                    }
                }
                
                JSONObject notificationData = new JSONObject();
                notificationData.put("receiver_album_id", album.getId());
                notificationData.put("receiver_album_name", album.getName());
                notificationData.put("thumbnail_url", album.getThumbnailURL());
                notificationData.put("pushed_photo_count", addedCount);
                notificationData.put("sharer_name", sharerName);
                
                Notification notification = new Notification(NotificationType.PHOTOS_PUSHED, notificationData);
                receiver.addNotification(notification);

                String message = String.format("%s pushed %d photo%s to %s", sharerName, addedCount, addedCount == 1 ? "" : "s", album.getName());
                NotificationService.sendNotification(receiver, message);    
            }            
        }        
    }
}
