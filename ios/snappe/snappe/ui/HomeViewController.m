//
//  HomeViewController.m
//  snappe
//
//  Created by Shashank on 13/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeViewController.h"
#import "iCarousel.h"
#import "OwnAlbumsCarouselView.h"
#import "SyncControlView.h"
#import "FriendsAlbumsView.h"
#import "Macros.h"
#import "MenuBar.h"
#import "ModalSearchView.h"
#import "EventManager.h"
#import "SortManager.h"
#import "UIController.h"
#import "CreditsCountView.h"
#import "Util.h"
#import "AlbumCreatorView.h"
#import "UIView+Help.h"
#import "HomeIntroOverlay.h"
#import "ModalAlertView.h"
#import "Logger.h"
#import "AdProvider.h"

#define TOP_SECTION_FRACTION 0.40
#define MIDDLE_SECTION_FRACTION 0.1
#define BOTTOM_SECTION_TOP_PADDING 10

#define MENU_BAR_WIDTH 44
#define MENU_BAR_HEIGHT 44

#define FOOTER_HEIGHT 64

#define CREDITS_BAR_BOTTOM_MARGIN 0
#define CREDITS_BAR_WIDTH 65
#define CREDITS_BAR_HEIGHT 44

#define CREATE_ALBUM_BUTTON_SIZE 44


@interface HomeViewController ()
@property (strong) OwnAlbumsCarouselView *ownAlbumsCarousel;
@property (strong) SyncControlView *syncControl;
@property (strong) FriendsAlbumsView *friendsAlbumsCarousel;
@property (strong) MenuBar *menuBar;
@property (strong) MenuBar *sortingMenu;
@property (strong) UIButton *searchButton;
@property (strong) CreditsCountView *creditsCountView;
@property (strong) UIButton *createAlbumButton;
@end

@implementation HomeViewController
@synthesize ownAlbumsCarousel;
@synthesize syncControl;
@synthesize friendsAlbumsCarousel;
@synthesize menuBar;
@synthesize sortingMenu;
@synthesize creditsCountView;

-(id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        CGRect currFrame = self.frame;
        
        CGRect rect, topRect, middleRect, bottomRect, footerRect;
        CGRectDivide(currFrame, &topRect, &rect, currFrame.size.height * TOP_SECTION_FRACTION, CGRectMinYEdge);
        CGRectDivide(rect, &middleRect, &rect, currFrame.size.height * MIDDLE_SECTION_FRACTION, CGRectMinYEdge);
        CGRectDivide(rect, &footerRect, &bottomRect, FOOTER_HEIGHT, CGRectMaxYEdge);
        
        bottomRect.origin.y += BOTTOM_SECTION_TOP_PADDING;
        
        CGRect menuBarRect = CGRectMake(0, 0, MENU_BAR_WIDTH, MENU_BAR_HEIGHT);
        menuBarRect.origin.y = CGRectGetMidY(middleRect) - (MENU_BAR_HEIGHT/2);
        
        CGRect creditsBarRect = CGRectMake(0, 0, CREDITS_BAR_WIDTH, CREDITS_BAR_HEIGHT);
        creditsBarRect.origin.y = frame.size.height - CREDITS_BAR_HEIGHT;
        
        //leave space for help button
        CGRect createAlbumButtonRect = CGRectMake(self.frame.size.width - 2 * CREATE_ALBUM_BUTTON_SIZE, CGRectGetMinY(creditsBarRect), CREATE_ALBUM_BUTTON_SIZE, CREATE_ALBUM_BUTTON_SIZE);
        
        //init sync control first, it listens to events fired by the
        //two carousels
        //TODO: find a better solution to this
        self.syncControl = [[SyncControlView alloc] initWithFrame:middleRect];
        [self addSubview:self.syncControl];
        
        self.ownAlbumsCarousel = [[OwnAlbumsCarouselView alloc] initWithFrame:bottomRect];
        [self addSubview: self.ownAlbumsCarousel];
        [[UIController getInstance] setUIControl:self.ownAlbumsCarousel :OWN_ALBUM_CAROUSEL_VIEW];
        
        self.friendsAlbumsCarousel = [[FriendsAlbumsView alloc] initWithFrame:topRect];
        [self addSubview: self.friendsAlbumsCarousel];
        [[UIController getInstance] setUIControl:self.friendsAlbumsCarousel :FRIEND_ALBUM_CAROUSEL_VIEW];
        
        self.creditsCountView = [[CreditsCountView alloc] initWithFrame:creditsBarRect];
        [self addSubview:self.creditsCountView];
        
        self.createAlbumButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.createAlbumButton setImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
        [self.createAlbumButton addTarget:self action:@selector(onCreateAlbumClick) forControlEvents:UIControlEventTouchUpInside];
        [self.createAlbumButton setTitle:@"Create" forState:UIControlStateNormal];
        [self.createAlbumButton setClipsToBounds:YES];
        [self.createAlbumButton setFrame:createAlbumButtonRect];
        self.createAlbumButton.backgroundColor = UIColorFromRGBA(0x0, 0.45);
        [self.createAlbumButton.layer setBorderWidth:1.0];
        [self.createAlbumButton.layer setBorderColor:UIColorFromRGBA(0xFFFFFF, 0.1).CGColor];
        [Util drawFastShadow:self.createAlbumButton];
        [self.createAlbumButton.layer setShadowOffset:CGSizeMake(-2.0, 2.0)];
        [self.createAlbumButton.layer setShadowRadius:3.0];
        [self.createAlbumButton.layer setShadowOpacity:0.0];
        [self addSubview:self.createAlbumButton];
        [self.createAlbumButton setHelpText:@"create a new album"];
        
        //self.menuBar = [[MenuBar alloc] initWithFrame:menuBarRect:LEFT:NO];
        //[self addSubview:self.menuBar];
        
        UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [settingsButton setImage:[UIImage imageNamed:@"settings.png"] forState:UIControlStateNormal];
        [settingsButton addTarget:self action:@selector(onSettingsButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [settingsButton setTitle:@"Settings" forState:UIControlStateNormal];
        [settingsButton setClipsToBounds:YES];
        [self.menuBar addSection:settingsButton:MENU_BAR_WIDTH];
        
        
        NSMutableArray *sortingMenuButtonProps = [[NSMutableArray alloc] init];
        [sortingMenuButtonProps addObject:[NSArray arrayWithObjects:@"brain_icon.png", @"onSmartSortClick:", [NSNumber numberWithInt:SMART], nil]];
        [sortingMenuButtonProps addObject:[NSArray arrayWithObjects:@"clock_icon.png", @"onDateSortClick:", [NSNumber numberWithInt:DATE], nil]];
        [sortingMenuButtonProps addObject:[NSArray arrayWithObjects:@"alpha_icon.png", @"onAlphabeticalSortClick:", [NSNumber numberWithInt:ALPHABET], nil]];
        
        self.sortingMenu = [[MenuBar alloc] initWithFrame:CGRectMake(0, 0, MENU_BAR_WIDTH, MENU_BAR_HEIGHT):TOP:YES];
        [self.menuBar addSection:self.sortingMenu :MENU_BAR_WIDTH];
        
        SortingType currentSortingType = [SortManager getSortingTypePreference];
        UIButton *currentSortingButton = nil;
        
        for(NSArray *sortingMenuButtonProp in sortingMenuButtonProps) {
            NSString *iconName = [sortingMenuButtonProp objectAtIndex:0];
            NSString *selectorName = [sortingMenuButtonProp objectAtIndex:1];
            SortingType sortingType = [[sortingMenuButtonProp objectAtIndex:2] intValue]; 
            
            UIButton *sortingMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [sortingMenuButton setImage:[UIImage imageNamed:iconName] forState:UIControlStateNormal];
            [sortingMenuButton addTarget:self action:NSSelectorFromString(selectorName) forControlEvents:UIControlEventTouchUpInside];
            [sortingMenuButton setClipsToBounds:YES];
            [self.sortingMenu addSection:sortingMenuButton :MENU_BAR_HEIGHT];
            
            if(sortingType == currentSortingType) {
                currentSortingButton = sortingMenuButton;
            }
        }
        
        if(currentSortingButton) {
            [currentSortingButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
        
        [[EventManager getInstance] addEventListener:MENU_BAR_SHOULD_EXPAND :^(va_list args) {
            [self.menuBar expand];
            [settingsButton setBackgroundColor:UIColorFromRGBA(0x888888, 0.5)];
        }];
        [[EventManager getInstance] addEventListener:MENU_BAR_SHOULD_COLLAPSE :^(va_list args) {
            [self.menuBar collapse];
            [settingsButton setBackgroundColor:[UIColor clearColor]];
        }];
        
        [HomeIntroOverlay show:nil];
        
        //[AdProvider show:self];
    }
    
    return self;
}

-(void)onSettingsButtonClick {
    EventType event = [self.menuBar isCollapsed] ? MENU_BAR_SHOULD_EXPAND : MENU_BAR_SHOULD_COLLAPSE; 
    [[EventManager getInstance] fireEvent:event, nil];
}

-(void)onSmartSortClick:(id)sender {
    [self selectSortingMethod:SMART :sender];
}

-(void)onDateSortClick:(id)sender {
    [self selectSortingMethod:DATE :sender];
}

-(void)onAlphabeticalSortClick:(id)sender {
    [self selectSortingMethod:ALPHABET :sender];
}

-(void)selectSortingMethod:(SortingType)sortingType :(UIButton*)sourceButton {
    SortingType currentSortingType = [SortManager getSortingTypePreference];
    
    UIView *existingSortButton = [self.sortingMenu getDefaultSection];
    if(existingSortButton) {
        [existingSortButton setBackgroundColor:[UIColor clearColor]];    
    }
    
    [sourceButton setSelected:!sourceButton.selected];
    [sourceButton setBackgroundColor:UIColorFromRGBA(0x888888, 0.5)];
    
    [self.sortingMenu setDefaultSection:sourceButton];
    [self.sortingMenu toggle];
    
    if(sortingType != currentSortingType) {
        [SortManager setSortingTypePreference:sortingType];
        [[EventManager getInstance] fireEvent:SORTING_TYPE_CHANGED, [NSNumber numberWithInt:sortingType], nil];
    }
    
    //do something with the new sorting, be sure to check that there is indeed a change
}

-(void)onCreateAlbumClick {
    
    [Logger track:@"create_album_button_click"];
    
    if(![Util checkAndAlertAuth:@"create new albums from your own & friends' photos"]) {
        return;
    }
    [AlbumCreatorView show];
}
@end
