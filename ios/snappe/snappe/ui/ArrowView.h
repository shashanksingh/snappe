//
//  ArrowView.h
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import <UIKit/UIKit.h>

@interface ArrowView : UIView
-(void)setRects:(CGRect)firstRect:(CGRect)secondRect;
@end
