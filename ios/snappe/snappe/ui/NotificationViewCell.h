//
//  NotificationViewCell.h
//  snappe
//
//  Created by Shashank on 29/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Notification.h"

@interface NotificationViewCell : UITableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier :(Notification*)notification :(CGFloat)height;
-(void)update:(Notification *)notification;
@end
