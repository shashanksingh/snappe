//
//  ProgressView.h
//  snappe
//
//  Created by Shashank on 12/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressView : UIView
+(ProgressView*)show;
-(void)setStatus:(NSString*)status;
-(void)setDone:(BOOL)success;
@end
