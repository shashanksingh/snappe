//
//  SearchBar.m
//  snappe
//
//  Created by Shashank on 14/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define LEFT_MARGIN 0
#define PADDING_BOTTOM 2
#define PADDING_RIGHT 5
#define CUSTOM_CANCEL_BUTTON_TAG 1
#define CANCEL_BUTTON_SIZE 18

#define GLOW_SIZE 3

#import "SearchBar.h"
#import <QuartzCore/QuartzCore.h>


@interface SearchBar()
@property (strong) UITextField *originalTextField;
@property (strong) UIButton *customCancelButton;
@end

@implementation SearchBar
@synthesize originalTextField;
@synthesize customCancelButton;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        for (UIView *subview in self.subviews) {
            if ([subview isKindOfClass:[UITextField class]]) {
                self.originalTextField = (UITextField *)subview;
                break;
            }
        }
        
        [self styleSearchTextField];
        [self createCancelButton];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeOriginalCancelButton) name:UITextFieldTextDidBeginEditingNotification object:self.originalTextField];
        
        [self setBackgroundColor:[UIColor blackColor]];
        
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setFrame:(CGRect)frame {
    CGRect cancelButtonFrame = self.customCancelButton.frame;
    cancelButtonFrame.size.width = CANCEL_BUTTON_SIZE;
    cancelButtonFrame.size.height = CANCEL_BUTTON_SIZE;
    cancelButtonFrame.origin.x = frame.size.width - CANCEL_BUTTON_SIZE - PADDING_RIGHT;
    cancelButtonFrame.origin.y = (frame.size.height - CANCEL_BUTTON_SIZE)/2 - PADDING_BOTTOM;
    self.customCancelButton.frame = cancelButtonFrame;
    
    [super setFrame:frame];
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame = CGRectInset(self.frame, 0, 0);//copy
    frame.size.height -= PADDING_BOTTOM;
    self.originalTextField.frame = frame;
}

- (void)styleSearchTextField {
    for (UIView *subview in self.subviews) {
        if ([subview.description hasPrefix:@"<UISearchBarBackground"]) {
            [subview removeFromSuperview];
        }
    }
    
    self.originalTextField.borderStyle = UITextBorderStyleNone;
    self.originalTextField.backgroundColor = [UIColor whiteColor];
    self.originalTextField.background = nil;
    self.originalTextField.text = @"";
    self.originalTextField.clearButtonMode = UITextFieldViewModeNever;
    self.originalTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, LEFT_MARGIN, 0)];
    self.originalTextField.placeholder = @"search albums";
    self.originalTextField.font = [UIFont fontWithName:@"ArialMT" size:12];
    
}

-(UIButton*)getOriginalCancelButton {
    for (UIView *subview in self.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            if ([subview tag] != CUSTOM_CANCEL_BUTTON_TAG) {
                return (UIButton*)subview;
            }
        }
    }
    return nil;
}

-(void)removeOriginalCancelButton {
    [[self getOriginalCancelButton] setHidden:YES];
}

-(void)clearSearchField {
    [[self getOriginalCancelButton] sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (void)createCancelButton {
    self.customCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.customCancelButton setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    [self.customCancelButton addTarget:self action:@selector(onClearButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.customCancelButton setTitle:@"clear" forState:UIControlStateNormal];
    [self.customCancelButton setClipsToBounds:YES];
    [self.customCancelButton setTag:CUSTOM_CANCEL_BUTTON_TAG];
    
    [self addSubview:self.customCancelButton];
    [self bringSubviewToFront:self.customCancelButton];
}

- (void)showCancelButton {
    [self.customCancelButton setHidden:NO];
}

- (void)hideCancelButton {
    [self.customCancelButton setHidden:YES];
}

-(void) onClearButtonClick {
    [self clearSearchField];
}

-(void)setReturnKeyType:(UIReturnKeyType)type {
    self.originalTextField.returnKeyType = type;
    self.originalTextField.enablesReturnKeyAutomatically = NO;
}
@end
