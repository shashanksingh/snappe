//
//  RootViewController.h
//  snappe
//
//  Created by Shashank on 11/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreController.h"
#import "HomeViewController.h"
#import "NotificationViewController.h"
#import "SVSegmentedControl.h"

@interface RootViewController : UIViewController
@property (strong) UIView *homeViewController;
@property (strong) NotificationViewController *notificationController;

-(void)showHome;
-(void)showNotifications;
-(void)setUnreadNotificationCountBadge:(NSUInteger)count;
@end
