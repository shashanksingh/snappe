//
//  PhotoPickerView.m
//  snappe
//
//  Created by Shashank on 21/02/13.
//
//

#import "PhotoPickerView.h"
#import "GMGridView.h"
#import "GMGridViewLayoutStrategies.h"
#import "PhotoLibraryReader.h"
#import "Util.h"
#import "Logger.h"
#import "UIView+Help.h"
#import "GraphManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <objc/runtime.h>

#define GRID_SPACING 10
#define PHOTOS_PER_ROW 3


#define PHOTO_LIST_CACHE_SIZE 60

static char const * const CELL_PHOTO_ID_PROPERTY_KEY = "snappe_picker_cell_photo_id";

@interface DuplicatePhotoSentinel : NSObject
@end

@interface OrderedPhotoSource : NSObject
@property int count;
-(Photo*)get:(int)index;
@end

@interface OrderedPhotoSource()
@property (strong) NSArray *source;
@property (strong) NSArray *currentCache;
@property int currentDatabaseOffset;
@property (strong) NSMutableDictionary *digestToMinIndex;
@property (strong) NSMutableDictionary *thumbSrcToMinIndex;
@property (strong) NSMutableDictionary *pHashToMinIndex;
@property (strong) NSMutableArray *uniqueIndexList;
@end

@implementation OrderedPhotoSource

-(id)init:(NSArray*)source {
    self = [super init];
    if(self) {
        self.source = source;
        
        if(self.source) {
            self.count = self.source.count;
        }
        else {
            self.count = [GraphManager getCount:@"Photo"];
        }
        
        self.currentCache = @[];
        self.currentDatabaseOffset = -1;
        
        self.digestToMinIndex = [NSMutableDictionary dictionary];
        self.thumbSrcToMinIndex = [NSMutableDictionary dictionary];
        self.pHashToMinIndex = [NSMutableDictionary dictionary];
        
        //dedupedCountList[i] = how many unique photos after
        //scanning i elements in the raw list
        self.uniqueIndexList = [NSMutableArray array];
        
    }
    return self;
}

-(Photo*)get:(int)index {
    
    if(self.source) {
        if(index < self.source.count) {
            return self.source[index];
        }
        return nil;
    }
    
    BOOL enoughItemsToIncludeUniqueIndex = [self readUntilSeenUniquesToIncludeIndex:index];
    if(!enoughItemsToIncludeUniqueIndex) {
        return nil;
    }

    int databaseIndex = [self getDatabaseIndexForUniqueIndex:index];
    
    if(databaseIndex >= self.currentDatabaseOffset && databaseIndex < self.currentDatabaseOffset + self.currentCache.count)
    {
        BOOL goingForward = databaseIndex >= self.currentDatabaseOffset;
        
        __block typeof(self) bself = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            if(goingForward) {
                if(bself.currentDatabaseOffset + bself.currentCache.count - databaseIndex <= PHOTO_LIST_CACHE_SIZE/4)
                {
                    [bself readBatch:databaseIndex];
                }
            }
            else {
                if(databaseIndex - bself.currentDatabaseOffset < PHOTO_LIST_CACHE_SIZE/4) {
                    [bself readBatch:MAX(0, bself.currentDatabaseOffset - ((PHOTO_LIST_CACHE_SIZE * 3)/4))];
                }
            }
        });
        
        return self.currentCache[databaseIndex - self.currentDatabaseOffset];
    }
    
    [self readBatch:databaseIndex];
    
    return self.currentCache[databaseIndex - self.currentDatabaseOffset];
}

-(BOOL)readUntilSeenUniquesToIncludeIndex:(int)index {
    
    while ([self getMaxSeenUniqueIndex] < index) {
        BOOL end = [self readBatch:self.uniqueIndexList.count];
        if(end) {
            break;
        }
    }
    
    if([self getMaxSeenUniqueIndex] < index) {
        return NO;
    }
    
    return YES;
}

-(int)getMaxSeenUniqueIndex {
    if(self.uniqueIndexList.count == 0) {
        return -1;
    }
    NSNumber *maxUniquesSeen = self.uniqueIndexList[self.uniqueIndexList.count - 1];
    return maxUniquesSeen.intValue;
}

-(int)getDatabaseIndexForUniqueIndex:(int)uniqueIndex {
    for(int i=0; i<self.uniqueIndexList.count; i++) {
        NSNumber *uniqueIndexNumber = self.uniqueIndexList[i];
        if(uniqueIndexNumber.intValue == uniqueIndex) {
            return i;
        }
    }
    return -1;
}

-(BOOL)readBatch:(int)offset {
    
    self.currentCache = [Photo getSortedPhotos:offset :PHOTO_LIST_CACHE_SIZE];
    self.currentDatabaseOffset = offset;
    
    //necessary to break recursion if we go past the end of stream
    if(self.currentCache.count == 0) {
        self.count = [self getMaxSeenUniqueIndex];
        return YES;
    }
    
    for(int i=0; i<self.currentCache.count; i++) {
        int idx = offset + i;
        
        Photo *photo = self.currentCache[i];
        NSString *digest = photo.digest;
        NSString *thumbSrc = photo.thumbSrc;
        NSString *pHash = photo.pHash;
        
        //find the index of the closest match
        NSNumber *currentMinIndexNumber = digest ? self.digestToMinIndex[digest] : nil;
        if(!currentMinIndexNumber) {
            currentMinIndexNumber = thumbSrc ? self.thumbSrcToMinIndex[thumbSrc] : nil;
            if(!currentMinIndexNumber) {
                if(pHash) {
                    NSSet *pHashes = [NSSet setWithArray:[self.pHashToMinIndex allKeys]];
                    NSString *similarPHash = [Util findSimilarPHash:pHash :pHashes];
                    if(similarPHash) {
                        currentMinIndexNumber = pHash ? self.pHashToMinIndex[similarPHash] : nil;
                    }
                }
                
            }
        }
        
        if(!currentMinIndexNumber) {
            if(digest) {
                self.digestToMinIndex[digest] = @(idx);
            }
            if(thumbSrc) {
                self.thumbSrcToMinIndex[thumbSrc] = @(idx);
            }
            if(pHash) {
                self.pHashToMinIndex[pHash] = @(idx);
            }
        }
        
        if(self.uniqueIndexList.count <= idx) {
            if(!currentMinIndexNumber) {
                if(idx == 0) {
                    self.uniqueIndexList[idx] = @(0);
                }
                else {
                    self.uniqueIndexList[idx] = @(((NSNumber*)self.uniqueIndexList[idx - 1]).intValue + 1);
                }
            }
            else {
                if(idx == 0) {
                    self.uniqueIndexList[idx] = @(0);
                }
                else {
                    self.uniqueIndexList[idx] = self.uniqueIndexList[idx - 1];
                }
            }
            
        }
        
    }
    
    return NO;
}

@end

@interface PhotoPickerView()<GMGridViewDataSource, GMGridViewActionDelegate>
@property PhotoPickerType *type;
@property (strong) OrderedPhotoSource *photoSource;
@property (strong) NSMutableSet *selectedPhotoIDs;
@property (strong) GMGridView *photoGrid;
@end

@implementation PhotoPickerView

- (id)initWithFrame:(CGRect)frame:(PhotoPickerType)type
{
    self = [super initWithFrame:frame];
    if (self) {
        self.type = type;
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

-(void)populate {
    [self populate:nil];
}

-(void)populate:(NSArray*)photos {
    self.photoSource = [[OrderedPhotoSource alloc] init:photos];
    
    self.selectedPhotoIDs = [NSMutableSet set];
    if(self.photoGrid) {
        [self.photoGrid removeFromSuperview];
    }
    
    self.photoGrid = [[GMGridView alloc] initWithFrame:self.bounds];
    [self.photoGrid setShowsVerticalScrollIndicator:NO];
    [self.photoGrid setShowsHorizontalScrollIndicator:NO];
    [self.photoGrid setDataSource:self];
    [self.photoGrid setActionDelegate:self];
    
    [self.photoGrid setBackgroundColor:[UIColor clearColor]];
    self.photoGrid.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.photoGrid.style = GMGridViewStylePush;
    self.photoGrid.itemSpacing = GRID_SPACING;
    self.photoGrid.minEdgeInsets = UIEdgeInsetsMake(GRID_SPACING, GRID_SPACING, GRID_SPACING, GRID_SPACING);
    self.photoGrid.centerGrid = NO;
    self.photoGrid.layoutStrategy = [GMGridViewLayoutStrategyFactory strategyFromType:GMGridViewLayoutVertical];
    [self addSubview:self.photoGrid];
}

-(NSArray*)getSelectedPhotos {
    NSDictionary *idToPhoto = [GraphManager getGraphObjectsById:[self.selectedPhotoIDs allObjects] WithEntityName:@"Photo"];
    return [idToPhoto allValues];
}


- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView {
    return self.photoSource.count;
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    CGFloat size = (gridView.frame.size.width - ((PHOTOS_PER_ROW + 1) * GRID_SPACING))/PHOTOS_PER_ROW;
    return CGSizeMake(size, size);
}

- (GMGridViewCell *)GMGridView:(GMGridView*)gridView cellForItemAtIndex:(NSInteger)index
{
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    
    if (!cell)
    {
        cell = [[GMGridViewCell alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    }
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    Photo *photo = [self.photoSource get:index];
    if(photo) {
        cell.contentView = [photo getUIImageView:size.width :size.height :YES :NO];
        [self setPhotoIDForCell:photo.id :cell.contentView];
        [self updateMask:cell.contentView];
    }
    else {
        NSString *message = [NSString stringWithFormat:@"PhotoPicker nil photo for index: %d", index];
        [Logger logError:message :nil];
        [gridView reloadData];
        return nil;
    }
    
    if(self.type == SELECT) {
        [cell setHelpText:@"tap on a photo to select it"];
    }
    else {
        [cell setHelpText:@"tap on a photo to make it private. private photos are not uploaded or shared"];
    }
    return cell;
}

- (BOOL)GMGridView:(GMGridView *)gridView shouldAllowShakingBehaviorWhenMovingCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    return NO;
}

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
 
    UIView *photoImageView = [gridView cellForItemAtIndex:position].contentView;
    NSString *photoID = [self getPhotoIDForCell:photoImageView];
    BOOL wasSelected = [self.selectedPhotoIDs containsObject:photoID];
    
    if(wasSelected) {
        [self.selectedPhotoIDs removeObject:photoID];
    }
    else {
        [self.selectedPhotoIDs addObject:photoID];
    }
    [self updateMask:photoImageView];
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    return;
}

- (void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
    return;
}

-(void)updateMask:(UIView*)cell {
    NSString *photoID = [self getPhotoIDForCell:cell];
    if([self.selectedPhotoIDs containsObject:photoID]) {
        if(self.type == SELECT) {
            [Util setSelectedImageMask:cell];
        }
        else {
            [Util setPrivateImageMask:cell];
        }
    }
    else {
        [Util clearImageMask:cell];
    }
}

-(void)setPhotoIDForCell:(NSString*)photoID:(UIView*)cell {
    objc_setAssociatedObject(cell, CELL_PHOTO_ID_PROPERTY_KEY, photoID, OBJC_ASSOCIATION_RETAIN);
}

-(NSString*)getPhotoIDForCell:(UIView*)cell {
    return objc_getAssociatedObject(cell, CELL_PHOTO_ID_PROPERTY_KEY);
}
@end
