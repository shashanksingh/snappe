//
//  GalleryViewController.h
//  snappe
//
//  Created by Shashank on 25/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumAccessor.h"

@interface GalleryView : UIView<UIGestureRecognizerDelegate, UIScrollViewDelegate>
- (id)init:(Album*)album;
@end
