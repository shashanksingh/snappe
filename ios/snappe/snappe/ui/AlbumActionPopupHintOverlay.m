//
//  AlbumActionPopupHintOverlay.m
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import "AlbumActionPopupHintOverlay.h"
#import "UIView+Help.h"

@implementation LocalAlbumActionPopupHintOverlay
+(NSString*)getIntroID {
    return @"album_action_popup_local";
}
@end

@implementation RemoteAlbumActionPopupHintOverlay
+(NSString*)getIntroID {
    return @"album_action_popup_remote";
}
@end


@implementation AlbumActionPopupHintOverlay

-(NSArray*)getParameters {
    NSMutableArray *hints = [NSMutableArray array];
    
    NSArray *actionViews = self.data;
    
    for(int i=0; i<actionViews.count; i++) {
        UIView *actionView = actionViews[i];
        
        CGRect frame = [actionView convertRect:actionView.bounds toView:self];
        BOOL lowerHalf = frame.origin.y > self.frame.size.height * 0.5;
        
        IntroHint *hint = [IntroHint new];
        hint.labelText = [actionView helpText];
        if(lowerHalf) {
            hint.labelRect = CGRectMake(self.frame.size.width * 0.05 + (i * 70), CGRectGetMinY(frame) - 80 - (i * 90), 100, 60);
            hint.hintTargetLocation = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
        }
        else {
            hint.hintTargetLocation = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
            hint.labelRect = CGRectMake(self.frame.size.width * 0.05 + (i * 70), CGRectGetMaxY(frame) + 40 + (i * 90), 100, 60);
        }
    
        [hints addObject:hint];
    }
    
    return hints;
}
@end
