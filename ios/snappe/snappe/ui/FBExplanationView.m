//
//  FBExplanationView.m
//  snappe
//
//  Created by Shashank on 06/06/13.
//
//

#import "FBExplanationView.h"
#import "Util.h"
#import "Logger.h"
#import "RTLabel.h"
#import "Carousel.h"
#import <QuartzCore/QuartzCore.h>

#define MAIN_WIDTH 280
#define MAIN_PADDING 30

#define HEADER_SECTION_HEIGHT 30
#define HEADER_PADDING 5

#define MAIN_MESSAGE_SECTION_HEIGHT 60
#define MAIN_MESSAGE_PADDING 5

#define EXPLANATION_SECTION_HEIGHT 80
#define SECTION_PADDING 10

#define ICON_SECTION_WIDTH 54
#define ICON_SIZE 44

#define MESSAGE_SECTION_X_PADDING 5

#define FB_LOGIN_BUTTON_SECTION_HEIGHT 80
#define FB_LOGIN_BUTTON_HEIGHT 80
#define FB_LOGIN_BUTTON_WIDTH 220

@interface FBExplanationView()
@property UIView *contentView;
@end


@implementation FBExplanationView

- (id)init {
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75f]];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        [self addGestureRecognizer:tapRecognizer];
        
        [self draw];
    }
    return self;
}

+(FBExplanationView*) getInstance {
    static FBExplanationView *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[FBExplanationView alloc] init];
    });
    return sharedInstance;
}


+(void)show {
    FBExplanationView *view = [self getInstance];
    [Util showModalView:view];
    [Logger track:@"fb_explanation_view_shown"];
}

-(void)draw {
    
    CGRect mainRect = CGRectMake(0, 0, MAIN_WIDTH + 2 * MAIN_PADDING, self.bounds.size.height);
    mainRect = CGRectInset(mainRect, MAIN_PADDING, 0);
    CGRect rect = CGRectMake(0, 0, mainRect.size.width, mainRect.size.height);
    
    NSMutableArray *itemsToAdd = [NSMutableArray array];
    
    CGRect mainMessageRect, fbLoginButtonSectionRect;
    CGRectDivide(rect, &mainMessageRect, &rect, MAIN_MESSAGE_SECTION_HEIGHT, CGRectMinYEdge);
    
    UIView *mainMessageView = [[UIView alloc] initWithFrame:mainMessageRect];
    mainMessageView.backgroundColor = UIColorFromRGBA(0x343434, 0.9);
    [itemsToAdd addObject:mainMessageView];
    
    UILabel *mainMessageLabel = [[UILabel alloc] initWithFrame:CGRectInset(mainMessageView.bounds, MAIN_MESSAGE_PADDING, MAIN_MESSAGE_PADDING)];
    mainMessageLabel.backgroundColor = [UIColor clearColor];
    mainMessageLabel.lineBreakMode = UILineBreakModeWordWrap;
    mainMessageLabel.numberOfLines = 0;
    mainMessageLabel.adjustsFontSizeToFitWidth = YES;
    [mainMessageView addSubview:mainMessageLabel];
    
    mainMessageLabel.text = @"Facebook Permissions";
    
    mainMessageLabel.textAlignment = NSTextAlignmentCenter;
    mainMessageLabel.textColor = UIColorFromRGBA(0xCCCCCC, 0.9);
    mainMessageLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
    
    NSArray *permissions = @[
         @[@"Photos", @"so that we can we can list albums in Snappe which you can then share & sync", @"camera.png"],
         @[@"Location, Checkins", @"so that we can suggest friends to share albums with and you have to type less", @"checkin.png"],
         @[@"Email", @"so that we can deliver any important service related information to you", @"envelope.png"]
    ];
    
    for(int i=0; i<permissions.count; i++) {
        
        NSArray *permissionInfo = permissions[i];
        
        CGRect sectionRect;
        CGRectDivide(rect, &sectionRect, &rect, EXPLANATION_SECTION_HEIGHT, CGRectMinYEdge);
        
        UIView *sectionView = [[UIView alloc] initWithFrame:sectionRect];
        [itemsToAdd addObject:sectionView];
        sectionView.backgroundColor = i%2 == 0 ? UIColorFromRGB(0xF9F9F9) : UIColorFromRGB(0xFFFFFF);
        
        
        CGRect rowRect = CGRectInset(sectionView.bounds, SECTION_PADDING, SECTION_PADDING);
        
        CGRect iconSectionRect, messageRect;
        CGRectDivide(rowRect, &iconSectionRect, &messageRect, ICON_SECTION_WIDTH, CGRectMinXEdge);
        
        CGRect iconRect = CGRectMake(0, 0, ICON_SIZE, ICON_SIZE);
        iconRect = [Util centerRect:iconRect :[Util getRectCenter:iconSectionRect]];

        UIImageView *icon = [[UIImageView alloc] initWithFrame:iconRect];
        icon.image = [UIImage imageNamed:permissionInfo[2]];
        [sectionView addSubview:icon];
        
        messageRect = CGRectInset(messageRect, MESSAGE_SECTION_X_PADDING, 0);
        
        RTLabel *explanation = [[RTLabel alloc] initWithFrame:messageRect];
        explanation.text = [NSString stringWithFormat:@"<b>%@</b>: %@", permissionInfo[0], permissionInfo[1]];
        explanation.font = [UIFont fontWithName:@"NotoSerif" size:11];
        
        [sectionView addSubview:explanation];
    }
    
    CGRectDivide(rect, &fbLoginButtonSectionRect, &rect, FB_LOGIN_BUTTON_SECTION_HEIGHT, CGRectMinYEdge);
    
    CGRect fbLoginButtonRect = CGRectMake(0, 0, FB_LOGIN_BUTTON_WIDTH, FB_LOGIN_BUTTON_HEIGHT);
    fbLoginButtonRect = [Util centerRect:fbLoginButtonRect :[Util getRectCenter:fbLoginButtonSectionRect]];
    
    UIButton *fbLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [fbLoginButton setImage:[UIImage imageNamed:@"connect_using_facebook.png"] forState:UIControlStateNormal];
    [fbLoginButton setAlpha:1.0f];
    [fbLoginButton setFrame:fbLoginButtonRect];
    [fbLoginButton setClipsToBounds:YES];
    [fbLoginButton setBackgroundColor:[UIColor clearColor]];
    [fbLoginButton addTarget:self action:@selector(onFacebookLoginClick) forControlEvents:UIControlEventTouchUpInside];
    [itemsToAdd addObject:fbLoginButton];
    
    
    CGFloat totalHeight = 0.0f;
    for(UIView *itemToAdd in itemsToAdd) {
        totalHeight += itemToAdd.bounds.size.height;
    }
    
    mainRect.size.height = totalHeight;
    mainRect = [Util centerRect:mainRect :[Util getRectCenter:self.bounds]];
    mainRect.origin.y = MAIN_PADDING;
    UIView *container = [[UIView alloc] initWithFrame:mainRect];
    for(UIView *itemToAdd in itemsToAdd) {
        [container addSubview:itemToAdd];
    }
    
    UIColor *bgColor = UIColorFromRGB(0xFFFFFF);
    container.backgroundColor = bgColor;
    [container.layer setBorderColor: bgColor.CGColor];
    [container.layer setBorderWidth:0];
    [Util drawFastShadow:container];
    [container.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [container.layer setShadowRadius:3.0];
    [container.layer setShadowOpacity:0.9];
    [self addSubview:container];
    
    self.contentView = container;
}

-(void)onFacebookLoginClick {
    [Carousel onFacebookLoginClick];
    [self hide];
    [Logger track:@"fb_explanation_view_action" :@{@"action": @"fb_login_click"}];
}

-(void)onTap:(UIGestureRecognizer*)gestureRecognizer {
    if(!CGRectContainsPoint(self.contentView.frame, [gestureRecognizer locationInView:self])) {
        [self hide];
        [Logger track:@"fb_explanation_view_action" :@{@"action": @"tap_hide"}];
    }
}

-(void)hide {
    [self removeFromSuperview];
}

@end
