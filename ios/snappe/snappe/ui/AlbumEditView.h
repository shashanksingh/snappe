//
//  AlbumEditViewController.h
//  snappe
//
//  Created by Shashank on 19/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Album.h"

@interface AlbumEditView : UIView <UITextViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>
+(void)show:(Album*)album;
@end
