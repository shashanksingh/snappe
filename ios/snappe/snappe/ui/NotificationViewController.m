//
//  NotificationViewControllerViewController.m
//  snappe
//
//  Created by Shashank on 13/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationManager.h"
#import "EventManager.h"
#import "NotificationAccessor.h"
#import "NotificationViewCell.h"
#import "RootViewController.h"
#import "UIController.h"
#import "Util.h"
#import <QuartzCore/QuartzCore.h>

#define MAX_NOTIFICATIONS 64
#define THUMB_WIDTH 64
#define THUMB_HEIGHT 64
#define ROW_HEIGHT 120


@interface NotificationViewController ()
@property (strong) NSMutableArray *notifications;
@property (strong) UIView *noNotificationView;
@end

@implementation NotificationViewController
@synthesize notifications;
@synthesize noNotificationView;

-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if (self) {
        self.dataSource = self;
        self.delegate = self;
        
        [self setBackgroundColor:[UIColor clearColor]];
        self.separatorColor = [UIColor clearColor];
        
        self.notifications = [[NSMutableArray alloc] init];
        
        NSArray *notifications = [[NotificationManager getInstance] getNotifications:MAX_NOTIFICATIONS];
        [self addNewNotifications:notifications];
        
        [[EventManager getInstance] addEventListener:NEW_NOTIFICATION_AVAILABLE :^(va_list args) {
            NSArray *newNotifications = va_arg(args, NSArray*);
            [self addNewNotifications:newNotifications];
            
        }];
        [[EventManager getInstance] addEventListener:PENDING_NOTIFICATION_COUNT_CHANGED :^(va_list args) {
            NSNumber *pendingCount = va_arg(args, NSNumber*);
            [self updatePendingNotificationBadges:[pendingCount unsignedIntValue]];
        }];
        [[EventManager getInstance] addEventListener:NOTIFICATION_STATUS_CHANGED :^(va_list args) {
            Notification *notification = va_arg(args, Notification*);
            [self updateNotification:notification];
        }];
        [[EventManager getInstance] addEventListener:NOTIFICATION_MARKED_SEEN :^(va_list args) {
            Notification *notification = va_arg(args, Notification*);
            [self updateNotification:notification];
        }];
        
        [self addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:nil];
        
        [self updatePendingNotificationBadges:[[NotificationManager getInstance] getPendingNotificationCount]];
        
    }
    return self;
}

-(void)dealloc {
    [self removeObserver:self forKeyPath:@"hidden"];
}

-(void) addNewNotifications:(NSArray*)newNotifications {
    NSMutableSet *existingNotificationIDs = [[NSMutableSet alloc] init];
    for(Notification *notification in self.notifications) {
        [existingNotificationIDs addObject:notification.id];
    }
    
    
    for(unsigned int i=0; i<[newNotifications count]; ++i) {
        Notification *newNotification = [newNotifications objectAtIndex:i];
        
        NSString *newNotificationID = newNotification.id;
        if([existingNotificationIDs containsObject:newNotificationID]) {
            continue;
        }
        [self.notifications addObject:newNotification];
    }
    
    self.notifications = [[self.notifications sortedArrayUsingComparator:^NSComparisonResult(Notification *n1, Notification *n2) 
    {
        return [n2.timestamp doubleValue] - [n1.timestamp doubleValue];
    }] mutableCopy];
    
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    for(unsigned int i=0; i<[self.notifications count]; ++i) {
        Notification *notification = [self.notifications objectAtIndex:i];
        if(![existingNotificationIDs containsObject:notification.id]) {
            NSUInteger indexArr[] = {0, i};
            NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:indexArr length:2];
            [indexPaths addObject:indexPath];
        }
    }

    [self insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
    [self showNoNotificationMessage:[self.notifications count] == 0];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.notifications count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NotificationCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    Notification *notification = [notifications objectAtIndex:[indexPath row]];
    if(!cell) {
        
        CGFloat height = [self tableView:tableView heightForRowAtIndexPath:indexPath];
        cell = [[NotificationViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier :notification :height];
    }
    else {
        [(NotificationViewCell*)cell update:notification];
    }
    
    if(!self.hidden && [notification.pending boolValue]) {
        [[NotificationManager getInstance] markNotificationShown:notification];    
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ROW_HEIGHT;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self markVisibleNotificationsSeen];
}

-(void)updatePendingNotificationBadges:(NSUInteger)pendingCount {
    NSString *badge = nil;
    if(pendingCount > 0) {
        badge = [NSString stringWithFormat:@"%d", pendingCount];
    }
    RootViewController *rvc = [[UIController getInstance] getUIControl:ROOT_VIEW_CONTROLLER];
    [rvc setUnreadNotificationCountBadge:pendingCount];
}

-(void)updateNotification:(Notification*)notification {
    for(NSUInteger i=0; i<[self.notifications count]; ++i) {
        Notification *n = [self.notifications objectAtIndex:i];
        
        if([n.id isEqualToString:notification.id]) {
            NotificationViewCell *notificationViewCell = (NotificationViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            [self.notifications replaceObjectAtIndex:i withObject:notification];
            //DLog(@"status: %@", [notification.data objectForKey:@"status"]);
            [notificationViewCell update:notification];
            return;
        }
    }
    
    DLog(@"warning: processing status update for missing notification with id: %@", notification.id);
}

-(void)markVisibleNotificationsSeen {
    NSArray *visibleNotifications = [self getVisibleNotifications];
    for(Notification *notification in visibleNotifications) {
        if([[notification pending] boolValue]) {
            [[NotificationManager getInstance] markNotificationShown:notification];
        }
    }
}

-(void)updateNotifications {
    //these updates are besides the notification updates that
    //get fired from outside, these will take care of updating
    //the timestamp
    for(NSUInteger i=0; i<[self.notifications count]; ++i) {
        Notification *n = [self.notifications objectAtIndex:i];
        NotificationViewCell *notificationViewCell = (NotificationViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        [notificationViewCell update:n];
    }
}

-(NSArray*)getVisibleNotifications {
    NSMutableArray *rv = [[NSMutableArray alloc] init];
    if(self.hidden) {
        return rv;
    }
    
    //this extra call is due to a bug in iOS < 5
    //http://stackoverflow.com/questions/4099188/uitableviews-indexpathsforvisiblerows-incorrect
    [self visibleCells];
    
    NSArray *visibleIndexPaths = [self indexPathsForVisibleRows];
    for(NSIndexPath *visiblePath in visibleIndexPaths) {
        NSUInteger i = visiblePath.row;
        Notification *visibleNotification = [self.notifications objectAtIndex:i];
        [rv addObject:visibleNotification];
    }
    return rv;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"hidden"]) {
        if([object valueForKeyPath:keyPath] != [NSNull null]) {
            BOOL hidden = [[object valueForKeyPath:keyPath] boolValue];
            if(!hidden) {
                [self onShow];
            }
            else {
                [self onHide];
            }
        }
    }
}

-(void)onShow {
    [self markVisibleNotificationsSeen];
    [self updateNotifications];
}

-(void)onHide {
        
}

-(void)showNoNotificationMessage:(BOOL)show {
    if(!self.noNotificationView) {
        CGRect rect = CGRectMake(0, 0, self.frame.size.width, ROW_HEIGHT);
        rect = CGRectInset(rect, 40, 0);
        
        rect = [Util centerRect:rect :[Util getRectCenter:self.bounds]];
        self.noNotificationView = [[UIView alloc] initWithFrame:rect];
        [self addSubview:self.noNotificationView];
        
        [self.noNotificationView setBackgroundColor:[UIColor whiteColor]];
        [self.noNotificationView.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        [self.noNotificationView.layer setBorderWidth: 0];
        [Util drawFastShadow:self.noNotificationView];
        [self.noNotificationView.layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
        [self.noNotificationView.layer setShadowRadius:3.0];
        [self.noNotificationView.layer setShadowOpacity:1.0];
        self.noNotificationView.layer.opacity  = 0.95;
        
        CGRect messageRect = self.noNotificationView.bounds;
        messageRect = CGRectInset(messageRect, 20, 20);
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:messageRect];
        [self.noNotificationView addSubview:messageLabel];
        messageLabel.backgroundColor = [UIColor clearColor];
        messageLabel.center = [Util getRectCenter:self.noNotificationView.bounds];
        messageLabel.textAlignment = UITextAlignmentCenter;
        messageLabel.textColor = UIColorFromRGB(0x0);
        messageLabel.font = [UIFont fontWithName:@"MarkerFelt-Thin" size:13.0f];
        messageLabel.numberOfLines = 0;
        messageLabel.lineBreakMode = UILineBreakModeWordWrap;
        messageLabel.text = @"There are no notifications yet! As you use Snappe, notifications will start to appear here";
    }
    
    self.noNotificationView.hidden = !show;
}
@end
