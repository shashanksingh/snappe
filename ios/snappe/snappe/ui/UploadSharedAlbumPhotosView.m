//
//  UploadSharedAlbumPhotosView.m
//  snappe
//
//  Created by Shashank on 27/02/13.
//
//

#import "UploadSharedAlbumPhotosView.h"
#import "Macros.h"
#import "Util.h"
#import "PhotoPickerView.h"
#import "PhotoLibraryReader.h"
#import "Logger.h"
#import "PhotoUploadOperation.h"
#import "GraphManager.h"
#import "BButton.h"
#import <QuartzCore/QuartzCore.h>

#define HEIGHT 360
#define WIDTH 250
#define HEADER_HEIGHT 30
#define MAIN_MESSAGE_HEIGHT 120
#define BUTTON_SECTION_HEIGHT 80

#define HEADER_PADDING 5
#define MAIN_MESSAGE_PADDING 10

#define BUTTON_WIDTH 100
#define BUTTON_HEIGHT 30

@interface UploadSharedAlbumPhotosView()<UIGestureRecognizerDelegate>
@property (strong) NSArray *photos;
@property (strong) NSMutableDictionary *urlToHash;
@property (strong) PhotoPickerView *photoPicker;
@property (strong) UIView *contentView;
@end

@implementation UploadSharedAlbumPhotosView

- (id)init {
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if(self) {
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75f]];
        
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        tapRecognizer.delegate = self;
        tapRecognizer.cancelsTouchesInView = NO;
        [self addGestureRecognizer:tapRecognizer];
        
        
        
        [self drawOnRect:frame];
        //[Util showPopup:nil :self :YES :nil];
    }
    return  self;
}

+(UploadSharedAlbumPhotosView *) getInstance {
    static UploadSharedAlbumPhotosView *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[UploadSharedAlbumPhotosView alloc] init];
    });
    return sharedInstance;
}


+(void)show:(NSArray*)photosToUploadIDs {
    UploadSharedAlbumPhotosView *view = [self getInstance];
    
    NSArray *photosToUpload = [[GraphManager getGraphObjectsById:photosToUploadIDs WithEntityName:@"Photo"] allValues];
    
    view.photos = photosToUpload;
    
    [view.photoPicker populate:photosToUpload];
    [Util showModalView:view];
    
    [Logger track:@"uplod_shared_album_photos_view_shown" :@{@"photo_count" : @(photosToUploadIDs.count)}];
}

-(void)drawOnRect:(CGRect)frame {
    CGRect mainFrame = CGRectMake(0, 0, WIDTH, HEIGHT);
    mainFrame = [Util centerRect:mainFrame :[Util getRectCenter:frame]];
    
    self.contentView = [[UIView alloc] initWithFrame:mainFrame];
    [self addSubview:self.contentView];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    [self.contentView.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [Util drawFastShadow:self.contentView];
    [self.contentView.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [self.contentView.layer setShadowRadius:3.0];
    [self.contentView.layer setShadowOpacity:1.0];
    
    CGRect headerBox, mainMessageBox, photoGridBox, doButtonBox;
    CGRect r = self.contentView.bounds;
    CGRectDivide(r, &headerBox, &r, HEADER_HEIGHT, CGRectMinYEdge);
    CGRectDivide(r, &mainMessageBox, &r, MAIN_MESSAGE_HEIGHT, CGRectMinYEdge);
    CGRectDivide(r, &doButtonBox, &photoGridBox, BUTTON_SECTION_HEIGHT, CGRectMaxYEdge);
    
    
    UIView *headerView = [[UIView alloc] initWithFrame:headerBox];
    [self.contentView addSubview:headerView];
    [headerView setBackgroundColor:UIColorFromRGBA(0x343434, 0.9)];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectInset(headerView.bounds, HEADER_PADDING, HEADER_PADDING)];
    headerLabel.backgroundColor = [UIColor clearColor];
    [headerView addSubview:headerLabel];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    [headerLabel setTextColor:UIColorFromRGBA(0xFFFFFF, 0.9)];
    [headerLabel setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:13]];
    [headerLabel setTextAlignment:NSTextAlignmentCenter];
    [headerLabel setText:@"New Photos Found"];
    
    UIView *mainMessageView = [[UIView alloc] initWithFrame:mainMessageBox];
    [self.contentView addSubview:mainMessageView];
    mainMessageView.backgroundColor = [UIColor clearColor];
    
    
    UILabel *mainMessageLabel = [[UILabel alloc] initWithFrame:CGRectInset(mainMessageView.bounds, MAIN_MESSAGE_PADDING, MAIN_MESSAGE_PADDING)];
    [mainMessageView addSubview:mainMessageLabel];
    mainMessageLabel.lineBreakMode = UILineBreakModeWordWrap;
    mainMessageLabel.numberOfLines = 0;
    [mainMessageLabel setBackgroundColor:[UIColor clearColor]];
    [mainMessageLabel setFont:[UIFont fontWithName:@"Sansation_Regular" size:10]];
    [mainMessageLabel setTextColor:UIColorFromRGBA(0x7C3030, 1)];
    [mainMessageLabel setTextAlignment:NSTextAlignmentCenter];
    mainMessageLabel.text = @"There are new photos in some of your shared albums. Please tap on the ones you want to keep private, rest will be available to friends the albums are already shared with";
    
    UIView *photoGridScrollerContainer = [[UIView alloc] initWithFrame:photoGridBox];
    [self.contentView addSubview:photoGridScrollerContainer];
    [photoGridScrollerContainer setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 0.95)];
    [photoGridScrollerContainer.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [photoGridScrollerContainer.layer setBorderWidth:2];
    
    CGRect photoGridRect = photoGridScrollerContainer.bounds;
    self.photoPicker = [[PhotoPickerView alloc] initWithFrame:photoGridRect:PRIVATIZE];
    [photoGridScrollerContainer addSubview:self.photoPicker];
    
    CGRect buttonRect = CGRectMake(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
    buttonRect = [Util centerRect:buttonRect :[Util getRectCenter:doButtonBox]];
    
    UIButton *doButton = [[BButton alloc] initWithFrame:buttonRect type:BButtonTypeSuccess];
    [doButton setTitle:@"OK" forState:UIControlStateNormal];
    [doButton addTarget:self action:@selector(onDoButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:doButton];
}

-(void)onTap:(UIGestureRecognizer*)gestureRecognizer {
    /*if(!CGRectContainsPoint(self.contentView.frame, [gestureRecognizer locationInView:self])) {
        [self removeFromSuperview];
    }*/
}

-(void)onDoButtonClick {
    [self removeFromSuperview];
    
    NSArray *privatePhotos = [self.photoPicker getSelectedPhotos];
    NSSet *privatePhotoIDSet = [NSSet setWithArray:[privatePhotos valueForKey:@"id"]];
    
    NSMutableArray *photosToUpload = [NSMutableArray array];
    
    for(Photo *photo in self.photos) {
        if(![privatePhotoIDSet containsObject:photo.id]) {
            [photosToUpload addObject:photo];
        }
    }
    
    [Logger track:@"uplod_shared_album_photos_do_button_clicked" :@{@"ptivate_count" : @(privatePhotoIDSet.count), @"shared_count" : @(photosToUpload.count)}];
    
    //marking photos as private is taken care of by the operation
    if(photosToUpload.count > 0 || privatePhotos.count > 0) {
        PhotoUploadOperation *op = [[PhotoUploadOperation alloc] init:privatePhotos :photosToUpload];
        [Util executeExtOp:op];
    }
    
}
@end
