//
//  HelpOverlay.h
//  snappe
//
//  Created by Shashank on 17/03/13.
//
//

#import <UIKit/UIKit.h>

@interface HelpOverlay : UIView
+(HelpOverlay*) getInstance;
+(void)hide;
+(void)show;
@end
