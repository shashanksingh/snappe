//
//  AlbumPhotoView.m
//  snappe
//
//  Created by Shashank on 11/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AlbumPhotoView.h"
#import "GMGridView.h"
#import "GMGridViewLayoutStrategies.h"
#import "AlbumAccessor.h"
#import "PhotoAccessor.h"
#import "Util.h"
#import "SnappeHTTPService.h"
#import "GraphManager.h"
#import "Logger.h"
#import "UIView+Help.h"

#define GRID_SPACING 10
#define PHOTOS_PER_ROW 3

@interface AlbumPhotoView() <GMGridViewDataSource, GMGridViewActionDelegate>
@property (strong) Album *album;
@end

@implementation AlbumPhotoView

- (id)initWithFrame:(CGRect)frame :(Album*)album
{
    self = [super initWithFrame:frame];
    if (self) {
        self.album = album;
        [self setBackgroundColor:[UIColor clearColor]];
        
        CGRect gridFrame = frame;
        gridFrame.origin = CGPointMake(0, 0);
        
        GMGridView *photoGrid = [[GMGridView alloc] initWithFrame:gridFrame];
        [photoGrid setShowsVerticalScrollIndicator:NO];
        [photoGrid setShowsHorizontalScrollIndicator:NO];
        [photoGrid setDataSource:self];
        [photoGrid setActionDelegate:self];
        
        [photoGrid setBackgroundColor:[UIColor clearColor]];        
        photoGrid.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        photoGrid.style = GMGridViewStylePush;
        photoGrid.itemSpacing = GRID_SPACING;
        photoGrid.minEdgeInsets = UIEdgeInsetsMake(GRID_SPACING, GRID_SPACING, GRID_SPACING, GRID_SPACING);
        photoGrid.centerGrid = NO;
        photoGrid.layoutStrategy = [GMGridViewLayoutStrategyFactory strategyFromType:GMGridViewLayoutVertical];
        [self addSubview:photoGrid];
    }
    return self;
}

-(void)maskPhotoIfPrivate:(Photo*)photo :(UIView*)imageView {
    if(![photo.markedPrivate boolValue]) {
        [Util clearImageMask:imageView];
    }
    else {
        [Util setPrivateImageMask:imageView];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView {
    return [self.album.photos count];
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    CGFloat size = (gridView.frame.size.width - ((PHOTOS_PER_ROW + 1) * GRID_SPACING))/PHOTOS_PER_ROW;
    return CGSizeMake(size, size);
}

- (GMGridViewCell *)GMGridView:(GMGridView*)gridView cellForItemAtIndex:(NSInteger)index
{
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    
    if (!cell) 
    {
        cell = [[GMGridViewCell alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    }
    
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    Photo *photo = [self.album.photos objectAtIndex:index];
    UIImageView *photoImageView = [photo getUIImageView:size.width :size.height :YES :NO];
    cell.contentView = photoImageView;
    [cell setHelpText:@"tap on a photo to make it private, private photos from your phone are not shared with anyone"];
    
    [self maskPhotoIfPrivate:photo :photoImageView];
    
    return cell;
}

- (BOOL)GMGridView:(GMGridView *)gridView shouldAllowShakingBehaviorWhenMovingCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    return NO;
}

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    UIView *photoImageView = [gridView cellForItemAtIndex:position].contentView;
    Photo *photo = [self.album.photos objectAtIndex:position];
    [photo toogleIsPrivate];
    [self maskPhotoIfPrivate:photo :photoImageView];
    
    if([photo.markedPrivate boolValue]) {
        [[SnappeHTTPService getInstance] markPhotosPrivate:@[photo] :^{
            [Logger track:@"photo_marked_private" :[NSDictionary dictionaryWithObjectsAndKeys:photo.id, @"photo_id", nil]];
        } :^(NSError *error) {
            [Logger logError:@"error in marking photo private":error];
            [Util alertError:error:@"There was an error in making your photo private. Please try again later"];
        }];
    }
    else {
        [[SnappeHTTPService getInstance] markPhotosPublic:@[photo] :^{
            DLog(@"successfully marked photo as public");
        } :^(NSError *error) {
            [Logger logError:@"error in marking photo public":error];
            [Util alertError:error:@"There was an error in making your photo non-private. Please try again later"];
        }];
    }
    
    [[GraphManager getInstance] savePersistentContext];
    
    
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    return;
}

- (void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
    return;
}


@end
