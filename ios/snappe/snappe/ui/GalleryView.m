//
//  GalleryViewController.m
//  snappe
//
//  Created by Shashank on 25/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GalleryView.h"
#import "PhotoAccessor.h"
#import <QuartzCore/QuartzCore.h>
#import "EventManager.h"
#import "Util.h"
#import "Logger.h"
#import "HelpOverlay.h"

#define CONTENT_INSET 0
#define PREVIEWS_PER_PAGE 8
#define PREVIEW_INSET 0.5
#define SELECTED_PREVIEW_INSET 4
#define POSTIION_VIEW_Y 15
#define POSITION_VIEW_WIDTH 60
#define POSITION_VIEW_HEIGHT 33
#define CLOSE_BUTTON_SIZE 24
#define CLOSE_BUTTON_MARGIN 20

#define CONTROL_AUTO_HIDE_INTERVAL 5.0
#define CONTROL_SHOW_HIDE_ANIMATION_DURATION 0.4

#define PREVIEW_MASK_TAG 64

@interface GalleryView ()
@property (retain) Album *album;
@property (strong) NSArray *availablePhotos;
@property (strong) UIView *contentArea;
@property (strong) UIScrollView *fullViewGrid;
@property (strong) UIScrollView *previewGrid;
@property (strong) UITextView *positionView;
@property (strong) UIButton *closeButton;
@property (strong) NSTimer *controlHideTimer;
@property (strong) NSString *appActiveListenerID;
@end

@implementation GalleryView
@synthesize album;
@synthesize contentArea;
@synthesize fullViewGrid;
@synthesize previewGrid;
@synthesize positionView;
@synthesize closeButton;
@synthesize controlHideTimer;
@synthesize appActiveListenerID;

- (id)init:(Album*)album
{
    //these few lines seem to be a working quick fix of top offset
    //because of hidden navigation bar
    //TODO: find a better solution
    CGRect frame = [Util getAppFrame];
    frame.origin.y = -20;
    frame.size.height += 20;
    
    self = [super initWithFrame:frame];
    if (self) {
        self.album = album;
        self.availablePhotos = [self.album getPhotos:YES :YES :NO];
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        
        [self rotateToLandscape];
        [self drawWithFrame:self.bounds];
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:1.0f]];
        
        self.controlHideTimer = [NSTimer scheduledTimerWithTimeInterval:CONTROL_AUTO_HIDE_INTERVAL target:self selector:@selector(hideControls) userInfo:nil repeats:YES];
        
        
        __weak typeof(self) bself = self;
        self.appActiveListenerID = [[EventManager getInstance] addEventListener:APPLICATION_ACTIVE :^(va_list args)
        {
            if(bself) {
                [bself resetControlHideTimer];
            }
        }];
        
    }
    return self;
}

-(void)dealloc {
    [[EventManager getInstance] removeEventListener:self.appActiveListenerID];
}


-(void)rotateToLandscape {
    if([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait) {
        return;
    }
    
    CGFloat width = [Util getAppFrame].size.height;
    CGFloat height = [Util getAppFrame].size.width;
    
    CGRect frame = CGRectMake(0, 0, width, height);
    
    [self setBounds:frame];
    [self setTransform:CGAffineTransformMakeRotation(M_PI / 2)];
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeRight;
    
    [HelpOverlay hide];
}

-(void)drawWithFrame:(CGRect)frame {
    
    CGRect contentRect = CGRectInset(frame, CONTENT_INSET, CONTENT_INSET);
    CGRect fullViewRect = CGRectMake(0, 0, contentRect.size.width, contentRect.size.height);
    
    CGFloat previewSize = contentRect.size.width/PREVIEWS_PER_PAGE;
    
    CGRect previewGridRect = CGRectMake(0, contentRect.size.height - previewSize, contentRect.size.width, previewSize);
    
    CGSize fullViewContentSize = CGSizeMake(contentRect.size.width * self.availablePhotos.count, contentRect.size.height);
    

    CGSize previewContentSize = CGSizeMake(previewSize * self.availablePhotos.count, previewSize);
    
    CGRect positionViewRect = CGRectMake(0, POSTIION_VIEW_Y, POSITION_VIEW_WIDTH, POSITION_VIEW_HEIGHT);
    CGRect closeButtonRect = CGRectMake(frame.size.width - CLOSE_BUTTON_SIZE - CLOSE_BUTTON_MARGIN, CLOSE_BUTTON_MARGIN, CLOSE_BUTTON_SIZE, CLOSE_BUTTON_SIZE);

    self.contentArea = [[UIView alloc] initWithFrame:contentRect];
    [self addSubview:self.contentArea];
    
    self.fullViewGrid = [[UIScrollView alloc] initWithFrame:fullViewRect];
    self.fullViewGrid.pagingEnabled = YES;
    self.fullViewGrid.contentSize = fullViewContentSize;
    [self.contentArea addSubview:fullViewGrid];

    
    self.previewGrid = [[UIScrollView alloc] initWithFrame:previewGridRect];
    self.previewGrid.contentSize = previewContentSize;
    [self.previewGrid setBackgroundColor:UIColorFromRGBA(0x0, 0.45f)];
    [self.contentArea addSubview:previewGrid];
    
    self.positionView = [[UITextView alloc] initWithFrame:positionViewRect];
    [self.positionView setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.55]];
    [self.positionView setTextAlignment:UITextAlignmentCenter];
    [self.positionView setFont:[UIFont fontWithName:@"Arial-BoldMT" size:13]];
    [self.positionView setTextColor:UIColorFromRGB(0xCECECE)];
    
    [self.contentArea addSubview:self.positionView];
    
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeButton setImage:[UIImage imageNamed:@"full_view_close_button.png"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(onCloseButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.closeButton setTitle:@"Close" forState:UIControlStateNormal];
    [self.closeButton setFrame:closeButtonRect];
    [self.closeButton setClipsToBounds:YES];
    [self.closeButton setBackgroundColor:[UIColor clearColor]];
    [self.contentArea addSubview:closeButton];
    
    self.fullViewGrid.delegate = self;
    self.previewGrid.delegate = self;
    
    UITapGestureRecognizer *fullViewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onFullViewTap:)];
    [self.fullViewGrid addGestureRecognizer:fullViewTapRecognizer];
    
    [self scrollViewDidScroll:self.fullViewGrid];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if([scrollView isEqual:self.fullViewGrid]) {
        [self updateUIOnScroll];
    }
    else {
        [self ensurePreviews:NO];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return [touch.view isEqual:self];
}

-(void)onCloseButtonClick {
    if(self.controlHideTimer) {
        [self.controlHideTimer invalidate];
    }
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
    [HelpOverlay show];
    
    [Logger track:@"gallery_close_button_clicked" :@{@"aid" : self.album.id}];
    
    [self removeFromSuperview];
}

-(void)onFullViewTap:(UITapGestureRecognizer*)gestureRecognizer {
    [self resetControlHideTimer];
    [self toggleShowControls];
}

-(void)resetControlHideTimer {
    if(self.controlHideTimer){
        self.controlHideTimer.fireDate = [NSDate dateWithTimeIntervalSinceNow:CONTROL_AUTO_HIDE_INTERVAL];    
    }
}

-(void)hideControls {
    [UIView animateWithDuration:CONTROL_SHOW_HIDE_ANIMATION_DURATION animations:^{
        self.previewGrid.alpha = 0.0;
        self.positionView.alpha = 0.0;
        self.closeButton.alpha = 0.0;
    }];
}

-(void)showControls {
    [UIView animateWithDuration:CONTROL_SHOW_HIDE_ANIMATION_DURATION animations:^{
        self.previewGrid.alpha = 1.0;
        self.positionView.alpha = 1.0;
        self.closeButton.alpha = 1.0;
    }];
}

-(void)toggleShowControls {
    [UIView animateWithDuration:CONTROL_SHOW_HIDE_ANIMATION_DURATION animations:^{
        self.previewGrid.alpha = 1.0 - self.previewGrid.alpha;
        self.positionView.alpha = 1.0 - self.positionView.alpha;
        self.closeButton.alpha = 1.0 - self.closeButton.alpha;
    }];
}

-(void)onPreviewTap:(UITapGestureRecognizer*)gestureRecognizer {
    UIView *preview = gestureRecognizer.view;
    NSUInteger newIndex = preview.tag - 1;
    [self scrollToIndex:newIndex];
}

-(void)scrollToIndex:(NSUInteger)index {
    CGFloat newXOffset = [self getContentOffsetForIndex:index];
    [self.fullViewGrid setContentOffset:CGPointMake(newXOffset, 0) animated:YES];
}

-(void)updateUIOnScroll {
    [self ensureFullViews];
    [self ensurePreviews:YES];
    [self updatePositionTextForCurrentIndex];
    [self updatePreviewForCurrentIndex];
}

-(void)updatePreviewForCurrentIndex {
    NSUInteger currentIndex = [self getIndexForCurrentOffset];
    
    for(UIView *preview in self.previewGrid.subviews) {
        if(preview.tag == currentIndex + 1) {
            [self markPreview:preview :YES];
        }
        else {
            [self markPreview:preview :NO];
        }
    }
}

-(void)markPreview:(UIView*)preview :(BOOL)selected {
    if(!preview) {
        return;
    }
    UIImageView *imageView = [self getPreviewImageView:preview];
    if(!imageView) {
        return;
    }
    
    if(selected) {
        [self removeMaskFromPreview:preview];
    }
    else {
        [self addMaskToPreview:preview];
        
    }
}

-(void)ensureFullViews {
    int currentPage = self.fullViewGrid.contentOffset.x/self.fullViewGrid.frame.size.width;
    
    int keepMin = MAX(0, currentPage - 1);
    int keepMax = MIN(self.availablePhotos.count - 1, currentPage +  1);
    
    
    
    for(int i=keepMin; i<=keepMax; ++i) {
        if([self.fullViewGrid viewWithTag:(i + 1)]) {
            continue;
        }
        
        Photo *photo = self.availablePhotos[i];
        CGSize size = self.fullViewGrid.frame.size;
        UIImageView *imageView = [photo getUIImageView:size.width :size.height :NO :YES];
        CGRect imageViewRect = CGRectMake(size.width * i, 0, size.width, size.height);
        [imageView setFrame:imageViewRect];
        [imageView setTag:(i + 1)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [self.fullViewGrid addSubview:imageView];
    }
    
    for(UIView *subview in self.fullViewGrid.subviews) {
        if(subview.tag < (keepMin + 1) || subview.tag > (keepMax + 1)) {
            [subview removeFromSuperview];
        }
    }

}

-(void)ensurePreviews:(BOOL)ensureCurrentVisible {
    CGFloat previewSize = self.previewGrid.frame.size.width/PREVIEWS_PER_PAGE;
    int currentStartIndex = self.previewGrid.contentOffset.x/previewSize;
    
    
    int keepMin = MAX(0, currentStartIndex - PREVIEWS_PER_PAGE);
    int keepMax = MIN(self.availablePhotos.count - 1, currentStartIndex +  PREVIEWS_PER_PAGE + PREVIEWS_PER_PAGE);
    
    for(int i=keepMin; i<=keepMax; ++i) {
        if([self.previewGrid viewWithTag:(i + 1)]) {
            continue;
        }
        
        CGFloat previewImageSize = previewSize - 2 * PREVIEW_INSET;
        CGRect previewContainerRect = CGRectMake(previewSize * i, 0, previewSize, previewSize);
        CGRect previewImageRect = CGRectMake(PREVIEW_INSET, PREVIEW_INSET, previewImageSize, previewImageSize);
        
        UIView *previewContainer = [[UIView alloc] initWithFrame:previewContainerRect];
        [previewContainer setTag:(i+1)];
        [self.previewGrid addSubview:previewContainer];
        [previewContainer setBackgroundColor:UIColorFromRGB(0xFFFFFF)];
        
        Photo *photo = self.availablePhotos[i];
        UIImageView *imageView = [photo getUIImageView:previewImageSize :previewImageSize :YES :NO];
        [imageView setFrame:previewImageRect];
        [previewContainer addSubview:imageView];
        
        //do it after adding image view
        [self addMaskToPreview:previewContainer];
        
        UITapGestureRecognizer *previewTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPreviewTap:)];
        [previewContainer addGestureRecognizer:previewTapRecognizer];
        
    }
    
    if(ensureCurrentVisible) {
        UIView *currentPreview = [self.previewGrid viewWithTag:([self getIndexForCurrentOffset] + 1)];
        CGFloat currentPreviewX = currentPreview.frame.origin.x;
        if(currentPreviewX < self.previewGrid.contentOffset.x 
           || currentPreviewX >= self.previewGrid.contentOffset.x + self.previewGrid.frame.size.width) 
        {
            [self.previewGrid setContentOffset:CGPointMake(currentPreviewX, 0) animated:YES];    
        }
    }
    
    for(UIView *subview in self.previewGrid.subviews) {
        if(subview.tag < (keepMin + 1) || subview.tag > (keepMax + 1)) {
            [subview removeFromSuperview];
        }
    }

}

-(CGFloat)getContentOffsetForIndex:(NSUInteger)index {
    return self.fullViewGrid.frame.size.width * index;
}

-(NSUInteger)getIndexForCurrentOffset {
    return self.fullViewGrid.contentOffset.x/self.fullViewGrid.frame.size.width;
}

-(void)updatePositionTextForCurrentIndex {
    NSUInteger index = [self getIndexForCurrentOffset];
    NSString *text = [NSString stringWithFormat:@"%d/%d", (index + 1), self.availablePhotos.count];
    [self.positionView setText:text];
}

-(void)removeMaskFromPreview:(UIView*)preview {
    UIView *mask = [preview viewWithTag:PREVIEW_MASK_TAG];
    if(!mask) {
        CGRect maskFrame = CGRectMake(0, 0, preview.frame.size.width, preview.frame.size.height);
        mask = [[UIView alloc] initWithFrame:maskFrame];
        mask.tag = PREVIEW_MASK_TAG;
        //[mask setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75]];
        [mask setBackgroundColor:[UIColor clearColor]];
        [mask.layer setBackgroundColor:[UIColor clearColor].CGColor];
        [mask.layer setBorderWidth:3];
        [mask.layer setBorderColor:[UIColor whiteColor].CGColor];
        [preview addSubview:mask];
    }
    
    [preview bringSubviewToFront:mask];
}

-(void)addMaskToPreview:(UIView*)preview {
    UIView *mask = [preview viewWithTag:PREVIEW_MASK_TAG];
    if(mask) {
        [preview sendSubviewToBack:mask];
    }
}

-(UIImageView*)getPreviewImageView:(UIView*)previewContainer {
    for(UIView *subview in previewContainer.subviews) {
        if([subview isKindOfClass:[UIImageView class]]) {
            return (UIImageView*)subview;
        }
    }
    return nil;
}

@end
