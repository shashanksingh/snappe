//
//  ModalAlertView.m
//  snappe
//
//  Created by Shashank on 16/12/12.
//
//

#import "ModalAlertView.h"
#import "Macros.h"
#import "Util.h"
#import "UIController.h"
#import "RootViewController.h"
#import "Logger.h"
#import "RTLabel.h"
#import "EventManager.h"
#import <QuartzCore/QuartzCore.h>

#define ALERT_VIEW_WIDTH 250
#define ALERT_VIEW_HEIGHT 130
#define MESSAGE_PADDING 10

static ModalAlertView *sharedInstance = nil;

typedef enum ModalAlertType {
    ERROR, BUSY, SUCCESS
} ModalAlertType;

@implementation ModalAlertView

- (id)init
{
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75f]];
        RootViewController *rvc = [[UIController getInstance] getUIControl:ROOT_VIEW_CONTROLLER];
        [rvc.view addSubview:self];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        tapRecognizer.delegate = self;
        [self addGestureRecognizer:tapRecognizer];

    }
    return self;
}

+(ModalAlertView *) getInstance {
    @synchronized(self) {
        if(!sharedInstance) {
            sharedInstance = [[ModalAlertView alloc] init];
        }
    }
    return sharedInstance;
}


+(void)showError:(NSString*)message {
    [self show:ERROR:message];
}

+(void)showSuccess:(NSString*)message {
    [self show:SUCCESS:message];
}

+(void)showBusy:(NSString*)message {
    [self show:BUSY:message];
}

+(void)show:(ModalAlertType)type:(NSString*)message {
    UIView *view = nil;
    if(type == ERROR || type == SUCCESS) {
        view = [self getAlertView:type:message];
    }
    else if(type == BUSY) {
        view = [self getBusyView:message];
    }
    else {
        NSString *errorMessage = [NSString stringWithFormat:@"unknow ModalAlertType: %d", type];
        [Logger logError:errorMessage :nil];
        return;
    }
    
    ModalAlertView *this = [ModalAlertView getInstance];
    view.center = this.center;
    view.tag = type;
    
    [Util showModalView:this];
    
    BOOL animated = type == ERROR;
    [Util showPopup:this:view:animated:nil];
}

+(UIView*)getAlertView:(ModalAlertType)type:(NSString*)message {
    CGFloat ALERT_ICON_SIZE = 64;
    
    CGRect alertFrame = CGRectMake(0, 0, ALERT_VIEW_WIDTH, ALERT_VIEW_HEIGHT);
    
    UIView *alertView = [[UIView alloc] initWithFrame:alertFrame];
    [alertView setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 0.90)];
    [alertView.layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
    [alertView.layer setShadowRadius:3.0];
    [alertView.layer setShadowOpacity:1.0];
    [alertView.layer setBorderColor:UIColorFromRGBA(0xFFFFFF, 0.55).CGColor];
    [alertView.layer setBorderWidth:2];
    [Util drawFastShadow:alertView];
    
    CGRect contentRect = CGRectInset(alertView.bounds, MESSAGE_PADDING, MESSAGE_PADDING);
    
    
    CGRect iconViewRect = CGRectMake(0, 0, ALERT_ICON_SIZE, ALERT_ICON_SIZE);
    iconViewRect = [Util centerRect:iconViewRect :[Util getRectCenter:alertFrame]];
    
    NSString *bgImageName = @"error_alert_background_icon.png";
    if(type == SUCCESS) {
        bgImageName = @"success_alert_background_icon.png";
    }
    
    UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:bgImageName]];
    
    [iconView setFrame:iconViewRect];
    [alertView addSubview:iconView];
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:contentRect];
    messageLabel.lineBreakMode = UILineBreakModeWordWrap;
    messageLabel.numberOfLines = 0;
    messageLabel.adjustsFontSizeToFitWidth = YES;
    [messageLabel setBackgroundColor:[UIColor clearColor]];
    [messageLabel setFont:[UIFont fontWithName:@"NotoSerif" size:11]];
    [messageLabel setTextColor:UIColorFromRGBA(0x0, 1)];
    [messageLabel setTextAlignment:NSTextAlignmentCenter];
    [messageLabel setText:message];
    [alertView addSubview:messageLabel];
    
    return alertView;
}

+(UIView*)getBusyView:(NSString*)message {
    CGFloat WIDTH = 220;
    CGFloat HEIGHT = 50;
    CGFloat BORDER = 0;
    CGFloat GIF_SECTION_SIZE = 64;
    
    CGRect boxRect = CGRectMake(0, 0, WIDTH, HEIGHT);
    
    UIColor *bgColor = UIColorFromRGBA(0xFFFFFF, 1);
    
    UIView *mainBox = [[UIView alloc] initWithFrame:boxRect];
    [mainBox setBackgroundColor:bgColor];
    [mainBox.layer setBorderColor: bgColor.CGColor];
    [mainBox.layer setBorderWidth: BORDER];
    [Util drawFastShadow:mainBox];
    [mainBox.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [mainBox.layer setShadowRadius:3.0];
    [mainBox.layer setShadowOpacity:0.9];
    
    CGRect gifRect, messageRect;
    CGRectDivide(mainBox.bounds, &gifRect, &messageRect, GIF_SECTION_SIZE, CGRectMinXEdge);
    
    UIView *thumbView = [[UIView alloc] initWithFrame:gifRect];
    [mainBox addSubview:thumbView];
    [Util setViewSkin:thumbView :LOADING_IMAGE_INDICATOR_LIGHT];
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:messageRect];
    [mainBox addSubview:messageLabel];
    messageLabel.adjustsFontSizeToFitWidth = NO;
    messageLabel.lineBreakMode = UILineBreakModeTailTruncation;
    messageLabel.textAlignment = UITextAlignmentLeft;
    messageLabel.backgroundColor = [UIColor clearColor];
    [messageLabel setTextColor:UIColorFromRGB(0x555555)];
    [messageLabel setFont: [UIFont fontWithName:@"TrebuchetMS-Bold" size:13]];
    [messageLabel setText:message];
    return mainBox;
}

+(void)hideAlert {
    [self hideOfType:ERROR];
    [self hideOfType:SUCCESS];
}

+(void)hideBusyAlert {
    [self hideOfType:BUSY];
}

+(void)hideOfType:(ModalAlertType)type {
    for(UIView *alert in sharedInstance.subviews) {
        //user can't remove busy messag by tapping
        //it has to be removed programmatically
        if(alert.tag == type) {
            [alert removeFromSuperview];
        }
    }
    
    if([sharedInstance.subviews count] == 0) {
        [sharedInstance removeFromSuperview];
        sharedInstance = nil;
        
        [[EventManager getInstance] fireEvent:MODAL_ALERT_VIEW_HIDING];
    }
}

-(void)onTap:(UIView*)sender {
    [self.class hideAlert];

}
@end
