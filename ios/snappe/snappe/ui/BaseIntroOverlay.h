//
//  BaseIntroOverlay.h
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import <UIKit/UIKit.h>

@interface IntroHint : NSObject
@property CGRect labelRect;
@property NSString *labelText;
@property CGPoint hintTargetLocation;
@end

@interface BaseIntroOverlay : UIView
@property (retain) id data;
+(void)show:(id)data;
@end
