//
//  FriendsAlbumsViewController.m
//  snappe
//
//  Created by Shashank on 14/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <stdlib.h>
#import "FriendsAlbumsView.h"
#import "CoreController.h"
#import "FakeAlbum.h"
#import "AlbumView.h"
#import "Macros.h"
#import "Util.h"
#import "PhotoLibraryOrganizer.h"
#import "GraphManager.h"
#import "UserView.h"
#import "EventManager.h"
#import "SortManager.h"
#import "UIController.h"
#import "Logger.h"
#import "Carousel.h"
#import "SortingButtonPanel.h"
#import "OwnAlbumsCarouselView.h"
#import "UIView+Help.h"
#import <QuartzCore/QuartzCore.h>

#define NUMBER_OF_PLACEHOLDER_VIEWS 5;
#define NUMBER_OF_VISIBLE_ITEMS 5;


#define PADDING_TOP 8
#define TITLE_PADDING_TOP 12
#define TITLE_PADDING_BOTTOM 5
#define TITLE_SECTION_HEIGHT 25
#define TITLE_SECTION_WIDTH 160
#define FB_LOGIN_BUTTON_WIDTH 232
#define FB_LOGIN_BUTTON_HEIGHT 68

#define SORT_BEGIN_DELAY_SECONDS 2.0

@interface FriendsAlbumsView ()<SortingDelegate>
@property CoreController *coreController;
@property NSArray *friendsAndAlbums;
@property (strong) NSTimer *prefetchTimer;
@end

@implementation FriendsAlbumsView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self drawOnFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self loadDataShowData];
        [self setupEvents];
    }
    return self;
}

-(NSArray*)data {
    return self.friendsAndAlbums;
}

- (void)drawOnFrame:(CGRect)frame {

    float w = frame.size.width;
    float h = frame.size.height;
    
    CGRect titleRect, carouselRect;
    CGRectDivide(frame, &titleRect, &carouselRect, TITLE_SECTION_HEIGHT, CGRectMinYEdge);
    
    titleRect.origin.y += PADDING_TOP;
    //titleRect.origin.x = titleRect.size.width - TITLE_SECTION_WIDTH;
    titleRect.size.width = TITLE_SECTION_WIDTH;
    
    carouselRect.origin.y += TITLE_PADDING_BOTTOM;
    
    UILabel *title = [[UILabel alloc] initWithFrame:titleRect];
    [title setTextAlignment:UITextAlignmentCenter];
    [title setTextColor:[UIColor whiteColor]];
    [title setText: @"Friends' & Facebook Albums"];
    [title setFont: [UIFont fontWithName:@"MarkerFelt-Thin" size:13.0f]];
    [Util drawFastShadow:title];
    title.shadowColor = [UIColor blackColor];
    title.shadowOffset = CGSizeMake(0.0, -1.0);
    
    [title setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"friends-albums-view-title-background.png"]]];
    [self addSubview:title];
    
    SortingButtonPanel *sortingButtonPanel = [SortingButtonPanel new];
    sortingButtonPanel.delegate = self;
    CGRect sortingButtonPanelFrame = sortingButtonPanel.bounds;
    sortingButtonPanelFrame.origin = CGPointMake(w - sortingButtonPanelFrame.size.width, titleRect.origin.y);
    sortingButtonPanel.frame = sortingButtonPanelFrame;
    //[self addSubview:sortingButtonPanel];
	
    self.carousel = [[Carousel alloc] initWithFrame:carouselRect:REMOTE:self];
    [self addSubview:self.carousel];
    self.carousel.clipsToBounds = NO;
    
}

-(void) setupEvents {
    
    [[EventManager getInstance] addEventListener:PHOTO_LIBRARY_ORGANIZED :^(va_list args) {
        [self refresh];
    }];
    
    [[EventManager getInstance] addEventListener:FB_AUTHORIZED :^(va_list args) {
        //TODO: should we be having this listener non-one-shot?
        //we don't sync from here, sync view will listen to the same event and do that
        //TODO: what if sync view finishes before we are done?
        [self setLoading];
    }];
    
    [[EventManager getInstance] addEventListener:FB_NOT_AUTHORIZED :^(va_list args) {
        [self setLoading];
    }];
    [[EventManager getInstance] addEventListener:FB_AUTH_ERROR :^(va_list args) {
        [self setLoading];
    }];
    
    [[EventManager getInstance] addEventListener:ALBUM_OR_FRIEND_SELECTED_ON_SEARCH :^(va_list args) {
        NSManagedObject* data = va_arg(args, NSManagedObject*);
        if([data isKindOfClass:[Album class]] || [data isKindOfClass:[User class]]) {
            [self.carousel scrollToUserOrAlbum:data];
        }
    }];
}

-(void) loadData:(VoidCallback)callback :(GenericErrorback)errorback {
    [[PhotoLibraryOrganizer getInstance] getRemoteAlbums:^(NSArray *friendAlbums){
        //NSArray *friends = [[GraphManager getInstance] getFriends];
        self.friendsAndAlbums = friendAlbums;// [friendAlbums arrayByAddingObjectsFromArray:friends];
        if(callback) callback();
    }:errorback];
}

-(void) loadDataShowData {
    [self setLoading];
    [self loadData:^{
        [self reloadData];
        
    } :^(NSError *error) {
        [Logger logError:@"error in loading FriendsAlbumsView data" :error];
        NSString *message = [Util getSnappeErrorMessage:error:@"There was an error in loading remote albums. Please try again later"];
        [Util alertError:error:message];
    }];
}

-(void) setLoading {
    self.friendsAndAlbums = nil;
    //[self reloadData];
}

-(void) refresh {
    [self loadDataShowData];
}

-(void) reloadData {
    [self.carousel reloadData];
}

-(void)sortFor:(NSManagedObject*)data:(SortingType)sortingType {
    [[SortManager getInstance] sort:self.friendsAndAlbums :data :sortingType :NO:^(NSArray *sorted)
    {
        self.friendsAndAlbums = sorted;
        [self reloadData];
        if([self.friendsAndAlbums count] != 0) {
            [self.carousel scrollToUserOrAlbum:[self.friendsAndAlbums objectAtIndex:0]];
        }
    }:^(NSError *error){
        [Logger logError:@"error in sorting" :error];
    }];
}

-(NSManagedObject*)getCurrentFriendOrAlbum {
    if(!self.carousel || !self.friendsAndAlbums) return nil;
    return self.friendsAndAlbums[self.carousel.currentItemIndex];
}

-(void)onSmartSortClick {
    [self onSortClick:SMART];
}

-(void)onDateSortClick {
    [self onSortClick:DATE];
}

-(void)onAlphabeticalSortClick {
    [self onSortClick:ALPHABET];
}

-(void)onSortClick:(SortingType)type {
    OwnAlbumsCarouselView *oav = [[UIController getInstance] getUIControl:OWN_ALBUM_CAROUSEL_VIEW];
    NSManagedObject *currentAlbum = [oav getCurrentAlbum];
    if(currentAlbum) {
        [self sortFor:currentAlbum:type];
    }
}


@end
