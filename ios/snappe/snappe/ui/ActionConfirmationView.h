//
//  ActionConfirmationView.h
//  snappe
//
//  Created by Shashank on 08/04/13.
//
//

#import <UIKit/UIKit.h>
#import "Macros.h"

@interface ActionConfirmationView : UIView
+(void)confirm:(NSString*)key:(NSString*)message:(GenericCallback)callback;
@end
