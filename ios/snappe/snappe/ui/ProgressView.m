//
//  ProgressView.m
//  snappe
//
//  Created by Shashank on 12/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProgressView.h"
#import "Macros.h"
#import "CatchTheDot.h"
#import "Util.h"
#import "UIController.h"
#import "RootViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AdProvider.h"
#import "GameIntroOverlay.h"
#import "EventManager.h"

#define CONTENT_WIDTH 250
#define GAME_VIEW_HEIGHT 300
#define PROGRESS_INFO_VIEW_HEIGHT 32
#define INTER_VIEW_Y_GAP 20
#define PROGRESS_INDICATOR_SECTION_WIDTH 44
#define SUCCESS_ICON_SIZE 24

#define DISMISS_BUTTON_WIDTH 100
#define DISMISS_BUTTON_HEIGHT 30
#define DISMISS_BUTTON_TOP_MARGIN 10


@interface ProgressView()
@property (strong) UIView *contentView;
@property (strong) CatchTheDot *gameView;
@property (strong) UIView *progressIndicator;
@property (strong) UIView *progressIndicatorLoadingSkin;
@property (strong) UILabel *progressStatusLabel;
@property (strong) UIButton *runInBackgroundButton;
@property BOOL finished;
@end

@implementation ProgressView
@synthesize contentView;
@synthesize gameView;
@synthesize progressIndicator;
@synthesize progressIndicatorLoadingSkin;
@synthesize progressStatusLabel;
@synthesize runInBackgroundButton;
@synthesize finished;

static ProgressView *sharedInstance;

- (id)init
{
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if (self) {
        self.finished = NO;
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75f]];
        [self drawOnFrame:frame];
        
        //[GameIntroOverlay show:nil];
    }
    return self;
}

+(ProgressView*) getInstance {
    @synchronized(self) {
        if(!sharedInstance) {
            sharedInstance = [[ProgressView alloc] init];
        }
    }
    return sharedInstance;
}

+(ProgressView*)show {
    sharedInstance = nil;
    
    ProgressView *instance = [self getInstance];
    [Util showModalView:instance];
    [Util showPopup:[self getInstance] :[self.class getInstance].contentView :YES :^{
        [AdProvider show:[self getInstance]];
    }];
    return instance;
}

-(void)drawOnFrame:(CGRect)frame {
    CGRect gameAndInfoRect, gameRect, progressInfoRect;
    
    CGFloat leftMargin = (frame.size.width - CONTENT_WIDTH)/2;
    CGFloat topMargin = (frame.size.height - (GAME_VIEW_HEIGHT + PROGRESS_INFO_VIEW_HEIGHT))/2;
    
    gameAndInfoRect = CGRectMake(leftMargin, topMargin, CONTENT_WIDTH, GAME_VIEW_HEIGHT + PROGRESS_INFO_VIEW_HEIGHT);
    gameRect = CGRectMake(0, 0, CONTENT_WIDTH, GAME_VIEW_HEIGHT);
    progressInfoRect = CGRectMake(0, CGRectGetMaxY(gameRect), CONTENT_WIDTH, PROGRESS_INFO_VIEW_HEIGHT);
    
    
    self.contentView = [[UIView alloc] initWithFrame:gameAndInfoRect];
    [self addSubview:self.contentView];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    [self.contentView.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [Util drawFastShadow:self.contentView];
    [self.contentView.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [self.contentView.layer setShadowRadius:3.0];
    [self.contentView.layer setShadowOpacity:1.0];
    
    self.gameView = [[CatchTheDot alloc] initWithFrame:gameRect];
    [self.gameView setBackgroundColor:[UIColor whiteColor]];
    [self.contentView addSubview:self.gameView];
    
    UIView *progressInfoView = [[UIView alloc] initWithFrame:progressInfoRect];
    [self.contentView addSubview:progressInfoView];
    [progressInfoView setBackgroundColor:UIColorFromRGBA(0x343434, 0.9)];
    
    
    CGRect progressIndicatorSectionRect, progressStatusSectionRect;
    CGRectDivide(progressInfoView.bounds, &progressIndicatorSectionRect, &progressStatusSectionRect, PROGRESS_INDICATOR_SECTION_WIDTH, CGRectMinXEdge);
    
    self.progressIndicator = [[UIView alloc] initWithFrame:progressIndicatorSectionRect];
    [progressInfoView addSubview:self.progressIndicator];
    self.progressIndicatorLoadingSkin = [Util setViewSkin:self.progressIndicator:LOADING_IMAGE_INDICATOR_DARK];
    
    self.progressStatusLabel = [[UILabel alloc] initWithFrame:CGRectInset(progressStatusSectionRect, 5, 10)];
    [progressInfoView addSubview:self.progressStatusLabel];
    [self.progressStatusLabel setBackgroundColor:[UIColor clearColor]];
    
    [self.progressStatusLabel setTextColor:UIColorFromRGBA(0xFFFFFF, 1)];
    [self.progressStatusLabel setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:11]];
    [self.progressStatusLabel setTextAlignment:NSTextAlignmentLeft];
    
    CGRect runInBackgroundButtonRect = CGRectMake(CGRectGetMidX(gameAndInfoRect) - DISMISS_BUTTON_WIDTH/2, CGRectGetMaxY(gameAndInfoRect) + DISMISS_BUTTON_TOP_MARGIN, DISMISS_BUTTON_WIDTH, DISMISS_BUTTON_HEIGHT);
    
    self.runInBackgroundButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.runInBackgroundButton setTitle:@"Dismiss" forState:UIControlStateNormal];
    [self.runInBackgroundButton addTarget:self action:@selector(onRunInBackgroundClick) forControlEvents:UIControlEventTouchUpInside];
    [self.runInBackgroundButton setFrame:runInBackgroundButtonRect];
    [self.runInBackgroundButton setClipsToBounds:YES];
    [self.runInBackgroundButton setBackgroundColor:UIColorFromRGBA(0x343434, 0.9)];
    self.runInBackgroundButton.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:16];
    [self.runInBackgroundButton.layer setBorderColor: UIColorFromRGB(0x2A2A2A).CGColor];
    [self.runInBackgroundButton.layer setBorderWidth:1];
    [Util drawFastShadow:self.runInBackgroundButton];
    [self.runInBackgroundButton.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [self.runInBackgroundButton.layer setShadowRadius:3.0];
    [self.runInBackgroundButton.layer setShadowOpacity:1.0];
    [self addSubview:self.runInBackgroundButton];
    
    //disabling background execution
    self.runInBackgroundButton.hidden = YES;
}

-(void)setStatus:(NSString*)status {
    [self.progressStatusLabel setText:status];
}

-(void)setDone:(BOOL)success {
    self.finished = YES;
    [Util clearViewSkin:self.progressIndicatorLoadingSkin:LOADING_IMAGE_INDICATOR_DARK];
    
    CGRect successIconRect = CGRectMake(0, 0, SUCCESS_ICON_SIZE, SUCCESS_ICON_SIZE);
    successIconRect = [Util centerRect:successIconRect :[Util getRectCenter:self.progressIndicator.bounds]];
    UIImageView *successIconView = [[UIImageView alloc] initWithFrame:successIconRect];
    UIImage *successIcon = [UIImage imageNamed:success ? @"success.png" : @"error.png"];
    [successIconView setImage:successIcon];
    [self.progressIndicator addSubview:successIconView];

    [self.progressStatusLabel setText:success ? @"Done!" : @"Failed"];
    self.runInBackgroundButton.hidden = NO;
}

-(void)onRunInBackgroundClick {
    if(!self.finished) {
        RootViewController *rvc = [[UIController getInstance] getUIControl:ROOT_VIEW_CONTROLLER];
        [rvc showNotifications];
    }
    
    BOOL done = self.finished;
    
    [self removeFromSuperview];
    sharedInstance = nil;
    
    if (done) {
        [[EventManager getInstance] fireEvent:PROGRESS_VIEW_DISMISSED];
    }
    
}

@end
