//
//  UIView+Help.m
//  snappe
//
//  Created by Shashank on 16/03/13.
//
//

#import "UIView+Help.h"
#import <objc/runtime.h>

static char const * const HELP_TEXT_PROPERTY_KEY = "snappe_help_text";
static char const * const HELP_SPOTLIGHT_PROPERTY_KEY = "snappe_help_spotlight_oval";

@implementation UIView (Help)
-(void)setHelpText:(NSString *)helpText {
    objc_setAssociatedObject(self, HELP_TEXT_PROPERTY_KEY, helpText, OBJC_ASSOCIATION_RETAIN);
}
-(NSString*)helpText {
    return objc_getAssociatedObject(self, HELP_TEXT_PROPERTY_KEY);
}
-(void)setHelpSpotlightShapeOval:(BOOL)oval {
    objc_setAssociatedObject(self, HELP_SPOTLIGHT_PROPERTY_KEY, @(oval), OBJC_ASSOCIATION_RETAIN);
}
-(BOOL)getHelpSpotlightShapeOval {
    NSNumber *oval = objc_getAssociatedObject(self, HELP_SPOTLIGHT_PROPERTY_KEY);
    return oval && oval.boolValue;
}
@end
