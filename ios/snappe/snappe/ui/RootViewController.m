//
//  RootViewController.m
//  snappe
//
//  Created by Shashank on 11/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RootViewController.h"
#import "UIController.h"
#import "SVSegmentedControl.h"
#import "CustomBadge.h"
#import <QuartzCore/QuartzCore.h>
#import "Util.h"
#import "PhotoLibraryReader.h"
#import "Logger.h"
#import "LibraryReadProgressView.h"
#import "Macros.h"
#import "RTLabel.h"
#import "ModalAlertView.h"
#import "HelpOverlay.h"
#import "BButton.h"
#import "WelcomeView.h"
#import "EventManager.h"
#import "UIView+Help.h"
#import "AdProvider.h"

#define TAB_BAR_HEIGHT 44

@interface RootViewController()
@property (strong) UIView *libReadProgressView;
@property (strong) UIView *accessDeniedErrorView;
@property (strong) UIView *tabContainer;
@property (strong) SVSegmentedControl *tabBar;
@property (strong) CustomBadge *unreadNotificationsBadge;
@end

@implementation RootViewController
@synthesize libReadProgressView;
@synthesize homeViewController;
@synthesize notificationController;
@synthesize tabContainer;
@synthesize tabBar;
@synthesize unreadNotificationsBadge;

-(id) init {
    self = [super init];
    if(self) {
        [[UIController getInstance] setUIControl:self:ROOT_VIEW_CONTROLLER];
        [self load];
        
        [[EventManager getInstance] addEventListener:FB_AUTHORIZED :YES :^(va_list args) {
            NSNumber *firstTime = va_arg(args, NSNumber*);
            if(!firstTime.boolValue) {
                return;
            }
            
            [[EventManager getInstance] addEventListener:PROGRESS_VIEW_DISMISSED :YES :^(va_list args) {
                [ModalAlertView showBusy:@"please wait..."];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                    [WelcomeView show];
                    [ModalAlertView hideBusyAlert];
                });
                
            }];
        }];
    }
    
    return self;
}

-(void)load {
    self.view.frame = [Util getAppFrame];
    
    BOOL isFirstRun = [PhotoLibraryReader isFirstRun];
    [Logger track:@"root_load" :@{@"first_run" : @(isFirstRun)}];
    
    if(isFirstRun) {
        [Logger track:@"first_lib_parse_init"];
        [self initBeforeFirstLibraryRead];
        
LOG_OPERATION_TIME(@"first_lib_parse",
        [[PhotoLibraryReader getInstance] reload:^{
);
            [self loadAPNSToken];
            [self initAfterFirstLibraryRead:YES];
            
        } :^(NSError *error) {
            [Logger logError:nil:error];
            
            if([error.domain isEqualToString:SNAPPE_ERROR_DOMAIN] && error.code == SNAPPE_ERROR_ALASSET_ACCESS_DENIED)
            {
                if(self.libReadProgressView) {
                    [self.libReadProgressView removeFromSuperview];
                    self.libReadProgressView = nil;
                }
                
                [Logger track:@"first_lib_parse_access_denied_error"];
                
                self.accessDeniedErrorView = [self getPhotoAccessDeniedErrorView];
                CGRect frame = [Util centerRect:self.accessDeniedErrorView.bounds :[Util getRectCenter:self.view.bounds]];
                self.accessDeniedErrorView.frame = frame;
                [self.view addSubview:self.accessDeniedErrorView];
                [HelpOverlay hide];
            }
            else {
                NSString *message = @"Sorry! There was an error in reading your local photos. Please try again by restarting the app or contact support. We apologize for the inconvenience";
                [Util alertError:error :message];
            }
        }];
    }
    else {
        [[PhotoLibraryReader getInstance] validateLibraryAccess:^{
            [self initAfterFirstLibraryRead:NO];
            [self loadAPNSToken];
            
        } :^(NSError *error) {
            self.accessDeniedErrorView = [self getPhotoAccessDeniedErrorView];
            CGRect frame = [Util centerRect:self.accessDeniedErrorView.bounds :[Util getRectCenter:self.view.bounds]];
            self.accessDeniedErrorView.frame = frame;
            [self.view addSubview:self.accessDeniedErrorView];
            [HelpOverlay hide];
        }];
    }

}

-(void)initBeforeFirstLibraryRead {
    self.libReadProgressView = [[LibraryReadProgressView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:self.libReadProgressView];
    [HelpOverlay hide];
}

-(void)initAfterFirstLibraryRead:(BOOL)firstTime {
    [HelpOverlay show];
    
    UIView *loadingIndicator = self.libReadProgressView;
    if(!loadingIndicator) {
        loadingIndicator = [self getInitLoadingIndicator];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view addSubview:loadingIndicator];
        });
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        self.tabContainer = [[UIView alloc] initWithFrame:[Util getAppFrame]];
        [self.tabContainer setHidden:YES];
        [self.view addSubview:self.tabContainer];
        
        [self setupTabBar];
        [self setupNotificationBadge];
        
        self.homeViewController = [[HomeViewController alloc] initWithFrame:[Util getAppFrame]];
        [[UIController getInstance] setUIControl:homeViewController :HOME_VIEW_CONTROLLER];
        
        self.notificationController = [[NotificationViewController alloc] initWithFrame:[Util getAppFrame]];
        [[UIController getInstance] setUIControl:notificationController :NOTIFICATION_VIEW_CONTROLLER];
        
        self.homeViewController.hidden = YES;
        self.notificationController.hidden = YES;
        
        [self.tabContainer addSubview:self.homeViewController];
        [self.tabContainer addSubview:self.notificationController];
        
        self.homeViewController.hidden = NO;
        
        [self showHome];
        
        if(loadingIndicator) {
            [loadingIndicator removeFromSuperview];
        }
        [self.tabContainer setHidden:NO];
        
        if(!firstTime) {
            LOG_OPERATION_TIME_PROPS(@"photo_lib_sync",
                                     
            [[PhotoLibraryOrganizer getInstance] sync:^{
                DLog(@"synced with server after init");
                
            , @{@"first_time": @(firstTime)});
                
            } :^(NSError *error) {
                [Logger logError:@"error in syncing at startup" :error];
            }];
        }
        else {
            //doing this instead of Organized.sync ensures that the sync spinner is not
            //active. otherwise it might scare the user that we are auto uploading photos
            LOG_OPERATION_TIME_PROPS(@"photo_lib_sync",
            [PhotoLibraryReader invalidateModificationCache];
            [[PhotoLibraryReader getInstance] reload:^{
                DLog(@"synced with server after init");
            , @{@"first_time": @(firstTime)});
            } :^(NSError *error) {
                [Logger logError:@"error in re-loading at first startup" :error];
            }];
        }
        
        if([[FBManager getInstance] hasUserAuthorizedOnce]) {
            [WelcomeView show];
        }
    });
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)setupTabBar {
    self.tabBar = [[SVSegmentedControl alloc] initWithSectionTitles:@[@"", @""]];
    [self.tabBar setSectionImages:@[[UIImage imageNamed:@"home.png"], [UIImage imageNamed:@"notification-icon.png"]]];
    [self.tabBar addTarget:self action:@selector(onTabSelected:) forControlEvents:UIControlEventValueChanged];
    self.tabBar.crossFadeLabelsOnDrag = YES;
    self.tabBar.font = [UIFont fontWithName:@"Marker Felt" size:12];
    self.tabBar.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    self.tabBar.height = 44;
    self.tabBar.tintColor = UIColorFromRGBA(0x0, 0.35);
    
    self.tabBar.thumb.tintColor = [UIColor colorWithRed:0.999 green:0.889 blue:0.312 alpha:1.000];
    self.tabBar.thumb.textColor = [UIColor blackColor];
    self.tabBar.thumb.textShadowColor = [UIColor colorWithWhite:1 alpha:0.5];
    self.tabBar.thumb.textShadowOffset = CGSizeMake(0, 1);
    
    self.tabBar.helpText = @"switch between 'Home' and 'Notifications' views";
    
    
    [self.view addSubview:self.tabBar];
    
    CGRect tabBarSectionRect = CGRectMake(0, [Util getAppFrame].size.height - TAB_BAR_HEIGHT + 2, [Util getAppFrame].size.width, TAB_BAR_HEIGHT);
    self.tabBar.center = CGPointMake(CGRectGetMidX(tabBarSectionRect), CGRectGetMidY(tabBarSectionRect));
}

-(void)setupNotificationBadge {
    self.unreadNotificationsBadge = [CustomBadge customBadgeWithString:@"1"
												   withStringColor:[UIColor whiteColor]
													withInsetColor:[UIColor redColor]
													withBadgeFrame:YES
											   withBadgeFrameColor:[UIColor whiteColor]
														 withScale:1.0
													   withShining:YES];
    CGRect frame = self.unreadNotificationsBadge.frame;
    frame.origin = CGPointMake(CGRectGetMaxX(self.tabBar.frame) - frame.size.width * 0.75, CGRectGetMinY(self.tabBar.frame) - frame.size.height * 0.5);
    self.unreadNotificationsBadge.frame = frame;
    self.unreadNotificationsBadge.alpha = 0;
    [self.view addSubview:self.unreadNotificationsBadge];
    
}

-(void)onTabSelected:(SVSegmentedControl*)segmentedControl {
    if(segmentedControl.selectedIndex == 0) {
        self.homeViewController.hidden = NO;
        self.notificationController.hidden = YES;
        [HelpOverlay show];
        
        CATransition* transition = [CATransition animation];
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        transition.duration = 0.2f;
        transition.type =  kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        
        [self.tabContainer.layer addAnimation:transition forKey:@"switchView"];
        
        
    }
    else {
        self.homeViewController.hidden = YES;
        self.notificationController.hidden = NO;
        [HelpOverlay hide];
        
        CATransition* transition = [CATransition animation];
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        transition.duration = 0.2f;
        transition.type =  kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        
        [self.tabContainer.layer addAnimation:transition forKey:@"switchView"];
    }
    
    [Logger track:@"tab_selected" :@{@"home" :@(!self.homeViewController.hidden)}];
}

-(void)showHome {
    if(self.tabBar.selectedIndex != 0) {
        self.tabBar.selectedIndex = 0;
    }
}

-(void)showNotifications {
    self.tabBar.selectedIndex = 1;
}

-(void)setUnreadNotificationCountBadge:(NSUInteger)count {
    NSString *newBadgeText = [NSString stringWithFormat:@"%u", count];
    [self.unreadNotificationsBadge autoBadgeSizeWithString:newBadgeText];
    
    if(count == 0) {
        [UIView beginAnimations:@"fade out" context:nil];
        [UIView setAnimationDuration:1.0];
        self.unreadNotificationsBadge.alpha = 0.0;
        [UIView commitAnimations];
    }
    else {
        [UIView beginAnimations:@"fade in" context:nil];
        [UIView setAnimationDuration:1.0];
        self.unreadNotificationsBadge.alpha = 1.0;
        [UIView commitAnimations];

    }
}

-(void)loadAPNSToken {
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
}

-(UIView*)getInitLoadingIndicator {
    CGFloat WIDTH = 220;
    CGFloat HEIGHT = 60;
    CGFloat BORDER = 0;
    CGFloat GIF_SECTION_SIZE = 64;
    
    CGRect boxRect = CGRectMake(0, 0, WIDTH, HEIGHT);
    boxRect = [Util centerRect:boxRect :[Util getRectCenter:self.view.bounds]];
    
    UIColor *bgColor = UIColorFromRGBA(0xFFFFFF, 1);
    
    UIView *mainBox = [[UIView alloc] initWithFrame:boxRect];
    [mainBox setBackgroundColor:bgColor];
    [mainBox.layer setBorderColor: bgColor.CGColor];
    [mainBox.layer setBorderWidth: BORDER];
    [Util drawFastShadow:mainBox];
    [mainBox.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [mainBox.layer setShadowRadius:10.0];
    [mainBox.layer setShadowOpacity:0.9];
    
    CGRect gifRect, messageRect;
    CGRectDivide(mainBox.bounds, &gifRect, &messageRect, GIF_SECTION_SIZE, CGRectMinXEdge);
    
    UIView *thumbView = [[UIView alloc] initWithFrame:gifRect];
    [mainBox addSubview:thumbView];
    [Util setViewSkin:thumbView :LOADING_IMAGE_INDICATOR_LIGHT];
    
    UILabel *message = [[UILabel alloc] initWithFrame:messageRect];
    [mainBox addSubview:message];
    message.adjustsFontSizeToFitWidth = NO;
    message.lineBreakMode = UILineBreakModeTailTruncation;
    message.textAlignment = UITextAlignmentLeft;
    message.backgroundColor = [UIColor clearColor];
    [message setTextColor:UIColorFromRGB(0x999999)];
    message.shadowColor = UIColorFromRGB(0xCECECE);
    message.shadowOffset = CGSizeMake(0, -1);
    [message setFont: [UIFont fontWithName:@"TrebuchetMS-Bold" size:13]];
    [message setText:@"loading..."];
    
    
    return mainBox;
}
                   
-(UIView*)getPhotoAccessDeniedErrorView {
    const CGFloat WIDTH = 280;
    const CGFloat HEIGHT = 400;
    const CGFloat BORDER = 0;
    const CGFloat MAIN_BOX_PADDING = 10;
    
    const CGFloat MAIN_MESSAGE_BOX_HEIGHT = 120;
    const CGFloat MAIN_MESSAGE_BOX_X_PADDING = 10;
    const CGFloat RETRY_BUTTON_BOX_HEIGHT = 0;
    const CGFloat RETRY_BUTTON_WIDTH = 120;
    const CGFloat RETRY_BUTTON_HEIGHT = 0;
    const CGFloat SEPARATOR_HEIGHT = 1;
    const CGFloat MAIN_SEPARATOR_PADDING = 0;//25;
    const CGFloat STEP_GAP = 20;
    const CGFloat STEP_1_BOX_HEIGHT = 120;
    const CGFloat STEP_234_BOX_HEIGHT = 80;
    const CGFloat STEP_MESSAGE_HEIGHT = 40;
    const CGFloat STEP_MESSAGE_X_PADDING = 10;
    const CGFloat STEP_MESSAGE_Y_PADDING = 10;
    const CGFloat STEP_IMAGE_X_PADDING = 10;
    
    CGRect scrollBox, mainMessageBox, retryButtonBox, mainSeparatorBox, step1Box, step2Box, step3Box, step4Box;
    
    scrollBox = CGRectMake(0, 0, WIDTH, HEIGHT);
    
    CGFloat y = 0;
    CGFloat x = MAIN_BOX_PADDING;
    CGFloat w = WIDTH - 2 *x;
    mainMessageBox = CGRectMake(x, y, w, MAIN_MESSAGE_BOX_HEIGHT); y += mainMessageBox.size.height + STEP_GAP;
    retryButtonBox = CGRectMake(x, y, w, RETRY_BUTTON_BOX_HEIGHT); y += retryButtonBox.size.height /*+ STEP_GAP*/;
    mainSeparatorBox = CGRectMake(x + MAIN_SEPARATOR_PADDING, y, w - 2 * MAIN_SEPARATOR_PADDING, SEPARATOR_HEIGHT);
    step1Box = CGRectMake(x, y, w, STEP_1_BOX_HEIGHT); y += step1Box.size.height + STEP_GAP;
    step2Box = CGRectMake(x, y, w, STEP_234_BOX_HEIGHT); y += step2Box.size.height + STEP_GAP;
    step3Box = CGRectMake(x, y, w, STEP_234_BOX_HEIGHT); y += step3Box.size.height + STEP_GAP;
    step4Box = CGRectMake(x, y, w, STEP_234_BOX_HEIGHT);
    
    UIScrollView *container = [[UIScrollView alloc] initWithFrame:scrollBox];
    container.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"error_bg.png"]];
    [container.layer setBorderColor: container.backgroundColor.CGColor];
    [container.layer setBorderWidth: BORDER];
    [Util drawFastShadow:container];
    [container.layer setShadowOffset:CGSizeMake(-5.0, -5.0)];
    [container.layer setShadowRadius:10.0];
    [container.layer setShadowOpacity:0.9];
    container.contentSize = CGSizeMake(WIDTH, CGRectGetMaxY(step4Box) + MAIN_BOX_PADDING);
    
    mainMessageBox = CGRectInset(mainMessageBox, MAIN_MESSAGE_BOX_X_PADDING, 0);
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:mainMessageBox];
    messageLabel.lineBreakMode = UILineBreakModeWordWrap;
    messageLabel.numberOfLines = 0;
    [messageLabel setBackgroundColor:[UIColor clearColor]];
    [messageLabel setFont:[UIFont fontWithName:@"Cochin" size:16]];
    [messageLabel setTextColor:UIColorFromRGBA(0x7C3030, 1)];
    [messageLabel setTextAlignment:NSTextAlignmentCenter];
    messageLabel.text = @"Sorry! You need to grant Snappe access to photos for it to work. To do that please follow the steps below, then start Snappe again.";
    [container addSubview:messageLabel];
    
    
    CGRect retryButtonRect = CGRectMake(0, 0, RETRY_BUTTON_WIDTH, RETRY_BUTTON_HEIGHT);
    retryButtonRect = [Util centerRect:retryButtonRect :[Util getRectCenter:retryButtonBox]];
    
    UIButton *retryButton = [[BButton alloc] initWithFrame:retryButtonRect type:BButtonTypeSuccess];
    [retryButton setTitle:@"Retry" forState:UIControlStateNormal];
    [retryButton addTarget:self action:@selector(onAccessDeniedRetryButtonClick) forControlEvents:UIControlEventTouchUpInside];
    //[container addSubview:retryButton];
    
    UIView *mainSeparator = [[UIView alloc] initWithFrame:mainSeparatorBox];
    mainSeparator.backgroundColor = UIColorFromRGB(0x0);
    [container addSubview:mainSeparator];
    
    CGRect messageRect, imageRect;
    
    UIView *step1 = [[UIView alloc] initWithFrame:step1Box];
    [container addSubview:step1];
    
    CGRectDivide(step1.bounds, &messageRect, &imageRect, STEP_MESSAGE_HEIGHT, CGRectMinYEdge);
    messageRect = CGRectInset(messageRect, STEP_MESSAGE_X_PADDING, STEP_MESSAGE_Y_PADDING);
    imageRect = CGRectInset(imageRect, STEP_IMAGE_X_PADDING, 0);
    
    RTLabel *step1Label = [[RTLabel alloc] initWithFrame:messageRect];
    step1Label.textAlignment = NSTextAlignmentLeft;
    step1Label.font = [UIFont fontWithName:@"Cochin" size:14];
    step1Label.text = @"<b>Step 1</b>: Go to the <i><b>settings</b></i> menu";
    [step1 addSubview:step1Label];
    
    UIImageView *step1Image = [[UIImageView alloc] initWithFrame:imageRect];
    step1Image.image = [UIImage imageNamed:@"photo_access_denied_recovery_1.png"];
    [step1 addSubview:step1Image];
    
    UIView *step2 = [[UIView alloc] initWithFrame:step2Box];
    [container addSubview:step2];
    
    CGRectDivide(step2.bounds, &messageRect, &imageRect, STEP_MESSAGE_HEIGHT, CGRectMinYEdge);
    messageRect = CGRectInset(messageRect, STEP_MESSAGE_X_PADDING, STEP_MESSAGE_Y_PADDING);
    imageRect = CGRectInset(imageRect, STEP_IMAGE_X_PADDING, 0);
    
    RTLabel *step2Label = [[RTLabel alloc] initWithFrame:messageRect];
    step2Label.textAlignment = NSTextAlignmentLeft;
    step2Label.font = [UIFont fontWithName:@"Cochin" size:14];
    step2Label.text = @"<b>Step 2</b>: Tap on <i></b>Privacy</b></i>";
    [step2 addSubview:step2Label];
    
    UIImageView *step2Image = [[UIImageView alloc] initWithFrame:imageRect];
    step2Image.image = [UIImage imageNamed:@"photo_access_denied_recovery_2.png"];
    [step2 addSubview:step2Image];
    
    
    UIView *step3 = [[UIView alloc] initWithFrame:step3Box];
    [container addSubview:step3];
    
    CGRectDivide(step3.bounds, &messageRect, &imageRect, STEP_MESSAGE_HEIGHT, CGRectMinYEdge);
    messageRect = CGRectInset(messageRect, STEP_MESSAGE_X_PADDING, STEP_MESSAGE_Y_PADDING);
    imageRect = CGRectInset(imageRect, STEP_IMAGE_X_PADDING, 0);
    
    RTLabel *step3Label = [[RTLabel alloc] initWithFrame:messageRect];
    step3Label.textAlignment = NSTextAlignmentLeft;
    step3Label.font = [UIFont fontWithName:@"Cochin" size:14];
    step3Label.text = @"<b>Step 3</b>: Tap on <i><b>Photos</b></i>";
    [step3 addSubview:step3Label];
    
    UIImageView *step3Image = [[UIImageView alloc] initWithFrame:imageRect];
    step3Image.image = [UIImage imageNamed:@"photo_access_denied_recovery_3.png"];
    [step3 addSubview:step3Image];
    
    UIView *step4 = [[UIView alloc] initWithFrame:step4Box];
    [container addSubview:step4];
    
    CGRectDivide(step4.bounds, &messageRect, &imageRect, STEP_MESSAGE_HEIGHT, CGRectMinYEdge);
    messageRect = CGRectInset(messageRect, STEP_MESSAGE_X_PADDING, STEP_MESSAGE_Y_PADDING);
    imageRect = CGRectInset(imageRect, STEP_IMAGE_X_PADDING, 0);
    
    RTLabel *step4Label = [[RTLabel alloc] initWithFrame:messageRect];
    step4Label.textAlignment = NSTextAlignmentLeft;
    step4Label.font = [UIFont fontWithName:@"Cochin" size:14];
    step4Label.text = @"<b>Step 4</b>: Tap on the <i><b>OFF</b></i> button";
    [step4 addSubview:step4Label];
    
    UIImageView *step4Image = [[UIImageView alloc] initWithFrame:imageRect];
    step4Image.image = [UIImage imageNamed:@"photo_access_denied_recovery_4.png"];
    [step4 addSubview:step4Image];
    
    return container;
}
                   
-(void)onAccessDeniedRetryButtonClick {
    [self.accessDeniedErrorView removeFromSuperview];
    self.accessDeniedErrorView = nil;
    
    [ModalAlertView showBusy:@"retrying, please wait..."];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void)
    {
        [ModalAlertView hideBusyAlert];
        [self load];
    });
}
@end
