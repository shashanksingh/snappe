//
//  AlbumEditViewController.m
//  snappe
//
//  Created by Shashank on 19/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AlbumEditView.h"
#import "AlbumAccessor.h"
#import "Macros.h"
#import "AlbumPhotoView.h"
#import "SnappeHTTPService.h"
#import "Util.h"
#import "UIController.h"
#import "Logger.h"
#import "SSTextView.h"
#import "SnappeTextField.h"
#import "AlbumEditIntroOverlay.h"
#import "UIView+Help.h"
#import <QuartzCore/QuartzCore.h>

#define MARGIN_X 25
#define MARGIN_TOP 25
#define MARGIN_BOTTOM 70
#define NAME_FIELD_CONTAINER_HEIGHT 40
#define FIELD_CONTAINER_PADDING 2
#define INTER_FIELD_GAP 10
#define PHOTO_GRID_TOP_MARGIN 5

#define VIEW_X_INSET 5
#define VIEW_Y_INSET 5




@interface AlbumEditView ()
@property (strong) Album *album;
@property (strong) UIScrollView *scrollContainer;
@property (strong) SnappeTextField *albumNameField;
@property (strong) SSTextView *albumDescriptionField;
@end

@implementation AlbumEditView
@synthesize album;
@synthesize scrollContainer;
@synthesize albumNameField;
@synthesize albumDescriptionField;

-(id) init:(Album*)album {
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if(self) {
        self.album = album;
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75f]];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        tapRecognizer.delegate = self;
        tapRecognizer.cancelsTouchesInView = NO;
        [self addGestureRecognizer:tapRecognizer];
        
        [self drawOnRect:frame];
        [Util showPopup:self :self.scrollContainer :YES :nil];
        
        [AlbumEditIntroOverlay show:nil];
    }
    return  self;
}

+(void)show:(Album *)album {
    AlbumEditView *view = [[AlbumEditView alloc] init:album];
    [Util showModalView:view];
    
    [Logger track:@"album_edit_view_shown" :@{@"aid" : album.id}];
}

- (void)drawOnRect:(CGRect)frame
{
    
    CGRect mainScrollContainerRect, nameFieldContainerRect, photoGridScrollContainerRect, nameFieldRect;
        
    mainScrollContainerRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    mainScrollContainerRect = [Util rectInset:mainScrollContainerRect :MARGIN_TOP :MARGIN_X :MARGIN_BOTTOM :MARGIN_X];
    
    CGRect rect = CGRectMake(0, 0, mainScrollContainerRect.size.width, mainScrollContainerRect.size.height);
    CGRectDivide(rect, &nameFieldContainerRect, &photoGridScrollContainerRect, NAME_FIELD_CONTAINER_HEIGHT, CGRectMinYEdge);
    
    nameFieldContainerRect = CGRectInset(nameFieldContainerRect, VIEW_X_INSET, VIEW_Y_INSET);
    nameFieldRect = CGRectMake(0, 0, nameFieldContainerRect.size.width, nameFieldContainerRect.size.height);
    nameFieldRect = CGRectInset(nameFieldRect, FIELD_CONTAINER_PADDING, FIELD_CONTAINER_PADDING);
    
    photoGridScrollContainerRect = CGRectInset(photoGridScrollContainerRect, VIEW_X_INSET, VIEW_Y_INSET);
    
    self.scrollContainer = [[UIScrollView alloc] initWithFrame:mainScrollContainerRect];
    [self.scrollContainer setShowsVerticalScrollIndicator:NO];
    [self.scrollContainer setShowsHorizontalScrollIndicator:NO];
    [self.scrollContainer setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 1)];
    
    UIView *nameView = [[UIView alloc] initWithFrame:nameFieldContainerRect];
    [self.scrollContainer addSubview:nameView];
    [nameView setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 1)];
    [nameView.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [nameView.layer setBorderWidth: 3];
    [Util drawFastShadow:nameView];
    [nameView.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [nameView.layer setShadowRadius:3.0];
    [nameView.layer setShadowOpacity:0.5];
    
    self.albumNameField = [[SnappeTextField alloc] initWithFrame:nameFieldRect];
    self.albumNameField.delegate = self;
    self.albumNameField.placeholder = @"album name";
    self.albumNameField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.albumNameField setFont: [UIFont fontWithName:@"Helvetica" size:12]];
    [self.albumNameField setTextColor:UIColorFromRGB(0x0)];
    [self.albumNameField setText:[self.album getName]];
    [self.albumNameField setReturnKeyType:UIReturnKeyDone];
    [nameView addSubview:self.albumNameField];
    if(self.album.isCurrentDevicePhysicalAlbum) {
        nameView.backgroundColor = UIColorFromRGB(0xF0F0F0);
        self.albumNameField.enabled = NO;
    }
    [self.albumNameField setHelpText:@"change the album name"];
    
    UIView *photoGridScrollerContainer = [[UIView alloc] initWithFrame:photoGridScrollContainerRect];
    [self.scrollContainer addSubview:photoGridScrollerContainer];
    [photoGridScrollerContainer setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 0.95)];
    [photoGridScrollerContainer.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [photoGridScrollerContainer.layer setBorderWidth:2];
    [Util drawFastShadow:photoGridScrollerContainer];
    [photoGridScrollerContainer.layer setShadowOffset:CGSizeMake(0.0, -2.0)];
    [photoGridScrollerContainer.layer setShadowRadius:3.0];
    [photoGridScrollerContainer.layer setShadowOpacity:0.5];
    
    CGRect photoGridRect = CGRectMake(0, 0, photoGridScrollContainerRect.size.width, photoGridScrollContainerRect.size.height);
    AlbumPhotoView *photoGrid = [[AlbumPhotoView alloc] initWithFrame:photoGridRect :album];
    [photoGridScrollerContainer addSubview:photoGrid];
}

/*-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    CGPoint relativePoint = [self.homeButton convertPoint:point fromView:self];
    if(CGRectContainsPoint(self.homeButton.bounds, relativePoint)) {
        return self.homeButton;
    }
    return self;
}*/


- (void)textViewDidEndEditing:(UITextView*)textView {
    [self updateAlbumName];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void) moveTextViewForKeyboard:(UITextView *)textView onNotification:(NSNotification*)aNotification up: (BOOL) up{
    NSDictionary* userInfo = [aNotification userInfo];
    
    // Get animation info from userInfo
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    
    // Animate up or down
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = textView.frame;
    CGRect keyboardFrame = [self convertRect:keyboardEndFrame toView:nil];
    
    newFrame.size.height -= keyboardFrame.size.height * (up? 1 : -1);
    textView.frame = newFrame;
    
    [UIView commitAnimations];
}

-(void) updateAlbumName {
    NSString *text = self.albumNameField.text;
    NSString *exisitingName = self.album.name;
    if([exisitingName isEqualToString:text]) {
        return;
    }
    
    [self.album setName:text];
    [[SnappeHTTPService getInstance] updateAlbumName:text :self.album :^(id data) {
        [Logger track:@"album_description_updated" :[NSDictionary dictionaryWithObjectsAndKeys:self.album.id, @"album_id", exisitingName, @"old_value", text, @"new_value", nil]];
        
    } :^(NSError *error) {
        [Logger logError:[NSString stringWithFormat:@"error in updating album name, album id: %@, new name: %@", self.album.id, text] :error];
        
        [Util alertError:error:[NSString stringWithFormat:@"There was an unexpected error in updating album %@ name. Please try again later", self.album.name]];
    }];
}

-(void)onTap:(UIGestureRecognizer*)gestureRecognizer {
    [self updateAlbumName];
    
    [Logger track:@"album_edit_view_hidden" :@{@"aid" : self.album.id}];
    
    if([self.albumNameField isFirstResponder]) {
        [self.albumNameField resignFirstResponder];
    }
    else if(!CGRectContainsPoint(self.scrollContainer.frame, [gestureRecognizer locationInView:self])) {
        [self removeFromSuperview];
    }
}
@end