//
//  AlbumDownloadSuggestionView.h
//  snappe
//
//  Created by Shashank on 05/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumAccessor.h"

@interface AlbumSuggestionView : UIView<UIGestureRecognizerDelegate>
- (id)init:(NSString*)message:(NSArray*)suggestions:(Album*)defaultChoice;
-(void)update:(NSString*)message:(NSArray*)suggestions:(Album*)defaultChoice;
@end
