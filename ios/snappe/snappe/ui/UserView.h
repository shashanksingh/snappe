//
//  UserView.h
//  snappe
//
//  Created by Shashank on 16/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserAccessor.h"

@interface UserView : UIView
-(id) init:(CGRect)frame :(User *)user;
-(void)updateUser:(User*)user;
@end
