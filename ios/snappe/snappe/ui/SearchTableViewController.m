//
//  SearchView.m
//  snappe
//
//  Created by Shashank on 15/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchTableViewController.h"
#import "SearchBar.h"
#import <QuartzCore/QuartzCore.h>
#import "Macros.h"
#import "GraphManager.h"
#import "SearchResultViewCell.h"
#import "EventManager.h"
#import "ContactSearchManager.h"
#import "Contact.h"
#import "UserAccessor.h"
#import "SortManager.h"
#import "AlbumAccessor.h"
#import "Util.h"
#import "Logger.h"
#import "EmailShareIntroOverlay.h"
#import "FriendShareIntroOverlay.h"
#import "AlbumShareOperation.h"
#import "ActionConfirmationView.h"


#define SEARCH_BAR_HEIGHT 44
#define MAX_SEARCH_RESULTS 100
#define MAX_VISIBLE_ITEMS 5
#define SEARCH_RESULT_ROW_HEIGHT 64

#define TABLE_RESIZE_DURATION 0.25

#define SEARCH_START_DELAY 0

@interface SearchTableViewController ()
@property SearchType searchType;
@property (strong) NSMutableDictionary *context;
@property (strong) SearchBar *searchBar;
@property (strong) UISearchDisplayController *controller;
@property (strong) NSArray *searchResults;
@property BOOL isResizing;
@end

@implementation SearchTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.isResizing = NO;
        self.searchResults = nil;
        self.searchBar = [[SearchBar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, SEARCH_BAR_HEIGHT)];
        [self.searchBar setDelegate:self];
        
        self.tableView.tableHeaderView = self.searchBar;
        [self.tableView setDataSource:self];
        [self.tableView setDelegate:self];
        
        [self.view setBackgroundColor:[UIColor whiteColor]];
        [self.view.layer setBorderColor: [UIColor whiteColor].CGColor];
        [self.view.layer setBorderWidth:3];
        [Util drawFastShadow:self.view];
        [self.view.layer setShadowColor:[UIColor whiteColor].CGColor];
        [self.view.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
        [self.view.layer setShadowRadius:5.0];
        [self.view.layer setShadowOpacity:0.5];
        
        [self update:SNAPPE :@{}];
        
    }
    return self;
}

-(void)update:(SearchType)searchType_:(NSDictionary*)context_ {
    self.searchType = searchType_;
    self.context = [context_ mutableCopy];
    [self updatePlaceholderText];
    
    NSMutableDictionary *changes = [NSMutableDictionary dictionary];
    self.context[@"changes"] = changes;
    
    if(searchType_ == DEVICE) {
        self.searchBar.keyboardType = UIKeyboardTypeEmailAddress;
    }
    else if(searchType_ == SNAPPE) {
        [self.searchBar setReturnKeyType:UIReturnKeyDone];
    }
    else {
        self.searchBar.keyboardType = UIKeyboardTypeDefault;
        [self.searchBar setReturnKeyType:UIReturnKeyDone];
    }
    
    NSArray *suggestions = [context_ objectForKey:@"suggestions"];
    if(suggestions) {
        __weak typeof(self) bself = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.8 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            if(!bself) {
                return;
            }
            [bself prepopulate:suggestions];
        });
    }
}

-(NSDictionary*)getChanges {
    return self.context[@"changes"];
}

-(void)updatePlaceholderText {
    NSString *placeHolder = @"";
    if(self.searchType == SNAPPE) {
        placeHolder = @"search albums";
    }
    else if(self.searchType == DEVICE) {
        placeHolder = @"search email contacts or type in an email";
    }
    else if(self.searchType == ALBUM) {
        placeHolder = @"select album to download into";
    }
    else if(self.searchType == FB_ALBUM) {
        placeHolder = @"select album to upload to";
    }
    else if(self.searchType == FRIEND) {
        Album *album = [self.context objectForKey:@"album"];
        if([album isFacebookAlbum]) {
            placeHolder = [NSString stringWithFormat:@"search friends allowed to upload to '%@'", [Util ellipsify:album.name :13]];
        }
        else {
            placeHolder = [NSString stringWithFormat:@"search friends '%@' is shared with", [Util ellipsify:album.name :12]];
        }
    }
    else {
        DLog(@"warning: unknown value for searchType: %d", self.searchType);
    }
    [self.searchBar setPlaceholder:placeHolder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(!self.searchResults) {
        return 0;
    }
    
    NSUInteger count = [self.searchResults count];
    if(count == 0) {
        //this will force an empty row which will mean no
        //flashing of "no results". we anyway collapse
        //the rows, so the empty row won't be visible for long
        return 1;
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSMutableDictionary *ctxt = [self.context mutableCopy];
    if(self.searchResults && [self.searchResults count] > 0) {
        ctxt[@"subject"] = [self.searchResults objectAtIndex:indexPath.row];
    }
    ctxt[@"parent_context"] = self.context;
    ctxt[@"query"] = self.searchBar.text;
    
    //we pretend that there is 1 result in case of no results. return an empty cell in
    //this case, fill otherwise
    if(!cell || !(self.searchResults && [self.searchResults count] > 0)) {
        cell = [[SearchResultViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier: self.tableView.frame.size.width: SEARCH_RESULT_ROW_HEIGHT];
    }
    
    if(self.searchResults && [self.searchResults count] > 0) {
        SearchResultViewCell *customCell = (SearchResultViewCell*)cell;
        [customCell update:self.searchType:ctxt];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return SEARCH_RESULT_ROW_HEIGHT;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.searchType != FRIEND && self.searchType != DEVICE) {
        NSManagedObject *selectedObject = [self.searchResults objectAtIndex:indexPath.row];
        if([selectedObject isKindOfClass:[Contact class]]) {
            BOOL allowUpload = NO;
            
            SearchResultViewCell * cell = (SearchResultViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
            NSDictionary *cellContext = cell.context;
            if(cell.context) {
                NSNumber *allowUploadNumber = [cellContext objectForKey:@"allow_upload"];
                allowUpload = [allowUploadNumber boolValue];
            }
            
            [[EventManager getInstance] fireEvent:CONTACT_SELECTED_ON_SEARCH, selectedObject, @(allowUpload), nil];
        }
        else {
            [[EventManager getInstance] fireEvent:ALBUM_OR_FRIEND_SELECTED_ON_SEARCH, selectedObject, nil];
        }
        
        [[EventManager getInstance] fireEvent:SEARCH_OVERLAY_SHOULD_HIDE, nil];
    }
}

- (void)searchBar:(UISearchBar *)searchBar_ textDidChange:(NSString *)searchText {
    if(![self.searchBar isEqual:searchBar_]) {
        return;
    }
    
    [self refresh:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

-(void)refresh:(NSString*)searchQuery {
    
    if(searchQuery) {
        if(self.searchType == SNAPPE) {
            self.searchResults = [[GraphManager getInstance] search:searchQuery :MAX_SEARCH_RESULTS];
            [self onSearchResults];
        }
        else if(self.searchType == DEVICE) {
            self.searchResults = [[ContactSearchManager getInstance] search:searchQuery :MAX_SEARCH_RESULTS];
            if([self.searchResults count] < MAX_SEARCH_RESULTS && [Util isValidEmail:searchQuery]) {
                Contact *c = [[Contact alloc] init];
                c.name = searchQuery;
                c.emails = [NSArray arrayWithObject:searchQuery];
                NSMutableArray *tempArray = [self.searchResults mutableCopy];
                [tempArray addObject:c];
                self.searchResults = tempArray;
            }
            [self onSearchResults];
        }
        else if(self.searchType == ALBUM || self.searchType == FB_ALBUM) {
            
            NSArray *searchResults = @[];
            
            if (searchQuery.length == 0) {
                if(self.searchType == FB_ALBUM) {
                    searchResults = [[GraphManager getInstance] getFacebookAlbums];
                }
                else {
                    searchResults = [[GraphManager getInstance] getAlbums];
                }
            }
            else {
                searchResults = [[GraphManager getInstance] searchAlbums:searchQuery :(self.searchType == FB_ALBUM) :MAX_SEARCH_RESULTS - 1];
                searchResults = [@[[CurrentSearchQueryAsResult new]] arrayByAddingObjectsFromArray:searchResults];
            }
            
            self.searchResults = searchResults;
            [self onSearchResults];
        }
        else if(self.searchType == FRIEND) {
            Album *album = [self.context objectForKey:@"album"];
            
            NSArray *matchingObjects = @[];
            if(searchQuery.length == 0) {
                 matchingObjects = [[GraphManager getInstance] getUsers];
            }
            else {
                matchingObjects = [[GraphManager getInstance] searchUsers:searchQuery:MAX_SEARCH_RESULTS];
            }
            
            NSMutableArray *alreadyUploaders = [NSMutableArray array];
            NSMutableArray *friendsToSort = [NSMutableArray array];
            
            for(User *friend in matchingObjects) {
                if([album isAllowedUpload:friend]) {
                    [alreadyUploaders addObject:friend];
                }
                else {
                    [friendsToSort addObject:friend];
                }
            }
            
            __weak typeof(self) bself = self;
            [[SortManager getInstance] sort:friendsToSort:album:SMART:NO:^(NSArray *sortedNonUploaders){
                //if query has already changed, assume that this request was cancelled
                if([bself.searchBar.text isEqualToString:searchQuery]) {
                    NSMutableArray *results = alreadyUploaders;
                    [results addObjectsFromArray:sortedNonUploaders];
                    
                    NSRange range = NSMakeRange(0, MIN(MAX_SEARCH_RESULTS, [results count]));
                    results = [[results subarrayWithRange:range] mutableCopy];
                    
                    bself.searchResults = results;
                    [bself onSearchResults];
                }
                
            }:^(NSError *error){
                DLog(@"error in searching: %@", error);
            }];
            
        }
    }
    else {
        self.searchResults = @[];
        [self onSearchResults];
    }
    
}

-(void)prepopulate:(NSArray*)suggestions {
    self.searchResults = suggestions;
    [self onSearchResults];
}

-(void)onSearchResults {
    [self resizeTable];
    [self.tableView reloadData];
    [Logger track:@"search" :@{@"type" : [ModalSearchView searchTypeToString:self.searchType], @"query" : self.searchBar.text, @"result_count" : @(self.searchResults.count)}];
}

-(void)resizeTable {
    self.isResizing = YES;
    
    __block typeof(self) bself = self;
    
    [UIView animateWithDuration:TABLE_RESIZE_DURATION animations:^{
        if(!bself) {
            return;
        }
        
        CGFloat height = SEARCH_BAR_HEIGHT + (MIN([bself.searchResults count], MAX_VISIBLE_ITEMS) * SEARCH_RESULT_ROW_HEIGHT);
        
        [Util setUIViewHeight:bself.tableView :height];
        
        
    } completion:^(BOOL finished) {
        if(!bself) {
            return;
        }
        
        bself.isResizing = NO;
        
        if(bself.searchResults && bself.searchResults.count > 0) {
            if(bself.searchType == DEVICE) {
                Album *album = [bself.context objectForKey:@"album"];
                if([album currentUserCanUpload]) {
                    [EmailShareIntroOverlay show:nil];
                }
            }
            if(bself.searchType == FRIEND) {
                Album *album = [self.context objectForKey:@"album"];
                BOOL pointToShareButton = ![album isFacebookAlbum];
                [FriendShareIntroOverlay show:@(pointToShareButton)];
            }
        }
    }];
}

-(void)confirmSharing:(Album*)album:(NSDictionary*)changes:(GenericCallback)callback {
    [Logger track:@"search_confirm_sharing" :@{@"type" : [ModalSearchView searchTypeToString:self.searchType]}];
    if(self.searchType == CONTACT) {
        NSString *confrimationMessage = [NSString stringWithFormat:@"Are you sure you want to update sharing '%@' with '%@'?", album.name, [Util getNameForMultipleContacts:[changes.allValues valueForKey:@"subject"]]];
        
        [ActionConfirmationView confirm:@"email_share" :confrimationMessage :callback];
    }
    else {
        NSString *confrimationMessage = [NSString stringWithFormat:@"Are you sure you want to update sharing '%@' with '%@'?", album.name, [Util getNameForMultipleUsers:[changes.allValues valueForKey:@"subject"] :NO]];
        [ActionConfirmationView confirm:@"friend_share" :confrimationMessage :callback];
    }
}

-(void)processChanges {
    NSDictionary *changes = [self getChanges];
    
    [Logger track:@"search_process_changes" :@{@"type" : [ModalSearchView searchTypeToString:self.searchType], @"change_count" : @(changes.count)}];
    if(changes.count == 0) {
        return;
    }
    
    Album *album = [self.context objectForKey:@"album"];
    SearchType searchType = self.searchType;
    
    __block typeof(self) bself = self;
    
    [self confirmSharing:album :changes :^(NSNumber *ok) {
        if(!ok.boolValue) {
            return;
        }
       
        DLog(@"changes: %@", changes);
        
        BOOL allowingUpload = NO;
        
        NSMutableDictionary *changeDescriptors = [NSMutableDictionary dictionary];
        for(NSString *subjectID in changes.allKeys) {
            
            NSMutableDictionary *changeDescriptor = changes[subjectID];
            
            NSMutableDictionary *desc = [NSMutableDictionary dictionary];
            desc[@"subject"] = changeDescriptor[@"subject"];
            for(NSString *prop in @[@"share", @"allow_upload"]) {
                if(changeDescriptor[prop]) {
                    desc[prop] = changeDescriptor[prop];
                }
            }
            
            if(!allowingUpload) {
                NSNumber *allowUpload = changeDescriptor[@"allow_upload"];
                if(allowUpload && allowUpload.boolValue) {
                    allowingUpload = YES;
                }
            }
            
            changeDescriptors[subjectID] = desc;
        }
        
        if(!bself) {
            return;
        }
        
        VoidCallback onPermissionsAsked = ^() {
            if(searchType == FRIEND) {
                AlbumShareOperation *extOp = [[AlbumShareOperation alloc] init:album :changeDescriptors :USER];
                [Util executeExtOp:extOp];
                
            }
            else if(searchType == DEVICE) {
                AlbumShareOperation *extOp = [[AlbumShareOperation alloc] init:album :changeDescriptors :CONTACT];
                [Util executeExtOp:extOp];
            }
        };
        
        if(allowingUpload) {
            [bself askForPublishPermission:^(NSNumber *granted) {
                onPermissionsAsked();
            }];
        }
        else {
            onPermissionsAsked();
        }
     
        
    }];
}

-(void)askForPublishPermission:(GenericCallback)callback {
    __block typeof(self) bself = self;
    
    [[FBManager getInstance] authorizeWithPublishPermissions:^(NSNumber *publishPermissionGranted) {
        if(!publishPermissionGranted.boolValue) {
            [ActionConfirmationView confirm:@"allow_upload_wo_publish_permissions" :@"Without permission to post on your behalf photos added to your Facebook albums by your friends will require manual approval from you everytime. Do you wish to retry granting Snappe the permission?" :^(NSNumber *retry)
             {
                 if(retry.boolValue) {
                     if(!bself) {
                         return;
                     }
                     [bself askForPublishPermission:callback];
                 }
                 else {
                     callback(@(NO));
                 }
                 
             }];
        }
        else {
            callback(@(YES));
        }
    }];
}
@end
