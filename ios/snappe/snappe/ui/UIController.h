//
//  UICoreController.h
//  snappe
//
//  Created by Shashank on 20/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "OwnAlbumsCarouselView.h"

typedef enum UIControlID {
    ROOT_VIEW_CONTROLLER,
    HOME_VIEW_CONTROLLER,
    NOTIFICATION_VIEW_CONTROLLER,
    OWN_ALBUM_CAROUSEL_VIEW,
    FRIEND_ALBUM_CAROUSEL_VIEW
}UIControlID;

typedef enum CachedUIViewType {
    LOADING_IMAGE_INDICATOR_DARK,
    LOADING_IMAGE_INDICATOR_LIGHT,
    PHOTO_PLACEHOLDER,
    PROFILE_PIC_PLACEHOLDER
}CachedUIViewType;


@interface UIController : NSObject
+(UIController *) getInstance;
-(id)getUIControl:(UIControlID)type;
-(void)setUIControl:(id)control :(UIControlID)type;

-(void)queueCachedUIView:(UIView*)view:(CachedUIViewType)type;
-(UIView*)popCachedUIView:(CachedUIViewType)type;
@end
