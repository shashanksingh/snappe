//
//  UserView.m
//  snappe
//
//  Created by Shashank on 16/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserView.h"
#import "Util.h"
#import <QuartzCore/QuartzCore.h>

#define BORDER_SIZE 5

@interface UserView()
@property User *user;
@property UILabel *title;
@property UIView *profilePicContainer;
@end

@implementation UserView

-(id) init:(CGRect)frame :(User *)user
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.layer setBorderColor: [[UIColor whiteColor] CGColor]]; 
        [self.layer setBorderWidth: BORDER_SIZE];
        [Util drawFastShadow:self];
        [self.layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
        [self.layer setShadowRadius:3.0];
        [self.layer setShadowOpacity:1.0];
        
        [self drawWithFrame:self.frame];
        [self updateUser:user];
        
    }
    return self;
}

-(void)updateUser:(User*)user {
    self.user = user;
    [self.title setText: [self.user getName]];
    
    UIImageView *profilePicView = [self.user getProfilePic:self.profilePicContainer.bounds.size.width :self.profilePicContainer.frame.size.height :NO];
    [profilePicView setFrame:self.profilePicContainer.bounds];
    [Util removeAllSubviews:self.profilePicContainer];
    [self.profilePicContainer addSubview:profilePicView];
    
}

-(void) drawWithFrame:(CGRect)frame {
    CGFloat w = frame.size.width;
    CGFloat h = frame.size.height;
    
    CGFloat titleSectionHeight = h * 0.20;
    CGFloat thumbSectionHeight = h * 0.80;
    
    CGRect titleRect = CGRectMake(BORDER_SIZE + 2, 0, w - 2 * BORDER_SIZE, titleSectionHeight);
    CGRect profilePicRect = CGRectMake(BORDER_SIZE + 2, titleSectionHeight, w - 2*BORDER_SIZE, thumbSectionHeight);
    
    self.title = [[UILabel alloc] initWithFrame:titleRect];
    self.title.adjustsFontSizeToFitWidth = NO;
    self.title.lineBreakMode = UILineBreakModeTailTruncation;
    
    [self.title setTextColor:UIColorFromRGB(0x0)];
    [self.title setFont: [UIFont fontWithName:@"AmericanTypewriter" size:11.0f]];
    [self addSubview:self.title];
    
    
    self.profilePicContainer = [[UIView alloc] init];
    self.profilePicContainer.frame = profilePicRect;
    [self addSubview:self.profilePicContainer];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
