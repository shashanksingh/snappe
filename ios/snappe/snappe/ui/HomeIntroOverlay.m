//
//  HomeIntroOverlay.m
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import "HomeIntroOverlay.h"
#import "FBManager.h"

@implementation HomeIntroOverlay

-(NSArray*)getParameters {
    IntroHint *localAlbumsHint = [IntroHint new];
    localAlbumsHint.labelRect = CGRectMake(self.frame.size.width * 0.1, self.frame.size.height * 0.85, 200, 60);
    localAlbumsHint.labelText = @"photos in your phone, neatly arranged as albums";
    localAlbumsHint.hintTargetLocation = CGPointMake(self.frame.size.width * 0.1, self.frame.size.height * 0.8);
    
    IntroHint *remoteAlbumsHint = [IntroHint new];
    remoteAlbumsHint.labelRect = CGRectMake(self.frame.size.width * 0.1, 2, 250, 60);
    if([[FBManager getInstance] isAuthorized]) {
        remoteAlbumsHint.labelText = @"albums on Facebook & on friends' phones";
    }
    else {
        remoteAlbumsHint.labelText = @"albums on Facebook & on friends' phones will appear here once you connect with Facebook";
    }
    remoteAlbumsHint.hintTargetLocation = CGPointMake(self.frame.size.width * 0.1, self.frame.size.height * 0.2);
    
    IntroHint *syncControlsHint = [IntroHint new];
    syncControlsHint.labelRect = CGRectMake(self.frame.size.width * 0.1, self.frame.size.height * 0.38, 200, 20);
    syncControlsHint.labelText = @"pull, push & sync local & remote albums";
    syncControlsHint.hintTargetLocation = CGPointMake(self.frame.size.width * 0.40, self.frame.size.height * 0.45);
    
    IntroHint *helpHint = [IntroHint new];
    helpHint.labelRect = CGRectMake(self.frame.size.width - 180, self.frame.size.height * 0.57, 180, 20);
    helpHint.labelText = @"tap anytime to enter help mode";
    helpHint.hintTargetLocation = CGPointMake(self.frame.size.width - 60, self.frame.size.height - 60);
    
    
    return @[localAlbumsHint, remoteAlbumsHint, syncControlsHint, helpHint];
}

+(NSString*)getIntroID {
    return @"home";
}

@end
