//
//  AlbumCreatorView.m
//  snappe
//
//  Created by Shashank on 21/02/13.
//
//

#import "AlbumCreatorView.h"
#import "SnappeTextField.h"
#import "AlbumAccessor.h"
#import "Util.h"
#import "PhotoPickerView.h"
#import "PhotoSaveManager.h"
#import "ModalAlertView.h"
#import "Logger.h"
#import "PhotoLibraryOrganizer.h"
#import "PhotoLibraryReader.h"
#import "CreateAlbumIntroOverlay.h"
#import "UIView+Help.h"
#import "SnappeHTTPService.h"
#import "BButton.h"
#import "AlbumCreationOperation.h"
#import <QuartzCore/QuartzCore.h>

#define MARGIN_X 20
#define MARGIN_Y 45
#define ALBUM_TYPE_SELECTOR_BUTTON_SIZE 32
#define NAME_FIELD_CONTAINER_HEIGHT 44
#define FIELD_CONTAINER_PADDING 10
#define INTER_FIELD_GAP 10
#define PHOTO_GRID_TOP_MARGIN 5
#define BUTTON_SECTION_PADDING 7
#define DO_BUTTON_HEIGHT 38
#define DO_BUTTON_WIDTH 140

#define VIEW_X_INSET 5
#define VIEW_Y_INSET 5


@interface AlbumCreatorView()<UIGestureRecognizerDelegate, UITextFieldDelegate>
@property (strong) UIButton *deviceAlbumTypeButton;
@property (strong) UIButton *facebookAlbumTypeButton;
@property (strong) SnappeTextField *albumNameField;
@property (strong) UIView *container;
@property (strong) UIButton *doButton;
@property (strong) PhotoPickerView *photoPicker;
@end

@interface AlbumCreatorView()
@property BOOL facebookAlbum;
@end

@implementation AlbumCreatorView

-(id) init:(BOOL)facebookAlbum {
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if(self) {
        self.facebookAlbum = facebookAlbum;
        
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75f]];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        tapRecognizer.delegate = self;
        tapRecognizer.cancelsTouchesInView = NO;
        [self addGestureRecognizer:tapRecognizer];
        
        [self drawOnRect:frame];
        [Util showPopup:self :self.container :YES :nil];
        
        [CreateAlbumIntroOverlay show:nil];
    }
    return  self;
}

+(void)show {
    [self show:NO];
}

+(void)show:(BOOL)facebookAlbum {
    AlbumCreatorView *view = [[AlbumCreatorView alloc] init:facebookAlbum];
    [Util showModalView:view];
    
    [Logger track:@"album_creator_shown" :@{@"facebook":@(facebookAlbum)}];
}


- (void)drawOnRect:(CGRect)frame {
    CGRect nameFieldContainerRect, photoGridScrollContainerRect, albumTypeSelectorRect,
    deviceAlbumTypeButtonRect, facebookAlbumTypeButtonRect,
    nameFieldRect, buttonContainerRect, doButtonRect;
    
    CGRect mainRect = CGRectInset(frame, MARGIN_X, MARGIN_Y);
    CGRect r = CGRectMake(0, 0, mainRect.size.width, mainRect.size.height);
    
    //CGRectDivide(r, &albumTypeSelectorRect, &r, ALBUM_TYPE_SELECTOR_REGION_HEIGHT, CGRectMinYEdge);
    CGRectDivide(r, &nameFieldContainerRect, &r, NAME_FIELD_CONTAINER_HEIGHT, CGRectMinYEdge);
    CGRectDivide(r, &buttonContainerRect, &photoGridScrollContainerRect, DO_BUTTON_HEIGHT + 2 * BUTTON_SECTION_PADDING, CGRectMaxYEdge);
    
    CGRectDivide(nameFieldContainerRect, &albumTypeSelectorRect, &nameFieldRect, 84, CGRectMaxXEdge);
    nameFieldRect = CGRectInset(nameFieldRect, 5, 5);
    
    deviceAlbumTypeButtonRect = CGRectMake(0, 0, ALBUM_TYPE_SELECTOR_BUTTON_SIZE, ALBUM_TYPE_SELECTOR_BUTTON_SIZE);
    deviceAlbumTypeButtonRect = [Util centerRect:deviceAlbumTypeButtonRect :[Util getRectCenter:albumTypeSelectorRect]];
    facebookAlbumTypeButtonRect = deviceAlbumTypeButtonRect;
    
    deviceAlbumTypeButtonRect.origin.x -= (ALBUM_TYPE_SELECTOR_BUTTON_SIZE/2) + 2;
    facebookAlbumTypeButtonRect.origin.x += (ALBUM_TYPE_SELECTOR_BUTTON_SIZE/2) + 2;
    
    buttonContainerRect = CGRectInset(buttonContainerRect, VIEW_X_INSET, VIEW_Y_INSET);
    photoGridScrollContainerRect = CGRectInset(photoGridScrollContainerRect, VIEW_X_INSET, VIEW_Y_INSET);
    
    doButtonRect = [Util centerRect:CGRectMake(0, 0, DO_BUTTON_WIDTH, DO_BUTTON_HEIGHT) :[Util getRectCenter:buttonContainerRect]];
    
    
    self.container = [[UIView alloc] initWithFrame:mainRect];
    [self.container setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 1.0f)];
    
    UIView *nameView = [[UIView alloc] initWithFrame:nameFieldContainerRect];
    [self.container addSubview:nameView];
    [nameView setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 1)];
    [nameView.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [nameView.layer setBorderWidth: 3];
    [Util drawFastShadow:nameView];
    [nameView.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [nameView.layer setShadowRadius:3.0];
    [nameView.layer setShadowOpacity:0.5];
    
    self.albumNameField = [[SnappeTextField alloc] initWithFrame:nameFieldRect];
    self.albumNameField.delegate = self;
    self.albumNameField.placeholder = @"album name";
    self.albumNameField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.albumNameField setFont: [UIFont fontWithName:@"Helvetica" size:12]];
    [self.albumNameField setTextColor:UIColorFromRGB(0x0)];
    self.albumNameField.borderStyle = UITextBorderStyleLine;
    self.albumNameField.layer.masksToBounds= YES;
    self.albumNameField.layer.borderColor= UIColorFromRGB(0xCFCFCF).CGColor;
    self.albumNameField.layer.borderWidth= 2.0f;
    
    [self.albumNameField setReturnKeyType:UIReturnKeyDone];
    [nameView addSubview:self.albumNameField];
    [self.albumNameField setHelpText:@"type a name for the new album"];
    
    
    self.deviceAlbumTypeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deviceAlbumTypeButton setImage:[UIImage imageNamed:@"iphone_circle.png"] forState:UIControlStateSelected];
    [self.deviceAlbumTypeButton setImage:[UIImage imageNamed:@"iphone_circle_off.png"] forState:UIControlStateNormal];
    self.deviceAlbumTypeButton.adjustsImageWhenHighlighted = NO;
    [self.deviceAlbumTypeButton addTarget:self action:@selector(onDeviceAlbumTypeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.deviceAlbumTypeButton setFrame:deviceAlbumTypeButtonRect];
    [self.deviceAlbumTypeButton setClipsToBounds:YES];
    [self.deviceAlbumTypeButton setBackgroundColor:[UIColor clearColor]];
    [self.deviceAlbumTypeButton setHelpText:@"create an album on this iPhone"];
    [self.deviceAlbumTypeButton setHelpSpotlightShapeOval:YES];
    [nameView addSubview:self.deviceAlbumTypeButton];
    
    self.facebookAlbumTypeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.facebookAlbumTypeButton setImage:[UIImage imageNamed:@"facebook_circle.png"] forState:UIControlStateSelected];
    [self.facebookAlbumTypeButton setImage:[UIImage imageNamed:@"facebook_circle_off.png"] forState:UIControlStateNormal];
    self.facebookAlbumTypeButton.adjustsImageWhenHighlighted = NO;
    [self.facebookAlbumTypeButton addTarget:self action:@selector(onFacebookAlbumTypeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.facebookAlbumTypeButton setFrame:facebookAlbumTypeButtonRect];
    [self.facebookAlbumTypeButton setClipsToBounds:YES];
    [self.facebookAlbumTypeButton setBackgroundColor:[UIColor clearColor]];
    [self.facebookAlbumTypeButton setHelpText:@"create an album on Facebook"];
    [self.facebookAlbumTypeButton setHelpSpotlightShapeOval:YES];
    [nameView addSubview:self.facebookAlbumTypeButton];
    

    
    UIView *photoGridScrollerContainer = [[UIView alloc] initWithFrame:photoGridScrollContainerRect];
    [self.container addSubview:photoGridScrollerContainer];
    [photoGridScrollerContainer setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 0.95)];
    
    CGRect photoGridRect = photoGridScrollerContainer.bounds;
    self.photoPicker = [[PhotoPickerView alloc] initWithFrame:photoGridRect:SELECT];
    [photoGridScrollerContainer addSubview:self.photoPicker];
    
    self.doButton = [[BButton alloc] initWithFrame:doButtonRect type:BButtonTypeSuccess];
    [self.doButton setTitle:@"Create Album" forState:UIControlStateNormal];
    [self.doButton addTarget:self action:@selector(onDoButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.container addSubview:self.doButton];
    
    self.deviceAlbumTypeButton.selected = !self.facebookAlbum;
    self.facebookAlbumTypeButton.selected = !self.deviceAlbumTypeButton.selected;
    
    [self populate];
    
}

-(void)populate {
    [self.photoPicker populate];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)textViewDidEndEditing:(UITextView*)textView {
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(void)onTap:(UIGestureRecognizer*)gestureRecognizer {
    if([self.albumNameField isFirstResponder]) {
        [self.albumNameField resignFirstResponder];
    }
    else if(!CGRectContainsPoint(self.container.frame, [gestureRecognizer locationInView:self])) {
        [self removeFromSuperview];
        [Logger track:@"album_creator_hidden_on_tap"];
    }
}


-(void)onDoButtonClick {
    NSString *albumName = self.albumNameField.text;
    if(albumName.length == 0) {
        [Util alertError:nil :@"Please enter a name for the album"];
        [Logger track:@"album_creator_empty_album_name"];
        return;
    }
    
    NSArray *selectedPhotos = [self.photoPicker getSelectedPhotos];
    
    AlbumCreationOperation *extOp = [[AlbumCreationOperation alloc] init:albumName :selectedPhotos :self.facebookAlbumTypeButton.selected];
    [Util executeExtOp:extOp];

    
    [Logger track:@"album_creator_do_button_clicked" :@{@"album_name": albumName, @"facebook": @(self.facebookAlbumTypeButton.selected), @"selected_photos_count" : @(selectedPhotos.count)}];
    
    [self removeFromSuperview];
}

-(void)onDeviceAlbumTypeButtonClick {
    self.deviceAlbumTypeButton.selected = !self.deviceAlbumTypeButton.selected;
    self.facebookAlbumTypeButton.selected = !self.deviceAlbumTypeButton.selected;
    [Logger track:@"album_creator_type_selected" :@{@"facebook" : @(NO)}];
}

-(void)onFacebookAlbumTypeButtonClick {
    self.facebookAlbumTypeButton.selected = !self.facebookAlbumTypeButton.selected;
    self.deviceAlbumTypeButton.selected = !self.facebookAlbumTypeButton.selected;
    [Logger track:@"album_creator_type_selected" :@{@"facebook" : @(YES)}];
}

@end
