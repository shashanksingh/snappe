//
//  Carousel.h
//  snappe
//
//  Created by Shashank on 23/01/13.
//
//

#import "SwipeView.h"
#import "AlbumAccessor.h"

typedef enum CarouselType {
    LOCAL, REMOTE
}CarouselType;

@protocol CarouselDataSource <NSObject>

@required
@property (readonly) NSArray *data;
@end

@interface Carousel : SwipeView
@property CarouselType type;

- (id)initWithFrame:(CGRect)frame:(CarouselType)type:(NSObject<CarouselDataSource>*)carouselDataSource;
-(UIView*) scrollToUserOrAlbum:(NSManagedObject*)scrollTarget;
-(void)fakeScrollToAlbum:(Album*)album:(NSTimeInterval)duration;
+(void) onFacebookLoginClick;
@end
