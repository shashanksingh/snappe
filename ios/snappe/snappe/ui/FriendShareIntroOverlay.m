//
//  FriendShareIntroOverlay.m
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import "FriendShareIntroOverlay.h"

@implementation FriendShareIntroOverlay

-(NSArray*)getParameters {
    IntroHint *shareHint = [IntroHint new];
    shareHint.labelRect = CGRectMake(self.frame.size.width * 0.05, self.frame.size.height * 0.45, 150, 60);
    shareHint.labelText = @"tap on this button to share the album with this friend";
    shareHint.hintTargetLocation = CGPointMake(self.frame.size.width * 0.6, self.frame.size.height * 0.22);
    
    IntroHint *allowUploadHint = [IntroHint new];
    allowUploadHint.labelRect = CGRectMake(self.frame.size.width * 0.2, self.frame.size.height * 0.65, 250, 60);
    allowUploadHint.labelText = @"tap on this button to allow the friend to upload photos to this album, from their phone!";
    allowUploadHint.hintTargetLocation = CGPointMake(self.frame.size.width * 0.75, self.frame.size.height * 0.25);
    
    BOOL pointToShareButton = ((NSNumber*)self.data).boolValue;
    if(pointToShareButton) {
        return @[shareHint, allowUploadHint];
    }
    return @[allowUploadHint];
    
}

+(NSString*)getIntroID {
    return @"friend_share";
}

-(CGFloat)getOpacity {
    return 0.2;
}

@end
