//
//  CreateAlbumIntroOverlay.m
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import "CreateAlbumIntroOverlay.h"

@implementation CreateAlbumIntroOverlay

-(NSArray*)getParameters {
    IntroHint *nameHint = [IntroHint new];
    nameHint.labelRect = CGRectMake(self.frame.size.width * 0.2, self.frame.size.height * 0.05, 150, 40);
    nameHint.labelText = @"type a name for the album";
    nameHint.hintTargetLocation = CGPointMake(self.frame.size.width * 0.25, self.frame.size.height * 0.08);
    
    IntroHint *thumbHint = [IntroHint new];
    thumbHint.labelRect = CGRectMake(self.frame.size.width * 0.1, self.frame.size.height * 0.85, 250, 60);
    thumbHint.labelText = @"tap on a photo to select it";
    thumbHint.hintTargetLocation = CGPointMake(self.frame.size.width * 0.25, self.frame.size.height * 0.25);
    
    return @[nameHint, thumbHint];
}

+(NSString*)getIntroID {
    return @"create_album";
}

@end
