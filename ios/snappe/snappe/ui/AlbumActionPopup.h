//
//  AlbumActionPopup.h
//  snappe
//
//  Created by Shashank on 01/03/13.
//
//

#import <UIKit/UIKit.h>

@interface AlbumActionPopup : UIView
- (id)init:(NSArray*)items:(CGPoint)point;
@end
