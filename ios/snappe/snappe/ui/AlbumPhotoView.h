//
//  AlbumPhotoView.h
//  snappe
//
//  Created by Shashank on 11/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumAccessor.h"

@interface AlbumPhotoView : UIScrollView
- (id)initWithFrame:(CGRect)frame :(Album*)album;
@end
