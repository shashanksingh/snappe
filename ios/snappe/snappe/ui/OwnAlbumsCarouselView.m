//
//  OwnAlbumsCarouselViewController.m
//  snappe
//
//  Created by Shashank on 13/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OwnAlbumsCarouselView.h"
#import "CoreController.h"
#import "AlbumView.h"
#import "Util.h"
#import "Macros.h"
#import "EventManager.h"
#import "SortManager.h"
#import "Logger.h"
#import "UIController.h"
#import "FriendsAlbumsView.h"
#import "SortingButtonPanel.h"
#import "UIView+Help.h"
#import <QuartzCore/QuartzCore.h>

#define PADDING_TOP 0
#define TITLE_CAROUSEL_GAP 5
#define TITLE_SECTION_HEIGHT 25
#define TITLE_SECTION_WIDTH 120

#define SORT_BUTTON_SIZE 38

#define SORT_BEGIN_DELAY_SECONDS 2.0



@interface OwnAlbumsCarouselView ()<SortingDelegate>

@property (strong) UIView *smartSortButton;
@property (retain) NSArray *albums;
@end

@implementation OwnAlbumsCarouselView;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupEvents];
        
        [self drawOnFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self loadDataShowData];
        
    }
    return self;
}

-(NSArray*)data {
    return self.albums;
}

-(void) drawOnFrame:(CGRect)frame  {
    
    float w = self.frame.size.width;
    float h = self.frame.size.height;
    
    CGRect carouselRect, titleRect;
    CGRectDivide(frame, &titleRect, &carouselRect, TITLE_SECTION_HEIGHT, CGRectMinYEdge);
    
    //titleRect.origin.x = titleRect.size.width - TITLE_SECTION_WIDTH;
    titleRect.size.width = TITLE_SECTION_WIDTH;
    
    carouselRect.origin.y += TITLE_CAROUSEL_GAP;
    
    
    
    UILabel *title = [[UILabel alloc] initWithFrame:titleRect];
    [title setTextAlignment:UITextAlignmentCenter];
    [title setTextColor:[UIColor whiteColor]];
    [title setText: @"Albums in this iPhone"];
    [title setFont: [UIFont fontWithName:@"MarkerFelt-Thin" size:13.0f]];
    [Util drawFastShadow:title];
    title.shadowColor = [UIColor blackColor];
    title.shadowOffset = CGSizeMake(0.0, -1.0);
    
    [title setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"own-album-view-title-background.png"]]];
    [self addSubview:title];
        
	self.carousel = [[Carousel alloc] initWithFrame:carouselRect:LOCAL:self];
	[self addSubview:self.carousel];
    self.carousel.clipsToBounds = NO;
    
    SortingButtonPanel *sortingButtonPanel = [SortingButtonPanel new];
    sortingButtonPanel.delegate = self;
    CGRect sortingButtonPanelFrame = sortingButtonPanel.bounds;
    sortingButtonPanelFrame.origin = CGPointMake(w - sortingButtonPanelFrame.size.width, 2);
    sortingButtonPanel.frame = sortingButtonPanelFrame;
    //[self addSubview:sortingButtonPanel];

}

-(void) setupEvents {
    [[EventManager getInstance] addEventListener:ALBUM_UPDATED :^(va_list args) {
        [self refresh];
    }];
    [[EventManager getInstance] addEventListener:PHOTO_LIBRARY_ORGANIZED :^(va_list args) {
        [self refresh];
    }];
    [[EventManager getInstance] addEventListener:ALBUM_OR_FRIEND_SELECTED_ON_SEARCH :^(va_list args) {
        NSManagedObject* data = va_arg(args, NSManagedObject*);
        if([data isKindOfClass:[Album class]]) {
            Album *album = (Album*)data;
            [self.carousel scrollToUserOrAlbum:album];
        }
    }];
}

-(void) loadData:(VoidCallback)callback :(GenericErrorback)errorback {
    [[PhotoLibraryOrganizer getInstance] getLocalAlbums:^(NSArray * loadedAlbums) {
        self.albums = loadedAlbums;
        if(callback) callback();
    }:errorback];
}

-(void) loadDataShowData {
    [self setLoading];
    [self loadData:^{
        [self reloadData];
    } :^(NSError *error) {
        [Logger logError:@"error loading Own Album View data" :error];
        [Util alertError:error:@"There was an error in loading local albums. Please try again later"];
    }];
}

-(void) setLoading {
    self.albums = nil;
    //[self reloadData];
}

-(void) refresh {
    [self loadDataShowData]; 
}

-(void)sortFor:(NSManagedObject*)data:(SortingType)sortType {
    //TODO: synchronization in case of multiple requests
    NSArray *previousOrder = self.albums;
    [[SortManager getInstance] sort:self.albums :data :sortType :NO:^(NSArray *sorted)
    {
        self.albums = sorted;
        
        BOOL reloadNeeded = NO;
        NSArray *visibleIndexes = self.carousel.indexesForVisibleItems;
        for(NSNumber *index in visibleIndexes) {
            NSUInteger i = [index intValue];
            if([previousOrder count] > i && [self.albums count] > i) {
                NSString *previousItemAtIndexID = [[previousOrder objectAtIndex:i] valueForKey:@"id"];
                NSString *currentItemAtIndexID = [[self.albums objectAtIndex:i] valueForKey:@"id"];
                if([previousItemAtIndexID isEqualToString:currentItemAtIndexID]) {
                    continue;
                }
            }
            
            reloadNeeded = YES;
            break;
        }
        
        if(reloadNeeded) {
            [self.carousel reloadData];
        }
        
        if([self.albums count] != 0) {
            [self.carousel scrollToUserOrAlbum:[self.albums objectAtIndex:0]];
        }
    }:^(NSError *error){
        [Logger logError:@"error in sorting" :error];
    }];
}

-(void) reloadData {
    [self.carousel reloadData];
}

-(Album*)getCurrentAlbum {
    if(!self.carousel || !self.albums) return nil;
    return self.albums[self.carousel.currentItemIndex];
}


-(void)onSmartSortClick {
    [self onSortClick:SMART];
}

-(void)onDateSortClick {
    [self onSortClick:DATE];
}

-(void)onAlphabeticalSortClick {
    [self onSortClick:ALPHABET];
}

-(void)onSortClick:(SortingType)type {
    FriendsAlbumsView *fav = [[UIController getInstance] getUIControl:FRIEND_ALBUM_CAROUSEL_VIEW];
    NSManagedObject *currentFriendOrAlbum = [fav getCurrentFriendOrAlbum];
    if(currentFriendOrAlbum) {
        [self sortFor:currentFriendOrAlbum:type];
    }
}

@end
