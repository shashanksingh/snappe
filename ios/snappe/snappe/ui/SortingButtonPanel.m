//
//  SortingButtonPanel.m
//  snappe
//
//  Created by Shashank on 27/01/13.
//
//

#import "SortingButtonPanel.h"
#import "SortManager.h"
#import "Util.h"
#import <QuartzCore/QuartzCore.h>

#define SORT_BUTTON_SIZE 36
#define SORT_BUTTON_GAP 3


@implementation SortingButtonPanel

- (id)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = UIColorFromRGBA(0x0, 0.45);
        [self.layer setBorderWidth:1.0];
        [self.layer setBorderColor:UIColorFromRGBA(0xFFFFFF, 0.1).CGColor];
        [Util drawFastShadow:self];
        [self.layer setShadowOffset:CGSizeMake(-2.0, 2.0)];
        [self.layer setShadowRadius:3.0];
        [self.layer setShadowOpacity:0.0];
        [self draw];
    }
    return self;
}

-(void)draw {
    NSArray *sortingMenuButtonProps = @[
    @[@"brain_icon.png", @"onSmartSortClick", [NSNumber numberWithInt:SMART]],
    @[@"clock_icon.png", @"onDateSortClick", [NSNumber numberWithInt:DATE]],
    @[@"alpha_icon.png", @"onAlphabeticalSortClick", [NSNumber numberWithInt:ALPHABET]]
    ];
    
    CGFloat w = 3 * SORT_BUTTON_SIZE + 2 * SORT_BUTTON_GAP;
    CGRect frame = self.frame;
    frame.size = CGSizeMake(w, SORT_BUTTON_SIZE);
    self.frame = frame;
    
    CGFloat x = w - SORT_BUTTON_SIZE;
    for(NSArray *sortingMenuButtonProp in sortingMenuButtonProps) {
        NSString *iconName = [sortingMenuButtonProp objectAtIndex:0];
        NSString *selectorName = [sortingMenuButtonProp objectAtIndex:1];
        
        CGRect frame = CGRectMake(x, 0, SORT_BUTTON_SIZE, SORT_BUTTON_SIZE);
        UIButton *sortingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [sortingButton setFrame:frame];
        [sortingButton setImage:[UIImage imageNamed:iconName] forState:UIControlStateNormal];
        [sortingButton addTarget:self action:NSSelectorFromString(selectorName) forControlEvents:UIControlEventTouchUpInside];
        [sortingButton setClipsToBounds:YES];
        
        [self addSubview:sortingButton];
        
        x -= SORT_BUTTON_SIZE + SORT_BUTTON_GAP;
    }
}

-(void)onSmartSortClick {
    if(![[FBManager getInstance] hasUserAuthorizedOnce]) {
        [Util alertError:nil :@"To use SmartSort please connect your Facebook account."];
        return;
    }
    
    if(self.delegate) {
        [self.delegate onSmartSortClick];
    }
}

-(void)onDateSortClick {
    if(self.delegate) {
        [self.delegate onDateSortClick];
    }
}

-(void)onAlphabeticalSortClick {
    if(self.delegate) {
        [self.delegate onAlphabeticalSortClick];
    }
}

@end
