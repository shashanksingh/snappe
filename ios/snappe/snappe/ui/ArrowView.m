//
//  ArrowView.m
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import "ArrowView.h"
#import "Util.h"

@interface ArrowView()
@property CGPoint start;
@property CGPoint end;
@end

@implementation ArrowView

-(id)init {
    CGRect frame = [UIScreen mainScreen].bounds;
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void)setRects:(CGRect)firstRect:(CGRect)secondRect {
    CGPoint firstPoints[] = {
        CGPointMake(CGRectGetMidX(firstRect), CGRectGetMinY(firstRect)),
        CGPointMake(CGRectGetMaxX(firstRect), CGRectGetMidY(firstRect)),
        CGPointMake(CGRectGetMidX(firstRect), CGRectGetMaxY(firstRect)),
        CGPointMake(CGRectGetMinX(firstRect), CGRectGetMidY(firstRect))
    };
    
    CGPoint secondPoints[] = {
        CGPointMake(CGRectGetMidX(secondRect), CGRectGetMinY(secondRect)),
        CGPointMake(CGRectGetMaxX(secondRect), CGRectGetMidY(secondRect)),
        CGPointMake(CGRectGetMidX(secondRect), CGRectGetMaxY(secondRect)),
        CGPointMake(CGRectGetMinX(secondRect), CGRectGetMidY(secondRect))
    };
    
    CGFloat minDistance = MAXFLOAT;
    CGPoint minDistanceEdgePair = CGPointMake(0, 0);
    for(int i=0; i<4; i++) {
        for(int j=0; j<4; j++) {
            CGPoint firstPoint = firstPoints[i];
            CGPoint secondPoint = secondPoints[j];
            
            CGFloat distance = [Util getDistanceBetweenTwoPoints:firstPoint :secondPoint];
            if(distance < minDistance) {
                minDistance = distance;
                minDistanceEdgePair = CGPointMake(i, j);
            }
        }
    }
    
    self.start = firstPoints[(int)minDistanceEdgePair.x];
    self.end = secondPoints[(int)minDistanceEdgePair.y];
}

-(void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextMoveToPoint(context, self.start.x, self.start.y);
    CGContextAddQuadCurveToPoint(context, self.start.x - 20, (self.start.y + self.end.y)/2, self.end.x, self.end.y);
    CGContextStrokePath(context);
}

@end
