//
//  ActionConfirmationView.m
//  snappe
//
//  Created by Shashank on 08/04/13.
//
//

#import "ActionConfirmationView.h"
#import "Util.h"
#import "SSCheckBoxView.h"
#import "BButton.h"
#import "Logger.h"
#import <QuartzCore/QuartzCore.h>

#define ALERT_ICON_SIZE 64
#define ALERT_VIEW_WIDTH 270
#define ALERT_VIEW_HEIGHT 180
#define MESSAGE_PADDING 10
#define BUTTON_SECTION_HEIGHT 40
#define CHECKBOX_SECTION_HEIGHT 40
#define CHECKBOX_SIZE 20
#define BUTTON_PADDING_X 10
#define CHECKBOX_PADDING_X 5
#define CHECKBOX_MESSAGE_PADDING_X 15

static char const * const PREF_KEY_PREFIX = "snappe_confirmation_key";

@interface ActionConfirmationView()
@property (retain) NSString *key;
@property (retain) NSString *message;
@property (copy) GenericCallback callback;
@property (strong) SSCheckBoxView *checkbox;
@end

@implementation ActionConfirmationView

- (id)init:(NSString*)key:(NSString*)message:(GenericCallback)callback {
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if (self) {
        self.key = key;
        self.message = message;
        self.callback = callback;
        
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75f]];
        [self drawOnFrame:frame];
    }
    return self;
}

-(void)drawOnFrame:(CGRect)rect {
    CGRect alertFrame = CGRectMake(0, 0, ALERT_VIEW_WIDTH, ALERT_VIEW_HEIGHT);
    alertFrame = [Util centerRect:alertFrame :[Util getRectCenter:rect]];
    
    UIView *alertView = [[UIView alloc] initWithFrame:alertFrame];
    [alertView setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 0.90)];
    [alertView.layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
    [alertView.layer setShadowRadius:3.0];
    [alertView.layer setShadowOpacity:1.0];
    [alertView.layer setBorderColor:UIColorFromRGBA(0xFFFFFF, 0.55).CGColor];
    [alertView.layer setBorderWidth:2];
    [Util drawFastShadow:alertView];
    [self addSubview:alertView];
    
    CGRect contentRect = CGRectInset(alertView.bounds, MESSAGE_PADDING, MESSAGE_PADDING);
    
    CGRect iconViewRect = CGRectMake(0, 0, ALERT_ICON_SIZE, ALERT_ICON_SIZE);
    iconViewRect = [Util centerRect:iconViewRect :[Util getRectCenter:contentRect]];
    
    UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error_alert_background_icon.png"]];
    [iconView setFrame:iconViewRect];
    [alertView addSubview:iconView];
    
    CGRect messageSectionRect, checkboxSectionRect, buttonSectionRect;
    
    CGRect r = contentRect;
    CGRectDivide(r, &buttonSectionRect, &r, BUTTON_SECTION_HEIGHT, CGRectMaxYEdge);
    CGRectDivide(r, &checkboxSectionRect, &messageSectionRect, CHECKBOX_SECTION_HEIGHT, CGRectMaxYEdge);
    
    checkboxSectionRect = CGRectInset(checkboxSectionRect, CHECKBOX_PADDING_X, 0);
    
    //hack to work with longer confirmation text
    //TODO: make it better
    CGFloat fontSize = self.message.length < 150 ? 12 : 11;
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:messageSectionRect];
    messageLabel.lineBreakMode = UILineBreakModeWordWrap;;
    messageLabel.numberOfLines = 0;
    messageLabel.adjustsFontSizeToFitWidth = YES;
    [messageLabel setBackgroundColor:[UIColor clearColor]];
    [messageLabel setFont:[UIFont fontWithName:@"NotoSerif" size:fontSize]];
    [messageLabel setTextColor:UIColorFromRGBA(0x572222, 1)];
    [messageLabel setTextAlignment:NSTextAlignmentCenter];
    [messageLabel setText:self.message];
    [alertView addSubview:messageLabel];
    
    CGRect checkboxRect, checkboxMessageRect;
    CGRectDivide(checkboxSectionRect, &checkboxRect, &checkboxMessageRect, CHECKBOX_SIZE, CGRectMinXEdge);
    
    checkboxMessageRect = CGRectInset(checkboxMessageRect, CHECKBOX_MESSAGE_PADDING_X, 0);
    
    self.checkbox = [[SSCheckBoxView alloc] initWithFrame:checkboxRect
                                                               style:kSSCheckBoxViewStyleGlossy
                                                             checked:NO];
    [alertView addSubview:self.checkbox];
    
    UILabel *checkboxMessageLabel = [[UILabel alloc] initWithFrame:checkboxMessageRect];
    [checkboxMessageLabel setBackgroundColor:[UIColor clearColor]];
    [checkboxMessageLabel setFont:[UIFont fontWithName:@"FontAwesome" size:13]];
    [checkboxMessageLabel setTextColor:UIColorFromRGBA(0x8C6B6B, 1)];
    [checkboxMessageLabel setTextAlignment:NSTextAlignmentLeft];
    [checkboxMessageLabel setText:@"Don't ask again"];
    [alertView addSubview:checkboxMessageLabel];
    
    CGRect cancelButtonRect, okButtonRect;
    CGRectDivide(buttonSectionRect, &cancelButtonRect, &okButtonRect, buttonSectionRect.size.width * 0.5, CGRectMinXEdge);
    
    okButtonRect = CGRectInset(okButtonRect, BUTTON_PADDING_X, 0);
    cancelButtonRect = CGRectInset(cancelButtonRect, BUTTON_PADDING_X + 5, 5);
    
    BButton *okButton = [[BButton alloc] initWithFrame:okButtonRect type:BButtonTypeSuccess];
    [okButton setTitle:@"OK" forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(onOKButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [alertView addSubview:okButton];
    
    
    BButton *cancelButton = [[BButton alloc] initWithFrame:cancelButtonRect type:BButtonTypeGray icon:nil fontSize:12];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(onCancelButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [alertView addSubview:cancelButton];
}

-(void)onOKButtonClick:(UIButton*)button {
    if(self.checkbox.checked) {
        [self.class saveDecision:self.key :YES];
    }
    self.callback(@YES);
    
    [Logger track:@"action_confirmation_response" :@{@"response": @"ok", @"key":self.key}];
    [self removeFromSuperview];
}

-(void)onCancelButtonClick:(UIButton*)button {
    if(self.checkbox.checked) {
        [self.class saveDecision:self.key :NO];
    }
    self.callback(@NO);
    
    [Logger track:@"action_confirmation_response" :@{@"response": @"cancel", @"key":self.key}];
    
    [self removeFromSuperview];
}


+(void)confirm:(NSString*)key:(NSString*)message:(GenericCallback)callback {
    
    NSNumber *savedDecision = [self getSavedDecision:key];
    //saved decision currently works only for YES, otherwise the user will have
    //no way of re-setting it
    if(savedDecision && savedDecision.boolValue) {
        callback(savedDecision);
        [Logger track:@"action_confirmation_show" :@{@"key":key, @"saved" : @(YES)}];
        return;
    }

    ActionConfirmationView *view = [[ActionConfirmationView alloc] init:key :message :callback];
    [Util showModalView:view];
    
    [Logger track:@"action_confirmation_show" :@{@"key":key, @"saved" : @(NO)}];
}

+(NSNumber*)getSavedDecision:(NSString*)key {
    NSString *prefKey = [self getPrefKeyForKey:key];
    return [Util getUserDefault:prefKey];
}

+(void)saveDecision:(NSString*)key:(BOOL)defaultDecision {
    [Util setUserDefault:@(defaultDecision) forKey:[self.class getPrefKeyForKey:key]];
}

+(NSString*)getPrefKeyForKey:(NSString*)key {
    return [NSString stringWithFormat:@"%s-%@", PREF_KEY_PREFIX, key];
}


@end
