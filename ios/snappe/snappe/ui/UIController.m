//
//  UICoreController.m
//  snappe
//
//  Created by Shashank on 20/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIController.h"

#define DEFAULT_UIVIEW_CACHE_LIMIT 40
#define CACHE_LIMIT_ANTI_THRASHING_THRESHOLD 10

@interface UIController()
@property (strong) NSMutableDictionary *controlPool;
@property (strong) NSMutableDictionary *uiCache;
@property (strong) NSMutableDictionary *uiCacheLimits;
@end

@implementation UIController

-(id)init {
    self = [super init];
    if(self) {
        self.controlPool = [NSMutableDictionary dictionary];
        self.uiCache = [NSMutableDictionary dictionary];
    }
    return self;
}

+(UIController *) getInstance {
    static UIController *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[UIController alloc] init];
    });
    return instance;
}

-(id)getUIControl:(UIControlID)type {
    NSNumber *key = [NSNumber numberWithShort:type];
    return [self.controlPool objectForKey:key];
}

-(void)setUIControl:(id)control :(UIControlID)type {
    NSNumber *key = [NSNumber numberWithShort:type];
    [self.controlPool setObject:control forKey:key];
}

-(void)setCacheLimit:(CachedUIViewType)type:(NSUInteger)limit {
    [self.uiCacheLimits setObject:@(limit) forKey:@(type)];
}

-(NSMutableArray*)cacheForType:(CachedUIViewType)type {
    NSMutableArray *cacheForType = [self.uiCache objectForKey:@(type)];
    if(!cacheForType) {
        cacheForType = [NSMutableArray array];
        [self.uiCache setObject:cacheForType forKey:@(type)];
    }
    return cacheForType;
}

-(void)queueCachedUIView:(UIView*)view:(CachedUIViewType)type {
    NSMutableArray *cacheForType = [self cacheForType:type];
    [cacheForType addObject:view];
    [self trimUIViewCache:type:cacheForType];
    
}

-(void)trimUIViewCache:(CachedUIViewType)type:(NSMutableArray*)cache {
    NSNumber *cacheLimit = [self.uiCacheLimits objectForKey:@(type)];
    if(!cacheLimit) {
        cacheLimit = @(DEFAULT_UIVIEW_CACHE_LIMIT);
    }
    
    NSUInteger limit = [cacheLimit intValue];
    int extra = [cache count] - limit;
    if(extra > 0) {
        int countToRemove = MIN([cache count], extra + CACHE_LIMIT_ANTI_THRASHING_THRESHOLD);
        [cache removeObjectsInRange:NSMakeRange(0, countToRemove)];
    }
}

-(UIView*)popCachedUIView:(CachedUIViewType)type {
    NSMutableArray *cacheForType = [self cacheForType:type];
    if(!cacheForType || [cacheForType count] == 0) {
        return nil;
    }
    
    UIView *rv = [cacheForType objectAtIndex:0];
    [cacheForType removeObjectAtIndex:0];
    return rv;
}



@end
