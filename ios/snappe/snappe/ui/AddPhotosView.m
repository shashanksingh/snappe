//
//  AddPhotosView.m
//  snappe
//
//  Created by Shashank on 07/04/13.
//
//

#import "AddPhotosView.h"
#import "AlbumAccessor.h"
#import "Util.h"
#import "PhotoPickerView.h"
#import "PhotoAddOperation.h"
#import "ActionConfirmationView.h"
#import "BButton.h"
#import "Logger.h"
#import <QuartzCore/QuartzCore.h>

#define HEIGHT 360
#define WIDTH 280
#define HEADER_HEIGHT 30
#define BUTTON_SECTION_HEIGHT 60

#define HEADER_PADDING 5

#define BUTTON_WIDTH 120
#define BUTTON_HEIGHT 40


@interface AddPhotosView()
@property (strong) Album *album;
@property (strong) PhotoPickerView *photoPicker;
@property (strong) UIView *contentView;
@end

@implementation AddPhotosView

- (id)init {
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if(self) {
        
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75f]];
        
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        tapRecognizer.cancelsTouchesInView = NO;
        [self addGestureRecognizer:tapRecognizer];
        
        [self drawOnRect:frame];
    }
    return  self;
}

+(AddPhotosView*) getInstance {
    static AddPhotosView *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AddPhotosView alloc] init];
    });
    return sharedInstance;
}


+(void)show:(Album*)album {
    AddPhotosView *view = [self getInstance];
    view.album = album;
    [view.photoPicker populate];
    [Util showModalView:view];
    [Logger track:@"add_photos_view_shown" :@{@"aid" : view.album.id}];
}

-(void)drawOnRect:(CGRect)frame {
    CGRect mainFrame = CGRectMake(0, 0, WIDTH, HEIGHT);
    mainFrame = [Util centerRect:mainFrame :[Util getRectCenter:frame]];
    
    self.contentView = [[UIView alloc] initWithFrame:mainFrame];
    [self addSubview:self.contentView];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    [self.contentView.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [Util drawFastShadow:self.contentView];
    [self.contentView.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [self.contentView.layer setShadowRadius:3.0];
    [self.contentView.layer setShadowOpacity:1.0];
    
    CGRect headerBox, photoGridBox, doButtonBox;
    CGRect r = self.contentView.bounds;
    CGRectDivide(r, &headerBox, &r, HEADER_HEIGHT, CGRectMinYEdge);
    CGRectDivide(r, &doButtonBox, &photoGridBox, BUTTON_SECTION_HEIGHT, CGRectMaxYEdge);
    
    
    UIView *headerView = [[UIView alloc] initWithFrame:headerBox];
    [self.contentView addSubview:headerView];
    [headerView setBackgroundColor:UIColorFromRGBA(0x343434, 0.9)];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectInset(headerView.bounds, HEADER_PADDING, HEADER_PADDING)];
    headerLabel.backgroundColor = [UIColor clearColor];
    [headerView addSubview:headerLabel];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    [headerLabel setTextColor:UIColorFromRGBA(0xFFFFFF, 0.9)];
    [headerLabel setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:13]];
    [headerLabel setTextAlignment:NSTextAlignmentCenter];
    [headerLabel setText:@"Select Photos To Add"];
    
    UIView *photoGridScrollerContainer = [[UIView alloc] initWithFrame:photoGridBox];
    [self.contentView addSubview:photoGridScrollerContainer];
    [photoGridScrollerContainer setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 0.95)];
    [photoGridScrollerContainer.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [photoGridScrollerContainer.layer setBorderWidth:2];
    
    CGRect photoGridRect = photoGridScrollerContainer.bounds;
    self.photoPicker = [[PhotoPickerView alloc] initWithFrame:photoGridRect:SELECT];
    [photoGridScrollerContainer addSubview:self.photoPicker];
    
    CGRect buttonRect = CGRectMake(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
    buttonRect = [Util centerRect:buttonRect :[Util getRectCenter:doButtonBox]];
    
    BButton *doButton = [[BButton alloc] initWithFrame:buttonRect type:BButtonTypeSuccess];
    [doButton setTitle:@"Add" forState:UIControlStateNormal];
    [doButton addTarget:self action:@selector(onDoButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:doButton];
}

-(void)onTap:(UIGestureRecognizer*)gestureRecognizer {
    if(!CGRectContainsPoint(self.contentView.frame, [gestureRecognizer locationInView:self])) {
        [self removeFromSuperview];
        [Logger track:@"add_photos_view_hidden_on_tap" :@{@"aid" : self.album.id}];
    }
}

-(void)onDoButtonClick {
    [self removeFromSuperview];
    
    NSArray *selectedPhotos = [self.photoPicker getSelectedPhotos];
    [Logger track:@"add_photos_view_do_button_clicked" :@{@"aid" : self.album.id, @"selected_photos_count": @(selectedPhotos.count)}];
    
    if(selectedPhotos.count == 0) {
        return;
    }
    
    NSString *message = [NSString stringWithFormat:@"Are you sure you want to add %d photos to \"%@\"", selectedPhotos.count, self.album.name];
    
    Album *album = self.album;
    [ActionConfirmationView confirm:@"add_photos_to_album":message:^(NSNumber *ok) {
        if(!ok.boolValue) {
            return;
        }
        
        PhotoAddOperation *op = [[PhotoAddOperation alloc] init:album :selectedPhotos];
        [Util executeExtOp:op];
    }];
    
    
}

@end
