//
//  AppstoreReviewView.m
//  snappe
//
//  Created by Shashank on 24/02/13.
//
//

#import "AppstoreReviewView.h"
#import "Macros.h"
#import "Util.h"
#import "CoreController.h"
#import "Logger.h"
#import "BButton.h"
#import <QuartzCore/QuartzCore.h>

#define WIDTH 280
#define MESSAGE_SECTION_HEIGHT 150
#define BUTTON_SECTION_HEIGHT 60
#define BUTTON_WIDTH 210
#define BUTTON_HEIGHT 50
#define BOTTOM_PADDING 40

#define MESSAGE_PADDING 20

@interface AppstoreReviewView()<UIGestureRecognizerDelegate>
@end

@implementation AppstoreReviewView

- (id)init {
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if(self) {
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75f]];
        [self drawOnRect:frame];
        //[Util showPopup:nil :self :YES :nil];
    }
    return  self;
}

+(void)showIfNeeded {
    if(![[CoreController getInstance] shouldAskForAppstoreReview]) {
        return;
    }
    
    [[CoreController getInstance] saveLastAppstoreReviewPromptTime];
    
    AppstoreReviewView *view = [[AppstoreReviewView alloc] init];
    [Util showModalView:view];
    
    [Logger track:@"app_review_prompt" :@{@"act": @"show"}];
}


-(void)drawOnRect:(CGRect)frame {
    CGFloat height = MESSAGE_SECTION_HEIGHT + 3 * BUTTON_SECTION_HEIGHT + BOTTOM_PADDING;
    
    CGRect mainFrame = CGRectMake(0, 0, WIDTH, height);
    mainFrame = [Util centerRect:mainFrame :[Util getRectCenter:frame]];
    
    UIView *mainBox = [[UIView alloc] initWithFrame:mainFrame];
    mainBox.backgroundColor = [UIColor whiteColor];
    [self addSubview:mainBox];
    
    CGRect messageBox, yesButtonBox, noButtonBox, postponeButtonBox;
    CGRect r = mainBox.bounds;
    CGRectDivide(r, &messageBox, &r, MESSAGE_SECTION_HEIGHT, CGRectMinYEdge);
    CGRectDivide(r, &yesButtonBox, &r, BUTTON_SECTION_HEIGHT, CGRectMinYEdge);
    CGRectDivide(r, &noButtonBox, &r, BUTTON_SECTION_HEIGHT, CGRectMinYEdge);
    CGRectDivide(r, &postponeButtonBox, &r, BUTTON_SECTION_HEIGHT, CGRectMinYEdge);
    
    messageBox = CGRectInset(messageBox, MESSAGE_PADDING, MESSAGE_PADDING);
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:messageBox];
    messageLabel.lineBreakMode = UILineBreakModeWordWrap;
    messageLabel.numberOfLines = 0;
    [messageLabel setBackgroundColor:[UIColor clearColor]];
    [messageLabel setFont:[UIFont fontWithName:@"Sansation_Regular" size:16]];
    [messageLabel setTextColor:UIColorFromRGBA(0x7C3030, 1)];
    [messageLabel setTextAlignment:NSTextAlignmentCenter];
    messageLabel.text = @"If you enjoy using Snappe, would you mind taking a moment to rate it? It won't take more than a minute. Thanks for your support!";
    [mainBox addSubview:messageLabel];
    
    NSArray *buttonDescriptors = @[
        @[@"Rate Snappe", @"onYes", @(BButtonTypeSuccess), [NSValue valueWithCGRect:yesButtonBox]],
        @[@"No, Thanks", @"onNo", @(BButtonTypeDefault), [NSValue valueWithCGRect:noButtonBox]],
        @[@"Reminder me later", @"onPostpone", @(BButtonTypeGray), [NSValue valueWithCGRect:postponeButtonBox]]
    ];
    
    for(NSArray *descriptor in buttonDescriptors) {
        NSString *label = descriptor[0];
        NSString *selector = descriptor[1];
        BButtonType buttonType = ((NSNumber*)descriptor[2]).intValue;
        CGRect box = [descriptor[3] CGRectValue];
        
        CGRect buttonRect = CGRectMake(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
        buttonRect = [Util centerRect:buttonRect :[Util getRectCenter:box]];
        
        UIButton *button = [[BButton alloc] initWithFrame:buttonRect type:buttonType];
        [button setTitle:label forState:UIControlStateNormal];
        [button addTarget:self action:NSSelectorFromString(selector) forControlEvents:UIControlEventTouchUpInside];
        [mainBox addSubview:button];
    }
}

-(void)onYes {
    [[CoreController getInstance] saveAppstoreReviewPromptResponse:REVIEWED];
    NSString *url = [NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", @"601707881"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    [self removeFromSuperview];
    [Logger track:@"app_review_prompt" :@{@"act": @"yes"}];
}

-(void)onNo {
    [[CoreController getInstance] saveAppstoreReviewPromptResponse:DENIED];
    [self removeFromSuperview];
    [Logger track:@"app_review_prompt" :@{@"act": @"no"}];
}

-(void)onPostpone {
    [[CoreController getInstance] saveAppstoreReviewPromptResponse:POSTPONED];
    [self removeFromSuperview];
    [Logger track:@"app_review_prompt" :@{@"act": @"postpone"}];
}
@end
