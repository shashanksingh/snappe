//
//  LoadingIndicator.m
//  snappe
//
//  Created by Shashank on 23/01/13.
//
//

#import "LoadingIndicator.h"

#define GIF_WIDTH 18
#define GIF_HEIGHT 18

@interface LoadingIndicator()
@property (strong) UIImageView *loadingGIFView;
@end

@implementation LoadingIndicator

- (id)init:(BOOL)darkBackground
{
    self = [super init];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        
        self.darkBackground = darkBackground;
        
        CGRect loadingGIFFrame = CGRectMake(0, 0, GIF_WIDTH, GIF_HEIGHT);
        
        self.loadingGIFView = [[UIImageView alloc] initWithFrame:loadingGIFFrame];
        [self addSubview:self.loadingGIFView];
        
        NSMutableArray *frameImages = [[NSMutableArray alloc] init];
        for(int i=0;; ++i) {
            NSString *imageName = [NSString stringWithFormat:@"spinner_%@_%d.gif", self.darkBackground ? @"light" : @"dark", i];
            UIImage *frame = [UIImage imageNamed:imageName];
            if(!frame) {
                break;
            }
            [frameImages addObject:frame];
        }
        self.loadingGIFView.animationImages = frameImages;
        self.loadingGIFView.animationDuration = 1.0f;
        self.loadingGIFView.animationRepeatCount = 0;
        [self.loadingGIFView startAnimating];
    }
    return self;
}

-(void)layoutSubviews {
    self.loadingGIFView.center = self.center;
}

@end
