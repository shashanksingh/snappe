//
//  CatchTheDot.m
//  snappe
//
//  Created by Shashank on 11/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CatchTheDot.h"
#import "Util.h"
#import "AdProvider.h"
#import "CreditsManager.h"
#import "CoreController.h"
#import "UIView+Help.h"
#import "Logger.h"
#import <QuartzCore/QuartzCore.h>

#define GAME_LEVEL_PREF_KEY @"snappe_catch_the_dot_level"
#define STATUS_BAR_HEIGHT 50
#define GAME_LEVEL_LABEL_SECTION_WIDTH 80
#define GAME_LEVEL_NAME_LABEL_WIDTH 44
#define MAX_DOT_RADIUS 20
#define MIN_DOT_RADIUS 3
#define FRAME_INTERVAL 10
#define DIRECTION_RESET_MIN_FRAMES 1
#define DIRECTION_RESET_MAX_FRAMES 10
#define GAME_NAME_LABEL_PADDING 8
#define GAME_LEVEL_NAME_LABEL_PADDING 8
#define DOT_SHADOW_MAX_RADIUS 2.0

#define MAX_GAME_LEVELS 50
#define SHOW_AD_EVERY 3
#define SHOW_AD_MIN_LEVEL 5
#define AD_DURATION 12

@interface CatchTheDot()
@property (strong) UIView *animationView;
@property (strong) UIView *adView;
@property (strong) UILabel *gameLevelLabel;
@property (strong) CALayer *dotLayer;
@property (strong) CADisplayLink *displayLink;
@property CFTimeInterval lastDrawTime;
@property NSInteger gameLevel;
@property CGPoint dotCenter;
@property NSInteger dotDirection;
@property NSUInteger directionResetAfterFrames;
@property NSUInteger updateNumber;
@property BOOL changingLevels;
@end

@implementation CatchTheDot
@synthesize animationView;
@synthesize gameLevelLabel;
@synthesize dotLayer;
@synthesize displayLink;
@synthesize gameLevel;
@synthesize dotCenter;
@synthesize dotDirection;
@synthesize directionResetAfterFrames;
@synthesize updateNumber;
@synthesize changingLevels;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.changingLevels = NO;
        
        self.gameLevel = 0;
        id gameLevelState = [Util getUserDefault:GAME_LEVEL_PREF_KEY];
        if(gameLevelState) {
            self.gameLevel = [gameLevelState intValue];
        }
        
        self.updateNumber = 0;
        self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(setNeedsDisplay)];
        self.lastDrawTime = 0;
        [self.displayLink setFrameInterval:FRAME_INTERVAL];
            
        
        [self drawOnFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        [self addGestureRecognizer:tapRecognizer];
        
        [self.displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        
        [self drawRect:self.frame];
        
        self.adView = [[UIView alloc] initWithFrame:self.animationView.frame];
        [self addSubview:self.adView];
        [self sendSubviewToBack:self.adView];
        
        [self setHelpText:@"play \u2018catch-the-dot\u2019 while Snappe processes your request!"];
    }
    return self;
}

-(void)dealloc {
    if(self.displayLink) {
        [self.displayLink invalidate];
    }
}

- (void)drawRect:(CGRect)rect {
    
    CGFloat dotRadius = self.dotLayer.bounds.size.width/2;
    
    if(self.updateNumber == 0) {
        self.dotDirection = arc4random_uniform(361);
        self.directionResetAfterFrames = DIRECTION_RESET_MIN_FRAMES + (arc4random_uniform(DIRECTION_RESET_MAX_FRAMES - DIRECTION_RESET_MIN_FRAMES + 1));
    }
    
    CGFloat newRadius = [self getDotRadius];
    //DLog(@"dot radius: %f", newRadius);
    //delta = random(r/2, 3r/2)
    CGFloat deltaStep = (newRadius * 0.5) + arc4random_uniform((20.0/MAX_GAME_LEVELS) * pow(self.gameLevel, 3) * newRadius);
    
    CFTimeInterval timeSinceLast = self.lastDrawTime == 0 ? 1.0 : self.displayLink.timestamp - self.lastDrawTime;
    CGFloat millisSinceLast = timeSinceLast;///pow(10, 6);
    CGFloat delta = millisSinceLast * deltaStep;
    
    self.lastDrawTime = self.displayLink.timestamp;
    
    //DLog(@"delta: %f, %f", delta, deltaStep);
    
    CGFloat deltaX = delta * cos(self.dotDirection * M_PI/180);
    CGFloat deltaY = delta * sin(self.dotDirection * M_PI/180);
    
    CGFloat newX = self.dotLayer.position.x + deltaX;
    CGFloat newY = self.dotLayer.position.y + deltaY;
    
    
    BOOL atBoundary = NO;
    
    if(newX < dotRadius) {
        newX = dotRadius;
        self.dotDirection = arc4random_uniform(361);
        atBoundary = YES;
    }
    else if(newX > self.animationView.bounds.size.width - dotRadius) {
        newX = self.animationView.bounds.size.width - dotRadius;
        self.dotDirection = arc4random_uniform(361);
        atBoundary = YES;
    }
    
    if(newY < dotRadius) {
        newY = dotRadius;
        self.dotDirection = arc4random_uniform(361);
        atBoundary = YES;
    }
    else if(newY > self.animationView.bounds.size.height - dotRadius) {
        newY = self.animationView.bounds.size.height - dotRadius;
        self.dotDirection = arc4random_uniform(361);
        atBoundary = YES;
    }
    
    self.dotLayer.position = CGPointMake(newX, newY);
    
    if(atBoundary) {
        self.updateNumber = 0;
    }
    else {
        self.updateNumber = (self.updateNumber + 1)%self.directionResetAfterFrames;
    }
    
}


-(void)drawOnFrame:(CGRect)frame {
    CGRect statusBarRect, animationRect;
    CGRectDivide(frame, &statusBarRect, &animationRect, STATUS_BAR_HEIGHT, CGRectMinYEdge);
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:statusBarRect];
    [self addSubview:statusBarView];
    [statusBarView setBackgroundColor:UIColorFromRGBA(0x343434, 0.9)];
    
    CGRect gameNameLabelRect, gameLevelLabelRect;
    CGRectDivide(statusBarRect, &gameLevelLabelRect, &gameNameLabelRect, GAME_LEVEL_LABEL_SECTION_WIDTH, CGRectMaxXEdge);
    
    gameNameLabelRect = CGRectInset(gameNameLabelRect, GAME_NAME_LABEL_PADDING, GAME_NAME_LABEL_PADDING);
    UILabel *gameNameLabel = [[UILabel alloc] initWithFrame:gameNameLabelRect];
    [statusBarView addSubview:gameNameLabel];
    [gameNameLabel setBackgroundColor:[UIColor clearColor]];
    [gameNameLabel setTextAlignment:UITextAlignmentLeft];
    [gameNameLabel setTextColor:UIColorFromRGBA(0x9B6A52, 1)];
    [gameNameLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:14]];
    [gameNameLabel setText:@"Catch the Dot!"];
    
    CGRect levelNameLabelRect, levelValueLabelRect;
    CGRectDivide(gameLevelLabelRect, &levelNameLabelRect, &levelValueLabelRect, GAME_LEVEL_NAME_LABEL_WIDTH, CGRectMinXEdge);
    
    levelNameLabelRect = CGRectInset(levelNameLabelRect, GAME_LEVEL_NAME_LABEL_PADDING, GAME_LEVEL_NAME_LABEL_PADDING);
    UILabel *levelNameLabel = [[UILabel alloc] initWithFrame:levelNameLabelRect];
    [statusBarView addSubview:levelNameLabel];
    [levelNameLabel setTextAlignment:UITextAlignmentRight];
    [levelNameLabel setBackgroundColor:[UIColor clearColor]];
    [levelNameLabel setTextColor:UIColorFromRGBA(0xFFFFFF, 1)];
    [levelNameLabel setFont:[UIFont fontWithName:@"ArialMT" size:10]];
    [levelNameLabel setText:@"Level:"];
    
    self.gameLevelLabel = [[UILabel alloc] initWithFrame:levelValueLabelRect];
    [statusBarView addSubview:self.gameLevelLabel];
    [self.gameLevelLabel setTextAlignment:UITextAlignmentLeft];
    [self.gameLevelLabel setBackgroundColor:[UIColor clearColor]];
    [self.gameLevelLabel setTextColor:UIColorFromRGBA(0xFFFFFF, 1)];
    [self.gameLevelLabel setFont:[UIFont fontWithName:@"ArialMT" size:20]];
    [self.gameLevelLabel setText:[NSString stringWithFormat:@"%d", self.gameLevel]];
    
    self.animationView = [[UIView alloc] initWithFrame:animationRect];
    [self addSubview:animationView];
    
    self.dotLayer = [CALayer layer];
    [self.dotLayer setFrame:[self getDotRect]];
    [self.dotLayer setCornerRadius:self.dotLayer.bounds.size.width/2];
    self.dotLayer.backgroundColor = UIColorFromRGBA(0xFF0000, 0.9).CGColor;
    self.dotLayer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.dotLayer.bounds cornerRadius:self.dotLayer.cornerRadius].CGPath;
    [self.dotLayer setShadowColor:[[UIColor blackColor ] CGColor ] ];
    [self.dotLayer setShadowOpacity:0.65];
    [self.dotLayer setShadowRadius:DOT_SHADOW_MAX_RADIUS/[self getDotRadius]];
    [self.dotLayer setShadowOffset:CGSizeMake(0, 0)];
    [self.animationView.layer addSublayer:self.dotLayer];
}

-(void) setRandomDotPosition {
    CGFloat dotRadius = [self getDotRadius];
    CGFloat x = dotRadius + (arc4random_uniform(self.animationView.bounds.size.width - 2 * dotRadius + 1));
    CGFloat y = dotRadius + (arc4random_uniform(self.animationView.bounds.size.height - 2 * dotRadius + 1));
    
    self.dotLayer.position = CGPointMake(x, y);
    self.dotCenter = CGPointMake(x, y);
}

-(CGRect)getDotRect {
    CGFloat dotRadius = [self getDotRadius];
    if(CGPointEqualToPoint(self.dotCenter, CGPointZero)) {
        [self setRandomDotPosition];
    }
    
    return CGRectMake(0, 0, dotRadius * 2, dotRadius * 2);
}

-(CGFloat)getDotRadius {
    //evenly distribute between game levels
    return MAX_DOT_RADIUS - (((MAX_DOT_RADIUS - MIN_DOT_RADIUS) * 1.0/MAX_GAME_LEVELS)*self.gameLevel);
}

-(void)fadeDot:(BOOL)fadeIn {
    CABasicAnimation *fadeOutAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeOutAnimation.fromValue = [NSNumber numberWithFloat:self.dotLayer.opacity];
    fadeOutAnimation.toValue = [NSNumber numberWithFloat:fadeIn ? 1.0 : 0.0];
    fadeOutAnimation.duration = 1.0;
    self.dotLayer.opacity = fadeIn ? 1.0 : 0.0;
    [self.dotLayer addAnimation:fadeOutAnimation forKey:@"opacity"];
}

-(void)incrementGameLevel {
    
    self.gameLevel = (self.gameLevel + 1)%MAX_GAME_LEVELS;
    [Util setUserDefault:[NSNumber numberWithInt:self.gameLevel] forKey:GAME_LEVEL_PREF_KEY];
    
    DLog(@"new level: %@", [Util getUserDefault:GAME_LEVEL_PREF_KEY]);
    
    [Logger track:@"game_level_up" :@{@"level" : @(self.gameLevel)}];
    
    [self.gameLevelLabel setText:[NSString stringWithFormat:@"%d", self.gameLevel]];
    [self.displayLink setPaused:YES];
    
    [self showAd:^{
        [self fadeDot:NO];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            [self setRandomDotPosition];
            [self.dotLayer setFrame:[self getDotRect]];
            [self.dotLayer setCornerRadius:self.dotLayer.bounds.size.width/2];
            [self.dotLayer setShadowRadius:DOT_SHADOW_MAX_RADIUS/[self getDotRadius]];
            
            [self fadeDot:YES];
            [self.displayLink setPaused:NO];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
                self.changingLevels = NO;
            });
            
        });
    }];
}

-(void)showAd:(VoidCallback)callback {
    if(self.gameLevel == 0 || self.gameLevel < SHOW_AD_MIN_LEVEL || self.gameLevel % SHOW_AD_EVERY != 0 || ![[CoreController getInstance] canShowAds]) {
        callback();
        return;
    }
    
    UIView *ad = [AdProvider getCustomAdView:self.adView.bounds.size];
    ad.frame = self.adView.bounds;
    [self.adView addSubview:ad];
    
    [UIView transitionFromView:self.animationView toView:self.adView duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve completion:^(BOOL finished) {
    }];
    
    CALayer *timerLayer = [self drawAdTimer];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(AD_DURATION * NSEC_PER_SEC)),dispatch_get_main_queue(), ^(void)
    {
        [timerLayer removeFromSuperlayer];
        [ad removeFromSuperview];
        [self showGame:callback];
    });
}

-(void)showGame:(VoidCallback)callback {
    [UIView transitionFromView:self.adView toView:self.animationView duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve completion:^(BOOL finished) {
        callback();
    }];
}

-(CALayer*) drawAdTimer {
    int radius = 10;
    CAShapeLayer *circle = [CAShapeLayer layer];
    circle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, radius, radius)
                                             cornerRadius:radius].CGPath;
    // Center the shape in self.view
    circle.position = CGPointMake(CGRectGetMaxX(self.adView.bounds) - 3 * radius,
                                  CGRectGetMaxY(self.adView.bounds) - 3 * radius);
    
    // Configure the apperence of the circle
    circle.fillColor = [UIColor blackColor].CGColor;
    circle.strokeColor = [UIColor whiteColor].CGColor;
    circle.lineWidth = radius;
    
    // Add to parent layer
    [self.adView.layer addSublayer:circle];
    
    // Configure animation
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration            = AD_DURATION;
    drawAnimation.repeatCount         = 1.0;  // Animate only once..
    drawAnimation.removedOnCompletion = YES;   // Remain stroked after the animation..
    
    // Animate from no part of the stroke being drawn to the entire stroke being drawn
    drawAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    drawAnimation.toValue   = [NSNumber numberWithFloat:1.0f];
    
    // Experiment with timing to get the appearence to look the way you want
    drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    // Add the animation to the circle
    [circle addAnimation:drawAnimation forKey:@"drawCircleAnimation"];
    
    return circle;
    
}

-(void)onCatch {
    if(self.changingLevels) {
        return;
    }
    
    self.changingLevels = YES;
    [self incrementGameLevel];
}

-(void)onTap:(UITapGestureRecognizer*)recognizer {
    if(recognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint tapLocation = [recognizer locationInView:self.animationView];
        CGRect dotRect = [self.dotLayer.presentationLayer frame];
        if(CGRectContainsPoint(dotRect, tapLocation)) {
            [self onCatch];    
        }
    }
}

@end
