//
//  OwnAlbumsCarouselViewController.h
//  snappe
//
//  Created by Shashank on 13/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Album.h"
#import "Carousel.h"

@interface OwnAlbumsCarouselView : UIView<CarouselDataSource>
@property (strong) Carousel *carousel;
-(Album *) getCurrentAlbum;
@end
