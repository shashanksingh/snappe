//
//  SearchResultViewCell.m
//  snappe
//
//  Created by Shashank on 18/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchResultViewCell.h"
#import "AlbumAccessor.h"
#import "UserAccessor.h"
#import "Contact.h"
#import "Logger.h"
#import "Util.h"
#import "AlbumShareOperation.h"
#import "UIView+Glow.h"
#import "RTLabel.h"
#import "UIView+Help.h"
#import "EmailShareIntroOverlay.h"
#import <QuartzCore/QuartzCore.h>

#define CELL_X_PADDING 10
#define CELL_Y_PADDING 10
#define THUMB_WIDTH 44
#define THUMB_HEIGHT 44
#define THUMB_PADDING 1
#define TEXT_X_PADDING 5
#define TEXT_Y_PADDING 2
#define SHARE_BUTTON_SIZE 38
#define SHARE_BUTTON_PADDING 3

#define REPLACEMENT_THUMB_TAG 1>>9

@interface SearchResultCellView : UIView
@property (retain) NSMutableDictionary *context;
@property SearchType searchType;
@property (strong) UIImageView *thumbView;
@property (strong) RTLabel *textLabel;
@property (strong) UILabel *descriptionLabel;
@property (strong) UIButton *shareAlbumButton;
@property (strong) UIButton *allowUploadButton;
-(void)update:(SearchType)searchType:(NSMutableDictionary*)context;
@end

@implementation SearchResultCellView
@synthesize context;
@synthesize searchType;
@synthesize thumbView;
@synthesize textLabel;
@synthesize descriptionLabel;
@synthesize shareAlbumButton;
@synthesize allowUploadButton;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self drawOnFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    }
    return self;
}

-(void)update:(SearchType)searchType_:(NSMutableDictionary*)context_{
    SearchType oldSearchType = self.searchType;
    self.context = context_;
    self.searchType = searchType_;
    
    //we need to redraw, share button might need to be hidden/shown
    if(self.searchType != oldSearchType) {
        [self drawOnFrame:self.bounds];
    }
    
    CGRect thumbFrame = self.thumbView.frame;
    
    UIView *oldThumbView = self.thumbView;
    
    id subject = [self.context objectForKey:@"subject"];
    NSString *searchQuery = [self.context objectForKey:@"query"];
    
    if([subject isKindOfClass:CurrentSearchQueryAsResult.class]) {
        CurrentSearchQueryAsResult *result = (CurrentSearchQueryAsResult*)subject;
        result.value = searchQuery;
        
        if(self.searchType == ALBUM || self.searchType == FB_ALBUM) {
            self.thumbView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo_icon.png"]];
            self.textLabel.text = [NSString stringWithFormat:@"<font face='Arial-ItalicMT' size='10' color='#808080'><i>create a new album <b>%@</b></i>...</font>", searchQuery];
            self.descriptionLabel.text = nil;
        }
        [self.thumbView setFrame:thumbFrame];
        [self addSubview:self.thumbView];
        
    }
    else if([subject isKindOfClass:[DefaultSearchSuggestion class]]) {
        
        DefaultSearchSuggestion *suggestion = (DefaultSearchSuggestion*)subject;
        
        if(self.searchType == ALBUM || self.searchType == FB_ALBUM) {
            self.thumbView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo_icon.png"]];
            self.textLabel.text = [NSString stringWithFormat:@"<font face='Arial-ItalicMT' size='10' color='#808080'><i>create a new album <b>%@</b></i>...</font>", suggestion.name];;
            self.descriptionLabel.text = nil;
        }
        
        [self.thumbView setFrame:thumbFrame];
        [self addSubview:self.thumbView];
        
    }
    else if([subject isKindOfClass:[Album class]]) {
        
        Album *album = (Album*)subject;
        self.thumbView = [album getCoverPhoto:thumbFrame.size.width :thumbFrame.size.height :YES];
        self.textLabel.text = album.name;
        self.descriptionLabel.text = [NSString stringWithFormat:@"by %@", album.owner.name];

        
        [self.thumbView setFrame:thumbFrame];
        [self addSubview:self.thumbView];
        
    }
    else if([subject isKindOfClass:[User class]]) {
        User *user = (User*)subject;
        
        self.thumbView = [user getProfilePic:thumbFrame.size.width :thumbFrame.size.height :YES];
        [self.thumbView setFrame:thumbFrame];
        [self addSubview:self.thumbView];
        
        self.textLabel.text = user.name;
        self.descriptionLabel.text = nil;
        
        if(self.searchType == FRIEND) {
            Album *album = [self.context objectForKey:@"album"];
            [self updateAllowUploadButtonState:user:album];
            [self updateShareButtonState:user:album];
        }
    }
    else if([subject isKindOfClass:[Contact class]]) {
        Contact *contact = (Contact*)subject;
        
        self.thumbView = [contact getThumb:thumbFrame.size.width :thumbFrame.size.height];
        [self.thumbView setFrame:thumbFrame];
        [self addSubview:self.thumbView];
        
        self.textLabel.text = contact.name;
        self.descriptionLabel.text = [contact getBestEmail];
    }
    else {
        DLog(@"warning, unknown subject type: %@", [subject class]);
    }
    
    if(!self.thumbView) {
        self.thumbView = [[UIImageView alloc] initWithFrame:oldThumbView.frame];
        [self addSubview:self.thumbView];
    }
    
    //remove dummy or old one
    [oldThumbView removeFromSuperview];
}

-(void)markSubjectChange:(NSString*)key:(NSObject*)value {
    NSObject *subject = [self.context objectForKey:@"subject"];
    NSMutableDictionary *parentContext = self.context[@"parent_context"];
    NSMutableDictionary *changes = parentContext[@"changes"];
    
    NSString *subjectID = nil;
    if([subject isKindOfClass:[User class]] || [subject isKindOfClass:[Album class]]) {
        subjectID = [subject valueForKey:@"id"];
    }
    else if([subject isKindOfClass:[Contact class]]) {
        subjectID = [(Contact*)subject getBestEmail];
    }
    else {
        [Logger track:@"unexpected_subject_type" :@{@"class":subject.class, @"description": subject.description}];
        return;
    }
    
    if(!changes[subjectID]) {
        NSMutableDictionary *change = [NSMutableDictionary dictionary];
        change[@"subject"] = subject;
        changes[subjectID] = change;
    }
    
    changes[subjectID][key] = value;
}

-(void)onAllowUploadButtonClicked {
    [Logger track:@"search_allow_upload_button_click" :@{@"type" : [ModalSearchView searchTypeToString:self.searchType]}];
    
    [self.allowUploadButton glowOnce];
    
    self.allowUploadButton.selected = !self.allowUploadButton.selected;
    [self markSubjectChange:@"allow_upload" :@(self.allowUploadButton.selected)];
    
    if(self.allowUploadButton.selected && self.searchType == CONTACT) {
        self.shareAlbumButton.selected = YES;
    }
}

-(void)onShareAlbumButtonClicked {
    [Logger track:@"search_share_album_button_click" :@{@"type" : [ModalSearchView searchTypeToString:self.searchType]}];
    [self.shareAlbumButton glowOnce];

    self.shareAlbumButton.selected = !self.shareAlbumButton.selected;
    [self markSubjectChange:@"share" :@(self.shareAlbumButton.selected)];
    
    if(!self.shareAlbumButton.selected) {
        self.allowUploadButton.selected = NO;
        [self markSubjectChange:@"allow_upload" :@(self.allowUploadButton.selected)];
    }
}

-(void)drawOnFrame:(CGRect)frame {
    [Util removeAllSubviews:self];
    
    CGRect thumbViewContainerRect, textContainerRect,
           descriptionContainerRect, shareAlbumButtonContainerRect, allowUploadButtonContainerRect;
    
    CGFloat shareButtonSize = (self.searchType == FRIEND || self.searchType == DEVICE) ? SHARE_BUTTON_SIZE : 0;
    CGFloat shareButtonPadding = (self.searchType == FRIEND || self.searchType == DEVICE) ? SHARE_BUTTON_PADDING : 0;
    
    CGFloat allowUploadButtonSize = 0;
    CGFloat allowUploadButtonPadding = 0;
    if(self.searchType == FRIEND || self.searchType == DEVICE) {
        Album *album = [self.context objectForKey:@"album"];
        if([album currentUserCanUpload]) {
            allowUploadButtonSize = SHARE_BUTTON_SIZE;
            allowUploadButtonPadding = SHARE_BUTTON_PADDING;
        }
    }
    
    CGRect r = frame;
    CGRectDivide(r, &thumbViewContainerRect, &r, THUMB_WIDTH + 2 * THUMB_PADDING, CGRectMinXEdge);
    CGRectDivide(r, &allowUploadButtonContainerRect, &r, allowUploadButtonSize + 2 * allowUploadButtonPadding, CGRectMaxXEdge);
    CGRectDivide(r, &shareAlbumButtonContainerRect, &r, shareButtonSize + 2 * shareButtonPadding, CGRectMaxXEdge);
    CGRectDivide(r, &textContainerRect, &descriptionContainerRect, r.size.height/2, CGRectMinYEdge);
    
    self.thumbView = [[UIImageView alloc] initWithFrame:CGRectInset(thumbViewContainerRect, THUMB_PADDING, THUMB_PADDING)];
    [self addSubview:self.thumbView];
    
    self.textLabel = [[RTLabel alloc] initWithFrame:CGRectInset(textContainerRect, TEXT_X_PADDING, TEXT_Y_PADDING)];
    [self.textLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:11]];
    [self.textLabel setTextAlignment:NSTextAlignmentLeft];
    [self addSubview:self.textLabel];
    //[self.textLabel setBackgroundColor:[UIColor blueColor]];
    
    self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectInset(descriptionContainerRect, TEXT_X_PADDING, TEXT_Y_PADDING)];
    [self addSubview:self.descriptionLabel];
    self.descriptionLabel.font = [UIFont fontWithName:@"Arial-ItalicMT" size:10];
    self.descriptionLabel.textColor = UIColorFromRGB(0x808080);
    //[self.descriptionLabel setBackgroundColor:[UIColor yellowColor]];
    
    CGRect shareButtonRect = CGRectMake(0, 0, shareButtonSize, shareButtonSize);
    shareButtonRect = [Util centerRect:shareButtonRect :[Util getRectCenter:shareAlbumButtonContainerRect]];
    
    CGRect allowUploadButtonRect = CGRectMake(0, 0, allowUploadButtonSize, allowUploadButtonSize);
    allowUploadButtonRect = [Util centerRect:allowUploadButtonRect :[Util getRectCenter:allowUploadButtonContainerRect]];
    
    
    self.shareAlbumButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.shareAlbumButton setImage:[UIImage imageNamed:@"album_shared_icon.png"] forState:UIControlStateSelected];
    [self.shareAlbumButton setImage:[UIImage imageNamed:@"album_not_shared_icon.png"] forState:UIControlStateNormal];
    [self.shareAlbumButton addTarget:self action:@selector(onShareAlbumButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.shareAlbumButton setTitle:@"shared" forState:UIControlStateNormal];
    [self.shareAlbumButton setFrame:shareButtonRect];
    [self.shareAlbumButton setClipsToBounds:YES];
    [self.shareAlbumButton setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.shareAlbumButton];
    [self bringSubviewToFront:self.shareAlbumButton];
    
    [self.shareAlbumButton setHelpText:@"share this album with this friend"];
    [self.shareAlbumButton setHelpSpotlightShapeOval:YES];

    
    self.allowUploadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.allowUploadButton setImage:[UIImage imageNamed:@"album_upload_allowed_icon.png"] forState:UIControlStateSelected];
    [self.allowUploadButton setImage:[UIImage imageNamed:@"album_upload_not_allowed_icon.png"] forState:UIControlStateNormal];
    [self.allowUploadButton addTarget:self action:@selector(onAllowUploadButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.allowUploadButton setTitle:@"upload allowed" forState:UIControlStateNormal];
    [self.allowUploadButton setFrame:allowUploadButtonRect];
    [self.allowUploadButton setClipsToBounds:YES];
    [self.allowUploadButton setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.allowUploadButton];
    [self bringSubviewToFront:self.allowUploadButton];
    
    if(self.searchType == DEVICE) {
        [self.allowUploadButton setHelpText:@"allow the email recipient to upload photos to this album"];
    }
    else {
        [self.allowUploadButton setHelpText:@"allow this friend to upload photos directly to this album"];
    }
    [self.allowUploadButton setHelpSpotlightShapeOval:YES];

}

-(void)updateAllowUploadButtonState:(NSObject*)subject:(Album*)album {
    if([subject isKindOfClass:[Contact class]]) {
        self.allowUploadButton.selected = NO;
    }
    else {
        User *user = (User*)subject;
        self.allowUploadButton.selected = [album isAllowedUpload:user];    
    }
    
}

-(void)updateShareButtonState:(User*)user:(Album*)album {
    if(![album isFacebookAlbum]) {
        self.shareAlbumButton.selected = [album isSharedWithUser:user];
    }
    else {
        self.shareAlbumButton.hidden = YES;
    }
}
@end


@interface SearchResultViewCell()
@property (strong) SearchResultCellView *cell;
@end

@implementation SearchResultViewCell
@synthesize cell;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier:(CGFloat)width:(CGFloat)height
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self setBackgroundColor:[UIColor clearColor]];

        
        //cell frame height is always set to 44, need to set it manually
        self.frame = CGRectMake(0, 0, width, height);
        CGRect contentRect = CGRectInset(self.frame, CELL_X_PADDING, CELL_Y_PADDING);
        
        self.cell = [[SearchResultCellView alloc] initWithFrame:contentRect];
        [self.contentView addSubview:self.cell];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)update:(SearchType)searchType:(NSMutableDictionary*)context {
    self.context = context;
    [self.cell update:searchType:context];
}
@end
