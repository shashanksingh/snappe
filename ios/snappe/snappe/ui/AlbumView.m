//
//  EventAlbumView.m
//  snappe
//
//  Created by Shashank on 12/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AlbumView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>
#import "UIController.h"
#import "AlbumEditView.h"
#import "RootViewController.h"
#import "Macros.h"
#import "Util.h"
#import "PhotoAccessor.h"
#import "EventManager.h"
#import "ModalSearchView.h"
#import "Contact.h"
#import "GalleryView.h"
#import "UserAccessor.h"
#import "AlbumAccessor.h"
#import "FakeAlbum.h"
#import "ExtOpQueue.h"
#import "AlbumDownloadOperation.h"
#import "SVProgressHUD.h"
#import "ExtendedOperationAccessor.h"
#import "AlbumSuggestionView.h"
#import "ProgressView.h"
#import "Logger.h"
#import "AlbumShareOperation.h"
#import "UIView+Glow.h"
#import "ModalAlertView.h"
#import "AlbumPushOperation.h"
#import "AlbumActionPopup.h"
#import "CustomBadge.h"
#import "GraphManager.h"
#import "AddPhotosView.h"
#import "ActionConfirmationView.h"


#define BORDER_SIZE (IS_IPAD ? 10 : 5)

#define ACTION_ICON_DIAMETER 44
#define ACTION_ICON_MARGIN 8
#define ICON_BUTTON_PANEL_HEIGHT 46
#define HIND_VIEW_ICON_BUTTON_PANEL_HEIGHT 36
#define ICON_BUTTON_PANEL_PADDING 2

#define TITLE_SECTION_HEIGHT (IS_IPAD ? 50 : 25)

#define INFO_SECTION_HEIGHT_FRACTION 0.18
#define INFO_ICON_SECTION_WIDTH 24
#define INFO_ICON_SIZE 16

#define TITLE_INFO_ICON_SECTION_SIZE 18

#define MAX_FRIEND_SUGGESTIONS 2000
#define MAX_ALBUM_SUGGESTIONS 2000

typedef enum ActionButtonType {
    INFO, EDIT, VIEW, ACTION_META, DOWNLOAD,
    FB_UPLOAD, FRIENDS, EMAIL, FLIP_BACK,
    MARK_PRIVATE, ADD_PHOTOS
} ActionButtonType;


@interface AlbumView()
@property (strong) UIView *frontView;
@property (strong) UIView *hindView;
@property (strong) UILabel *title;
@property (strong) UIImageView *ownershipIcon;
@property (strong) UIImageView *deviceIcon;
@property (strong) UIView *preview;
@property (strong) UIView *iconButtonPanel;
@property (strong) AlbumActionPopup *albumActionPopup;
@property (strong) CustomBadge *badge;
@end

@implementation AlbumView
@synthesize album;
@synthesize frontView;
@synthesize hindView;

- (id)initWithFrame:(CGRect)frame:(Album *)album_
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.layer setBorderColor: [[UIColor whiteColor] CGColor]]; 
        [self.layer setBorderWidth:0];
        [Util drawFastShadow:self];
        [self.layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
        [self.layer setShadowRadius:3.0];
        [self.layer setShadowOpacity:1.0];
    
        self.album = album_;
        [self drawWithFrame:CGRectInset(self.bounds, BORDER_SIZE, BORDER_SIZE)];
        
        [self updateAlbum:album_];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)];
        [self addGestureRecognizer:tapRecognizer];
    }
    return self;
}

-(void)updateAlbum:(Album*)album_ {
    self.album = album_;
    
    if([self.album isCurrentDeviceAlbum]) {
        [self setHelpText:@"photo albums in this phone"];
    }
    else {
        [self setHelpText:@"your & your friends' Facebook albums & albums friends have shared with you on Snappe"];
    }
    
    [self.title setText: [self.album getName]];
    
    NSString *ownershipIconName = (![[FBManager getInstance] hasUserAuthorizedOnce] || [self.album isOwnedByCurrentUser]) ? @"user_icon.png" : @"friends_icon.png";
    self.ownershipIcon.image = [UIImage imageNamed:ownershipIconName];
    
    NSString *deviceIconName = (![self isAlbumFake] && [self.album isFacebookAlbum]) ? @"facebook_icon.png" : @"phone_icon.png";
    self.deviceIcon.image = [UIImage imageNamed:deviceIconName];
    
    [self drawAlbumPreview];
    [self updateIconButtonPanel];
    
    if(!self.hindView.hidden) {
        [self flipBackToFrontView:YES];
    }
    
    [self updateBadge];
}

-(void) drawWithFrame:(CGRect)frame {
    
    self.frontView = [[UIView alloc] initWithFrame:frame];
    [self addSubview:self.frontView];
    
    [self drawFrontView];
    
    self.hindView = [[UIView alloc] initWithFrame:frame];
    [self.hindView setHidden:YES];
    
    [self addSubview:self.hindView];
    
    [self drawBadge];
    
    self.frontView.clipsToBounds = NO;
    self.hindView.clipsToBounds = NO;
}

-(void)drawBadge {
    
    CGRect badgeFrame = CGRectMake(self.bounds.size.width - 20, -15, 30, 30);
    
    self.badge = [CustomBadge customBadgeWithString:@"\u272A"
                       withStringColor:[UIColor whiteColor]
                        withInsetColor:[UIColor redColor]
                        withBadgeFrame:YES
                   withBadgeFrameColor:[UIColor whiteColor]
                             withScale:1.0
                           withShining:YES];
    self.badge.frame = badgeFrame;
    self.badge.hidden = YES;
    [self.frontView addSubview:self.badge];
}

-(void)drawFrontView {
    CGFloat w = self.frontView.bounds.size.width;
    CGFloat h = self.frontView.bounds.size.height;
    
    CGFloat titleSectionHeight = TITLE_SECTION_HEIGHT;
    CGFloat thumbSectionHeight = h - titleSectionHeight;
    
    NSArray *titleBarItems = [self drawTitle:self.frontView:self.frontView.bounds];
    self.title = [titleBarItems objectAtIndex:0];
    self.ownershipIcon = [titleBarItems objectAtIndex:1];
    self.deviceIcon = [titleBarItems objectAtIndex:2];
    
    CGRect previewRect = CGRectMake(0, titleSectionHeight, w, thumbSectionHeight);
    CGRect iconButtonPanelRect = CGRectMake(0, h - ICON_BUTTON_PANEL_HEIGHT, w, ICON_BUTTON_PANEL_HEIGHT);
    
    
    self.preview = [[UIView alloc] initWithFrame:previewRect];
    [self.frontView addSubview:self.preview];
    
    UIView *iconButtonPanel = [[UIView alloc] initWithFrame: iconButtonPanelRect];
    [iconButtonPanel setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6]];
    [self.frontView addSubview:iconButtonPanel];
    
    NSMutableArray *actionButtons = [NSMutableArray array];
    [actionButtons addObject:@[@(INFO), @"info_icon.png", @"onAlbumInfoClick:", @"Album Info", @"get information about the album"]];
    [actionButtons addObject:@[@(VIEW), @"view_icon.png", @"onViewAlbumClick", @"View Album Photos", @"view all photos in the album"]];
    
    [actionButtons addObject:@[@(EDIT), @"edit_icon.png", @"onAlbumEditClick:", @"Edit", @"edit the album, make photos private"]];
    
    [actionButtons addObject:@[@(ADD_PHOTOS), @"add_photos_icon.png", @"onAddPhotosClick", @"Add Photos", @"add more photos to this album"]];
    
    [actionButtons addObject:@[@(ACTION_META), @"action_meta_icon.png", @"onActionButtonClick:", @"Actions", @"share the album with Facebook friends, email"]];
    
    [self drawIconButtonPanel:self.frontView:actionButtons];

}

-(void)drawHindView {
    NSArray *titleBarItems = [self drawTitle:self.hindView:self.hindView.bounds];
    UILabel *titleLabel = [titleBarItems objectAtIndex:0];
    [titleLabel setText:self.album.name];
    
    CGFloat w = self.hindView.bounds.size.width;
    CGFloat h = self.hindView.bounds.size.height;
    
    CGFloat titleSectionHeight = TITLE_SECTION_HEIGHT;
    CGFloat infoSectionHeight = h * INFO_SECTION_HEIGHT_FRACTION;
    
    CGRect titleSectionSepratorFrame = CGRectMake(0, titleSectionHeight - 4, w, 1);
    UIView *titleSectionSeprator = [[UIView alloc] initWithFrame:titleSectionSepratorFrame];
    [titleSectionSeprator setBackgroundColor:[UIColor blackColor]];
    [self.hindView addSubview:titleSectionSeprator];
    
    //album.owner is not set if the user is not authenticated yet
    NSString *ownerName = @"You";
    if(self.album.owner) {
        ownerName = self.album.owner.name;
        if([self.album isOwnedByCurrentUser]) {
            ownerName = [ownerName stringByAppendingString:@" (you)"];
        }
    }
    
    NSMutableArray *sectionData = [[NSMutableArray alloc] init];
    [sectionData addObject:[NSArray arrayWithObjects:@"user_icon.png", ownerName, nil]];
    
    NSString *photoCountText = [NSString stringWithFormat:@"%d", [self.album getPhotos:NO :YES :NO].count];
    [sectionData addObject:[NSArray arrayWithObjects:@"photo_icon_small.png", photoCountText, nil]];
    
    [sectionData addObject:[NSArray arrayWithObjects:@"calendar_icon.png", [self.album getDate], nil]];
    [sectionData addObject:[NSArray arrayWithObjects:@"globe_icon.png", [self.album getLocationName], nil]];
    
    for(NSUInteger i=0; i<sectionData.count; ++i) {
        NSArray *sectionDatum = [sectionData objectAtIndex:i];
        
        CGRect sectionFrame = CGRectMake(0, titleSectionHeight + i * infoSectionHeight, w, infoSectionHeight);
        UIView *sectionView = [[UIView alloc] initWithFrame:sectionFrame];
        if(i%2 == 0) {
            [sectionView setBackgroundColor:UIColorFromRGBA(0xEFEFEF, 0.55)];
        }
        if(i == sectionData.count - 1) {
            CGRect f = sectionView.frame;
            f.size.width -= ICON_BUTTON_PANEL_PADDING + ACTION_ICON_DIAMETER;
            sectionView.frame = f;
        }
        [self.hindView addSubview:sectionView];
        
        CGRect iconSectionFrame, textFrame;
        CGRectDivide(sectionView.bounds, &iconSectionFrame, &textFrame, INFO_ICON_SECTION_WIDTH, CGRectMinXEdge);
        
        CGRect iconFrame = CGRectMake(0, 0, INFO_ICON_SIZE, INFO_ICON_SIZE);
        iconFrame = [Util centerRect:iconFrame :[Util getRectCenter:iconSectionFrame]];
        
        UIImageView *iconView = [[UIImageView alloc] initWithFrame:iconFrame];
        [iconView setBackgroundColor:[UIColor clearColor]];
        [sectionView addSubview:iconView];
        iconView.image = [UIImage imageNamed:[sectionDatum objectAtIndex:0]];
        iconView.contentMode = UIViewContentModeCenter;
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:textFrame];
        [textLabel setBackgroundColor:[UIColor clearColor]];
        [textLabel setTextAlignment:UITextAlignmentLeft];
        [textLabel setFont:[UIFont fontWithName:@"AmericanTypewriter" size:11]];
        [textLabel setTextColor:[UIColor grayColor]];
        [sectionView addSubview:textLabel];
        [textLabel setText:[sectionDatum objectAtIndex:1]];
        textLabel.adjustsFontSizeToFitWidth = NO;
        textLabel.lineBreakMode = UILineBreakModeTailTruncation;
        
    }
    
    NSMutableArray *actionButtons = [[NSMutableArray alloc] init];
    [actionButtons addObject:@[@(FLIP_BACK), @"go_back.png", @"onInfoFlipBackClick", @"Album Info"]];
    [self drawIconButtonPanel:self.hindView :actionButtons];
    
}

-(NSArray*)drawTitle:(UIView*)parent:(CGRect)frame {
    CGFloat w = frame.size.width;
    
    CGFloat titleSectionHeight = TITLE_SECTION_HEIGHT;
    CGRect titleSectionRect = CGRectMake(0, -3, w, titleSectionHeight - 3);
    
    CGRect titleRect, ownershipIconSectionRect, deviceIconSectionRect;
    
    CGRect r = titleSectionRect;
    CGRectDivide(r, &ownershipIconSectionRect, &r, TITLE_INFO_ICON_SECTION_SIZE, CGRectMaxXEdge);
    CGRectDivide(r, &deviceIconSectionRect, &titleRect, TITLE_INFO_ICON_SECTION_SIZE, CGRectMaxXEdge);
    
    UILabel *title = [[UILabel alloc] initWithFrame:titleRect];
    title.adjustsFontSizeToFitWidth = NO;
    title.lineBreakMode = UILineBreakModeTailTruncation;
    
    [title setTextColor:UIColorFromRGB(0x0)];
    [title setFont: [UIFont fontWithName:@"AmericanTypewriter" size:11.0f]];
    [parent addSubview:title];
    
    CGRect iconRect = CGRectMake(0, 0, INFO_ICON_SIZE, INFO_ICON_SIZE);
    
    CGRect ownershipIconRect = [Util centerRect:iconRect :[Util getRectCenter:ownershipIconSectionRect]];
    UIImageView *ownershipIcon = [[UIImageView alloc] initWithFrame:ownershipIconRect];
    [parent addSubview:ownershipIcon];
    
    CGRect deviceIconRect = [Util centerRect:iconRect :[Util getRectCenter:deviceIconSectionRect]];
    UIImageView *deviceIcon = [[UIImageView alloc] initWithFrame:deviceIconRect];
    [parent addSubview:deviceIcon];
    
    return @[title, ownershipIcon, deviceIcon];
    
}

-(void) drawAlbumPreview {
    
    [Util removeAllSubviews:self.preview];
    
    CGFloat w = self.preview.bounds.size.width;
    CGFloat h = self.preview.bounds.size.height;
    
    NSArray *availablePhotos = [self.album getPhotos:NO :YES :NO];
    
    Photo *firstPhoto, *secondPhoto;
    NSUInteger numPhotos = availablePhotos.count;
    if(numPhotos > 0) {
        firstPhoto = availablePhotos[0];
    }
    if(numPhotos > 1) {
        secondPhoto = availablePhotos[1];
    }
    
    if(numPhotos >= 2) {
        
        UIImageView *firstImageView = [firstPhoto getUIImageView:(w/2) :h :YES :NO];
        CGRect firstRect = CGRectMake(0, 0, (w/2), h);
        [firstImageView setFrame:firstRect];
        [self.preview addSubview:firstImageView];
        
        UIImageView *secondImageView = [secondPhoto getUIImageView:(w/2) :h :YES :NO];
        CGRect secondRect = CGRectMake((w/2), 0, (w/2), h);
        [secondImageView setFrame:secondRect];
        [self.preview addSubview:secondImageView];
        
    }
    else {
        
        UIImageView *imageView = [firstPhoto getUIImageView:w :h :YES :NO];
        CGRect rect = CGRectMake(0, 0, w, h);
        [imageView setFrame:rect];
        [self.preview addSubview:imageView];
        
    }

}

-(void)drawIconButtonPanel:(UIView*)parent:(NSArray*)actionButtons {
    
    
    CGRect frame = parent.frame;
    CGFloat w = frame.size.width;
    CGFloat h = frame.size.height;
    
    CGFloat panelHeight = parent == self.hindView ? HIND_VIEW_ICON_BUTTON_PANEL_HEIGHT : ICON_BUTTON_PANEL_HEIGHT;
    
    CGRect iconButtonPanelRect = CGRectMake(0, h - panelHeight, w, panelHeight);
    self.iconButtonPanel = [[UIView alloc] initWithFrame: iconButtonPanelRect];
    [self.iconButtonPanel setBackgroundColor:UIColorFromRGBA(0x0, 0.1)];
    [parent addSubview:self.iconButtonPanel];
    
    for(unsigned int i=0; i<[actionButtons count]; ++i) {
        CGRect buttonRect = CGRectMake(iconButtonPanelRect.size.width - (i + 1) * ACTION_ICON_DIAMETER - i * ACTION_ICON_MARGIN, iconButtonPanelRect.size.height/2 - ACTION_ICON_DIAMETER/2, ACTION_ICON_DIAMETER, ACTION_ICON_DIAMETER);
        
        NSArray *buttonInfo = [actionButtons objectAtIndex:i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:[buttonInfo objectAtIndex:1]] forState:UIControlStateNormal];
        [button addTarget:self action:NSSelectorFromString([buttonInfo objectAtIndex:2]) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:[buttonInfo objectAtIndex:3] forState:UIControlStateNormal];
        [button setFrame:buttonRect];
        [button setClipsToBounds:YES];
        [button setBackgroundColor:[UIColor clearColor]];
        button.tag = ((NSNumber*)buttonInfo[0]).intValue;
        [self.iconButtonPanel addSubview:button];
        
        if(buttonInfo.count >= 5) {
            [button setHelpText:[buttonInfo objectAtIndex:4]];
            [button setHelpSpotlightShapeOval:YES];
        }
    
    }
}

-(void)onActionButtonClick:(UIView*)button {
    NSMutableArray *actionButtons = [NSMutableArray array];
    [actionButtons addObject:@[@(DOWNLOAD), @"download_icon.png", self, @"onDownloadClick", @"Download", @"download the album as a local album"]];
    [actionButtons addObject:@[@(FB_UPLOAD), @"fb_icon.png", self, @"onFacebookShareClick", @"Facebook Upload", @"upload the album to Facebook"]];
    [actionButtons addObject:@[@(EMAIL), @"email_icon.png", self, @"onEmailShareClick", @"Email Share", @"share the album by email, allow recipient to upload"]];
    [actionButtons addObject:@[@(FRIENDS), @"view_uploaders_icon.png", self, @"onViewUploadersClick", @"Friend Share", @"share the album with Facebook friends, add them as uploaders"]];
    
    NSMutableArray *applicableButtons = [NSMutableArray array];
    for(int i=0; i<actionButtons.count; i++) {
        ActionButtonType type = ((NSNumber*)actionButtons[i][0]).intValue;
        
        BOOL applicable = YES;
        //if not authorized yet, show all options for onboarding
        if([[FBManager getInstance] hasUserAuthorizedOnce]) {
            if(type == FB_UPLOAD) {
                applicable = [self.album isOwnedByCurrentUser] && ![self.album isFacebookAlbum];
            }
            
            else if(type == EMAIL) {
                applicable = YES;
            }
            else if(type == FRIENDS) {
                applicable = [self.album isOwnedByCurrentUser];
            }
        }
        
        //showing download on local albums can be confusing
        if(type == DOWNLOAD) {
            applicable = ![self isAlbumFake] && [[FBManager getInstance] hasUserAuthorizedOnce] && ![self.album isCurrentDeviceAlbum];
        }
        
        if(applicable) {
            [applicableButtons addObject:actionButtons[i]];
        }
    }
    
    RootViewController *rvc = [[UIController getInstance] getUIControl:ROOT_VIEW_CONTROLLER];
    
    CGPoint point = CGPointMake(button.frame.size.width/2, 0);
    point = [button convertPoint:point toView:rvc.view];
    self.albumActionPopup = [[AlbumActionPopup alloc] init:applicableButtons :point];
    
    [Util showPopup:rvc.view :self.albumActionPopup :YES :nil];
    
}

-(void) updateIconButtonPanel {
    NSMutableArray *showIcons = [NSMutableArray array];
    
    for(unsigned int i=0; i<[self.iconButtonPanel.subviews count]; ++i) {
        ActionButtonType type = ((UIView*)self.iconButtonPanel.subviews[i]).tag;
        
        BOOL show = YES;
        //if not authorized yet, show max options for onboarding
        if([[FBManager getInstance] hasUserAuthorizedOnce]) {
            if(type == EDIT) {
                show = [self.album isOwnedByCurrentUser] && ![self.album isFacebookAlbum];
            }
        }
        
        //before first auth only current device albums are present
        //they have another place for this button
        if(type == ADD_PHOTOS) {
            show = ![self.album isCurrentDeviceAlbum] && [self.album currentUserCanUpload];
        }
        
        //no view photos icon if empty album, authorized or not
        if(type == VIEW) {
            //hide the view album icon if no photos
            show = ![self isAlbumFake] && [[self.album getPhotos:NO :YES :NO] count] > 0;
        }
        
        [showIcons addObject:@(show)];
    }
    
    int x = self.iconButtonPanel.bounds.size.width;
    for(int i=0; i<self.iconButtonPanel.subviews.count; i++) {
        BOOL show = ((NSNumber*)showIcons[i]).boolValue;
        UIView *button = self.iconButtonPanel.subviews[i];
        button.hidden = !show;
        
        CGRect frame = button.frame;
        if(show) {
            x -= frame.size.width + (i > 0  ? ACTION_ICON_MARGIN : 0);
            frame.origin.x = x;
            button.frame = frame;
        }
    }
}

-(void)updateBadge {
    if(!self.album.newPhotosCount || self.album.newPhotosCount.intValue == 0) {
        self.badge.hidden = YES;
    }
    else {
        self.badge.hidden = NO;
        //Not setting count until we get the counts right
        //until then we will just indicate that there are
        //new photos
        //[self.badge setBadgeText:[NSString stringWithFormat:@"%d", self.album.newPhotosCount.intValue]];
    }
}

-(void)onTap {
    if(self.album.newPhotosCount.intValue > 0) {
        
        NSString *albumID = self.album.id;
        
        self.album.newPhotosCount = @(0);
        
        dispatch_async([CoreController getCoreDataBackgroundQueue] , ^{
            Album *a = [Album getById:albumID];
            a.newPhotosCount = @(0);
            [[GraphManager getInstance] savePersistentContext];
        });
        
        [UIView beginAnimations:@"fade out" context:nil];
        [UIView setAnimationDuration:1.0];
        self.badge.alpha = 0.0;
        [UIView commitAnimations];
        
        [Logger track:@"album_view_change_marker_hidden_on_tap" :@{@"aid" : self.album.id}];
    }
    
    //clicking on hind view flips back
    if(!self.hindView.hidden) {
        [self flipBackToFrontView:NO];
    }
}

-(void) onAlbumInfoClick:(UIView*)sender {
    //we don't need hind view for fake albums as we never show them
    //hind view is redrawn every time, which is ok since it is done
    //rarely and the flip animantion hides any delay
    if(![self isAlbumFake]) {
        [Util removeAllSubviews:self.hindView];
        [self drawHindView];
        [Logger track:@"album_view_info_button_click" :@{@"aid" : self.album.id}];
    }
    
    self.badge.hidden = YES;
    [UIView transitionFromView:self.frontView toView:self.hindView duration:1 options:UIViewAnimationOptionTransitionFlipFromRight|UIViewAnimationOptionShowHideTransitionViews completion:^(BOOL finished) {
        
    }];
}

-(void) onAlbumEditClick:(UIView*)button {
    
    [Logger track:@"album_view_edit_button_click" :@{@"aid" : self.album.id}];
    
    NSMutableArray *actionButtons = [NSMutableArray array];
    [actionButtons addObject:@[@(MARK_PRIVATE), @"mark_photos_private_icon.png", self, @"onMarkPrivateClick", @"Set Privacy", @"change photo privacy"]];
    [actionButtons addObject:@[@(ADD_PHOTOS), @"add_photos_icon.png", self, @"onAddPhotosClick", @"Add Photos", @"add more photos to this album"]];
    
    NSMutableArray *applicableButtons = [NSMutableArray array];
    for(int i=0; i<actionButtons.count; i++) {
        [applicableButtons addObject:actionButtons[i]];
    }
    
    RootViewController *rvc = [[UIController getInstance] getUIControl:ROOT_VIEW_CONTROLLER];
    
    CGPoint point = CGPointMake(button.frame.size.width/2, 0);
    point = [button convertPoint:point toView:rvc.view];
    self.albumActionPopup = [[AlbumActionPopup alloc] init:applicableButtons :point];
    
    [Util showPopup:rvc.view :self.albumActionPopup :YES :nil];
}

-(void)onMarkPrivateClick {
    
    [Logger track:@"album_view_mark_private_menu_button_click" :@{@"aid" : self.album.id}];
    
    if(![Util checkAndAlertAuth:@"set photo privacy"]) return;
    [AlbumEditView show:self.album];
}

-(void)onAddPhotosClick {
    
    [Logger track:@"album_view_add_menu_button_click" :@{@"aid" : self.album.id}];
    
    if(![Util checkAndAlertAuth:@"add photos to your & your friends' albums"]) return;
    [AddPhotosView show:self.album];
}

-(void) onFacebookShareClick {
    [Logger track:@"album_view_fb_share_menu_button_click" :@{@"aid" : self.album.id}];
    
    if(![Util checkAndAlertAuth:@"share your albums on Facebook"]) return;
    
    [ModalAlertView showBusy:@"please wait..."];
    
    __weak typeof(self) bself = self;
    [self getSelectedAlbumToUploadTo:self.album :YES :^(NSObject *selection){
        if(selection) {
            [bself uploadToFacebook:selection];
        }
        
    }:^(NSError *error){
        [Util alertError:error :@"An unexpected error occurred. Please try again."];
        [Logger logError:@"album_view_fb_share_error" :error];
    }];
    
}

-(void) onViewAlbumClick {
    [Logger track:@"album_view_view_album_button_click" :@{@"aid" : self.album.id}];
    UIViewController *rootViewController = [[UIController getInstance] getUIControl:ROOT_VIEW_CONTROLLER];
    GalleryView *galleryView = [[GalleryView alloc] init:self.album];
    [rootViewController.view addSubview:galleryView];
}

-(void) onEmailShareClick {
    [Logger track:@"album_view_email_share_menu_button_click" :@{@"aid" : self.album.id}];
    if(![Util checkAndAlertAuth:@"share albums by email and allow email uploads"]) return;
    [ModalSearchView show:DEVICE:@{@"album" : self.album}];
}

-(void) onDownloadClick {
    [Logger track:@"album_view_download_album_menu_button_click" :@{@"aid" : self.album.id}];
    if(![Util checkAndAlertAuth:@"download albums to your phone"]) return;
    
    [ModalAlertView showBusy:@"please wait..."];
    
    //TODO: what if the carousel is reloaded and the album corresponding to self
    //changes before we start our action?
    __weak typeof(self) bself = self;
    
    Album *src = self.album;
    
    [self getSelectedAlbumToUploadTo:self.album :NO :^(NSObject *selection){
        
        NSString *destinationName = ([selection isKindOfClass:[Album class]]) ? ((Album*)selection).name : selection.description;
                                      
        NSString *confrimationMessage = [NSString stringWithFormat:@"Are you sure you want to download '%@' to '%@'?", src.name, destinationName];
        
        [ActionConfirmationView confirm:@"album_download" :confrimationMessage :^(NSNumber *ok) {
            [ModalAlertView hideBusyAlert];
            if(ok.boolValue) {
                if(selection) {
                    [bself download:src:selection];
                }
            }
        }];
        
    }:^(NSError *error){
        [ModalAlertView hideBusyAlert];
        [Util alertError:error :@"An unexpected error occurred. Please try again."];
    }];
}

-(void)onInfoFlipBackClick {
    [Logger track:@"album_view_flip_back_button_click" :@{@"aid" : self.album.id}];
    [self flipBackToFrontView:NO];
}

-(void)flipBackToFrontView:(BOOL)fast {
    [UIView transitionFromView:self.hindView toView:self.frontView duration:(fast ? 0.0 : 1.0) options:UIViewAnimationOptionTransitionFlipFromLeft|UIViewAnimationOptionShowHideTransitionViews completion:^(BOOL finished) {
        [self updateBadge];
    }];
}

-(void)onViewUploadersClick {
    [Logger track:@"album_view_view_uploaders_menu_button_click" :@{@"aid" : self.album.id}];
    if(![Util checkAndAlertAuth:@"share albums with Friends & allow them to upload photos to your albums"]) return;
    
    [Album getSimilarUsers:album :MAX_FRIEND_SUGGESTIONS :^(NSArray *suggestions) {
        [ModalSearchView show:FRIEND:@{@"album": self.album, @"suggestions": suggestions}];
        
    } :^(NSError *error) {
        [Logger logError:@"error in sorting" :error];
        [Util alertError:nil :@"There was an unexpected error. Please try again later."];
    }];
    
}

-(BOOL)isAlbumFake {
    return [self.album.class isSubclassOfClass:[BaseFakeAlbum class]];
}

-(void)download:(Album*)srcAlbum:(NSObject*)destinationAlbum {
    AlbumDownloadOperation *op = [[AlbumDownloadOperation alloc] initWithAlbums:srcAlbum:destinationAlbum];
    [Util executeExtOp:op];
}

-(void)uploadToFacebook:(NSObject*)destinationAlbum {
    AlbumPushOperation *op = [[AlbumPushOperation alloc] initWithAlbums:self.album :destinationAlbum];
    [Util executeExtOp:op];
}

-(void)getSelectedAlbumToUploadTo:(Album*)album:(BOOL)facebook:(GenericCallback)callback:(GenericErrorback)errorback {
    
    [Album getSimilarAlbums:self.album :!facebook :facebook :facebook :MAX_ALBUM_SUGGESTIONS:^(NSArray *suggestedAlbums)
    {
        BOOL namesakeInSuggestions = NO;
        for(Album *suggestedAlbum in suggestedAlbums) {
            if([suggestedAlbum.name isEqualToString:self.album.name]) {
                namesakeInSuggestions = YES;
                break;
            }
        }
        
        NSMutableArray *suggestions = [suggestedAlbums mutableCopy];
        if(!namesakeInSuggestions) {
            DefaultSearchSuggestion *namesakeSuggestion = [DefaultSearchSuggestion new];
            namesakeSuggestion.name = self.album.name;
            [suggestions insertObject:namesakeSuggestion atIndex:0];
        }
        
        [ModalAlertView hideBusyAlert];
        
        [ModalSearchView show:(facebook ? FB_ALBUM : ALBUM):@{@"album": self.album, @"suggestions": suggestions}];
        
        __block NSString *onSelectListenerID, *onDismissListenerID;
        
        onSelectListenerID = [[EventManager getInstance] addEventListener:ALBUM_OR_FRIEND_SELECTED_ON_SEARCH :YES :^(va_list args)
                              {
                                  [[EventManager getInstance] removeEventListener:onDismissListenerID];
                                  
                                  NSObject *selected = va_arg(args, NSObject*);
                                  if([selected isKindOfClass:[DefaultSearchSuggestion class]]) {
                                      callback(((DefaultSearchSuggestion*)selected).name);
                                  }
                                  else if([selected isKindOfClass:[CurrentSearchQueryAsResult class]]) {
                                      callback(((CurrentSearchQueryAsResult*)selected).value);
                                  }
                                  else {
                                      Album *selectedAlbum = (Album*)selected;
                                      callback(selectedAlbum);
                                  }
                              }];
        
        onDismissListenerID = [[EventManager getInstance] addEventListener:SEARCH_DISMISSED :YES :^(va_list args)
                               {
                                   [[EventManager getInstance] removeEventListener:onSelectListenerID];
                                   callback(nil);
                               }];
        
    }:errorback];
}

-(void)showShareView {
    [self onViewUploadersClick];
}
@end
