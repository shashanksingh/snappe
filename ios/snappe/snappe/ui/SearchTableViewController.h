//
//  SearchView.h
//  snappe
//
//  Created by Shashank on 15/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModalSearchView.h"

@interface SearchTableViewController : UITableViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
- (id)initWithStyle:(UITableViewStyle)style;
-(void)update:(SearchType)searchType:(NSDictionary*)context;
-(void)processChanges;
@end
