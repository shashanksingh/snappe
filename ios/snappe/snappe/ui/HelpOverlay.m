//
//  HelpOverlay.m
//  snappe
//
//  Created by Shashank on 17/03/13.
//
//

#import "HelpOverlay.h"
#import "Macros.h"
#import "Util.h"
#import "RootViewController.h"
#import "UIController.h"
#import "UIView+Help.h"
#import "HoleyView.h"
#import "EventManager.h"
#import "ArrowView.h"
#import "Logger.h"
#import <QuartzCore/QuartzCore.h>

#define HELP_BUTTON_SIZE 44
#define HELP_TEXT_VIEW_WIDTH 220
#define HELP_TEXT_VIEW_HEIGHT 80

@interface HelpOverlay()<UIGestureRecognizerDelegate>
@property (strong) HoleyView *holeyView;
@property (strong) UIButton *helpButton;
@property (strong) UIView *textView;
@property (strong) UILabel *helpText;
@property (strong) ArrowView *arrow;
@property (strong) UILabel *descrption;
@end

@implementation HelpOverlay

- (id)init {
    CGRect frame = [UIScreen mainScreen].bounds;
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        [self setBackgroundColor:UIColorFromRGBA(0x0, 0.0)];
        [self draw];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        [self addGestureRecognizer:tapRecognizer];
        
        UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe)];
        swipeGesture.direction = (UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionRight);
        [self addGestureRecognizer:swipeGesture];
    }
    return self;
}

+(HelpOverlay*) getInstance {
    static HelpOverlay *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[HelpOverlay alloc] init];
    });
    return sharedInstance;
}

+(void)hide {
    [self getInstance].hidden = YES;
}

+(void)show {
    [self getInstance].hidden = NO;
}

-(void)draw {
    self.holeyView = [[HoleyView alloc] initWithFrame:self.frame];
    self.holeyView.hidden = YES;
    [self addSubview:self.holeyView];
    
    self.textView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, HELP_TEXT_VIEW_WIDTH, HELP_TEXT_VIEW_HEIGHT)];
    self.textView.hidden = YES;
    self.textView.backgroundColor = UIColorFromRGBA(0xFFFFFF, 0.0);
    [self.textView.layer setCornerRadius:3.0f];
    [self.textView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.textView.layer setBorderWidth:0];
    [self.textView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.textView.layer setShadowOpacity:0.8];
    [self.textView.layer setShadowRadius:3.0];
    [self.textView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [self addSubview:self.textView];
    
    self.helpText = [[UILabel alloc] initWithFrame:CGRectInset(self.textView.bounds, 10, 10)];
    self.helpText.backgroundColor = [UIColor clearColor];
    self.helpText.font = [UIFont fontWithName:@"MarkerFelt-Thin" size:13.0f];
    self.helpText.numberOfLines = 0;
    self.helpText.lineBreakMode = UILineBreakModeWordWrap;
    self.helpText.textAlignment = NSTextAlignmentCenter;
    self.helpText.textColor = UIColorFromRGBA(0xFFFFFF, 0.8);
    self.helpText.shadowColor = [UIColor blackColor];
    self.helpText.shadowOffset = CGSizeMake(0, -1.0);
    [self.textView addSubview:self.helpText];
    
    self.arrow = [[ArrowView alloc] init];
    [self addSubview:self.arrow];
    
    CGRect buttonFrame = CGRectMake(0, 0, HELP_BUTTON_SIZE, HELP_BUTTON_SIZE);
    buttonFrame.origin.x = self.bounds.size.width - buttonFrame.size.width;
    buttonFrame.origin.y = self.bounds.size.height - buttonFrame.size.height;
    
    self.helpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.helpButton setImage:[UIImage imageNamed:@"question_mark.png"] forState:UIControlStateNormal];
    [self.helpButton setImage:[UIImage imageNamed:@"question_mark.png"] forState:UIControlStateSelected];
    //[self.helpButton addTarget:self action:@selector(onHelpButtonClick) forControlEvents:UIControlEventTouchUpInside];
    self.helpButton.userInteractionEnabled = NO;
    [self.helpButton setTitle:@"Create" forState:UIControlStateNormal];
    [self.helpButton setClipsToBounds:YES];
    [self.helpButton setFrame:buttonFrame];
    self.helpButton.backgroundColor = UIColorFromRGBA(0x0, 0.45);
    [self.helpButton.layer setBorderWidth:1.0];
    [self.helpButton.layer setBorderColor:UIColorFromRGBA(0xFFFFFF, 0.1).CGColor];
    [Util drawFastShadow:self.helpButton];
    [self.helpButton.layer setShadowOffset:CGSizeMake(-2.0, 2.0)];
    [self.helpButton.layer setShadowRadius:3.0];
    [self.helpButton.layer setShadowOpacity:1.0];
    [self addSubview:self.helpButton];
    
    CGRect frame = CGRectMake(0, 0, 280, 200);
    frame = [Util centerRect:frame :[Util getRectCenter:self.holeyView.bounds]];
    frame.origin.y += 15;//to push it below sync buttons for better visibility
    
    self.descrption = [[UILabel alloc] initWithFrame:frame];
    self.descrption.font = [UIFont fontWithName:@"MarkerFelt-Thin" size:13.0f];
    self.descrption.numberOfLines = 0;
    self.descrption.lineBreakMode = UILineBreakModeWordWrap;
    self.descrption.textAlignment = NSTextAlignmentCenter;
    self.descrption.textColor = UIColorFromRGBA(0xFFFFFF, 0.8);
    self.descrption.shadowColor = [UIColor blackColor];
    self.descrption.shadowOffset = CGSizeMake(0, -1.0);
    self.descrption.backgroundColor = [UIColor clearColor];
    self.descrption.text = @"tap on anything to get its description";
    [self.holeyView addSubview:self.descrption];
    self.descrption.alpha = 0.0;
    
    
}

-(BOOL)isActive {
    return self.helpButton.isSelected;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    if(self.helpButton.hidden) {
        return NO;
    }
    
    if(![self isActive]) {
        BOOL rv = CGRectContainsPoint(self.helpButton.frame, point);
        return rv;
    }
    return CGRectContainsPoint(self.bounds, point);
    
}

-(void)onHelpButtonClick {
    if([self isActive]) {
        self.helpButton.selected = NO;
        self.holeyView.hidden = YES;
        self.textView.hidden = YES;
        self.arrow.hidden = YES;
    }
    else {
        self.helpButton.selected = YES;
        [self.holeyView removeHole];
        self.holeyView.hidden = NO;
        [self showDescription];
    }
    
    [Logger track:@"help_button_clicked" :@{@"activated" : @([self isActive])}];
}

-(void)onSwipe {
    [self onHelpButtonClick];
}

-(void)onTap:(UITapGestureRecognizer *)recognizer {
    if(recognizer.state == UIGestureRecognizerStateRecognized) {
        
        CGPoint point = [recognizer locationInView:recognizer.view];
        
        if(!self.helpButton.hidden && CGRectContainsPoint(self.helpButton.frame, point)) {
            [self onHelpButtonClick];
            return;
        }
        
        if(self.descrption.alpha != 0.0) {
            self.descrption.alpha = 0.0;
        }
        
        RootViewController *rvc = [[UIController getInstance] getUIControl:ROOT_VIEW_CONTROLLER];
        UIView *tappedElement = [rvc.view hitTest:point withEvent:nil];
        
        while (!tappedElement.helpText && tappedElement.superview) {
            tappedElement = tappedElement.superview;
        }
        
        if(tappedElement.helpText) {
            
            [Logger track:@"help_shown" :@{@"text" : tappedElement.helpText}];
            
            CGRect tappedElementBox = [tappedElement convertRect:tappedElement.bounds toView:self];
            [self.holeyView setHole:tappedElementBox:tappedElement.getHelpSpotlightShapeOval];
            
            self.textView.hidden = NO;
            self.textView.frame = [self getTextRectForTargetElementRect:tappedElementBox];
            self.helpText.text = tappedElement.helpText;
            
            self.arrow.hidden = NO;
            [self.arrow setRects:tappedElementBox :self.textView.frame];
            [self.arrow setNeedsDisplay];
        }
        else {
            [Logger track:@"help_hidden"];
            
            self.textView.hidden = YES;
            self.arrow.hidden = YES;
            [self.holeyView removeHole];
        }
    }
}

-(CGRect)getTextRectForTargetElementRect:(CGRect)targetElementRect {
    //TODO: improve the logic
    CGRect topHalf, bottonHalf, leftHalf, rightHalf;
    CGRectDivide(self.bounds, &topHalf, &bottonHalf, self.bounds.size.height/2, CGRectMinYEdge);
    CGRectDivide(self.bounds, &leftHalf, &rightHalf, self.bounds.size.width/2, CGRectMinXEdge);
    
    CGFloat topIntersection = [Util getRectArea:CGRectIntersection(targetElementRect, topHalf)];
    CGFloat bottomIntersection = [Util getRectArea:CGRectIntersection(targetElementRect, bottonHalf)];
    CGFloat leftIntersection = [Util getRectArea:CGRectIntersection(targetElementRect, leftHalf)];
    CGFloat rightIntersection = [Util getRectArea:CGRectIntersection(targetElementRect, rightHalf)];
    
    CGFloat minIntersection = MIN(topIntersection, MIN(bottomIntersection, MIN(leftIntersection, rightIntersection)));
    
    CGRect recipient = topHalf;
    if(minIntersection == topIntersection) {
        
    }
    else if(minIntersection == bottomIntersection) {
        recipient = bottonHalf;
    }
    /*else if(minIntersection == leftIntersection) {
        recipient = leftHalf;
    }
    else {
        recipient = rightHalf;
    }*/

    return [Util centerRect:self.textView.bounds :[Util getRectCenter:recipient]];
}

-(void)showDescription {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    self.descrption.alpha = 1.0;
    [UIView commitAnimations];
    
    __block typeof(self) bself = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
        if(!bself) {
            return;
        }
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        bself.descrption.alpha = 0.0;
        [UIView commitAnimations];
        
    });
    
}

@end
