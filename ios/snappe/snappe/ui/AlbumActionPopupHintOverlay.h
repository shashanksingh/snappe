//
//  AlbumActionPopupHintOverlay.h
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import "BaseIntroOverlay.h"

@interface AlbumActionPopupHintOverlay : BaseIntroOverlay
@end

@interface LocalAlbumActionPopupHintOverlay : AlbumActionPopupHintOverlay
@end

@interface RemoteAlbumActionPopupHintOverlay : AlbumActionPopupHintOverlay
@end
