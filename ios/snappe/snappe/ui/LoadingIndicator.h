//
//  LoadingIndicator.h
//  snappe
//
//  Created by Shashank on 23/01/13.
//
//

#import <UIKit/UIKit.h>

@interface LoadingIndicator : UIView
@property BOOL darkBackground;

- (id)init:(BOOL)darkBackground;
@end
