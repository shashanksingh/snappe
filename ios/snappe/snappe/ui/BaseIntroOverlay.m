//
//  BaseIntroOverlay.m
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import "BaseIntroOverlay.h"
#import "HelpOverlay.h"
#import "Macros.h"
#import "ArrowView.h"
#import "Util.h"
#import <QuartzCore/QuartzCore.h>

static BaseIntroOverlay *instance;

@implementation IntroHint
@end

@implementation BaseIntroOverlay

- (id)init:(id)data {
    CGRect frame = [UIScreen mainScreen].bounds;
    self = [super initWithFrame:frame];
    if (self) {
        self.data = data;
        self.backgroundColor = UIColorFromRGBA(0x0, [self getOpacity]);
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)];
        [self addGestureRecognizer:tapRecognizer];
        
        [self draw];
    }
    return self;
}

+(void)show:(id)data {
    if([self alreadyShown]) {
        return;
    }
    
    if(!instance) {
        instance = [[[self class] alloc] init:data];
    }
    
    HelpOverlay *helpOverlay = [HelpOverlay getInstance];
    [helpOverlay.superview insertSubview:instance aboveSubview:helpOverlay];
}

-(NSArray*)getParameters {
    return @[];
}

+(NSString*)getIntroID {
    return @"";
}

-(CGFloat)getOpacity {
    return 0.65;
}

+(NSString*)getUserDefaultsKey {
    return [NSString stringWithFormat:@"intro_overlay_%@_shown", [self getIntroID]];
}

+(BOOL)alreadyShown {
    return [Util getUserDefault:[self getUserDefaultsKey]] != nil;
}

- (void)draw {
    NSArray *introHints = [self getParameters];
    
    for(IntroHint *hint in introHints) {
        UILabel *label = [[UILabel alloc] initWithFrame:hint.labelRect];
        label.text = hint.labelText;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:@"MarkerFelt-Thin" size:13.0f];
        label.numberOfLines = 0;
        label.lineBreakMode = UILineBreakModeWordWrap;
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = UIColorFromRGBA(0xFFFFFF, 0.8);
        [label.layer setShadowOffset:CGSizeMake(-2.0, 2.0)];
        [label.layer setShadowRadius:3.0];
        [label.layer setShadowOpacity:1.0];
        
        [self addSubview:label];
        
        CGRect hintTargetRect = CGRectMake(hint.hintTargetLocation.x, hint.hintTargetLocation.y, 20, 20);
        ArrowView *arrow = [ArrowView new];
        [arrow setRects:label.frame :hintTargetRect];
        [self addSubview:arrow];
    }
}

-(void)onTap {
    NSString *key = [self.class getUserDefaultsKey];
    [Util setUserDefault:@(YES) forKey:key];
    [self removeFromSuperview];
    instance = nil;
}



@end
