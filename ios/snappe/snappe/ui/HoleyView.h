//
//  HoleyView.h
//  snappe
//
//  Created by Shashank on 17/03/13.
//
//

#import <UIKit/UIKit.h>

@interface HoleyView : UIView
-(void)setHole:(CGRect)holeRect:(BOOL)oval;
-(void)removeHole;
@end
