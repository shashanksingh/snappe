
//
//  CreditsCountView.m
//  snappe
//
//  Created by Shashank on 12/01/13.
//
//

#import "CreditsCountView.h"
#import "EventManager.h"
#import "CreditsManager.h"
#import "Util.h"
#import "FBManager.h"
#import "InAppPurchaseView.h"
#import "UIView+Help.m"
#import <QuartzCore/QuartzCore.h>

#define PADDING 2
#define CREDIT_ICON_SECTION_WIDTH 32
#define CREDIT_ICON_SIZE 22

@interface CreditsCountView()
@property (strong) UILabel *countText;
@end

@implementation CreditsCountView
@synthesize countText;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self drawOnFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self setupEvents];
        
        [self updateCount:[CreditsManager getCreditsCount]];
        
        if(![[FBManager getInstance] isAuthorized]) {
            [self setHidden:YES];
        }
        
        [self setHelpText:@"see how you can enhance your Snappe experience!"];
    }
    return self;
}

-(void)drawOnFrame:(CGRect)frame {
    CGRect creditIconSectionFrame, countTextFrame;
    
    CGRect r = CGRectInset(frame, PADDING, PADDING);
    CGRectDivide(r, &creditIconSectionFrame, &countTextFrame, CREDIT_ICON_SECTION_WIDTH, CGRectMinXEdge);
    
    
    CGRect creditIconFrame = CGRectMake(0, 0, CREDIT_ICON_SIZE, CREDIT_ICON_SIZE);
    creditIconFrame = [Util centerRect:creditIconFrame :[Util getRectCenter:creditIconSectionFrame]];
    UIImageView *creditIcon = [[UIImageView alloc] initWithFrame:creditIconFrame];
    creditIcon.image = [UIImage imageNamed:@"credit_icon.png"];
    [self addSubview:creditIcon];
    
    self.countText = [[UILabel alloc] initWithFrame:CGRectInset(countTextFrame, PADDING, PADDING)];
    self.countText.backgroundColor = [UIColor clearColor];
    self.countText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
    self.countText.textColor = UIColorFromRGBA(0xFFFFFF, 1);
    [self.countText setAdjustsFontSizeToFitWidth:YES];
    self.countText.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.countText];
    
    self.backgroundColor = UIColorFromRGBA(0x0, 0.45);
    [self.layer setBorderWidth:1.0];
    [self.layer setBorderColor:UIColorFromRGBA(0xFFFFFF, 0.1).CGColor];
    [Util drawFastShadow:self];
    [self.layer setShadowOffset:CGSizeMake(-2.0, 2.0)];
    [self.layer setShadowRadius:3.0];
    [self.layer setShadowOpacity:0.0];
    
}

-(void)setupEvents {
    [[EventManager getInstance] addEventListener:CREDITS_COUNT_UPDATED :^(va_list args) {
        int count = va_arg(args, int);
        [self updateCount:count];
    }];
    [[EventManager getInstance] addEventListener:FB_AUTHORIZED :^(va_list args) {
        [self setHidden:NO];
    }];
    [[EventManager getInstance] addEventListener:FB_NOT_AUTHORIZED :^(va_list args) {
        [self setHidden:YES];
    }];
    [[EventManager getInstance] addEventListener:FB_AUTH_ERROR :^(va_list args) {
        [self setHidden:YES];
    }];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)];
    tapRecognizer.delegate = self;
    [self addGestureRecognizer:tapRecognizer];
}

-(void)updateCount:(int)count {
    NSString *text = [NSString stringWithFormat:@"%d", count];
    self.countText.textColor = UIColorFromRGBA(0xFFFFFF, 1);
    
    if(count >= [CreditsManager getInfinityCreditsValue]) {
        text = @"\u221E";
        self.countText.font = [UIFont fontWithName:@"TrebuchetMS" size:40];
    }
    else if(count >= 1000 & count < 10000) {
        text = [NSString stringWithFormat:@"%dK", (int)(count/1000)];
        self.countText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
    }
    else if(count >= 100 && count < 1000) {
        self.countText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
    }
    else if(count >= 10 && count < 100) {
        self.countText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
    }
    else {
        self.countText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
        self.countText.textColor = UIColorFromRGBA(0xE62020, 0.9);
    }
    [self.countText setText:text];
}

-(void)onTap {
    DLog(@"show purchase screen");
    [InAppPurchaseView show];
}
@end
