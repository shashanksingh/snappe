//
//  SyncControlView.m
//  snappe
//
//  Created by Shashank on 18/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SyncControlView.h"
#import "CoreController.h"
#import <QuartzCore/QuartzCore.h>
#import "Macros.h"
#import "EventManager.h"
#import "SnappeHTTPService.h"
#import "UserAccessor.h"
#import "SyncManager.h"
#import "FakeAlbum.h"
#import "AlbumShareOperation.h"
#import "AlbumDownloadOperation.h"
#import "Util.h"
#import "Logger.h"
#import "UIView+Glow.h"
#import "AlbumPushOperation.h"
#import "PhotoLibraryReader.h"
#import "ModalSearchView.h"
#import "AlbumSyncOperation.h"
#import "AlbumPullOperation.h"
#import "UIView+Help.h"
#import "ActionConfirmationView.h"


#define BUTTON_DIAMETER 38
#define RIDGE_LENGTH 100
#define RIDGE_HEIGHT 2
#define BUTTON_RIDGE_GAP 5
#define BUTTON_GAP 10
#define SYNC_BUTTON_SIZE 44
#define SYNC_BUTTON_INSET 5
#define SYNC_BUTTON_ROTATION_ANIMATION_NAME @"__sync_rotation_animation"

@interface SyncButton : UIButton
@end

@implementation SyncButton

-(void) setSelected:(BOOL)selected {
    self.layer.borderColor = selected ? [UIColor whiteColor].CGColor : [UIColor grayColor].CGColor;
    [super setSelected:selected];
}

@end

@interface SyncControlView ()
@property (strong) SyncButton *syncInButton;
@property (strong) SyncButton *syncOutButton;
@property (strong) SyncButton *syncAlbumsButton;
@property (strong) UIButton *syncGraphButton;
@property (strong) UIButton *searchButton;
@property (strong) CoreController *coreController;
@property (strong) Album *currentOwnAlbum;
@property (strong) id currentFriendAlbum;
@end

@implementation SyncControlView
@synthesize syncInButton;
@synthesize syncOutButton;
@synthesize syncGraphButton;
@synthesize coreController;
@synthesize currentOwnAlbum;
@synthesize currentFriendAlbum;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.coreController = [CoreController getInstance];
        //self.backgroundColor = [UIColor yellowColor];
        
        float w = frame.size.width;
        float h = frame.size.height;
        
        CGRect searchButtonRect = CGRectMake(0, (h - SYNC_BUTTON_SIZE)/2, SYNC_BUTTON_SIZE, SYNC_BUTTON_SIZE);
        
        CGFloat buttonSectionWidth = BUTTON_DIAMETER + BUTTON_GAP;
        
        CGRect buttonInRect = CGRectMake(w/2 - 1.5 * buttonSectionWidth, h/2 - BUTTON_DIAMETER/2, BUTTON_DIAMETER, BUTTON_DIAMETER);
        CGRect buttonOutRect = CGRectMake(w/2 - 0.5 * buttonSectionWidth, h/2 - BUTTON_DIAMETER/2, BUTTON_DIAMETER, BUTTON_DIAMETER);
        CGRect buttonSyncRect = CGRectMake(w/2 + 0.5 * buttonSectionWidth, h/2 - BUTTON_DIAMETER/2, BUTTON_DIAMETER, BUTTON_DIAMETER);
        
        CGRect leftRidgeRect = CGRectMake(buttonInRect.origin.x - BUTTON_RIDGE_GAP - RIDGE_LENGTH, h/2 - RIDGE_HEIGHT/2, RIDGE_LENGTH, RIDGE_HEIGHT);
        CGRect middleRidgeRect = CGRectMake(w/2 - 5, h/2 - RIDGE_HEIGHT/2, 10, RIDGE_HEIGHT);
        CGRect rightRidgeRect = CGRectMake(buttonSyncRect.origin.x + BUTTON_DIAMETER + BUTTON_RIDGE_GAP, h/2 - RIDGE_HEIGHT/2, RIDGE_LENGTH, RIDGE_HEIGHT);
        CGRect syncButtonRect = CGRectMake(w - SYNC_BUTTON_SIZE, (h - SYNC_BUTTON_SIZE)/2, SYNC_BUTTON_SIZE, SYNC_BUTTON_SIZE);
        
        UIView *leftRidge = [[UIView alloc] initWithFrame:leftRidgeRect];
        [self addSubview:leftRidge];
        [leftRidge setBackgroundColor:UIColorFromRGB(0x808080)];
        
        UIView *middleRidge = [[UIView alloc] initWithFrame:middleRidgeRect];
        [self addSubview:middleRidge];
        [middleRidge setBackgroundColor:UIColorFromRGB(0x808080)];
        
        
        self.searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.searchButton setImage:[UIImage imageNamed:@"search_left.png"] forState:UIControlStateNormal];
        [self.searchButton addTarget:self action:@selector(onFriendsAlbumsSearchClick) forControlEvents:UIControlEventTouchUpInside];
        [self.searchButton setTitle:@"Search" forState:UIControlStateNormal];
        [self.searchButton setClipsToBounds:YES];
        [self.searchButton setFrame:searchButtonRect];
        self.searchButton.backgroundColor = UIColorFromRGBA(0x0, 0.45);
        [self.searchButton.layer setBorderWidth:1.0];
        [self.searchButton.layer setBorderColor:UIColorFromRGBA(0xFFFFFF, 0.1).CGColor];
        [Util drawFastShadow:self.searchButton];
        [self.searchButton.layer setShadowOffset:CGSizeMake(-2.0, 2.0)];
        [self.searchButton.layer setShadowRadius:3.0];
        [self.searchButton.layer setShadowOpacity:0.0];
        [self addSubview:self.searchButton];
        
        [self.searchButton setHelpText:@"tap to search in all albums"];
        [self.searchButton setHelpSpotlightShapeOval:YES];
        
        self.syncInButton = [self drawAlbumSyncButton:@[@"sync_in_icon.png", @"onSyncInButtonClicked", [NSValue valueWithCGRect:buttonInRect]]];
        UIView *wrapper = [Util wrapUIView:self.syncInButton];
        wrapper.userInteractionEnabled = YES;
        [wrapper setHelpSpotlightShapeOval:YES];
        [wrapper setHelpText:@"pull the remote album into the local album"];
        
        self.syncOutButton = [self drawAlbumSyncButton:@[@"sync_out_icon.png", @"onSyncOutButtonClicked", [NSValue valueWithCGRect:buttonOutRect]]];
        wrapper = [Util wrapUIView:self.syncOutButton];
        wrapper.userInteractionEnabled = YES;
        [wrapper setHelpSpotlightShapeOval:YES];
        [wrapper setHelpText:@"push the local album to the remote album"];
        
        self.syncAlbumsButton = [self drawAlbumSyncButton:@[@"sync_albums_icon.png", @"onSyncAlbumsButtonClicked", [NSValue valueWithCGRect:buttonSyncRect]]];
        wrapper = [Util wrapUIView:self.syncAlbumsButton];
        wrapper.userInteractionEnabled = YES;
        [wrapper setHelpSpotlightShapeOval:YES];
        [wrapper setHelpText:@"sync the local & the remote albums"];
        
        UIView *rightRidge = [[UIView alloc] initWithFrame:rightRidgeRect];
        [self addSubview:rightRidge];
        [rightRidge setBackgroundColor:UIColorFromRGB(0x808080)];
        
        
        self.syncGraphButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.syncGraphButton addTarget:self action:@selector(onSyncGraphButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.syncGraphButton setTitle:@"sync" forState:UIControlStateNormal];
        [self.syncGraphButton setFrame:syncButtonRect];
        self.syncGraphButton.imageEdgeInsets = UIEdgeInsetsMake(SYNC_BUTTON_INSET, SYNC_BUTTON_INSET, SYNC_BUTTON_INSET, SYNC_BUTTON_INSET);
        self.syncGraphButton.backgroundColor = UIColorFromRGBA(0x0, 0.45);
        [self.syncGraphButton.layer setBorderWidth:1.0];
        [self.syncGraphButton.layer setBorderColor:UIColorFromRGBA(0xFFFFFF, 0.1).CGColor];
        [Util drawFastShadow:self.syncGraphButton];
        [self.syncGraphButton.layer setShadowOffset:CGSizeMake(-2.0, 2.0)];
        [self.syncGraphButton.layer setShadowRadius:3.0];
        [self.syncGraphButton.layer setShadowOpacity:0.0];
        [self addSubview:self.syncGraphButton];
        [self.syncGraphButton setImage:[UIImage imageNamed:@"sync_icon.png"] forState:UIControlStateNormal];
        
        [self.syncGraphButton setHelpText:@"tap to sync with server"];
        [self.syncGraphButton setHelpSpotlightShapeOval:YES];
        
        [self disableButton:self.syncInButton];
        [self disableButton:self.syncOutButton];
        
        [self setupEvents];
        [self updateSyncButtons];
        
    }
    return self;
}

-(SyncButton*)drawAlbumSyncButton:(NSArray*)descriptor {
    NSString *icon = descriptor[0];
    SEL selector = NSSelectorFromString(descriptor[1]);
    CGRect frame = [descriptor[2] CGRectValue];
    
    
    SyncButton *button = [SyncButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:icon] forState:UIControlStateNormal];
    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:frame];
    [button setClipsToBounds:YES];
    [button.layer setBorderWidth:3.0];
    [button.layer setBorderColor:[UIColor grayColor].CGColor];
    [button.layer setCornerRadius:BUTTON_DIAMETER/2];
    [Util drawFastShadow:button];
    [button.layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
    [button.layer setShadowRadius:3.0];
    [button.layer setShadowOpacity:1.0];
    [self addSubview:button];
    
    return button;
}

-(void) setupEvents {
    [[EventManager getInstance] addEventListener:APPLICATION_BECAME_ACTIVE :^(va_list args) {
        if([[PhotoLibraryOrganizer getInstance] isSyncing]) {
            [self startSyncButtonAnimation];
        }
    }];
    
    [[EventManager getInstance] addEventListener:OWN_CAROUSEL_FLIPPED :^(va_list args) {
        id currentAlbum = va_arg(args, id);
        self.currentOwnAlbum = currentAlbum;
        [self updateSyncButtons];
    }];
    [[EventManager getInstance] addEventListener:FRIEND_CAROUSEL_FLIPPED :^(va_list args) {
        id currentAlbum = va_arg(args, id);
        self.currentFriendAlbum = currentAlbum;
        [self updateSyncButtons];
    }];
    [[EventManager getInstance] addEventListener:PHOTO_LIBRARY_ORGANIZING :^(va_list args) {
        [self startSyncButtonAnimation];
    }];
    [[EventManager getInstance] addEventListener:PHOTO_LIBRARY_ORGANIZED :^(va_list args) {
        [self stopSyncButtonAnimation];
    }];
    [[EventManager getInstance] addEventListener:FB_AUTHORIZED :^(va_list args) {
        [self onSyncGraphButtonClicked];
    }];
}

-(void)onFriendsAlbumsSearchClick {
    [Logger track:@"search_button_click"];
    
    if(![Util checkAndAlertAuth:@"search albums by name, owner or place"]) return;
    
    [[EventManager getInstance] fireEvent:MENU_BAR_SHOULD_COLLAPSE, nil];
    [ModalSearchView show:SNAPPE:nil];
}

-(void)updateSyncButtons {
    if(![[FBManager getInstance] isAuthorized] || !self.currentFriendAlbum || !self.currentOwnAlbum) {
        [self disableButton:self.syncInButton];
        [self disableButton:self.syncOutButton];
        [self disableButton:self.syncAlbumsButton];
    }
    else if([self.currentFriendAlbum isKindOfClass:[Album class]]) {
        [self enableButton:self.syncInButton];
        
        Album *album = (Album*)self.currentFriendAlbum;
        if([album currentUserCanUpload]) {
            [self enableButton:self.syncOutButton];
            [self enableButton:self.syncAlbumsButton];
        }
        else {
            [self disableButton:self.syncOutButton];
            [self disableButton:self.syncAlbumsButton];
        }
    }
    else {
        [self enableButton:self.syncOutButton];
        [self disableButton:self.syncInButton];
    }
}

-(void)onSyncOutButtonClicked {
    
    [Logger track:@"sync_out_button_click"];
    
    if(![Util checkAndAlertAuth:@"push local albums to remote albums"]) return;

    Album *src = self.currentOwnAlbum;

    NSString *confrimationMessage = [NSString stringWithFormat:@"Are you sure you want to push '%@' to '%@'?", src.name, ((Album*)currentFriendAlbum).name];
    
    __block typeof (self) bself = self;
    [ActionConfirmationView confirm:@"sync_out_button" :confrimationMessage :^(NSNumber *ok) {
        if(ok.boolValue) {
            Album *dest = (Album*)bself.currentFriendAlbum;
            AlbumPushOperation *op = [[AlbumPushOperation alloc] initWithAlbums:src :dest];
            [Util executeExtOp:op];
        }
    }];

}

-(void)onSyncInButtonClicked {
    [Logger track:@"sync_in_button_click"];
    
    if(![Util checkAndAlertAuth:@"pull remote albums into local albums"]) return;
    
    Album *source = (Album*)self.currentFriendAlbum;
    
    NSString *confrimationMessage = [NSString stringWithFormat:@"Are you sure you want to pull photos from the album '%@' into '%@'?", source.name, self.currentOwnAlbum.name];
    
    __block typeof (self) bself = self;
    [ActionConfirmationView confirm:@"sync_in_button" :confrimationMessage :^(NSNumber *ok) {
        if(ok.boolValue) {
            AlbumPullOperation *op = [[AlbumPullOperation alloc] init:source :bself.currentOwnAlbum];
            [Util executeExtOp:op];
        }
    }];
    
}

-(void)onSyncAlbumsButtonClicked {
    [Logger track:@"sync_albums_button_click"];
    
    if(![Util checkAndAlertAuth:@"sync local albums with remote albums"]) return;
    
    Album *friendAlbum = (Album*)self.currentFriendAlbum;
    Album *ownAlbum = self.currentOwnAlbum;
    
    NSString *confrimationMessage = [NSString stringWithFormat:@"Are you sure you want to sync '%@' with '%@'?",ownAlbum.name, friendAlbum.name];
    
    [ActionConfirmationView confirm:@"album_sync_button" :confrimationMessage :^(NSNumber *ok) {
        if(ok.boolValue) {
            AlbumSyncOperation *op = [[AlbumSyncOperation alloc] init:ownAlbum :friendAlbum: NO];
            [Util executeExtOp:op];
        }
    }];
}

-(void)onSyncGraphButtonClicked {
    BOOL alreadySyncing = [[PhotoLibraryOrganizer getInstance] isSyncing];
    
    [Logger track:@"sync_graph_button_click" :@{@"already_syncing" : @(alreadySyncing)}];
    
    if(alreadySyncing) {
        return;
    }
    
    //TODO: do we need this? can we ever miss a mod to
    //local lib making our cache bad?
    [PhotoLibraryReader invalidateModificationCache];
    
    [[SnappeHTTPService getInstance] invalidateFBGraph];
    
    [[PhotoLibraryOrganizer getInstance] sync:^{
        [Logger track:@"photo_library_synced"];
    } :^(NSError *error) {
        [self.syncGraphButton.imageView stopAnimating];
        [Logger logError:@"error in syncing" :error];
        [Util alertError:error :@"There was an unexpected error in syncing. Please try again later."];
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)disableButton:(UIButton*)button {
    button.backgroundColor = [UIColor grayColor];
    button.alpha = 0.7;
    button.enabled = NO;
}

-(void)enableButton:(UIButton*)button {
    button.backgroundColor = [UIColor clearColor];
    button.alpha = 1.0;
    button.enabled = YES;
}

-(void)startSyncButtonAnimation {
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/];
    rotationAnimation.duration = 1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = HUGE_VALF;
    
    [self.syncGraphButton.imageView.layer addAnimation:rotationAnimation forKey:SYNC_BUTTON_ROTATION_ANIMATION_NAME];
}

-(void)stopSyncButtonAnimation {
    [self.syncGraphButton.imageView.layer removeAnimationForKey:SYNC_BUTTON_ROTATION_ANIMATION_NAME];
}

@end
