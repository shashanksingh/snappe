//
//  NotificationViewCell.m
//  snappe
//
//  Created by Shashank on 29/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NotificationViewCell.h"
#import "NotificationAccessor.h"
#import "Macros.h"
#import "Util.h"
#import "RTLabel.h"
#import <QuartzCore/QuartzCore.h>

#define CELL_Y_PADDING 10
#define CELL_X_PADDING 15
#define THUMB_WIDTH 48
#define THUMB_HEIGHT 48
#define BORDER_SIZE 10
#define THUMB_Y_PADDING 8
#define THUMB_X_PADDING 10
#define DESC_X_PADDING 5
#define DESC_Y_PADDING 8
#define FOOTER_Y_PADDING 5
#define FOOTER_X_PADDING 5
#define TITLE_X_PADDING 6
#define TITLE_Y_PADDING 4

@interface NotificationCellView : UIView<RTLabelDelegate>
@property (strong) UILabel *title;
@property (strong) RTLabel *longDesc;
@property (strong) UILabel *footer;
@property (strong) UIView *thumbContainer;
@end

@implementation NotificationCellView

@synthesize title;
@synthesize longDesc;
@synthesize footer;
@synthesize thumbContainer;

- (id)initWithFrame:(CGRect)frame :(Notification*)notification {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        
        [self.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        [Util drawFastShadow:self];
        [self.layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
        [self.layer setShadowRadius:3.0];
        [self.layer setShadowOpacity:1.0];
        
        frame.origin = CGPointMake(0, 0);
        
        [self drawWithFrame:frame];
        [self update:notification];
    }
    return self;
}

-(void)update:(Notification*)notification {
    if([[notification pending] boolValue]) {
        [self setBackgroundColor:UIColorFromRGBA(0xDAE9FF, 1)];
    }
    else {
        [self setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 1)];
    }
    
    [self.title setText: [notification getTitle]];
    [self.title sizeToFit];
    
    
    [Util removeAllSubviews:self.thumbContainer];
    UIImageView *thumb = [notification getThumbnail:THUMB_WIDTH :THUMB_HEIGHT];
    [self.thumbContainer addSubview:thumb];
    
    NSString *footerText = [notification getStatusText];
    [self.footer setText:footerText];
    
    [self.longDesc setText: [notification getDescription]];
    [self.longDesc sizeToFit];
}

-(void) drawWithFrame:(CGRect)frame {
    CGRect rect = frame;
    
    CGFloat w = rect.size.width;
    CGFloat h = rect.size.height;
    
    CGFloat headerHeight = h * 0.25;
    CGFloat bodyHeight = h * 0.55;
    
    CGRect headerRect, bodyRect, footerRect;
    
    CGRectDivide(rect, &headerRect, &rect, headerHeight, CGRectMinYEdge);
    CGRectDivide(rect, &bodyRect, &rect, bodyHeight, CGRectMinYEdge);
    footerRect = rect;
    
    footerRect = CGRectInset(footerRect, FOOTER_X_PADDING, FOOTER_Y_PADDING);
    
    CGFloat thumbWidth = bodyRect.size.width * 0.30;
    CGRect thumbRect, longDescRect;
    
    CGRectDivide(bodyRect, &thumbRect, &longDescRect, thumbWidth, CGRectMinXEdge);
    
    thumbRect = CGRectInset(thumbRect, THUMB_X_PADDING, THUMB_Y_PADDING);
    longDescRect = CGRectInset(longDescRect, DESC_X_PADDING, DESC_Y_PADDING);
    
    UIView *headerContainer = [[UIView alloc] initWithFrame:headerRect];
    [headerContainer setBackgroundColor:UIColorFromRGB(0x343434)];
    [self addSubview:headerContainer];
    
    title = [[UILabel alloc] initWithFrame:CGRectInset(headerRect, TITLE_X_PADDING, TITLE_Y_PADDING)];
    title.adjustsFontSizeToFitWidth = NO;
    title.lineBreakMode = UILineBreakModeTailTruncation;
    title.textAlignment = UITextAlignmentLeft;
    title.backgroundColor = [UIColor clearColor];
    title.textColor = [UIColor whiteColor];
    
    [title setBackgroundColor:[UIColor clearColor]];
    [title setTextColor:UIColorFromRGB(0xFFFFFF)];
    [title setFont: [UIFont fontWithName:@"TrebuchetMS-Bold" size:12]];
    [headerContainer addSubview:title];
    
    footer = [[UILabel alloc] initWithFrame:footerRect];
    footer.adjustsFontSizeToFitWidth = NO;
    footer.lineBreakMode = UILineBreakModeTailTruncation;
    footer.textAlignment = UITextAlignmentRight;
    footer.backgroundColor = [UIColor clearColor];
    
    [footer setTextColor:UIColorFromRGB(0x676767)];
    [footer setFont: [UIFont fontWithName:@"Arial-ItalicMT" size:9]];
    [self addSubview:footer];
    
    self.thumbContainer = [[UIView alloc] initWithFrame:thumbRect];
    [self.thumbContainer.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self addSubview:self.thumbContainer];
    
    
    longDesc = [[RTLabel alloc] initWithFrame:longDescRect];
    longDesc.delegate = self;
    longDesc.linkAttributes = longDesc.selectedLinkAttributes = @{@"underline": @"1", @"style":@"italic"};
    longDesc.lineBreakMode = UILineBreakModeWordWrap;
    longDesc.backgroundColor = [UIColor clearColor];
    
    [longDesc setTextColor:UIColorFromRGB(0x0)];
    [longDesc setFont: [UIFont fontWithName:@"TrebuchetMS" size:11]];
    [self addSubview:longDesc];
}

- (void)rtLabel:(id)rtLabel didSelectLinkWithURL:(NSURL*)url
{
	[[UIApplication sharedApplication] openURL:url];
}

@end


@interface NotificationViewCell()
@property (strong) NotificationCellView *cell;
@end 

@implementation NotificationViewCell
@synthesize cell;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier :(Notification*)notification :(CGFloat)height
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self setBackgroundColor:[UIColor clearColor]];
        
        //cell frame height is always set to 44, need to set it manually
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, height);
        CGRect contentRect = CGRectInset(self.frame, CELL_X_PADDING, CELL_Y_PADDING);
        
        self.cell = [[NotificationCellView alloc] initWithFrame:contentRect :notification];
        [self.contentView addSubview:self.cell];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)update:(Notification *)notification {
    [self.cell update:notification];
}
@end



