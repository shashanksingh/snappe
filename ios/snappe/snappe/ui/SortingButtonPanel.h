//
//  SortingButtonPanel.h
//  snappe
//
//  Created by Shashank on 27/01/13.
//
//

#import <UIKit/UIKit.h>

@protocol SortingDelegate<NSObject>

@required
-(void)onSmartSortClick;
-(void)onDateSortClick;
-(void)onAlphabeticalSortClick;
@end

@interface SortingButtonPanel : UIView
@property NSObject<SortingDelegate> *delegate;
@end
