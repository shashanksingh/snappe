//
//  SnappeTextField.m
//  snappe
//
//  Created by Shashank on 21/02/13.
//
//
#import "SnappeTextField.h"

@implementation SnappeTextField

- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect inset = CGRectMake(bounds.origin.x + 8, bounds.origin.y, bounds.size.width - 8, bounds.size.height);
    return inset;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    CGRect inset = CGRectMake(bounds.origin.x + 8, bounds.origin.y, bounds.size.width - 8, bounds.size.height);
    return inset;
}
@end
