//
//  SearchResultViewCell.h
//  snappe
//
//  Created by Shashank on 18/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModalSearchView.h"

@interface SearchResultViewCell : UITableViewCell
@property (retain) NSMutableDictionary *context;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier:(CGFloat)width:(CGFloat)height;
-(void)update:(SearchType)searchType:(NSDictionary*)context;
@end
