//
//  WelcomeView.m
//  snappe
//
//  Created by Shashank on 28/04/13.
//
//

#import "WelcomeView.h"
#import "Util.h"
#import "UserAccessor.h"
#import "RTLabel.h"
#import "AlbumCreatorView.h"
#import "OwnAlbumsCarouselView.h"
#import "FriendsAlbumsView.h"
#import "UIController.h"
#import "AlbumView.h"
#import "Logger.h"
#import <QuartzCore/QuartzCore.h>

#define MAIN_WIDTH 280
#define MAIN_PADDING 30

#define HEADER_SECTION_HEIGHT 30
#define HEADER_PADDING 5

#define MAIN_MESSAGE_SECTION_HEIGHT 60
#define MAIN_MESSAGE_PADDING 5

#define SUGGESTION_SECTION_HEIGHT_BIG 80
#define SUGGESTION_SECTION_HEIGHT_SMALL 80
#define SUGGESTION_SECTION_PADDING 10
#define BROWSE_ALL_SUGGESTION_SECTION_HEIGHT 20
#define THUMB_SIZE_BIG 60
#define THUMB_SIZE_SMALL 60
#define DESCRIPTION_PADDING 10

#define WELCOME_VIEW_SHOWN_PREF_KEY @"welcome_view_shown"

@interface WelcomeView()
@property Album *mostShareworthyLocalAlbum;
@property Album *mostShareworthyFacebookAlbum;
@end

@implementation WelcomeView

- (id)init {
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75f]];
        [self addTapHandler:@selector(onTap) :self];
        [self draw];
    }
    return self;
}

+(WelcomeView*) getInstance {
    static WelcomeView *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[WelcomeView alloc] init];
    });
    return sharedInstance;
}


+(void)show {
    WelcomeView *view = [self getInstance];
    [Util showModalView:view];
    [Logger track:@"welcome_view_shown"];
}

-(void)draw {
    CGRect mainRect = CGRectMake(0, 0, MAIN_WIDTH + 2 * MAIN_PADDING, self.bounds.size.height);
    mainRect = CGRectInset(mainRect, MAIN_PADDING, 0);
    CGRect rect = CGRectMake(0, 0, mainRect.size.width, mainRect.size.height);
    
    NSMutableArray *itemsToAdd = [NSMutableArray array];
    
    CGRect headerRect, mainMessageRect;
    //CGRectDivide(rect, &headerRect, &rect, HEADER_SECTION_HEIGHT, CGRectMinYEdge);
    CGRectDivide(rect, &mainMessageRect, &rect, MAIN_MESSAGE_SECTION_HEIGHT, CGRectMinYEdge);
    
    UIView *headerView = [[UIView alloc] initWithFrame:headerRect];
    [headerView setBackgroundColor:UIColorFromRGBA(0x343434, 0.9)];
    //[itemsToAdd addObject:headerView];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectInset(headerView.bounds, HEADER_PADDING, HEADER_PADDING)];
    headerLabel.backgroundColor = [UIColor clearColor];
    [headerView addSubview:headerLabel];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    [headerLabel setTextColor:UIColorFromRGBA(0xFFFFFF, 0.9)];
    [headerLabel setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:13]];
    [headerLabel setText:@"Welcome!"];
    
    UIView *mainMessageView = [[UIView alloc] initWithFrame:mainMessageRect];
    mainMessageView.backgroundColor = UIColorFromRGBA(0x343434, 0.9);
    [itemsToAdd addObject:mainMessageView];
    
    UILabel *mainMessageLabel = [[UILabel alloc] initWithFrame:CGRectInset(mainMessageView.bounds, MAIN_MESSAGE_PADDING, MAIN_MESSAGE_PADDING)];
    mainMessageLabel.backgroundColor = [UIColor clearColor];
    mainMessageLabel.lineBreakMode = UILineBreakModeWordWrap;
    mainMessageLabel.numberOfLines = 0;
    mainMessageLabel.adjustsFontSizeToFitWidth = YES;
    [mainMessageView addSubview:mainMessageLabel];
    
    mainMessageLabel.textAlignment = NSTextAlignmentCenter;
    mainMessageLabel.textColor = UIColorFromRGBA(0xCCCCCC, 0.9);
    mainMessageLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:11];
    
    if(![self isShownBefore]) {
        mainMessageLabel.text = [NSString stringWithFormat:@"Congratulations %@! Your Snappe is all set. Choose  below to get started", [User getCurrentUser].firstName];
    }
    else {
        mainMessageLabel.text = [NSString stringWithFormat:@"Welcome back %@! Choose an action below to get started", [User getCurrentUser].firstName];
    }
    
    for(NSNumber *num in @[@(0), @(1)]) {
        Album *album = nil;
        if(num.intValue == 0) {
            album = self.mostShareworthyLocalAlbum = [Album getOwnLatestDeviceAlbumWithPhotos];
        }
        else {
            album = self.mostShareworthyFacebookAlbum = [Album getOwnLatestFacebookAlbumWithPhotos];
        }
    
        if(album) {
            CGRect shareSectionRect;
            CGRectDivide(rect, &shareSectionRect, &rect, SUGGESTION_SECTION_HEIGHT_BIG, CGRectMinYEdge);
            
            UIView *shareView = [[UIView alloc] initWithFrame:shareSectionRect];
            [itemsToAdd addObject:shareView];
            shareView.backgroundColor = num.intValue == 0 ? UIColorFromRGB(0xF9F9F9) : UIColorFromRGB(0xFFFFFF);
            
            
            CGRect rowRect = CGRectInset(shareView.bounds, SUGGESTION_SECTION_PADDING, SUGGESTION_SECTION_PADDING);
            
            CGRect leftR, rightR, descriptionR, browseSuggestionR;
            CGRectDivide(rowRect, &leftR, &rightR, THUMB_SIZE_BIG, CGRectMinXEdge);
            CGRectDivide(rightR, &browseSuggestionR, &descriptionR, BROWSE_ALL_SUGGESTION_SECTION_HEIGHT, CGRectMaxYEdge);
            
            UIImageView *albumCover = [album getCoverPhoto:THUMB_SIZE_BIG :THUMB_SIZE_BIG :YES];
            albumCover.frame = [Util centerRect:albumCover.bounds :[Util getRectCenter:leftR]];
            albumCover.layer.borderColor = (num.intValue == 1 ? UIColorFromRGB(0xEFEFEF) : UIColorFromRGB(0xE7E7E7)).CGColor;
            albumCover.layer.borderWidth = 4.0f;
            [shareView addSubview:albumCover];
            
            CGRect descriptionFrame = CGRectInset(descriptionR, DESCRIPTION_PADDING, 0);
            UILabel *descriptionLabel = [[UILabel alloc] init];
            descriptionLabel.frame = [Util centerRect:descriptionFrame :[Util getRectCenter:descriptionR]];
            descriptionLabel.font = [UIFont fontWithName:@"NotoSerif" size:10];
            descriptionLabel.backgroundColor = [UIColor clearColor];
            descriptionLabel.lineBreakMode = UILineBreakModeWordWrap;
            descriptionLabel.numberOfLines = 0;
            descriptionLabel.textColor = UIColorFromRGBA(0x666666, 1.0);
            descriptionLabel.adjustsFontSizeToFitWidth = YES;
            
            [shareView addSubview:descriptionLabel];
            
            UIView *browseSuggestionView = [[UIView alloc] initWithFrame:CGRectInset(browseSuggestionR, DESCRIPTION_PADDING, 0)];
            [shareView addSubview:browseSuggestionView];

            RTLabel *browseSuggestionLabel = [[RTLabel alloc] initWithFrame:browseSuggestionView.bounds];
            browseSuggestionLabel.font = [UIFont fontWithName:@"NotoSerif" size:10];
            [browseSuggestionView addSubview:browseSuggestionLabel];
            
            NSString *albumName = [Util ellipsify:album.name:30];
            if(num.intValue == 0) {
                descriptionLabel.text = [NSString stringWithFormat:@"Share '%@' privately with friends, let them upload", albumName];
                browseSuggestionLabel.text = @"or, <u><i>browse all</i></u>";
                
                [self addTapHandler:@selector(onShareLocalAlbumTap) :shareView];
                [self addTapHandler:@selector(onBrowseLocalCarousel) :browseSuggestionLabel];
                
            }
            else {
                descriptionLabel.text = [NSString stringWithFormat:@"Select friends to add as uploaders to your Facebook album '%@'", albumName];
                browseSuggestionLabel.text = @"or, <u><i>browse all</i></u>";
                
                [self addTapHandler:@selector(onShareFacebookAlbumTap) :shareView];
                [self addTapHandler:@selector(onBrowseRemoteCarousel) :browseSuggestionLabel];
            }
            
            [self addSeparator:shareView];
            
        }
    }
    
    for(NSNumber *num in @[@(0), @(1)]) {
        CGRect createNewAlbumRect;
        CGRectDivide(rect, &createNewAlbumRect, &rect, SUGGESTION_SECTION_HEIGHT_SMALL, CGRectMinYEdge);
        
        
        UIView *createNewDeviceAlbumSuggestionView = [[UIView alloc] initWithFrame:createNewAlbumRect];
        [itemsToAdd addObject:createNewDeviceAlbumSuggestionView];
        createNewDeviceAlbumSuggestionView.backgroundColor = num.intValue == 0 ? UIColorFromRGB(0xF5F5F5) : UIColorFromRGB(0xFFFFFF);
        
        CGRect rowRect = CGRectInset(createNewDeviceAlbumSuggestionView.bounds, SUGGESTION_SECTION_PADDING, SUGGESTION_SECTION_PADDING);
        CGRect thumbR, descriptionR;
        
        CGRectDivide(rowRect, &thumbR, &descriptionR, THUMB_SIZE_SMALL, CGRectMinXEdge);
        
        NSString *iconName = num.intValue == 0 ? @"iphone_icon.png" : @"fb_circular_icon_glossy.png";
        UIImageView *thumb = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconName]];
        thumb.contentMode = UIViewContentModeCenter;
        thumb.frame = [Util centerRect:CGRectMake(0, 0, THUMB_SIZE_SMALL, THUMB_SIZE_SMALL) :[Util getRectCenter:thumbR]];;
        [createNewDeviceAlbumSuggestionView addSubview:thumb];
        
        CGRect descriptionFrame = CGRectInset(descriptionR, DESCRIPTION_PADDING, DESCRIPTION_PADDING);
        UILabel *descriptionLabel = [[UILabel alloc] init];
        descriptionLabel.frame = [Util centerRect:descriptionFrame :[Util getRectCenter:descriptionR]];
        descriptionLabel.font = [UIFont fontWithName:@"NotoSerif" size:10];
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.textColor = UIColorFromRGBA(0x444444, 1.0);
        [createNewDeviceAlbumSuggestionView addSubview:descriptionLabel];
        
        
        if(num.intValue == 0) {
            descriptionLabel.text = @"Create a new album on this iPhone";
            [self addTapHandler:@selector(onCreateDeviceAlbumTap) :createNewDeviceAlbumSuggestionView];
        }
        else {
            descriptionLabel.text = @"Create a new Facebook Album";
            [self addTapHandler:@selector(onCreateFacebookAlbumTap) :createNewDeviceAlbumSuggestionView];
        }
        
        [self addSeparator:createNewDeviceAlbumSuggestionView];
    }
    
    CGFloat totalHeight = 0.0f;
    for(UIView *itemToAdd in itemsToAdd) {
        totalHeight += itemToAdd.bounds.size.height;
    }

    mainRect.size.height = totalHeight;
    mainRect = [Util centerRect:mainRect :[Util getRectCenter:self.bounds]];
    mainRect.origin.y = MAIN_PADDING;
    UIView *container = [[UIView alloc] initWithFrame:mainRect];
    for(UIView *itemToAdd in itemsToAdd) {
        [container addSubview:itemToAdd];
    }
    
    UIColor *bgColor = UIColorFromRGB(0xFFFFFF);
    container.backgroundColor = bgColor;
    [container.layer setBorderColor: bgColor.CGColor];
    [container.layer setBorderWidth:0];
    [Util drawFastShadow:container];
    [container.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [container.layer setShadowRadius:3.0];
    [container.layer setShadowOpacity:0.9];
    [self addSubview:container];
}

-(void)addSeparator:(UIView*)parent {
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, parent.bounds.size.width, 1)];
    separator.backgroundColor = UIColorFromRGB(0xCECECE);
    [parent addSubview:separator];
}

-(BOOL)isShownBefore {
    return [Util getUserDefault:WELCOME_VIEW_SHOWN_PREF_KEY] != nil;
}

-(void)addTapHandler:(SEL)selector:(UIView*)view {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:selector];
    [view addGestureRecognizer:tapRecognizer];
}

-(void)onTap {
    [self hide];
    [Logger track:@"welcome_view_action" :@{@"action": @"tap_hide"}];
}

-(void)onShareLocalAlbumTap {
    OwnAlbumsCarouselView *localView = [[UIController getInstance] getUIControl:OWN_ALBUM_CAROUSEL_VIEW];
    AlbumView *albumView = (AlbumView*)[localView.carousel scrollToUserOrAlbum:self.mostShareworthyLocalAlbum];
    
    [localView.carousel fakeScrollToAlbum:self.mostShareworthyLocalAlbum :1];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
        if(albumView) {
            [albumView showShareView];
        }
    });
    
    [self hide];
    [Logger track:@"welcome_view_action" :@{@"action": @"share_local_album", @"aid": albumView.album.id}];
}

-(void)onShareFacebookAlbumTap {
    FriendsAlbumsView *remoteView = [[UIController getInstance] getUIControl:FRIEND_ALBUM_CAROUSEL_VIEW];
    AlbumView *albumView = (AlbumView*)[remoteView.carousel scrollToUserOrAlbum:self.mostShareworthyFacebookAlbum];
    
    [remoteView.carousel fakeScrollToAlbum:self.mostShareworthyFacebookAlbum:1];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
        if(albumView) {
            [albumView showShareView];
        }
    });
    
    [self hide];
    [Logger track:@"welcome_view_action" :@{@"action": @"share_fb_album", @"aid": albumView.album.id}];
}

-(void)onBrowseLocalCarousel {
    OwnAlbumsCarouselView *localView = [[UIController getInstance] getUIControl:OWN_ALBUM_CAROUSEL_VIEW];
    [localView.carousel fakeScrollToAlbum:nil:1];
    [self hide];
    [Logger track:@"welcome_view_action" :@{@"action": @"browse_local"}];
}

-(void)onBrowseRemoteCarousel {
    FriendsAlbumsView *remoteView = [[UIController getInstance] getUIControl:FRIEND_ALBUM_CAROUSEL_VIEW];
    [remoteView.carousel fakeScrollToAlbum:nil:1];
    [self hide];
    [Logger track:@"welcome_view_action" :@{@"action": @"browse_remote"}];
}

-(void)onCreateDeviceAlbumTap {
    [AlbumCreatorView show];
    [self hide];
    [Logger track:@"welcome_view_action" :@{@"action": @"create_local_album"}];
}

-(void)onCreateFacebookAlbumTap {
    [AlbumCreatorView show:YES];
    [self hide];
    [Logger track:@"welcome_view_action" :@{@"action": @"create_fb_album"}];
}

-(void)hide {
    [Util setUserDefault:@(YES) forKey:WELCOME_VIEW_SHOWN_PREF_KEY];
    [self removeFromSuperview];
}

@end
