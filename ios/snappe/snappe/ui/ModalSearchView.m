//
//  ModalSearchView.m
//  snappe
//
//  Created by Shashank on 18/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ModalSearchView.h"
#import "SearchTableViewController.h"
#import "EventManager.h"
#import "Util.h"
#import "BButton.h"
#import "Logger.h"

#define SEARCH_BOX_TOP_INSET 20
#define SEARCH_BOX_SIDE_INSET 20
#define SEARCH_BOX_HEIGHT 40
#define DO_BUTTON_HEIGHT 38
#define DO_BUTTON_WIDTH 100
#define DO_BUTTON_BOTTOM_MARGIN 20
#define BUTTON_GAP 20

static ModalSearchView *instance = nil;

@implementation CurrentSearchQueryAsResult
@end

@implementation DefaultSearchSuggestion
@end

@interface ModalSearchView()
@property (strong) SearchTableViewController *searchView;
@property (strong) UIButton *doButton;
@property (strong) UIButton *cancelButton;
@end

@implementation ModalSearchView
@synthesize searchView;

- (id)init {
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColorFromRGB(0x0) colorWithAlphaComponent:0.75f]];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)];
        tapRecognizer.delegate = self;
        [self addGestureRecognizer:tapRecognizer];
        
        CGRect searchViewRect = CGRectMake(SEARCH_BOX_SIDE_INSET, SEARCH_BOX_TOP_INSET, self.frame.size.width - 2 * SEARCH_BOX_SIDE_INSET, SEARCH_BOX_HEIGHT);
        
        self.searchView = [[SearchTableViewController alloc] initWithStyle:UITableViewStylePlain];
        [self.searchView.view setFrame:searchViewRect];
        
        
        CGRect doButtonRect = CGRectMake(0, 0, DO_BUTTON_WIDTH, DO_BUTTON_HEIGHT);
        doButtonRect.origin.y = frame.size.height - DO_BUTTON_BOTTOM_MARGIN - DO_BUTTON_HEIGHT;
        doButtonRect.origin.x = frame.size.width/2 + BUTTON_GAP/2;
        self.doButton = [[BButton alloc] initWithFrame:doButtonRect type:BButtonTypeSuccess];
        
        CGRect cancelButtonRect = doButtonRect;
        cancelButtonRect.origin.x = frame.size.width/2 - BUTTON_GAP/2 - cancelButtonRect.size.width;
        self.cancelButton = [[BButton alloc] initWithFrame:cancelButtonRect type:BButtonTypeGray];
        
        [self addSubview:self.doButton];
        [self.doButton setTitle:@"Next" forState:UIControlStateNormal];
        [self.doButton addTarget:self action:@selector(onDoButtonClick) forControlEvents:UIControlEventTouchUpInside];
        self.doButton.hidden = YES;
        
        [self addSubview:self.cancelButton];
        [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [self.cancelButton addTarget:self action:@selector(onCanelButtonClick) forControlEvents:UIControlEventTouchUpInside];
        self.cancelButton.hidden = YES;
        
        [Util showPopup:self :self.searchView.view :YES :nil];
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:SEARCH_OVERLAY_SHOULD_HIDE :YES :^(va_list args) {
            [bself endEditing:NO];
            [bself removeFromSuperview];
            instance = nil;
        }];
        
        
    }
    return self;
}

+(ModalSearchView *) getInstance {
    @synchronized(self) {
        if(!instance) {
            instance = [[self alloc] init];
        }
    }
    return instance;
}


+(void)show:(SearchType)searchType:(NSDictionary*)context {
    ModalSearchView *searchView = [self getInstance];
    if(!context) {
        context = @{};
    }
    [searchView setSearchType:searchType :context];
    
    if(searchType == FRIEND || searchType == DEVICE) {
        searchView.doButton.hidden = NO;
        searchView.cancelButton.hidden = NO;
    }
    
    [Util showModalView:searchView];
    [Logger track:@"modal_search_view_shown" :@{@"type" : [self searchTypeToString:searchType]}];
}

-(void)setSearchType:(SearchType)searchType:(NSDictionary*)context {
    [self.searchView update:searchType :context];
    [Logger track:@"modal_search_view_updated" :@{@"type" : [self.class searchTypeToString:searchType]}];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return [touch.view isEqual:self];
}

-(void)onTap {
    //hide on tap applies only when the next/cancel buttons are not shown
    if(self.doButton.hidden) {
        [Logger track:@"modal_search_view_hidden_on_tap"];
        [self hide];
    }
}

-(void)hide {
    [[EventManager getInstance]fireEvent:SEARCH_DISMISSED];
    [[EventManager getInstance]fireEvent:SEARCH_OVERLAY_SHOULD_HIDE];
}

-(void)onDoButtonClick {
    [Logger track:@"modal_search_view_do_button_click"];
    [self.searchView processChanges];
    [self hide];
}

-(void)onCanelButtonClick {
    [Logger track:@"modal_search_view_cancel_button_click"];
    [self hide];
}

+(NSString*)searchTypeToString:(SearchType)type {
    if(type == SNAPPE) {
        return @"SNAPPE";
    }
    if(type == DEVICE) {
        return @"DEVICE";
    }
    if(type == FRIEND) {
        return @"FRIEND";
    }
    if(type == ALBUM) {
        return @"ALBUM";
    }
    if(type == FB_ALBUM) {
        return @"FB_ALBUM";
    }
    return @"UNKNOWN";
}

@end
