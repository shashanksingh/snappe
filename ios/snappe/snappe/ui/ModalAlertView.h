//
//  ModalAlertView.h
//  snappe
//
//  Created by Shashank on 16/12/12.
//
//

#import <UIKit/UIKit.h>

@interface ModalAlertView : UIView<UIGestureRecognizerDelegate>
+(void)showSuccess:(NSString*)message;
+(void)showError:(NSString*)message;
+(void)showBusy:(NSString*)message;
+(void)hideErrorAlert;
+(void)hideBusyAlert;
@end
