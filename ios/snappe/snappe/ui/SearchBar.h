//
//  SearchBar.h
//  snappe
//
//  Created by Shashank on 14/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchBar : UISearchBar
- (void)showCancelButton;
- (void)hideCancelButton;
-(void)setReturnKeyType:(UIReturnKeyType)type;
@end
