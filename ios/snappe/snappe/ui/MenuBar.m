//
//  CarouselMenuBar.m
//  snappe
//
//  Created by Shashank on 12/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MenuBar.h"
#import "Macros.h"
#import "Util.h"
#import <QuartzCore/QuartzCore.h>
#import "EventManager.h"

#define EXPAND_ANIMATION_DURATION 0.45
#define EXPAND_ANIMATION_DELAY 0.225
#define SHOW_ANIMATION_DURATION 0.025
#define SHOW_ANIMATION_DELAY 0.01250

@interface MenuBar()
@property BOOL collapsed;
@property BOOL animationRunning;
@property (strong) NSMutableArray *sections;
@property Alignment alignment;
@property BOOL centered;
@property CGRect collapsedFrame;
@end

@implementation MenuBar
@synthesize collapsed;
@synthesize animationRunning;
@synthesize sections;
@synthesize alignment;
@synthesize centered;
@synthesize collapsedFrame;

- (id)initWithFrame:(CGRect)frame :(Alignment)alignment :(BOOL)centered
{
    self = [super initWithFrame:frame];
    if (self) {
        self.collapsedFrame = frame;
        self.alignment = alignment;
        self.centered = centered;
        
        self.collapsed = YES;
        self.animationRunning = NO;
        self.sections = [[NSMutableArray alloc] init];
        
        UIColor *color = UIColorFromRGBA(0x0, 0.45);
        [self setBackgroundColor:color];
    }
    return self;
}

-(void)addSection:(UIView*)section :(CGFloat)size {
    BOOL horizontal = self.alignment == LEFT || self.alignment == RIGHT;

    CGFloat width = horizontal ? size : self.collapsedFrame.size.width;
    CGFloat height = horizontal ? self.collapsedFrame.size.height : size;
    section.frame = CGRectMake(0, 0, width, height);
    
    section.hidden = YES;
    section.alpha = 0.0;
    
    [self addSubview:section];
    if(!self.centered || [self.sections count]%2 == 0) {
        [self.sections addObject:section];    
    }
    else {
        [self.sections insertObject:section atIndex:0];
    }
    
    
    if([self.sections count] == 1) {
        [self setDefaultSection:section];
    }
}

-(void)setDefaultSection:(UIView*)section {
    [self.sections removeObject:section];
    
    UIView *currentDefault = [self getDefaultSection];
    if(self.collapsed) {
        if(currentDefault) {
            currentDefault.hidden = YES;
            currentDefault.alpha = 0.0;
        }    
    }
    
    [self.sections insertObject:section atIndex:[self getDefaultSectionIndex]];
    section.hidden = NO;
    section.alpha = 1.0;
}

-(UIView*)getDefaultSection {
    NSUInteger sectionCount = [self.sections count];
    if(sectionCount == 0) {
        return nil;
    }
    return [self.sections objectAtIndex:[self getDefaultSectionIndex]];
}

-(NSUInteger)getDefaultSectionIndex {
    if(self.centered) {
        return [self.sections count]/2;
    }
    return 0;
}

-(void)expand {
    if(self.animationRunning) {
        return;
    }
    
    self.animationRunning = YES;
    [UIView animateWithDuration:EXPAND_ANIMATION_DURATION delay:EXPAND_ANIMATION_DELAY options:UIViewAnimationCurveEaseInOut animations:^{
        [self setExpandedFrames];
    } completion:^(BOOL finished) {
        [self showSections:^{
            self.animationRunning = NO;
            self.collapsed = !self.collapsed;
        }];
    }];
}

-(void)collapse {
    if(self.animationRunning) {
        return;
    }
    
    self.animationRunning = YES;
    [self hideSections:^{
        [UIView animateWithDuration:EXPAND_ANIMATION_DURATION delay:EXPAND_ANIMATION_DELAY options:UIViewAnimationCurveEaseInOut animations:^{
            self.frame = self.collapsedFrame;
            [self getDefaultSection].frame = CGRectMake(0, 0, self.collapsedFrame.size.width, self.collapsedFrame.size.height);
        } completion:^(BOOL finished) {
            self.animationRunning = NO;
            self.collapsed = !self.collapsed;
        }];
    }];
}

-(void)toggle {
    if(self.collapsed) {
        [self expand];
    }
    else {
        [self collapse];
    }
}

-(BOOL)isCollapsed {
    return self.collapsed;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    if (CGRectContainsPoint(self.bounds, point)) {
        return YES;
    }
    for(UIView *subview in self.sections) {
        CGPoint relativePoint = [subview convertPoint:point fromView:self];
        if([subview pointInside:relativePoint withEvent:event]) {
            return YES;
        }
    }
    
    return NO;
}


-(void) showSections:(VoidCallback)callback {
    NSTimeInterval eachSectionDuration = SHOW_ANIMATION_DURATION/[self.sections count];
    NSTimeInterval eachSectionDelay = SHOW_ANIMATION_DELAY/[self.sections count];
    
    for(unsigned int i=0; i<[self.sections count]; ++i) {
        UIView *section = [self.sections objectAtIndex:i];
        section.alpha = 0.0f;
        [section setHidden:NO];
        
        [UIView animateWithDuration:eachSectionDuration delay:i * eachSectionDelay options:UIViewAnimationCurveEaseInOut animations:^{
            section.alpha = 1.0f;
        } completion:^(BOOL finished) {
            if(i == [self.sections count] - 1) {
                callback();
            }
        }];
    }
    
}

-(void) hideSections:(VoidCallback)callback {
    NSTimeInterval eachSectionDuration = SHOW_ANIMATION_DURATION/[self.sections count];
    NSTimeInterval eachSectionDelay = SHOW_ANIMATION_DELAY/[self.sections count];
    
    NSUInteger defaultSectionIndex = [self getDefaultSectionIndex];
    for(unsigned int i=0; i<[self.sections count]; ++i) {
        if(i == defaultSectionIndex) {
            continue;
        }
        UIView *section = [self.sections objectAtIndex:i];
        
        [UIView animateWithDuration:eachSectionDuration delay:eachSectionDelay options:UIViewAnimationCurveEaseInOut animations:^{
            section.alpha = 0.0f;
            
        } completion:^(BOOL finished) {
            [section setHidden:YES];
            if(i == [self.sections count] - 1) {
                callback();
            }
        }];
    }
}
     
-(unsigned int)getTotalSize {
    unsigned int total = 0;
    for(unsigned int i=0; i<[self.sections count]; ++i) {
        UIView *section = [self.sections objectAtIndex:i];
        total += (self.alignment == LEFT || self.alignment == RIGHT) ? section.frame.size.width : section.frame.size.height;
    }
    return total;
}

-(CGRect) getExpandedFrame {
    CGRect rv = CGRectInset(self.collapsedFrame, 0, 0);//copy frame
    
    unsigned int size = [self getTotalSize];
    
    if(alignment == LEFT || alignment == RIGHT) {
        rv.size.width = size;
    }
    else {
        rv.size.height = size;
    }
    
    if(self.centered) {
        rv = [Util centerRect:rv :[Util getRectCenter:self.collapsedFrame]];
    }
    else {
        if(alignment == RIGHT) {
            rv.origin.x = CGRectGetMaxX(self.collapsedFrame) - size;
        }
        else if(alignment == BOTTOM) {
            rv.origin.y = CGRectGetMaxY(self.collapsedFrame) - size;
        }
    }
    
    return rv;
}

-(void)setExpandedFrames {
    self.frame = [self getExpandedFrame];
    
    CGFloat currPos = 0;
    if(self.alignment == LEFT) {
        currPos = self.frame.size.width;
    }
    else if(self.alignment == RIGHT) {
        currPos = 0;
    }
    else if(self.alignment == TOP) {
        currPos = self.frame.size.height;
    }
    else {
        currPos = 0;
    }
    
    
    BOOL horizontal = self.alignment == LEFT || self.alignment == RIGHT;
    for(UIView *section in self.sections) {
        CGRect sectionFrame = section.frame;
        CGFloat size = horizontal ? sectionFrame.size.width : sectionFrame.size.height;
        
        if(self.alignment == LEFT) {
            currPos -= size;
            sectionFrame.origin.x = currPos;
        }
        else if(self.alignment == RIGHT) {
            currPos += size;
            sectionFrame.origin.x = currPos;
        }
        else if(self.alignment == TOP) {
            currPos -= size;
            sectionFrame.origin.y = currPos;
        }
        else if(self.alignment == BOTTOM) {
            currPos += size;
            sectionFrame.origin.y = currPos;
        }
        
        section.frame = sectionFrame;
    }
    
}

@end
