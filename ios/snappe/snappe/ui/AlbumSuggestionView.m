
//
//  AlbumDownloadSuggestionView.m
//  snappe
//
//  Created by Shashank on 05/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AlbumSuggestionView.h"
#import <QuartzCore/QuartzCore.h>
#import "Macros.h"
#import "Util.h"
#import "EventManager.h"

#define MAIN_BOX_WIDTH 260
#define MAIN_BOX_BORDER 0
#define MAIN_MESSAGE_HEIGHT 60
#define MAIN_MESSAGE_PADDING 5
#define ALBUM_HEIGHT 64
#define ALBUM_COVER_SECTION_WIDTH 64
#define ALBUM_NAME_SECTION_HEIGHT_FRACTION 0.7
#define ALBUM_COVER_PADDING 5
#define ALBUM_COVER_SIZE 54
#define ALBUM_INFO_SECTION_X_PADDING 20

#define CREATE_NEW_ALBUM_ICON_SIZE 32

#define ROW_BACKGROUND_COLOR 0xFFFFFF

@interface AlbumSuggestionView()
@property (retain) NSString *message;
@property (retain) Album *defaultChoice;
@property (strong) NSMutableArray *suggestionsArray;
@property (strong) UIView *contentContainer;
@end

@implementation AlbumSuggestionView

- (id)init {
    return [self init:nil :nil :nil];
}

- (id)init:(NSString*)message:(NSArray*)suggestions:(Album*)defaultChoice {
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:UIColorFromRGBA(0x0, 0.75)];
        
        self.contentContainer = [UIView new];
        self.contentContainer.backgroundColor = UIColorFromRGB(0xFFFFFF);
        [self addSubview:self.contentContainer];
        
        [self update:message :suggestions :defaultChoice];
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)];
        tapRecognizer.delegate = self;
        [self addGestureRecognizer:tapRecognizer];
        
        
    }
    return self;
}

-(void)update:(NSString*)message:(NSArray*)suggestions:(Album*)defaultChoice {
    self.message = message;
    self.defaultChoice = defaultChoice;
    
    if(suggestions) {
        self.suggestionsArray = [suggestions mutableCopy];
        if(self.defaultChoice) {
            //ensure that the default choice is in the front of the list
            [self.suggestionsArray removeObject:self.defaultChoice];
            [self.suggestionsArray insertObject:self.defaultChoice atIndex:0];
        }
        
        
        UIView *albumsView = [self getAlbumsView];
        [Util removeAllSubviews:self.contentContainer];
        
        __weak typeof(self) bself = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            CGRect finalFrame = [Util centerRect:albumsView.frame :[Util getRectCenter:bself.bounds]];
            [UIView animateWithDuration:0.3 animations:^{
                bself.contentContainer.frame = finalFrame;
            } completion:^(BOOL finished) {
                [bself.contentContainer addSubview:albumsView];
                bself.contentContainer.frame = finalFrame;
            }];
        });
        
    }
    else {
        UIView *busyView = [self getBusyView];
        self.contentContainer.frame = busyView.frame;
        self.contentContainer.center = self.center;
        [self.contentContainer addSubview:busyView];
        [Util showPopup:self :self.contentContainer :YES :nil];
    }
}


-(UIView*)getBusyView {
    CGFloat WIDTH = 220;
    CGFloat HEIGHT = 50;
    CGFloat BORDER = 0;
    CGFloat GIF_SECTION_SIZE = 64;
    
    CGRect boxRect = CGRectMake(0, 0, WIDTH, HEIGHT);
    
    UIColor *bgColor = UIColorFromRGBA(0xFFFFFF, 1);
    
    UIView *mainBox = [[UIView alloc] initWithFrame:boxRect];
    [mainBox setBackgroundColor:bgColor];
    [mainBox.layer setBorderColor: bgColor.CGColor];
    [mainBox.layer setBorderWidth: BORDER];
    [Util drawFastShadow:mainBox];
    [mainBox.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [mainBox.layer setShadowRadius:10.0];
    [mainBox.layer setShadowOpacity:0.9];
    
    CGRect gifRect, messageRect;
    CGRectDivide(mainBox.bounds, &gifRect, &messageRect, GIF_SECTION_SIZE, CGRectMinXEdge);
    
    UIView *thumbView = [[UIView alloc] initWithFrame:gifRect];
    [mainBox addSubview:thumbView];
    [Util setViewSkin:thumbView :LOADING_IMAGE_INDICATOR_LIGHT];
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:messageRect];
    [mainBox addSubview:messageLabel];
    messageLabel.adjustsFontSizeToFitWidth = NO;
    messageLabel.lineBreakMode = UILineBreakModeTailTruncation;
    messageLabel.textAlignment = UITextAlignmentLeft;
    messageLabel.backgroundColor = [UIColor clearColor];
    [messageLabel setTextColor:UIColorFromRGB(0x555555)];
    [messageLabel setFont: [UIFont fontWithName:@"TrebuchetMS-Bold" size:13]];
    [messageLabel setText:@"please wait..."];
    
    return mainBox;
}

-(UIView*)getAlbumsView {
    CGRect mainContentBoxRect, mainMessageContainerRect;
    
    CGFloat mainBoxHeight = MAIN_MESSAGE_HEIGHT + (ALBUM_HEIGHT * ([self.suggestionsArray count] + 1));
    mainContentBoxRect = CGRectMake(0, 0, MAIN_BOX_WIDTH, mainBoxHeight);
    
    mainMessageContainerRect = CGRectMake(MAIN_BOX_BORDER, MAIN_BOX_BORDER, mainContentBoxRect.size.width - 2 * MAIN_BOX_BORDER, MAIN_MESSAGE_HEIGHT);
    
    UIView *mainContentBox = [[UIView alloc] initWithFrame:mainContentBoxRect];
    [mainContentBox setBackgroundColor:[UIColor whiteColor]];
    [mainContentBox.layer setBorderWidth:MAIN_BOX_BORDER];
    [mainContentBox.layer setBorderColor:UIColorFromRGBA(0xFFFFFF, 0.45).CGColor];
    [Util drawFastShadow:mainContentBox];
    [mainContentBox.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [mainContentBox.layer setShadowRadius:3.0];
    [mainContentBox.layer setShadowOpacity:1.0];
    
    UIView *mainMessageContainer = [[UIView alloc] initWithFrame:mainMessageContainerRect];
    [mainContentBox addSubview:mainMessageContainer];
    [mainMessageContainer setBackgroundColor:UIColorFromRGB(0x343434)];
    
    CGRect mainMessageRect = CGRectInset(mainMessageContainerRect, MAIN_MESSAGE_PADDING, MAIN_MESSAGE_PADDING);
    UILabel *mainMessage = [[UILabel alloc] initWithFrame:mainMessageRect];
    [mainMessageContainer addSubview:mainMessage];
    [mainMessage setBackgroundColor:[UIColor clearColor]];
    [mainMessage setTextColor:UIColorFromRGBA(0xFFFFFF, 0.9)];
    [mainMessage setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:13]];
    [mainMessage setTextAlignment:NSTextAlignmentCenter];
    [mainMessage setText:self.message];
    
    
    CGFloat y = CGRectGetMaxY(mainMessageRect);
    for(NSUInteger i=0; i<[self.suggestionsArray count]; ++i) {
        Album *suggestion = [self.suggestionsArray objectAtIndex:i];
        CGRect albumViewRect = CGRectMake(MAIN_BOX_BORDER, y, mainContentBoxRect.size.width - 2 * MAIN_BOX_BORDER, ALBUM_HEIGHT);
        UIView *albumView = [[UIView alloc] initWithFrame:albumViewRect];
        [mainContentBox addSubview:albumView];
        [albumView setBackgroundColor:UIColorFromRGBA(ROW_BACKGROUND_COLOR, 1)];
        
        CGRect albumCoverSectionRect, albumInfoSectionRect;
        CGRectDivide(albumView.bounds, &albumCoverSectionRect, &albumInfoSectionRect, ALBUM_COVER_SECTION_WIDTH, CGRectMinXEdge);
        
        CGRect albumCoverRect = CGRectMake(0, 0, ALBUM_COVER_SIZE, ALBUM_COVER_SIZE);
        albumCoverRect = [Util centerRect:albumCoverRect :[Util getRectCenter:albumCoverSectionRect]];
        
        albumInfoSectionRect = CGRectInset(albumInfoSectionRect, ALBUM_INFO_SECTION_X_PADDING, 0);
        
        CGRect albumNameRect, albumDateRect;
        CGRectDivide(albumInfoSectionRect, &albumNameRect, &albumDateRect, ALBUM_NAME_SECTION_HEIGHT_FRACTION * albumInfoSectionRect.size.height, CGRectMinYEdge);
        
        
        
        UIImageView *albumCover = [suggestion getCoverPhoto:ALBUM_COVER_SIZE :ALBUM_COVER_SIZE :YES];
        [albumCover setFrame:albumCoverRect];
        [albumView addSubview:albumCover];
        [albumCover.layer setBorderWidth:3];
        [albumCover.layer setBorderColor:UIColorFromRGBA(0xFFFFFF, 1.0).CGColor]; 
        
        UILabel *albumName = [[UILabel alloc] initWithFrame:albumNameRect];
        [albumView addSubview:albumName];
        [albumName setText:suggestion.name];
        [albumName setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:11]];
        [albumName setTextAlignment:UITextAlignmentLeft];
        [albumName setBackgroundColor:[UIColor clearColor]];
        
        UILabel *albumDate = [[UILabel alloc] initWithFrame:albumDateRect];
        [albumView addSubview:albumDate];
        [albumDate setText:[suggestion getDate]];
        [albumDate setTextColor:UIColorFromRGBA(0xAEAEAE, 1)];
        [albumDate setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:10]];
        [albumDate setTextAlignment:UITextAlignmentRight];
        [albumDate setBackgroundColor:[UIColor clearColor]];
        
        UIView *border = [[UIView alloc] initWithFrame:CGRectMake(MAIN_BOX_BORDER, CGRectGetMaxY(albumViewRect), albumViewRect.size.width, 1)];
        [border setBackgroundColor:[UIColor brownColor]];
        [mainContentBox addSubview:border];
        
        albumView.tag = i + 1;
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAlbumTap:)];
        [albumView addGestureRecognizer:tapRecognizer];
        //tapRecognizer.delegate = self;
        
        y = CGRectGetMaxY(albumViewRect) + 1;
    }
    
    CGRect createNewAlbumViewRect = CGRectMake(MAIN_BOX_BORDER, y, mainContentBoxRect.size.width - 2 * MAIN_BOX_BORDER, ALBUM_HEIGHT);
    UIView *createNewAlbumView = [[UIView alloc] initWithFrame:createNewAlbumViewRect];
    [createNewAlbumView setBackgroundColor:UIColorFromRGBA(ROW_BACKGROUND_COLOR, 1)];
    createNewAlbumView.tag = -1;
    [mainContentBox addSubview:createNewAlbumView];
    
    CGRect createNewAlbumIconSectionRect, createNewAlbumLabelRect, createNewAlbumIconRect;
    CGRectDivide(createNewAlbumView.bounds, &createNewAlbumIconSectionRect, &createNewAlbumLabelRect, ALBUM_COVER_SECTION_WIDTH, CGRectMinXEdge);
    
    createNewAlbumIconRect = CGRectMake(0, 0, CREATE_NEW_ALBUM_ICON_SIZE, CREATE_NEW_ALBUM_ICON_SIZE);
    createNewAlbumIconRect = [Util centerRect:createNewAlbumIconRect :[Util getRectCenter:createNewAlbumIconSectionRect]];
    
    UIImageView *createNewAlbumIcon = [[UIImageView alloc] initWithFrame:createNewAlbumIconRect];
    [createNewAlbumIcon setImage:[UIImage imageNamed:@"create_new_album_icon.png"]];
    [createNewAlbumView addSubview:createNewAlbumIcon];
    [createNewAlbumIcon setBackgroundColor:[UIColor clearColor]];
    
    UILabel *createNewAlbumLabel = [[UILabel alloc] initWithFrame:createNewAlbumLabelRect];
    [createNewAlbumView addSubview:createNewAlbumLabel];
    [createNewAlbumLabel setText:@"or create a new album..."];
    [createNewAlbumLabel setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:11]];
    [createNewAlbumLabel setTextAlignment:UITextAlignmentCenter];
    [createNewAlbumLabel setTextColor:[UIColor blackColor]];
    [createNewAlbumLabel setBackgroundColor:[UIColor clearColor]];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onAlbumTap:)];
    [createNewAlbumView addGestureRecognizer:tapRecognizer];
    
    return mainContentBox;
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return [touch.view isEqual:self];
}

-(void)onTap {
    [self removeFromSuperview];
    [[EventManager getInstance] fireEvent:ALBUM_SUGGESTION_DISMISSED, nil];
}

-(void)onAlbumTap:(UITapGestureRecognizer*)recognizer {
    
    NSInteger tag = recognizer.view.tag;
    if(tag == 0) {
        DLog(@"warning: missing tag on recognizer view, ignoring tap");
        return;
    }
    
    Album *selectedAlbum = nil;
    if(tag != -1) {
        selectedAlbum = [self.suggestionsArray objectAtIndex:(tag - 1)];
    }

    [self removeFromSuperview];
    [[EventManager getInstance] fireEvent:ALBUM_SELECTED_ON_SUGGESTION, selectedAlbum, nil];
}
@end
