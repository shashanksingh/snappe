//
//  AddPhotosView.h
//  snappe
//
//  Created by Shashank on 07/04/13.
//
//

#import <UIKit/UIKit.h>
#import "AlbumAccessor.h"

@interface AddPhotosView : UIView
+(void)show:(Album*)album;
@end
