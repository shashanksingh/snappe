//
//  NotificationViewControllerViewController.h
//  snappe
//
//  Created by Shashank on 13/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreController.h"

@interface NotificationViewController : UITableView<UITableViewDataSource, UITableViewDelegate>
@end
