//
//  InAppPurchaseView.h
//  snappe
//
//  Created by Shashank on 13/01/13.
//
//

#import <UIKit/UIKit.h>

@interface InAppPurchaseView : UIView<UIGestureRecognizerDelegate>
+(void)show;
@end
