//
//  EventAlbumView.h
//  snappe
//
//  Created by Shashank on 12/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumAccessor.h"
#import "UIView+Help.h"

@interface AlbumView : UIView
@property (strong) Album *album;
- (id)initWithFrame:(CGRect)frame:(Album*)album;
-(void)updateAlbum:(Album*)album;
-(void)showShareView;
@end
