//
//  FriendsAlbumsViewController.h
//  snappe
//
//  Created by Shashank on 14/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Carousel.h"

@interface FriendsAlbumsView : UIView<CarouselDataSource>
@property (strong) Carousel *carousel;
-(NSManagedObject*)getCurrentFriendOrAlbum;
@end
