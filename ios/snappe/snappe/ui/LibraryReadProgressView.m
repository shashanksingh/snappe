//
//  LibraryReadProgressView.m
//  snappe
//
//  Created by Shashank on 01/01/13.
//
//

#import "LibraryReadProgressView.h"
#import "Util.h"
#import "EventManager.h"
#import "AlbumAccessor.h"
#import "RTLabel.h"
#import <QuartzCore/QuartzCore.h>

#define BOX_HEIGHT 100
#define BOX_WIDTH 260
#define MAIN_BOX_BORDER_SIZE 5
#define THUMB_SIZE 64
#define SECTION_PADDING 8

@interface LibraryReadProgressView()
@property (strong) UIImageView *thumb;
@property (strong) UILabel *statusText;
@property (strong) UILabel *statusSubText;
@property NSInteger currentPhotoIndex;
@end

@implementation LibraryReadProgressView
@synthesize thumb;
@synthesize statusText;
@synthesize statusSubText;
@synthesize currentPhotoIndex;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self drawOnFrame:self.bounds];
        [self setupEvents];
    }
    return self;
}

-(void)drawOnFrame:(CGRect)frame {
    CGRect mainBoxRect;
    mainBoxRect = CGRectMake(0, 0, BOX_WIDTH, BOX_HEIGHT);
    mainBoxRect = [Util centerRect:mainBoxRect :[Util getRectCenter:frame]];
    
    CGRect thumbSectionRect, statusTextSectionRect, statusSubTextSectionRect, clarificationTextSectionRect;
    CGRect r = mainBoxRect;
    CGRectDivide(r, &clarificationTextSectionRect, &r, 20, CGRectMaxYEdge);
    CGRectDivide(r, &thumbSectionRect, &r, THUMB_SIZE + 2 * SECTION_PADDING, CGRectMinXEdge);
    CGRectDivide(r, &statusTextSectionRect, &statusSubTextSectionRect, r.size.height * 0.6, CGRectMinYEdge);
    
    CGRect thumbRect = CGRectInset(thumbSectionRect, SECTION_PADDING, SECTION_PADDING);
    CGRect statusTextRect = [Util rectInset:statusTextSectionRect :2 * SECTION_PADDING :SECTION_PADDING :0 :SECTION_PADDING];
    CGRect statusSubTextRect = [Util rectInset:statusSubTextSectionRect :0 :SECTION_PADDING :SECTION_PADDING :SECTION_PADDING];
    statusSubTextRect.size.height = 10;
    
    UIView *mainBox = [[UIView alloc] initWithFrame:mainBoxRect];
    [self addSubview:mainBox];
    [mainBox setBackgroundColor:[UIColor whiteColor]];
    [mainBox.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [mainBox.layer setBorderWidth: MAIN_BOX_BORDER_SIZE];
    [Util drawFastShadow:mainBox];
    [mainBox.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [mainBox.layer setShadowRadius:10.0];
    [mainBox.layer setShadowOpacity:0.9];
    

    thumb = [[UIImageView alloc] initWithFrame:thumbRect];
    [thumb setBackgroundColor:[UIColor clearColor]];
    [thumb.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [thumb.layer setBorderWidth:2];
    [self addSubview:thumb];
    
    statusText = [[UILabel alloc] initWithFrame:statusTextRect];
    [self addSubview:statusText];
    statusText.adjustsFontSizeToFitWidth = NO;
    statusText.lineBreakMode = UILineBreakModeTailTruncation;
    statusText.textAlignment = UITextAlignmentLeft;
    statusText.backgroundColor = [UIColor clearColor];
    [statusText setTextColor:UIColorFromRGB(0x555555)];
    [statusText setFont: [UIFont fontWithName:@"TrebuchetMS-Bold" size:13]];
    
    
    statusSubText = [[UILabel alloc] initWithFrame:statusSubTextRect];
    statusSubText.adjustsFontSizeToFitWidth = NO;
    statusSubText.textAlignment = UITextAlignmentLeft;
    statusSubText.backgroundColor = [UIColor clearColor];
    [statusSubText setTextColor:UIColorFromRGB(0x676767)];
    [statusSubText setFont: [UIFont fontWithName:@"TrebuchetMS-Bold" size:9]];
    [self addSubview:statusSubText];
    
    self.statusText.text = @"reading local photos...";
    self.statusText.textColor = [UIColor grayColor];
    self.statusSubText.text = @"";
    [self setThumbImage:[UIImage imageNamed:@"photo_icon.png"]];
    
    clarificationTextSectionRect = CGRectInset(clarificationTextSectionRect, 0, 0);
    UILabel *clarificationLabel = [[UILabel alloc] initWithFrame:clarificationTextSectionRect];
    clarificationLabel.text = @"  * photos are never uploaded/shared until you explicitly share them";
    clarificationLabel.font = [UIFont fontWithName:@"TrebuchetMS-Italic" size:8];
    clarificationLabel.textColor = UIColorFromRGB(0x222222);
    clarificationLabel.textAlignment = NSTextAlignmentLeft;
    clarificationLabel.backgroundColor = UIColorFromRGB(0xF5F5F5);
    [self addSubview:clarificationLabel];
}


-(void)setupEvents {
    
    __block NSString *onParseAlbumListenerID, *onParsePhotoListenerID;
    
    onParseAlbumListenerID = [[EventManager getInstance] addEventListener:LOCAL_LIB_PARSING_ALBUM :^(va_list args)
    {
        ALAssetsGroup *group = va_arg(args, ALAssetsGroup*);
        
        self.currentPhotoIndex = 0;
        
        [statusText setTextColor:UIColorFromRGB(0x0)];
        self.thumb.layer.opacity = 1.0;
        
        self.statusText.text = [group valueForProperty:ALAssetsGroupPropertyName];
        self.statusSubText.text = [NSString stringWithFormat:@"reading photos: 0 of %d", [group numberOfAssets]];
        [self setThumbImage:[UIImage imageWithCGImage:[group posterImage]]];
        
    }];
    
    onParsePhotoListenerID = [[EventManager getInstance] addEventListener:LOCAL_LIB_PARSING_PHOTO :^(va_list args) {
        ALAssetsGroup *group = va_arg(args, ALAssetsGroup*);
        ALAsset *asset = va_arg(args, ALAsset*);
        
        self.currentPhotoIndex++;
        
        __block typeof(self) bself = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            if(!bself) {
                return;
            }
            
            bself.statusSubText.text = [NSString stringWithFormat:@"reading photos: %d of %d", bself.currentPhotoIndex, [group numberOfAssets]];
            [bself setThumbImage:[UIImage imageWithCGImage:[asset thumbnail]]];
        });
    }];
    
    __block typeof(self) bself = self;
    [[EventManager getInstance] addEventListener:LOCAL_LIB_PARSE_ENDED :YES :^(va_list args) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            if(!bself) {
                return;
            }
            bself.statusSubText.text = @"all photos read!";
        });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            if(!bself) {
                return;
            }
            [bself showDoneView];
        });
    }];
    [[EventManager getInstance] addEventListener:LOCAL_LIB_PARSE_ENDED :YES :^(va_list args) {
        [[EventManager getInstance] removeEventListener:onParseAlbumListenerID];
        [[EventManager getInstance] removeEventListener:onParsePhotoListenerID];
    }];
}

-(void)setThumbImage:(UIImage*)image {
    self.thumb.image = image;
}

-(void)showDoneView {
    self.thumb.image = nil;
    [Util setViewSkin:self.thumb :LOADING_IMAGE_INDICATOR_LIGHT];
    
    self.statusSubText.hidden = YES;
    
    self.statusText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
    self.statusText.textColor = [UIColor grayColor];
    self.statusText.text = @"organizing your photos...";
    self.statusText.textAlignment = UITextAlignmentLeft;
    
    CGRect frame = self.statusText.frame;
    frame.origin.x -= 25;
    frame.origin.y += 8;
    self.statusText.frame = frame;

    //self.statusText.center = self.statusText.superview.center;
    
}
@end
