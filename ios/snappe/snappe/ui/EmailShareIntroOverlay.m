//
//  EmailShareIntroOverlay.m
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import "EmailShareIntroOverlay.h"

@implementation EmailShareIntroOverlay

-(NSArray*)getParameters {
    IntroHint *allowUploadHint = [IntroHint new];
    allowUploadHint.labelRect = CGRectMake(self.frame.size.width * 0.2, self.frame.size.height * 0.35, 250, 60);
    allowUploadHint.labelText = @"tap on this button to allow the email recipient to upload photos to this album, from their computer!";
    allowUploadHint.hintTargetLocation = CGPointMake(self.frame.size.width * 0.75, self.frame.size.height * 0.25);
    
    return @[allowUploadHint];
}

+(NSString*)getIntroID {
    return @"email_share";
}

-(CGFloat)getOpacity {
    return 0.2;
}
@end
