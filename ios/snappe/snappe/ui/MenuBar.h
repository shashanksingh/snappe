//
//  CarouselMenuBar.h
//  snappe
//
//  Created by Shashank on 12/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuBar : UIView
typedef enum Alignment {
    LEFT, RIGHT, TOP, BOTTOM, CENTER
} Alignment;

-(id)initWithFrame:(CGRect)frame :(Alignment)alignment :(BOOL)centered;
-(void)addSection:(UIView*)section :(CGFloat)size;
-(UIView*)getDefaultSection;
-(void)setDefaultSection:(UIView*)section;
-(void)expand;
-(void)collapse;
-(void)toggle;
-(BOOL)isCollapsed;
@end
