//
//  PhotoPickerView.h
//  snappe
//
//  Created by Shashank on 21/02/13.
//
//

#import <UIKit/UIKit.h>

typedef enum PhotoPickerType {
    SELECT, PRIVATIZE
} PhotoPickerType;

@interface PhotoPickerView : UIScrollView
-(id)initWithFrame:(CGRect)frame:(PhotoPickerType)type;
-(void)populate;
-(void)populate:(NSArray*)photos;
-(NSArray*)getSelectedPhotos;
@end
