//
//  AlbumCreatorView.h
//  snappe
//
//  Created by Shashank on 21/02/13.
//
//

#import <UIKit/UIKit.h>

@interface AlbumCreatorView : UIView
+(void)show;
+(void)show:(BOOL)facebookAlbum;
@end
