//
//  InAppPurchaseView.m
//  snappe
//
//  Created by Shashank on 13/01/13.
//
//

#import "InAppPurchaseView.h"
#import "Util.h"
#import "Logger.h"
#import "EventManager.h"
#import "CreditsManager.h"
#import "ModalAlertView.h"
#import "CreditsManager.h"
#import "HelpOverlay.h"
#import "BButton.h"
#import <StoreKit/StoreKit.h>
#import <QuartzCore/QuartzCore.h>

#define MAIN_BOX_WIDTH 310
#define MAIN_BOX_BORDER 0
#define MAIN_MESSAGE_HEIGHT 60
#define MAIN_MESSAGE_PADDING 5
#define SECTION_HEIGHT 90
#define SECTION_X_PADDING 0
#define SECTION_Y_PADDING 5
#define SECTION_TITLE_Y_PADDING 3
#define ICON_SECTION_WIDTH 44
#define BUY_BUTTON_SECTION_WIDTH 100
#define ICON_SIZE 24
#define ITEM_NAME_SECTION_HEIGHT_FRACTION 0.25
#define BUY_BUTTON_HEIGHT_FRACTION 0.6

#define RESTORE_SECTION_HEIGHT 60
#define RESTORE_MESSAGE_HEIGHT 30
#define RESTORE_BUTTON_WIDTH 100
#define RESTORE_BUTTON_HEIGHT 30

#define ROW_BACKGROUND_COLOR 0xFFFFFF

@implementation InAppPurchaseView

- (id)init
{
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if (self) {
        [self draw];
        
        [self setBackgroundColor:UIColorFromRGBA(0x0, 0.75)];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)];
        tapRecognizer.delegate = self;
        [self addGestureRecognizer:tapRecognizer];
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:APP_STORE_PRODUCT_PURCHASE_SUCCEEDED :^(va_list args) {
            if(bself) {
                [bself onTap];
                [ModalAlertView hideBusyAlert];
                
                NSString *productID = va_arg(args, NSString*);
                NSString *productName = [[CreditsManager getInstance] getProductName:productID];
                NSString *message = [NSString stringWithFormat:@"Congratulations! You have successfully purchased \"%@\"", productName];
                [ModalAlertView showSuccess:message];
            }
        }];
        
        [[EventManager getInstance] addEventListener:APP_STORE_PRODUCT_PURCHASE_RESTORED :^(va_list args) {
            if(bself) {
                [bself redraw];
            }
            [ModalAlertView hideBusyAlert];
        }];
        
        [[EventManager getInstance] addEventListener:APP_STORE_PRODUCT_PURCHASE_FAILED :^(va_list args) {
            NSError *error = va_arg(args, NSError*);
            [Util alertError:error :@"There was an error in processing your purchase. Please try again."];
            if(bself) {
                [bself redraw];
            }
            [ModalAlertView hideBusyAlert];
        }];
        
        [[EventManager getInstance] addEventListener:APP_STORE_PRODUCT_PURCHASE_CANCELLED :^(va_list args) {
            if(bself) {
                [bself redraw];
            }
            [ModalAlertView hideBusyAlert];
        }];
        
    }
    return self;
}

+(void)show {
    InAppPurchaseView *view = [[InAppPurchaseView alloc] init];
    [Util showModalView:view];
    [HelpOverlay hide];
    [Logger track:@"iap_view_shown"];
}


-(void)draw {
    [ModalAlertView showBusy:@"please wait..."];
    
    __weak typeof(self) bself = self;
    [[CreditsManager getInstance] getProducts:^(NSArray *products) {
        [ModalAlertView hideBusyAlert];
        if(bself) {
            [bself draw:products];
        }
    } :^(NSError *error) {
        [ModalAlertView hideBusyAlert];
        [Util alertError:error:@"There was an unexpected error. Please try again later."];
    }];
}

-(void)redraw {
    [Util removeAllSubviews:self];
    [self draw];
    [Logger track:@"iap_view_updated"];
}

-(void)draw:(NSArray*)products {
    CGRect frame = self.bounds;
    
    NSSet *purchasedProducts = [CreditsManager getPurchasedProductIDs];
    
    CGRect mainContentBoxRect, mainMessageContainerRect;
    
    CGFloat mainBoxHeight = MAIN_MESSAGE_HEIGHT + (SECTION_HEIGHT * products.count) + RESTORE_SECTION_HEIGHT;
    mainContentBoxRect = CGRectMake(0, 0, MAIN_BOX_WIDTH, mainBoxHeight);
    mainContentBoxRect = [Util centerRect:mainContentBoxRect :[Util getRectCenter:frame]];
    
    mainMessageContainerRect = CGRectMake(MAIN_BOX_BORDER, MAIN_BOX_BORDER, mainContentBoxRect.size.width - 2 * MAIN_BOX_BORDER, MAIN_MESSAGE_HEIGHT);
    
    UIView *mainContentBox = [[UIView alloc] initWithFrame:mainContentBoxRect];
    [mainContentBox setBackgroundColor:[UIColor whiteColor]];
    [mainContentBox.layer setBorderWidth:MAIN_BOX_BORDER];
    [mainContentBox.layer setBorderColor:UIColorFromRGBA(0xFFFFFF, 0.45).CGColor];
    [Util drawFastShadow:mainContentBox];
    [mainContentBox.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [mainContentBox.layer setShadowRadius:3.0];
    [mainContentBox.layer setShadowOpacity:1.0];
    
    UIView *mainMessageContainer = [[UIView alloc] initWithFrame:mainMessageContainerRect];
    [mainContentBox addSubview:mainMessageContainer];
    [mainMessageContainer setBackgroundColor:UIColorFromRGB(0x343434)];
    
    CGRect mainMessageRect = CGRectInset(mainMessageContainerRect, MAIN_MESSAGE_PADDING, MAIN_MESSAGE_PADDING);
    UILabel *mainMessage = [[UILabel alloc] initWithFrame:mainMessageRect];
    [mainMessageContainer addSubview:mainMessage];
    [mainMessage setBackgroundColor:[UIColor clearColor]];
    [mainMessage setTextColor:UIColorFromRGBA(0xFFFFFF, 0.9)];
    [mainMessage setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:13]];
    [mainMessage setTextAlignment:NSTextAlignmentCenter];
    [mainMessage setText:@"Enhance your Snappe Experience!"];
    
    CGFloat y = CGRectGetMaxY(mainMessageRect);
    
    for(int i=0; i<[products count]; i++) {
        SKProduct *product = products[i];
        BOOL isAlreadyPurchased = [purchasedProducts containsObject:product.productIdentifier];
        
        CGRect itemViewRect = CGRectMake(MAIN_BOX_BORDER, y, mainContentBoxRect.size.width - 2 * MAIN_BOX_BORDER, SECTION_HEIGHT);
        itemViewRect = CGRectInset(itemViewRect, SECTION_X_PADDING, SECTION_Y_PADDING);
        
        
        UIView *itemView = [[UIView alloc] initWithFrame:itemViewRect];
        
        [mainContentBox addSubview:itemView];
        [itemView setBackgroundColor:UIColorFromRGBA(ROW_BACKGROUND_COLOR, 1)];
        //itemView.backgroundColor = [UIColor yellowColor];
        CGRect itemInfoSectionRect, buySectionRect;
        CGRect r = itemView.bounds;
        
        CGRectDivide(r, &buySectionRect, &itemInfoSectionRect, BUY_BUTTON_SECTION_WIDTH, CGRectMaxXEdge);
        
        //manual adjustments
        itemInfoSectionRect.origin.x += 20;
        itemInfoSectionRect.size.width -= 20;
        buySectionRect.size.width -= 10;
        
        CGRect itemNameRect, itemDescriptionRect;
        CGRectDivide(itemInfoSectionRect, &itemNameRect, &itemDescriptionRect, ITEM_NAME_SECTION_HEIGHT_FRACTION * itemInfoSectionRect.size.height, CGRectMinYEdge);
        
        
        CGRect buyButtonRect, priceLabelRect;
        CGRectDivide(buySectionRect, &buyButtonRect, &priceLabelRect, BUY_BUTTON_HEIGHT_FRACTION * buySectionRect.size.height, CGRectMinYEdge);
        
        itemNameRect.origin.y += SECTION_TITLE_Y_PADDING;
        UILabel *itemName = [[UILabel alloc] initWithFrame:itemNameRect];
        [itemView addSubview:itemName];
        [itemName setText:product.localizedTitle];
        [itemName setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:12]];
        [itemName setTextAlignment:UITextAlignmentLeft];
        [itemName setBackgroundColor:[UIColor clearColor]];
        
        UILabel *itemDescription = [[UILabel alloc] initWithFrame:itemDescriptionRect];
        [itemView addSubview:itemDescription];
        [itemDescription setNumberOfLines:0];
        [itemDescription setText:product.localizedDescription];
        [itemDescription setTextColor:UIColorFromRGBA(0x0, 0.65)];
        [itemDescription setFont:[UIFont fontWithName:@"TrebuchetMS" size:10]];
        [itemDescription setBackgroundColor:[UIColor clearColor]];
        
        BOOL canPurchaseMore = (![CreditsManager isUnlimitedCreditsPurchased]) && (!isAlreadyPurchased || [CreditsManager isConsumableProduct:product.productIdentifier]);
        
        NSString *buyButtonIconName = !canPurchaseMore ? @"buy_credits_icon_already_bought.png" : [NSString stringWithFormat:@"buy_now_button_%d.png", i + 1];
        
        UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [buyButton setImage:[UIImage imageNamed:buyButtonIconName] forState:UIControlStateNormal];
        [buyButton addTarget:self action:@selector(onBuyButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [buyButton setTitle:@"Buy Now!" forState:UIControlStateNormal];
        [buyButton setFrame:buyButtonRect];
        [buyButton setClipsToBounds:YES];
        [buyButton setBackgroundColor:[UIColor clearColor]];
        [itemView addSubview:buyButton];
        
        if(!canPurchaseMore) {
            [buyButton setEnabled:NO];
        }
        
        UILabel *priceLabel = [[UILabel alloc] initWithFrame:priceLabelRect];
        [itemView addSubview:priceLabel];
        [priceLabel setText:[self.class getLocalizedPrice:product]];
        [priceLabel setTextColor:UIColorFromRGBA(0x0, 0.95)];
        [priceLabel setFont:[UIFont fontWithName:@"euphorigenic" size:15]];
        [priceLabel setBackgroundColor:[UIColor clearColor]];
        [priceLabel setTextAlignment:NSTextAlignmentCenter];
        
        
        UIView *border = [[UIView alloc] initWithFrame:CGRectMake(MAIN_BOX_BORDER, CGRectGetMaxY(itemViewRect), mainContentBox.frame.size.width, 1)];
        [border setBackgroundColor:[UIColor brownColor]];
        [mainContentBox addSubview:border];
        
        
        BOOL isLastItem = i == [products count] - 1;
        if(isLastItem) {
            UIImageView *ribbon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"featured_ribbon.png"]];
            [ribbon setFrame:CGRectMake(-2, -20, 30, 82)];
            //[itemView addSubview:ribbon];
        }
        
        
        buyButton.tag = i + 1;
        y = CGRectGetMaxY(itemViewRect) + 1;
    }
    
    CGRect restoreSectionRect = CGRectMake(MAIN_BOX_BORDER, y, mainContentBoxRect.size.width - 2 * MAIN_BOX_BORDER, SECTION_HEIGHT);
    UIView *restoreSection = [[UIView alloc] initWithFrame:restoreSectionRect];
    [mainContentBox addSubview:restoreSection];
    
    CGRect restoreMessageSection, restoreButtonSection;
    CGRectDivide(restoreSection.bounds, &restoreMessageSection, &restoreButtonSection, RESTORE_MESSAGE_HEIGHT, CGRectMinYEdge);
    
    restoreMessageSection = CGRectInset(restoreMessageSection, 20, 0);
    restoreMessageSection.origin.y += 10;
    
    UILabel *restoreMessage = [[UILabel alloc] initWithFrame:restoreMessageSection];
    restoreMessage.text = @"Already purchased? Tap below to restore all your in-app purchases for free";
    restoreMessage.textAlignment = NSTextAlignmentCenter;
    restoreMessage.font = [UIFont fontWithName:@"TrebuchetMS" size:10];
    restoreMessage.textColor = [UIColor grayColor];
    restoreMessage.numberOfLines = 0;
    restoreMessage.lineBreakMode = UILineBreakModeWordWrap;
    [restoreSection addSubview:restoreMessage];
    
    CGRect restoreButtonRect = CGRectMake(0, 0, RESTORE_BUTTON_WIDTH, RESTORE_BUTTON_HEIGHT);
    restoreButtonRect = [Util centerRect:restoreButtonRect :[Util getRectCenter:restoreButtonSection]];
    UIButton *restoreButton = [[BButton alloc] initWithFrame:restoreButtonRect type:BButtonTypeSuccess icon:nil fontSize:14];
    [restoreButton setTitle:@"Restore" forState:UIControlStateNormal];
    [restoreSection addSubview:restoreButton];
    
    [restoreButton addTarget:self action:@selector(onRestoreButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect appRect = [Util getAppFrame];
    CGRect mainScrollRect = CGRectMake(0, 0, mainContentBox.bounds.size.width, appRect.size.height - 60);
    mainScrollRect = [Util centerRect:mainScrollRect :[Util getRectCenter:appRect]];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:mainScrollRect];
    scrollView.contentSize = mainContentBox.bounds.size;
    [scrollView addSubview:mainContentBox];
    
    [Util showPopup:self :scrollView :!self.superview :nil];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return [touch.view isEqual:self];
}

-(void)onTap {
    [HelpOverlay show];
    [self removeFromSuperview];
    [Logger track:@"iap_view_hidden_on_tap"];
}

-(void)onBuyButtonClick:(UIButton*)sender {
    NSInteger tag = sender.tag;
    if(tag == 0) {
        [Logger logError:@"missing tag on buy button" :nil];
        return;
    }
    
    [sender setImage:[UIImage imageNamed:@"buy_credits_icon_processing.png"] forState:UIControlStateNormal];
    
    [ModalAlertView showBusy:@"please wait..."];
    
    [Logger track:@"iap_view_buy_button_clicked" :@{@"tag" : @(tag)}];
    
    __weak typeof(self) bself = self;
    [[CreditsManager getInstance] getProducts:^(NSArray *products) {
        if(bself) {
            SKProduct *selectedProduct = products[tag - 1];
            [Logger track:@"iap_view_buying_product" :@{@"product_id" : selectedProduct.productIdentifier}];
            [[CreditsManager getInstance] buy:selectedProduct];
            [ModalAlertView hideBusyAlert];
        }
    } :^(NSError *error) {
        [ModalAlertView hideBusyAlert];
        [Util alertError:error:@"There was an unexpected error. Please try again later."];
        [Logger track:@"iap_view_fetcehed_product_error" :@{@"description" : error.localizedDescription}];
        [Logger logError:@"iap_view_fetcehed_product_error" :error];
    }];
}

-(void)onRestoreButtonClick:(UIButton*)sender {
    [ModalAlertView showBusy:@"please wait..."];
    
    [Logger track:@"iap_view_restore_button_clicked"];
    
    [sender setTitle:@"Restoring..." forState:UIControlStateNormal];
    [[EventManager getInstance] addEventListener:APP_STORE_PRODUCT_PURCHASE_RESTORED :YES :^(va_list args) {
        [ModalAlertView showSuccess:@"Transactions Successfully Restored"];
        [sender setTitle:@"Restore" forState:UIControlStateNormal];
    }];
    [[EventManager getInstance] addEventListener:APP_STORE_PRODUCT_PURCHASE_CANCELLED :YES :^(va_list args) {
        [sender setTitle:@"Restore" forState:UIControlStateNormal];
    }];
    [[EventManager getInstance] addEventListener:APP_STORE_PRODUCT_PURCHASE_FAILED :YES :^(va_list args) {
        NSError *error = va_arg(args, NSError*);
        [Util alertError:error :@"Sorry! There was an error in restoring your past purchases. Please try again"];
        [sender setTitle:@"Restore" forState:UIControlStateNormal];
    }];
    
    [[CreditsManager getInstance] restore];
    
}

+(NSString*)getLocalizedPrice:(SKProduct*)product {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:product.priceLocale];
    NSString *formattedString = [numberFormatter stringFromNumber:product.price];
    return formattedString;
}
@end
