//
//  UIView+Help.h
//  snappe
//
//  Created by Shashank on 16/03/13.
//
//

#import <UIKit/UIKit.h>
@interface UIView (Help)
-(void)setHelpText:(NSString *)helpText;
-(NSString*)helpText;
-(void)setHelpSpotlightShapeOval:(BOOL)oval;
-(BOOL)getHelpSpotlightShapeOval;
@end
