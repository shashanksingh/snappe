//
//  UploadSharedAlbumPhotosView.h
//  snappe
//
//  Created by Shashank on 27/02/13.
//
//

#import <UIKit/UIKit.h>

@interface UploadSharedAlbumPhotosView : UIView
+(void)show:(NSArray*)photosToUploadIDs;
@end
