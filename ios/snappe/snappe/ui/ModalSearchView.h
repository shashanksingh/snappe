//
//  ModalSearchView.h
//  snappe
//
//  Created by Shashank on 18/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum SearchType {
    SNAPPE, DEVICE, FRIEND, ALBUM, FB_ALBUM
}SearchType;

@interface CurrentSearchQueryAsResult : NSObject
@property (retain) NSString *value;
@end

@interface DefaultSearchSuggestion : NSObject
@property (retain) NSString *name;
@end

@interface ModalSearchView : UIView<UIGestureRecognizerDelegate>
+(void)show:(SearchType)searchType:(NSDictionary*)context;
+(NSString*)searchTypeToString:(SearchType)type;
@end
