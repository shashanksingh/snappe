
//
//  AlbumActionPopup.m
//  snappe
//
//  Created by Shashank on 01/03/13.
//
//

#import "AlbumActionPopup.h"
#import "Macros.h"
#import "Util.h"
#import "UIView+Help.h"
#import "AlbumActionPopupHintOverlay.h"
#import <QuartzCore/QuartzCore.h>

#define BORDER_SIZE 2

#define ITEM_HEIGHT 52
#define ITEM_WIDTH 52
#define POINTER_HEIGHT 10
#define ICON_SIZE 50
#define SEPARATOR_WIDTH 1

@interface Pointer : UIView
@end

@implementation Pointer

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextBeginPath(ctx);
    CGContextMoveToPoint   (ctx, CGRectGetMinX(rect), CGRectGetMinY(rect));  // top left
    CGContextAddLineToPoint(ctx, CGRectGetMaxX(rect), CGRectGetMinY(rect));  // top right
    CGContextAddLineToPoint(ctx, CGRectGetMidX(rect), CGRectGetMaxY(rect));  // bottom mid
    CGContextClosePath(ctx);
    
    CGContextSetRGBFillColor(ctx, 1, 1, 1, 1);
    //CGContextSetShadowWithColor(ctx, CGSizeMake(1.0f, 1.0f), 1.0, UIColorFromRGBA(0xFFFFFF, 1).CGColor);
    CGContextFillPath(ctx);
}

@end

@interface AlbumActionPopup()<UIGestureRecognizerDelegate>
@property (retain) NSArray *items;
@property CGPoint point;
@property (strong) UIView *popupView;
@property (strong) UIView *itemsView;
@end

@implementation AlbumActionPopup

- (id)init:(NSArray*)items:(CGPoint)point
{
    CGRect frame = [Util getAppFrame];
    self = [super initWithFrame:frame];
    if(self) {
        self.items = items;
        self.point = point;
        
        [self setBackgroundColor:UIColorFromRGBA(0x0, 0)];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
        tapRecognizer.delegate = self;
        tapRecognizer.cancelsTouchesInView = NO;
        [self addGestureRecognizer:tapRecognizer];
        
        [self drawOnRect:frame];
    }
    return self;
}

-(void)drawOnRect:(CGRect)frame {
    CGRect r = CGRectMake(0, 0, ITEM_WIDTH * self.items.count + ((self.items.count - 1) * SEPARATOR_WIDTH), ITEM_HEIGHT + POINTER_HEIGHT);
    
    UIView *popupView = [[UIView alloc] initWithFrame:r];
    self.popupView = popupView;
    [self addSubview:popupView];
    
    CGRect itemsRect, pointerRect;
    CGRectDivide(popupView.bounds, &pointerRect, &itemsRect, POINTER_HEIGHT, CGRectMaxYEdge);
    
    UIView *itemsView = [[UIView alloc] initWithFrame:itemsRect];
    self.itemsView = itemsView;
    UIColor *bgColor = UIColorFromRGBA(0xFFFFFF, 1.0);
    itemsView.backgroundColor = bgColor;
    [itemsView.layer setBorderColor: bgColor.CGColor];
    [itemsView.layer setBorderWidth: 0];
    [Util drawFastShadow:itemsView];
    [itemsView.layer setShadowOffset:CGSizeMake(0, 0.0)];
    [itemsView.layer setShadowRadius:2.0];
    [itemsView.layer setShadowOpacity:1.0];
    
    [popupView addSubview:itemsView];
    
    UIView *pointerView = [[UIView alloc] initWithFrame:pointerRect];
    [popupView addSubview:pointerView];
    
    
    Pointer *pointer = [[Pointer alloc] initWithFrame:CGRectMake((pointerView.bounds.size.width - POINTER_HEIGHT)/2 , -3, 1.5 * POINTER_HEIGHT, POINTER_HEIGHT)];
    [pointerView addSubview:pointer];

    NSMutableArray *actionViews = [NSMutableArray array];
    
    r = itemsView.bounds;
    for(int i=0; i<self.items.count; i++) {
        NSArray  *item = self.items[i];
        
        //type, icon, target, selector, text
        NSString *icon = item[1];
        //NSString *text = item[4];
        NSString *helpText = item.count >= 6 ? item[5] : nil;
        
        CGRect itemRect;
        CGRectDivide(r, &itemRect, &r, ITEM_WIDTH, CGRectMinXEdge);
        
        CGRect iconRect, textRect;
        //CGRectDivide(itemRect, &iconRect, &textRect, ICON_SIZE, CGRectMinXEdge);
        iconRect = itemRect;
        
        iconRect = [Util centerRect:CGRectMake(0, 0, ICON_SIZE, ICON_SIZE) :[Util getRectCenter:iconRect]];
        UIImageView *iconView = [[UIImageView alloc] initWithFrame:iconRect];
        iconView.image = [UIImage imageNamed:icon];
        
        if(helpText) {
            [iconView setHelpText:helpText];
            iconView.userInteractionEnabled = YES;
            [iconView setHelpSpotlightShapeOval:YES];
        }
        
        [itemsView addSubview:iconView];
        [actionViews addObject:iconView];
        
        //UILabel *textView = [[UILabel alloc] initWithFrame:textRect];
        //textView.text = text;
        //[itemsView addSubview:textView];
        
        if(i < self.items.count - 1) {
            CGRect separatorRect = CGRectMake(CGRectGetMaxX(itemRect), popupView.frame.size.height * 0.15, SEPARATOR_WIDTH, popupView.frame.size.height/2);
            UIView *separator = [[UIView alloc] initWithFrame:separatorRect];
            separator.backgroundColor = UIColorFromRGB(0xBDBDBD);
            [itemsView addSubview:separator];
        }
    }
    
    CGRect f = popupView.frame;
    f.origin.x = MAX(0, self.point.x - popupView.bounds.size.width/2);
    f.origin.y = MAX(0, self.point.y - self.popupView.bounds.size.height);
    popupView.frame = f;
    
    if(f.origin.y < self.frame.size.height * 0.5) {
        [LocalAlbumActionPopupHintOverlay show:actionViews];
    }
    else {
        [RemoteAlbumActionPopupHintOverlay show:actionViews];
    }
}

-(void)onTap:(UIGestureRecognizer*)gestureRecognizer {
    CGPoint point = [gestureRecognizer locationInView:self.itemsView];
    int index = floor(point.x/(self.itemsView.frame.size.width/self.items.count));
    if(index >= 0 && index < self.items.count) {
        id target = self.items[index][2];
        SEL selector = NSSelectorFromString(self.items[index][3]);
        [target performSelector:selector];
    }
    
    [self removeFromSuperview];
}

@end
