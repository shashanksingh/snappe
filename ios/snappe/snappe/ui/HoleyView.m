//
//  HoleyView.m
//  snappe
//
//  Created by Shashank on 17/03/13.
//
//

#import "HoleyView.h"
#import "Macros.h"

@interface HoleyView()
@property CGRect holeRect;
@property BOOL oval;
@end

@implementation HoleyView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        self.holeRect = CGRectMake(0, 0, 0, 0);
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat opacity = self.holeRect.size.width > 0 && self.holeRect.size.height > 0 ? 0.75 : 0.5;
    CGContextSetFillColorWithColor(context, UIColorFromRGBA(0x0, opacity).CGColor);
    CGContextFillRect(context, rect);
    
    if(CGRectIntersectsRect(self.holeRect, rect)) {
        CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
        CGContextSetBlendMode(context, kCGBlendModeClear);
        if(self.oval) {
            CGContextFillEllipseInRect(context, self.holeRect);
        }
        else {
            CGContextFillRect(context, self.holeRect);    
        }
    }
    
    return;
}

-(void)setHole:(CGRect)holeRect:(BOOL)oval {
    self.holeRect = holeRect;
    self.oval = oval;
    [self setNeedsDisplay];
}

-(void)removeHole {
    self.holeRect = CGRectMake(0, 0, 0, 0);
    [self setNeedsDisplay];
}
@end
