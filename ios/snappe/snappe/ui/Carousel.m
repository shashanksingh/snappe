//
//  Carousel.m
//  snappe
//
//  Created by Shashank on 23/01/13.
//
//

#import "Carousel.h"
#import "AlbumAccessor.h"
#import "UserAccessor.h"
#import "AlbumView.h"
#import "UserView.h"
#import "FakeAlbum.h"
#import "Util.h"
#import "GraphManager.h"
#import "EventManager.h"
#import "Logger.h"
#import "ProgressView.h"
#import "PhotoLibraryOrganizer.h"
#import "ActionConfirmationView.h"
#import "ModalAlertView.h"
#import "GameIntroOverlay.h"
#import "RTLabel.h"
#import "FBExplanationView.h"
#import <QuartzCore/QuartzCore.h>

#define NUMBER_OF_PLACEHOLDER_VIEWS 5;
#define NUMBER_OF_VISIBLE_ITEMS 5;


#define ITEM_WIDTH (IS_IPAD ? 550 : 220)
#define ITEM_HEIGHT (IS_IPAD ? 325 : 130)


#define PREFETCH_WINDOW 10
#define INITIAL_PREFETCH_WINDOWS 4

#define SCROLL_DURATION 0.5

@interface Carousel() <SwipeViewDataSource, SwipeViewDelegate, UIGestureRecognizerDelegate>
@property NSObject<CarouselDataSource> *carouselDataSource;
@property BOOL programmaticallyScrolling;
@end

@implementation Carousel

- (id)initWithFrame:(CGRect)frame:(CarouselType)type:(NSObject<CarouselDataSource>*)carouselDataSource;
{
    self = [super initWithFrame:frame];
    if (self) {
        self.type = type;
        self.carouselDataSource = carouselDataSource;
        self.delegate = self;
        self.dataSource = self;
        
        self.alignment = SwipeViewAlignmentCenter;
        self.pagingEnabled = NO;
        self.wrapEnabled = NO;
        self.itemsPerPage = 1;
        self.truncateFinalPage = NO;
        self.bounces = YES;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:SNAPPE_URL_OPENED :^(va_list args) {
            NSURL *url = va_arg(args, NSURL*);
            NSDictionary *params = [Util getURLParameters:url];
            NSString *id = [params objectForKey:@"id"];
            if(bself && id) {
                [bself scrollToID:id:YES];
            }
        }];
    }
    return self;
}

-(void)reloadData {
    NSString * currentItemID =  [[self getCurrentAlbumOrFriend] valueForKey:@"id"];
    [super reloadData];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
        if(currentItemID) {
            [self scrollToID:currentItemID:NO];
        }
    });
    
    EventType event = self.type == LOCAL ? OWN_CAROUSEL_FLIPPED : FRIEND_CAROUSEL_FLIPPED;
    [[EventManager getInstance] fireEvent:event, [self getCurrentAlbumOrFriend], nil];
    
    [self prefetchForFastScroll];
}

-(void)scrollToItemAtIndex:(NSInteger)index duration:(NSTimeInterval)duration {
    [super scrollToItemAtIndex:index duration:duration];
    self.programmaticallyScrolling = YES;
}

-(id) getCurrentAlbumOrFriend {
    if(self.currentItemIndex >= [self.carouselDataSource.data count]) {
        return nil;
    }
    return [self.carouselDataSource.data objectAtIndex:self.currentItemIndex];
}

-(NSUInteger)getIndexForID:(NSString*)scrollTargetID {
    for(unsigned int i=0; i<[self.carouselDataSource.data count]; ++i) {
        Album *album = [self.carouselDataSource.data objectAtIndex:i];
        if(![self isFakeAlbum:album] && [album.id isEqualToString:scrollTargetID]) {
            return i;
        }
    }
    return -1;
}

-(UIView*) scrollToUserOrAlbum:(NSManagedObject*)scrollTarget {
    return [self scrollToID:[scrollTarget valueForKey:@"id"] :YES];
}

-(UIView*) scrollToID:(NSString*)scrollTargetID:(BOOL)animated {
    NSUInteger index = [self getIndexForID:scrollTargetID];
    if(index == -1) {
        return nil;
    }
    
    [self scrollToItemAtIndex:index duration:animated ? SCROLL_DURATION : 0.0];
    return [self swipeView:self viewForItemAtIndex:index reusingView:nil];
}


-(void)fakeScrollToAlbum:(Album*)album:(NSTimeInterval)duration {
    NSUInteger index = self.currentItemIndex;
    
    if(album != nil) {
        index = [self getIndexForID:album.id];
        if(index == -1) {
            return;
        }
    }
    
    int scrollBy = index > self.currentItemIndex ? -10 : 10;
    
    [self scrollByNumberOfItems:scrollBy duration:duration * 0.5];
    
    __block typeof(self) bself = self;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * 0.55 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
        if(!bself) {
            return;
        }
        [bself scrollToItemAtIndex:index duration:duration * 0.45];
    });
}

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    if(self.type == REMOTE && ![[FBManager getInstance] isAuthorized] ) {
        return 1;
    }
    
    if(self.carouselDataSource.data != nil) {
        return MAX(1, [self.carouselDataSource.data count]);
    }
    return NUMBER_OF_PLACEHOLDER_VIEWS;
}


- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    CGRect frame = CGRectMake(0, 0, ITEM_WIDTH, ITEM_HEIGHT);
    
    if(self.type == REMOTE && ![[FBManager getInstance] isAuthorized] ) {
        return [self getFacbookAuthView:frame];
    }
    
    if(self.carouselDataSource.data == nil) {
        return [self getLoadingPlaceholderView:frame];
    }
    if([self.carouselDataSource.data count] == 0) {
        return [self getNoAlbumView:frame];
    }
    
    //we have issues in reusing views here as we have two different types of views
    //we have to live with suboptimal reuse where we reuse only if the reuse candidate
    //is of the correct type
    id friendOrAlbum = [self.carouselDataSource.data objectAtIndex:index];
    if([friendOrAlbum isKindOfClass:[Album class]] || [friendOrAlbum isKindOfClass:[BaseFakeAlbum class]]) {
        Album *album = friendOrAlbum;
        if(!view || ![view isKindOfClass:[AlbumView class]]) {
            view = [[AlbumView alloc] initWithFrame:frame:album];
        }
        else {
            AlbumView *albumView = (AlbumView*)view;
            [albumView updateAlbum:album];
        }
        
    }
    else {
        User *friend = friendOrAlbum;
        if(!view || ![view isKindOfClass:[UserView class]]) {
            view = [[UserView alloc] init:frame :friend];
        }
        else {
            UserView *userView = (UserView*)view;
            [userView updateUser:friend];
        }
    }
    
    return view;
}

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    EventType event = self.type == LOCAL ? OWN_CAROUSEL_FLIPPED : FRIEND_CAROUSEL_FLIPPED;
    [[EventManager getInstance] fireEvent:event, [self getCurrentAlbumOrFriend], nil];
    
    if(!self.programmaticallyScrolling) {
        event = self.type == REMOTE ? OWN_CAROUSEL_FLIPPED_USER : FRIEND_CAROUSEL_FLIPPED_USER;
        [[EventManager getInstance] fireEvent:event, [self getCurrentAlbumOrFriend], nil];
    }
}

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView {
    return CGSizeMake(ITEM_WIDTH + 20, ITEM_HEIGHT);
}


- (void)swipeViewDidScroll:(SwipeView *)swipeView {
    self.programmaticallyScrolling = NO;
}

- (void)swipeViewDidEndScrollingAnimation:(SwipeView *)swipeView {
    [self prefetchForFastScroll];
}


-(UIView*)getLoadingPlaceholderView:(CGRect)frame {
    UIImageView *placeHolderView = [[UIImageView alloc] initWithFrame:frame];
    [placeHolderView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:1.0f]];
    [placeHolderView.layer setBorderWidth:3.0f];
    
    [placeHolderView.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [placeHolderView.layer setBorderWidth: 5];
    [placeHolderView.layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
    [placeHolderView.layer setShadowRadius:3.0];
    [placeHolderView.layer setShadowOpacity:1.0];
    
    [Util setViewSkin:placeHolderView:LOADING_IMAGE_INDICATOR_LIGHT];
    return placeHolderView;
}

-(UIView*)getNoAlbumView:(CGRect)frame {
    UIView *noAlbumView = [[UIView alloc] initWithFrame:frame];
    [noAlbumView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:1.0f]];
    [noAlbumView.layer setBorderWidth:3.0f];
    
    [noAlbumView.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [noAlbumView.layer setBorderWidth: 5];
    [noAlbumView.layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
    [noAlbumView.layer setShadowRadius:3.0];
    [noAlbumView.layer setShadowOpacity:1.0];
    [Util drawFastShadow:noAlbumView];
    noAlbumView.layer.opacity = 0.95;
    
    CGRect messageRect = noAlbumView.bounds;
    messageRect = CGRectInset(messageRect, 20, 20);
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:messageRect];
    [noAlbumView addSubview:messageLabel];
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.center = [Util getRectCenter:noAlbumView.bounds];
    messageLabel.textAlignment = UITextAlignmentCenter;
    messageLabel.textColor = UIColorFromRGB(0x0);
    messageLabel.font = [UIFont fontWithName:@"MarkerFelt-Thin" size:13.0f];
    messageLabel.numberOfLines = 0;
    messageLabel.lineBreakMode = UILineBreakModeWordWrap;
    
    if(self.type == LOCAL) {
        NSString *deviceName = IS_IPAD ? @"iPad" : @"iPhone";
        messageLabel.text = [NSString stringWithFormat:@"There are no albums in this %@ yet. Albums will appear here as you snap photos with your %@", deviceName, deviceName];
    }
    else if(self.type == REMOTE) {
        if([[PhotoLibraryOrganizer getInstance] isSuccessfullySyncedOnce]) {
            messageLabel.text = @"You and your Facebook friends don't have any albums yet. As you add photos to Facebook they will start to appear here.";
        }
        else {
            messageLabel.text = @"There was an error in syncing your friends and remote albums. Please try again by tapping the sync button";
        }
    }
    
    return noAlbumView;
}

-(UIView*)getFacbookAuthView:(CGRect)frame {
    UIView *noAlbumView = [[UIView alloc] initWithFrame:frame];
    [noAlbumView setBackgroundColor:UIColorFromRGBA(0xFFFFFF, 0.75)];
    [noAlbumView.layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
    [noAlbumView.layer setShadowRadius:3.0];
    [noAlbumView.layer setShadowOpacity:1.0];
    [Util drawFastShadow:noAlbumView];
    
    CGFloat FB_LOGIN_BUTTON_HEIGHT = 60;
    CGFloat FB_LOGIN_BUTTON_WIDTH = 220;
    
    CGRect messageSectionRect, fbLoginButtonSectionRect, fbExplainLinkRect;
    CGRectDivide(frame, &messageSectionRect, &frame, frame.size.height * 0.2, CGRectMinYEdge);
    CGRectDivide(frame, &fbLoginButtonSectionRect, &fbExplainLinkRect, frame.size.height * 0.65, CGRectMinYEdge);
    
    CGRect fbLoginButtonRect = CGRectMake(0, 0, FB_LOGIN_BUTTON_WIDTH, FB_LOGIN_BUTTON_HEIGHT);
    fbLoginButtonRect = [Util centerRect:fbLoginButtonRect :[Util getRectCenter:fbLoginButtonSectionRect]];
    fbLoginButtonRect.origin.y += 5;
    
    CGPoint center = [Util getRectCenter:messageSectionRect];
    center.y = messageSectionRect.size.height * 0.75;
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:messageSectionRect];
    [noAlbumView addSubview:messageLabel];
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.center = center;
    messageLabel.textAlignment = UITextAlignmentCenter;
    messageLabel.textColor = UIColorFromRGB(0x6F4242);
    messageLabel.font = [UIFont fontWithName:@"MarkerFelt-Thin" size:13.0f];
    messageLabel.numberOfLines = 0;
    messageLabel.lineBreakMode = UILineBreakModeWordWrap;
    messageLabel.text = @"Connect with Facebook to share albums!";
    
    UIButton *fbLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [fbLoginButton setImage:[UIImage imageNamed:@"connect_using_facebook.png"] forState:UIControlStateNormal];
    [fbLoginButton setAlpha:1.0f];
    [fbLoginButton setFrame:fbLoginButtonRect];
    [fbLoginButton setClipsToBounds:YES];
    [fbLoginButton setBackgroundColor:[UIColor clearColor]];
    [fbLoginButton addTarget:self.class action:@selector(onFacebookLoginClick) forControlEvents:UIControlEventTouchUpInside];
    [noAlbumView addSubview:fbLoginButton];
    
    RTLabel *fbExplanationLink = [[RTLabel alloc] initWithFrame:fbExplainLinkRect];
    fbExplanationLink.text = @"<u><i><center>what permissions does Snappe need?</center></i></u>";
    fbExplanationLink.font = [UIFont fontWithName:@"NotoSerif" size:10.5];
    UITapGestureRecognizer *fbExplanationTapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showFBExplanation)];
    [fbExplanationLink addGestureRecognizer:fbExplanationTapper];
    fbExplanationLink.userInteractionEnabled = YES;
    fbExplanationLink.backgroundColor = [UIColor clearColor];
    fbExplanationLink.textAlignment = NSTextAlignmentCenter;
    [noAlbumView addSubview:fbExplanationLink];
    
    noAlbumView.helpText = @"Connect with Facebbok to share photos with Facebook friends privately or on Facebook";
    return noAlbumView;
}

-(void)showFBExplanation {
    [FBExplanationView show];
}

-(void)prefetchForFastScroll {
    if(!self.carouselDataSource.data) {
        return;
    }
    
    int currentIndex = self.currentItemIndex;
    if(currentIndex%PREFETCH_WINDOW != 0) {
        return;
    }
    
    NSRange range;
    if(currentIndex == 0) {
        range = NSMakeRange(0, INITIAL_PREFETCH_WINDOWS * PREFETCH_WINDOW);
    }
    else {
        range = NSMakeRange(currentIndex + (INITIAL_PREFETCH_WINDOWS * PREFETCH_WINDOW), PREFETCH_WINDOW);
    }
    
    range = NSIntersectionRange(range, NSMakeRange(0, [self.carouselDataSource.data count]));
    if(range.length == 0) {
        return;
    }
    
    NSMutableArray *albumsToFault = [NSMutableArray array];
    NSMutableArray *photosToFault = [NSMutableArray array];
    NSMutableArray *usersToFault = [NSMutableArray array];
    NSMutableSet *userObjectIDSet = [NSMutableSet set];
    
    for(int i=range.location; i<range.location + range.length; i++) {
        NSManagedObject *friendOrAlbum = [self.carouselDataSource.data objectAtIndex:i];
        if([friendOrAlbum isKindOfClass:[Album class]]) {
            Album *album = (Album*)friendOrAlbum;
            [albumsToFault addObject:album];
            int photoCount = [album.photos count];
            int i = -1;
            while (++i <= 1 && i < photoCount) {
                [photosToFault addObject:[album.photos objectAtIndex:i]];
            }
            
            User *owner = album.owner;
            if(owner && ![userObjectIDSet containsObject:owner.objectID]) {
                [userObjectIDSet addObject:owner.objectID];
                [usersToFault addObject:owner];
            }
        }
        else if([friendOrAlbum isKindOfClass:[User class]]) {
            User *friend = (User*)friendOrAlbum;
            if(![userObjectIDSet containsObject:friend.objectID]) {
                [userObjectIDSet addObject:friend.objectID];
                [usersToFault addObject:friend];
            }
        }
    }
    
    
    [GraphManager prefetchObjects:@"Album":albumsToFault];
    [GraphManager prefetchObjects:@"Photo":photosToFault];
    [GraphManager prefetchObjects:@"User":usersToFault];
}

+(void) onFacebookLoginClick {
    [Logger track:@"fb_login_click"];
    
    [[FBManager getInstance] authorize:^(NSNumber *data){
        [Logger track:@"fb_login" :@{@"authorized" : data}];
        DLog(@"Facebook authorization done, authorized?: %@", data);
        if(data.boolValue) {
            ProgressView *progressView = [ProgressView show];
            
            [[EventManager getInstance] addEventListener:PHOTO_LIBRARY_ORGANIZED :YES :^(va_list args) {
                NSNumber *successNumber = va_arg(args, NSNumber*);
                [progressView setDone:successNumber.boolValue];
            }];
            [[EventManager getInstance] addEventListener:LOCAL_LIB_PARSE_BEGAN :YES :^(va_list args) {
                [progressView setStatus:@"reading local photo library ..."];
            }];
            [[EventManager getInstance] addEventListener:LOCAL_LIB_PARSE_ENDED :YES :^(va_list args) {
                [progressView setStatus:@"talking to server ..."];
            }];
            [[EventManager getInstance] addEventListener:LOCAL_GRAPH_UPDATE_BEGIN :YES :^(va_list args) {
                [progressView setStatus:@"updating local database ..."];
            }];
            [[EventManager getInstance] addEventListener:LOCAL_GRAPH_UPDATE_END :YES :^(va_list args) {
                BOOL *success = va_arg(args, NSNumber*).boolValue;
                if(success) {
                    [progressView setStatus:@"successfully updated local database"];
                }
                else {
                    [progressView setStatus:@"error in updating local database"];
                }
            }];
            [progressView setStatus:@"syncing with the server ..."];
            
            [ModalAlertView showBusy:@"please wait..."];
            
            [[FBManager getInstance] getFriendIDsToInvite:^(NSArray *friendIDs) {
                [ModalAlertView hideBusyAlert];
                
                if(friendIDs.count > 0) {
                    [ActionConfirmationView confirm:@"invite_fb_friends" :@"Snappe works best when your friends are  using Snappe too. Would you like to invite your friends to use Snappe?" :^(NSNumber *okay)
                     {
                         if(okay.boolValue) {
                             [Logger track:@"fb_invite_agreed"];
                             [[FBManager getInstance] inviteFriends:friendIDs :^(NSNumber *invited) {
                                 if(invited.boolValue) {
                                     [Logger track:@"fb_invited" :@{@"invited_count" : @(friendIDs.count)}];
                                 }
                                 else {
                                     [Logger track:@"fb_invite_canceled" :@{@"invitee_count" : @(friendIDs.count)}];
                                 }
                                 
                                 // XXX: can't use the default help showing mechanism as invited popup
                                 // hides the ProgressView but not the help overlay
                                 // @TODO: find a better way to show help overlay only if the owner
                                 // view is at the top (not blocked by any other view)
                                 [GameIntroOverlay show:nil];
                             }];
                             
                         }
                         else {
                             [Logger track:@"fb_invite_declined"];
                             [GameIntroOverlay show:nil];
                         }
                     }];
                }
                else {
                    [Logger track:@"fb_invite_no_invitee"];
                    [GameIntroOverlay show:nil];
                }
                
            } :^(NSError *error) {
                [ModalAlertView hideBusyAlert];
                [GameIntroOverlay show:nil];
            }];
            
            
        }
    }];

}

-(BOOL)isFakeAlbum:(NSObject*)suspect {
    return [suspect isKindOfClass:[BaseFakeAlbum class]];
}

@end
