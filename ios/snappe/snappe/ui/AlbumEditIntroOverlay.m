//
//  AlbumEditIntroOverlay.m
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import "AlbumEditIntroOverlay.h"

@implementation AlbumEditIntroOverlay

-(NSArray*)getParameters {
    IntroHint *thumbHint = [IntroHint new];
    thumbHint.labelRect = CGRectMake(self.frame.size.width * 0.1, self.frame.size.height * 0.85, 250, 60);
    thumbHint.labelText = @"tap on a photo to make it private, private photos from your phone are not shared with anyone";
    thumbHint.hintTargetLocation = CGPointMake(self.frame.size.width * 0.25, self.frame.size.height * 0.25);
    
    return @[thumbHint];
}

+(NSString*)getIntroID {
    return @"album_edit";
}

@end
