//
//  GameIntroOverlay.m
//  snappe
//
//  Created by Shashank on 19/03/13.
//
//

#import "GameIntroOverlay.h"

@implementation GameIntroOverlay

-(NSArray*)getParameters {
    IntroHint *gameHint = [IntroHint new];
    gameHint.labelRect = CGRectMake(self.frame.size.width * 0.1, self.frame.size.height * 0.05, 250, 40);
    gameHint.labelText = @"play \u2018catch-the-dot\u2019 while Snappe processes your request!";
    gameHint.hintTargetLocation = CGPointMake(self.frame.size.width * 0.25, self.frame.size.height * 0.2);
    
    return @[gameHint];
}

+(NSString*)getIntroID {
    return @"game";
}

@end
