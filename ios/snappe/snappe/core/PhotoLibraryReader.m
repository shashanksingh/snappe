//
//  PhotoLibraryReader.m
//  snappe
//
//  Created by Shashank on 09/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoLibraryReader.h"
#import "Util.h"
#import "Macros.h"
#import "AlbumAccessor.h"
#import "PhotoAccessor.h"
#import <CommonCrypto/CommonDigest.h>
#import "GraphManager.h"
#import "EventManager.h"
#import "Logger.h"
#import "CoreController.h"
#import "ImageHasher.h"
#import "ALAssetInfo.h"
#import "ALAssetsGroupInfo.h"
#import <ImageIO/CGImageProperties.h>

#define LIBRARY_RELOAD_TIMER_INTERVAL 60.0f

#define STATE_FILE_NAME @"snappe_photo_lib_reader"
#define ASSET_HASH_TO_URL_KEY @"asset_hash_to_url"
#define ASSET_URL_TO_HASH_KEY @"asset_url_to_hash"
#define ASSET_URL_TO_PHASH_KEY @"asset_url_to_phash"
#define ASSET_URL_TO_MOD_TIME_KEY @"asset_url_to_mod_time"
#define ASSET_GROUP_ID_TO_GROUP_URL_KEY @"asset_group_id_to_group_url"

#define FIRST_RUN_PREF_KEY @"snappe_is_first_run"

static const NSString *LIB_READ_SYNCHRONIZATION_KEY = @"snappe_lib_read_sync_key";
static ALAssetsGroupType ACCEPTED_ASSET_GROUP_TYPES =  ALAssetsGroupAlbum | ALAssetsGroupSavedPhotos | ALAssetsGroupLibrary;

@interface PhotoLibraryReader ()
@property (strong) ALAssetsLibrary *assetsLibrary;
@property (strong) NSMutableDictionary *assetURLToHash;
@property (strong) NSMutableDictionary *assetURLToPHash;
@property (strong) NSMutableDictionary *assetURLToModTime;
@property (strong) NSMutableDictionary *assetHashToURL;
@property (strong) NSMutableDictionary *assetGroupIDToGroupURL;
@property BOOL isReadingLibrary;
@property (strong) NSTimer *libraryReloadTimer;
@property (strong) NSDictionary *lastModDescriptor;
@end


@implementation PhotoLibraryReader
@synthesize assetsLibrary;
@synthesize assetURLToHash;
@synthesize assetURLToPHash;
@synthesize assetURLToModTime;
@synthesize assetHashToURL;
@synthesize assetGroupIDToGroupURL;
@synthesize libraryReloadTimer;

-(id) init {
    self = [super init];
    if(self) {
        self.assetsLibrary = [[ALAssetsLibrary alloc] init];
        self.isReadingLibrary = NO;
        self.libraryReloadTimer = nil;
        self.lastModDescriptor = nil;
        
        [self loadFromDisk];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPhotoLibraryChange:) name:ALAssetsLibraryChangedNotification object:nil];
        
        [[EventManager getInstance] addEventListener:LOCAL_LIBRARY_MODIFIED :^(va_list args) {
            [self.class invalidateModificationCache];
            
            if(self.libraryReloadTimer) {
                [self.libraryReloadTimer invalidate];
            }
            
            self.libraryReloadTimer = [NSTimer scheduledTimerWithTimeInterval:LIBRARY_RELOAD_TIMER_INTERVAL target:self selector:@selector(reload) userInfo:nil repeats:NO];
            
        }];
        [[EventManager getInstance] addEventListener:APPLICATION_ENTERED_BACKGROUND :^(va_list args) {
            [self saveToDisk];
        }];
        [[EventManager getInstance] addEventListener:APPLICATION_WILL_TERMINATE :^(va_list args) {
            if(self.libraryReloadTimer && self.libraryReloadTimer.isValid) {
                [self.libraryReloadTimer invalidate];
            }
            [self saveToDisk];
        }];
        [[EventManager getInstance] addEventListener:LOCAL_GRAPH_SYNCED :^(va_list args) {
            [self.class invalidateModificationCache];
        }];
        
    }
    
    return self;
}

- (void) dealloc
{
    // If you don't remove yourself as an observer, the Notification Center
    // will continue to try and send notification objects to the deallocated
    // object.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+(PhotoLibraryReader *) getInstance {
    static PhotoLibraryReader *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PhotoLibraryReader alloc] init];
    });
    return sharedInstance;
}

-(void)saveToDisk {
    @synchronized(LIB_READ_SYNCHRONIZATION_KEY) {
        NSMutableDictionary *stateData = [NSMutableDictionary dictionary];
        [stateData setObject:self.assetHashToURL forKey:ASSET_HASH_TO_URL_KEY];
        [stateData setObject:self.assetURLToHash forKey:ASSET_URL_TO_HASH_KEY];
        [stateData setObject:self.assetURLToModTime forKey:ASSET_URL_TO_MOD_TIME_KEY];
        [stateData setObject:self.assetURLToPHash forKey:ASSET_URL_TO_PHASH_KEY];
        [stateData setObject:self.assetGroupIDToGroupURL forKey:ASSET_GROUP_ID_TO_GROUP_URL_KEY];
        
        [Util saveObjectInFile:stateData :STATE_FILE_NAME];
    }
    
}

-(void)loadFromDisk {
    NSDictionary *stateData = (NSDictionary*)[Util readObjectFromFile:STATE_FILE_NAME];
    if(!stateData) {
        [self clearCache];
    }
    else {
        self.assetHashToURL = [stateData objectForKey:ASSET_HASH_TO_URL_KEY];
        self.assetURLToHash = [stateData objectForKey:ASSET_URL_TO_HASH_KEY];
        self.assetURLToPHash = [stateData objectForKey:ASSET_URL_TO_PHASH_KEY];
        self.assetURLToModTime = [stateData objectForKey:ASSET_URL_TO_MOD_TIME_KEY];
        if(!self.assetURLToModTime) {
            self.assetURLToModTime = [NSMutableDictionary dictionary];
        }
        self.assetGroupIDToGroupURL = [stateData objectForKey:ASSET_GROUP_ID_TO_GROUP_URL_KEY];
    }
}

-(ALAssetsLibrary*) getLibrary {
    return self.assetsLibrary;
}

+(void)invalidateModificationCache {
    @synchronized(LIB_READ_SYNCHRONIZATION_KEY) {
        [self getInstance].lastModDescriptor = nil;    
    }
}

+(void)invalidateAssetCache {
    PhotoLibraryReader *instance = [self getInstance];
    [instance clearCache];
}

-(void)clearCache {
    self.assetHashToURL = [NSMutableDictionary dictionary];
    self.assetURLToHash = [NSMutableDictionary dictionary];
    self.assetURLToPHash = [NSMutableDictionary dictionary];
    self.assetURLToModTime = [NSMutableDictionary dictionary];
    self.assetGroupIDToGroupURL = [NSMutableDictionary dictionary];
}

-(void)refreshCache:(NSSet*)groupIDs:(NSSet*)assetURLs {
    NSMutableSet *missingGroupIDs = [NSMutableSet setWithArray:self.assetGroupIDToGroupURL.allKeys];
    [missingGroupIDs minusSet:groupIDs];
    for(NSString *groupID in missingGroupIDs) {
        [self onMissingAssetGroup:groupID];
    }
    
    NSMutableSet *missingAssetURLs = [NSMutableSet setWithArray:self.assetURLToHash.allKeys];
    [missingAssetURLs minusSet:assetURLs];
    for(NSString *assetURL in missingAssetURLs) {
        [self onMissingAsset:assetURL];
    }
}

-(void)onMissingAsset:(NSString*)assetURL {
    NSString *assetHash = self.assetURLToHash[assetURL];
    if(assetHash) {
        [self.assetHashToURL removeObjectForKey:assetHash];
    }
    [self.assetURLToHash removeObjectForKey:assetURL];
    [self.assetURLToPHash removeObjectForKey:assetURL];
    [self.assetURLToModTime removeObjectForKey:assetURL];
}

-(void)onMissingAssetGroup:(NSString*)devicePersistentID {
    [self.assetGroupIDToGroupURL removeObjectForKey:devicePersistentID];
}


+(void)getSortedPhotosURLs:(ArrayCallback)callback:(GenericErrorback)errorback {
    if([self getInstance].assetURLToModTime.count == 0) {
        [[self getInstance] reload:^{
            callback([[self getInstance] getSortedPhotosPrivate]);
        } :errorback];
    }
    else {
        callback([[self getInstance] getSortedPhotosPrivate]);
    }
}

-(NSArray*)getSortedPhotosPrivate {
    return [self.assetURLToModTime keysSortedByValueUsingComparator:^NSComparisonResult(NSNumber *t1, NSNumber *t2) {
        return -[t1 compare:t2];
    }];
}

-(void)reload {
    [self reload:^() {
        [Logger track:@"reload_on_lib_change" :@{@"status": @"success"}];
    } :^(NSError *error) {
        [Logger logError:@"error in reloading after lib change" :error];
        [Logger track:@"reload_on_lib_change" :@{@"status": @"failure", @"error": error.localizedDescription}];
    }];
}

-(void) reload:(VoidCallback)callback:(GenericErrorback)errorback {
    [self getLocalLibrayModifications:^(id data) {
        [self saveToDisk];
        [self.class setRunFinished];
        callback();
    } :^(NSError *error){
        errorback(error);
    }];
}

-(void) getLocalLibrayModifications:(GenericCallback)callback :(GenericErrorback)errorback {
    if(![NSThread isMainThread]) {
        DLog(@"not on main thread!");
    }
    
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    @synchronized(LIB_READ_SYNCHRONIZATION_KEY) {
        //if cache is good, simply return that
        if(self.lastModDescriptor != nil) {
            callback(self.lastModDescriptor);
            return;
        }
        
        [[EventManager getInstance] addEventListener:LOCAL_LIB_GET_MOD_ENDED :YES :^(va_list args) {
            NSNumber *success = va_arg(args, NSNumber*);
            if(success.boolValue) {
                NSDictionary *modDescriptor = va_arg(args, NSDictionary*);
                
                dispatch_async(callerQ, ^{
                    callback(modDescriptor);
                });
            }
            else {
                NSError *error = va_arg(args, NSError*);
                dispatch_async(callerQ, ^{
                    errorback(error);
                });
            }
        }];
        
        //if read is already in progress nothing to do, return, fire a read otherwise
        //and toggle the flag
        if(self.isReadingLibrary) {
            return;
        }
        self.isReadingLibrary = YES;
        
    }
    
    [self validateLibraryAccess:^{
        dispatch_async([CoreController getALAssetsBackgroundQueue], ^{
            __block NSDate *t1 = [NSDate date];
            [self readLibrary:^(NSArray *libArray) {
                NSMutableDictionary *albumIDToGroup = libArray[0];
                NSMutableDictionary *albumIDToAssetURLs = libArray[1];
                NSMutableDictionary *assetURLToAssetInfo = libArray[2];
                
                DLog(@"bg time: %f", [[NSDate date] timeIntervalSinceDate:t1]);
                t1 = [NSDate date];
                
                [self getLibraryModifications:albumIDToGroup :albumIDToAssetURLs :assetURLToAssetInfo:^(NSDictionary *modDescriptor)
                {
                    @synchronized(LIB_READ_SYNCHRONIZATION_KEY) {
                        self.lastModDescriptor = modDescriptor;
                        self.isReadingLibrary = NO;
                    }
                    
                    DLog(@"main time: %f", [[NSDate date] timeIntervalSinceDate:t1]);
                    [[EventManager getInstance] fireEvent:LOCAL_LIB_GET_MOD_ENDED, @(YES), modDescriptor];
                }];
                
            } :^(NSError *error){
                @synchronized(LIB_READ_SYNCHRONIZATION_KEY) {
                    self.isReadingLibrary = NO;
                }
                
                [[EventManager getInstance] fireEvent:LOCAL_LIB_GET_MOD_ENDED, @(NO), error];
                
            }];
        });
    } :errorback];
}

-(NSError*)getLibraryAccessDeniedError {
    return [NSError errorWithDomain:SNAPPE_ERROR_DOMAIN code:SNAPPE_ERROR_ALASSET_ACCESS_DENIED userInfo:nil];
}

-(void)validateLibraryAccess:(VoidCallback)callback:(GenericErrorback)errorback {
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    [Util executeBlockOnMainQueue:^{
        if([self.assetsLibrary respondsToSelector:@selector(authorizationStatus)]) {
            ALAuthorizationStatus authStatus = [ALAssetsLibrary authorizationStatus];
            if(authStatus == ALAuthorizationStatusAuthorized) {
                dispatch_async(callerQ, ^{
                    callback();
                });
                
            }
            else {
                dispatch_async(callerQ, ^{
                    errorback([self getLibraryAccessDeniedError]);
                });
            }
            
        }
        else {
            
            [self.assetsLibrary assetForURL:[NSURL URLWithString:@""] resultBlock:^(ALAsset *asset) {
                DLog(@"test asset get success");
                dispatch_async(callerQ, ^{
                    callback();
                });
                
            } failureBlock:^(NSError *error) {
                
                DLog(@"test asset get failure");
                if(error.code == ALAssetsLibraryAccessUserDeniedError || error.code == ALAssetsLibraryAccessGloballyDeniedError)
                {
                    dispatch_async(callerQ, ^{
                        errorback([self getLibraryAccessDeniedError]);
                    });
                    
                }
                else {
                    dispatch_async(callerQ, ^{
                        errorback(error);
                    });
                }            
            }];
        }
    }];
}

-(void)readLibrary:(GenericCallback)callback:(GenericErrorback)errorback {
    DLog(@"started reading library");
    
    NSDate *startTime = [NSDate date];
    __block NSTimeInterval totalHashingTime = 0.0;
    __block NSTimeInterval md5HashingTime = 0.0;
    __block NSTimeInterval pHashingTime = 0.0;
    
    BOOL isFirstRun = [self.class isFirstRun];
    __block int fakeHashCounter = 0;
    
    [Util executeBlockOnMainQueue:^{
        [[EventManager getInstance] fireEvent:LOCAL_LIB_PARSE_BEGAN];
    }];
    
    NSDateFormatter* exifDateFormat = [[NSDateFormatter alloc] init];
    [exifDateFormat setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
    
    NSMutableDictionary *albumIDToGroup = [NSMutableDictionary dictionary];
    NSMutableDictionary *albumIDToAssetURLs = [NSMutableDictionary dictionary];
    NSMutableDictionary *assetURLToAssetInfo = [NSMutableDictionary dictionary];
    
    NSMutableSet *allAlbumIDs = [NSMutableSet set];
    NSMutableSet *allAssetURLs = [NSMutableSet set];
    
    ALAssetsLibraryGroupsEnumerationResultsBlock assetsLibraryEnumerator = ^(ALAssetsGroup *group, BOOL *stop) {
        if(group) {
            
            [Util executeBlockOnMainQueue:^{
                [[EventManager getInstance] fireEvent:LOCAL_LIB_PARSING_ALBUM, group];
            }];
            
            ALAssetsGroupInfo *groupInfo = [ALAssetsGroupInfo fromAssetsGroup:group];
            [albumIDToGroup setObject:groupInfo forKey:groupInfo.devicePersistentID];
            
            [allAlbumIDs addObject:groupInfo.devicePersistentID];
            
            NSMutableArray *assetURLsArray = [NSMutableArray array];
            [albumIDToAssetURLs setObject:assetURLsArray forKey:groupInfo.devicePersistentID];
            
            [self.assetGroupIDToGroupURL setObject:groupInfo.url forKey:groupInfo.devicePersistentID];
            
            [group enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop) {
                if(asset) {
                    if(![[asset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
                        DLog(@"ignoring non-photo");
                        return;
                    }
                    
                    [Util executeBlockOnMainQueue:^{
                        [[EventManager getInstance] fireEvent:LOCAL_LIB_PARSING_PHOTO, group, asset];
                    }];
                    
                    ALAssetInfo *assetInfo = [ALAssetInfo new];
                    
                    @autoreleasepool {
                        NSString *assetURL = [[asset defaultRepresentation].url absoluteString];
                        [allAssetURLs addObject:assetURL];
                    
                        NSString *assetHash = [self.assetURLToHash objectForKey:assetURL];
                        if(!assetHash || [assetHash rangeOfString:@"_"].location == 0) {
                            NSDate *hashStartTime = [NSDate date];
                            if(isFirstRun) {
                                assetHash = [NSString stringWithFormat:@"_%d", fakeHashCounter++];
                            }
                            else {
                                assetHash = [self.class getMD5DigestForAsset:asset];
                            }
                            
                            NSTimeInterval delta = [[NSDate date] timeIntervalSinceDate:hashStartTime];
                            totalHashingTime += delta;
                            md5HashingTime += delta;
                            
                            if(assetHash) {
                                [self.assetHashToURL setObject:assetURL forKey:assetHash];
                                [self.assetURLToHash setObject:assetHash forKey:assetURL];
                            }
                        }
                        
                        NSString *assetPHash = [self.assetURLToPHash objectForKey:assetURL];
                        if(!assetPHash || assetPHash.length == 1) {
                            NSDate *hashStartTime = [NSDate date];
                            if(isFirstRun) {
                                assetPHash = @"_";
                            }
                            else {
                                assetPHash = [ImageHasher getHashForALAsset:asset];
                            }
                            
                            NSTimeInterval delta = [[NSDate date] timeIntervalSinceDate:hashStartTime];
                            totalHashingTime += delta;
                            pHashingTime += delta;
                            
                            if(assetPHash) {
                                [self.assetURLToPHash setObject:assetPHash forKey:assetURL];
                            }
                        }
                        
                        CGSize thumbSize = [Util getALAssetThumbSize:asset];
                        CGSize size = [Util getALAssetSize:asset];
                        
                        assetInfo.hash = assetHash;
                        assetInfo.pHash = assetPHash;
                        assetInfo.url = assetURL;
                        assetInfo.size = [NSValue valueWithCGSize:size];
                        assetInfo.thumbSize = [NSValue valueWithCGSize:thumbSize];
                        assetInfo.location = [asset valueForProperty:ALAssetPropertyLocation];
                        
                        //default to aset ceation date
                        assetInfo.date = [asset valueForProperty:ALAssetPropertyDate];
                        self.assetURLToModTime[assetURL] = @([assetInfo.date timeIntervalSince1970]);
                        
                        //this is extremely slow
                        /*
                        if(!isFirstRun) {
                            NSDictionary *metadata = [[asset defaultRepresentation] metadata];
                            NSDictionary *exifData = metadata[(NSString*)kCGImagePropertyExifDictionary];
                            //no timezone info :(
                            if(exifData) {
                                NSString *exifDateString = exifData[(NSString*)kCGImagePropertyExifDateTimeOriginal];
                                if(!exifDateString) {
                                    exifDateString = exifData[(NSString*)kCGImagePropertyExifDateTimeDigitized];
                                }
                                if(exifDateString) {
                                    NSDate *exifDate = [exifDateFormat dateFromString:exifDateString];
                                    if(exifDate) {
                                        assetInfo.date = exifDate;
                                    }
                                }
                                
                            }
                        }*/
                        
                        [assetURLsArray addObject:assetURL];
                        [assetURLToAssetInfo setObject:assetInfo forKey:assetURL];
                     }
                
                    [Util executeBlockOnMainQueue:^{
                        [[EventManager getInstance] fireEvent:LOCAL_LIB_PARSED_PHOTO, asset];
                    }];
                }
                else {
                    [Util executeBlockOnMainQueue:^{
                        [[EventManager getInstance] fireEvent:LOCAL_LIB_PARSED_ALBUM, group];
                    }];
                }
            }];
        }
        else {
            [Util executeBlockOnMainQueue:^{
                [[EventManager getInstance] fireEvent:LOCAL_LIB_PARSE_ENDED];
            }];
            
            DLog(@"parsing assets lib took: %f, total hashing time: %f, md5: %f, phash: %f", -[startTime timeIntervalSinceNow], totalHashingTime, md5HashingTime, pHashingTime);
            
            NSArray *libData = [NSArray arrayWithObjects:albumIDToGroup, albumIDToAssetURLs, assetURLToAssetInfo, nil];
            
            [self refreshCache:allAlbumIDs:allAssetURLs];
            
            DLog(@"ended reading library");
            callback(libData);
        }
    };
    
    ALAssetsLibraryAccessFailureBlock failureBLock = ^(NSError *error) {
        [Logger logError:@"error in ALAssetsLibrary::enumerateGroupsWithTypes" :error];
        if(errorback) {
            errorback(error);
        }
    };
    
    [self.assetsLibrary enumerateGroupsWithTypes:ACCEPTED_ASSET_GROUP_TYPES usingBlock:assetsLibraryEnumerator failureBlock:failureBLock];
}

-(void)getLibraryModifications:(NSDictionary*)albumIDToGroup :(NSDictionary*)albumIDToAssetURLs :(NSDictionary*)assetURLToAssetInfo:(GenericCallback)callback
{
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    __block typeof(self) bself = self;
    dispatch_async([CoreController getCoreDataBackgroundQueue], ^{
        [bself getLibraryModificationsHelper:albumIDToGroup :albumIDToAssetURLs :assetURLToAssetInfo :^(id data) {
            dispatch_async(callerQ, ^{
                callback(data);
            });
        }];
    });
}

-(void)getLibraryModificationsHelper:(NSDictionary*)albumIDToGroup :(NSDictionary*)albumIDToAssetURLs :(NSDictionary*)assetURLToAssetInfo:(GenericCallback)callback
{
    //NSAssert([NSThread isMainThread],@"PhotoLibraryReader.getLibraryModifications should happen on main thread");
    
    NSDate *startTime = [NSDate date];
    __block NSTimeInterval transientPhotoTime = 0.0;
    
    NSMutableArray *transientAlbums = [NSMutableArray array];
    NSMutableDictionary *existingAlbumPersistentIDToAlbum = [NSMutableDictionary dictionary];
    NSMutableDictionary *existingPersistentPhotoHasheToPhoto = [NSMutableDictionary dictionary];
    NSMutableSet *foundAlbumPersistentIDs = [NSMutableSet set];
    NSMutableSet *deletedPersistentAlbums = [NSMutableSet set];
    NSMutableSet *deletedPersistentPhotos = [NSMutableSet set];
    NSMutableArray *modifiedAlbums = [NSMutableArray array];
    
    
    for(Album *album in [[GraphManager getInstance] getAlbums]) {
        NSString *albumPersistentID = album.devicePersistentID;
        if(![album isCurrentDeviceAlbum]) {
            continue;
        }
        for(Photo *p in album.photos) {
            [existingPersistentPhotoHasheToPhoto setObject:p forKey:p.digest];
        }
        
        if(!albumPersistentID) {
            continue;
        }
        [existingAlbumPersistentIDToAlbum setObject:album forKey:albumPersistentID];
    }

    NSMutableArray *specialAlbums = [NSMutableArray array];
    
    NSMutableSet *namedAlbumPhotoHashes = [NSMutableSet set];
    for(NSString *albumID in albumIDToGroup) {
        
        ALAssetsGroupInfo *groupInfo = [albumIDToGroup objectForKey:albumID];
        
        [foundAlbumPersistentIDs addObject:groupInfo.devicePersistentID];
        
        Album *persistentAlbum = [existingAlbumPersistentIDToAlbum objectForKey:groupInfo.devicePersistentID];
        
        NSMutableSet *foundAssetHashes = [NSMutableSet set];
        
        NSMutableSet *persistentPhotoHashes = [NSMutableSet set];
        if(persistentAlbum) {
            for(Photo *photo in persistentAlbum.photos) {
                [persistentPhotoHashes addObject:[photo getHash]];
            }
        }
        
        Album *transientAlbum = [Album fromAssetGroup:groupInfo :NO];
        if([transientAlbum isSavedPhotosGroup]) {
            [specialAlbums addObject:transientAlbum];
        }
        else {
            [transientAlbums addObject:transientAlbum];
        }
        
        //look for modifications in existing device albums
        //currenly only name changes
        //only for non-default album, otherwise there will always be
        //a modfication since we don't save saved photo album
        if(![transientAlbum isSavedPhotosGroup] && persistentAlbum && ![transientAlbum.name isEqualToString:persistentAlbum.name])
        {
            persistentAlbum.name = transientAlbum.name;
            [modifiedAlbums addObject:persistentAlbum];
        }
        
        NSArray *assetURLs = [albumIDToAssetURLs objectForKey:albumID];
        for(NSString *assetURL in assetURLs) {
            ALAssetInfo *assetInfo = [assetURLToAssetInfo objectForKey:assetURL];
            
            //we assume that photos don't change, or at least we don't care
            //about any modifications made to any photo, if the url is the same
            //any change to the photo does not need to be updated in our system
            
            [foundAssetHashes addObject:assetInfo.hash];
            
            if(![transientAlbum isSavedPhotosGroup]) {
                [namedAlbumPhotoHashes addObject:assetInfo.hash];
            }
            
            if(persistentAlbum) {
                if([persistentPhotoHashes containsObject:assetInfo.hash]) {
                    continue;
                }
            }
            
            NSDate *t1 = [NSDate date];
            Photo *transientPhoto = [Photo fromALAsset:assetInfo];
            transientPhotoTime += [[NSDate date] timeIntervalSinceDate:t1];
            [transientPhoto setAlbum:transientAlbum];
            
        }
        
        //done iterating over all the photos
        if(persistentAlbum) {
            //if this album already existed in our graph, find all
            //exisitng photos that no longer exist and mark them
            //for deletion
            for(Photo *persistentPhoto in persistentAlbum.photos) {
                if([foundAssetHashes containsObject:[persistentPhoto getHash]]) {
                    continue;
                }
                [deletedPersistentPhotos addObject:persistentPhoto];
            }
        }
    }
    
    //collect all the photos in the special albums including those who
    //are a part of the user created albums
    NSMutableSet *specialAlbumPhotosHashes = [NSMutableSet set];
    for(Album *specialAlbum in specialAlbums) {
        NSOrderedSet *hashes = [specialAlbum.photos valueForKey:@"digest"];
        [specialAlbumPhotosHashes unionSet:[hashes set]];
    }
    
    //mark all existing persisted photos for deletion which are not available in
    //special albums
    for(NSString *hash in existingPersistentPhotoHasheToPhoto) {
        if(![specialAlbumPhotosHashes containsObject:hash]) {
            [deletedPersistentPhotos addObject:existingPersistentPhotoHasheToPhoto[hash]];
        }
    }
    
    
    //remove photos from saved photos album which exist in any named album
    //or which exist in an auto created album
    NSMutableArray *photosToRemove = [NSMutableArray array];
    for(Album *specialAlbum in specialAlbums) {
        for(Photo *photo in specialAlbum.photos) {
            if([namedAlbumPhotoHashes containsObject:photo.digest] || [existingPersistentPhotoHasheToPhoto objectForKey:photo.digest])
            {
                [photosToRemove addObject:photo];
            }
        }
    }
    
    for(Photo *photo in photosToRemove) {
        //TODO: do we need to do this? What about other relationships?
        //Do we establish any?
        [photo setAlbum:nil];
        [[photo managedObjectContext] deleteObject:photo];
    }
    
    for(Album *specialAlbum in specialAlbums) {
        if([specialAlbum.photos count] > 0) {
            [transientAlbums addObject:specialAlbum];
        }
    }
    
    //remove all old transient albums without any new photos
    NSMutableArray *newAlbums = [[transientAlbums filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(Album *transientAlbum, NSDictionary *bindings)
    {
        return [transientAlbum.photos count] > 0 || ![existingAlbumPersistentIDToAlbum objectForKey:transientAlbum.devicePersistentID];
    }]] mutableCopy];
    
    
    for(NSString *devicePersistentID in existingAlbumPersistentIDToAlbum) {
        if([foundAlbumPersistentIDs containsObject:devicePersistentID]) {
            continue;
        }
        Album *deletedAlbum = [existingAlbumPersistentIDToAlbum objectForKey:devicePersistentID];
        [deletedPersistentAlbums addObject:deletedAlbum];
    }
    
    //remove all deleted, added pairs in the same album that are similar to each other
    NSMutableDictionary *addedPhotoPHashesByAlbumID = [NSMutableDictionary dictionary];
    NSMutableDictionary *addedPhotoByPHash = [NSMutableDictionary dictionary];
    
    for(Album *newPhotosAlbum in newAlbums) {
        NSMutableSet *pHashes = [NSMutableSet set];
        for(Photo *newPhoto in newPhotosAlbum.photos) {
            [pHashes addObject:newPhoto.pHash];
            addedPhotoByPHash[newPhoto.pHash] = newPhoto;
        }
        
        addedPhotoPHashesByAlbumID[newPhotosAlbum.devicePersistentID] = pHashes;
    }
    
    NSMutableSet *deletedPhotosToRemove = [NSMutableSet set];
    NSMutableSet *addedPhotosToRemove = [NSMutableSet set];
    
    for(Photo *deletedPhoto in deletedPersistentPhotos) {
        Album *deletedPhotoAlbum = deletedPhoto.album;
        
        NSMutableSet *addedPhotoPHashes = addedPhotoPHashesByAlbumID[deletedPhotoAlbum.devicePersistentID];
        if(addedPhotoPHashes) {
            NSString *matchingPHash = [Util findSimilarPHash:deletedPhoto.pHash :addedPhotoPHashes];
            if(matchingPHash) {
                Photo *addedPhoto = addedPhotoByPHash[matchingPHash];
                
                [deletedPhoto setHash:addedPhoto.digest];
                [deletedPhoto setPHash:addedPhoto.pHash];
                
                [addedPhotosToRemove addObject:addedPhoto];
                [deletedPhotosToRemove addObject:deletedPhoto];
            }
        }
        
    }
    
    for(Photo *photoToRemove in addedPhotosToRemove) {
        photoToRemove.album = nil;
    }
    for(Photo *photoToRemove in deletedPhotosToRemove) {
        [deletedPersistentPhotos removeObject:photoToRemove];
    }
    
    NSMutableArray *newPhotoAlbumsToRemove = [NSMutableArray array];
    for(Album *newPhotosAlbum in newAlbums) {
        if(newPhotosAlbum.photos.count == 0 && [existingAlbumPersistentIDToAlbum objectForKey:newPhotosAlbum.devicePersistentID])
        {
            [newPhotoAlbumsToRemove addObject:newPhotosAlbum];
        }
    }
    for(Album * album in newPhotoAlbumsToRemove) {
        [newAlbums removeObject:album];
    }
    
    
    NSDictionary *deleteDescriptor = [NSDictionary dictionaryWithObjectsAndKeys:deletedPersistentAlbums, @"album", deletedPersistentPhotos, @"photo",  nil];
    
    NSDictionary *modDescriptor = [NSDictionary dictionaryWithObjectsAndKeys:newAlbums, @"add", deleteDescriptor, @"delete", modifiedAlbums, @"modify", nil];
    
    DLog(@"parsing assets lib took: %f", -[startTime timeIntervalSinceNow]);
    
    callback(modDescriptor);
}

-(void) getOrCreateLocalAssetsGroup:(NSString*)albumName :(ArrayCallback)callback :(GenericErrorback)errorback
{
    //this is an infrequently used call so it is ok to have a library access
    //check here
    
    [self validateLibraryAccess:^{
        [self getOrCreateLocalAssetsGroupPrivate:albumName :^(NSArray *args) {
            //try to merge photos from auto albums with same name into this
            //physical album. we do this even if this is not a newly created
            //album
            ALAssetsGroup *group = args[0];
            BOOL newlyCreated = ((NSNumber*)args[1]).boolValue;
            if(!newlyCreated) {
                callback(args);
            }
            else {
                [Album populateNewDeviceAlbum:group :^(NSNumber *photosAdded) {
                    if(photosAdded.intValue == 0) {
                        callback(args);
                    }
                    else {
                        [[PhotoLibraryOrganizer getInstance] sync:^{
                            callback(args);
                        } :^(NSError *error) {
                            errorback(error);
                        }];
                    }
                    
                } :^(NSError *error){
                    errorback(error);
                }];
            }
            
        } :^(NSError *error) {
            errorback(error);
        }];
    } :^(NSError *error){
        errorback(error);
    }];
}

-(void) getOrCreateLocalAssetsGroupPrivate:(NSString*)albumName :(ArrayCallback)callback :(GenericErrorback)errorback
{

    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    __weak typeof (self) bself = self;
    [self.assetsLibrary addAssetsGroupAlbumWithName:albumName resultBlock:^(ALAssetsGroup *group) {
        if(group) {
            dispatch_async(callerQ, ^{
                callback(@[group, @(YES)]);
            });
            return;
        }
        
        __block BOOL found = NO;
        
        [bself.assetsLibrary enumerateGroupsWithTypes:ACCEPTED_ASSET_GROUP_TYPES usingBlock:^(ALAssetsGroup *group, BOOL *stop)
        {
            if(group) {
                NSString *groupName = [group valueForProperty:ALAssetsGroupPropertyName];
                //DLog(@"album named: %@", groupName);
                
                if([groupName isEqualToString:albumName]) {
                    DLog(@"found album named: %@!!", groupName);
                    
                    *stop = YES;
                    found = YES;
                    
                    dispatch_async(callerQ, ^{
                        callback(@[group, @(NO)]);
                    });
                }
            }
            else if(!found) {
                //creating failed but no album with same name found either, weird situation
                //report unknown error
                dispatch_async(callerQ, ^{
                    errorback(nil);
                });
            }
            
        } failureBlock:^(NSError *error) {
            dispatch_async(callerQ, ^{
                errorback(error);
            });
        }];
        
    } failureBlock:^(NSError *error) {
        dispatch_async(callerQ, ^{
            errorback(error);
        });
    }];
}

-(NSString *) getAssetHash:(ALAsset *)asset {
    NSString *url = [asset defaultRepresentation].url.absoluteString;
    NSString *hash = [self.assetURLToHash objectForKey:url];
    if(hash == nil) {
        hash = [self.class getMD5DigestForAsset:asset];
        
        @synchronized(LIB_READ_SYNCHRONIZATION_KEY) {
            [self.assetURLToHash setValue:hash forKey:url];
            [self.assetHashToURL setValue:url forKey:hash];
        }
    }
    return hash;
}

-(NSString*) getAssetURLForHash:(NSString*)hash {
    NSString *rv = [self.assetHashToURL objectForKey:hash];
    if(!rv) {
        [Logger logError:@"missing url for hash" :nil];
    }
    return rv;
}

-(BOOL)assetExistsWithHash:(NSString*)hash {
    return [self.assetHashToURL objectForKey:hash] != nil;
}

-(void) getAssetByHash:(NSString *)hash callback:(ALAssetsLibraryAssetForURLResultBlock)callback errorBack:(ALAssetsLibraryAccessFailureBlock) errorback
{
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    dispatch_async([CoreController getCoreDataBackgroundQueue], ^(void) {
        [self getAssetByHashPrivate:hash callback:^(ALAsset *asset) {
            dispatch_async(callerQ, ^{
                callback(asset);
            });
        } errorBack:^(NSError *error) {
            dispatch_async(callerQ, ^{
                errorback(error);
            });
        }];
    });
}

-(void) getAssetByHashPrivate:(NSString *)hash callback:(ALAssetsLibraryAssetForURLResultBlock)callback errorBack:(ALAssetsLibraryAccessFailureBlock) errorback
{
    NSString *url = [self.assetHashToURL objectForKey:hash];
    if(url == nil) {
        [Logger logError:[NSString stringWithFormat:@"no cache entry for asset with hash: %@", hash] :nil];
        callback(nil);
        return;
    }
    
    NSURL *urlObj = [[NSURL alloc] initWithString:url];
    [self getAssetByURL:urlObj :callback :errorback];
    
}

-(void) getAssetByURL:(NSURL*)assetURL :(ALAssetsLibraryAssetForURLResultBlock)callback :(ALAssetsLibraryAccessFailureBlock)errorback
{
    
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    typeof(self) bself = self;
    [self.assetsLibrary assetForURL:assetURL resultBlock:^(ALAsset *asset) {
        if(!asset) {
            [bself onMissingAsset:assetURL.absoluteString];
        }
        
        dispatch_async(callerQ, ^{
            callback(asset);
        });
    } failureBlock:^(NSError *error) {
        dispatch_async(callerQ, ^{
            errorback(error);
        });
    }];
}

-(void)getAssetGroupForDevicePersistentID:(NSString*)devicePersistentID:(GenericCallback)callback:(GenericErrorback)errorback
{
    [self getAssetGroupForDevicePersistentIDPrivate:devicePersistentID :^(id data) {
        callback(data);
    } :^(NSError *error) {
        errorback(error);
    }];
}

-(void)getAssetGroupForDevicePersistentIDPrivate:(NSString*)devicePersistentID:(GenericCallback)callback:(GenericErrorback)errorback
{
    NSString *assetGroupURL = [self.assetGroupIDToGroupURL objectForKey:devicePersistentID];
    if(!assetGroupURL) {
        [Logger logError:[NSString stringWithFormat:@"getAssetGroupForDevicePersistenID, missing groupDevicePersistentID: %@", devicePersistentID] :nil];
        callback(nil);
        return;
    }
    
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    typeof(self) bself = self;
    [self.assetsLibrary groupForURL:[NSURL URLWithString:assetGroupURL] resultBlock:^(ALAssetsGroup *group) {
        
        if(!group) {
            [bself onMissingAssetGroup:devicePersistentID];
        }
        
        dispatch_async(callerQ, ^{
            callback(group);
        });
    } failureBlock:^(NSError *error){
        dispatch_async(callerQ, ^{
            errorback(error);
        });
    }];
    
}

+(BOOL)isFirstRun {
    NSNumber *storedVal = [Util getUserDefault:FIRST_RUN_PREF_KEY];
    return !storedVal || [storedVal boolValue];
}

+(void)setRunFinished {
    [Util setUserDefault:@(NO) forKey:FIRST_RUN_PREF_KEY];
}

+(NSString*)getHashForPhotoData:(NSData*)data {
    NSUInteger hashedContentSize = MIN(1000, data.length);
    Byte *buf = malloc(hashedContentSize);
    [data getBytes:buf range:NSMakeRange(MAX(0, data.length - hashedContentSize), hashedContentSize)];
    
    NSData *slicedDataToHash = [NSData dataWithBytesNoCopy:buf length:hashedContentSize freeWhenDone:NO];
    NSString *rv = [self getMD5Digest:slicedDataToHash];
    
    free(buf);
    return rv;
}

//src: http://mobiledevelopertips.com/core-services/create-md5-hash-from-nsstring-nsdata-or-file.html
+(NSString*)getMD5Digest:(NSData*)data {
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(data.bytes, data.length, md5Buffer);
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x",md5Buffer[i]];
    }
    return output;
}

+(NSString*)getMD5DigestForAsset:(ALAsset*)asset {
    //read default rep bytes instead of fullResolutionImage
    //it might be compressed and samller but still good for distinct
    //hashing
    //ref:http://stackoverflow.com/questions/8840307/set-maximum-size-for-cgimageref-from-alassetrepresentation
    ALAssetRepresentation *rep = [asset defaultRepresentation];
    long long repSize = rep.size;
    
    NSUInteger hashedContentSize = MIN(1000, repSize);
    
    Byte *buf = malloc(hashedContentSize);
    NSError *error = nil;
    
    NSUInteger bytesRead = [rep getBytes:buf fromOffset:MAX(0, repSize - hashedContentSize - 1) length:hashedContentSize error:&error];
    if(error) {
        free(buf);
        NSString *message = [NSString stringWithFormat:@"error in reading bytes of asset: %@", rep.url];
        [Logger logError:message :error];
        return nil;
    }
    if(bytesRead == 0) {
        free(buf);
        NSString *message = [NSString stringWithFormat:@"zero bytes read for asset: %@", rep.url];
        [Logger logError:message :error];
        return nil;
    }
    
    NSData *data = [NSData dataWithBytesNoCopy:buf length:hashedContentSize freeWhenDone:NO];
    NSString *rv = [self getMD5Digest:data];
    
    free(buf);
    return rv;
}

//=========callbacks=========//
-(void) onPhotoLibraryChange: (NSNotification *)notification {
    DLog(@"onPhotoLibraryChange!");
    [[EventManager getInstance] fireEvent:LOCAL_LIBRARY_MODIFIED, nil];
}


@end

