
//
//  AdDelegate.m
//  snappe
//
//  Created by Shashank on 26/02/13.
//
//

#import "AdDelegate.h"
#import "Macros.h"

@implementation AdDelegate

+(AdDelegate*) getInstance {
    static AdDelegate *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AdDelegate alloc] init];
    });
    return sharedInstance;
}

- (void)adView:(DFPBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    DLog(@"adView:didFailToReceiveAdWithError:%@", [error localizedDescription]);
}

- (void)adViewDidReceiveAd:(DFPBannerView *)bannerView {
    DLog(@"adViewDidReceiveAd");
}
@end
