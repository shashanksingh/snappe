//
//  AlbumFBUploadOp.h
//  snappe
//
//  Created by Shashank on 05/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExtOp.h"
#import "AlbumAccessor.h"

@interface AlbumUploadOp : BaseExtOp
-(id)initWithAlbums:(Album*)srcAlbum:(NSObject*)destAlbum;
@end
