//
//  SortManager.h
//  snappe
//
//  Created by Shashank on 21/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Macros.h"

@interface SortManager : NSObject
typedef enum {
    SMART, DATE, ALPHABET
} SortingType;

+(SortManager*) getInstance;
-(void)sort:(NSArray*)friendsAndAlbums :(NSManagedObject*)sortCriterion:(SortingType)type:(BOOL)comparableOnly:(ArrayCallback)callback:(GenericErrorback)errorback;

+(SortingType)getSortingTypePreference;
+(void)setSortingTypePreference:(SortingType)sortingType;
@end
