//
//  PhotoBatchUploader.h
//  snappe
//
//  Created by Shashank on 28/03/13.
//
//

#import "PhotoBatchProcessor.h"

@interface PhotoBatchUploader : PhotoBatchProcessor
+(void)upload:(NSArray*)photos:(GenericCallback)callback:(GenericErrorback)errorback;
@end
