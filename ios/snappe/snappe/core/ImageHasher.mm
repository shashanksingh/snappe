//
//  ImageHasher.m
//  snappe
//
//  Created by Shashank on 20/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ImageHasher.h"
#import <ImageIO/ImageIO.h>
#import "Util.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

static unsigned int SIMILARITY_DISTANCE_THRESHOLD = 20;

@implementation ImageHasher

+(NSString*) getHashForURL:(NSURL*)url {
    //@TODO: manage memory if this function is going to be used regularly
    CGImageSourceRef imageSource = CGImageSourceCreateWithURL((__bridge CFURLRef)url, NULL);
    CGImageRef image = CGImageSourceCreateImageAtIndex(imageSource, 0, NULL);
    
    CFRelease(imageSource);
    
    NSString *rv = [self getHash:image];
    CGImageRelease(image);
    return rv;
}

+(NSString*) getHashForPNGData:(NSData*)data {
    return [self getHashForImageData:data :PNG];
}

+(NSString*) getHashForJPEGData:(NSData*)data {
    return [self getHashForImageData:data :JPEG];
}

+(NSString*)getHashForImageData:(NSData*)data:(ImageType)type {
    CFDataRef imgData;
    CGDataProviderRef imgDataProvider;
    CGImageRef image;
    
    imgData = (__bridge_retained CFDataRef)data;
    imgDataProvider = CGDataProviderCreateWithCFData(imgData);
    CFRelease(imgData);
    
    if(type == PNG) {
        image = CGImageCreateWithPNGDataProvider(imgDataProvider, NULL, true, kCGRenderingIntentDefault);
    }
    else {
        image = CGImageCreateWithJPEGDataProvider(imgDataProvider, NULL, true, kCGRenderingIntentDefault);
    }
    
    CGDataProviderRelease(imgDataProvider);
    
    NSString* rv = [self getHash:image];
    CGImageRelease(image);
    return rv;
}

+(NSString*) getHashForData:(NSData *)data {
    ImageType type = [Util getImageTypeFromData:data];
    if(type == JPEG) {
        return [self getHashForJPEGData:data];
    }
    else if(type == PNG) {
        return [self getHashForPNGData:data];
    }
    DLog(@"warn uknown image type");
    return nil;
}

+(NSString*) getHashForALAsset:(ALAsset*)asset {
    return [self getHash:asset.aspectRatioThumbnail];
}

+(UInt8) getHashDistance:(UInt64)hash1 :(UInt64)hash2 {
    UInt8 count = 0;
    UInt64 zor = hash1 ^ hash2;
    while(zor > 0) {
        zor &= zor - 1;
        count++;
    }
    return count;
}

+(UInt8) getHashDistanceForStringHash:(NSString*)hash1 :(NSString*)hash2 {
    UInt64 hash1UInt = 0;
    UInt64 hash2UInt = 0;
    
    NSScanner *scanner = [[NSScanner alloc] initWithString:hash1];
    [scanner scanHexLongLong:&hash1UInt];
    
    scanner = [[NSScanner alloc] initWithString:hash2];
    [scanner scanHexLongLong:&hash2UInt];
    
    return [self getHashDistance:hash1UInt :hash2UInt];
}


+(BOOL) areSimilarImages:(NSString*)hash1 :(NSString*)hash2 {
    UInt64 hash1UInt = 0;
    UInt64 hash2UInt = 0;
    
    NSScanner *scanner = [[NSScanner alloc] initWithString:hash1];
    [scanner scanHexLongLong:&hash1UInt];
    
    scanner = [[NSScanner alloc] initWithString:hash2];
    [scanner scanHexLongLong:&hash2UInt];
    
    UInt8 distance = [self getHashDistance:hash1UInt :hash2UInt];
    BOOL rv = distance <= SIMILARITY_DISTANCE_THRESHOLD;
    if(rv) {
        //NSLog(@"hashes %@ and %@ found similar with distance: %d", hash1, hash2, distance);
    }
    return rv;
}

+(cv::Mat)cvMatFromUIImage:(UIImage*)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    //CGColorSpaceRelease(colorSpace);
    
    return cvMat;
}

//TODO: this works for now, I am not sure why. Might casue trouble with unexpected formats
+(cv::Mat*)cvMatFromUIImage:(CGImageRef)image:(CGFloat)width:(CGFloat)height:(int)channels
{
    CGRect imageRect = CGRectMake(0, 0, width, height);
    
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image);
    
    cv::Mat *cvMat = new cv::Mat(width, height, CV_8UC4); // 8 bits per component, 3/4 channels
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat->data,                 // Pointer to  data
                                                    width,                       // Width of bitmap
                                                    height,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat->step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaPremultipliedLast
                                                    | kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextSetInterpolationQuality(contextRef, kCGInterpolationHigh);
    CGContextDrawImage(contextRef, imageRect, image);
    CGContextRelease(contextRef);
    //CGColorSpaceRelease(colorSpace);
    return cvMat;
}

+(float)median:(std::vector<float>)array {
    std::vector<float> copy(array);
    std::sort(copy.begin(), copy.end());
    if(copy.size() % 2 != 0) {
        return copy.at(copy.size()/2);
    }
    else {
        return (copy.at(copy.size()/2) + copy.at((copy.size()/2) + 1)) * 0.5;
    }
}

+(float)average:(std::vector<float>)array {
    float sum = 0.0f;
    for(int i=0; i<array.size(); i++) {
        sum += array.at(i);
    }
    return sum/array.size();
}

+(void)printMat:(cv::Mat)mat {
    for(int j=0; j<mat.rows; j++) {
        for(int i=0; i<mat.cols; i++) {
            std::cout << (float)mat.at<float>(j,i) << " ";
        }
        std::cout<<std::endl;
    }
}

+(NSString*)getHash:(CGImageRef)image {
    cv::Mat mat = [self cvMatFromUIImage:[UIImage imageWithCGImage:image]];
    
    //TODO:what if the original color space is not RGB?
    cv::cvtColor(mat, mat, CV_RGB2YCrCb);
    
    cv::Mat Y(mat.rows, mat.cols, CV_8UC1);
    cv::Mat crcb(mat.rows, mat.cols, CV_8UC2);
    cv::Mat output[] = {Y, crcb};
    
    int fromTo[] = {0,0, 1,1, 2,2};
    cv::mixChannels(&mat, 1, output, 2, fromTo, 3);
    
    Y.convertTo(Y, CV_32F);
    
    cv::Mat meanFilter(7, 7, CV_32FC1);
    meanFilter.setTo(1.0f);

    cv::filter2D(Y, Y, -1, meanFilter);
    
    cv::resize(Y, Y, cv::Size(32, 32));
    
    cv::dct(Y, Y);
    
    cv::Rect cropArea(1, 1, 8, 8);
    Y = Y(cropArea);
    
    std::vector<float> pixelArray;
    for(int y=0; y<Y.rows; y++) {
        for(int x=0; x<Y.cols; x++) {
            pixelArray.push_back(Y.at<float>(y, x));
        }
    }
    
    float median = [self median:pixelArray];
    
    UInt64 one = 0x0000000000000001;
    UInt64 hash = 0x0000000000000000;
    for (int i=0;i< 64;i++){
        float current = pixelArray.at(i);
        if (current > median)
            hash |= one;
        one = one << 1;
    }
    
    return [NSString stringWithFormat:@"%llx", hash];
    
}

@end
