//
//  AlbumDownloadOperation.m
//  snappe
//
//  Created by Shashank on 02/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AlbumDownloadOperation.h"
#import "EventManager.h"
#import "GraphManager.h"
#import "ExtendedOperationAccessor.h"
#import "Util.h"
#import "Logger.h"
#import "PhotoLibraryOrganizer.h"

#define SRC_ALBUM_ID_KEY @"src_album_id"
#define DEST_ALBUM_ID_KEY @"dest_album_id"
#define DEST_ALBUM_NAME_KEY @"dest_album_name"
#define PHOTO_ID_TO_STATUS_MAP_KEY @"photo_id_to_status"

@interface AlbumDownloadOperation()
@property (retain) Album *src;
@property (retain) NSObject *dest;
@end

@implementation AlbumDownloadOperation

-(id)initWithAlbums:(Album*)srcAlbum:(NSObject*)dest {
    self = [super init];
    if(self) {
        
        self.src = srcAlbum;
        self.dest = dest;
        
        [self setUp];
    }
    return self;
}

-(id)initWithPersistedState:(NSDictionary*)persistedState {
    self = [super initWithPersistedState:persistedState];
    if(self) {
        NSString *srcAlbumId = [persistedState objectForKey:SRC_ALBUM_ID_KEY];
        self.src = [Album getById:srcAlbumId];
        if(!self.src) {
            DLog(@"warning: error in getting album download op from persistent state, invalid album id: %@", srcAlbumId);
            return nil;
        }
        
        NSString *destAlbumId = [persistedState objectForKey:DEST_ALBUM_ID_KEY];
        if(destAlbumId) {
            self.dest = [Album getById:destAlbumId];
        }
        else {
            NSString *destAlbumName = [persistedState objectForKey:DEST_ALBUM_NAME_KEY];
            self.dest = destAlbumName;
        }
    }
    return self;
}

-(NSArray*)getPhotos {
    return [self.src.photos array];
}

-(NSArray*)getEventTypes {
    return @[@(PHOTO_DOWNLOAD)];
}

-(NSDictionary*)getState {
    NSMutableDictionary *state = [[super getState] mutableCopy];
    [state setObject:NSStringFromClass(self.class) forKey:@"class"];
    [state setObject:self.src.id forKey:SRC_ALBUM_ID_KEY];
    
    if([self.dest isKindOfClass:[Album class]]) {
        [state setObject:((Album*)self.dest).id forKey:DEST_ALBUM_ID_KEY];
    }
    else {
        [state setObject:self.dest forKey:DEST_ALBUM_NAME_KEY];
    }
    return state;
}

-(void)start:(GenericCallback)callback {
    self.status = STARTED;
    [self fireUpdate];
    
    VoidCallback onSuccess = ^{
        DLog(@"album %@ successfully downloaded", [self.src getName]);
        
        self.status = SUCCESSFUL;
        self.completion = 100;
        self.finishedOn = [NSDate date];
        
        [[PhotoLibraryOrganizer getInstance] sync:^{
            
            DLog(@"successfully synced with server after album download operation");
            
            callback(@(self.status));
            
            [self onFinish:YES];
            
        } :^(NSError *error) {
            
            [Logger logError:@"error in syncing after AlbumDownloadOperation" :error];
            
            callback(@(FAILURE));
            
            [self onFinish:NO];
        }];
    };
    
    GenericErrorback onError = ^(NSError *error_) {
        [Logger logError:[NSString stringWithFormat:@"error in downloading album %@", self.src ? [self.src getName] : @"unknown album"]:error_];
        
        [Util alertError:error_:@"Album download failed due to an unexpected error. Please try again later"];
        
        self.status = FAILURE;
        self.error = [error_ localizedDescription];
        self.finishedOn = [NSDate date];
        
        callback(@(self.status));
        
        [self onFinish:NO];
        
    };
    
    if([self.dest isKindOfClass:[Album class]]) {
        [self.src download :(Album*)self.dest :YES :onSuccess :onError];
    }
    else {
        [self.src downloadIntoAlbumByName:(NSString*)self.dest :YES :onSuccess :onError];
    }
    
}

-(NSString*)getName {
    return [NSString stringWithFormat:@"Downloading album \"%@\"", self.src.name];
}

-(NSString*)getType {
    return @"EXTOP_ALBUM_DOWNLOAD";
}


-(NSString*)getStatusText {
    NSString *statusText = nil;
    if(self.status == NOT_STARTED) {
        statusText = @"waiting...";    
    }
    else if(self.status == STARTED) {
        statusText = [NSString stringWithFormat:@"download %d%% completed", MIN(self.completion, 99)];
    }
    else if(self.status == SUCCESSFUL) {
        long secondsSinceDone = [self.finishedOn timeIntervalSince1970];
        statusText = [NSString stringWithFormat:@"successfully downloaded %@", [Util getRelativeTime:secondsSinceDone]];
    }
    else if(self.status == FAILURE) {
        statusText = @"failed, please try again";
    }
    else {
        DLog(@"warning: unknown value for ext op status: %d", self.status);
    }
    return statusText;
}

-(Notification*)getNotification {
    Notification *notification = [super getNotification];
    
    NSMutableDictionary *notificationData = notification.data;
    [notificationData setObject:self.src.id forKey:@"src_album_id"];
    //we need to provide album name so that we can show *some* info even
    //if the album is deleted afterwards
    [notificationData setObject:self.src.name forKey:@"src_album_name"];
    
    NSString *destAlbumName = [self getDestName];
    [notificationData setObject:destAlbumName forKey:@"dest_album_name"];
    
    return notification;
}

-(NSString*)getDestName {
    return [self.dest isKindOfClass:[Album class]] ? ((Album*)self.dest).name : (NSString*)self.dest;
}
@end

