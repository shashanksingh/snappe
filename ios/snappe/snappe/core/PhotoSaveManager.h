//
//  PhotoSaveManager.h
//  snappe
//
//  Created by Shashank on 21/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"
#import "PhotoAccessor.h"

@interface PhotoSaveManager : NSObject
+(PhotoSaveManager*) getInstance;
-(void) savePhoto:(NSData*)imageData :(NSString*)albumName :(NSDictionary*)metadata :(GenericCallback)callback :(GenericErrorback)errorback;
-(void)addPhotoToAlbum:(Photo*)photo:(Album*)album:(GenericCallback)callback:(GenericErrorback)errorback;
+(void)addLocalPhotosToAssetGroup:(ALAssetsGroup*)group:(NSArray*)assetURLs:(GenericCallback)callback:(GenericErrorback)errorback;
@end
