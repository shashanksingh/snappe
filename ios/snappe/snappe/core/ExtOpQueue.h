//
//  ExtOpQueue.h
//  snappe
//
//  Created by Shashank on 02/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExtOp.h"

@interface ExtOpQueue : NSObject
+(ExtOpQueue*) getInstance;
-(void)start;
-(void)save;
-(void)queue:(NSObject<ExtOp>*)op;
-(NSArray*)getNotifications;
@end
