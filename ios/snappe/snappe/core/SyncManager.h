//
//  SyncManager.h
//  snappe
//
//  Created by Shashank on 17/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserAccessor.h"
#import "AlbumAccessor.h"
#import "Macros.h"

@interface SyncManager : NSObject
+(SyncManager *) getInstance;

+(unsigned long) getLastSyncTime;

-(void) syncWithServerChangeSet:(NSDictionary *)json:(VoidCallback)callback:(GenericErrorback)errorback;
@end
