//
//  PhotoBatchFacebookUploader.m
//  snappe
//
//  Created by Shashank on 28/03/13.
//
//

#import "PhotoBatchFacebookUploader.h"
#import "EventManager.h"
#import "FBManager.h"
#import "AlbumAccessor.h"
#import "ExtendedOperationAccessor.h"

#define MAX_RETRIES 1

@implementation PhotoBatchFacebookUploader
+(PhotoBatchFacebookUploader*) getInstance {
    static PhotoBatchFacebookUploader *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PhotoBatchFacebookUploader alloc] init];
    });
    return sharedInstance;
}

+(void)upload:(NSArray*)photos:(NSObject*)target:(GenericCallback)callback:(GenericErrorback)errorback {
    
    if([target isKindOfClass:[NSString class]]) {
        [[FBManager getInstance] createAlbum:target :^(NSObject *destAlbumDescriptor){
            
            //created album json has the key "id"
            NSString *albumObjectID = [destAlbumDescriptor valueForKey:@"id"];
            
            [[self getInstance] uploadPhotosToNewlyCreatedAlbum:photos :albumObjectID :callback :errorback];
            
        } :errorback];
    }
    else {
        Album *targetAlbum = (Album*)target;
        [[self getInstance] uploadPhotosToExistingAlbum:photos :targetAlbum :callback :errorback];
    }
    

}

-(void)uploadPhotosToExistingAlbum:(NSArray*)photos:(Album*)destAlbum:(GenericCallback)callback:(GenericErrorback)errorback
{
    [destAlbum getOrFetchHashes:^{
        [self process:photos:@{@"destAlbumOrObjectID" : destAlbum}:callback:errorback];
    } :errorback];
}

-(void)uploadPhotosToNewlyCreatedAlbum:(NSArray*)photos:(NSString*)albumObjectID:(GenericCallback)callback:(GenericErrorback)errorback
{
    [self process:photos:@{@"destAlbumOrObjectID" : albumObjectID}:callback:errorback];
}


-(void)process:(Photo*)photo:(id)context {
    NSObject *destAlbumOrObjectID = [context valueForKey:@"destAlbumOrObjectID"];
    
    NSString *albumObjectID = nil;
    
    if([destAlbumOrObjectID isKindOfClass:[Album class]]) {
        Album *destAlbum = (Album*)destAlbumOrObjectID;
        
        albumObjectID = destAlbum.fbObjectID;
        
        
        //NSArray *photosToUpload = [bself getSortedAvailablePhotos];
        if(photo.markedPrivate.boolValue) {
            DLog(@"not uploading photo %@ to album %@ because it is private", photo.id, destAlbum);
            [[EventManager getInstance] fireEvent:PHOTO_UPLOAD, photo.id, @(SUCCESSFUL), nil];
            
            [self onPhotoDone:photo:nil];
            return;
        }
        
        if(photo.digest && [destAlbum hasPhotoWithHash:photo.digest]) {
            DLog(@"not uploading photo %@ to album %@ because they are identical", photo.id, destAlbum);
            [[EventManager getInstance] fireEvent:PHOTO_UPLOAD, photo.id, @(SUCCESSFUL), nil];
            
            [self onPhotoDone:photo:nil];
            return;
        }
        
        if(photo.pHash && [destAlbum hasSimilarPhoto:photo.pHash]) {
            DLog(@"not uploading photo %@ to album %@ because they are similar", photo.id, destAlbum);
            [[EventManager getInstance] fireEvent:PHOTO_UPLOAD, photo.id, @(SUCCESSFUL), nil];
            
            [self onPhotoDone:photo:nil];
            return;
        }
        
    }
    else {
        albumObjectID = (NSString*)destAlbumOrObjectID;
    }
    
    
    [[FBManager getInstance] uploadPhotoToAlbum:photo :albumObjectID :MAX_RETRIES :^(id data) {
        
        [[EventManager getInstance] fireEvent:PHOTO_UPLOAD, photo.id, @(SUCCESSFUL), nil];
        
        [self onPhotoDone:photo:nil];
        
    } :^(NSError *error) {
        
        [self onPhotoDone:photo :error];
        
    }];
    
}


@end
