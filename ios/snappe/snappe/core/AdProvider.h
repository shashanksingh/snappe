//
//  AdProvider.h
//  snappe
//
//  Created by Shashank on 12/01/13.
//
//

#import <Foundation/Foundation.h>

@interface AdProvider : NSObject
+(void)show:(UIView*)parent;
+(UIView*)getCustomAdView:(CGSize)size;
@end
