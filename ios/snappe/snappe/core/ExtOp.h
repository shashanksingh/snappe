//
//  ExtOp.h
//  snappe
//
//  Created by Shashank on 02/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"
#import "NotificationAccessor.h"
@protocol ExtOp
-(NSString*)getId;
-(id)initWithPersistedState:(NSDictionary*)persistedState;
-(NSDictionary*)getState;
-(void)start:(GenericCallback)callback;
-(Notification*)getNotification;
@end
