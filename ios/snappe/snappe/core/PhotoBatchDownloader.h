//
//  PhotoBatchDownloader.h
//  snappe
//
//  Created by Shashank on 28/03/13.
//
//

#import "PhotoBatchProcessor.h"

@interface PhotoBatchDownloader : PhotoBatchProcessor
+(void)download:(NSArray*)photos:(Album*)album:(NSSet*)existingHashes:(NSSet*)existingPHashes:(GenericCallback)callback:(GenericErrorback)errorback;
@end
