//
//  Util.h
//  snappe
//
//  Created by Shashank on 12/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "Macros.h"
#import "FBManager.h"
#import "SortManager.h"
#import "ExtOp.h"
#import "UIController.h"

typedef enum {
    PNG,
    JPEG,
    UNKNOWN
} ImageType;

@interface Util : NSObject
+(NSArray *) map:(NSArray *) array WithLambda:(id(^)(id))lambda;
+(NSInteger) indexOfMin:(NSArray *) array;
+(id) min:(NSArray *) array;
+(NSInteger) indexOfMax:(NSArray *) array;
+(id) max:(NSArray *) array;

+(NSArray*)splitArray:(NSArray*)array:(NSUInteger)chunkSize;

+(NSDate *) unixTimestampToDate:(NSNumber *)timestamp;

+(NSString *)getUUID;

+(id) getUserDefault:(NSString*)key;
+(void) setUserDefault:(id)value forKey:(NSString*)key;

+(UIView*) setViewSkin:(UIView*)view:(CachedUIViewType)skinType;
+(void) clearViewSkin:(UIView*)skinView:(CachedUIViewType)skinType;

+(UIImageView*)getBrokenImageView:(NSUInteger)width :(NSUInteger)height;
+(UIImageView*) getUIImageViewWithSkin:(NSURL*)url:(CachedUIViewType)skinType:(NSUInteger)width:(NSUInteger)height;

+(UIImage*)resizeUIImage:(UIImage*)image:(NSUInteger)width:(NSUInteger)height;
+(UIImage *) cropUIImage:(UIImage *)image :(NSUInteger)croppedWidth :(NSUInteger) croppedHeight;
+(UIImage *) cropUIImage:(UIImage *)image :(NSUInteger)croppedWidth :(NSUInteger) croppedHeight :(CGFloat)cropCenterX :(CGFloat)cropCenterY;

+(NSManagedObject*) copyManagedObjectToContext:(NSManagedObject*)sourceObject :(NSManagedObjectContext*)destinationContext;

+(ImageType)getImageTypeFromData:(NSData*)data;

+(NSString *)getRelativeTime:(long)epochTime;

+(void)setPrivateImageMask:(UIView*)imageView;
+(void)setSelectedImageMask:(UIView*)imageView;
+(void)clearImageMask:(UIView*)imageView;

+(CGPoint)getRectCenter:(CGRect)rect;
+(CGRect)centerRect:(CGRect)rect :(CGPoint)point;
+(CGRect)rectInset:(CGRect)rect:(NSInteger)top:(NSInteger)right:(NSInteger)bottom:(NSInteger)left;
+(CGFloat)getRectArea:(CGRect)rect;
+(CGFloat)getDistanceBetweenTwoPoints:(CGPoint)point1:(CGPoint)point2;

+(int)compareUnsignedIntegers:(unsigned int)a:(unsigned int)b;
+(int)absDiffUnsignedIntegers:(unsigned int)a:(unsigned int)b;

+(BOOL)appIsInBackground;

+(NSArray*)getNGrams:(NSString*)string:(NSUInteger)n;

+(BOOL)isEmptyDictionary:(NSDictionary*)dict;

+(BOOL)saveObjectInFile:(NSObject*)objectToSave:(NSString*)fileName;
+(NSObject*)readObjectFromFile:(NSString*)fileName;

+(void)executeExtOp:(NSObject<ExtOp>*)op;
+(void)executeExtOps:(NSArray*)ops;

+(BOOL)isSnappeErrorOfType:(NSError*)error:(SnappeErrorCode)code;
+(void)alertError:(NSError*)error:(NSString*)defaultMessage;

+(void)removeAllSubviews:(UIView*)view;
+(void)showPopup:(UIView*)parent:(UIView*)popupView:(BOOL)animate:(VoidCallback)callback;
+(void)showModalView:(UIView*)modalView;

+(void)logViewHierarchy:(UIView*)view;

+(NSString*)getSnappeErrorMessage:(NSError*)error:(NSString*)defaultMessage;

+(BOOL) isValidEmail:(NSString *)checkString;

+(void)executeBlockOnMainQueue:(VoidCallback)block;

+(CGSize)getALAssetThumbSize:(ALAsset*)asset;
+(CGSize)getALAssetSize:(ALAsset*)asset;

+(void)drawFastShadow:(UIView*)view;

+(NSDictionary*)getURLParameters:(NSURL*)url;
+(NSString*)getSnappeURLForID:(NSString*)id;

+(CGRect)getAppFrame;

+(UIView*)wrapUIView:(UIView*)wrapped;

+(BOOL)checkAndAlertAuth:(NSString*)action;

+(void)incrementNewPhotoCount:(Album*)album;

+(NSString*)findSimilarPHash:(NSString*)newPHash:(NSSet*)existingPHashes;

+(void)setUIViewHeight:(UIView*)view:(CGFloat)height;

+(NSString*)getNameForMultipleUsers:(NSArray*)users:(BOOL)html;
+(NSString*)getNameForMultipleUsers:(NSArray*)userIDs:(NSArray*)names:(BOOL)html;
+(NSString*)getNameForMultipleContacts:(NSArray*)contacts;

+(NSString*)ellipsify:(NSString*)s:(int)maxLength;

+(double)rand;
@end
