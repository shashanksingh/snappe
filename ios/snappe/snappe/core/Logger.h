//
//  Logger.h
//  snappe
//
//  Created by Shashank on 14/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Logger : NSObject
+(void)start;
+(void)logError:(NSString*)message:(NSError*)error;
+(void)track:(NSString*)event;
+(void)track:(NSString*)event:(NSDictionary*)properties;
@end
