//
//  Snappe.m
//  snappe
//
//  Created by Shashank on 11/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CoreController.h"
#import "Util.h"
#import "Lockbox.h"
#import "CreditsManager.h"
#import <dispatch/dispatch.h>

#define SUCCESSFUL_OPERATION_COUNT_KEY @"successful_operations_count"
#define APP_STORE_REVIEW_PROMPT_RESPONSE_KEY @"app_store_review_prompt_response"
#define LAST_APP_STORE_REVIEW_PROMPT_TIME_KEY @"last_app_store_review_prompt_time"

#define APPSTORE_PROMPT_MIN_SUCCESSFUL_OPERATIONS 3
#define APPSTORE_PROMPT_MIN_INTERVAL 86400 * 7

static dispatch_queue_t alAssetsBackgroundQueue;
static dispatch_queue_t coreDataBackgroundQueue;

@interface CoreController()
@property NSString *deviceID;
@property NSString *apnsToken;
@end

@implementation CoreController

+(CoreController *) getInstance {
    static CoreController *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[CoreController alloc] init];
    });
    return instance;
}

+(dispatch_queue_t)getALAssetsBackgroundQueue
{
    static dispatch_once_t initQueueOnceToken;
    
    dispatch_once(&initQueueOnceToken, ^{
        alAssetsBackgroundQueue = dispatch_queue_create("pe.snap.alassets_background_queue", 0);
    });
    return alAssetsBackgroundQueue;
}

+(dispatch_queue_t)getCoreDataBackgroundQueue
{
    static dispatch_once_t initQueueOnceToken;
    
    dispatch_once(&initQueueOnceToken, ^{
        coreDataBackgroundQueue = dispatch_queue_create("pe.snap.coredata_background_queue", 0);
    });
    return coreDataBackgroundQueue;
}

-(NSString*) getDeviceID {
    if(self.deviceID) {
        return self.deviceID;
    }
    
    NSString *key = @"snappe-device-id";
    self.deviceID = [Lockbox stringForKey:key];
    if(!self.deviceID) {
#if TARGET_IPHONE_SIMULATOR
        self.deviceID =  @"1";
#else
        self.deviceID = [Util getUUID];
#endif
        [Lockbox setString:self.deviceID forKey:key];
    }
    return self.deviceID;

}

-(NSString*)getAPNSToken {
    return self.apnsToken;
}

-(void)setAPNSToken:(NSString*)token {
    self.apnsToken = token;
}

-(int)getSuccessfulOperationCount {
    NSNumber *storedCount = [Util getUserDefault:SUCCESSFUL_OPERATION_COUNT_KEY];
    int count = 0;
    if(storedCount) {
        count = storedCount.intValue;
    }
    return count;
}

-(void)incrementSuccessfullOperationCount {
    int count = [self getSuccessfulOperationCount];
    [Util setUserDefault:@(count + 1) forKey:SUCCESSFUL_OPERATION_COUNT_KEY];
}

-(void)saveAppstoreReviewPromptResponse:(AppstoreReviewPromptResponse)response {
    [Lockbox setString:[NSString stringWithFormat:@"%d", response] forKey:APP_STORE_REVIEW_PROMPT_RESPONSE_KEY];
}

-(void)saveLastAppstoreReviewPromptTime {
    int time = round([[NSDate date] timeIntervalSince1970]);
    [Lockbox setString:[NSString stringWithFormat:@"%d", time] forKey:LAST_APP_STORE_REVIEW_PROMPT_TIME_KEY];
}

-(BOOL)shouldAskForAppstoreReview {
    //note that the user might not respond to prompt (close the app e.g.). Hence we might register
    //prompt time without registering the response to it
    NSString *lastPromptTimeString = [Lockbox stringForKey:LAST_APP_STORE_REVIEW_PROMPT_TIME_KEY];
    if(!lastPromptTimeString) {
        int successfulOperationsCount = [self getSuccessfulOperationCount];
        return successfulOperationsCount >= APPSTORE_PROMPT_MIN_SUCCESSFUL_OPERATIONS;
    }
    
    int lastPromptTime = lastPromptTimeString.intValue;
    int currentTime = round([[NSDate date] timeIntervalSince1970]);
    if(lastPromptTime + APPSTORE_PROMPT_MIN_INTERVAL > currentTime) {
        return NO;
    }
    
    NSString *response = [Lockbox stringForKey:APP_STORE_REVIEW_PROMPT_RESPONSE_KEY];
    if(!response) {
        return YES;
    }
    
    //denied when asked or clicked on review button when asked
    if(response.intValue == DENIED || response.intValue == REVIEWED) {
        return NO;
    }
    
    return YES;
}

-(BOOL)canShowAds {
#if DEBUG
    return YES;
#endif
    return ![CreditsManager isAdRemovalPurchased] && [self getSuccessfulOperationCount] >= 1;
}
@end
