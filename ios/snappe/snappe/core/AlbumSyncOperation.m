//
//  AlbumSyncOperation.m
//  snappe
//
//  Created by Shashank on 03/03/13.
//
//

#import "AlbumSyncOperation.h"
#import "EventManager.h"
#import "Logger.h"
#import "Util.h"
#import "PhotoLibraryOrganizer.h"
#import "SnappeHTTPService.h"

#define OWN_ALBUM_ID_KEY @"own_album_id"
#define OWN_ALBUM_NAME_KEY @"own_album_name"
#define FRIEND_ALBUM_ID_KEY @"friend_album_id"
#define FRIEND_ALBUM_NAME_KEY @"friend_album_name"

@interface AlbumSyncOperation()
@property (retain) Album *ownAlbum;
@property (retain) Album *friendAlbum;
@property (retain) NSString *ownAlbumName;
@property (retain) NSString *friendAlbumName;
@end

@implementation AlbumSyncOperation
-(id)init:(Album*)ownAlbum:(Album*)friendAlbum:(BOOL)bridge {
    self = [super init];
    if(self) {
        self.ownAlbum = ownAlbum;
        self.friendAlbum = friendAlbum;
        self.ownAlbumName = ownAlbum.name;
        self.friendAlbumName = friendAlbum.name;
        
        [self setUp];
    }
    return self;
}

-(id)initWithPersistedState:(NSDictionary*)persistedState {
    self = [super initWithPersistedState:persistedState];
    if(self) {
        NSString *ownAlbumID = [persistedState objectForKey:OWN_ALBUM_ID_KEY];
        NSString *friendAlbumID = [persistedState objectForKey:FRIEND_ALBUM_ID_KEY];
        
        self.ownAlbumName = [persistedState objectForKey:OWN_ALBUM_NAME_KEY];
        self.friendAlbumName = [persistedState objectForKey:FRIEND_ALBUM_NAME_KEY];
        
        self.ownAlbum = [Album getById:ownAlbumID];
        if(!self.ownAlbum) {
            DLog(@"missing own album with id %@", ownAlbumID);
        }
        else {
            self.ownAlbumName = self.ownAlbum.name;
        }
        
        self.friendAlbum = [Album getById:friendAlbumID];
        if(!self.friendAlbum) {
            DLog(@"missing friend album with id %@", friendAlbumID);
        }
        else {
            self.friendAlbumName = self.friendAlbum.name;
        }
        
        [self setUp];
    }
    return self;
}

-(NSArray*)getPhotos {
    NSMutableArray *rv = [NSMutableArray array];
    
    [rv addObjectsFromArray:[self.ownAlbum.photos array]];
    [rv addObjectsFromArray:[self.friendAlbum.photos array]];
    
    return rv;
}

-(NSArray*)getEventTypes {
    return @[@(PHOTO_UPLOAD), @(PHOTO_DOWNLOAD)];
}

-(NSDictionary*)getState {
    NSMutableDictionary *state = [[super getState] mutableCopy];
    [state setObject:NSStringFromClass(self.class) forKey:@"class"];
    [state setObject:self.ownAlbum.id forKey:OWN_ALBUM_ID_KEY];
    [state setObject:self.friendAlbum.id forKey:FRIEND_ALBUM_ID_KEY];
    [state setObject:self.ownAlbumName forKey:OWN_ALBUM_NAME_KEY];
    [state setObject:self.friendAlbumName forKey:FRIEND_ALBUM_NAME_KEY];
    
    return state;
}

-(void)start:(GenericCallback)callback {
    self.status = STARTED;
    [self fireUpdate];
    
    VoidCallback onSuccess = ^() {
        
        [Logger track:@"album_sync"];
        
        self.status = SUCCESSFUL;
        self.completion = 100;
        self.finishedOn = [NSDate date];
        
        [[PhotoLibraryOrganizer getInstance] sync:^{
            DLog(@"successfully synced with server after album share operation");
            
            callback(@(self.status));
            
            [self onFinish:YES];
            
        } :^(NSError *error) {
            [Logger logError:@"error in syncing after AlbumShareOperation" :error];
            
            callback(@(FAILURE));
            
            [self onFinish:NO];
            
        }];
    };
    
    GenericErrorback onError = ^(NSError *error_) {
        
        if(!([error_.domain isEqualToString:SNAPPE_ERROR_DOMAIN] && error_.code == SNAPPE_ERROR_INSUFFICIENT_CREDITS))
        {
            [Logger logError:[NSString stringWithFormat:@"error in syncing albums (own: %@, friend: %@)", self.ownAlbum.id, self.friendAlbum.id] :error_];
            
            //TODO: may be dial down the last sync time to allow repeat of the process without user
            //intervention
            NSString *userMessage = [NSString stringWithFormat:@"There was an error in uploading the new photos. Please add them again and resync"];
            
            [Util alertError:error_:userMessage];
            
        }
        
        self.status = FAILURE;
        self.error = [error_ localizedDescription];
        self.finishedOn = [NSDate date];
        
        callback(@(self.status));
        
        [self onFinish:NO];
        
    };
    
    
    [self.ownAlbum syncWith:self.friendAlbum :onSuccess :onError];
    

}

-(NSString*)getName {
    return [NSString stringWithFormat:@"Album Sync"];
}

-(NSString*)getType {
    return @"EXTOP_ALBUM_SYNC";
}

-(NSString*)getStatusText {
    NSString *statusText = nil;
    if(self.status == NOT_STARTED) {
        statusText = @"waiting...";
    }
    else if(self.status == STARTED) {
        statusText = [NSString stringWithFormat:@"syncing %d%% completed", MIN(self.completion, 99)];
    }
    else if(self.status == SUCCESSFUL) {
        long secondsSinceDone = [self.finishedOn timeIntervalSince1970];
        statusText = [NSString stringWithFormat:@"successfully synced %@", [Util getRelativeTime:secondsSinceDone]];
    }
    else if(self.status == FAILURE) {
        statusText = @"failed, please try again";
    }
    else {
        DLog(@"warning: unknown value for ext op status: %d", self.status);
    }
    return statusText;
}

-(Notification*)getNotification {
    Notification *notification = [super getNotification];
    
    NSMutableDictionary *notificationData = notification.data;
    [notificationData setObject:self.ownAlbum.id forKey:@"own_album_id"];
    [notificationData setObject:self.friendAlbum.id forKey:@"friend_album_id"];
    [notificationData setObject:self.ownAlbumName forKey:@"own_album_name"];
    [notificationData setObject:self.friendAlbumName forKey:@"friend_album_name"];
    
    return notification;
}


@end
