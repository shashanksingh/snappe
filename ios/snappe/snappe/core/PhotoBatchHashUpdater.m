//
//  PhotoBatchHashUpdater.m
//  snappe
//
//  Created by Shashank on 28/03/13.
//
//

#import "PhotoBatchHashUpdater.h"
#import "PhotoHashUpdater.h"

@implementation PhotoBatchHashUpdater

+(PhotoBatchHashUpdater*) getInstance {
    static PhotoBatchHashUpdater *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PhotoBatchHashUpdater alloc] init];
    });
    return sharedInstance;
}

+(void)update:(NSArray*)photos:(GenericCallback)callback:(GenericErrorback)errorback {
    [[self getInstance] process:photos:nil:callback:errorback];
}

-(void)process:(Photo*)photo:(id)context {
    [photo getOrFetchHashes:^{
        [self onPhotoDone:photo :nil];
        [[PhotoHashUpdater getInstance] add:photo.id :photo.digest :photo.pHash];
    } :^(NSError *error) {
        [self onPhotoDone:photo :error];
    }];
}
@end
