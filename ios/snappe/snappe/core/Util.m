 //
//  Util.m
//  snappe
//
//  Created by Shashank on 12/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Util.h"
#import "Macros.h"
#import "AppDelegate.h"
#import "PhotoLibraryReader.h"
#include <libkern/OSByteOrder.h>
#include "UIImageView+AFNetworking.h"
#import <QuartzCore/QuartzCore.h>
#include <stdlib.h>
#import "ProgressView.h"
#import "RootViewController.h"
#import "UIController.h"
#import "EventManager.h"
#import "ExtOpQueue.h"
#import "ExtendedOperationAccessor.h"
#import "ModalAlertView.h"
#import "Logger.h"
#import "LoadingIndicator.h"
#import "UIImage+Resize.h"
#import "GraphManager.h"
#import "ImageHasher.h"
#import "UserAccessor.h"

#define ANIMATING_ELLIPSES_ANIMATION_KEY @"__snappe__ellipsese_animation_key"

static unsigned int PRIVATE_MASK_TAG = 1 >> 4;


@implementation Util
+(NSArray *) map:(NSArray *) array WithLambda:(id(^)(id))lambda {
    NSMutableArray *rv = [[NSMutableArray alloc] initWithCapacity: [array count]];
    for(id element in array) {
        [rv addObject:lambda(element)];
    }
    return rv;
}
+(NSInteger) indexOfMin:(NSArray *) array {
    if(![array count]) return -1;
    id currMin = [array objectAtIndex:0];
    NSInteger rv = 0;
    
    for(unsigned i = 1; i < [array count]; i++) {
        id currObj = [array objectAtIndex:i];
        if(currMin > currObj) {
            currMin = currObj;
            rv = i;
        }
    }
    return rv;
};

+(id) min:(NSArray *) array {
    NSInteger indexOfMin = [self indexOfMin:array];
    if (indexOfMin < 0) return nil;
    return [array objectAtIndex:indexOfMin];
}

+(NSInteger) indexOfMax:(NSArray *) array {
    if(![array count]) return -1;
    id currMax = [array objectAtIndex:0];
    NSInteger rv = 0;
    
    for(unsigned i = 1; i < [array count]; i++) {
        id currObj = [array objectAtIndex:i];
        if(currMax < currObj) {
            currMax = currObj;
            rv = i;
        }
    }
    return rv;
}

+(id) max:(NSArray *) array {
    NSInteger indexOfMax = [self indexOfMax:array];
    if (indexOfMax < 0) return nil;
    return [array objectAtIndex:indexOfMax];
}

+(NSArray*)splitArray:(NSArray*)array:(NSUInteger)chunkSize {
    NSMutableArray *rv = [NSMutableArray array];
    for(int i=0; i<array.count; i+= chunkSize) {
        NSRange subArrayRange = NSMakeRange(i, MIN(chunkSize, array.count - i));
        NSArray *subArray = [array subarrayWithRange:subArrayRange];
        [rv addObject:subArray];
    }
    return rv;
}

+(NSDate *) unixTimestampToDate:(NSNumber *)timestamp {
    return [NSDate dateWithTimeIntervalSince1970:[timestamp doubleValue]];
}

+(id) getUserDefault:(NSString*)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults valueForKey:key];
}

+(void) setUserDefault:(id)value forKey:(NSString*)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:value forKey:key];
    [defaults synchronize];
}

+(UIImageView*)getBrokenImageView:(NSUInteger)width :(NSUInteger)height {
    UIImageView *rv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    rv.image = [UIImage imageNamed:@"broken_image.png"];
    return rv;
}

+(UIView*) setViewSkin:(UIView*)view:(CachedUIViewType)skinType {
    UIView *skin = [[UIController getInstance] popCachedUIView:skinType];
    if(!skin) {
        if(skinType == LOADING_IMAGE_INDICATOR_DARK) {
            skin = [[LoadingIndicator alloc] init:YES];
        }
        else if(skinType == LOADING_IMAGE_INDICATOR_LIGHT) {
            skin = [[LoadingIndicator alloc] init:NO];
        }
        else if(skinType == PHOTO_PLACEHOLDER) {
            skin = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo_icon_small.png"]];
            skin.contentMode = UIViewContentModeCenter;
        }
        else if(skinType == PROFILE_PIC_PLACEHOLDER) {
            skin = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_silhouette.png"]];
        }
    }
    
    skin.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    [view addSubview:skin];
    
    return skin;
}

+(void) clearViewSkin:(UIView*)skinView:(CachedUIViewType)skinType {
    [[UIController getInstance] queueCachedUIView:skinView :skinType];
    [skinView removeFromSuperview];
}

+(UIImageView*) getUIImageViewWithSkin:(NSURL*)url:(CachedUIViewType)skinType:(NSUInteger)width:(NSUInteger)height {
    UIImageView *uiimageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    __weak UIImageView *uiimageViewWeakRef = uiimageView;
    
    UIView *skin = [Util setViewSkin:uiimageView :skinType];
    
    [uiimageView 
     setImageWithURLRequest:[NSURLRequest requestWithURL:url] 
     placeholderImage:nil
     success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
         image = [Util cropUIImage:image :width :height];
         [uiimageViewWeakRef setImage:image];
         [Util clearViewSkin:skin :skinType];
     }
     failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
         [uiimageViewWeakRef setImage:[UIImage imageNamed:@"broken_image.png"]];
         [Util clearViewSkin:skin :skinType];
         
         NSString *message = [NSString stringWithFormat:@"error getting image at url: %@ [response: %@]", url, response];
         [Logger logError:message :error];
     }
     ];
    
    return uiimageView;
}

+(UIImage*)resizeUIImage:(UIImage*)image:(NSUInteger)width:(NSUInteger)height {
    return [image resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(width, height) interpolationQuality:kCGInterpolationLow];
}

+(UIImage *) cropUIImage:(UIImage *)image :(NSUInteger)croppedWidth :(NSUInteger)croppedHeight {
    return [self cropUIImage:image :croppedWidth :croppedHeight :-1.0f :-1.0f];    
}

+(UIImage *) cropUIImage:(UIImage *)image :(NSUInteger)croppedWidth :(NSUInteger)croppedHeight :(CGFloat)cropCenterX :(CGFloat)cropCenterY
{    
    CGFloat imageWidth = image.size.width;
    CGFloat imageHeight = image.size.height;
    
    if(imageWidth <= croppedWidth && imageHeight <= croppedHeight) return image;
    
    if(cropCenterX < 0) cropCenterX = 0.5f;
    if(cropCenterY < 0) cropCenterY = 0.5f;
    
    CGFloat centerX = croppedWidth * cropCenterX;
    CGFloat centerY = croppedHeight * cropCenterY;
    
    CGFloat x = MAX(0, centerX - (croppedWidth)/2);
    CGFloat y = MAX(0, centerY - (croppedHeight)/2);
    
    CGRect croppedRect = CGRectMake(x, y, croppedWidth, croppedHeight);
    CGImageRef iref = CGImageCreateWithImageInRect([image CGImage], croppedRect);
    UIImage *rv = [UIImage imageWithCGImage:iref];
    CGImageRelease(iref);
    return rv;
}

+(NSString *)getUUID
{
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidStr = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    CFRelease(uuid);
    
    return uuidStr;
}

+(NSManagedObject*) copyManagedObjectToContext:(NSManagedObject*)sourceObject:(NSManagedObjectContext*)destinationContext 
{
    if([sourceObject managedObjectContext] == destinationContext) return sourceObject;
    NSEntityDescription *entity = [sourceObject entity];
    
    NSManagedObject *copy = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext: destinationContext];
    NSDictionary *newValues = [sourceObject dictionaryWithValuesForKeys:[[entity attributesByName] allKeys]];
    [copy setValuesForKeysWithDictionary:newValues];
    return copy;
    
}

+(ImageType)getImageTypeFromData:(NSData*)data {
    uint8_t *buffer = (Byte*)malloc(2);
    if(!buffer) {
        NSLog(@"out of memory while trying to getImageTypeFromData");
        return UNKNOWN;
    }
    [data getBytes:buffer length:2];
    if(buffer[0] == 255 && buffer[1] == 216) {
        free(buffer);
        return JPEG;
    }
    if(buffer[0] == 137 && buffer[1] == 80) {
        free(buffer);
        return PNG;
    }
    
    DLog(@"uknown image type, first two bytes: %d, %d", buffer[0], buffer[1]);
    free(buffer);
    return UNKNOWN;
}

//copied from http://stackoverflow.com/a/932130
+(NSString *)getRelativeTime:(long)epochTime {
    NSDate *otherDate = [NSDate dateWithTimeIntervalSince1970:epochTime];
    NSDate *todayDate = [NSDate date];
    
    double ti = [otherDate timeIntervalSinceDate:todayDate];
    ti = ti * -1;
    if(ti < 1) {
    	return @"just now";
    } else 	if (ti < 60) {
    	return @"less than a minute ago";
    } else if (ti < 3600) {
    	int diff = round(ti / 60);
    	return [NSString stringWithFormat:@"%d minute%@ ago", diff, diff <= 1 ? @"" : @"s"];
    } else if (ti < 86400) {
    	int diff = round(ti / 60 / 60);
    	return[NSString stringWithFormat:@"%d hour%@ ago", diff, diff <= 1 ? @"" : @"s"];
    } else if (ti < 2629743) {
    	int diff = round(ti / 60 / 60 / 24);
    	return[NSString stringWithFormat:@"%d day%@ ago", diff, diff <= 1 ? @"" : @"s"];
    } else {
    	return @"long time ago";
    }	
}

+(void)setPrivateImageMask:(UIView*)imageView {
    [self setImageMask:@"private_mask.png" :imageView];
}

+(void)setSelectedImageMask:(UIView*)imageView {
    [self setImageMask:@"selected_photo_mask.png" :imageView];
}

+(void)setImageMask:(NSString*)imageName:(UIView*)imageView {
    CGRect frame = CGRectMake(0, 0, imageView.bounds.size.width, imageView.bounds.size.height);
    UIImageView *maskImageView = [[UIImageView alloc] initWithFrame:frame];
    [maskImageView setTag:PRIVATE_MASK_TAG];
    [maskImageView setContentMode:UIViewContentModeScaleToFill];
    [maskImageView setBackgroundColor:UIColorFromRGBA(0x0, 0.45)];
    [maskImageView setImage:[UIImage imageNamed:imageName]];
    [imageView addSubview:maskImageView];
}

+(void)clearImageMask:(UIView*)imageView {
    for(UIView *subview in imageView.subviews) {
        if(subview.tag == PRIVATE_MASK_TAG) {
            [subview removeFromSuperview];
            break;
        }
    }
}

+(CGPoint)getRectCenter:(CGRect)rect {
    return CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
}

+(CGRect)centerRect:(CGRect)rect :(CGPoint)point {
    CGFloat deltaX = point.x - CGRectGetMidX(rect);
    CGFloat deltaY = point.y - CGRectGetMidY(rect);
    
    return CGRectOffset(rect, deltaX, deltaY);
}

+(CGRect)rectInset:(CGRect)rect:(NSInteger)top:(NSInteger)right:(NSInteger)bottom:(NSInteger)left {
    CGRect rv = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    rv.origin.x += left;
    rv.origin.y += top;
    rv.size.width -= left + right;
    rv.size.height -= top + bottom;
    
    return rv;
}

+(CGFloat)getRectArea:(CGRect)rect {
    //no infinites
    return rect.size.width * rect.size.height;
}

+(CGFloat)getDistanceBetweenTwoPoints:(CGPoint)point1:(CGPoint)point2 {
    CGFloat dx = point2.x - point1.x;
    CGFloat dy = point2.y - point1.y;
    return sqrt(dx*dx + dy*dy );
};

+(int)compareUnsignedIntegers:(unsigned int)a:(unsigned int)b {
    if(a == b) {
        return 0;
    }
    if(a > b) {
        return 1;
    }
    return -1;
}

+(int)absDiffUnsignedIntegers:(unsigned int)a:(unsigned int)b {
    if(a == b) {
        return 0;
    }
    if(a > b) {
        return a - b; 
    }
    return b - a;
}

+(BOOL)appIsInBackground {
    return [[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground;
}

+(NSArray*)getNGrams:(NSString*)string:(NSUInteger)n {
    
    NSMutableArray *nGrams = [[NSMutableArray alloc] init];
    if(!string || [string length] == 0) {
        return nGrams;
    }
    
    NSArray *words = [string componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];

    for(NSString *word in words) {
        if([word length] <= n) {
            [nGrams addObject:word];
            continue;
        }
        
        for(int i=0; i<=[word length] - n; ++i) {
            NSRange range = NSMakeRange(i, n);
            [nGrams addObject:[word substringWithRange:range]];
        }
    }
    return nGrams;
}

+(BOOL)isEmptyDictionary:(NSDictionary*)dict {
    BOOL nonEmpty = NO;
    for(NSString *key in dict) {
        NSArray *ids = [dict objectForKey:key];
        if([ids count] > 0) {
            nonEmpty = YES;
            break;
        }
    }
    return !nonEmpty;
}

+(NSString*)getObjectPersistenceFilePath:(NSString*)fileName {
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *filePath = [docsDir stringByAppendingPathComponent:fileName];
    return filePath;
}

+(BOOL)saveObjectInFile:(NSObject*)objectToSave:(NSString*)fileName {
    NSString *filePath = [self getObjectPersistenceFilePath:fileName];
    return [NSKeyedArchiver archiveRootObject:objectToSave toFile:filePath];
}

+(NSObject*)readObjectFromFile:(NSString*)fileName {
    NSString *filePath = [self getObjectPersistenceFilePath:fileName];
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    }
    return nil;
}

+(void)executeExtOp:(NSObject<ExtOp>*)op {
    [self executeExtOps:@[op]];
}

+(void)executeExtOps:(NSArray*)ops {
    ProgressView *progressView = [ProgressView show];
    
    __block NSString *onUpdateListenerID;
    __block int currentIndex = 0;
    
    onUpdateListenerID = [[EventManager getInstance] addEventListener:EXTOP_STATUS_CHANGED :^(va_list args){
        NSObject<ExtOp> *updateOp = va_arg(args, NSObject<ExtOp>*);
        [Util executeBlockOnMainQueue:^{
            if([[updateOp getId] isEqualToString:[ops[currentIndex] getId]]) {
                NSDictionary *state = [updateOp getState];
                ExtOpStatus status = [[state objectForKey:@"status"] intValue];
                if(status == SUCCESSFUL) {
                    if(currentIndex == ops.count - 1) {
                        [progressView setDone:YES];
                        [[EventManager getInstance] removeEventListener:onUpdateListenerID];
                    }
                    else {
                        currentIndex++;
                        [[ExtOpQueue getInstance] queue:ops[currentIndex]];
                    }
                }
                else if(status == FAILURE) {
                    [progressView setDone:NO];
                    [[EventManager getInstance] removeEventListener:onUpdateListenerID];
                }
                else {
                    NSString *statusText = [state objectForKey:@"status_text"];
                    [progressView setStatus:statusText];
                }
            }
        }];
        
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void)
    {
        [[ExtOpQueue getInstance] queue:ops[0]];
    });
    
}

+(void)alertError:(NSError*)error:(NSString*)defaultMessage {
    NSString *message = [self getSnappeErrorMessage:error :defaultMessage];
    [ModalAlertView showError:message];
}

+(void)removeAllSubviews:(UIView*)view {
    [view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
}

+(void)showPopup:(UIView*)parent:(UIView*)popupView:(BOOL)animate:(VoidCallback)callback {
    if(!parent) {
        RootViewController *rvc = [[UIController getInstance] getUIControl:ROOT_VIEW_CONTROLLER];
        parent = rvc.view;
        [parent endEditing:NO];
    }
    
    [parent addSubview:popupView];
    if(animate) {
        popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if(animate) {
            [UIView animateWithDuration:0.3/1.5 animations:^{
                popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3/2 animations:^{
                    popupView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3/2 animations:^{
                        popupView.transform = CGAffineTransformIdentity;
                    } completion:^(BOOL finished) {
                        if(callback) {
                            callback();
                        }
                    }];
                }];
            }];
        }
        else {
            if(callback) {
                callback();
            }
        }
    });
    
}
+(void)showModalView:(UIView *)modalView {
    [self showPopup:nil :modalView :NO: nil];
}

+(void)logViewHierarchy:(UIView*)view {
    NSLog(@"%@", view);
    for (UIView *subview in view.subviews) {
        [self logViewHierarchy:subview];
    }
}

+(NSString*)getSnappeErrorMessage:(NSError*)error:(NSString*)defaultMessage {
    if(!error) {
        return defaultMessage;
    }
    
    if(!error.domain || ![error.domain isEqualToString:SNAPPE_ERROR_DOMAIN]) {
        return defaultMessage;
    }
    
    NSInteger code = error.code;
    if(code == SNAPPE_ERROR_NETWORK_UNREACHABLE) {
        return @"You are not connected to the internet. Please try again later";
    }
    if(code == SNAPPE_ERROR_ALASSET_UNEXPECTED) {
        return @"Unexpected error occurred in modifying local photo albums. Please try again later";
    }
    
    return defaultMessage;
}

+(BOOL)isSnappeErrorOfType:(NSError*)error:(SnappeErrorCode)code {
    if(!error) {
        return NO;
    }
    
    if(!error.domain || ![error.domain isEqualToString:SNAPPE_ERROR_DOMAIN]) {
        return NO;
    }
    
    return error.code == code;
}

//http://stackoverflow.com/questions/3139619/check-that-an-email-address-is-valid-on-ios
+(BOOL) isValidEmail:(NSString *)checkString {
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(void)executeBlockOnMainQueue:(VoidCallback)block {
    dispatch_async(dispatch_get_main_queue(), block);
}

+(CGSize)getALAssetThumbSize:(ALAsset*)asset {
    CGImageRef thumb = asset.thumbnail;
    CGFloat thumbWidth = CGImageGetWidth(thumb);
    CGFloat thumbHeight = CGImageGetHeight(thumb);
    CGSize thumbSize = CGSizeMake(thumbWidth, thumbHeight);
    
    return thumbSize;
}

+(CGSize)getALAssetSize:(ALAsset*)asset {
    CGSize size;
    ALAssetRepresentation *rep = asset.defaultRepresentation;
    if([rep respondsToSelector:@selector(dimensions)]) {
        size = rep.dimensions;
    }
    else {
        NSDictionary *metadata = rep.metadata;
        NSNumber *width = [metadata objectForKey:@"PixelWidth"];
        NSNumber *height = [metadata objectForKey:@"PixelHeight"];
        
        if(width && height) {
            size = CGSizeMake([width floatValue], [height floatValue]);
        }
        else {
            CGImageRef fullImage = [asset.defaultRepresentation fullResolutionImage];
            CGFloat width = CGImageGetWidth(fullImage);
            CGFloat height = CGImageGetHeight(fullImage);
            size = CGSizeMake(width, height);
        }
    }
    return size;
}

+(void)drawFastShadow:(UIView*)view {
    view.layer.shadowPath = [UIBezierPath bezierPathWithRect:view.bounds].CGPath;
}

+(NSDictionary*)getURLParameters:(NSURL*)url {
    //http://stackoverflow.com/a/2225838
    NSString * q = [url query];
    NSArray * pairs = [q componentsSeparatedByString:@"&"];
    NSMutableDictionary * kvPairs = [NSMutableDictionary dictionary];
    for (NSString * pair in pairs) {
        NSArray * bits = [pair componentsSeparatedByString:@"="];
        NSString * key = [[bits objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        NSString * value = [[bits objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        [kvPairs setObject:value forKey:key];
    }
    return kvPairs;
}

+(NSString*)getSnappeURLForID:(NSString*)id {
    if(!id || id.length == 0) {
        id = @"0";
    }
    return [NSString stringWithFormat:@"%@://objects?id=%@", SNAPPE_URL_SCHEME, id];
}

+(CGRect)getAppFrame {
    CGRect rv = [UIScreen mainScreen].applicationFrame;
    return CGRectOffset(rv, -rv.origin.x, -rv.origin.y);
}

+(UIView*)wrapUIView:(UIView*)wrapped {
    UIView *wrapper = [[UIView alloc] initWithFrame:wrapped.frame];
    wrapped.frame = wrapped.bounds;
    if(wrapped.superview) {
        [wrapped.superview insertSubview:wrapper aboveSubview:wrapped];
        [wrapper addSubview:wrapped];
    }
    return wrapper;
}

+(BOOL)checkAndAlertAuth:(NSString*)action {
    if(![[FBManager getInstance] isAuthorized]) {
        NSString *m = [NSString stringWithFormat:@"Snappe needs you to be connected to Facebook to be able to %@. Please do so by clicking on the \"Connect with Facebook\" button!", action];
        [ModalAlertView showError:m];
        return NO;
    }
    return YES;
}

+(void)incrementNewPhotoCount:(Album*)album {
    //TODO: find a better way to sync count between threads
    
    int currentCount = album.newPhotosCount ? album.newPhotosCount.intValue : 0;
    int newCount = currentCount + 1;
    
    album.newPhotosCount = @(newCount);
    
    //if it is not main thread, do it on main thread as well,
    //UI elements will pick count from the main thread
    if(![NSThread isMainThread]) {
        NSString *albumID = album.id;
        
        [Util executeBlockOnMainQueue:^{
            Album *a = [Album getById:albumID];
            a.newPhotosCount = @(newCount);
        }];
    }
}

+(NSString*)findSimilarPHash:(NSString*)newPHash:(NSSet*)existingPHashes {
    for(NSString *existingPHash in existingPHashes) {
        if([ImageHasher areSimilarImages:existingPHash :newPHash]) {
            return existingPHash;
        }
    }
    return nil;
}

+(void)setUIViewHeight:(UIView*)view:(CGFloat)height {
    CGRect f = view.frame;
    f.size.height = height;
    view.frame = f;
}

+(NSString*)getNameForMultipleUsers:(NSArray*)users:(BOOL)html {
    if(users.count == 0) {
        return @"";
    }
    
    User *firstUser = users[0];
    NSString *firstUserName = firstUser.name;
    if(html) {
        firstUserName = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:firstUser.id], firstUser.name];
    }
    
    if(users.count == 1) {
        return firstUserName;
    }
    return [NSString stringWithFormat:@"%@ & %d others", firstUserName, users.count - 1];
}

+(NSString*)getNameForMultipleUsers:(NSArray*)userIDs:(NSArray*)names:(BOOL)html {
    if(userIDs.count == 0) {
        return @"";
    }
    
    NSString *firstUserID = userIDs[0];
    NSString *firstUserName = names[0];
    if(html) {
        firstUserName = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:firstUserID], firstUserName];
    }
    
    if(userIDs.count == 1) {
        return firstUserName;
    }
    return [NSString stringWithFormat:@"'%@' & %d others", firstUserName, userIDs.count - 1];
}


+(NSString*)getNameForMultipleContacts:(NSArray*)contacts {
    if(contacts.count == 0) {
        return @"";
    }
    
    Contact *firstContact = contacts[0];
    NSString *firstName = firstContact.name;
    if(!firstName || firstName.length == 0) {
        firstName = [firstContact getBestEmail];
    }
    
    if(contacts.count == 1) {
        return firstName;
    }
    return [NSString stringWithFormat:@"'%@' & %d others", firstName, contacts.count - 1];
}

//TODO: this won't work with all languages
+(NSString*)ellipsify:(NSString*)s:(int)maxLength {
    if(s.length <= maxLength) {
        return s;
    }
    return [NSString stringWithFormat:@"%@...", [s substringToIndex:maxLength - 3]];
}

+(double)rand {
    return ((double)arc4random()/ULONG_MAX);
}

@end
