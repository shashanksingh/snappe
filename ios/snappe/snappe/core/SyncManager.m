//
//  SyncManager.m
//  snappe
//
//  Created by Shashank on 17/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SyncManager.h"
#import "Macros.h"
#import "PhotoAccessor.h"
#import "AlbumAccessor.h"
#import "Util.h"
#import "GraphManager.h"
#import "SnappeHTTPService.h"
#import "LocationAccessor.h"
#import "TagAccessor.h"
#import "UserAccessor.h"
#import "EventManager.h"
#import "ExtendedOperationAccessor.h"
#import "Logger.h"
#import "CoreController.h"
#import "Macros.h"
#import "UploadSharedAlbumPhotosView.h"

static NSRecursiveLock *lock;

@interface SyncManager ()
@property BOOL isSyncing;
@end

@implementation SyncManager
@synthesize isSyncing;

-(id) init {
    self = [super init];
    if(self) {
        self.isSyncing = NO;
    }
    return self;
}

+(SyncManager *) getInstance {
    static SyncManager *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SyncManager alloc] init];
    });
    return sharedInstance;
}

/*
 * modify this method very carefully, make sure 
 * 1. locks and unlock are always balanced, check all exit paths
 * 2. lock only in after entering the background thread
*/
-(void) syncWithServerChangeSet:(NSDictionary *)json:(VoidCallback)callback:(GenericErrorback)errorback
{
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    dispatch_async([CoreController getCoreDataBackgroundQueue], ^{
        
        [lock lock];
        
        NSManagedObjectContext *context = [[GraphManager getInstance] getPersistentManagedObjectContext];
        unsigned long syncEndTime = ((NSNumber*)[json valueForKeyPath:@"end_time"]).unsignedLongValue;
        
        NSDictionary *graphChangeData = [json valueForKeyPath:@"graphChangeData"];
        NSArray *changes = [graphChangeData objectForKey:@"changes"];
        
        [Util executeBlockOnMainQueue:^{
            [[EventManager getInstance] fireEvent:LOCAL_GRAPH_UPDATE_BEGIN];
        }];
        
        
LOG_OPERATION_TIME_PROPS(@"database_sync",
        [self syncWithServerChangeSet:syncEndTime:graphChangeData :context :^(){
, @{@"changeset_size" : @(changes.count)});
    
            [lock unlock];
    
            [Util executeBlockOnMainQueue:^{
                [[EventManager getInstance] fireEvent:LOCAL_GRAPH_UPDATE_END, @(YES), nil];
            }];
    
            dispatch_async(callerQ, ^{
                callback();
            });
        }:^(NSError *e){
            
            [lock unlock];
            
            dispatch_async(callerQ, ^{
                [Util executeBlockOnMainQueue:^{
                    [[EventManager getInstance] fireEvent:LOCAL_GRAPH_UPDATE_END, @(NO), nil];
                }];
                errorback(e);
            });
        }];
    });
}

-(void) syncWithServerChangeSet:(unsigned long)syncEndTime:(NSDictionary *)json:(NSManagedObjectContext*)context:(VoidCallback)callback:(GenericErrorback)errorback
{
    DLog(@"syncing json: %lul", syncEndTime);
    DLog(@"syncing with server...");
    
    NSMutableDictionary *objectsById = [NSMutableDictionary dictionary];
    NSMutableArray *userChangeDescriptors = [NSMutableArray array];
    NSMutableArray *albumChangeDescriptors = [NSMutableArray array];
    NSMutableArray *photoChangeDescriptors = [NSMutableArray array];
    NSMutableArray *locationChangeDescriptors = [NSMutableArray array];
    NSMutableArray *tagChangeDescriptors = [NSMutableArray array];
    NSMutableArray *relationshipChangeDescriptors = [NSMutableArray array];
    NSMutableDictionary *photosToDownload = [NSMutableDictionary dictionary];
    NSMutableDictionary *photosToUpload = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *deletedObjectIds = [NSMutableDictionary dictionary];
    NSMutableSet *newAlbumIDs = [NSMutableSet set];
    
    NSArray *changes = [json objectForKey:@"changes"];
    //we need to make sure that changes happen in order. for example if old current location for
    //a user was deleted and a new one set we will end up with no current location if do update
    //before delete. essentially non-idempotent operations need to be done in order
    [changes sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *desc1, NSDictionary *desc2)
     {
         NSNumber *modTime1 = [desc1 objectForKey:@"__mod_on__"];
         NSNumber *modTime2 = [desc2 objectForKey:@"__mod_on__"];
         
         if(!modTime1) {
             modTime1 = @(syncEndTime);
         }
         if(!modTime2) {
             modTime2 = @(syncEndTime);
         }
         return [modTime1 compare:modTime2];
         
     }];
    
    NSMutableArray *objectIDs = [NSMutableArray array];
    
    for(NSDictionary *changeDescriptor in changes) {
        NSString *type = [changeDescriptor objectForKey:@"snappe_object_type"];
        
        NSString *objectID = [changeDescriptor objectForKey:@"id"];;
        BOOL isDeleted = [changeDescriptor objectForKey:@"__is_deleted__"] != nil;
        if(isDeleted) {
            [deletedObjectIds setObject:type forKey:objectID];
        }
        
        [objectIDs addObject:objectID];
        
        NSMutableArray *toAddTo = nil;
        
        if([@"user" isEqualToString:type]) {
            toAddTo = userChangeDescriptors;
        }
        else if([@"album" isEqualToString:type]) {
            toAddTo = albumChangeDescriptors;
        }
        else if([@"photo" isEqualToString:type]) {
            toAddTo = photoChangeDescriptors;
        }
        else if([@"location" isEqualToString:type]) {
            toAddTo = locationChangeDescriptors;
        }
        else if([@"tag" isEqualToString:type]) {
            toAddTo = tagChangeDescriptors;
        }
        else if([@"relationship" isEqualToString:type]) {
            toAddTo = relationshipChangeDescriptors;
        }
        
        if(toAddTo != nil) {
            [toAddTo addObject:changeDescriptor];
        }
        else {
            DLog(@"warning unknown object type: %@", type);
        }
    }
    
    NSArray *userAddUpdateDescriptors = [User createOrUpdateWithChangeSets:userChangeDescriptors InContext:context];
    for(NSArray *descriptor in userAddUpdateDescriptors) {
        User *user = descriptor[0];
        //BOOL *preExisting = ((NSNumber*)descriptor[1]).intValue;
        
        [self addToCache:[user getId] :user :objectsById];
    }
    
    NSArray *albumAddUpdateDescriptors = [Album createOrUpdateWithChangeSets:albumChangeDescriptors InContext:context];
    for(NSArray *descriptor in albumAddUpdateDescriptors) {
        Album *album = descriptor[0];
        BOOL *preExisting = ((NSNumber*)descriptor[1]).boolValue;
        if(!preExisting) {
            [newAlbumIDs addObject:[album getId]];
        }
        
        [self addToCache:[album getId] :album :objectsById];
    }
    
    NSArray *photoAddUpdateDescriptors = [Photo createOrUpdateWithChangeSets:photoChangeDescriptors InContext:context];
    for(NSArray *descriptor in photoAddUpdateDescriptors) {
        Photo *photo = descriptor[0];
        //BOOL *preExisting = ((NSNumber*)descriptor[1]).intValue;
        
        [self addToCache:[photo getId] :photo :objectsById];
    }
    
    NSArray *locationAddUpdateDescriptors = [Location createOrUpdateWithChangeSets:locationChangeDescriptors InContext:context];
    for(NSArray *descriptor in locationAddUpdateDescriptors) {
        Location *location = descriptor[0];
        //BOOL *preExisting = ((NSNumber*)descriptor[1]).intValue;
        
        [self addToCache:[location getId] :location :objectsById];
    }
    
    NSArray *tagAddUpdateDescriptors = [Tag createOrUpdateWithChangeSets:tagChangeDescriptors InContext:context];
    for(NSArray *descriptor in tagAddUpdateDescriptors) {
        Tag *tag = descriptor[0];
        //BOOL *preExisting = ((NSNumber*)descriptor[1]).intValue;
        
        [self addToCache:[tag getId] :tag :objectsById];
    }
    
    for(NSDictionary *changeDescriptor in relationshipChangeDescriptors) {
        NSString *relationshipID = [NSString stringWithFormat:@"%@", [changeDescriptor objectForKey:@"id"]];
        NSString *startNodeId = [NSString stringWithFormat:@"%@", [changeDescriptor objectForKey:@"start_node"]];
        NSString *endNodeId = [NSString stringWithFormat:@"%@", [changeDescriptor objectForKey:@"end_node"]];
        NSString *relationshipType = [changeDescriptor objectForKey:@"relationship_type"];
        BOOL isDeleted = [deletedObjectIds objectForKey:relationshipID] != nil;
        
        //DLog(@"objectById: %@", objectsById);
        
        if([@"FRIEND" isEqualToString:relationshipType]) {
            User *firstUser = [self getFromCache:User.class :startNodeId :objectsById];
            if(firstUser == nil) {
                DLog(@"warning: unknown User with id: %@", startNodeId);
                continue;
            }
            
            User *secondUser = [self getFromCache:User.class :endNodeId :objectsById];
            if(secondUser == nil) {
                DLog(@"warning: unknown User with id: %@", endNodeId);
                continue;
            }
            
            if(isDeleted) {
                [firstUser removeFriend:secondUser];
            }
            else {
                [firstUser addFriend:secondUser];    
            }
        
        }
        else if([@"USER_OWNS_ALBUM" isEqualToString:relationshipType]) {
            User *user = [self getFromCache:User.class :startNodeId :objectsById];
            if(user == nil) {
                DLog(@"warning: unknown User with id: %@", startNodeId);
                continue;
            }
            
            Album *album = [self getFromCache:Album.class :endNodeId :objectsById];
            if(album == nil) {
                DLog(@"warning: unknown album with id: %@", endNodeId);
                continue;
            }
            
            if(isDeleted) {
                [user removeOwnedAlbumObject:album];
            }
            else {
                [user addOwnedAlbumObject:album];
            }
        }
        else if([@"PHOTO_IN_ALBUM" isEqualToString:relationshipType]) {
            Photo *photo = [self getFromCache:Photo.class :startNodeId :objectsById];
            if(photo == nil) {
                DLog(@"warning: unknown Photo with id: %@", startNodeId);
                continue;
            }
            
            Album *album = [self getFromCache:Album.class :endNodeId :objectsById];
            if(album == nil) {
                DLog(@"warning: unknown Album with id: %@", endNodeId);
                continue;
            }
            //there is no need to delete this relationship here
            //cascading rules will take care of it
            if(!isDeleted) {
                
                NSString *photoSource = [changeDescriptor objectForKey:@"album_photo_source"];
                
                BOOL newPhotoInExistingAlbum = NO;
                if(![newAlbumIDs containsObject:[album getId]]) {
                    BOOL photoAlreadyInAlbum = photo.album && [photo.album.id isEqualToString:album.id];
                    if(!photoAlreadyInAlbum) {
                        newPhotoInExistingAlbum = YES;
                    }
                }
                
                //if a photo not from this device has been added to an album from the current
                //device save the photo on the device physically
                //for physical device albums this will add them to the physical album too
                if([album isCurrentDeviceAlbum]) {
                    
                    BOOL isPhotoFromThisDevice = [photoSource isEqualToString:@"DEVICE"];
                    BOOL toBeDownloaded = NO;
                    
                    if(![photo isLocalPhoto]) {
                        //photo not available locally definitely needs to be downloaded
                        //(and added to the corresponding album)
                        toBeDownloaded = YES;
                    }
                    else if([album isCurrentDevicePhysicalAlbum] && !isPhotoFromThisDevice) {
                        //for logical albums nothing to do since the photo is locally available
                        //for physical we might still need to add the photo to the actual album
                        //this will only be required if this relationship was not created by
                        //this device (but someone else)
                        toBeDownloaded = YES;
                    }
                    
                    if(toBeDownloaded) {
                        [self markPhotoForDownload:photosToDownload :photo :album];
                    }
                    else {
                        
                        //upload only if new photo was added by (this?) device to an existing album
                        if(newPhotoInExistingAlbum && isPhotoFromThisDevice) {
                            if([album isCurrentDeviceAlbum] && [photo isLocalPhoto] && [album isShared]) {
                                [photosToUpload setObject:photo.digest forKey:photo.id];
                            }
                            
                        }
                        
                        //add photo to album only if it is not to be downloaded
                        //photos to be downloaded will be added to the album after
                        //download
                        [photo setAlbum:album];
                        
                        
                    }
                }
                else {
                    [photo setAlbum:album];
                }
                
                
            }
            
        }
        else if([@"ALBUM_SHARED_PUSHED" isEqualToString:relationshipType]) {
            Album *album = [self getFromCache:Album.class :startNodeId :objectsById];
            if(album == nil) {
                DLog(@"warning: unknown Album with id: %@", startNodeId);
                continue;
            }
            
            User *friend = [self getFromCache:User.class :endNodeId :objectsById];
            if(friend == nil) {
                DLog(@"warning: unknown User with id: %@", endNodeId);
                continue;
            }
            
            if(isDeleted) {
                [album unsetIsSharedWith:friend];
            }
            else {
                [album setIsSharedWith:friend];
            }
        
        }
        else if([@"PHOTO_LOCATION" isEqualToString:relationshipType]) {
            Photo *photo = [self getFromCache:Photo.class :startNodeId :objectsById];
            if(photo == nil) {
                DLog(@"warning: unknown Photo with id: %@", startNodeId);
                continue;
            }
            
            Location *location = [self getFromCache:Location.class :endNodeId :objectsById];
            if(location == nil) {
                DLog(@"warning: unknown Location with id: %@", endNodeId);
                continue;
            }
            
            if(isDeleted) {
                [photo unsetLocation];
            }
            else {
                [photo setLocation:location];
            }
        }
        else if([@"USER_CURRENT_LOCATION" isEqualToString:relationshipType]) {
            User *user = [self getFromCache:User.class :startNodeId :objectsById];
            if(user == nil) {
                DLog(@"warning: unknown User with id: %@", startNodeId);
                continue;
            }
            
            Location *location = [self getFromCache:Location.class :endNodeId :objectsById];
            if(location == nil) {
                DLog(@"warning: unknown Location with id: %@", endNodeId);
                continue;
            }
            
            if(isDeleted) {
                [user unsetCurrentLocation];
            }
            else {
                [user setCurrentLocation:location];
            }
        }
        else if([@"USER_TAG" isEqualToString:relationshipType]) {
            User *user = [self getFromCache:User.class :startNodeId :objectsById];
            if(user == nil) {
                DLog(@"warning: unknown User with id: %@", startNodeId);
                continue;
            }
            
            Tag *tag = [self getFromCache:Tag.class :endNodeId :objectsById];
            if(tag == nil) {
                DLog(@"warning: unknown Tag with id: %@", endNodeId);
                continue;
            }
            
            if(isDeleted) {
                [user removeUserTag:tag];
            }
            else {
                [user addUserTag:tag];
            }
        
        }
        else if([@"TAG_LOCATION" isEqualToString:relationshipType]) {
            Tag *tag = [self getFromCache:Tag.class :startNodeId :objectsById];
            if(tag == nil) {
                DLog(@"warning: unknown Tag with id: %@", startNodeId);
                continue;
            }
            
            Location *location = [self getFromCache:Location.class :endNodeId :objectsById];
            if(location == nil) {
                DLog(@"warning: unknown Location with id: %@", endNodeId);
                continue;
            }
            //cascading rules will take care of delete
            if(!isDeleted) {
                [tag setLocation:location];
            }
        }
        else if([@"CAN_UPLOAD_TO_ALBUM" isEqualToString:relationshipType]) {
            User *user = [self getFromCache:User.class :startNodeId :objectsById];
            if(user == nil) {
                DLog(@"warning: unknown User with id: %@", startNodeId);
                continue;
            }
            
            Album *album = [self getFromCache:Album.class :endNodeId :objectsById];
            if(album == nil) {
                DLog(@"warning: unknown Album with id: %@", endNodeId);
                continue;
            }
            
            if(isDeleted) {
                [album unsetIsAllowUpload:user];
            }
            else {
                [album setIsAllowedUpload:user];
            }
        }
        else {
            DLog(@"unknow relationship type: %@", relationshipType);
        }
    }
    
    for(NSString *deletedObjectId in deletedObjectIds) {
        NSManagedObject *deletedObject = [objectsById objectForKey:deletedObjectId];
        
        NSString *type = [deletedObjectIds objectForKey:deletedObjectId];
        if(!deletedObject) {
            Class class = nil;
            if([@"user" isEqualToString:type]) {
                class = User.class;
            }
            else if([@"album" isEqualToString:type]) {
                class = Album.class;
            }
            else if([@"photo" isEqualToString:type]) {
                class = Photo.class;
            }
            else if([@"location" isEqualToString:type]) {
                class = Location.class;
            }
            else if([@"tag" isEqualToString:type]) {
                class = Tag.class;
            }
            else if([@"relationship" isEqualToString:type]) {
                //ignore, core-data takes care of deleting
                //appropriate relationships with its delete rules
            }
            else {
                DLog(@"warning: unknown deleted object type %@ with id %@", type, deletedObjectId);
            }
            
            if(class) {
                deletedObject = [self getFromCache:class :deletedObjectId :objectsById];
            }
        }
        
        if(deletedObject) {
            [GraphManager deleteObject:deletedObject];    
        }
        else {
            NSString *message = [NSString stringWithFormat:@"attempt to delete non-existent object of type %@ and id %@", type, deletedObjectId];
            [Logger logError:message :nil];
        }
    }
    
    __weak typeof(self) bself = self;
    [self downloadUploadedPhotos:photosToDownload:0:^(){
        [bself cleanupTempObjectsAfterPhotoDownload:photosToDownload];
        [bself saveContext:context :callback :errorback :syncEndTime :photosToUpload:0];
        
    }:errorback];
    
}
                   
-(void)saveContext:(NSManagedObjectContext*)context:(VoidCallback)callback:(GenericErrorback)errorback:(long)syncEndTime:(NSDictionary*)photosToUpload:(int)attempt
{
    [context MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if(error) {
            [Logger logError:@"error in syncing with server change set" :error];
            if(attempt == 0) {
                BOOL fixAttempted = [self fixIfValidationError:error];
                if(fixAttempted) {
                    [self saveContext:context :callback :errorback :syncEndTime :photosToUpload :attempt + 1];
                    return;
                }
            }
            
            errorback(error);
            return;
        }
        
        DLog(@"successfully saved local graph after sync");
        
        [self.class updateLastSyncTime:syncEndTime];
        [[EventManager getInstance] fireEvent:LOCAL_GRAPH_SYNCED];
        
        callback();
        
        if(photosToUpload.count > 0) {
            NSSet *hashes = [NSSet setWithArray:[photosToUpload allValues]];
            
            [[SnappeHTTPService getInstance] getPhotoHashesNeedingUpload:hashes :^(NSArray *data) {
                [Util executeBlockOnMainQueue:^{
                    NSSet *hashesNeedingUpload = [NSSet setWithArray:data];
                    NSMutableArray *photoIDsToUpload = [NSMutableArray array];
                    for(NSString *uploadCandidateID in photosToUpload) {
                        NSString *hash = [photosToUpload objectForKey:uploadCandidateID];
                        if([hashesNeedingUpload containsObject:hash]) {
                            [photoIDsToUpload addObject:uploadCandidateID];
                        }
                    }
                    
                    if(photoIDsToUpload.count > 0) {
                        [UploadSharedAlbumPhotosView show:photoIDsToUpload];
                    }
                }];
                
            } :^(NSError *error) {
                DLog(@"error in getting photos needing upload after sync");
                [Logger track:@"error_fetching_photos_needing_upload"];
            }];
            
        }
        
    }];
}

+(unsigned long) getLastSyncTime {
    NSNumber *lastSyncTime = [Util getUserDefault:@"LastSyncTime"];
    if(lastSyncTime == nil) {
        return 0;
    }
    return [lastSyncTime unsignedLongValue];
}

+(void) updateLastSyncTime:(unsigned long)ts {
    [Util setUserDefault:@(ts) forKey:@"LastSyncTime"];
}

-(void)downloadUploadedPhotos:(NSDictionary*)photosToDownload:(NSUInteger)index:(VoidCallback)callback:(GenericErrorback)errorback
{
    if(index >= [photosToDownload count]) {
        //@TODO: ensure that re-parsing of local lib happens after this
        DLog(@"successfully downloaded uploaded photos in %d albums", index);
        callback();
        return;
    }
    
    NSString *albumID = [[[photosToDownload allKeys] sortedArrayUsingSelector: @selector(compare:)] objectAtIndex:index];
    Album *albumToDownloadTo = [Album getById:albumID];
    if(!albumToDownloadTo) {
        [self downloadUploadedPhotos:photosToDownload :index+1 :callback :errorback];
        return;
    }
    
    NSArray *photos = [photosToDownload objectForKey:albumID];
    [Album download:photos :albumToDownloadTo :YES :^{
        for(Photo *p in photos) {
            [p setAlbum:albumToDownloadTo];
        }
        
        DLog(@"successfully downloaded %d uploaded photos for album: %@", [photos count], albumToDownloadTo.id);
        [self downloadUploadedPhotos:photosToDownload :index + 1:callback:errorback];
        
    } :^(NSError *error) {
        
        NSString *message = [NSString stringWithFormat:@"error in downloading %d uploaded photos for the album: %@", [photos count], albumToDownloadTo.id];
        [Logger logError:message :error];
        
        //don't notify user of the error as the user did not explicitly start the action
        errorback(error);
    }];
}

-(void)markPhotoForDownload:(NSMutableDictionary*)photosToDownload:(Photo*)photo:(Album*)album {
    NSMutableArray *photosToDownloadForThisAlbum = [photosToDownload objectForKey:album.id];
    if(!photosToDownloadForThisAlbum) {
        photosToDownloadForThisAlbum = [NSMutableArray array];
        [photosToDownload setObject:photosToDownloadForThisAlbum forKey:album.id];
    }
    [photosToDownloadForThisAlbum addObject:photo];
}

-(void)cleanupTempObjectsAfterPhotoDownload:(NSDictionary*)photosToDownload {
    for(NSString *albumID in photosToDownload) {
        for(Photo *photo in photosToDownload[albumID]) {
            //a photo might be available for download without its
            //parent album being known, we don't want to save such
            //photos in the graph
            if(!photo.album) {
                [GraphManager deleteObject:photo];
            }
        }
    }
}

-(NSString*)cacheKeyForObject:(Class)class:(NSString*)objectID {
    NSString *name = NSStringFromClass(class);
    name = [name lowercaseString];
    NSString *prefix = [name substringWithRange:NSMakeRange(0, 2)];
    return [NSString stringWithFormat:@"%@_%@", prefix, objectID];
}
                   
-(void)addToCache:(NSString*)objectID:(NSObject*)object:(NSMutableDictionary*)cache {
    NSString *key = [self cacheKeyForObject:object.class :objectID];
    cache[key] = object;
}

-(id)getFromCache:(Class)class:(NSString*)objectID:(NSMutableDictionary*)cache {
    NSString *key = [self cacheKeyForObject:class:objectID];
    id o = [cache valueForKey:key];
    if(o == nil) {
        o = [class getById:objectID];
        if(o) {
            [cache setObject:o forKey:key];
        }
    }
    return o;
}
                   
-(BOOL)fixIfValidationError:(NSError*)error {
    if(error.code != 1560 && error.code != 1570) {
        return NO;
    }
    
    NSArray *validationErrors;
    if(error.code == 1570) {
        validationErrors = @[error];
    }
    else {
        validationErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
    }
    
    for(NSError *e in validationErrors) {
        NSManagedObject *culprit = e.userInfo[@"NSValidationErrorObject"];
        if(culprit) {
            [GraphManager deleteObject:culprit];
            DLog(@"deleted object to fix invalidation error (%@)", culprit);
        }
    }
    return YES;
}
@end
