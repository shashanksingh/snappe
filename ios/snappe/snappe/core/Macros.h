//
//  Macros.h
//  snappe
//
//  Created by Shashank on 16/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef snappe_Macros_h
#define snappe_Macros_h

#define SNAPPE_APP_ID @"601707881"
#define SNAPPE_URL_SCHEME @"snappe"
#define SNAPPE_ERROR_DOMAIN @"snappe"

typedef enum SnappeErrorCode {
    SNAPPE_ERROR_NETWORK_UNREACHABLE,
    SNAPPE_ERROR_ALASSET_UNEXPECTED,
    SNAPPE_ERROR_FB_NOT_AUTHORIZED,
    SNAPPE_ERROR_ALASSET_ACCESS_DENIED,
    SNAPPE_ERROR_UNEXPECTED_VALUE,
    SNAPPE_ERROR_PURCHASE_VERIFICATION_FAILED,
    SNAPPE_ERROR_INSUFFICIENT_CREDITS,
    SNAPPE_ERROR_ALBUM_ALREADY_EXISTS_WITH_NAME
}SnappeErrorCode;


#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define UIColorFromRGBA(rgbValue, a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#define debug_rect( arg ) NSLog( @"CGRect ( %f, %f, %f, %f)", arg.origin.x, arg.origin.y, arg.size.width, arg.size.height );

#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif

// ALog always displays output regardless of the DEBUG setting
#define ALog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define LOG_OPERATION_TIME_PROPS(name, code, props) \
[Logger track:[name stringByAppendingString:@"_init"] :props]; \
NSDate *startDate = [NSDate date]; \
code \
NSTimeInterval delta = [[NSDate date] timeIntervalSinceDate:startDate]; \
delta = round(delta * 1000); \
NSMutableDictionary *loggedProps = [props mutableCopy]; \
[loggedProps setObject:@(delta) forKey:@"time_taken"]; \
[Logger track:name :loggedProps];

#define LOG_OPERATION_TIME(name, code) \
LOG_OPERATION_TIME_PROPS(name, code, @{});

typedef void(^GenericCallback)(id data);
typedef void(^GenericErrorback)(NSError *error);

typedef void(^ArrayCallback)(NSArray *data);
typedef void(^VoidCallback)();

typedef void(^IndexedDataCallback)(NSUInteger index, id data);
typedef void(^StringCallback)(NSString *string);
typedef void(^VarargsCallback)(va_list);

typedef BOOL(^GenericFilterCallback)(id data);

typedef void(^ProgressCallback)(long long done, long long toDo);

#endif
