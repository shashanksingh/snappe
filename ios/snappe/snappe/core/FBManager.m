//
//  FBManager.m
//  snappe
//
//  Created by Shashank on 14/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FBManager.h"
#import "Macros.h"
#import "EventManager.h"
#import "AlbumAccessor.h"
#import "LocationAccessor.h"
#import "Util.h"
#import "Logger.h"
#import "FBSBJSON.h"

#define MAX_INVITES 50

@interface FBManager()
@property (strong) Facebook *facebook;
@property (strong) FBSessionStateHandler sessionStateChangeHandler;
@property (strong) FBSessionReauthorizeResultHandler sessionReauthorizationHandler;
@end

@implementation FBManager
@synthesize facebook;
@synthesize sessionStateChangeHandler;

-(id) init {
    self = [super init];
    if(self) {
        __weak typeof(self) bself = self;
        self.sessionStateChangeHandler = ^(FBSession *session, FBSessionState status, NSError *error) {
            [bself handleStateChange:session :status :error];
        };
        self.sessionReauthorizationHandler = ^(FBSession *session, NSError *error) {
            FBSessionState state = FBSession.activeSession.state;
            [bself handleStateChange:session :state :error];
        };
    }
    return self;
}

+(FBManager*) getInstance {
    static FBManager *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[FBManager alloc] init];
    });
    return sharedInstance;
}


-(void) authorize:(GenericCallback)callback {
    if([self isAuthorized]) {
        callback([NSNumber numberWithBool:YES]);
        return;
    }
    
    [[EventManager getInstance] addEventListener:FB_SESSION_STATE_CHANGED :YES :^(va_list args) {
        BOOL isAuthorized = [va_arg(args, NSNumber*) boolValue];
        callback([NSNumber numberWithBool:isAuthorized]);
    }];
    
    [self openSession:NO];
}

-(void)authorizeWithPublishPermissions:(GenericCallback)callback {
    if(FBSession.activeSession && [FBSession.activeSession.permissions containsObject:@"publish_stream"]) {
        callback(@(YES));
        return;
    }
    
    NSArray *permissions = @[@"publish_stream"];
    
    [[EventManager getInstance] addEventListener:FB_SESSION_STATE_CHANGED :YES :^(va_list args) {
        BOOL isAuthorized = va_arg(args, NSNumber*).boolValue;
        if(!isAuthorized) {
            callback(@(NO));
            return;
        }
        
        NSMutableSet *currentPermissions = [NSMutableSet setWithArray:FBSession.activeSession.permissions];
        NSMutableSet *requiredPermissions = [NSMutableSet setWithArray:permissions];
        [requiredPermissions intersectSet:currentPermissions];
        
        isAuthorized = requiredPermissions.count == permissions.count;
        
        callback(@(isAuthorized));
    }];
    
    if(FBSession.activeSession && FBSession.activeSession.isOpen) {
        [FBSession.activeSession requestNewPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends completionHandler:self.sessionReauthorizationHandler];
    }
    else {
        [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:YES completionHandler:self.sessionStateChangeHandler];
    }
}

-(void)openSession:(BOOL)background {
    NSArray *permissions = @[@"user_location",
                            @"user_photos",
                            @"user_checkins",
                            @"friends_location",
                            @"friends_photos",
                            @"friends_checkins",
                            @"email"];
    
    if(FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        [FBSession.activeSession openWithCompletionHandler:self.sessionStateChangeHandler];
    }
    else if(!background) {
        [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:self.sessionStateChangeHandler];
    }
    else {
        [[EventManager getInstance] fireEvent:FB_NOT_AUTHORIZED];
    }
}


-(BOOL) isAuthorized {
    //opening fbsession can take time, we don't want to
    //wait for it in case we have already authorized the
    //user in the past at least once
    FBSessionState s = FBSession.activeSession.state;
    return FB_ISSESSIONOPENWITHSTATE(s);
}

-(NSString*) getAccessToken {
    if(!FBSession.activeSession) {
        return nil;
    }
    return FBSession.activeSession.accessToken;
}

-(BOOL) handleOpenURL:(NSURL *)url {
    return [FBSession.activeSession handleOpenURL:url];
}

-(void)handleDidBecomeActive {
    [FBSession.activeSession handleDidBecomeActive];
}

-(void)handleStateChange:(FBSession*)session:(FBSessionState)state:(NSError*)error {
    if(error) {
        [FBSession.activeSession closeAndClearTokenInformation];
        DLog(@"facebook authorization failed with error: %@", error);
        [Logger logError:@"facebook_authorization_failed":error];
        [[EventManager getInstance] fireEvent:FB_AUTH_ERROR, error];
    }
    
    else if(FB_ISSESSIONOPENWITHSTATE(state)) {
        [Util setUserDefault:FBSession.activeSession.accessToken forKey:@"FBAccessTokenKey"];
        [Util setUserDefault:FBSession.activeSession.expirationDate forKey:@"FBExpirationDateKey"];
        
        [self updateUserInfo];
        [self getFriendIDsToInvite:^(NSArray *data) {
            DLog(@"data: %@", data);
        } :^(NSError *error) {
            DLog(@"error: %@", error);
        }];
    }
    else if(state == FBSessionStateClosed) {
        [[EventManager getInstance] fireEvent:FB_LOGOUT, nil];
    }
    else if(state == FBSessionStateClosedLoginFailed) {
        [FBSession.activeSession closeAndClearTokenInformation];
        [[EventManager getInstance] fireEvent:FB_AUTH_DECLINED];
    }
    else{
        DLog(@"unexpected FBSessionState in handleStateChange: %d", state);
    }
    
    if(!self.facebook) {
        self.facebook = [[Facebook alloc]
                         initWithAppId:FBSession.activeSession.appID
                         andDelegate:nil];
        self.facebook.accessToken = FBSession.activeSession.accessToken;
        self.facebook.expirationDate = FBSession.activeSession.expirationDate;
        [self.facebook enableFrictionlessRequests];
    }
    
    [[EventManager getInstance] fireEvent:FB_SESSION_STATE_CHANGED, [NSNumber numberWithBool:FB_ISSESSIONOPENWITHSTATE(state)]];
}


-(NSString*) getUserId {
    id userInfo = [self getUserInfo];
    if(userInfo == nil) return nil;
    return [userInfo objectForKey:@"id"];
}

-(BOOL) hasUserAuthorizedOnce {
    return [Util getUserDefault:@"FBAccessTokenKey"] != nil;
}

-(void)updateUserInfo {
    [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
        BOOL authoizedBefore = [self getUserInfo] != nil;
        
        if(error) {
            if([[error userInfo][FBErrorParsedJSONResponseKey][@"body"][@"error"][@"code"] compare:@190] == NSOrderedSame)
            {
                [[EventManager getInstance] fireEvent:FB_NOT_AUTHORIZED];
            }
            [Logger logError:@"error in getting logged in user's info" :error];
            return;
        }
        
        [Util setUserDefault:result forKey:@"FBUserInfo"];
        [[EventManager getInstance] fireEvent:FB_AUTHORIZED, @(!authoizedBefore), nil];
        
    }];
}

-(FBGraphObject*)getUserInfo {
    return [Util getUserDefault:@"FBUserInfo"];
}

-(void)createAlbum:(NSObject*)source:(GenericCallback)callback:(GenericErrorback)errorback {
    
    [self authorizeWithPublishPermissions:^(NSNumber *data) {
        BOOL isAuthorized = [data boolValue];
        if(!isAuthorized) {
            NSError *error = [NSError errorWithDomain:SNAPPE_ERROR_DOMAIN code:SNAPPE_ERROR_FB_NOT_AUTHORIZED userInfo:nil];
            errorback(error);
            return;
        }
        
        NSMutableDictionary *albumAttributes = [NSMutableDictionary dictionary];
        BOOL sourceIsAlbum = [source isKindOfClass:[Album class]];
        if(sourceIsAlbum) {
            Album *album = (Album*)source;
            [albumAttributes setObject:album.name forKey:@"name"];
            
            if(album.summary && album.summary.length > 0) {
                [albumAttributes setObject:album.summary forKey:@"message"];
            }
            if(album.location) {
                [albumAttributes setObject:album.location.id forKey:@"place"];
            }
        }
        else {
            [albumAttributes setObject:(NSString*)source forKey:@"name"];
        }
        
        
        [[FBRequest requestForPostWithGraphPath:@"/me/albums" graphObject:[FBGraphObject graphObjectWrappingDictionary:albumAttributes]] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
         {
             if(error || !result) {
                 [Logger logError:@"error in creating a new album" :error];
                 errorback(error);
                 return;
             }
             
             callback(result);
         }];
    }];
}

-(void)uploadPhotoToAlbum:(Photo*)photo:(NSString*)albumObjectID:(NSInteger)allowedRetries:(GenericCallback)callback:(GenericErrorback)errorback
{
    [photo getUIImage:FULL_RESOLUTION :^(UIImage *image) {
        FBRequest *req = [FBRequest requestForUploadPhoto:image];
        req.graphPath = [NSString stringWithFormat:@"%@/photos", albumObjectID];
        //[req.parameters setObject:albumObjectID forKey:@"album"];
        
        Location *photoLocation = [photo getSelfOrAlbumLocation];
        if(photoLocation) {
            [req.parameters setObject:photoLocation.id forKey:@"place"];
        }
        
        FBRequestConnection *connection = [[FBRequestConnection alloc] init];
        [connection addRequest:req completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
        {
            if(error) {
                NSString *message = [NSString stringWithFormat:@"error in uploading photo with id: %@ to album with facebook object id: %@", photo.id, albumObjectID];
                [Logger logError:message :error];
                errorback(error);
                return;
            }
            if(!result && allowedRetries > 0) {
                NSMutableDictionary *args = [NSMutableDictionary dictionary];
                [args setObject:albumObjectID forKey:@"album_object_id"];
                [args setObject:photo.id forKey:@"photo_id"];
                [args setObject:@(allowedRetries) forKey:@"retries_left"];
                [Logger track:@"fb_album_photo_upload_retry"];
                
                [self uploadPhotoToAlbum:photo :albumObjectID :allowedRetries - 1 :callback :errorback];
                return;
            }
            
            callback(result);
        }];
        [connection start];
        
    } :^(NSError *error) {
        NSString *message = [NSString stringWithFormat:@"error in getting full resolution image for uploading photo with id: %@ and hash: %@", photo.id, [photo getHash]];
        [Logger logError:message :error];
        errorback(error);
    }];
}

-(void)sendUserToUserRequest:(NSArray*)recipientIDs:(NSString*)message:(NSDictionary*)data:(GenericCallback)callback
{
    FBSBJSON *jsonWriter = [FBSBJSON new];
    
    NSString *dataParam = [jsonWriter stringWithObject:data];
    if(dataParam.length > 255) {
        [Logger track:@"fb_u2u_req_data_too_long" :@{@"data": dataParam}];
        
        NSMutableDictionary *shorterData =  [data mutableCopy];
        [shorterData removeObjectForKey:@"from_name"];
        [shorterData removeObjectForKey:@"album_name"];
        
        dataParam = [jsonWriter stringWithObject:shorterData];
    }
    
    NSDictionary *params = @{@"to": [recipientIDs componentsJoinedByString:@","],
                             @"message": message,
                             @"data": dataParam
                        };
    
    [[EventManager getInstance] addEventListener:FB_REQUEST_DIALOG_DISMISSED :YES :^(va_list args) {
        NSString *requestID = va_arg(args, NSString*);
        callback(requestID);
    }];
    
    [self.facebook dialog:@"apprequests"
                andParams:[params mutableCopy]
              andDelegate:self];
}

-(void)dialogCompleteWithUrl:(NSURL *)url {
    NSDictionary *params = [self parseURLParams:[url query]];
    NSString *requestID = [params valueForKey:@"request"];
    [[EventManager getInstance] fireEvent:FB_REQUEST_DIALOG_DISMISSED, requestID];
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [[kv objectAtIndex:1]
         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}

-(void)getFriendIDsToInvite:(ArrayCallback)callback:(GenericErrorback)errorback {
    NSString *query = @"SELECT uid, friend_count, mutual_friend_count, likes_count, devices FROM user WHERE NOT is_app_user AND uid IN (SELECT uid2 FROM friend WHERE uid1 = me())";
    
    [FBRequestConnection startWithGraphPath:@"/fql" parameters:@{@"q" : query} HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, NSDictionary *result, NSError *error)
    {
        if(error) {
            [Logger track:@"fb_get_invitees_error" :@{@"error" : error.localizedDescription}];
            errorback(error);
            return;
        }
        
        DLog(@"results: %@", result);
        
        NSArray *friendInfoList = result[@"data"];
        if(!friendInfoList) {
            [Logger track:@"fb_get_invitees_error" :@{@"error" : @"missing data field"}];
            errorback(error);
            return;
        }
        
        NSMutableDictionary *friendScore = [NSMutableDictionary dictionary];
        for(NSDictionary *friendInfo in friendInfoList) {
            NSString *friendID = friendInfo[@"uid"];
            
            id likesCount = friendInfo[@"likes_count"];
            if(!likesCount || likesCount == [NSNull null]) {
                likesCount = @(0);
            }
            
            id friendCount = friendInfo[@"friend_count"];
            if(!friendCount || friendCount == [NSNull null]) {
                friendCount = @(0);
            }
            
            id mutualFriendCount = friendInfo[@"mutual_friend_count"];
            if(!mutualFriendCount || mutualFriendCount == [NSNull null]) {
                mutualFriendCount = @(0);
            }
            
            NSArray *devices = friendInfo[@"devices"];
            if(!devices || friendInfo[@"devices"] == [NSNull null]) {
                devices = @[];
            }
            
            int iosDeviceCount = 0;
            for(NSDictionary *device in devices) {
                NSString *os = device[@"os"];
                if(os && [[os lowercaseString] isEqualToString:@"ios"]) {
                    iosDeviceCount++;
                }
            }
            
            friendScore[friendID] = @{@"likes": likesCount, @"friends":friendCount, @"mutual_friends": mutualFriendCount, @"ios_devices" : @(iosDeviceCount)};
            
        }
        
        
        NSArray *rv = [friendScore keysSortedByValueUsingComparator:^NSComparisonResult(NSDictionary *score1, NSDictionary *score2)
        {
            int iosDeviceCount1 = ((NSNumber*)score1[@"ios_devices"]).intValue;
            int iosDeviceCount2 = ((NSNumber*)score2[@"ios_devices"]).intValue;
            
            if(iosDeviceCount1 != iosDeviceCount2) {
                return iosDeviceCount1 > iosDeviceCount2 ? NSOrderedAscending : NSOrderedDescending;
            }
            
            int friends1 = ((NSNumber*)score1[@"friends"]).intValue;
            int friends2 = ((NSNumber*)score2[@"friends"]).intValue;
            
            if(friends1 != friends2) {
                return friends1 > friends2 ? NSOrderedAscending : NSOrderedDescending;
            }
            
            int mutualFriends1 = ((NSNumber*)score1[@"mutual_friends"]).intValue;
            int mutualFriends2 = ((NSNumber*)score2[@"mutual_friends"]).intValue;
            
            if(mutualFriends1 != mutualFriends2) {
                return mutualFriends1 > mutualFriends2 ? NSOrderedAscending : NSOrderedDescending;
            }
            
            int likes1 = ((NSNumber*)score1[@"likes"]).intValue;
            int likes2 = ((NSNumber*)score2[@"likes"]).intValue;
            
            return likes1 > likes2 ? NSOrderedAscending : NSOrderedDescending;
            
        }];
        
        int maxInvites = [Util rand] < 0.5 ? 15 : MAX_INVITES;
        if(rv.count > maxInvites) {
            rv = [rv subarrayWithRange:NSMakeRange(0, maxInvites)];
        }
        
        DLog(@"rv: %@", rv);
        
        callback(rv);
        
    }];
}

-(void)inviteFriends:(NSArray*)friendIDs:(GenericCallback)callback {
    [self sendUserToUserRequest:friendIDs :@"Privately share photos with me on Snappe!" :@{@"type" : @"friend_invite", @"from_id" : [self getUserId]} :^(NSString *requestID)
    {
        if(requestID) {
            callback(@(YES));
        }
        else {
            callback(@(NO));
        }
    }];
}

@end
