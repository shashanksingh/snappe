//
//  AlbumPullOperation.h
//  snappe
//
//  Created by Shashank on 10/03/13.
//
//

#import "PhotoTransferOperation.h"
#import "AlbumAccessor.h"

@interface AlbumPullOperation : PhotoTransferOperation
-(id)init:(Album*)source:(Album*)destination;
@end
