//
//  PhotoLibraryOrganizer.h
//  snappe
//
//  Created by Shashank on 09/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "Album.h"
#import "AlbumAccessor.h"
#import "Macros.h"

@interface PhotoLibraryOrganizer : NSObject

+(PhotoLibraryOrganizer*) getInstance;
-(BOOL)isSyncing;
-(BOOL) isSuccessfullySyncedOnce;
-(void) sync:(VoidCallback)callback :(GenericErrorback)errorback;
-(void) getLocalAlbums :(ArrayCallback)callback :(GenericErrorback)errorback;
-(void) getRemoteAlbums :(ArrayCallback)callback :(GenericErrorback)errorback;
@end


