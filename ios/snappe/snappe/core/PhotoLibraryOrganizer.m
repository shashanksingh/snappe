//
//  PhotoLibraryOrganizer.m
//  snappe
//
//  Created by Shashank on 09/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoLibraryOrganizer.h"

#import "PhotoLibraryReader.h"
#import "PhotoAccessor.h"
#import "Util.h"
#import "SnappeHTTPService.h"
#import "AppDelegate.h"
#import "UserAccessor.h"
#import "GraphManager.h"
#import "FakeAlbum.h"
#import "EventManager.h"
#import "SyncManager.h"
#import "Logger.h"

#define MAX_SYNC_ATTEMPT_COUNT 3

static const NSString *LIB_ORGANIZATION_SYNCHRONIZATION_KEY = @"snappe_lib_organize_sync_key";


@interface PhotoLibraryOrganizer ()
@property BOOL organizing;
@property (strong) NSArray *albums;
@end

@implementation PhotoLibraryOrganizer
@synthesize organizing;
@synthesize albums;

-(id) init {
    self = [super init];
    if(self) {
        self.organizing = NO;
        [self loadAlbums];
        
        [self setupEvents];
    }
    return self;
}

+(PhotoLibraryOrganizer*) getInstance {
    static PhotoLibraryOrganizer *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PhotoLibraryOrganizer alloc] init];
    });
    return sharedInstance;
}

-(void) setupEvents {
    [[EventManager getInstance] addEventListener:LOCAL_LIBRARY_MODIFIED :^(va_list args) {
        //TODO:refresh
    }];
}

-(void)loadAlbums {
    self.albums = [[GraphManager getInstance] getAlbums];
}

-(void) onOrganizationSuccess {
    @synchronized(LIB_ORGANIZATION_SYNCHRONIZATION_KEY) {
        self.organizing = NO;
    }
    
    [self updateLastSuccessfulOrganizationDate];
    [self loadAlbums];
    
    [[EventManager getInstance] fireEvent:PHOTO_LIBRARY_ORGANIZED, @(YES), nil];
}

-(void) onOrganizationSkipped {
    @synchronized(LIB_ORGANIZATION_SYNCHRONIZATION_KEY) {
        self.organizing = NO;
    }
    
    [self loadAlbums];
    [[EventManager getInstance] fireEvent:PHOTO_LIBRARY_ORGANIZED, @(YES), nil];
}

-(void) onOrganizationFailure:(NSError*)error {
    @synchronized(LIB_ORGANIZATION_SYNCHRONIZATION_KEY) {
        self.organizing = NO;
    }
    
    [self loadAlbums];
    [[EventManager getInstance] fireEvent:PHOTO_LIBRARY_ORGANIZED, @(NO), error, nil];
}

-(NSArray*) getFakeAlbums {
    return [NSArray arrayWithObjects:[MaleAlbum new], [HawaiiAlbum new], [FemaleAlbum new], nil];
}

-(BOOL) isSuccessfullySyncedOnce {
    return [self getLastSuccessfulOrganizationDate] != nil;
}

-(NSDate *) getLastSuccessfulOrganizationDate {
    return [Util getUserDefault:@"lastSuccessfulOrganization"];
}

-(void) updateLastSuccessfulOrganizationDate {
    [Util setUserDefault:[NSDate date] forKey:@"lastSuccessfulOrganization"];
}

-(NSArray*) sortAlbums:(NSArray*)albumArray {
    return [albumArray sortedArrayUsingComparator:^NSComparisonResult(Album* a1, Album *a2) {
        BOOL hasNewPhotos1 = a1.newPhotosCount && a1.newPhotosCount.intValue > 0;
        BOOL hasNewPhotos2 = a2.newPhotosCount && a2.newPhotosCount.intValue > 0;
        
        if(hasNewPhotos1 && !hasNewPhotos2) {
            return NSOrderedAscending;
        }
        if(!hasNewPhotos1 && hasNewPhotos2) {
            return NSOrderedDescending;
        }
        return -[a1.modifiedOn compare:a2.modifiedOn];
    }];
}

-(BOOL)isSyncing {
    return self.organizing;
}

-(void) sync:(VoidCallback)callback :(GenericErrorback)errorback {
    
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    [[EventManager getInstance] addEventListener:PHOTO_LIBRARY_ORGANIZED :YES :^(va_list args)
     {
         NSNumber *successNumber = va_arg(args, NSNumber*);
         if(successNumber.boolValue) {
             dispatch_async(callerQ, ^{
                 callback();
             });
         }
         else {
             NSError *error = va_arg(args, NSError*);
             if(errorback) {
                 dispatch_async(callerQ, ^{
                     errorback(error);
                 });
             }
             
         }
     }];
    
    @synchronized(LIB_ORGANIZATION_SYNCHRONIZATION_KEY) {
        if(self.organizing) {
            return;
        }
        self.organizing = YES;
    }
    
    [self organize:0];
}

-(void) organize:(NSInteger)attemptCount {
    
    [[EventManager getInstance] fireEvent:PHOTO_LIBRARY_ORGANIZING];
    
    [[PhotoLibraryReader getInstance] getLocalLibrayModifications:^(NSDictionary *localModDescriptor) {
        NSArray *localAddDescriptor = [localModDescriptor objectForKey:@"add"];
        NSDictionary *localDeleteDescriptor = [localModDescriptor objectForKey:@"delete"];
        NSArray *localModifyDescriptor = [localModDescriptor objectForKey:@"modify"];
        
        
        if(attemptCount > MAX_SYNC_ATTEMPT_COUNT) {
            NSString *message = [NSString stringWithFormat:@"infinite loop in syncing with server, current changeset: %@", localModDescriptor];
            [Logger logError:message :nil];
            [self onOrganizationSuccess];
            
            [Logger track:@"sync_infinite_loop"];
            return;
        }
        
        //no modifications
        if(attemptCount > 0 && [localAddDescriptor count] == 0 && [Util isEmptyDictionary:localDeleteDescriptor] && [localModifyDescriptor count] == 0)
        {
            [self onOrganizationSuccess];
            return;
        }
        
        if(![[FBManager getInstance] isAuthorized]) {
            [self onOrganizationSkipped];
            //delete locally deleted from graph only after after their id has been extracted
            [GraphManager removeNonExistentLocalItemsFromGraph:localDeleteDescriptor];
            return;
        }
        
        //send to server and get albums for photos
        [[SnappeHTTPService getInstance] applyLocalModifications:localModDescriptor:^(id json){
            NSLog(@"modifications applied to remote: %@", localModDescriptor);
            
            //delete anything that the server just said should be found locally but was missing
            [self organize:(attemptCount + 1)];
            return;
                
        } :^(NSError *error) {
            [Logger logError:@"error in saving new photos" :error];
            [self onOrganizationFailure:error];
        }];
        //delete locally deleted from graph only after after their id has been extracted
        [GraphManager removeNonExistentLocalItemsFromGraph:localDeleteDescriptor];
        
        
    } :^(NSError * error){
        [Logger logError:@"error in getting local library modifications" :error];
        [self onOrganizationFailure:error];
    }];
}

-(void) getLocalAlbums :(ArrayCallback)callback :(GenericErrorback)errorback {
    [self getAlbums:YES :callback :errorback];
}

-(void) getRemoteAlbums :(ArrayCallback)callback :(GenericErrorback)errorback {
    [self getAlbums:NO :callback :errorback];
}


-(void) getAlbums:(BOOL)local:(ArrayCallback)callback:(GenericErrorback)errorback {
    //non-empty albums list => organized at least once in the lifetime of the app
    //since installation
    //the only situation in which this implication does not hold is when the user
    //connected succesfully but has no albums (own or friends) in which case
    //everything is going to be empty anyway
    if([self.albums count] > 0) {
        
        User *currentUser = [User getCurrentUser];
        
        NSArray *filtered = [self.albums filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(Album * album, NSDictionary *bindings)
        {
            if(local) {
                return [album isCurrentDeviceAlbum];
            }
            else {
                if(![album isFacebookAlbum]) {
                    //we don't have feature to share between
                    //one user's multiple devices, yet!
                    return ![album isOwnedByCurrentUser] && [album isSharedWithUser:currentUser];
                }
                return YES;
            }
            return (!local) ^ [album isCurrentDeviceAlbum];
        }]];
        
        NSArray *sorted = [self sortAlbums:filtered];
        callback(sorted);
        return;
    }
    if(local) {
        [self getLocallyOrganizedAlbums:callback :errorback];
    }
    else {
        if([[FBManager getInstance] hasUserAuthorizedOnce]) {
            //once the user has authorized never show
            //her fake albums
            callback(@[]);
        }
        else {
            NSArray *fakeAlbums = [self getFakeAlbums];
            callback(fakeAlbums);
        }
        
        
    }
    
}


-(void) getLocallyOrganizedAlbums :(GenericCallback)callback :(GenericErrorback)errorback {
    [[PhotoLibraryReader getInstance] getLocalLibrayModifications:^(NSDictionary *modificationDescriptor) {
        NSArray *newAlbums = [modificationDescriptor objectForKey:@"add"];
        
        NSArray *localOrganizedAlbums = [self organizeLocalAlbumsAndPhotos:newAlbums];
        NSArray *sorted = [self sortAlbums:localOrganizedAlbums];
        callback(sorted);
    } :errorback];
}

//@NOTE: This method organizes only 'recently modified' albums and photos
//the returned set of albums and photos therein is neither persisted
//not includes any previously persisted albums. This is good for now
//because we call this only before we have sent local album info to
//the server
-(NSArray*) organizeLocalAlbumsAndPhotos:(NSArray*)modifiedAlbums {
    NSMutableArray *localAlbums = [NSMutableArray array];
    NSMutableDictionary *dayToAlbum = [NSMutableDictionary dictionary];
    
    NSMutableSet *namedAlbumPhotoHashes = [NSMutableSet set];
    for(Album *modifiedAlbum in modifiedAlbums) {
        if(![modifiedAlbum isSavedPhotosGroup]) {
            [localAlbums addObject:modifiedAlbum];
            for(Photo *photo in modifiedAlbum.photos) {
                [namedAlbumPhotoHashes addObject:[photo getHash]];
            }
        }
    }
    
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd YYYY"];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    
    NSDate *epoch = [NSDate dateWithTimeIntervalSince1970:0];
    
    NSString *deviceID = [[CoreController getInstance] getDeviceID];
    
    //we re-organize photos in the default album
    for(Album *modifiedAlbum in modifiedAlbums) {
        if(![modifiedAlbum isSavedPhotosGroup]) continue;
        
        NSOrderedSet *modifiedPhotos = [[NSOrderedSet alloc] initWithOrderedSet:[modifiedAlbum photos]];
        
        for(Photo *photo in modifiedPhotos) {
            if([namedAlbumPhotoHashes containsObject:[photo getHash]]) continue;
            
            NSDate *clickedOn = [photo getClickedOn];
            NSDateComponents *breakdownInfo = [sysCalendar components:NSDayCalendarUnit fromDate:epoch toDate:clickedOn options:0];
            NSInteger days = [breakdownInfo day];
            NSNumber *daysKey = [NSNumber numberWithInt:days];
            
            //@TODO: check if there is an existing album for this date
            Album *album = [dayToAlbum objectForKey:daysKey];
            if(album == nil) {
                album = [Album getTransientInstance];
                [album setName:[dateFormatter stringFromDate:clickedOn]];
                [dayToAlbum setObject:album forKey:daysKey];
                [localAlbums addObject:album];
                [album setModifiedOn:clickedOn];
                [album setId:[Util getUUID]];
                [album setDeviceID:deviceID];
            }
            [photo setAlbum:album];
        }
    }
    
    
    return localAlbums;
}

@end
