//
//  PhotoTransferOperation.m
//  snappe
//
//  Created by Shashank on 02/04/13.
//
//

#import "PhotoTransferOperation.h"
#import "EventManager.h"
#import "PhotoAccessor.h"

@interface PhotoTransferOperation()
@property (strong) NSMutableArray *progressListenerIDs;
@property (strong) NSMutableDictionary *eachPhotoCompletion;
@end

@implementation PhotoTransferOperation

-(id)init {
    self = [super init];
    if(self) {
        self.progressListenerIDs = [NSMutableArray array];
        self.eachPhotoCompletion = [NSMutableDictionary dictionary];
    }
    return self;
}

-(NSArray*)getPhotos {
    return @[];
}

-(NSArray*)getEventTypes {
    return @[];
}

-(void)setUp {
    NSMutableSet *photosIDSet = [NSMutableSet set];
    NSMutableSet *photoURLSet = [NSMutableSet set];
    for(Photo *photo in [self getPhotos]) {
        [photosIDSet addObject:photo.id];
        if(photo.src) {
            [photoURLSet addObject:photo.src];
        }
    }
    
    for(NSNumber *eventTypeNumber in [self getEventTypes]) {
        EventType event = eventTypeNumber.intValue;
        NSString *listenerID = [[EventManager getInstance] addEventListener:event :^(va_list args) {
            NSString *photoId = va_arg(args, NSString*);
            
            NSString *url = nil;
            if(event == PHOTO_DOWNLOAD) {
                url = va_arg(args, NSString*);
            }
            
            ExtOpStatus status = [va_arg(args, NSNumber*) intValue];
            
            if([photosIDSet containsObject:photoId] || (url && [photoURLSet containsObject:url])) {
                if(status == ONGOING) {
                    double currentDoneFraction = [va_arg(args, NSNumber*) doubleValue];
                    [self.eachPhotoCompletion setObject:@(currentDoneFraction) forKey:photoId];
                    [self updateCompletion];
                }
                else if(status == SUCCESSFUL || status == FAILURE) {
                    [self.eachPhotoCompletion setObject:@(1.0) forKey:photoId];
                    [self updateCompletion];
                }
            }
        }];
        [self.progressListenerIDs addObject:listenerID];
        
    }
    

}

-(void)updateCompletion {
    double done = 0;
    for(NSString *photoID in self.eachPhotoCompletion) {
        NSNumber *thisDoneFraction = self.eachPhotoCompletion[photoID];
        done += (100.0/[self getPhotos].count) * thisDoneFraction.doubleValue;
    }
    self.completion = done;
    [self fireUpdate];
}

-(void)onFinish:(BOOL)success {
    [super onFinish:success];
    for(NSString *listenerID in self.progressListenerIDs) {
        [[EventManager getInstance] removeEventListener:listenerID];
    }
}

@end
