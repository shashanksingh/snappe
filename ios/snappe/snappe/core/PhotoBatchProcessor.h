//
//  PhotoUploader.h
//  snappe
//
//  Created by Shashank on 28/03/13.
//
//

#import <Foundation/Foundation.h>
#import "Macros.h"
#import "PhotoAccessor.h"

@interface PhotoBatchProcessor : NSObject
-(void)process:(NSArray*)photos:(id)context:(GenericCallback)callback:(GenericErrorback)errorback;
-(void)onPhotoDone:(Photo*)photo:(NSError*)error;
@end
