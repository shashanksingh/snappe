//
//  Logger.m
//  snappe
//
//  Created by Shashank on 14/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Logger.h"
#import "Mixpanel.h"
#import "Flurry.h"
#import "FBManager.h"
#import "CoreController.h"
#import "TestFlight.h"
#import "UserAccessor.h"
#import "EventManager.h"
#import "Util.h"

#define MIXPANEL_API_KEY @"8cb2e9c9cb5b552e8771c0ff659929e4"
#define FLURRY_API_KEY @"TQ7TZRSCVBW5C675WPKK"

@interface Logger()
@property (strong) NSDictionary *samplingRatios;
@property (strong) Mixpanel *mp;
@property (strong) NSString *deviceID;
@property (strong) NSDate *appInitTime;
@end

@implementation Logger
@synthesize mp;
-(id)init {
    self = [super init];
    if(self) {
        self.appInitTime = [NSDate date];
        self.mp = [Mixpanel sharedInstanceWithToken:MIXPANEL_API_KEY];
        self.deviceID = [[CoreController getInstance] getDeviceID];
        
        self.samplingRatios = @{@"search" : @(0.1)};
        
        if([[FBManager getInstance] hasUserAuthorizedOnce]) {
            [self mixpanelPeopleIdentify];
        }
        else {
            __block typeof(self) bself = self;
            [[EventManager getInstance] addEventListener:FB_AUTHORIZED :YES :^(va_list args) {
                if(!bself) {
                    return;
                }
                [bself mixpanelPeopleIdentify];
            }];
        }
    }
    return self;
}

-(void)mixpanelPeopleIdentify {
    FBGraphObject *currentUser = [[FBManager getInstance] getUserInfo];
    if(!currentUser) {
        //[self.class track:@"mixpanel_people_identify_missing_user"];
        return;
    }
    
    NSString *email = [currentUser objectForKey:@"email"];
    if(!email) {
        email = @"";
    }
    
    [mp identify:[currentUser objectForKey:@"id"]];
    [mp.people identify:[currentUser objectForKey:@"id"]];
    [mp.people set:@{
        @"id" : [currentUser objectForKey:@"id"],
        @"name": [currentUser objectForKey:@"name"],
        @"$first_name": [currentUser objectForKey:@"first_name"],
        @"$last_name": [currentUser objectForKey:@"last_name"],
        @"$last_name": [currentUser objectForKey:@"last_name"],
        @"$email": email
    }];
    
    NSString *apnsToken = [[CoreController getInstance] getAPNSToken];
    if(apnsToken) {
        [mp.people addPushDeviceToken:[apnsToken dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

+(Logger*) getInstance {
    static Logger *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Logger alloc] init];
    });
    return sharedInstance;
}

+(void)start {
    [self getInstance];
    [Flurry startSession:FLURRY_API_KEY];
}

+(void)logError:(NSString *)message :(NSError *)error {
    DLog(@"%@: %@", message, error);
    TFLog(@"%@: %@", message, error);
    [Flurry logError:@"" message:message error:error];
}

+(void)track:(NSString *)event {
    [self track:event:@{}];
}

+(void)track:(NSString *)event :(NSDictionary *)properties_ {
    Logger *this = [self getInstance];
    
    double samplingRatio = 1.0;
    NSNumber *samplingRatioNumber = [this.samplingRatios objectForKey:event];
    if(samplingRatioNumber) {
        samplingRatio = samplingRatioNumber.doubleValue;
    }
    
    if([Util rand] > samplingRatio) {
        return;
    }
    
    NSMutableDictionary *properties = [properties_ mutableCopy];
    properties[@"device_id"] = [self getInstance].deviceID;
    if([self getInstance].mp.people.distinctId) {
        properties[@"uid"] = [self getInstance].mp.people.distinctId;
    }
    
    NSTimeInterval delta = [[NSDate date] timeIntervalSinceDate:[self getInstance].appInitTime];
    delta = round(delta * 1000);
    properties[@"time_since_init"] = @(delta);
    
    [Flurry logEvent:event withParameters:properties];
    [[self getInstance].mp track:event properties:properties];
}
@end
