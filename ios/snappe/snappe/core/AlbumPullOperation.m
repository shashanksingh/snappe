//
//  AlbumPullOperation.m
//  snappe
//
//  Created by Shashank on 10/03/13.
//
//

#import "AlbumPullOperation.h"
#import "EventManager.h"
#import "Logger.h"
#import "PhotoLibraryOrganizer.h"
#import "Util.h"
#import "Macros.h"

#define SOURCE_ALBUM_ID_KEY @"src_album_id"
#define DESTINATION_ALBUM_ID_KEY @"dest_album_id"
#define SOURCE_ALBUM_NAME_KEY @"src_album_name"
#define DESTINATION_ALBUM_NAME_KEY @"dest_album_name"

@interface AlbumPullOperation()
@property (retain) Album *source;
@property (retain) Album *destination;
@property (strong) NSString *sourceName;
@property (strong) NSString *destinationName;
@end

@implementation AlbumPullOperation
-(id)init:(Album*)source:(Album*)destination {
    self = [super init];
    if(self) {
        self.source = source;
        self.destination = destination;
        self.sourceName = source.name;
        self.destinationName = destination.name;
        
        [self setUp];
    }
    return self;
}

-(id)initWithPersistedState:(NSDictionary*)persistedState {
    assert(false);//no implemented, yet
}

-(NSArray*)getPhotos {
    return [self.source.photos array];
}

-(NSArray*)getEventTypes {
    return @[@(PHOTO_DOWNLOAD)];
}

-(NSDictionary*)getState {
    NSMutableDictionary *state = [[super getState] mutableCopy];
    [state setObject:NSStringFromClass(self.class) forKey:@"class"];
    [state setObject:self.source.id forKey:SOURCE_ALBUM_ID_KEY];
    [state setObject:self.destination.id forKey:DESTINATION_ALBUM_ID_KEY];
    [state setObject:self.sourceName forKey:SOURCE_ALBUM_NAME_KEY];
    [state setObject:self.destinationName forKey:DESTINATION_ALBUM_NAME_KEY];
    
    return state;
}

-(void)start:(GenericCallback)callback {
    self.status = STARTED;
    [self fireUpdate];
    
    VoidCallback onSuccess = ^() {
        
        [Logger track:@"album_pull" :@{@"source": self.source.id, @"destination": self.destination.id}];
        
        self.status = SUCCESSFUL;
        self.completion = 100;
        self.finishedOn = [NSDate date];
        
        
        [[PhotoLibraryOrganizer getInstance] sync:^{
            DLog(@"successfully synced with server after album share operation");
            
            callback(@(self.status));
            
            [self onFinish:YES];
            
        } :^(NSError *error) {
            [Logger logError:@"error in syncing after AlbumShareOperation" :error];
            
            callback(@(FAILURE));
                     
            [self onFinish:NO];
            
        }];
    };
    
    GenericErrorback onError = ^(NSError *error_) {
        
        if(!([error_.domain isEqualToString:SNAPPE_ERROR_DOMAIN] && error_.code == SNAPPE_ERROR_INSUFFICIENT_CREDITS))
        {
            [Logger logError:[NSString stringWithFormat:@"error in pulling album (source: %@, destination: %@)", self.source.id, self.destination.id] :error_];
            
            //TODO: may be dial down the last sync time to allow repeat of the process without user
            //intervention
            NSString *userMessage = [NSString stringWithFormat:@"There was an error in pulling album. Please try again"];
            
            [Util alertError:error_:userMessage];
            
        }
        
        self.status = FAILURE;
        self.error = [error_ localizedDescription];
        self.finishedOn = [NSDate date];
        
        callback(@(self.status));
        
        [self onFinish:NO];
    };
    
    
    [self.source pullInto:self.destination :onSuccess :onError];
    
    
}

-(NSString*)getName {
    return [NSString stringWithFormat:@"Album Pull"];
}

-(NSString*)getType {
    return @"EXTOP_ALBUM_PULL";
}

-(NSString*)getStatusText {
    NSString *statusText = nil;
    if(self.status == NOT_STARTED) {
        statusText = @"waiting...";
    }
    else if(self.status == STARTED) {
        statusText = [NSString stringWithFormat:@"pulling %d%% completed", MIN(self.completion, 99)];
    }
    else if(self.status == SUCCESSFUL) {
        long secondsSinceDone = [self.finishedOn timeIntervalSince1970];
        statusText = [NSString stringWithFormat:@"successfully pulled %@", [Util getRelativeTime:secondsSinceDone]];
    }
    else if(self.status == FAILURE) {
        statusText = @"failed, please try again";
    }
    else {
        DLog(@"warning: unknown value for ext op status: %d", self.status);
    }
    return statusText;
}

-(Notification*)getNotification {
    Notification *notification = [super getNotification];
    
    NSMutableDictionary *notificationData = notification.data;
    [notificationData setObject:self.source.id forKey:SOURCE_ALBUM_ID_KEY];
    [notificationData setObject:self.destination.id forKey:DESTINATION_ALBUM_ID_KEY];
    [notificationData setObject:self.sourceName forKey:SOURCE_ALBUM_NAME_KEY];
    [notificationData setObject:self.destinationName forKey:DESTINATION_ALBUM_NAME_KEY];
    
    return notification;
}


@end
