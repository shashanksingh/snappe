//  PhotoSaveManager.m
//  snappe
//
//  Created by Shashank on 21/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoSaveManager.h"
#import "PhotoLibraryReader.h"
#import "Util.h"
#import "PhotoAccessor.h"
#import "ImageHasher.h"
#import "Logger.h"
#import "CoreController.h"

#define PHOTO_SAVE_MAX_TRIES 2
#define PHOTO_SAVE_RETRY_DELAY_SECONDS 5
#define PHOTO_ADD_TO_ALBUM_RETRY_DELAY_SECONDS 2

static const NSString *PHOTO_SAVE_SYNCHRONIZATION_KEY = @"snappe_photo_save_sync_key";

@interface _PhotoSaveRequest : NSObject
@property (strong) NSData *imageData;
@property (strong) NSString *albumName;
@property (strong) NSDictionary *metadata;
@property dispatch_queue_t callerQ;
@property (strong) GenericCallback callback;
@property (strong) GenericErrorback errorback;
@property (strong) NSNumber *tries;
@property (strong) NSURL *assetURL;
@end

@implementation _PhotoSaveRequest

-(id) init: (NSData*)imageData :(NSString*)albumName :(NSDictionary*)metadata :(GenericCallback)callback :(GenericErrorback)errorback
{
    self = [super init];
    if(self) {
        
        [callback copy];
        [errorback copy];
        
        self.imageData = imageData;
        self.albumName = albumName;
        self.metadata = metadata;
        self.callerQ = dispatch_get_current_queue();
        self.callback = callback;
        self.errorback = errorback;
        self.tries = @(0);
        self.assetURL = nil;
    }
    return self;
}
@end



@interface PhotoSaveManager()
@property (strong) NSMutableArray *savePhotoRequestQueue;
@end


@implementation PhotoSaveManager
@synthesize savePhotoRequestQueue;

-(id) init {
    self = [super init];
    if(self) {
        self.savePhotoRequestQueue = [NSMutableArray array];
    }
    return self;
}

+(PhotoSaveManager *) getInstance {
    static PhotoSaveManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PhotoSaveManager alloc] init];
    });
    return sharedInstance;
}

-(void)queueRequest:(_PhotoSaveRequest*)request {
    @synchronized(PHOTO_SAVE_SYNCHRONIZATION_KEY) {
        [self.savePhotoRequestQueue addObject:request];
    }
    [self saveNextPhoto];
}

-(void)requeueRequest:(_PhotoSaveRequest*)request {
    @synchronized(PHOTO_SAVE_SYNCHRONIZATION_KEY) {
        [self.savePhotoRequestQueue insertObject:request atIndex:0];
    }
    [self saveNextPhoto];
}

-(void) savePhoto:(NSData*)imageData :(NSString*)albumName :(NSDictionary*)metadata :(GenericCallback)callback :(GenericErrorback)errorback
{
    _PhotoSaveRequest *request = [[_PhotoSaveRequest alloc] init:imageData :albumName :metadata :callback :errorback];
    [self queueRequest:request];
}

-(void)addPhotoToAlbum:(Photo*)photo:(Album*)album:(GenericCallback)callback:(GenericErrorback)errorback {
    NSString *assetURL = [[PhotoLibraryReader getInstance] getAssetURLForHash:[photo getHash]];
    if(!assetURL) {
        NSString *message = [NSString stringWithFormat:@"addPhotoToAlbum: no assetURL for asset with hash: %@", [photo getHash]];
        [Logger logError:message :nil];
        errorback(nil);
        return;
    }
    
    _PhotoSaveRequest *request = [[_PhotoSaveRequest alloc] init:nil :album.name :nil :callback :errorback];
    [request setAssetURL:[NSURL URLWithString:assetURL]];
    [self queueRequest:request];
}

-(void) saveNextPhoto {
    //@TODO: catch possible exception in callback and errorback
    
    _PhotoSaveRequest *request = nil;
    @synchronized(PHOTO_SAVE_SYNCHRONIZATION_KEY) {
        if(self.savePhotoRequestQueue.count == 0) return;
        request = self.savePhotoRequestQueue[0];
        [self.savePhotoRequestQueue removeObjectAtIndex:0];
    }
    
    if(request.assetURL != nil) {
        //already saved to saved photos album
        [self addPhotoToAlbum:request];
    }
    else if(request.albumName) {
        //if albumName is not provided, save only in global album
        [self savePhotoToSavedPhotosAlbum:request];
    }
    else {
        [self saveNextPhoto];
    }
}

-(void) savePhotoToSavedPhotosAlbum:(_PhotoSaveRequest*)request {
    DLog(@"started saving photo");
    
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    __weak typeof(self) bself = self;
    dispatch_async([CoreController getALAssetsBackgroundQueue], ^{
        [[[PhotoLibraryReader getInstance] getLibrary] writeImageDataToSavedPhotosAlbum:[request imageData] metadata:[request metadata] completionBlock:^(NSURL *assetURL, NSError *error)
         {
             DLog(@"ended saving photo");
             dispatch_async(callerQ, ^{
                 if(error != nil) {
                     if([error code] != ALAssetsLibraryWriteBusyError) {
                         [bself onError:error:request];
                     }
                     else {
                         [Logger track:@"alasset_write_busy_error"];
                         [bself onError:nil:request];
                     }
                 }
                 else if(assetURL == nil) {
                     NSError *err = [NSError errorWithDomain:SNAPPE_ERROR_DOMAIN code:SNAPPE_ERROR_ALASSET_UNEXPECTED userInfo:[NSDictionary dictionaryWithObject:@"nil assetURL returned on saving image data to photo albums" forKey:@"message"]];
                     [bself onError:err:request];
                 }
                 else {
                     [request setAssetURL:assetURL];
                     [bself addPhotoToAlbum:request];
                 }
             });
         }];
    });
    
}

-(void) addPhotoToAlbum:(_PhotoSaveRequest*)request {
    [[PhotoLibraryReader getInstance] getOrCreateLocalAssetsGroup:[request albumName] :^(NSArray *groupData) {
        __block ALAssetsGroup *group = groupData[0];
        
        if(![group isEditable]) {
            [Logger logError:[NSString stringWithFormat:@"album not editable: %@", [group valueForProperty:ALAssetsGroupPropertyName]] :nil];
            [self onError:nil:request];
            return;
        }
        
        [[PhotoLibraryReader getInstance] getAssetByURL:[request assetURL] :^(ALAsset* asset) {
            if(!asset) {
                DLog(@"warning: asset not found for url: %@", request.assetURL);
                [self onError:nil:request];
                return;
            }
            
            BOOL saved = [group addAsset:asset];
            if(!saved) {
                NSString *message = [[NSString alloc] initWithFormat:@"unknow error in adding photo at URL %@ to album named %@", [[request assetURL] absoluteString], [request albumName]];
                DLog(@"%@", message);
                [self onError:nil:request];
            }
            else {
                //[[PhotoLibraryReader getInstance] getAssetHash:asset];
                [self onSuccess:asset:request];
            }
        } :^(NSError *error){
            [self onError:error:request];
        }];
    }:^(NSError *error){
        [self onError:error:request];
    }];
}

-(void) onError:(NSError*)fatalError:(_PhotoSaveRequest*)request {
    if(PHOTO_SAVE_MAX_TRIES < request.tries.intValue && fatalError == nil) {
        fatalError = [NSError errorWithDomain:@"snappe" code:1 userInfo:[NSDictionary dictionaryWithObject:@"too many retries for saving photo" forKey:@"message"]];
    }
    
    if(fatalError != nil) {
        dispatch_async(request.callerQ, ^{
            request.errorback(fatalError);
        });
        [self saveNextPhoto];
        //[self requeueRequest:request];
    }
    else {
        request.tries = @(request.tries.intValue + 1);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, PHOTO_SAVE_RETRY_DELAY_SECONDS * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            [self requeueRequest:request];
        });
    }
}

-(void) onSuccess:(ALAsset*)asset:(_PhotoSaveRequest*)request {
    dispatch_async(request.callerQ, ^{
        request.callback(asset);
    });
    [self saveNextPhoto];
}

+(void)createOrUpdateAlbumWithLocalPhotos:(NSString*)albumName:(NSArray*)assetURLs:(GenericCallback)callback:(GenericErrorback)errorback
{
    [[PhotoLibraryReader getInstance] getOrCreateLocalAssetsGroup:albumName :^(NSArray *groupData) {
        ALAssetsGroup *group = groupData[0];
        [self addLocalPhotosToAssetGroup:group :assetURLs :callback :errorback];
    }:^(NSError *error){
        errorback(error);
    }];
}

+(void)addLocalPhotosToAssetGroupPrivate:(ALAssetsGroup*)group:(NSArray*)assetURLs:(NSInteger)index:(GenericCallback)callback:(GenericErrorback)errorback
{
    if(index >= assetURLs.count) {
        callback(@(assetURLs.count));
        return;
    }
    
    NSString *assetURL = assetURLs[index];
    [[PhotoLibraryReader getInstance] getAssetByURL:[NSURL URLWithString:assetURL] :^(ALAsset* asset) {
        if(!asset) {
            [Logger logError:[NSString stringWithFormat:@"asset not found for url: %@", assetURL] :nil];
        }
        else {
            BOOL saved = [group addAsset:asset];
            if(!saved) {
                NSString *message = [[NSString alloc] initWithFormat:@"unknow error in adding photo at URL %@ to album named %@", assetURL, [group valueForProperty:ALAssetsGroupPropertyName]];
                [Logger logError:message :nil];
            }
        }
        [self addLocalPhotosToAssetGroupPrivate:group :assetURLs :index + 1 :callback :errorback];
        
    } :errorback];
}

+(void)addLocalPhotosToAssetGroup:(ALAssetsGroup*)group:(NSArray*)assetURLs:(GenericCallback)callback:(GenericErrorback)errorback
{
    if(![group isEditable]) {
        [Logger logError:[NSString stringWithFormat:@"album not editable: %@", [group valueForProperty:ALAssetsGroupPropertyName]] :nil];
        errorback(nil);
        return;
    }
    
    [self addLocalPhotosToAssetGroupPrivate:group :assetURLs :0 :callback :errorback];
}
@end