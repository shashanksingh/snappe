//
//  PhotoUploadOperation.m
//  snappe
//
//  Created by Shashank on 28/02/13.
//
//

#import "PhotoUploadOperation.h"
#import "PhotoAccessor.h"
#import "AlbumAccessor.h"
#import "EventManager.h"
#import "SnappeHTTPService.h"
#import "Logger.h"
#import "Util.h"
#import "PhotoBatchUploader.h"

#define PRIVATE_PHOTO_IDS_KEY @"private_photo_ids"
#define PHOTOS_TO_UPLOAD_IDS_KEY @"photos_to_upload"

@interface PhotoUploadOperation()
@property (retain) NSArray *privatePhotos;
@property (retain) NSArray *photosToUpload;
@end

@implementation PhotoUploadOperation
-(id)init:(NSArray*)privatePhotos:(NSArray*)photosToUpload {
    self = [super init];
    if(self) {
        self.privatePhotos = privatePhotos;
        self.photosToUpload = photosToUpload;
        [self setUp];
    }
    return self;
}

-(id)initWithPersistedState:(NSDictionary*)persistedState {
    self = [super initWithPersistedState:persistedState];
    if(self) {
        NSArray *privatePhotosIDs = [persistedState objectForKey:PRIVATE_PHOTO_IDS_KEY];
        NSArray *photosToUploadIDs = [persistedState objectForKey:PHOTOS_TO_UPLOAD_IDS_KEY];
        
        NSMutableArray *privatePhotos = [NSMutableArray array];
        NSMutableArray *photosToUpload = [NSMutableArray array];
        
        for(NSString *pid in privatePhotosIDs) {
            Photo *p = [Photo getById:pid];
            if(!p) {
                DLog(@"missing private photo with id %@ after PhotoUploadOperation init from persistence", pid);
                continue;
            }
            [privatePhotos addObject:p];
        }
        for(NSString *pid in photosToUploadIDs) {
            Photo *p = [Photo getById:pid];
            if(!p) {
                DLog(@"missing photo to upload with id %@ after PhotoUploadOperation init from persistence", pid);
                continue;
            }
            [photosToUpload addObject:p];
        }
        
        self.privatePhotos = privatePhotos;
        self.photosToUpload = photosToUpload;
        
        [self setUp];
    }
    return self;
}

-(NSArray*)getPhotos {
    return self.photosToUpload;
}

-(NSArray*)getEventTypes {
    return @[@(PHOTO_UPLOAD)];
}


-(NSDictionary*)getState {
    NSMutableDictionary *state = [[super getState] mutableCopy];
    [state setObject:NSStringFromClass(self.class) forKey:@"class"];
    [state setObject:[self.privatePhotos valueForKey:@"id"] forKey:PRIVATE_PHOTO_IDS_KEY];
    [state setObject:[self.photosToUpload valueForKey:@"id"] forKey:PHOTOS_TO_UPLOAD_IDS_KEY];
    return state;
}

-(void)start:(GenericCallback)callback {
    self.status = STARTED;
    [self fireUpdate];
    
    GenericCallback onSuccess = ^(id data) {
        [Logger track:@"new_photo_upload" :@{@"private": @(self.privatePhotos.count), @"uploaded": @(self.photosToUpload.count)}];
        
        self.status = SUCCESSFUL;
        self.completion = 100;
        self.finishedOn = [NSDate date];
        
        
        callback(@(self.status));
        
        [self onFinish:YES];
    };
    
    GenericErrorback onError = ^(NSError *error_) {
        if(!([error_.domain isEqualToString:SNAPPE_ERROR_DOMAIN] && error_.code == SNAPPE_ERROR_INSUFFICIENT_CREDITS))
        {
            [Logger logError:[NSString stringWithFormat:@"error in uploading new photos (private: %@, new: %@)", self.privatePhotos, self.photosToUpload] :error_];
            
            //TODO: may be dial down the last sync time to allow repeat of the process without user
            //intervention
            NSString *userMessage = [NSString stringWithFormat:@"There was an error in uploading the new photos. Please add them again and resync"];
            
            [Util alertError:error_:userMessage];
            
        }
        
        self.status = FAILURE;
        self.error = [error_ localizedDescription];
        self.finishedOn = [NSDate date];
        
        callback(@(self.status));
        
        [self onFinish:NO];
        
    };
    
    
    [[SnappeHTTPService getInstance] markPhotosPrivate:self.privatePhotos :^{
        [PhotoBatchUploader upload:self.photosToUpload :onSuccess :onError];
    } :^(NSError *error) {
        onError(error);
    }];
}

-(NSString*)getName {
    return @"Sharing Photos";
}

-(NSString*)getType {
    return @"EXTOP_NEW_PHOTOS_UPLOAD";
}

-(NSString*)getStatusText {
    NSString *statusText = nil;
    if(self.status == NOT_STARTED) {
        statusText = @"waiting...";
    }
    else if(self.status == STARTED) {
        statusText = [NSString stringWithFormat:@"upload %d%% completed", MIN(self.completion, 99)];
    }
    else if(self.status == SUCCESSFUL) {
        long secondsSinceDone = [self.finishedOn timeIntervalSince1970];
        statusText = [NSString stringWithFormat:@"successfully uploaded %@", [Util getRelativeTime:secondsSinceDone]];
    }
    else if(self.status == FAILURE) {
        statusText = @"failed, please try again";
    }
    else {
        DLog(@"warning: unknown value for ext op status: %d", self.status);
    }
    return statusText;
}

-(Notification*)getNotification {
    Notification *notification = [super getNotification];
    
    NSMutableDictionary *notificationData = notification.data;
    [notificationData setObject:[self.photosToUpload valueForKey:@"id"] forKey:@"photos_to_upload"];
    
    return notification;
}


@end
