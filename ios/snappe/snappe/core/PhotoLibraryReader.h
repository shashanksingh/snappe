//
//  PhotoLibraryReader.h
//  snappe
//
//  Created by Shashank on 09/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Macros.h"
#import "AlbumAccessor.h"

typedef void(^PhotoLibraryReaderCallback)(NSArray *album, NSArray*photos);

@interface PhotoLibraryReader : NSObject
+(PhotoLibraryReader *) getInstance;
-(void)validateLibraryAccess:(VoidCallback)callback:(GenericErrorback)errorback;
-(void) reload:(VoidCallback)callback:(GenericErrorback)errorback;
-(ALAssetsLibrary*) getLibrary; 
-(void) getLocalLibrayModifications:(GenericCallback)callback :(GenericErrorback)errorback;
-(NSString *) getAssetHash:(ALAsset *)asset;
-(void) getAssetByHash:(NSString *)hash callback:(ALAssetsLibraryAssetForURLResultBlock)callback errorBack:(ALAssetsLibraryAccessFailureBlock) errorback;
-(void) getAssetByURL:(NSURL*)assetURL :(ALAssetsLibraryAssetForURLResultBlock)callback :(ALAssetsLibraryAccessFailureBlock)errorback;
-(NSString*) getAssetURLForHash:(NSString*)hash;
-(void)getAssetGroupForDevicePersistentID:(NSString*)devicePersistentID:(GenericCallback)callback:(GenericErrorback)errorback;

-(void) getOrCreateLocalAssetsGroup:(NSString*)albumName :(ArrayCallback)callback :(GenericErrorback)errorback;

+(NSString*)getHashForPhotoData:(NSData*)data;
+(NSString*)getMD5DigestForAsset:(ALAsset*)asset;

-(BOOL)assetExistsWithHash:(NSString*)hash;

+(BOOL)isFirstRun;
+(void)invalidateModificationCache;

+(void)getSortedPhotosURLs:(ArrayCallback)callback:(GenericErrorback)errorback;
@end
