//
//  AlbumSyncOperation.h
//  snappe
//
//  Created by Shashank on 03/03/13.
//
//

#import "PhotoTransferOperation.h"
#import "AlbumAccessor.h"

@interface AlbumSyncOperation : PhotoTransferOperation
-(id)init:(Album*)ownAlbum:(Album*)friendAlbum:(BOOL)bridge;
@end
