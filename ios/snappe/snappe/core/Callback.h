//
//  Callback.h
//  snappe
//
//  Created by Shashank on 26/03/13.
//
//

#import <Foundation/Foundation.h>

@interface GenericCallback : NSObject
+(GenericCallback*)get:(void (^)(id data))callback;
-(void)call:(id)data;
@end

@interface GenericErrorback : NSObject
+(GenericErrorback*)get:(void (^)(NSError *error))errorback;
-(void)call:(NSError*)error;
@end

@interface VoidCallback : NSObject
+(VoidCallback*)get:(void (^)(void))callback;
-(void)call;
@end