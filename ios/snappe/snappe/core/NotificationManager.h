//
//  NotificationManager.h
//  snappe
//
//  Created by Shashank on 29/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Macros.h"
#import "NotificationAccessor.h"

@interface NotificationManager : NSObject
+(NotificationManager*) getInstance;
-(void) updateWithServerNotifications:(NSArray*) notificationJSONObjects;
-(NSArray*)getNotifications:(UInt16)limit;
-(NSUInteger)getPendingNotificationCount;
-(void)markNotificationShown:(Notification*)notification;
@end
