//
//  GraphManager.h
//  snappe
//
//  Created by Shashank on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface GraphManager : NSObject
+(GraphManager *) getInstance;

-(NSManagedObjectContext *)getTransientManagedObjectContext;
-(NSManagedObjectContext *)getPersistentManagedObjectContext;
+(BOOL) isManagedObjectTransient:(NSManagedObject*)managedObject;

+(NSArray*) getGraphObjectsByProperties:(NSDictionary*)properties :(NSString*)entityName;
+(id) getGraphObjectById:(NSString*)id WithEntityName:(NSString*)entityName;
+(NSDictionary*) getGraphObjectsById:(NSArray*)idList WithEntityName:(NSString*)entityName;

-(NSArray*)search:(NSString*)query:(NSUInteger)limit;
-(NSArray*)searchAlbums:(NSString*)query:(BOOL)facebookOnly:(NSUInteger)limit;
-(NSArray*)searchUsers:(NSString*)query:(NSUInteger)limit;

-(NSArray*) getUsers;
-(NSArray*) getFriends;
-(NSArray*) getAlbums;
-(NSArray*) getFacebookAlbums;
-(NSArray*) getDeviceAlbums;

+(void) deleteObject:(NSManagedObject*)object;
-(void) savePersistentContext;

+(void) removeNonExistentLocalItemsFromGraph:(NSDictionary*)deleteDescriptor;
+(void)prefetchObjects:(NSString*)entityName:(NSArray*)objects;
+(NSArray*)changeContext:(NSString*)entityName:(NSArray*)objectIDs:(NSManagedObjectContext*)destinationContext;
+(NSDictionary*)groupByAggregate:(NSString*)entityName:(NSString*)aggregationFunction:(NSString*)aggregationProperty:(NSString*)groupProperty:(NSPredicate*)filterPredicate;

+(int)getCount:(NSString*)entityName;
+(NSArray*)getSorted:(NSString*)entityName:(NSString*)sortProperty:(int)offset:(int)limit;
@end
