//
//  Snappe.h
//  snappe
//
//  Created by Shashank on 11/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoLibraryOrganizer.h"
#import "FBManager.h"

typedef enum AppstoreReviewPromptResponse {
    DENIED, POSTPONED, REVIEWED
} AppstoreReviewPromptResponse;

@interface CoreController : NSObject
+(CoreController *) getInstance;
+(dispatch_queue_t)getALAssetsBackgroundQueue;
+(dispatch_queue_t)getCoreDataBackgroundQueue;
-(NSString*) getDeviceID;
-(NSString*) getAPNSToken;
-(void)setAPNSToken:(NSString*)token;
-(void)incrementSuccessfullOperationCount;
-(BOOL)shouldAskForAppstoreReview;
-(void)saveAppstoreReviewPromptResponse:(AppstoreReviewPromptResponse)response;
-(void)saveLastAppstoreReviewPromptTime;
-(BOOL)canShowAds;
@end
