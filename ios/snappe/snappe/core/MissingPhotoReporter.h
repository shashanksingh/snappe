//
//  MissingPhotoReporter.h
//  snappe
//
//  Created by Shashank on 06/01/13.
//
//

#import <Foundation/Foundation.h>
#import "PhotoAccessor.h"

@interface MissingPhotoReporter : NSObject
+(MissingPhotoReporter*) getInstance;
-(void)reportMissingPhoto:(Photo*)photo;
@end
