//
//  BaseExtOp.m
//  snappe
//
//  Created by Shashank on 14/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseExtOp.h"
#import "Util.h"
#import "EventManager.h"
#import "GraphManager.h"
#import "CoreController.h"
#import "AppstoreReviewView.h"

#define ID_KEY @"id"
#define STATUS_KEY @"status"
#define COMPLETION_KEY @"completion"
#define ERROR_KEY @"error"
#define QUEUED_ON_KEY @"queued_on"
#define FINISHED_ON_KEY @"finished_on"
#define NOTIFICATION_ID_KEY @"notification_id"
#define NOTIFICATION_STATUS_TEXT @"status_text"

@interface BaseExtOp()
@property NSInteger lastCompletion;
@end

@implementation BaseExtOp
@synthesize id;
@synthesize status;
@synthesize completion;
@synthesize error;
@synthesize queuedOn;
@synthesize finishedOn;
@synthesize notificationId;

-(id)init {
    self = [super init];
    if(self) {
        self.id = [Util getUUID];
        self.status = NOT_STARTED;
        self.completion = 0;
        self.error = nil;
        self.queuedOn = [NSDate date];
        self.lastCompletion = -1;
    }
    return self;
}

-(id)initWithPersistedState:(NSDictionary*)persistedState {
    self = [super init];
    if(self) {
        self.id = [persistedState objectForKey:ID_KEY];
        self.status = [[persistedState objectForKey:STATUS_KEY] intValue];
        self.completion = [[persistedState objectForKey:COMPLETION_KEY] intValue];
        self.error = [persistedState objectForKey:ERROR_KEY];
        self.queuedOn = [persistedState objectForKey:QUEUED_ON_KEY];
        self.finishedOn = [persistedState objectForKey:FINISHED_ON_KEY];
        self.notificationId = [persistedState objectForKey:NOTIFICATION_ID_KEY];
    }
    return self;
}

-(NSString*)getId {
    return self.id;
}

-(NSString*)getStatusText {
    return nil;
}

-(NSString*)getType {
    return nil;
}

-(NSDictionary*)getState {
    NSMutableDictionary *state = [NSMutableDictionary dictionary];
    [state setObject:self.id forKey:ID_KEY];
    [state setObject:NSStringFromClass(self.class) forKey:@"class"];

    [state setObject:[NSNumber numberWithInt:self.status] forKey:STATUS_KEY];
    [state setObject:[NSNumber numberWithInt:self.completion] forKey:COMPLETION_KEY];
    [state setObject:self.queuedOn forKey:QUEUED_ON_KEY];
    
    if(self.error) {
        [state setObject:self.error forKey:ERROR_KEY];        
    }
    if(self.finishedOn) {
        [state setObject:self.finishedOn forKey:FINISHED_ON_KEY];    
    }
    if(self.notificationId) {
        [state setObject:self.notificationId forKey:NOTIFICATION_ID_KEY];    
    }
    
    NSString *statusText = [self getStatusText];
    if(statusText) {
        [state setObject:statusText forKey:NOTIFICATION_STATUS_TEXT];
    }
    
    return state;
}

-(void)start:(GenericCallback)callback {
}

-(void)onFinish:(BOOL)success {
    if(success) {
        [[CoreController getInstance] incrementSuccessfullOperationCount];
        double delayInSeconds = 4.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [AppstoreReviewView showIfNeeded];
        });
    }
    [self fireUpdate];
}

-(Notification*)getNotification {
    Notification *notification = nil;
    if(self.notificationId) {
        notification = [Notification getById:self.notificationId];
    }
    else {
        self.notificationId = [Util getUUID];
        notification = [Notification create];
        [notification setId:self.notificationId];
        [notification setPending:[NSNumber numberWithBool:YES]];
        [notification setTimestamp:[NSNumber numberWithInt:[self.queuedOn timeIntervalSince1970]]];
        [notification setType:[self getType]];
    }
    
    NSMutableDictionary *notificationData = [[self getState] mutableCopy];
    [notification setData:notificationData];
    
    if(self.lastCompletion < 0 || self.completion - self.lastCompletion > 20 || self.completion >= 99) {
        
    }
    
    self.lastCompletion = self.completion;
    return notification;
    
}

-(void)fireUpdate {
    [[EventManager getInstance] fireEvent:EXTOP_STATUS_CHANGED, self, nil];
    //firing update on ONGOING is too costly
    if(self.notificationId && self.status != ONGOING) {
        [[EventManager getInstance] fireEvent:NOTIFICATION_STATUS_CHANGED, [self getNotification], nil];
    }
}

@end
