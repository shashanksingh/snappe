//
//  UIImage+fixOrientation.h
//  snappe
//
//  Created by Shashank on 01/04/13.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)
- (UIImage *)fixOrientation;
@end
