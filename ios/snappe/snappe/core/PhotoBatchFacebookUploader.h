//
//  PhotoBatchFacebookUploader.h
//  snappe
//
//  Created by Shashank on 28/03/13.
//
//

#import "PhotoBatchProcessor.h"

@interface PhotoBatchFacebookUploader : PhotoBatchProcessor
+(void)upload:(NSArray*)photos:(NSObject*)target:(GenericCallback)callback:(GenericErrorback)errorback;
@end
