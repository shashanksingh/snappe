//
//  UserAccessor.m
//  snappe
//
//  Created by Shashank on 17/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserAccessor.h"
#import "Util.h"
#import "Macros.h"
#import "GraphManager.h"
#import "UIImageView+AFNetworking.h"
#import "TagAccessor.h"
#import "SpatioTemporalLocation.h"

@implementation User (accessor)

+(User*) getById:(NSString *)id {
    return [GraphManager getGraphObjectById:id WithEntityName:@"User"];
}

+(User*) getCurrentUser {
    NSString *currentUserID = [[FBManager getInstance] getUserId];
    if(!currentUserID) {
        return nil;
    }
    return [self getById:currentUserID];
}

+(NSArray*) createOrUpdateWithChangeSets:(NSArray*)changeSets InContext:(NSManagedObjectContext *)context {
    NSDictionary *existingObjects = [GraphManager getGraphObjectsById:[changeSets valueForKey:@"id"] WithEntityName:@"User"];
    
    NSMutableArray *rv = [NSMutableArray array];
    
    for(NSDictionary *changeSet in changeSets) {
        NSString *id = [changeSet objectForKey:@"id"];
        User *user = [existingObjects objectForKey:id];
        BOOL preExisting = user != nil;
        if(!user) {
            user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext: context];
            [user setId:id];
        }
        [self updateWithChangeSet:user :changeSet];
        [rv addObject:@[user, @(preExisting)]];
    }
    
    return rv;
}

+(User*) createOrUpdateWithChangeSet:(NSDictionary *)changeSet InContext:(NSManagedObjectContext *)context {
    
    NSString *id = [changeSet objectForKey:@"id"];
    User *user = [User getById:id];
    if(user == nil) {
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext: context];
        [user setId:[NSString stringWithFormat:@"%@", id]];
    }
    
    [self updateWithChangeSet:user :changeSet];

    return user;
    
}

+(void)updateWithChangeSet:(User*)user:(NSDictionary*)changeSet {
    for(NSString *propertyName in changeSet) {
        
        if([@"first_name" isEqualToString:propertyName]) {
            [user setFirstName:[changeSet objectForKey:propertyName]];
        }
        else if([@"middle_name" isEqualToString:propertyName]) {
            [user setMiddleName: [changeSet objectForKey:propertyName]];
        }
        else if([@"last_name" isEqualToString:propertyName]) {
            [user setLastName: [changeSet objectForKey:propertyName]];
        }
        else if([@"locale" isEqualToString:propertyName]) {
            [user setLocale: [changeSet objectForKey:propertyName]];
        }
        else if([@"name" isEqualToString:propertyName]) {
            [user setName: [changeSet objectForKey:propertyName]];
        }
        else if([@"sex" isEqualToString:propertyName]) {
            [user setSex: [changeSet objectForKey:propertyName]];
        }
        else if([@"is_registered_user" isEqualToString:propertyName]) {
            [user setIsRegisteredUser:[changeSet objectForKey:propertyName]];
        }
        else {
            //DLog(@"warning: unused property: %@, value: %@", propertyName, [changeSet objectForKey:propertyName]);
        }
    }
}

-(NSString*) getId {
    return self.id;
}

-(NSString*) getName {
    return self.name;
}

-(NSArray*) getFriends {
    return [[self friendship] allObjects];
}

-(void) addFriend:(User*)friend {
    [self addFriendshipObject:friend];
}

-(void) removeFriend:(User*)friend {
    [self removeFriendshipObject:friend];
}

-(void) addUserTag:(Tag*)tag {
    [self addTagObject:tag];
}

-(void) removeUserTag:(Tag*)tag {
    [self removeTagObject:tag];
}

-(NSString*) getProfilePhotoURL:(BOOL)isThumb {
    NSString *photoType = isThumb ? @"small" : @"normal";
    return [[NSString alloc] initWithFormat:@"http://graph.facebook.com/%@/picture?type=%@", [self getId], photoType];
}

-(UIImageView *) getProfilePic:(NSUInteger)width :(NSUInteger)height :(BOOL)isThumb {
    NSURL *picURL = [NSURL URLWithString:[self getProfilePhotoURL:isThumb]];
    return [Util getUIImageViewWithSkin:picURL:PROFILE_PIC_PLACEHOLDER:width :height];
}

-(NSArray*) getTaggedLocations {
    NSMutableArray *rv = [[NSMutableArray alloc] init];
    for(Tag *tag in self.tag) {
        SpatioTemporalLocation *loc = [[SpatioTemporalLocation alloc] initWithTag:tag];
        [rv addObject:loc];
    }
    return rv;
}

@end
