//
//  NotificationAccessor.m
//  snappe
//
//  Created by Shashank on 29/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NotificationAccessor.h"
#import "UIImageView+AFNetworking.h"
#import "Util.h"
#import "GraphManager.h"
#import "AlbumAccessor.m"
#import "AlbumShareOperation.h"
#import "ExtendedOperationAccessor.h"

@implementation Notification (accessor)

+(Notification*) getById:(NSString*)notificationId {
    Notification *rv = [GraphManager getGraphObjectById:notificationId WithEntityName:@"Notification"];
    return rv;
}

+(BOOL) checkedExistsLocally:(NSDictionary*)notificationJSON {
    NSString *notificationID = [notificationJSON objectForKey:@"id"];
    Notification *notification = [Notification getById:notificationID];
    return notification != nil;
}

+(Notification*) createFromServerJSON:(NSDictionary*)notificationJSON {
    NSManagedObjectContext *context = [[GraphManager getInstance] getPersistentManagedObjectContext];
    Notification *notification = [NSEntityDescription insertNewObjectForEntityForName:@"Notification" inManagedObjectContext: context];
    
    NSString *notificationID = [notificationJSON objectForKey:@"id"];
    NSNumber *pending = [notificationJSON objectForKey:@"isPending"];
    NSNumber *timestamp = [notificationJSON objectForKey:@"timestamp"];
    NSString *type = [notificationJSON objectForKey:@"type"];
    id data = [notificationJSON objectForKey:@"data"]; 
    
    [notification setId:notificationID];
    [notification setPending:pending];
    [notification setTimestamp:timestamp];
    [notification setType:type];
    [notification setData:data];
    
    return notification;
}

+(Notification*)create{
    NSManagedObjectContext *context = [[GraphManager getInstance] getPersistentManagedObjectContext];
    Notification *notification = [NSEntityDescription insertNewObjectForEntityForName:@"Notification" inManagedObjectContext: context];
    return notification;
}


-(UIImageView*)getThumbnail :(NSUInteger)width :(NSUInteger)height {
    if([self.type isEqualToString:@"ALBUM_SHARED"] || [self.type isEqualToString:@"ALBUM_PUSHED"] || [self.type isEqualToString:@"PHOTOS_UPLOADED"] || [self.type isEqualToString:@"ADDED_AS_UPLOADER"] || [self.type isEqualToString:@"ALBUMS_SYNCED"] || [self.type isEqualToString:@"PHOTOS_PUSHED"])
    {
        NSDictionary *data = self.data;
        NSString *thumbnailURL = [data objectForKey:@"thumbnail_url"];
        if(!thumbnailURL) {
            return nil;
        }
        
        return [Util getUIImageViewWithSkin:[[NSURL alloc] initWithString:thumbnailURL]:PHOTO_PLACEHOLDER:width:height];
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_DOWNLOAD"] || [self.type isEqualToString:@"EXTOP_ALBUM_SHARE"] || [self.type isEqualToString:@"EXTOP_ALBUM_FB_UPLOAD"] || [self.type isEqualToString:@"EXTOP_ALBUM_PUSH"] ||  [self.type isEqualToString:@"EXTOP_ALBUM_PULL"])
    {
        NSString *albumId = [self.data objectForKey:@"src_album_id"];
        Album *album = [Album getById:albumId];
        if(!album) {
            DLog(@"warning: trying to get notification of type %@ with album id %@ which is missing", self.type, albumId);
            return [Util getBrokenImageView:width:height];
        }
        
        return [album getCoverPhoto:width :height :YES];
    }
    else if([self.type isEqualToString:@"EXTOP_NEW_PHOTOS_UPLOAD"]) {
        NSArray *photosToUpload = [self.data objectForKey:@"photos_to_upload"];
        if(photosToUpload.count > 0) {
            Photo *photo = [Photo getById:photosToUpload[0]];
            return [photo getUIImageView:width :height :YES :NO];
        }
        else {
            return [Util getBrokenImageView:width:height];
        }
        
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_SYNC"]) {
        NSString *albumID = [self.data objectForKey:@"own_album_id"];
        Album *album = [Album getById:albumID];
        if(!album) {
            DLog(@"warning: trying to get notification of type %@ with own album id %@ which is missing", self.type, albumID);
            albumID = [self.data objectForKey:@"friend_album_id"];
            album = [Album getById:albumID];
            if(!album) {
                DLog(@"warning: trying to get notification of type %@ with friend album id %@ which is missing", self.type, albumID);    
            }
            return [Util getBrokenImageView:width:height];
        }
        return [album getCoverPhoto:width :height :YES];
    }
    else if([self.type isEqualToString:@"EXTOP_ADD_PHOTOS"]) {
        NSString *albumId = [self.data objectForKey:@"recipient_album_id"];
        Album *album = [Album getById:albumId];
        if(!album) {
            DLog(@"warning: trying to get notification of type %@ with album id %@ which is missing", self.type, albumId);
            return [Util getBrokenImageView:width:height];
        }
        return [album getCoverPhoto:width :height :YES];
    }
    else if([self.type isEqualToString:@"EXTOP_CREATE_ALBUM"]) {
        NSArray *photoIDs = self.data[@"photo_ids"];
        if(photoIDs.count > 0) {
            Photo *coverPhoto = [Photo getById:photoIDs[0]];
            if(coverPhoto) {
                return [coverPhoto getUIImageView:width :height :YES :NO];
            }
        }
        
        return [Util getBrokenImageView:width:height];
    }
    else {
        DLog(@"unknown notification type: %@", self.type);
    }
    return nil;
}

-(NSString*)getDescription {
    if([self.type isEqualToString:@"ALBUM_SHARED"]) {
        NSDictionary *data = self.data;
        NSString *sharerFullName = [data objectForKey:@"from_full_name"];
        NSString *fromID = [data objectForKey:@"from_id"];
        
        NSString *albumName = [data objectForKey:@"album_name"];
        NSString *albumID = [data objectForKey:@"album_id"];
        
        NSString *a = sharerFullName;
        if(fromID) {
            a = [NSString stringWithFormat:@"%@", sharerFullName ];
        }
        
        NSString *b = albumName;
        if(albumID) {
            b = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:albumID], albumName];
        }
        
        NSString *description = [NSString stringWithFormat:@"%@ shared album %@", a, b];
        return description;    
    }
    else if([self.type isEqualToString:@"ALBUM_PUSHED"]) {
        NSDictionary *data = self.data;
        NSString *pusherFullName = [data objectForKey:@"from_full_name"];
        NSString *pusherID = [data objectForKey:@"from_id"];
        
        NSString *destAlbumName = [data objectForKey:@"destination_album_name"];
        NSString *destAlbumID = [data objectForKey:@"destination_album_id"];
        
        NSString *a = pusherFullName;
        if(pusherID) {
            a = [NSString stringWithFormat:@"%@", pusherFullName];
        }
        
        NSString *b = destAlbumName;
        if(destAlbumID) {
            b = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:destAlbumID], destAlbumName];
        }
        
        NSString *description = [NSString stringWithFormat:@"%@ pushed their album to %@", a, b];
        return description;
    }
    else if([self.type isEqualToString:@"ALBUM_PULLED"]) {
        NSDictionary *data = self.data;
        NSString *pullerFullName = [data objectForKey:@"from_full_name"];
        NSString *pullerID = [data objectForKey:@"from_id"];
        
        NSString *srcAlbumName = [data objectForKey:@"source_album_name"];
        NSString *srcAlbumID = [data objectForKey:@"source_album_id"];
        
        NSString *a = pullerFullName;
        if(pullerID) {
            a = [NSString stringWithFormat:@"%@", pullerFullName];
        }
        
        NSString *b = srcAlbumName;
        if(srcAlbumID) {
            b = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:srcAlbumID], srcAlbumName];
        }
        
        NSString *description = [NSString stringWithFormat:@"%@ pulled album %@ into their album", a, b];
        return description;
    }
    else if([self.type isEqualToString:@"ALBUMS_SYNCED"]) {
        NSDictionary *data = self.data;
        NSString *syncerFullName = [data objectForKey:@"from_full_name"];
        NSString *syncerID = [data objectForKey:@"from_id"];
        
        NSString *remoteAlbumName = [data objectForKey:@"remote_album_name"];
        NSString *remoteAlbumID = [data objectForKey:@"remote_album_id"];
        
        NSString *a = syncerFullName;
        if(syncerID) {
            a = [NSString stringWithFormat:@"%@", syncerFullName];
        }
        
        NSString *b = remoteAlbumName;
        if(remoteAlbumID) {
            b = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:remoteAlbumID], remoteAlbumName];
        }
        
        NSString *description = [NSString stringWithFormat:@"%@ synced their album with %@", a, b];
        return description;
    }
    else if([self.type isEqualToString:@"PHOTOS_UPLOADED"]) {
        NSDictionary *data = self.data;
        NSString *sharerFullName = [data objectForKey:@"from_full_name"];
        NSString *fromID = [data objectForKey:@"from_id"];
        
        NSString *albumName = [data objectForKey:@"album_name"];
        NSString *albumID = [data objectForKey:@"album_id"];
        
        NSString *a = sharerFullName;
        if(fromID) {
            a = [NSString stringWithFormat:@"%@", sharerFullName ];
        }
        
        NSString *b = albumName;
        if(albumID) {
            b = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:albumID], albumName];
        }
        
        NSString *description = [NSString stringWithFormat:@"%@ uploaded photos to album %@", a, b];
        return description;
    }
    else if([self.type isEqualToString:@"PHOTOS_PUSHED"]) {
        NSDictionary *data = self.data;
        
        int pushedPhotoCount = ((NSNumber*)[data objectForKey:@"pushed_photo_count"]).intValue;
        
        NSString *albumName = [data objectForKey:@"receiver_album_name"];
        NSString *albumID = [data objectForKey:@"receiver_album_id"];
        
        NSString *a = albumName;
        if(albumID) {
            a = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:albumID], albumName];
        }
        
        NSString *description = [NSString stringWithFormat:@"%d photo%@ pushed to album %@", pushedPhotoCount, pushedPhotoCount > 1 ? @"s" : @"", a];
        return description;
    }
    else if([self.type isEqualToString:@"ADDED_AS_UPLOADER"]) {
        NSDictionary *data = self.data;
        NSString *sharerFullName = [data objectForKey:@"from_full_name"];
        NSString *fromID = [data objectForKey:@"from_id"];
        
        NSString *albumName = [data objectForKey:@"album_name"];
        NSString *albumID = [data objectForKey:@"album_id"];
        
        NSString *a = sharerFullName;
        if(fromID) {
            a = [NSString stringWithFormat:@"%@", sharerFullName ];
        }
        
        NSString *b = albumName;
        if(albumID) {
            b = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:albumID], albumName];
        }
        
        NSString *description = [NSString stringWithFormat:@"%@ added you as uploader to their album %@", a, b];
        return description;
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_DOWNLOAD"]) {
        //we specially record album_name (besides album id) to take
        //care of cases where album gets deleted
        NSString *sourceAlbumName = [self.data objectForKey:@"src_album_name"];
        NSString *sourceAlbumID = [self.data objectForKey:@"src_album_id"];
        
        NSString *destinationAlbumName = [self.data objectForKey:@"dest_album_name"];
        
        NSString *a = sourceAlbumName;
        if(sourceAlbumID) {
            a = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:sourceAlbumID], sourceAlbumName];
        }
        
        NSString *description = [NSString stringWithFormat:@"%@ download%@ to <i>%@</i>", a, [self isDoneSuccessfully] ? @"ed" : @"", destinationAlbumName];
        
        return description;
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_SHARE"]) {
        //we specially record album_name (besides album id) to take
        //care of cases where album gets deleted
        NSString *sourceAlbumName = [self.data objectForKey:@"src_album_name"];
        NSString *sourceAlbumID = [self.data objectForKey:@"src_album_id"];
        
        NSArray *destinationUserNames = [self.data objectForKey:@"dest_user_name"];
        NSArray *destinationUserIDs = [self.data objectForKey:@"dest_user_id"];
        
        NSString *a = sourceAlbumName;
        if(sourceAlbumID) {
            a = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:sourceAlbumID], sourceAlbumName];
        }

        NSString *b = [Util getNameForMultipleUsers:destinationUserIDs:destinationUserNames :YES];
        
        NSString *description = [NSString stringWithFormat:@"%@ share%@ with %@", a, [self isDoneSuccessfully] ? @"d" : @"", b];
        return description;
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_FB_UPLOAD"] || [self.type isEqualToString:@"EXTOP_ALBUM_PUSH"] || [self.type isEqualToString:@"EXTOP_ALBUM_PULL"])
    {
        //we specially record album_name (besides album id) to take
        //care of cases where album gets deleted
        NSString *sourceAlbumName = [self.data objectForKey:@"src_album_name"];
        NSString *sourceAlbumID = [self.data objectForKey:@"src_album_id"];
        
        NSString *destinationAlbumName = [self.data objectForKey:@"dest_album_name"];
        NSString *destinationAlbumID = [self.data objectForKey:@"dest_album_id"];
        
        NSString *a = sourceAlbumName;
        if(sourceAlbumID) {
            a = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:sourceAlbumID], sourceAlbumName];
        }
        
        NSString *b = destinationAlbumName;
        if(destinationAlbumID) {
            b = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", [Util getSnappeURLForID:destinationAlbumID], destinationAlbumName];
        }
        
        NSString *verb = @"upload";
        if([self.type isEqualToString:@"EXTOP_ALBUM_PUSH"]) {
            verb = @"push";
        }
        else if([self.type isEqualToString:@"EXTOP_ALBUM_PULL"]) {
            verb = @"pull";
        }
        
        NSString *description = [NSString stringWithFormat:@"%@ %@%@ to %@", a, verb, [self isDoneSuccessfully] ? @"ed" : @"", b];
        
        return description;
    }
    else if([self.type isEqualToString:@"EXTOP_NEW_PHOTOS_UPLOAD"]) {
        NSArray *photosToUpload = [self.data objectForKey:@"photos_to_upload"];
        if([self isDoneSuccessfully]) {
            return [NSString stringWithFormat:@"%d new photo%@ added to shared albums", photosToUpload.count, photosToUpload.count == 1 ? @"" : @"s"];
        }
        return [NSString stringWithFormat:@"adding %d new photo%@ to shared albums", photosToUpload.count, photosToUpload.count == 1 ? @"" : @"s"];
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_SYNC"]) {
        NSString *ownAlbumID = [self.data objectForKey:@"own_album_id"];
        NSString *friendAlbumID = [self.data objectForKey:@"friend_album_id"];
        
        NSString *ownAlbumName = [self.data objectForKey:@"own_album_name"];
        NSString *friendAlbumName = [self.data objectForKey:@"friend_album_name"];
        
        return [NSString stringWithFormat:@"<a href=\"%@\">%@</a> sync%@ with <a href=\"%@\">%@</a>", [Util getSnappeURLForID:ownAlbumID], ownAlbumName, ([self isDoneSuccessfully] ? @"ed" : @"ing"), [Util getSnappeURLForID:friendAlbumID], friendAlbumName];
    }
    else if([self.type isEqualToString:@"EXTOP_ADD_PHOTOS"]) {
        NSString *recipientAlbumID = [self.data objectForKey:@"recipient_album_id"];
        NSString *recipientAlbumName = [self.data objectForKey:@"recipient_album_name"];
        NSArray *addedPhotoIDs = [self.data objectForKey:@"photos_to_upload"];
        
        return [NSString stringWithFormat:@"%d photo%@ added to <a href=\"%@\">%@</a>", addedPhotoIDs.count, addedPhotoIDs.count > 1 ? @"s" : @"", [Util getSnappeURLForID:recipientAlbumID], recipientAlbumName];
    }
    else if([self.type isEqualToString:@"EXTOP_CREATE_ALBUM"]) {
        NSString *albumName = self.data[@"album_name"];
        NSArray *photoIDs = self.data[@"photo_ids"];
        NSNumber *isFacebook = self.data[@"is_facebook_album"];
        
        //TODO: link to the new album
        return [NSString stringWithFormat:@"%@ %@ %@%@", [self isDoneSuccessfully] ? @"Created" : @"Creating", isFacebook.boolValue ? @"Facebook album" : @"Album",  albumName, photoIDs.count == 0 ? @"" : [NSString stringWithFormat:@" with %d photo%@", photoIDs.count, photoIDs.count == 1 ? @"" : @"s"]];
    }
    else {
        DLog(@"warning: unknown notification type: %@", self.type);
    }
    
    return nil;
    
}

-(NSString*)getTitle {
    if([self.type isEqualToString:@"ALBUM_SHARED"]) {
        return @"Album Shared";
    }
    if([self.type isEqualToString:@"ALBUM_PUSHED"]) {
        return @"Album Pushed";
    }
    else if([self.type isEqualToString:@"PHOTOS_UPLOADED"]) {
        return @"Photos Uploaded";
    }
    else if([self.type isEqualToString:@"ADDED_AS_UPLOADER"]) {
        return @"Added As Uploader";
    }
    else if([self.type isEqualToString:@"PHOTOS_PUSHED"]) {
        return @"Photos Pushed";
    }
    else if([self.type isEqualToString:@"EXTOP_ADD_PHOTOS"]) {
        return @"Photos Added";
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_DOWNLOAD"]) {
        return [self isDoneSuccessfully] ? @"Album Downloaded" : @"Album Downloading";
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_SHARE"]) {
        return [self isDoneSuccessfully] ? @"Album Shared" : @"Sharing Album";
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_FB_UPLOAD"]) {
        //this type includes both outgoing FB and Snappe uploads
        return [self isDoneSuccessfully] ? @"Album Uploaded" : @"Uploading Album";
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_PUSH"]) {
        return [self isDoneSuccessfully] ? @"Album Pushed" : @"Pushing Album";
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_PULL"]) {
        return [self isDoneSuccessfully] ? @"Album Pulled" : @"Pulling Album";
    }
    else if([self.type isEqualToString:@"EXTOP_NEW_PHOTOS_UPLOAD"]) {
        return [self isDoneSuccessfully] ? @"New Photos Shared" : @"Sharing New Photos";
    }
    else if([self.type isEqualToString:@"EXTOP_ALBUM_SYNC"] || [self.type isEqualToString:@"ALBUMS_SYNCED"]) {
        return [self isDoneSuccessfully] ? @"Albums Synced" : @"Syncing Albums";
    }
    else if([self.type isEqualToString:@"EXTOP_CREATE_ALBUM"]) {
        return [self isDoneSuccessfully] ? @"Album Created" : @"Creating Album";
    }
    else {
        DLog(@"warning: unknown notification type: %@", self.type);
    }
    
    return nil;
}

-(BOOL)isDoneSuccessfully {
    //take care of both extop notifications and other notifications
    NSNumber *status = [self.data objectForKey:@"status"];
    return !status || [status intValue] == SUCCESSFUL;
}

-(NSString*)getStatusText {
    //if it's not done yet or failed let the extop notification's custom
    //text be the status text
    if(![self isDoneSuccessfully]) {
        NSString *statusText = [self.data objectForKey:@"status_text"];
        if(statusText) {
            return statusText;
        }
    }

    return [Util getRelativeTime:[self.timestamp longValue]];
}

@end
