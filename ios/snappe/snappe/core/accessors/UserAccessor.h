//
//  UserAccessor.h
//  snappe
//
//  Created by Shashank on 17/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface User (accessor)
+(User*) getById:(NSString *)id;
+(User*) getCurrentUser;

+(User*) createOrUpdateWithChangeSet:(NSDictionary *)changeSet InContext:(NSManagedObjectContext *)context;
+(NSArray*) createOrUpdateWithChangeSets:(NSArray*)changeSets InContext:(NSManagedObjectContext *)context;

-(NSString*) getId;
-(NSString*) getName;
-(NSArray*) getFriends; 

-(UIImageView*) getProfilePic:(NSUInteger)width :(NSUInteger)height :(BOOL)isThumb;

-(void) unsetCurrentLocation;

-(void) addFriend:(User*)friend;
-(void) removeFriend:(User*)friend;

-(void) addUserTag:(Tag*)tag;
-(void) removeUserTag:(Tag*)tag;

-(NSArray*) getTaggedLocations;
@end
