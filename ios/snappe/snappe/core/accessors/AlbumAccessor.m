//
//  PhotoEvent.m
//  snappe
//
//  Created by Shashank on 10/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

#import "AlbumAccessor.h"
#import "Util.h"
#import "UserAccessor.h"
#import "GraphManager.h"
#import "SnappeHTTPService.h"
#import "EventManager.h"
#import "PhotoLibraryReader.h"
#import "PhotoAccessor.h"
#import "CoreController.h"
#import "ImageHasher.h"
#import "SpatioTemporalLocation.h"
#import "PhotoSaveManager.h"
#import "ExtendedOperationAccessor.h"
#import "PhotoHashUpdater.h"
#import "Logger.h"
#import "CreditsManager.h"
#import "PhotoBatchUploader.h"
#import "PhotoBatchDownloader.h"
#import "PhotoBatchHashUpdater.h"
#import "PhotoBatchFacebookUploader.h"

#define PHOTO_LIBRARY_CHANGE_AFTER_DOWNLOAD_WAIT 10

@implementation Album (accessor)

+(Album*) getById:(NSString *)id {
    return [GraphManager getGraphObjectById:id WithEntityName:@"Album"];
}

+(Album*) getByDevicePersistentId:(NSString *)devicePersistentID {
    NSArray *matches = [GraphManager getGraphObjectsByProperties:[NSDictionary dictionaryWithObject:devicePersistentID forKey:@"devicePersistentID"] :@"Album"];
    if([matches count] > 1) {
        DLog(@"warning: duplicated entries with same persistent id: %@", devicePersistentID);
    }
    if([matches count] == 0) {
        return nil;
    }
    return [matches objectAtIndex:0];
}

+(NSArray*)getDeviceAlbumsWithName:(NSString*)name {
    NSString *deviceID = [[CoreController getInstance] getDeviceID];
    NSDictionary *queryParams = @{@"name": name, @"deviceID" : deviceID};
    NSArray *localAlbumsWithName = [GraphManager getGraphObjectsByProperties :queryParams :@"Album"];
    return localAlbumsWithName;
}

+(NSArray*) createOrUpdateWithChangeSets:(NSArray*)changeSets InContext:(NSManagedObjectContext *)context {
    NSDictionary *existingObjects = [GraphManager getGraphObjectsById:[changeSets valueForKey:@"id"] WithEntityName:@"Album"];
    
    NSMutableArray *rv = [NSMutableArray array];
    
    for(NSDictionary *changeSet in changeSets) {
        NSString *id = [changeSet objectForKey:@"id"];
        Album *album = [existingObjects objectForKey:id];
        BOOL preExisting = album != nil;
        if(!album) {
            album = [NSEntityDescription insertNewObjectForEntityForName:@"Album" inManagedObjectContext: context];
            [album setId:id];
        }
        [self updateWithChangeSet:album :changeSet];
        [rv addObject:@[album, @(preExisting)]];
    }
    
    return rv;
}

+(Album*)createOrUpdateWithChangeSet:(NSDictionary *)changeSet InContext:(NSManagedObjectContext *)context {
    
    NSString *id = [changeSet objectForKey:@"id"];
    Album *album = [Album getById:id];
    if(album == nil) {
        album = [NSEntityDescription insertNewObjectForEntityForName:@"Album" inManagedObjectContext: context];
        [album setId:id];
    }
    
    [self updateWithChangeSet:album :changeSet];
    
    return album;
}

+(void)updateWithChangeSet:(Album*)album:(NSDictionary*)changeSet {
    for(NSString *propertyName in changeSet) {
        
        if([@"name" isEqualToString:propertyName]) {
            [album setName:[changeSet objectForKey:propertyName]];
        }
        else if([@"description" isEqualToString:propertyName]) {
            [album setDescription:[changeSet objectForKey:propertyName]];
        }
        else if([@"created" isEqualToString:propertyName]) {
            [album setCreatedOn:[Util unixTimestampToDate:[changeSet objectForKey:propertyName]]];
        }
        else if([@"modified" isEqualToString:propertyName] || [@"__mod_on__" isEqualToString:propertyName]) {
            NSDate *newDate = [Util unixTimestampToDate:[changeSet objectForKey:propertyName]];
            NSDate *currentDate = album.modifiedOn;
            
            if(!currentDate) {
                if([@"modified" isEqualToString:propertyName]) {
                    [album setModifiedOn:newDate];
                }
            }
            else {
                if([currentDate compare:newDate] == NSOrderedAscending) {
                    [album setModifiedOn:newDate];    
                }
            }
        
        }
        else if([@"album_type" isEqualToString:propertyName]) {
            [album setType:[changeSet objectForKey:propertyName]];
        }
        else if([@"device_id" isEqualToString:propertyName]) {
            [album setDeviceID:[changeSet objectForKey:propertyName]];
        }
        else if([@"device_persistent_id" isEqualToString:propertyName]) {
            [album setDevicePersistentID:[changeSet objectForKey:propertyName]];
        }
        else if([@"object_id" isEqualToString:propertyName]) {
            NSString *objectID = [NSString stringWithFormat:@"%@", [changeSet objectForKey:propertyName]];
            [album setFBObjectID:objectID];
        }
        else {
            //DLog(@"warning: unused property: %@, value: %@", propertyName, [changeSet objectForKey:propertyName]);
        }
    }
}

+(Album*) getTransientInstance {
    NSManagedObjectContext *context = [[GraphManager getInstance] getTransientManagedObjectContext];
    Album *album = [NSEntityDescription insertNewObjectForEntityForName:@"Album" inManagedObjectContext: context];
    return album;
}

-(void) setDescription:(NSString *)description {
    [self setValue:description forKey:@"summary"];
}

-(void) setFBObjectID:(NSString*)fbObjectID {
    [self setValue:fbObjectID forKey:@"fbObjectID"];
}

-(void) setIsSharedWith:(User*)user {
    [self addSharedWithObject:user];
}

-(void) unsetIsSharedWith:(User*)user {
    [self removeSharedWithObject:user];
}

-(BOOL) isSharedWithUser:(User*)user {
    for(User *friend in self.sharedWith) {
        if([friend.id isEqualToString:user.id]) {
            return YES;
        }
    }
    return NO;
    return [self.sharedWith containsObject:user];
}

-(BOOL) isShared {
    return self.sharedWith.count > 0;
}

-(NSString *) getDescription {
    return [self valueForKey:@"summary"];
}

-(NSString*) getId {
    return self.id;
}

-(NSString *) getName {
    return self.name;
}

-(NSUInteger) getPhotoCount {
    return [self.photos count];
}

-(NSArray*) getPhotos:(BOOL)sorted:(BOOL)available:(BOOL)privateOnly {
    NSMutableArray *array = [NSMutableArray array];
    BOOL currentDeviceAlbum = [self isCurrentDeviceAlbum];
    
    for(Photo *photo in self.photos) {
        if(available && !currentDeviceAlbum && !photo.src) {
            continue;
        }
        if(privateOnly && photo.markedPrivate.boolValue) {
            continue;
        }
        
        [array addObject:photo];
    }
    
    if(!sorted) {
        return array;
    }
    
    return [array sortedArrayUsingComparator:^NSComparisonResult(Photo *p1, Photo *p2) {
        return [p1.clickedOn compare:p2.clickedOn];
    }];
}

-(NSDate*) getModifiedOn {
    return self.modifiedOn;
}

-(Photo *) getPhotoAtIndex:(NSUInteger) index {
    return [self.photos objectAtIndex:index];
}

-(NSString*) getDevicePersistentID {
    return self.devicePersistentID;
}

-(NSString*)getFBObjectID {
    return self.fbObjectID;
}

-(ALAssetsGroupType) getDeviceAlbumType {
    return [self.deviceAlbumType intValue];
}

-(NSString*) getDeviceAlbumTypeName {
    ALAssetsGroupType type = [self.deviceAlbumType intValue];
    switch (type) {
        case ALAssetsGroupEvent:
            return @"event";
        case ALAssetsGroupAlbum:
            return @"album";
        case ALAssetsGroupPhotoStream:
            return @"photostream";
        default:
            return @"default";
    }
}


-(BOOL) isAlbumGroup {
    return [self.deviceAlbumType intValue] & ALAssetsGroupAlbum;
}

-(BOOL) isEventGroup {
    return [self.deviceAlbumType intValue] & ALAssetsGroupEvent;
}

-(BOOL) isPhotoStreamGroup {
    return [self.deviceAlbumType intValue] & ALAssetsGroupPhotoStream;
}

-(BOOL) isSavedPhotosGroup {
    ALAssetsGroupType type = self.deviceAlbumType.intValue;
    return (type & ALAssetsGroupSavedPhotos) || (type & ALAssetsGroupLibrary);
}

-(BOOL) isOwnedByCurrentUser {
    NSString *userId = [[FBManager getInstance] getUserId];
    return [userId isEqualToString:self.owner.id];
}

-(BOOL) isCurrentDeviceAlbum {
    NSString *dID = [[CoreController getInstance] getDeviceID];
    BOOL rv = self.deviceID && [self.deviceID isEqualToString:dID];
    return rv;
}

-(BOOL) isCurrentDevicePhysicalAlbum {
    return [self isCurrentDeviceAlbum] && self.devicePersistentID != nil;
}

-(BOOL) isFacebookAlbum {
    return self.deviceID && [self.deviceID isEqualToString:@"facebook"];
}

-(void) setIsAllowedUpload:(User*)user {
    [self addCanUploadObject:user];
}

-(void) unsetIsAllowUpload:(User*)user {
    User *toRemove = nil;
    for(User *allowedUploadUser in self.canUpload) {
        if([allowedUploadUser.id isEqualToString:user.id]) {
            toRemove = allowedUploadUser;
            break;
        }
    }
    
    if(toRemove) {
        [self removeCanUploadObject:toRemove];
    }
    else {
        DLog(@"trying to remove user %@ from allowed uploaders of %@, but user is not already allowed", user.id, self.id);
    }
}

-(BOOL) isAllowedUpload:(User*)user {
    for(User *allowedUploadUser in self.canUpload) {
        if([allowedUploadUser.id isEqualToString:user.id]) {
            return YES;
        }
    }
    return NO;
}

+(Album*) getByDevicePersistentID:(NSString*)devicePersistentID {
    NSManagedObjectContext *context = [[GraphManager getInstance] getPersistentManagedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setResultType: NSManagedObjectResultType];
    [request setEntity:[NSEntityDescription entityForName:@"Album" inManagedObjectContext:context]];
    [request setPredicate:[NSPredicate predicateWithFormat:@"devicePersistentID == %@", devicePersistentID]];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:request error:&error];
    if(fetchedObjects == nil) {
        [Logger logError:@"error in getting Album by devicePersistentID":error];
        return nil;
    }
    if([fetchedObjects count] == 0) {
        return nil;
    }
    return [fetchedObjects objectAtIndex:0];
}

+(Album*) fromAssetGroup:(ALAssetsGroupInfo*)group {
    return [self fromAssetGroup:group :YES];
}

+(Album*) fromAssetGroup:(ALAssetsGroupInfo*)group :(BOOL)checkExists {
    Album *album = nil;
    if(checkExists) {
        album = [Album getByDevicePersistentID:group.devicePersistentID];
        if(album) return album;    
    }
    
    NSManagedObjectContext *context = [[GraphManager getInstance] getTransientManagedObjectContext];
    album = [NSEntityDescription insertNewObjectForEntityForName:@"Album" inManagedObjectContext: context];
    
    [album setId:[Util getUUID]];
    [album setDevicePersistentID:group.devicePersistentID];
    [album setDeviceAlbumType:group.type];
    [album setName:group.name];
    [album setModifiedOn:[NSDate date]];
    [album setDeviceID:[[CoreController getInstance] getDeviceID]];
    
    return album;

}

+(void) getOrCreateDeviceAlbumByName:(NSString *)name :(GenericCallback)callback :(GenericErrorback)errorback {
    NSArray *localAlbumsWithName = [self getDeviceAlbumsWithName:name];
    if(!localAlbumsWithName) {
        errorback(nil);
        return;
    }
    
    Album *physicalDeviceAlbum = nil;
    for(Album *album in localAlbumsWithName) {
        if(album.devicePersistentID) {
            physicalDeviceAlbum = album;
            break;
        }
    }
    
    if(physicalDeviceAlbum) {
        callback(physicalDeviceAlbum);
    }
    else {
        [[PhotoLibraryReader getInstance] getOrCreateLocalAssetsGroup:name :^(NSArray *groupInfo) {
            ALAssetsGroup *group = groupInfo[0];
            callback([Album fromAssetGroup:[ALAssetsGroupInfo fromAssetsGroup:group]]);
        } :errorback];
    }
}

-(void)download:(Album*)destinationAlbum:(BOOL)ignoreSimilar :(VoidCallback)callback :(GenericErrorback)errorback
{
    if([destinationAlbum isCurrentDevicePhysicalAlbum]) {
        [self.class download:[self getPhotos:YES :YES :YES] :destinationAlbum :ignoreSimilar :callback :errorback];
    }
    else {
        [self downloadIntoAlbumByName:destinationAlbum.name :ignoreSimilar :callback :errorback];
    }
}

-(void)downloadIntoAlbumByName:(NSString*)destAlbumName:(BOOL)ignoreSimilar :(VoidCallback)callback :(GenericErrorback)errorback
{
    
    [[PhotoLibraryReader getInstance] getOrCreateLocalAssetsGroup:destAlbumName :^(NSArray *data) {
        ALAssetsGroup *assetGroup = data[0];
        BOOL newGroup = ((NSNumber*)data[1]).boolValue;
        
        
        ALAssetsGroupInfo *info = [ALAssetsGroupInfo fromAssetsGroup:assetGroup];
        Album *downloadAlbum;
        if(!newGroup) {
            NSString *devicePersistentID = info.devicePersistentID;
            downloadAlbum = [Album getByDevicePersistentID:devicePersistentID];
            
            //this should never happen
            if(!downloadAlbum) {
                [[PhotoLibraryOrganizer getInstance] sync:^{
                    [self downloadIntoAlbumByName:destAlbumName :ignoreSimilar :callback :errorback];
                } :errorback];
                return;
            }
        } else {
            downloadAlbum = [Album fromAssetGroup:info];
        }
        [self.class download:[self getPhotos:YES :YES :YES] :downloadAlbum :ignoreSimilar :callback :errorback];
        
    } :errorback];
    
}

+(void)download:(NSArray*)photosToDownload:(Album*)destinationAlbum:(BOOL)ignoreSimilar :(VoidCallback)callback :(GenericErrorback)errorback
{
    
    NSSet *existingHashes = [destinationAlbum getHashes];
    NSSet *existingPHashes = [destinationAlbum getPHashes];
    
    
    NSMutableArray *localPhotos = [NSMutableArray array];
    for(Photo *photo in photosToDownload) {
        NSString *hash = [photo getHash];
        if(hash && [[PhotoLibraryReader getInstance] assetExistsWithHash:hash]) {
            [localPhotos addObject:photo];
        }
    }
    
    NSArray *remotePhotos = [photosToDownload filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(Photo *photo, NSDictionary *bindings)
    {
        return ![localPhotos containsObject:photo];
    }]];
    
    DLog(@"saving %d local photos", localPhotos.count);
    [self saveLocalPhotosInLocalAlbum:localPhotos :destinationAlbum :0 :^(id data) {
        [PhotoBatchDownloader download:remotePhotos :destinationAlbum :existingHashes :existingPHashes :callback :errorback];
    } :errorback];
}

+(void)saveLocalPhotosInLocalAlbum:(NSArray*)photoToSave:(Album*)album:(NSUInteger)index:(GenericCallback)callback:(GenericErrorback)errorback
{
    if(index >= photoToSave.count) {
        callback(@(photoToSave.count));
        return;
    }
    
    Photo *photo = [photoToSave objectAtIndex:index];
    [[PhotoSaveManager getInstance] addPhotoToAlbum:photo :album :^(id data) {
        [self saveLocalPhotosInLocalAlbum:photoToSave :album :(index + 1) :callback :errorback];
    } :errorback];
    
}

-(void)upload:(GenericCallback)callback:(GenericErrorback)errorback {
    if(![self anyPhotoNeedsUploading]) {
        callback(@(0));
        return;
    }
    
    [[SnappeHTTPService getInstance] getPhotoHashesToUpload:self :^(NSArray *photoHashesNeedingUpload) {
        
        NSMutableSet *photoHashSetNeedingUpload = [NSMutableSet setWithArray:photoHashesNeedingUpload];
        
        NSMutableArray *photosNeedingUpload = [NSMutableArray array];
        
        //upload in the order in which photos were taken
        //TODO: we mangle things with set anyway later
        NSArray *photos = [self getPhotos:YES :YES :NO];
        for(Photo *srcPhoto in photos) {
            NSString *hash = [srcPhoto getHash];
            if(!hash) {
                continue;
            }
            
            if(srcPhoto.markedPrivate.boolValue) {
                [photoHashSetNeedingUpload removeObject:hash];
            }
            
            if(![photoHashSetNeedingUpload containsObject:hash]) {
                [[EventManager getInstance] fireEvent:PHOTO_UPLOAD, srcPhoto.id, @(STARTED), nil];
                [[EventManager getInstance] fireEvent:PHOTO_UPLOAD, srcPhoto.id, @(SUCCESSFUL), nil];
            }
            else {
                [photosNeedingUpload addObject:srcPhoto];
            }
        }
        
        [PhotoBatchUploader upload:photosNeedingUpload :callback :errorback];
        
    } :^(NSError *error){
        
        [Logger logError:@"error in uploading album":error];
        errorback(error);
    }];
}

-(void)updateFriendShareState:(NSArray*)changes :(GenericCallback)callback :(GenericErrorback)errorback {
    NSMutableSet *newlySharedFriendIDs = [NSMutableSet set];
    NSMutableSet *newlySharedFriendNames = [NSMutableSet set];
    NSMutableArray *changeDescriptors = [NSMutableArray array];
    
    BOOL allowingUpload = NO;
    
    for(NSDictionary *change in changes) {
        User *friend = change[@"subject"];
        
        NSMutableDictionary *changeDescriptor = [NSMutableDictionary dictionary];
        [changeDescriptors addObject:changeDescriptor];
        
        changeDescriptor[@"uid"] = friend.id;
        
        //we need to notify only those with whom sharing being done
        //for the first time and it's not an unshare
        NSNumber *share = change[@"share"];
        if(share) {
            changeDescriptor[@"share"] = share;
            if(share.boolValue && ![self isSharedWithUser:friend]) {
                [newlySharedFriendIDs addObject:friend.id];
                [newlySharedFriendNames addObject:friend.name];
            }
        }
        
        NSNumber *allowUpload = change[@"allow_upload"];
        if(allowUpload) {
            if(allowUpload.boolValue) {
                allowingUpload = YES;
            }
            
            changeDescriptor[@"allow_upload"] = allowUpload;
            if(allowUpload.boolValue && ![self isAllowedUpload:friend]) {
                [newlySharedFriendIDs addObject:friend.id];
                [newlySharedFriendNames addObject:friend.name];
            }
        }
    }
    
    NSString *albumId = [self getId];
    
    User *currentUser = [User getCurrentUser];
    NSMutableDictionary *context = [NSMutableDictionary dictionary];
    context[@"from_uid"] = currentUser.id;
    context[@"from_name"] = currentUser.name;
    context[@"album_id"] = albumId;
    context[@"album_name"] = self.name;
    context[@"ts"] = @([[NSDate date] timeIntervalSince1970]);
    
    NSString *message = [NSString stringWithFormat:@"%@ shared album '%@' with you on Snappe", currentUser.name, self.name];
    
    GenericCallback afterFBRequestInit = ^(NSString *requestID) {
        [[SnappeHTTPService getInstance] updateFriendShareState:albumId :changeDescriptors :requestID :callback :errorback];
    };
    
    VoidCallback afterUpload = ^() {
        if(allowingUpload) {
            [[FBManager getInstance] sendUserToUserRequest:newlySharedFriendIDs.allObjects :message :context: ^(NSString *requestID)
             {
                 afterFBRequestInit(requestID);
             }];
        }
        else {
            afterFBRequestInit(nil);
        }
    
    };
    
    if([self.class changesWarrantUploadCheck:changes]) {
        [self upload:^(id data) {
            afterUpload();
        } :errorback];
    }
    else {
        afterUpload();
    }

    
}

+(BOOL)changesWarrantUploadCheck:(NSArray*)changes {
    for(NSDictionary *change in changes) {
        if(change[@"share"]) {//allow_upload changes don't warrant upload
            NSNumber *share = change[@"share"];
            if(share.boolValue) {
                return YES;
            }
        }
    }
    return NO;
}

-(void) shareWithContacts:(NSArray*)changes:(GenericCallback)callback:(GenericErrorback)errorback {
    NSString *albumId = [self getId];
    
    NSMutableArray *changeDescriptors = [NSMutableArray array];
    
    for(NSDictionary *change in changes) {
        Contact *contact = change[@"subject"];
        
        NSMutableDictionary *changeDescriptor = [NSMutableDictionary dictionary];
        [changeDescriptors addObject:changeDescriptor];
        
        changeDescriptor[@"email"] = [contact getBestEmail];
        changeDescriptor[@"name"] = contact.name ? contact.name : [contact getBestEmail];
        for(NSString *prop in @[@"share", @"allow_upload"]) {
            if(change[prop]) {
                changeDescriptor[prop] = change[prop];
            }
        }
    }
    
    VoidCallback afterUpload = ^() {
        [[SnappeHTTPService getInstance] shareAlbumWithContacts:albumId :changeDescriptors:callback:errorback];
    };
    
    if([self.class changesWarrantUploadCheck:changes]) {
        [self upload:^(id data) {
            afterUpload();
        } :errorback];
    }
    else {
        afterUpload();
    }

}

-(void)pullInto:(Album *)otherAlbum :(GenericCallback)callback :(GenericErrorback)errorback {
    [[SnappeHTTPService getInstance] pullAlbum:self.id :otherAlbum.id :callback :errorback];
}

-(void)pushTo:(Album *)otherAlbum :(GenericCallback)callback :(GenericErrorback)errorback {
    [self upload:^(NSNumber *photosUploaded) {
        [[SnappeHTTPService getInstance] pushAlbum:self.id :otherAlbum.id :^(id data){
            callback(photosUploaded);
        } :errorback];
    } :errorback];
}

-(void)syncWith:(Album *)otherAlbum :(GenericCallback)callback :(GenericErrorback)errorback {
    [self upload:^(id data) {
        [[SnappeHTTPService getInstance] syncAlbums:self.id :otherAlbum.id :callback :errorback];
    } :errorback];
}

-(BOOL)hasPhotoWithHash:(NSString*)hash {
    for(Photo *albumPhoto in self.photos) {
        if(albumPhoto.digest && [albumPhoto.digest isEqualToString:hash]) {
            return YES;
        }
    }
    return NO;
}

-(BOOL) hasSimilarPhoto:(NSString*)pHash {
    for(Photo *albumPhoto in self.photos) {
        NSString *albumPhotoPHash = albumPhoto.pHash;
        BOOL areSimilar = [ImageHasher areSimilarImages:pHash :albumPhotoPHash];
        if(areSimilar) {
            DLog(@"photos with digest: %@ found similar to photo with pHash %@", albumPhoto.digest, pHash);
            return YES;
        }
    }
    return NO;
}

-(UIImageView*)getCoverPhoto :(NSUInteger)width :(NSUInteger)height :(BOOL)isThumb {
    if([self.photos count] == 0) {
        return nil;
    }
    
    Photo *coverPhoto = [self.photos objectAtIndex:0];
    return [coverPhoto getUIImageView:width :height :isThumb :NO];
}

-(NSArray*)getLocations {
    NSMutableArray *rv = [NSMutableArray array];
    for(Photo *photo in self.photos) {
        SpatioTemporalLocation *loc = [[SpatioTemporalLocation alloc] initWithPhoto:photo];
        if(loc.location) {
            [rv addObject:loc];
        }
    }
    return rv;
}

-(NSString*)getLocationName {
    if(self.location) {
        return self.location.name;
    }
    
    NSMutableDictionary *locationFrequencyMap = [NSMutableDictionary dictionary];
    for(Photo *photo in self.photos) {
        Location *photoLocation = [photo getSelfOrAlbumLocation];
        if(photoLocation) {
            NSString *locationName = photoLocation.name;
            if(locationName) {
                NSNumber *exisitingCountNum = [locationFrequencyMap objectForKey:locationName];
                
                int count = 1;
                if(exisitingCountNum) {
                    count =  [exisitingCountNum intValue] + 1;
                }
                [locationFrequencyMap setObject:[NSNumber numberWithInt:count] forKey:locationName];
            }
        }
    }
    
    NSArray *sortedLocations = [locationFrequencyMap keysSortedByValueUsingComparator:^NSComparisonResult(NSNumber *count1, NSNumber *count2) 
    {
        return [count2 compare:count1];
    }];
    
    if([sortedLocations count] == 0) {
        return @"Unknown";
    }
    
    return [sortedLocations objectAtIndex:0];
}

-(NSString*)getDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd YYYY"];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    
    if(self.createdOn) {
        return [dateFormatter stringFromDate:self.createdOn];
    }
    
    if(self.modifiedOn) {
        return [dateFormatter stringFromDate:self.modifiedOn];
    }
    
    NSDate *maxDate, *minDate;
    for(Photo *photo in self.photos) {
        if(photo.clickedOn) {
            if(!maxDate || [photo.clickedOn compare:maxDate] == NSOrderedDescending) {
                maxDate = photo.clickedOn;
            }
            if(!minDate || [photo.clickedOn compare:minDate] == NSOrderedAscending) {
                minDate = photo.clickedOn;
            }
        }
    }
    
    if(!maxDate && !minDate) {
        return @"Unknown";
    }
    
    NSString *format = @"MMM dd YYYY";
    NSTimeInterval window = [maxDate timeIntervalSinceDate:minDate];
    if(window > 86400 && window < 365 * 86400) {
        format = @"MMM YYYY";
    }
    else {
        format = @"YYYY";
    }
    
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:minDate];
}

-(NSSet*)getHashes {
    NSMutableSet *existingHashes = [[NSMutableSet alloc] init];
    for(Photo *photo in self.photos) {
        if(!photo.digest) {
            continue;
        }
        [existingHashes addObject:photo.digest];
    }
    return existingHashes;
}

-(NSSet*)getPHashes {
    NSMutableSet *existingPHashes = [[NSMutableSet alloc] init];
    for(Photo *photo in self.photos) {
        if(!photo.pHash) {
            continue;
        }
        [existingPHashes addObject:photo.pHash];
    }
    return existingPHashes;
}

-(NSInteger)getSamePhotoCount:(Album*)otherAlbum {
    NSSet *selfHashes = [self getHashes];
    NSSet *otherHashes = [otherAlbum getHashes];
    
    if([selfHashes count] == 0 || [otherHashes count] == 0) {
        return -1;//uncomparable
    }
    
    NSUInteger sameCount = 0;
    for(NSString *selfHash in selfHashes) {
        for(NSString *otherHash in otherHashes) {
            if([selfHash isEqualToString:otherHash]) {
                sameCount++;
            }
        }
    }
    return sameCount;
}

-(NSInteger)getSimilarPhotoCount:(Album*)otherAlbum {
    NSSet *selfPHashes = [self getPHashes];
    NSSet *otherPHashes = [otherAlbum getPHashes];
    
    if([selfPHashes count] == 0 || [otherPHashes count] == 0) {
        return -1;//uncomparable
    }
    
    NSUInteger similarCount = 0;
    for(NSString *selfPHash in selfPHashes) {
        for(NSString *otherPHash in otherPHashes) {
            if([ImageHasher areSimilarImages:selfPHash :otherPHash]) {
                similarCount++;
            }
        }
    }
    return similarCount;
}

+(void)getSimilarAlbums:(Album*)album:(BOOL)currentDeviceOnly:(BOOL)facebookOnly:(BOOL)currentUserOwnedOnly:(NSUInteger)limit:(ArrayCallback)callback:(GenericErrorback)errorback
{
    NSArray *allAlbums = [[GraphManager getInstance] getAlbums];
    [[SortManager getInstance] sort:allAlbums :album :SMART :YES:^(NSArray *closestFirstAlbums)
    {
        closestFirstAlbums = [closestFirstAlbums filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(Album *a, NSDictionary *bindings) {
            if([a.id isEqualToString:album.id]) {
                return NO;
            }
            if(currentDeviceOnly && ![a isCurrentDeviceAlbum]) {
                return NO;
            }
            if(facebookOnly && ![a isFacebookAlbum]) {
                return NO;
            }
            if(currentUserOwnedOnly && ![a isOwnedByCurrentUser]) {
                return NO;
            }
            return YES;
        }]];
        
        NSRange range = NSMakeRange(0, MIN([closestFirstAlbums count], limit));
        callback([closestFirstAlbums subarrayWithRange:range]);
        
    }:errorback];
}

-(void)getOrFetchHashes:(VoidCallback)callback:(GenericErrorback)errorback {
    [PhotoBatchHashUpdater update:[self.photos array] :callback :errorback];
}

-(void)uploadPhotosToFacebook:(NSObject*)target:(GenericCallback)callback:(GenericErrorback)errorback {
    [PhotoBatchFacebookUploader upload:[self getPhotos:YES :YES :YES] :target :callback :errorback];
}


-(BOOL)isAutoCreatedAlbum {
    return self.devicePersistentID == nil;
}

+(void)populateNewDeviceAlbum:(ALAssetsGroup*)group:(GenericCallback)callback:(GenericErrorback)errorback {
    Album *album = [Album fromAssetGroup:[ALAssetsGroupInfo fromAssetsGroup:group]];
    
    NSArray *deviceAlbumsWithName = [self getDeviceAlbumsWithName:album.name];
    if(!deviceAlbumsWithName) {
        errorback(nil);
        return;
    }
    
    NSMutableArray *photosURLsToAddToAlbum = [NSMutableArray array];
    for(Album *deviceAlbumWithName in deviceAlbumsWithName) {
        if(deviceAlbumWithName.devicePersistentID) {
            continue;
        }
        for(Photo *photo in deviceAlbumWithName.photos) {
            NSString *hash = photo.digest;
            NSString *assetURL = [[PhotoLibraryReader getInstance] getAssetURLForHash:hash];
            if(assetURL) {
                [photosURLsToAddToAlbum addObject:assetURL];
            }
        }
    }
    
    [PhotoSaveManager addLocalPhotosToAssetGroup:group :photosURLsToAddToAlbum :callback :errorback];
}

+(void)getSimilarUsers:(Album*)album:(NSUInteger)limit:(ArrayCallback)callback:(GenericErrorback)errorback {
    NSArray *allFriends = [[User getCurrentUser] getFriends];
    
    [[SortManager getInstance] sort:allFriends :album :SMART :NO:^(NSArray *sorted){
        NSRange range = NSMakeRange(0, MIN(limit, [allFriends count]));
        callback([sorted subarrayWithRange:range]);
    }:errorback];
}

-(BOOL)currentUserCanUpload {
    return [self isOwnedByCurrentUser] || [self isAllowedUpload:[User getCurrentUser]];
}

-(BOOL)anyPhotoNeedsUploading {
    for(Photo *photo in self.photos) {
        if(photo.markedPrivate.boolValue) {
            continue;
        }
        if(!photo.src && !photo.fullSrc) {
            return YES;
        }
    }
    return NO;
}

+(Album*)getOwnLatestDeviceAlbumWithPhotos {
    return [self getLatestAlbumWithPhotos:YES:NO:YES];
}

+(Album*)getOwnLatestFacebookAlbumWithPhotos {
    return [self getLatestAlbumWithPhotos:NO:YES:YES];
}

+(Album*)getLatestAlbumWithPhotos:(BOOL)device:(BOOL)facebook:(BOOL)userOwned {
    User *currentUser = [User getCurrentUser];
    
    NSArray *albums = @[];
    if(device && facebook) {
        albums = [[GraphManager getInstance] getAlbums];
    }
    else if(device) {
        albums = [[GraphManager getInstance] getDeviceAlbums];
    }
    else if(facebook) {
        albums = [[GraphManager getInstance] getFacebookAlbums];
    }
    
    albums = [albums filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(Album *album, NSDictionary *bindings)
    {
        return album.owner && (!userOwned || [album.owner.id isEqualToString:currentUser.id]) && album.photos.count > 0;
    }]];
    
    if(albums.count == 0) {
        return nil;
    }
              
    albums = [albums sortedArrayUsingComparator:^NSComparisonResult(Album *a1, Album *a2) {
        if(a1.modifiedOn && a2.modifiedOn) {
            return [a2.modifiedOn compare:a1.modifiedOn];
        }
        return a2.photos.count - a1.photos.count;
    }];
    
    return albums[0];
}

@end
