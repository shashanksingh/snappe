//
//  TagAccessor.m
//  snappe
//
//  Created by Shashank on 21/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TagAccessor.h"
#import "GraphManager.h"
#import "Util.h"

@implementation Tag (accessor)

+(Tag*) getById:(NSString *)id {
    return [GraphManager getGraphObjectById:id WithEntityName:@"Tag"];
}

+(NSArray*) createOrUpdateWithChangeSets:(NSArray*)changeSets InContext:(NSManagedObjectContext *)context {
    NSDictionary *existingObjects = [GraphManager getGraphObjectsById:[changeSets valueForKey:@"id"] WithEntityName:@"Tag"];
    
    NSMutableArray *rv = [NSMutableArray array];
    
    for(NSDictionary *changeSet in changeSets) {
        NSString *id = [changeSet objectForKey:@"id"];
        Tag *tag = [existingObjects objectForKey:id];
        BOOL preExisting = tag != nil;
        if(!tag) {
            tag = [NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext: context];
            [tag setId:id];
        }
        [self updateWithChangeSet:tag :changeSet];
        [rv addObject:@[tag, @(preExisting)]];
    }
    
    return rv;
}


+(Tag*) createOrUpdateWithChangeSet:(NSDictionary *)changeSet InContext:(NSManagedObjectContext *)context {
    
    NSString *id = [changeSet objectForKey:@"id"];
    Tag *tag = [Tag getById:id];
    if(tag == nil) {
        tag = [NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext: context];
        [tag setId:[NSString stringWithFormat:@"%@", id]];
    }
    
    [self updateWithChangeSet:tag :changeSet];
    return tag;
}

+(void)updateWithChangeSet:(Tag*)tag:(NSDictionary*)changeSet {
    for(NSString *propertyName in changeSet) {
        
        if([@"timestamp" isEqualToString:propertyName]) {
            [tag setTimestamp:[Util unixTimestampToDate:[changeSet objectForKey:propertyName]]];
        }
        else {
            //DLog(@"warning: unused property: %@, value: %@", propertyName, [changeSet objectForKey:propertyName]);
        }
    }
}

-(NSString*) getId {
    return self.id;
}

@end
