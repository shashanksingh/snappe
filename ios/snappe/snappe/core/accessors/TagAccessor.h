//
//  TagAccessor.h
//  snappe
//
//  Created by Shashank on 21/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tag.h"

@interface Tag (accessor)
+(Tag*) getById:(NSString *)id;
+(Tag*) createOrUpdateWithChangeSet:(NSDictionary *)changeSet InContext:(NSManagedObjectContext *)context;
+(NSArray*) createOrUpdateWithChangeSets:(NSArray*)changeSets InContext:(NSManagedObjectContext *)context;
-(NSString*) getId;
@end
