//
//  PhotoAccessor.h
//  snappe
//
//  Created by Shashank on 09/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Photo.h"
#import "Macros.h"
#import "ALAssetInfo.h"

@interface Photo (accessor)

typedef enum ImageSizeType {
    THUMB, FULL_SCREEN, FULL_RESOLUTION
}ImageSizeType;

+(Photo*) getById:(NSString *)id;

+(NSArray*) getPhotosWithHash:(NSString*)hash;

+(Photo *) fromImageResource:(NSString *)imageName;
+(Photo*) fromALAsset:(ALAssetInfo*)assetInfo;
-(NSDictionary*) getALAssetMetadata;

+(Photo*) createOrUpdateWithChangeSet:(NSDictionary *)changeSet InContext:(NSManagedObjectContext *)context;
+(NSArray*) createOrUpdateWithChangeSets:(NSArray*)changeSets InContext:(NSManagedObjectContext *)context;

-(NSString*) getId;
-(NSString*) getHash;
-(NSDate*)   getClickedOn;
-(NSNumber*) getWidth;
-(NSNumber*) getHeight;
-(NSNumber*) getThumbWidth;
-(NSNumber*) getThumbHeight;
-(NSString*) getPHash;
-(NSString*) getThumbnailURL;
-(NSString*) getURL;
-(Location*) getSelfOrAlbumLocation;
-(void) setHash:(NSString*)hash;
-(BOOL) toogleIsPrivate;
-(void) unsetLocation;


-(UIImageView *) getUIImageView:(NSUInteger)width :(NSUInteger)height :(BOOL)thumb :(BOOL)darkBackground;
-(void) getUIImage:(ImageSizeType)type:(GenericCallback)callback:(GenericErrorback)errorback;

-(void)getOrFetchHashes:(VoidCallback)callback:(GenericErrorback)errorback;

-(BOOL)isLocalPhoto;
-(BOOL) isImageDataAvailable;

+(NSArray*)getSortedPhotos:(int)offset:(int)limit;

@end
