//
//  ExtendedOperationAccessor.h
//  snappe
//
//  Created by Shashank on 02/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum ExtOpStatus {
    NOT_STARTED, STARTED, ONGOING, SUCCESSFUL, FAILURE
}ExtOpStatus;


@interface ExtendedOperationAccessor : NSObject
@end
