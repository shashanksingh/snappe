//
//  LocationAccessor.h
//  snappe
//
//  Created by Shashank on 09/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Location.h"

#import <CoreLocation/CoreLocation.h>

@interface Location (accessor)
+(Location*) fromCLLocation:(CLLocation*)cllocation;
-(CLLocation*) toCLLocation;
+(Location*) getById:(NSString *)id;
+(Location*) createOrUpdateWithChangeSet:(NSDictionary *)changeSet InContext:(NSManagedObjectContext *)context;
+(NSArray*) createOrUpdateWithChangeSets:(NSArray*)changeSets InContext:(NSManagedObjectContext *)context;
-(NSString*) getId;
-(double)distance:(Location*)other;
@end
