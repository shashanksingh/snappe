//
//  PhotoAccessor.m
//  snappe
//
//  Created by Shashank on 09/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoAccessor.h"
#import "AppDelegate.h"
#import "GraphManager.h"
#import "Util.h"
#import "LocationAccessor.h"
#import "SnappeHTTPService.h"
#import "PhotoLibraryReader.h"
#import "UIImageView+AFNetworking.h"
#import "ImageHasher.h"
#import <ImageIO/CGImageProperties.h>
#import <ImageIO/CGImageSource.h>
#import "Logger.h"
#import "MissingPhotoReporter.h"
#import "SyncManager.h"

#define REMOTE_PHOTO_LOAD_DELAY 0.0

@implementation Photo (accessor)

+(NSArray*) getPhotosWithHash:(NSString*)hash {
    return [GraphManager getGraphObjectsByProperties:[NSDictionary dictionaryWithObject:hash forKey:@"digest"] :@"Photo"];
}

+(Photo*) fromALAsset:(ALAssetInfo*)assetInfo {
    
    NSManagedObjectContext *context = [[GraphManager getInstance] getTransientManagedObjectContext];
    Photo * photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext: context];
    
    [photo setHash:assetInfo.hash];
    [photo setClickedOn:assetInfo.date];
    
    Location *location = [Location fromCLLocation:assetInfo.location];
    [photo setLocation:location];
    
    CGSize thumbSize = [assetInfo.thumbSize CGSizeValue];
    CGSize size = [assetInfo.size CGSizeValue];
    
    [photo setThumbWidth:[NSNumber numberWithInt:thumbSize.width]];
    [photo setThumbHeight:[NSNumber numberWithInt:thumbSize.height]];
    
    [photo setWidth:[NSNumber numberWithInt:size.width]];
    [photo setHeight:[NSNumber numberWithInt:size.height]];
    
    [photo setPHash:assetInfo.pHash];
    return photo;
}

+(Photo *) fromImageResource:(NSString *)imageName {
    NSManagedObjectContext *context = [[GraphManager getInstance] getTransientManagedObjectContext];
    Photo *photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext: context];
    
    NSString *fileName = [imageName stringByDeletingPathExtension];
    NSString *fileExtension = [imageName pathExtension];
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:fileName ofType:fileExtension]];
    [photo setSrc:[url path]];
    [photo setThumbSrc:[url path]];
    
    UIImage *image = [UIImage imageNamed:imageName];
    NSNumber *width = [[NSNumber alloc] initWithFloat:[image size].width];
    NSNumber *height = [[NSNumber alloc] initWithFloat:[image size].height];
    
    [photo setWidth:width];
    [photo setHeight:height];
    [photo setThumbWidth:width];
    [photo setThumbHeight:height];
    
    return photo;
}

+(Photo*) getById:(NSString *)id {
    return [GraphManager getGraphObjectById:id WithEntityName:@"Photo"];
}

-(NSDictionary*) getALAssetMetadata {
    
    NSMutableDictionary *rv = [NSMutableDictionary dictionary];
    
    NSDictionary *gpsDict = [self getGPSMetadata];
    [rv setValue:gpsDict forKey:(NSString*)kCGImagePropertyGPSDictionary];
    
    NSDictionary *exifDict = [self getExifMetadata];
    [rv setValue:exifDict forKey:(NSString*)kCGImagePropertyExifDictionary];
    
    return rv;
}

+(NSArray*) createOrUpdateWithChangeSets:(NSArray*)changeSets InContext:(NSManagedObjectContext *)context {
    NSDictionary *existingObjects = [GraphManager getGraphObjectsById:[changeSets valueForKey:@"id"] WithEntityName:@"Photo"];
    
    NSMutableArray *rv = [NSMutableArray array];
    
    for(NSDictionary *changeSet in changeSets) {
        NSString *id = [changeSet objectForKey:@"id"];
        Photo *photo = [existingObjects objectForKey:id];
        BOOL preExisting = photo != nil;
        if(!photo) {
            photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext: context];
            [photo setId:id];
        }
        [self updateWithChangeSet:photo :changeSet];
        [rv addObject:@[photo, @(preExisting)]];
    }
    
    return rv;
}


+(Photo*) createOrUpdateWithChangeSet:(NSDictionary *)changeSet InContext:(NSManagedObjectContext *)context {
    
    NSString *id = [changeSet objectForKey:@"id"];
    Photo *photo = [Photo getById:id];
    if(photo == nil) {
        photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext: context];
        [photo setId:id];
    }
    
    [self updateWithChangeSet:photo :changeSet];
    
    return photo;
    
}

+(void)updateWithChangeSet:(Photo*)photo:(NSDictionary*)changeSet {
    
    for(NSString *propertyName in changeSet) {
        
        if([@"caption" isEqualToString:propertyName]) {
            [photo setCaption:[changeSet objectForKey:propertyName]];
        }
        else if([@"created" isEqualToString:propertyName]) {
            [photo setClickedOn:[Util unixTimestampToDate:[changeSet objectForKey:propertyName]]];
        }
        else if([@"src" isEqualToString:propertyName]) {
            [photo setSrc:[changeSet objectForKey:propertyName]];
        }
        else if([@"width" isEqualToString:propertyName]) {
            [photo setWidth:[changeSet objectForKey:propertyName]];
        }
        else if([@"height" isEqualToString:propertyName]) {
            [photo setHeight:[changeSet objectForKey:propertyName]];
        }
        else if([@"src_small" isEqualToString:propertyName]) {
            [photo setThumbSrc:[changeSet objectForKey:propertyName]];
        }
        else if([@"src_small_width" isEqualToString:propertyName]) {
            [photo setThumbWidth:[changeSet objectForKey:propertyName]];
        }
        else if([@"src_small_height" isEqualToString:propertyName]) {
            [photo setThumbHeight:[changeSet objectForKey:propertyName]];
        }
        else if([@"src_big" isEqualToString:propertyName]) {
            [photo setFullSrc:[changeSet objectForKey:propertyName]];
        }
        else if([@"src_big_width" isEqualToString:propertyName]) {
            [photo setFullWidth:[changeSet objectForKey:propertyName]];
        }
        else if([@"src_big_height" isEqualToString:propertyName]) {
            [photo setFullHeight:[changeSet objectForKey:propertyName]];
        }
        else if([@"hash" isEqualToString:propertyName]) {
            [photo setHash:[changeSet objectForKey:propertyName]];
        }
        else if([@"phash" isEqualToString:propertyName]) {
            [photo setPHash:[changeSet objectForKey:propertyName]];
        }
        else if([@"snappe_private" isEqualToString:propertyName]) {
            [photo setMarkedPrivate:[changeSet objectForKey:propertyName]];
        }
        else {
            //DLog(@"warning: unused property: %@, value: %@", propertyName, [changeSet objectForKey:propertyName]);
        }
    }

}

-(void)setAlbum:(Album *)album {
    BOOL alreadyInAlbum = album && self.album && [self.album.id isEqualToString:album.id];
    
    [self willChangeValueForKey:@"album"];
    [self setPrimitiveValue:album forKey:@"album"];
    [self didChangeValueForKey:@"album"];
    
    if(!alreadyInAlbum && [SyncManager getLastSyncTime] != 0 && self.managedObjectContext !=[[GraphManager getInstance]  getTransientManagedObjectContext])
    {
        [Util incrementNewPhotoCount:album];
    }
}

-(void) setHash:(NSString*)hash {
    [self setDigest:hash];
}

-(NSString*) getId {
    return self.id;
}

-(NSString*) getHash {
    return self.digest;
}

-(NSDate*) getClickedOn {
    return self.clickedOn;
}

-(NSNumber*) getWidth {
    return self.width;
}

-(NSNumber*) getHeight {
    return self.height;
}

-(NSNumber*) getThumbWidth {
    return self.thumbWidth;
}

-(NSNumber*) getThumbHeight {
    return self.thumbHeight;
}

-(NSString*) getPHash {
    return self.pHash;
}

-(NSString*) getThumbnailURL {
    return self.thumbSrc;
}

-(NSString*) getURL {
    return self.fullSrc ? self.fullSrc : self.src;
}

-(void) unsetLocation {
    self.location = nil;
}

-(Location*) getSelfOrAlbumLocation {
    if(self.location) {
        return self.location;
    }
    return self.album.location;
}

-(BOOL) toogleIsPrivate {
    if([self.markedPrivate boolValue]) {
        self.markedPrivate = [NSNumber numberWithBool:NO];
    }
    else {
        self.markedPrivate = [NSNumber numberWithBool:YES];
    }
    return [self.markedPrivate boolValue];
}

-(NSString*)getStandardSizeURL {
    if(!self.fullSrc) {
        return self.src;
    }
    
    if(IS_IPAD) {
        return self.fullSrc;
    }
    
    //for phone snappe standrd size is sufficient, facebook standard
    //size is not
    if([self.src rangeOfString:@"snap.pe"].location != NSNotFound) {
        return self.src;
    }
    return self.fullSrc;
}

-(UIImageView *) getUIImageView:(NSUInteger)width :(NSUInteger)height :(BOOL)thumb :(BOOL)darkBackground {
    UIImageView *imageView;
    
    NSString *hash = [self getHash];
    if(hash && [[PhotoLibraryReader getInstance] assetExistsWithHash:hash]) {
        imageView = [self getUIImageViewForHash:self.digest :thumb :width :height :darkBackground];
    }
    else {
        NSString *url = thumb ? self.thumbSrc : [self getStandardSizeURL];
        if(url != nil) {
            NSURL *urlObj;
            if([url hasPrefix:@"/"]) {
                urlObj = [NSURL fileURLWithPath:url];
            }
            else {
                urlObj = [NSURL URLWithString:url];
            }
            imageView = [self getUIImageViewForURL:urlObj :thumb :width :height :darkBackground];
        }
        else {
            imageView = [self getUIImageViewForURL:nil :thumb :width :height :darkBackground];
        }
    }
    
    return imageView;
}

-(UIImageView *) getUIImageViewForURL:(NSURL *)url :(BOOL)thumb :(NSUInteger)width :(NSUInteger)height :(BOOL)darkBackground
{
    
    UIImageView *uiimageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    if(url == nil) {
        uiimageView.contentMode = UIViewContentModeCenter;
        [uiimageView setImage:[UIImage imageNamed:@"private_photo_icon.png"]];
        return uiimageView;
    }
    
    UIView *placeHolderSkin = [Util setViewSkin:uiimageView :PHOTO_PLACEHOLDER];
    
    __weak UIImageView *uiimageViewWeakRef = uiimageView;
    __block typeof(self) bself = self;
    
    VoidCallback cb = ^{
        [uiimageView
         setImageWithURLRequest:[NSURLRequest requestWithURL:url]
         placeholderImage:nil
         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
             if(thumb) {
                 image = [Util resizeUIImage:image :width :height];
             }
             [uiimageViewWeakRef setImage:image];
             [Util clearViewSkin:placeHolderSkin :PHOTO_PLACEHOLDER];
         }
         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
             if(response.statusCode == 404) {
                 [[MissingPhotoReporter getInstance] reportMissingPhoto:bself];
             }
             
             uiimageViewWeakRef.contentMode = UIViewContentModeCenter;
             [uiimageViewWeakRef setImage:[UIImage imageNamed:@"broken_image.png"]];
             [Util clearViewSkin:placeHolderSkin :PHOTO_PLACEHOLDER];
             
             [Logger logError:[NSString stringWithFormat:@"error getting image at url: %@, response: %@", url, response] :error];
         }
         ];
    };
    
    if(thumb) {
        cb();
    }
    else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, REMOTE_PHOTO_LOAD_DELAY * NSEC_PER_SEC),dispatch_get_current_queue(), cb);
    }
    
    
    return uiimageView;
}

-(UIImageView *) getUIImageViewForHash:(NSString *)hash :(BOOL)thumb :(NSUInteger)width :(NSUInteger)height :(BOOL)darkBackground
{
    UIImageView *uiimageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    UIImageView *uiimageViewWeakRef = uiimageView;
    
    UIView *placeHolderSkin = [Util setViewSkin:uiimageView :PHOTO_PLACEHOLDER];
    
    void(^onNoImage)() = ^(){
        [Util clearViewSkin:placeHolderSkin :PHOTO_PLACEHOLDER];
        uiimageViewWeakRef.contentMode = UIViewContentModeCenter;
        [uiimageViewWeakRef setImage:[UIImage imageNamed:@"broken_image.png"]];
    };
    
    [self getUIImage:(thumb ? THUMB : FULL_SCREEN) :^(UIImage *image) {
        if(!image) {
            onNoImage();
        }
        else {
            if(thumb) {
                image = [Util resizeUIImage:image :width :height];
            }
            [uiimageViewWeakRef setImage:image];
            [Util clearViewSkin:placeHolderSkin :PHOTO_PLACEHOLDER];
        }
    } :^(NSError *error) {
        [Logger logError:[NSString stringWithFormat:@"error getting asset by hash %@", hash] :error];
        onNoImage();
    }];
    
    return uiimageView;
}

-(void) getUIImage:(ImageSizeType)type:(GenericCallback)callback:(GenericErrorback)errorback {
    NSString *digest = [self getHash];
    if(!digest || ![[PhotoLibraryReader getInstance] assetExistsWithHash:digest]) {
        
        NSString *url = self.fullSrc;
        if(!url) {
            url = self.src;
        }
        
        UIImage *rv = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
        callback(rv);
    }
    else {
        [[PhotoLibraryReader getInstance] getAssetByHash:[self getHash] callback:^(ALAsset *asset) {
            if(!asset) {
                callback(nil);
            }
            else {
                CGImageRef iref;
                if(type == THUMB) {
                    iref = [asset thumbnail];
                }
                else if(type == FULL_SCREEN) {
                    iref = [[asset defaultRepresentation] fullScreenImage];
                }
                else {
                    iref = [[asset defaultRepresentation] fullResolutionImage];
                }
                UIImage *image = [UIImage imageWithCGImage:iref];
                callback(image);
            }
            
        } errorBack:errorback];
    }
}

//copied from http://stackoverflow.com/questions/3884060/saving-geotag-info-with-photo-on-ios4-1
- (NSDictionary *)getGPSMetadata {
    NSMutableDictionary *gps = [NSMutableDictionary dictionary];
    
    [gps setObject:@"2.2.0.0" forKey:(NSString *)kCGImagePropertyGPSVersion];
    
    if(self.clickedOn) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm:ss.SSSSSS"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        [gps setObject:[formatter stringFromDate:self.clickedOn] forKey:(NSString *)kCGImagePropertyGPSTimeStamp];
        [formatter setDateFormat:@"yyyy:MM:dd"];
        [gps setObject:[formatter stringFromDate:self.clickedOn] forKey:(NSString *)kCGImagePropertyGPSDateStamp];    
    }
    
    Location *loc = [self getSelfOrAlbumLocation];
    if(loc) {
        CGFloat latitude = [loc.latitude floatValue];
        if (latitude < 0) {
            latitude = -latitude;
            [gps setObject:@"S" forKey:(NSString *)kCGImagePropertyGPSLatitudeRef];
        } else {
            [gps setObject:@"N" forKey:(NSString *)kCGImagePropertyGPSLatitudeRef];
        }
        [gps setObject:[NSNumber numberWithFloat:latitude] forKey:(NSString *)kCGImagePropertyGPSLatitude];
        
        CGFloat longitude = [loc.longitude floatValue];
        if (longitude < 0) {
            longitude = -longitude;
            [gps setObject:@"W" forKey:(NSString *)kCGImagePropertyGPSLongitudeRef];
        } else {
            [gps setObject:@"E" forKey:(NSString *)kCGImagePropertyGPSLongitudeRef];
        }
        [gps setObject:[NSNumber numberWithFloat:longitude] forKey:(NSString *)kCGImagePropertyGPSLongitude];    
    }
    
    return gps;
}

//extracted from http://caffeinatedcocoa.com/blog/?p=7
- (NSDictionary *)getExifMetadata {
    NSMutableDictionary *exif = [NSMutableDictionary dictionary];
    
    if(self.clickedOn) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
        [dateFormatter setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
        NSString *date = [dateFormatter stringFromDate:self.clickedOn];
        
        [exif setObject:date forKey:(NSString *)kCGImagePropertyExifDateTimeDigitized];
        [exif setObject:date forKey:(NSString *)kCGImagePropertyExifDateTimeOriginal];    
    }
    
    return exif;
}

-(void)getOrFetchHashes:(VoidCallback)callback:(GenericErrorback)errorback {
    if(self.digest && self.pHash) {
        callback();
        return;
    }
    
    //hash is really here going to protect us from re-uploading the same
    //photo, since FB changes photos quite a bit it won't be able to save
    //us the first time, pHash will
    if(!self.thumbSrc) {
        callback();
        return;
    }
    
    [[SnappeHTTPService getInstance] downloadURL:self.thumbSrc :^(NSData *data) {
        NSString *hash = [PhotoLibraryReader getHashForPhotoData:data];
        NSString *pHash = [ImageHasher getHashForData:data];
        
        self.digest = hash;
        self.pHash = pHash;
        
        callback();
        
    } :^(NSError *error){
        
        if(error.code == 404) {
            [[MissingPhotoReporter getInstance] reportMissingPhoto:self];
            callback();
        }
        else {
            errorback(error);    
        }
        
    }:^(long long done, long long toDo){
        //NOOP
    }];
}

-(BOOL)isLocalPhoto {
    return [self getHash] && [[PhotoLibraryReader getInstance] assetExistsWithHash:[self getHash]];
}

-(BOOL) isImageDataAvailable {
    if([self isLocalPhoto]) {
        return YES;
    }
    return self.src || self.thumbSrc;
}

+(NSArray*)getSortedPhotos:(int)offset:(int)limit {
    return [GraphManager getSorted:@"Photo" :@"clickedOn" :offset :limit];
}
@end
