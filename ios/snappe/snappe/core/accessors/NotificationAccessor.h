//
//  NotificationAccessor.h
//  snappe
//
//  Created by Shashank on 29/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Notification.h"

@interface Notification (accessor)
+(Notification*)create;
+(Notification*) getById:(NSString*)notificationId;
+(BOOL) checkedExistsLocally:(NSDictionary*)notificationJSON;
+(Notification*) createFromServerJSON:(NSDictionary*)notificationJSON;

-(UIImageView*)getThumbnail :(NSUInteger)width :(NSUInteger)height;
-(NSString*)getTitle;
-(NSString*)getDescription;
-(NSString*)getStatusText;
@end
