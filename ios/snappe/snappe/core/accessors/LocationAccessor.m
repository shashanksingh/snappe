//
//  LocationAccessor.m
//  snappe
//
//  Created by Shashank on 09/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LocationAccessor.h"
#import "GraphManager.h"

@implementation Location (accessor)

+(Location*) fromCLLocation:(CLLocation*)cllocation {
    NSManagedObjectContext *context = [[GraphManager getInstance] getTransientManagedObjectContext];
    Location * location = [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext: context];
    location.latitude = [NSNumber numberWithDouble: cllocation.coordinate.latitude];
    location.longitude = [NSNumber numberWithDouble:cllocation.coordinate.longitude];
    return location;
    
}

-(CLLocation*) toCLLocation {
    CLLocation *rv = [[CLLocation alloc] initWithLatitude:[self.latitude doubleValue] longitude:[self.longitude doubleValue]];
    return rv;
}

+(Location*) getById:(NSString *)id {
    return [GraphManager getGraphObjectById:id WithEntityName:@"Location"];
}

+(NSArray*) createOrUpdateWithChangeSets:(NSArray*)changeSets InContext:(NSManagedObjectContext *)context {
    NSDictionary *existingObjects = [GraphManager getGraphObjectsById:[changeSets valueForKey:@"id"] WithEntityName:@"Location"];
    
    NSMutableArray *rv = [NSMutableArray array];
    
    for(NSDictionary *changeSet in changeSets) {
        NSString *id = [changeSet objectForKey:@"id"];
        Location *location = [existingObjects objectForKey:id];
        BOOL preExisting = location != nil;
        if(!location) {
            location = [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext: context];
            [location setId:id];
        }
        [self updateWithChangeSet:location :changeSet];
        [rv addObject:@[location, @(preExisting)]];
    }
    
    return rv;
}


+(Location*) createOrUpdateWithChangeSet:(NSDictionary *)changeSet InContext:(NSManagedObjectContext *)context {
    
    NSString *id = [changeSet objectForKey:@"id"];
    Location *location = [Location getById:id];
    if(location == nil) {
        location = [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext: context];
        [location setId:[NSString stringWithFormat:@"%@", id]];
    }
    
    [self updateWithChangeSet:location :changeSet];
    
    return location;
    
}

+(void)updateWithChangeSet:(Location*)location:(NSDictionary*)changeSet {
    for(NSString *propertyName in changeSet) {
        
        if([@"latitude" isEqualToString:propertyName]) {
            [location setLatitude:[changeSet objectForKey:propertyName]];
        }
        else if([@"longitude" isEqualToString:propertyName]) {
            [location setLongitude:[changeSet objectForKey:propertyName]];
        }
        else if([@"name" isEqualToString:propertyName]) {
            [location setName:[changeSet objectForKey:propertyName]];
        }
        else if([@"city" isEqualToString:propertyName]) {
            [location setCity:[changeSet objectForKey:propertyName]];
        }
        else if([@"state" isEqualToString:propertyName]) {
            [location setState:[changeSet objectForKey:propertyName]];
        }
        else if([@"country" isEqualToString:propertyName]) {
            [location setCountry:[changeSet objectForKey:propertyName]];
        }
        else if([@"zip" isEqualToString:propertyName]) {
            [location setZip:[changeSet objectForKey:propertyName]];
        }
        else {
            //DLog(@"warning: unused property: %@, value: %@", propertyName, [changeSet objectForKey:propertyName]);
        }
    }
}

-(NSString*) getId {
    return self.id;
}

-(double)distance:(Location*)other {
    CLLocation *locSelf = [[CLLocation alloc] initWithLatitude:[self.latitude doubleValue] longitude:[self.longitude doubleValue]];
    CLLocation *locOther = [[CLLocation alloc] initWithLatitude:[other.latitude doubleValue] longitude:[other.longitude doubleValue]];
    
    
    return [locSelf distanceFromLocation:locOther];
}

@end
