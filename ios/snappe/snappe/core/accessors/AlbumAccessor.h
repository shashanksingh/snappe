//
//  PhotoEvent.h
//  snappe
//
//  Created by Shashank on 10/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Album.h"
#import "Photo.h"
#import "Macros.h"
#import "Contact.h"
#import "ALAssetsGroupInfo.h"

@interface Album (accessor)

+(Album*) getById:(NSString *)id;
+(Album*) getByDevicePersistentId:(NSString *)devicePersistentID;
+(Album*) createOrUpdateWithChangeSet:(NSDictionary *)changeSet InContext:(NSManagedObjectContext *)context;
+(NSArray*) createOrUpdateWithChangeSets:(NSArray*)changeSets InContext:(NSManagedObjectContext *)context;
+(Album*) getTransientInstance;
+(Album*) fromAssetGroup:(ALAssetsGroupInfo*)group;
+(Album*) fromAssetGroup:(ALAssetsGroupInfo*)group :(BOOL)checkExists;

-(NSString*) getName;
-(NSString*) getId;

-(void) setDescription:(NSString *)description;
-(NSString *) getDescription;

-(NSString*) getFBObjectID;
-(void) setFBObjectID:(NSString*)fbObjectID;

-(NSUInteger) getPhotoCount;
-(Photo*) getPhotoAtIndex:(NSUInteger) index;
-(UIImageView*)getCoverPhoto :(NSUInteger)width :(NSUInteger)height :(BOOL)isThumb;
-(NSArray*)getPhotos:(BOOL)sorted:(BOOL)available:(BOOL)privateOnly;

-(NSDate*) getModifiedOn;

-(NSString*) getDevicePersistentID;

-(ALAssetsGroupType) getDeviceAlbumType;
-(NSString*) getDeviceAlbumTypeName;

-(BOOL) isAlbumGroup;
-(BOOL) isEventGroup;
-(BOOL) isPhotoStreamGroup;
-(BOOL) isSavedPhotosGroup;

-(void) setIsSharedWith:(User*)user;
-(void) unsetIsSharedWith:(User*)user;
-(BOOL) isSharedWithUser:(User*)user;
-(BOOL) isShared;

-(void)updateFriendShareState:(NSArray*)changes :(GenericCallback)callback :(GenericErrorback)errorback;
-(void) shareWithContacts:(NSArray*)changes:(GenericCallback)callback:(GenericErrorback)errorback;

-(void) setIsAllowedUpload:(User*)user;
-(void) unsetIsAllowUpload:(User*)user;
-(BOOL) isAllowedUpload:(User*)user;
-(void) allowUpload:(User*)user:(GenericCallback)callback:(GenericErrorback)errorback;
-(void) disAllowUpload:(User*)user:(GenericCallback)callback:(GenericErrorback)errorback;

-(void) pullInto:(Album*)otherAlbum :(GenericCallback)callback :(GenericErrorback)errorback;
-(void) pushTo:(Album*)otherAlbum :(GenericCallback)callback :(GenericErrorback)errorback;
-(void) syncWith:(Album*)otherAlbum :(GenericCallback)callback :(GenericErrorback)errorback;

+(void)download:(NSArray*)photosToDownload:(Album*)destinationAlbum:(BOOL)ignoreSimilar :(VoidCallback)callback :(GenericErrorback)errorback;
-(void)download:(Album*)destinationAlbum:(BOOL)ignoreSimilar :(VoidCallback)callback :(GenericErrorback)errorback;
-(void)downloadIntoAlbumByName:(NSString*)destAlbumName:(BOOL)ignoreSimilar :(VoidCallback)callback :(GenericErrorback)errorback;

-(BOOL)hasPhotoWithHash:(NSString*)hash;
-(BOOL) hasSimilarPhoto:(NSString*)pHash;

-(BOOL) isOwnedByCurrentUser;
-(BOOL) isCurrentDeviceAlbum;
-(BOOL) isCurrentDevicePhysicalAlbum;
-(BOOL) isFacebookAlbum;
-(BOOL) isAutoCreatedAlbum;

-(NSArray*)getLocations;
-(NSString*)getLocationName;
-(NSString*)getDate;

-(NSSet*)getHashes;
-(NSSet*)getPHashes;
-(void)getOrFetchHashes:(VoidCallback)callback:(GenericErrorback)errorback;

-(NSInteger)getSamePhotoCount:(Album*)otherAlbum;
-(NSInteger)getSimilarPhotoCount:(Album*)otherAlbum;

+(void)getSimilarAlbums:(Album*)album:(BOOL)currentDeviceOnly:(BOOL)facebookOnly:(BOOL)currentUserOwnedOnly:(NSUInteger)limit:(ArrayCallback)callback:(GenericErrorback)errorback;
+(void)getSimilarUsers:(Album*)album:(NSUInteger)limit:(ArrayCallback)callback:(GenericErrorback)errorback;

-(void)uploadPhotosToFacebook:(NSObject*)target:(GenericCallback)callback:(GenericErrorback)errorback;

+(void)populateNewDeviceAlbum:(ALAssetsGroup*)group:(GenericCallback)callback:(GenericErrorback)errorback;

-(BOOL)currentUserCanUpload;

-(BOOL)anyPhotoNeedsUploading;

+(Album*)getOwnLatestDeviceAlbumWithPhotos;
+(Album*)getOwnLatestFacebookAlbumWithPhotos;
@end
