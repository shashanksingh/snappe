//
//  AdDelegate.h
//  snappe
//
//  Created by Shashank on 26/02/13.
//
//

#import <Foundation/Foundation.h>
#import "DFPBannerView.h"

@interface AdDelegate : NSObject<GADBannerViewDelegate>
+(AdDelegate*) getInstance;
@end
