//
//  PhotoHashUpdater.h
//  snappe
//
//  Created by Shashank on 13/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoAccessor.h"

@interface PhotoHashUpdater : NSObject
+(PhotoHashUpdater*) getInstance;
-(void)add:(NSString*)photoID:(NSString*)hash:(NSString*)pHash;
@end
