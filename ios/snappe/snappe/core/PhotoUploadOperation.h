//
//  PhotoUploadOperation.h
//  snappe
//
//  Created by Shashank on 28/02/13.
//
//

#import "PhotoTransferOperation.h"

@interface PhotoUploadOperation : PhotoTransferOperation
-(id)init:(NSArray*)privatePhotos:(NSArray*)photosToUpload;
@end
