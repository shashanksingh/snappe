//
//  PhotoUploader.m
//  snappe
//
//  Created by Shashank on 28/03/13.
//
//

#import "PhotoBatchProcessor.h"
#import "Util.h"
#import "EventManager.h"
#import "SnappeHTTPService.h"
#import "CreditsManager.h"
#import "ExtendedOperationAccessor.h"
#import "Logger.h"

#define MAX_CONCURRENT_PROCESSING 5

static const NSString *PROCESSING_SYNCHRONIZATION_KEY = @"snappe_photo_uploader_sync_key";

@interface PhotoProcessingRequest : NSObject
@property (retain) NSSet *photoIDs;
@property (retain) id context;
@property (copy) GenericCallback callback;
@property (copy) GenericErrorback errorback;
@property int doneCount;
@end

@implementation PhotoProcessingRequest

-(id)init:(NSArray*)photos:(id)context:(GenericCallback)callback:(GenericErrorback)errorback {
    self = [super init];
    if(self) {
        self.photoIDs = [NSSet setWithArray:[photos valueForKey:@"id"]];
        self.context = context;
        self.callback = callback;
        self.errorback = errorback;
        self.doneCount = 0;
    }
    return self;
}

-(BOOL)markPhotoDone:(NSString*)donePhotoID:(BOOL)success {
    if(![self.photoIDs containsObject:donePhotoID]) {
        return NO;
    }
    
    if(!success) {
        return YES;
    }
    
    self.doneCount++;
    return self.doneCount >= self.photoIDs.count;
}

@end

@interface PhotoBatchProcessor()
@property int currentProcessingCount;
@property NSMutableArray *dataQueue;
@property NSMutableArray *processingRequestQueue;
@end

@implementation PhotoBatchProcessor

-(id)init {
    self = [super init];
    if(self) {
        self.currentProcessingCount = 0;
        self.dataQueue = [NSMutableArray array];
        self.processingRequestQueue = [NSMutableArray array];
    }
    return self;
}

-(void)process:(NSArray*)photos:(id)context:(GenericCallback)callback:(GenericErrorback)errorback {
    if(photos.count == 0) {
        callback(@(0));
        return;
    }
    
    PhotoProcessingRequest *request = [[PhotoProcessingRequest alloc] init:photos :context :callback :errorback];
    
    if(context == nil) {
        context = @{};
    }
    
    [self.processingRequestQueue addObject:request];
    for(Photo *photo in photos) {
        [self.dataQueue addObject:@[photo, context]];
    }
    
    [self processQueue];
}

-(NSArray*)getNext {
    @synchronized(PROCESSING_SYNCHRONIZATION_KEY) {
        if(self.dataQueue.count == 0) {
            return nil;
        }
        
        if(self.currentProcessingCount >= MAX_CONCURRENT_PROCESSING) {
            return nil;
        }
        
        self.currentProcessingCount++;
        DLog(@"PhotoUploader:: current processing count up: %d", self.currentProcessingCount);
        
        NSArray *nextData = self.dataQueue[0];
        [self.dataQueue removeObjectAtIndex:0];
        return nextData;
    }
}

-(void)processQueue {
    @synchronized(PROCESSING_SYNCHRONIZATION_KEY) {
        if(self.dataQueue.count == 0) {
            return;
        }
    }
    
    NSArray *data = nil;
    while((data = [self getNext])) {
        [self process:data[0]:data[1]];
    }
}

-(void)process:(Photo*)photo:(id)context {
    [self onPhotoDone:photo :nil];
}

-(void)onPhotoDone:(Photo*)photo:(NSError*)error {
    
    NSMutableArray *doneRequests = [NSMutableArray array];
    
    @synchronized(PROCESSING_SYNCHRONIZATION_KEY) {
        
        self.currentProcessingCount--;
        DLog(@"PhotoUploader:: current processing count down: %d", self.currentProcessingCount);
        
        for(PhotoProcessingRequest *request in self.processingRequestQueue) {
            BOOL isDone = [request markPhotoDone:photo.id :!error];
            if(isDone) {
                [doneRequests addObject:request];
            }
        }
        
        for(PhotoProcessingRequest *request in doneRequests) {
            [self.processingRequestQueue removeObject:request];
        }
    }
    
    [self processQueue];
    
    for(PhotoProcessingRequest *request in doneRequests) {
        if(!error) {
            request.callback(@(request.photoIDs.count));
        }
        else {
            request.errorback(error);
        }
    }
}
@end
