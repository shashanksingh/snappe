//
//  ExtOpQueue.m
//  snappe
//
//  Created by Shashank on 02/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ExtOpQueue.h"
#import "Macros.h"
#import "AlbumDownloadOperation.h"
#import "ExtendedOperationAccessor.h"
#import "EventManager.h"
#import "Util.h"
#import "Logger.h"

#define EXT_OP_Q_PERSISTENCE_FILE_NAME @"ext_op_q"

@interface ExtOpQueue()
@property (strong) NSMutableArray *queue;
@property (retain) NSObject<ExtOp> *currentOp;
@end

@implementation ExtOpQueue
@synthesize queue;
@synthesize currentOp;

-(id)init {
    self = [super init];
    if(self) {
        self.queue = [[NSMutableArray alloc] init];
        self.currentOp = nil;
        [self load];
        
        DLog(@"loaded extopQ with length: %d", [self.queue count]);
        
        
        [[EventManager getInstance] addEventListener:APPLICATION_ENTERED_BACKGROUND :^(va_list args) {
            [self save];
        }];
        [[EventManager getInstance] addEventListener:APPLICATION_WILL_TERMINATE :^(va_list args) {
            [self save];
        }];
        [[EventManager getInstance] addEventListener:APPLICATION_ENTERED_FOREGROUND :^(va_list args) {
            [self executeNext];
        }];
    }
    return self;
}

+(ExtOpQueue*) getInstance {
    static ExtOpQueue *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ExtOpQueue alloc] init];
    });
    return sharedInstance;
}

-(void)start {
    if(self.currentOp) {
        return;
    }
    [self executeNext];
}

-(void)queue:(NSObject<ExtOp>*)op {
    DLog(@"ExtOpQueue queued op of type: %@", [op class]);
    [self.queue addObject:op];
    [[EventManager getInstance] fireEvent:EXTOP_QUEUED, op];
    
    //no need for synchronization always called from the main thread
    if([self.queue count] == 1) {
        [self executeNext];
    }
}

-(void)executeNext {
    if([Util appIsInBackground]) {
        //while in background, we might just have finished
        //and popped an op, save the reduced queue
        [self save];
        return;
    }
    
    if(!self.currentOp && [self.queue count] == 0) {
        return;
    }
    
    if(!self.currentOp) {
        self.currentOp = [self.queue objectAtIndex:0];
    }
    
    DLog(@"started ext op of class %@, notification id %@ at %@", [self.currentOp class], [self.currentOp getNotification].id, [NSDate date]);
    
    __weak NSObject<ExtOp> *op = self.currentOp;
    
    [self.currentOp start:^(NSNumber *status) {
        DLog(@"finished ext op of class %@, notification id %@ at %@", [op class], [op getNotification].id, [NSDate date]);
        self.currentOp = nil;
        
        if([self.queue count] > 0) {
            NSObject<ExtOp> *head = [self.queue objectAtIndex:0];
            NSString *headNotificationID = [head getNotification].id;
            NSString *currentOpNotificationID = [op getNotification].id;
            
            if(![headNotificationID isEqualToString:currentOpNotificationID]) {
                NSString *message = [NSString stringWithFormat:@"error: finished extop (%@) different from the head of the queue (%@)", currentOpNotificationID, headNotificationID];
                [Logger logError:message :nil];
            }
            
            [self.queue removeObjectAtIndex:0];
            
        }
        else {
            NSString *message = [NSString stringWithFormat:@"error: queue was empty when extop with id %@ finished", [op getNotification].id];
            [Logger logError:message :nil];
        }
        
        [self executeNext];
    }];
}

-(void)load {
    NSArray *persistedQueue = (NSArray*)[Util readObjectFromFile:EXT_OP_Q_PERSISTENCE_FILE_NAME];
    //disabling extop persistence
    if(YES || !persistedQueue) {
        DLog(@"no persisted ExtOpQ found");
        return;
    }
    
    for(NSDictionary *persistedOperation in persistedQueue) {
        NSString *clazz = [persistedOperation objectForKey:@"class"];
        if([clazz isEqualToString:@"AlbumDownloadOperation"]) {
            AlbumDownloadOperation *downloadOp = [[AlbumDownloadOperation alloc] initWithPersistedState:persistedOperation];
            [self.queue addObject:downloadOp];
        }
        else {
            DLog(@"warning: read persisted operation of unknown class: %@", clazz);
        }
    }
}

-(void)save {
    //disabling extop persistence
    return;
    
    //@TODO: thread safety
    NSMutableArray *persistentQueue = [[NSMutableArray alloc] init];
    for(NSObject<ExtOp> *extOp in self.queue) {
        [persistentQueue addObject:[extOp getState]];
    }
    
    BOOL saveSuccess = [Util saveObjectInFile:persistentQueue :EXT_OP_Q_PERSISTENCE_FILE_NAME];
    if(!saveSuccess) {
        DLog(@"error in saving ExtOpQueue to disk");
    }
}

-(NSArray*)getNotifications {
    NSMutableArray *rv = [[NSMutableArray alloc] init];
    for(NSObject<ExtOp> *op in self.queue) {
        [rv addObject:[op getNotification]];
    }
    return rv;
}

@end
