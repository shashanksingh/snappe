//
//  PhotoBatchUploader.m
//  snappe
//
//  Created by Shashank on 28/03/13.
//
//

#import "PhotoBatchUploader.h"
#import "EventManager.h"
#import "Logger.h"
#import "SnappeHTTPService.h"
#import "PhotoAccessor.h"
#import "ExtendedOperationAccessor.h"
#import "Util.h"
#import "CreditsManager.h"
#import "InAppPurchaseView.h"
#import "JSONKit.h"

@implementation PhotoBatchUploader

+(PhotoBatchUploader*) getInstance {
    static PhotoBatchUploader *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PhotoBatchUploader alloc] init];
    });
    return sharedInstance;
}

+(void)upload:(NSArray*)photos:(GenericCallback)callback:(GenericErrorback)errorback {
    //we don't check before each photo, so there is a window for error
    //but we don't care so much
    int credits = [CreditsManager getCreditsCount];
    if(credits < photos.count) {
        NSError *e = [self onNotEnoughCredits:photos.count];
        errorback(e);
        return;
    }
    
    [[self getInstance] process:photos:@{@"photos_to_upload_count": @(photos.count)}:callback:errorback];
}

-(void)process:(Photo*)photo:(id)context {
    [[EventManager getInstance] fireEvent:PHOTO_UPLOAD, photo.id, @(STARTED), nil];
    
    [[SnappeHTTPService getInstance] uploadPhoto:photo :^(id data) {
        [[EventManager getInstance] fireEvent:PHOTO_UPLOAD, photo.id, @(SUCCESSFUL), nil];
        
        [self onPhotoDone:photo:nil];
        
    } :^(NSError *error){
        
        NSDictionary *userInfo = error.userInfo;
        if(userInfo) {
            NSString *apiResponseText = userInfo[@"NSLocalizedRecoverySuggestion"];
            if(apiResponseText) {
                NSDictionary *apiResponse = [apiResponseText objectFromJSONString];
                NSNumber *apiErrorCode = apiResponse[@"code"];
                if(apiErrorCode && apiErrorCode.intValue == 7) {
                    int photosToUploadCount = ((NSNumber*)context[@"photos_to_upload_count"]).intValue;
                    error = [self.class onNotEnoughCredits:photosToUploadCount];
                }
            }
        
        }
        
        [Logger logError:@"error in uploading photo":error];
        [[EventManager getInstance] fireEvent:PHOTO_UPLOAD, photo.id, @(FAILURE), error, nil];
        
        [self onPhotoDone:photo:error];
        
    }:^(long long done, long long toDo){
        
        double thisPhotoDoneFraction = ((double)done)/((double)toDo + 1);
        [[EventManager getInstance] fireEvent:PHOTO_UPLOAD, photo.id, @(ONGOING), @(thisPhotoDoneFraction), nil];
    }];
}

+(NSError*)onNotEnoughCredits:(int)photosToUploadCount {
    NSString *message = [NSString stringWithFormat:@"Sorry, you don't have enough credits to upload photos. You need %d credits but you have only %d available. Please consider purchasing more credits or wait for your credits to be reset on %@", photosToUploadCount, [CreditsManager getCreditsCount], [CreditsManager getNextResetDay]];
    
    [[EventManager getInstance] addEventListener:MODAL_ALERT_VIEW_HIDING :YES :^(va_list args) {
        [InAppPurchaseView show];
    }];
    
    [Util alertError:nil :message];
    
    NSError *e = [NSError errorWithDomain:SNAPPE_ERROR_DOMAIN code:SNAPPE_ERROR_INSUFFICIENT_CREDITS userInfo:nil];
    return e;
}
@end
