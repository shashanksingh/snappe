//
//  AlbumFBUploadOp.m
//  snappe
//
//  Created by Shashank on 05/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AlbumPushOperation.h"
#import "EventManager.h"
#import "Logger.h"
#import "Util.h"
#import "UserAccessor.h"
#import "PhotoLibraryOrganizer.h"
#import "SnappeHTTPService.h"

#define SRC_ALBUM_ID_KEY @"src_album_id"
#define DEST_ALBUM_ID_KEY @"dest_album_id"

@interface AlbumPushOperation()
@property (retain) Album *srcAlbum;
@property (retain) NSObject *dest;
@end

@implementation AlbumPushOperation

-(id)initWithAlbums:(Album*)srcAlbum:(NSObject*)dest {
    self = [super init];
    if(self) {
        self.srcAlbum = srcAlbum;
        self.dest = dest;
        [self setUp];
    }
    return self;
}

-(id)initWithPersistedState:(NSDictionary*)persistedState {
    self = [super initWithPersistedState:persistedState];
    if(self) {
        NSString *srcAlbumId = [persistedState objectForKey:SRC_ALBUM_ID_KEY];
        self.srcAlbum = [Album getById:srcAlbumId];
        if(!self.srcAlbum) {
            DLog(@"warning: error in getting AlbumFBUploadOp from persistent state, invalid src album id: %@", srcAlbumId);
            return nil;
        }
        
        NSString *destAlbumId = [persistedState objectForKey:DEST_ALBUM_ID_KEY];
        if(destAlbumId) {
            self.dest = [Album getById:destAlbumId];
            if(!self.dest) {
                NSString *message = [NSString stringWithFormat:@"warning: error in getting AlbumFBUploadOp from persistent state, invalid dest album id: %@", destAlbumId];
                [Logger logError:message :nil];
                return nil;
            }
        }
        [self setUp];
    }
    return self;
}

-(NSArray*)getPhotos {
    return [self.srcAlbum.photos array];
}

-(NSArray*)getEventTypes {
    return @[@(PHOTO_UPLOAD)];
}

-(NSDictionary*)getState {
    NSMutableDictionary *state = [[super getState] mutableCopy];
    [state setObject:NSStringFromClass(self.class) forKey:@"class"];
    [state setObject:self.srcAlbum.id forKey:SRC_ALBUM_ID_KEY];
    if([self.dest isKindOfClass:[Album class]]) {
        [state setObject:((Album*)self.dest).id forKey:DEST_ALBUM_ID_KEY];
    }    
    return state;
}

-(void)start:(GenericCallback)callback {
    self.status = STARTED;
    [self fireUpdate];
    
    GenericCallback onSuccess = ^(id data) {
        //TODO: resync with server, server will need to fetch the newly created album
        //this is important to be able to dedup on subsequent calls
        [Logger track:@"album_upload" :[NSDictionary dictionaryWithObjectsAndKeys:self.srcAlbum.id, @"album_id", [self getDestID], @"dest_id", nil]];
        
        self.status = SUCCESSFUL;
        self.completion = 100;
        self.finishedOn = [NSDate date];
        
        
        [[SnappeHTTPService getInstance] invalidateFBGraph];
        
        [[PhotoLibraryOrganizer getInstance] sync:^{
            DLog(@"synced with server after album fb upload");
            
            callback(@(self.status));
            
            [self onFinish:YES];
            
        } :^(NSError *error) {
            
            [Logger logError:@"error in syncing after AlbumFBUploadOp" :error];
            
            callback(@(FAILURE));
            
            [self onFinish:NO];
            
        }];
    };
    
    GenericErrorback onError = ^(NSError *error_) {
        if(!([error_.domain isEqualToString:SNAPPE_ERROR_DOMAIN] && error_.code == SNAPPE_ERROR_INSUFFICIENT_CREDITS))
        {
            [Logger logError:[NSString stringWithFormat:@"error in uploading album %@ to album %@", self.srcAlbum.id, [self getDestID]] :error_];
            
            NSString *userMessage = [NSString stringWithFormat:@"There was an error in uploading album %@ to %@. Please try again later.", self.srcAlbum.name, [self getDestName]];
            
            //TODO: make this check tighter than status code check
            if(error_.code == 401 && [self isDestAlbum]) {
                User *friend = ((Album*)self.dest).owner;
                userMessage = [NSString stringWithFormat:@"Sorry! It seems %@'s authorization for Facebook upload has expired. Please ask them to log into Snappe again. This will automatically re-authorize you.", [friend getName]];
            }
            [Util alertError:error_:userMessage];
            
        }
        
        self.status = FAILURE;
        self.error = [error_ localizedDescription];
        self.finishedOn = [NSDate date];
               
        callback(@(self.status));
        
        [self onFinish:NO];
        
    };
    
    if([self isFacebookUpload]) {
        [self.srcAlbum uploadPhotosToFacebook:self.dest :onSuccess :onError];
    }
    else {
        [self.srcAlbum pushTo:((Album*)self.dest) :onSuccess :onError];
    }
}

-(NSString*)getName {
    return [NSString stringWithFormat:@"Uploading album \"%@\" to \"%@\"", self.srcAlbum.name, [self getDestName]];
}

-(NSString*)getType {
    if([self isFacebookUpload]) {
        return @"EXTOP_ALBUM_FB_UPLOAD";
    }
    else {
        return @"EXTOP_ALBUM_PUSH";
    }
}

-(NSString*)getStatusText {
    NSString *statusText = nil;
    if(self.status == NOT_STARTED) {
        statusText = @"waiting...";
    }
    else if(self.status == STARTED) {
        statusText = [NSString stringWithFormat:@"upload %d%% completed", MIN(self.completion, 99)];
    }
    else if(self.status == SUCCESSFUL) {
        long secondsSinceDone = [self.finishedOn timeIntervalSince1970];
        statusText = [NSString stringWithFormat:@"successfully uploaded %@", [Util getRelativeTime:secondsSinceDone]];
    }
    else if(self.status == FAILURE) {
        statusText = @"failed, please try again";
    }
    else {
        DLog(@"warning: unknown value for ext op status: %d", self.status);
    }
    return statusText;
}

-(Notification*)getNotification {
    Notification *notification = [super getNotification];
    
    NSMutableDictionary *notificationData = notification.data;
    [notificationData setObject:self.srcAlbum.id forKey:@"src_album_id"];
    //we need to provide album name so that we can show *some* info even
    //if the album is deleted afterwards
    [notificationData setObject:self.srcAlbum.name forKey:@"src_album_name"];
    [notificationData setObject:[self getDestName] forKey:@"dest_album_name"];
    
    return notification;
}

-(BOOL)isDestAlbum {
    return [self.dest isKindOfClass:[Album class]];
}

-(NSString*)getDestName {
    return [self isDestAlbum] ? ((Album*)self.dest).name : (NSString*)self.dest;
}

-(NSString*)getDestID {
    return [self isDestAlbum] ? ((Album*)self.dest).id : nil;
}

-(BOOL)isFacebookUpload {
    return ![self isDestAlbum] || [((Album*)self.dest) isOwnedByCurrentUser];
}
@end
