//
//  ImageHasher.h
//  snappe
//
//  Created by Shashank on 20/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface ImageHasher : NSObject
+(NSString*) getHash:(CGImageRef)image;
+(NSString*) getHashForALAsset:(ALAsset*)asset;
+(NSString*) getHashForURL:(NSURL*)url;
+(NSString*) getHashForData:(NSData *)data;
+(NSString*) getHashForPNGData:(NSData*)data;
+(NSString*) getHashForJPEGData:(NSData*)data;
+(BOOL) areSimilarImages:(NSString*)hash1 :(NSString*)hash2;
+(UInt8) getHashDistanceForStringHash:(NSString*)hash1 :(NSString*)hash2;
@end
