//
//  CreditsManager.m
//  snappe
//
//  Created by Shashank on 12/01/13.
//
//

#import "CreditsManager.h"
#import "Lockbox.h"
#import "EventManager.h"
#import "Logger.h"
#import "SnappeHTTPService.h"
#import "Base64.h"

#define KEYCHAIN_CREDITS_COUNT_KEY @"credits_count_2"
#define KEYCHAIN_CREDITS_NEXT_RESET_TIME_KEY @"next_credit_reset_time_2"
#define KEYCHAIN_PURCHASED_NON_CONSUMABLE_PRODUCT_IDS_KEY @"purchased_non_consumable_product_ids_3"

#define INFINITY_CREDITS 10000
#define AUTO_RESET_INTERVAL_DAYS 30

#define FETCH_TIMER_WAIT 5

@interface CreditsManager()<SKProductsRequestDelegate, SKPaymentTransactionObserver>
@property (strong) NSArray *products;
@property (strong) NSTimer *fetchTimer;
@end


@implementation CreditsManager

-(id)init {
    self = [super init];
    if(self) {
        self.fetchTimer = nil;
        
        __weak typeof(self) bself = self;
        [[EventManager getInstance] addEventListener:APPLICATION_ENTERED_BACKGROUND :^(va_list args) {
            if(bself) {
                if(bself.fetchTimer) {
                    [bself.fetchTimer invalidate];
                    bself.fetchTimer = nil;
                }
            }
        }];
        
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    }
    return self;
}

+(CreditsManager *) getInstance {
    static CreditsManager *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[CreditsManager alloc] init];
    });
    return instance;
}

+(NSSet*)getProductIDs {
    return [NSSet setWithArray:@[
            @"pe.snap.products.non_consumables.remove_ads",
            @"pe.snap.products.consumables.300_credits",
            @"pe.snap.products.consumables.3000_credits",
            @"pe.snap.products.non_consumables.unlimited_credits_2"
    ]];
}

-(void)getProducts:(ArrayCallback)callback:(GenericErrorback)errorback {
    if(self.products) {
        [Logger track:@"credits_fetcehed_products" :@{@"product_count" : @(self.products.count), @"cached" : @(YES)}];
        callback(self.products);
        return;
    }
    [self updateProductsFromApple:^(NSArray *prods) {
        [Logger track:@"credits_fetcehed_products" :@{@"product_count" : @(self.products.count), @"cached" : @(NO)}];
        callback(prods);
    }:errorback];
    
}

-(void)fetchProducts {
    self.fetchTimer = [NSTimer scheduledTimerWithTimeInterval:FETCH_TIMER_WAIT target:self selector:@selector(onFetchTimerFire) userInfo:nil repeats:NO];
}

-(void)buy:(SKProduct*)product {
    [Logger track:@"product_purchase_initiated" :@{@"product_id": product.productIdentifier}];
    
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

-(void)restore {
    [Logger track:@"product_restore_initiated"];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

+(BOOL)isConsumableProduct:(NSString*)productID {
    return [productID rangeOfString:@".consumables."].location != NSNotFound;
}

-(void)updateProductsFromApple:(ArrayCallback)callback:(GenericErrorback)errorback {
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:[self.class getProductIDs]];
    request.delegate = self;
    
    [[EventManager getInstance] addEventListener:APP_STORE_PRODUCTS_UPDATED :YES :^(va_list args) {
        NSNumber *successObject = va_arg(args, NSNumber*);
        BOOL success = [successObject boolValue];
        if(success) {
            NSArray *products = va_arg(args, NSArray*);
            callback(products);
        }
        else {
            NSError *error = va_arg(args, NSError*);
            errorback(error);
        }
    }];
    [request start];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSArray *products = response.products;
    products = [products sortedArrayUsingComparator:^NSComparisonResult(SKProduct *p1, SKProduct *p2) {
        return p1.price.floatValue - p2.price.floatValue;
    }];
    self.products = products;
    [[EventManager getInstance] fireEvent:APP_STORE_PRODUCTS_UPDATED, @(YES), products, nil];
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    [[EventManager getInstance] fireEvent:APP_STORE_PRODUCTS_UPDATED, @(NO), error, nil];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    [Logger track:@"product_purchase_succeeded" :@{@"product_id":transaction.payment.productIdentifier}];
    
    NSString *receipt = [transaction.transactionReceipt base64EncodedString];
    [[SnappeHTTPService getInstance] verifyPurchase:receipt :^(NSNumber *verified) {
        if([verified boolValue]) {
            [self.class addPurchasedConsumable:transaction.payment.productIdentifier];
            
            [[EventManager getInstance] fireEvent:APP_STORE_PRODUCT_PURCHASE_SUCCEEDED, transaction.payment.productIdentifier, nil];
            
            [Logger track:@"product_purchase_receipt_verification_succeeded" :@{@"product_id": transaction.payment.productIdentifier, @"transaction_id": transaction.transactionIdentifier}];
        }
        else {
            NSError *error = [NSError errorWithDomain:SNAPPE_ERROR_DOMAIN code:SNAPPE_ERROR_PURCHASE_VERIFICATION_FAILED userInfo:@{@"description": @"Unable to verify purchase"}];
            [[EventManager getInstance] fireEvent:APP_STORE_PRODUCT_PURCHASE_FAILED, error, nil];
            
            [Logger track:@"product_purchase_receipt_verification_failed" :@{@"product_id": transaction.payment.productIdentifier, @"transaction_id": transaction.transactionIdentifier}];
        }
        
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        
    } :^(NSError *error) {
        [[EventManager getInstance] fireEvent:APP_STORE_PRODUCT_PURCHASE_FAILED, error, nil];
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        
        [Logger track:@"product_purchase_receipt_verification_failed" :@{@"product_id": transaction.payment.productIdentifier, @"error_code": @(error.code), @"error_message": error.description, @"transaction_id": transaction.transactionIdentifier}];
        
    }];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    [self.class addPurchasedConsumable:transaction.payment.productIdentifier];
    
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    [Logger track:@"product_purchase_restored" :@{@"product_id":transaction.payment.productIdentifier}];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        [Logger track:@"product_purchase_failed" :@{@"product_id": transaction.payment.productIdentifier, @"error_code": @(transaction.error.code), @"error_message": transaction.error.description, @"transaction_id": transaction.transactionIdentifier}];
        
        [[EventManager getInstance] fireEvent:APP_STORE_PRODUCT_PURCHASE_FAILED, transaction.error, nil];
    }
    else {
        [Logger track:@"product_purchase_cancelled" :@{@"product_id": transaction.payment.productIdentifier}];
        [[EventManager getInstance] fireEvent:APP_STORE_PRODUCT_PURCHASE_CANCELLED, nil];
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    [[EventManager getInstance] fireEvent:APP_STORE_PRODUCT_PURCHASE_RESTORED, nil];
}

-(void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    DLog(@"error: %@", error);
    
    if(error) {
        [Logger track:@"product_restore_failed" :@{@"code": @(error.code), @"message": error.localizedDescription}];
    }
    else {
        [Logger track:@"product_restore_failed"];
    }
    
    
    if(error.code == 2) {//"Cannot connect to iTunes Store" == USER_CANCELLED
        [[EventManager getInstance] fireEvent:APP_STORE_PRODUCT_PURCHASE_CANCELLED, nil];
    }
    else {
        [[EventManager getInstance] fireEvent:APP_STORE_PRODUCT_PURCHASE_FAILED, error, nil];
    }

}

-(void)onFetchTimerFire {
    [self updateProductsFromApple:^(NSArray *data) {
        [Logger track:@"products_synced"];
    } :^(NSError *error) {
        [Logger logError:@"error in syncing products" :error];
    }];
}


+(void)updateFromServerJSON:(id)serverJSON {
    int credits = [[serverJSON objectForKey:@"credits"] intValue];
    int nextResetTime = [[serverJSON objectForKey:@"next_reset"] intValue];
    [self setCreditsCount:credits];
    [self setNextResetTime:nextResetTime];
}

+(void)setCreditsCount:(int)count {
    [Lockbox setString:[NSString stringWithFormat:@"%d", count] forKey:KEYCHAIN_CREDITS_COUNT_KEY];
    [[EventManager getInstance] fireEvent:CREDITS_COUNT_UPDATED, count];
}

+(void)setNextResetTime:(int)time {
    [Lockbox setString:[NSString stringWithFormat:@"%d", time] forKey:KEYCHAIN_CREDITS_NEXT_RESET_TIME_KEY];
}

+(int)getNextResetTime {
    NSString *storedVal = [Lockbox stringForKey:KEYCHAIN_CREDITS_NEXT_RESET_TIME_KEY];
    if(!storedVal) {
        return [[NSDate date] timeIntervalSince1970] + (86400 * AUTO_RESET_INTERVAL_DAYS);
    }
    return [storedVal intValue];
}

+(NSString*)getNextResetDay {
    int t = [self getNextResetTime];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd YYYY"];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    return [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:t]];
}

+(void)addPurchasedConsumable:(NSString*)productID {
    NSMutableSet *alreadyPurchasedSet = [[self getPurchasedProductIDs] mutableCopy];
    [alreadyPurchasedSet addObject:productID];
    
    NSArray *newPurchasedProductsArray = [alreadyPurchasedSet allObjects];
    [Lockbox setArray:newPurchasedProductsArray forKey:KEYCHAIN_PURCHASED_NON_CONSUMABLE_PRODUCT_IDS_KEY];
    
    [[EventManager getInstance] fireEvent:PURCHASED_PRODUCT_UPDATED, productID];
}

+(int)getCreditsCount {
    NSString *storedVal = [Lockbox stringForKey:KEYCHAIN_CREDITS_COUNT_KEY];
    if(!storedVal) {
        return 0;
    }
    return [storedVal intValue];
}

+(NSSet*)getPurchasedProductIDs {
    NSArray *alreadyPurchasedArray = [Lockbox arrayForKey:KEYCHAIN_PURCHASED_NON_CONSUMABLE_PRODUCT_IDS_KEY];
    if(!alreadyPurchasedArray) {
        alreadyPurchasedArray = [NSArray array];
    }
    return  [NSSet setWithArray:alreadyPurchasedArray];
}

+(BOOL)isAdRemovalPurchased {
    return [[self getPurchasedProductIDs] containsObject:@"pe.snap.products.non_consumables.remove_ads"];
}

+(BOOL)isUnlimitedCreditsPurchased {
    return [[self getPurchasedProductIDs] containsObject:@"pe.snap.products.non_consumables.unlimited_credits_2"];
}


+(void)decrementCreditsCount {
    int currentCount = [self getCreditsCount];
    if(currentCount >= INFINITY_CREDITS) {
        return;
    }
    
    [self setCreditsCount:currentCount - 1];
}

+(int)getInfinityCreditsValue {
    return INFINITY_CREDITS;
}

-(NSString*)getProductName:(NSString*)productID {
    for(SKProduct *product in self.products) {
        if([product.productIdentifier isEqualToString:productID]) {
            return product.localizedTitle;
        }
    }
    return nil;
}
@end
