
//
//  NotificationManager.m
//  snappe
//
//  Created by Shashank on 29/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NotificationManager.h"
#import "NotificationAccessor.h"
#import "GraphManager.h"
#import "Macros.h"
#import "EventManager.h"
#import "GraphManager.h"
#import "SnappeHTTPService.h"
#import <Foundation/Foundation.h>
#import "ExtOpQueue.h"
#import "Logger.h"

#define MARK_SHOWN_QUEUE_FLUSH_DELAY_SECONDS 5.0

static UInt8 MAX_NOTIFICATIONS = 100;
static NSMutableArray *notifications;

@interface NotificationManager()
@property (strong) NSMutableArray *markShownQueue;
@property (strong) NSTimer *markShowQueueFlusher;
@end

@implementation NotificationManager
@synthesize markShownQueue;
@synthesize markShowQueueFlusher;

-(id)init{
    self = [super init];
    if(self) {
        self.markShownQueue = [[NSMutableArray alloc] init];
        self.markShowQueueFlusher = nil;
        
        [self loadSavedNotifications];
        
        [[EventManager getInstance] addEventListener:EXTOP_QUEUED :^(va_list args) {
            NSObject<ExtOp> *op = va_arg(args, NSObject<ExtOp>*);
            [self addNewNotification:[op getNotification]];
        }];
    }
    return self;
    
}

+(NotificationManager*) getInstance {
    static NotificationManager *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NotificationManager alloc] init];
    });
    return sharedInstance;
}

-(void)addNewNotification:(Notification*)notification {
    for(Notification *existingNotification in notifications) {
        if([existingNotification.id isEqualToString:notification.id]) {
            DLog(@"warning: trying to add notification with id %@ that already exists", notification.id);
            return;
        }
    }
    [notifications addObject:notification];
    notifications = [[self sortNotifications:notifications] mutableCopy];
    [self updateNewNotificationBadge];
    
    [[EventManager getInstance] fireEvent:NEW_NOTIFICATION_AVAILABLE, [NSArray arrayWithObjects:notification, nil]];
    
}

-(void)updateNewNotificationBadge {
    NSUInteger pendingCount = [self getPendingNotificationCount];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:pendingCount];
    [[EventManager getInstance] fireEvent:PENDING_NOTIFICATION_COUNT_CHANGED, [NSNumber numberWithInt:pendingCount], nil];
}

-(NSUInteger)getPendingNotificationCount {
    NSUInteger rv = 0;
    for(Notification *notification in notifications) {
        if([[notification pending] boolValue]) {
            rv++;
        }
    }
    return rv;
}

-(void)flushMarkShowQueue {
    if([self.markShownQueue count] == 0) {
        return;    
    }
    
    NSArray *queue = [NSArray arrayWithArray:self.markShownQueue];
    [self.markShownQueue removeAllObjects];
    [self markShown:queue :^(id data) {
        [self updateNewNotificationBadge];
    } :^(NSError *error) {
        [Logger logError:@"error in flushing mark-shown notification queue":error];
    }];
}

-(void)markNotificationShown:(Notification*)notification {
    if(![notification.pending boolValue]) {
        return;
    }
    [self.markShownQueue addObject:notification];
    if(self.markShowQueueFlusher) {
        [self.markShowQueueFlusher invalidate];
        self.markShowQueueFlusher = nil;
    }
    self.markShowQueueFlusher = [NSTimer scheduledTimerWithTimeInterval:MARK_SHOWN_QUEUE_FLUSH_DELAY_SECONDS target:self selector:@selector(flushMarkShowQueue) userInfo:nil repeats:NO];
}

-(void)markShown:(NSArray*)notifications :(GenericCallback)callback :(GenericErrorback)errorback {
    [[SnappeHTTPService getInstance] markNotificationsShown:notifications :^(id data) {
        for(Notification *notification in notifications) {
            [notification setPending:[NSNumber numberWithBool:NO]];
            [[EventManager getInstance] fireEvent:NOTIFICATION_MARKED_SEEN, notification, nil];
        }
        [[GraphManager getInstance] savePersistentContext];
        callback(data);
    } :errorback];
}

-(void) loadSavedNotifications {
    NSArray *allNotifications = [GraphManager getGraphObjectsByProperties:[[NSDictionary alloc] initWithObjectsAndKeys: nil] :@"Notification"];
    notifications = [[self sortNotifications:allNotifications] mutableCopy];
    
    if([notifications count] > MAX_NOTIFICATIONS) {
        NSRange rangeToRemove;
        rangeToRemove.length = MAX_NOTIFICATIONS - [notifications count];
        rangeToRemove.location = [notifications count] - rangeToRemove.length;
        
        NSArray *toRemove = [notifications subarrayWithRange:rangeToRemove];
        for(Notification *notification in toRemove) {
            [GraphManager deleteObject:notification];
        }
        
        NSRange rangeToKeep;
        rangeToKeep.length = MAX_NOTIFICATIONS;
        rangeToKeep.location = 0;
        notifications = [[notifications subarrayWithRange:rangeToKeep] mutableCopy];
        
        [[GraphManager getInstance] savePersistentContext];
    }
}

-(NSArray*)getNotifications:(UInt16)limit {
    NSRange range;
    range.location = 0;
    range.length = MIN(limit, [notifications count]);
    
    return [notifications subarrayWithRange:range];
}

-(void)clearAllNotifications {
    NSArray *notifications = [GraphManager getGraphObjectsByProperties:[[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:YES], @"pending", nil] :@"Notification"];
    for(Notification *notification in notifications) {
        [notification setPending:[NSNumber numberWithBool:NO]];
    }
    [[GraphManager getInstance] savePersistentContext];
}

-(void) updateWithServerNotifications:(NSArray*) notificationJSONObjects {
    DLog(@"updating notifications: %@", notificationJSONObjects);
    NSMutableArray *newNotifications = [NSMutableArray array];
    for(NSDictionary *notificationJSONObject in notificationJSONObjects) {
        if(![Notification checkedExistsLocally:notificationJSONObject]) {
            Notification *notification = [Notification createFromServerJSON:notificationJSONObject];
            [newNotifications addObject:notification];    
        }
    }
    [[GraphManager getInstance] savePersistentContext];
    
    [self loadSavedNotifications];
    
    NSArray *sortedNewNotifications = [self sortNotifications:newNotifications];
    if([newNotifications count] > 0) {
        [self updateNewNotificationBadge];
        [[EventManager getInstance] fireEvent:NEW_NOTIFICATION_AVAILABLE, sortedNewNotifications, nil];    
    }
}

-(NSArray*)sortNotifications:(NSArray*)unsortedNotifications {
    return [unsortedNotifications sortedArrayUsingComparator:^NSComparisonResult(Notification *not1, Notification *not2) 
     {
         return [not2.timestamp compare:not1.timestamp];
     }];
}

@end
