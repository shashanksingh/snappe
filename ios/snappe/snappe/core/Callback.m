//
//  Callback.m
//  snappe
//
//  Created by Shashank on 26/03/13.
//
//

#import "Callback.h"

@interface GenericCallback()
@property dispatch_queue_t callerQ;
@property (copy) void (^callback)(id);
@end

@implementation GenericCallback
-(id)init:(void (^)(id data))callback {
    self = [super init];
    if(self) {
        self.callerQ = dispatch_get_current_queue();
        self.callback = callback;
    }
    return self;
}

+(GenericCallback*)get:(void (^)(id data))callback {
    return [[GenericCallback alloc] init:callback];
}

-(void)call:(id)data {
    dispatch_async(self.callerQ, ^{
        self.callback(data);
    });
}
@end


@interface GenericErrorback()
@property dispatch_queue_t callerQ;
@property (copy) void (^errorback)(NSError*);
@end

@implementation GenericErrorback
-(id)init:(void (^)(NSError *error))errorback {
    self = [super init];
    if(self) {
        self.callerQ = dispatch_get_current_queue();
        self.errorback = errorback;
    }
    return self;
}

+(GenericErrorback*)get:(void (^)(NSError *error))errorback {
    return [[GenericErrorback alloc] init:errorback];
}

-(void)call:(NSError*)error {
    dispatch_async(self.callerQ, ^{
        self.errorback(error);
    });
}
@end

@interface VoidCallback()
@property dispatch_queue_t callerQ;
@property (copy) void (^callback)(void);
@end

@implementation VoidCallback
-(id)init:(void (^)(void))callback {
    self = [super init];
    if(self) {
        self.callerQ = dispatch_get_current_queue();
        self.callback = callback;
    }
    return self;
}

+(VoidCallback*)get:(void (^)(void))callback {
    return [[VoidCallback alloc] init:callback];
}

-(void)call {
    dispatch_async(self.callerQ, ^{
        self.callback();
    });
}
@end
