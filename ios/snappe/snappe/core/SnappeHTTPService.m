//
//  NetworkManager.m
//  snappe
//
//  Created by Shashank on 09/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SnappeHTTPService.h"
#import "PhotoAccessor.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "Util.h"
#import "Macros.h"
#import "SyncManager.h"
#import "PhotoLibraryReader.h"
#import "JSONKit.h"
#import "CoreController.h"
#import "PhotoSaveManager.h"
#import "NotificationManager.h"
#import "NotificationAccessor.h"
#import "LocationAccessor.h"
#import "Logger.h"
#import "CreditsManager.h"
#import "UIImage+fixOrientation.h"

#import <CoreLocation/CoreLocation.h>

#define REQUEST_TIMEOUT_INTERVAL 120

@interface SnappeHTTPService()
@property BOOL fbGraphInvalidated;
@end

static NSString * const API_ENDPOINT =  @"http://snap.pe/api/v1/";

typedef void(^SuccessHandler)(NSURLRequest*, NSHTTPURLResponse*, id);
typedef void(^FailureHandler)(NSURLRequest*, NSHTTPURLResponse*, NSError*, id);


@implementation SnappeHTTPService

-(id) init {
    self = [super initWithBaseURL:[NSURL URLWithString:API_ENDPOINT]];
    if(self) {
        //invalidate graph when the app starts
        self.fbGraphInvalidated = YES;
        
        SnappeHTTPService *ref = self;//to avoid retain cycle
        [self setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            if(status == AFNetworkReachabilityStatusNotReachable || status == AFNetworkReachabilityStatusUnknown) 
            {
                if(![ref.operationQueue isSuspended]) {
                    //TODO: alert
                    [ref.operationQueue setSuspended:YES];
                }
            }
            else {
                if([ref.operationQueue isSuspended]) {
                    //TODO:alert
                    [ref.operationQueue setSuspended:NO];    
                }
            }
        }];
    }
    return self;
}

+(SnappeHTTPService *) getInstance {
    static SnappeHTTPService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SnappeHTTPService alloc] init];
    });
    return sharedInstance;
}

-(void)invalidateFBGraph {
    self.fbGraphInvalidated = YES;
}

-(void) getPath:(NSString *)path WithParameters:(NSDictionary *)params callback:(GenericCallback)callback errorback:(GenericErrorback)errorback
{
    if(self.networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable)
    {
        NSError *error = [NSError errorWithDomain:@"snappe" code:SNAPPE_ERROR_NETWORK_UNREACHABLE userInfo:nil];
        errorback(error);
        return;
    }
    
    NSDate *startTime = [NSDate date];
    
    NSMutableURLRequest *request = [self requestWithMethod:@"GET" path:path parameters:params];
    
    [request setTimeoutInterval:REQUEST_TIMEOUT_INTERVAL];
    
    [self enqueueURLRequest:request WithCallback:callback errorback:^(NSError *error){
        NSError *underlyingError = error.userInfo[@"NSUnderlyingError"];
        if([underlyingError.description rangeOfString:@"timed out"].location != NSNotFound) {
            NSString *message = [NSString stringWithFormat:@"HTTP timeout in %fs", [[NSDate date] timeIntervalSinceDate:startTime]];
            [Logger logError:message :error];
        }
        errorback(error);
    }];
}


-(void) postPath:(NSString *)path WithParameters:(NSDictionary *)params callback:(GenericCallback)callback errorback:(GenericErrorback)errorback
{
    if(self.networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable)
    {
        NSError *error = [NSError errorWithDomain:@"snappe" code:SNAPPE_ERROR_NETWORK_UNREACHABLE userInfo:nil];
        errorback(error);
        return;
    }
    
    NSDate *startTime = [NSDate date];
    
    NSMutableURLRequest *request = [self requestWithMethod:@"POST" path:path parameters:params];
    
    [self enqueueURLRequest:request WithCallback:callback errorback:^(NSError *error){
        //error code is not set for timeout
        NSError *underlyingError = error.userInfo[@"NSUnderlyingError"];
        if(underlyingError && [underlyingError.description rangeOfString:@"timed out"].location != NSNotFound) {
            NSString *message = [NSString stringWithFormat:@"HTTP timeout in %fs", [[NSDate date] timeIntervalSinceDate:startTime]];
            [Logger logError:message :error];
        }
        errorback(error);
    }];
    
}

-(void) uploadFile:(NSString*)path :(NSDictionary*)extraParams :(NSString*)fileParamName :(NSString*)fileName :(NSData*)fileData :(NSString*)fileMimeType :(GenericCallback)callback :(GenericErrorback)errorback:(ProgressCallback)progressCallback
{
    
    if(![self ensureAuth:errorback]) {
        return;
    }
    
    NSMutableURLRequest *request = [self multipartFormRequestWithMethod:@"POST" path:path parameters:extraParams constructingBodyWithBlock: ^(id <AFMultipartFormData> formData)
    {
        [formData appendPartWithFileData:fileData name:fileParamName fileName:fileName mimeType:fileMimeType];
    }];
    
    [self setRequestAuthPayload:request];
    
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
    {
        
        dispatch_async(callerQ, ^{
            progressCallback(totalBytesWritten, totalBytesExpectedToWrite);
        });
        
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        dispatch_async(callerQ, ^{
            callback(responseObject);
        });
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        dispatch_async(callerQ, ^{
            errorback(error);
        });
        
    }];
    
    [self enqueueHTTPRequestOperation:operation];
}
                 
-(void) enqueueURLRequest:(NSMutableURLRequest*)request WithCallback:(GenericCallback)callback errorback:(GenericErrorback)errorback
{
    
    if(![self ensureAuth:errorback]) {
        return;
    }
    
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    SuccessHandler successHandler = ^(NSURLRequest *request, NSHTTPURLResponse *response, id json) {
        [self parseSuccessOfRequest:request:response:json:^(id data){
            dispatch_async(callerQ, ^{
                callback(data);
            });
        }:errorback];
    };           
    FailureHandler failureHandler = ^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id json) {
        if(errorback) {
            NSError *customError = [NSError errorWithDomain:error.domain code:response.statusCode userInfo:error.userInfo];
            dispatch_async(callerQ, ^{
                errorback(customError);
            });
        }
    };
    
    [self setRequestAuthPayload:request];
    
    AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:successHandler failure:failureHandler];
    
    [self enqueueHTTPRequestOperation: requestOperation];
}

-(BOOL)ensureAuth:(GenericErrorback)errorback {
    NSString *accessToken = [[FBManager getInstance] getAccessToken];
    if(!accessToken) {
        NSError *error = [NSError errorWithDomain:SNAPPE_ERROR_DOMAIN code:SNAPPE_ERROR_FB_NOT_AUTHORIZED userInfo:nil];
        if(errorback) {
            errorback(error);
        }
        else {
            [Logger logError:@"error in getting FB accesstoken" :error];
        }
        return NO;
    }
    return YES;
}

-(void)setRequestAuthPayload:(NSMutableURLRequest*)request {
    NSURL *apiEndPointURL = [NSURL URLWithString:API_ENDPOINT];
    //add auth headers only for our servers
    if(![request.URL.host isEqualToString:apiEndPointURL.host]) {
        return;
    }
    
    NSString *accessToken = [[FBManager getInstance] getAccessToken];
    if(!accessToken) {
        return;
    }
    
    NSString *deviceID = [[CoreController getInstance] getDeviceID];
    unsigned long lastSyncTime = [SyncManager getLastSyncTime];
    
    NSString *apnsToken = [[CoreController getInstance] getAPNSToken];
    
    DLog(@"last sync time: %lu", lastSyncTime);
    [request setValue:deviceID forHTTPHeaderField:@"X-DeviceID"];
    [request setValue:accessToken forHTTPHeaderField:@"X-FacebookAccessToken"];
    [request setValue:[NSString stringWithFormat:@"%lu", lastSyncTime] forHTTPHeaderField:@"X-LastSyncTime"];
    if(apnsToken) {
        [request setValue:apnsToken forHTTPHeaderField:@"X-APNSToken"];
    }
    if(self.fbGraphInvalidated) {
        [request setValue:@"1" forHTTPHeaderField:@"X-ForceFBRefresh"];
        self.fbGraphInvalidated = NO;
    }
    
}


-(void) applyLocalModifications:(NSDictionary*)localModDescriptor:(GenericCallback)callback:(GenericErrorback) errorback
{
    
    NSArray *newAlbums = [localModDescriptor objectForKey:@"add"];
    NSDictionary *localDeleteDescriptor = [localModDescriptor objectForKey:@"delete"];
    NSArray *modifiedAlbums = [localModDescriptor objectForKey:@"modify"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    NSMutableArray *newAlbumsArray = [NSMutableArray array];
    
    for(Album *album in newAlbums) {
        NSMutableDictionary *albumDict = [NSMutableDictionary dictionary];
        [newAlbumsArray addObject:albumDict];
        [albumDict setObject:[album getId] forKey:@"id"];
        [albumDict setObject:[album getDeviceAlbumTypeName] forKey:@"type"];
        [albumDict setObject:[album name] forKey:@"name"];
        [albumDict setObject:album.devicePersistentID forKey:@"devicePersistentId"];
        
        NSString *description = [album getDescription];
        [albumDict setObject:(description == nil ? @"" : description) forKey:@"description"];
        
        NSMutableArray *photoDictList = [[NSMutableArray alloc] init];
        [albumDict setObject:photoDictList forKey:@"photos"];
        
        for(Photo *photo in [album photos]) {
            NSMutableDictionary *photoDict = [NSMutableDictionary dictionary];
            [photoDictList addObject:photoDict];
            
            NSString *hash = [photo getHash];
            [photoDict setObject:hash forKey:@"hash"];
            
            NSDate *clickedOnDate = [photo getClickedOn];
            NSNumber *clickedOnTimestamp = [NSNumber numberWithDouble:[clickedOnDate timeIntervalSince1970]];
            [photoDict setObject:clickedOnTimestamp forKey:@"clickedOn"];
            
            [photoDict setObject:photo.location.latitude forKey:@"latitude"];
            [photoDict setObject:photo.location.longitude forKey:@"longitude"];
            
            [photoDict setObject:[photo getWidth] forKey:@"width"]; 
            [photoDict setObject:[photo getHeight] forKey:@"height"]; 
            [photoDict setObject:[photo getThumbWidth] forKey:@"thumbWidth"]; 
            [photoDict setObject:[photo getThumbHeight] forKey:@"thumbHeight"];
            [photoDict setObject:[photo getPHash] forKey:@"pHash"];
        }
        
    }
    
    NSMutableDictionary *remoteDeleteDescriptor = [NSMutableDictionary dictionary];
    for(NSString *key in localDeleteDescriptor) {
        NSMutableArray *deleteIDs = [[NSMutableArray alloc] init];
        [remoteDeleteDescriptor setObject:deleteIDs forKey:key];
        for(NSManagedObject *deleteObject in [localDeleteDescriptor objectForKey:key]) {
            [deleteIDs addObject:[deleteObject valueForKey:@"id"]];
        }
    }
    
    NSMutableArray *modifiedAlbumsArray = [NSMutableArray array];
    for(Album *modifiedAlbum in modifiedAlbums) {
        NSMutableDictionary *albumDict = [NSMutableDictionary dictionary];
        [modifiedAlbumsArray addObject:albumDict];
        [albumDict setObject:[modifiedAlbum getId] forKey:@"id"];
        [albumDict setObject:[modifiedAlbum getDeviceAlbumTypeName] forKey:@"type"];
        [albumDict setObject:[modifiedAlbum name] forKey:@"name"];
        [albumDict setObject:modifiedAlbum.devicePersistentID forKey:@"devicePersistentId"];
        
        NSString *description = [modifiedAlbum getDescription];
        [albumDict setObject:(description == nil ? @"" : description) forKey:@"description"];
    }
    
    [params setObject:[newAlbumsArray JSONString] forKey:@"add"];
    [params setObject:[remoteDeleteDescriptor JSONString] forKey:@"delete"];
    [params setObject:[modifiedAlbumsArray JSONString] forKey:@"modify"];
    DLog(@"applyLocalModifications params: %@", params);
    
    [self postPath:@"apply_local_mods" WithParameters:params callback:callback errorback:errorback];
}

-(void) getPhotoHashesToUpload: (Album*)album :(ArrayCallback)callback :(GenericErrorback)errorback {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"get_upload_status" forKey:@"action"];
    [params setObject:[album getId] forKey:@"aid"];
    
    [self getPath:@"share" WithParameters:params callback:callback errorback:errorback];
}

-(void)getPhotoHashesNeedingUpload:(NSSet*)hashes:(ArrayCallback)callback:(GenericErrorback)errorback {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"get_upload_status" forKey:@"action"];
    [params setObject:[[hashes allObjects] JSONString] forKey:@"hashes"];
    
    [self getPath:@"share" WithParameters:params callback:callback errorback:errorback];
}

-(void) updateFriendShareState:(NSString *)albumId :(NSArray*)changeDescriptors :(NSString*)appRequestID :(GenericCallback)callback :(GenericErrorback)errorback
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"action"] = @"update_share_state";
    params[@"aid"] = albumId;
    params[@"changes"] = [changeDescriptors JSONString];
    if(appRequestID) {
        params[@"app_request_id"] = appRequestID;
    }
    [self postPath:@"share" WithParameters:params callback:callback errorback:errorback];
}

-(void)pushAlbum:(NSString *)sourceAlbumID :(NSString *)destinationAlbumID :(GenericCallback)callback :(GenericErrorback)errorback
{
    [self postPath:@"share" WithParameters:@{
        @"action" : @"push",
        @"source_album_id" : sourceAlbumID,
        @"destination_album_id" : destinationAlbumID
    } callback:callback errorback:errorback];
}

-(void)pullAlbum:(NSString *)sourceAlbumID :(NSString *)destinationAlbumID :(GenericCallback)callback :(GenericErrorback)errorback
{
    [self postPath:@"share" WithParameters:@{
        @"action" : @"pull",
        @"source_album_id" : sourceAlbumID,
        @"destination_album_id" : destinationAlbumID
    } callback:callback errorback:errorback];
}

-(void)syncAlbums:(NSString *)localAlbumID :(NSString *)remoteAlbumID :(GenericCallback)callback :(GenericErrorback)errorback
{
    [self postPath:@"share" WithParameters:@{
        @"action" : @"sync",
        @"local_album_id" : localAlbumID,
        @"remote_album_id" : remoteAlbumID
    } callback:callback errorback:errorback];
}

-(void)shareAlbumWithContacts:(NSString *)albumId :(NSArray*)shareDescriptor :(GenericCallback)callback :(GenericErrorback)errorback
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"email_share" forKey:@"action"];
    [params setObject:albumId forKey:@"aid"];
    [params setObject:[shareDescriptor JSONString] forKey:@"descriptors"];
    [self postPath:@"share" WithParameters:params callback:callback errorback:errorback];
}

-(void)addPhotosToAlbum:(Album*)album:(NSArray*)photos:(VoidCallback)callback:(GenericErrorback)errorback {
    NSArray *photoIDs = [photos valueForKey:@"id"];
    NSDictionary *params = @{@"action":@"add_photos", @"aid": album.id, @"pids": [photoIDs JSONString]};
    [self postPath:@"share" WithParameters:params callback:^(id data){
        callback();
    } errorback:errorback];
}

-(void) uploadPhoto:(Photo*)photo :(GenericCallback)callback :(GenericErrorback)errorback:(ProgressCallback)progressCallback
{
    
    NSString *photoHash = [photo getHash];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:photoHash forKey:@"hash"];
    
    [[PhotoLibraryReader getInstance] getAssetByHash:photoHash callback:^(ALAsset *asset) {
        
        CGFloat quality = [CreditsManager isUnlimitedCreditsPurchased] ? 0.85 : 0.15;
        
        NSNumber* orientationNumber = [asset valueForProperty:ALAssetPropertyOrientation];
        
        UIImageOrientation orientation = UIImageOrientationUp;
        if(orientationNumber) {
            orientation = orientationNumber.intValue;
        }
        
        
        UIImage *assetImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage] scale:1.0 orientation:orientation];
        [assetImage fixOrientation];
        
        NSData *imageData = UIImageJPEGRepresentation(assetImage, quality);
        
        [self uploadFile:@"upload_photo" :params :@"photo_data" :@"dummy_file_name" :imageData :@"image/png" :callback :errorback:progressCallback];
        
    } errorBack:errorback];
}

-(void) downloadURL:(NSString*)url :(GenericCallback)callback :(GenericErrorback)errorback:(ProgressCallback)progressCallback
{
    NSURL *urlObj = [NSURL URLWithString:url];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlObj];
    [self setRequestAuthPayload:request];
    [request setValue:@"1" forHTTPHeaderField:@"X-Download"];
    
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
    {
        dispatch_async(callerQ, ^{
            progressCallback(totalBytesRead, totalBytesExpectedToRead);
        });
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if([operation hasAcceptableStatusCode]) {
            
            dispatch_async(callerQ, ^{
                callback(operation.responseData);
            });
            
        }
        else {
            NSInteger code = [operation.response statusCode];
            NSDictionary *info = [NSDictionary dictionaryWithObject:operation.responseString forKey:@"message"];
            
            NSError *error = [NSError errorWithDomain:@"snappe" code:code userInfo:info];
            NSString *message = [NSString stringWithFormat:@"error in downloading url: %@", url];
            [Logger logError:message :error];
            
            dispatch_async(callerQ, ^{
                errorback(error);
            });
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *message = [NSString stringWithFormat:@"error in downloading url: %@", url];
        [Logger logError:message :error];
        
        dispatch_async(callerQ, ^{
            errorback(error);
        });
    }];
    
    [operation start];
    
}

-(void) downloadPhoto:(Photo*)photo:(GenericCallback)callback :(GenericErrorback)errorback :(ProgressCallback)progressCallback
{
    [self downloadURL:[photo getURL] :callback :errorback :progressCallback];
}

-(void) markNotificationsShown:(NSArray*)shownNotifications :(GenericCallback)callback :(GenericErrorback)errorback 
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"mark_shown" forKey:@"action"];
    
    NSMutableArray *notificationsIDs = [[NSMutableArray alloc] init];
    for(Notification *notification in shownNotifications) {
        [notificationsIDs addObject:notification.id];
    }
    [params setObject:[notificationsIDs JSONString] forKey:@"id"];
    [self postPath:@"notifications" WithParameters:params callback:callback errorback:errorback];
    
}

-(void) markPhotosPrivate:(NSArray*)photos :(VoidCallback)callback :(GenericErrorback)errorback {
    if(photos.count == 0) {
        callback();
        return;
    }
    
    NSArray *photoIDs = [photos valueForKey:@"id"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"mark_photo_private" forKey:@"action"];
    [params setObject:[photoIDs JSONString] forKey:@"pid"];
    
    [self postPath:@"edit_album" WithParameters:params callback:^(id data){
        callback();
    } errorback:errorback];
}

-(void) markPhotosPublic:(NSArray*)photos :(VoidCallback)callback :(GenericErrorback)errorback {
    if(photos.count == 0) {
        callback();
        return;
    }
    
    NSArray *photoIDs = [photos valueForKey:@"id"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"mark_photo_public" forKey:@"action"];
    [params setObject:[photoIDs JSONString] forKey:@"pid"];
    
    [self postPath:@"edit_album" WithParameters:params callback:^(id data){
        callback();
    } errorback:errorback];
}

-(void) updateAlbumName:(NSString*)name :(Album*)album :(GenericCallback)callback :(GenericErrorback)errorback {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"update_album" forKey:@"action"];
    [params setObject:album.id forKey:@"aid"];
    [params setObject:@"name" forKey:@"prop"];
    [params setObject:name forKey:@"val"];
    
    [self getPath:@"edit_album" WithParameters:params callback:callback errorback:errorback];
}

-(void) updateAlbumDescription:(NSString*)description :(Album*)album :(GenericCallback)callback :(GenericErrorback)errorback {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"update_album" forKey:@"action"];
    [params setObject:album.id forKey:@"aid"];
    [params setObject:@"description" forKey:@"prop"];
    [params setObject:description forKey:@"val"];
    
    [self getPath:@"edit_album" WithParameters:params callback:callback errorback:errorback];
}

-(void) deleteObjects:(NSDictionary*)deleteDescriptor:(VoidCallback)callback:(GenericErrorback)errorback {
    if([Util isEmptyDictionary:deleteDescriptor]) {
        callback();
    }
    return;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[deleteDescriptor JSONString] forKey:@"descriptor"];
    [self postPath:@"delete" WithParameters:params callback:callback errorback:errorback];
}

-(void)logLocations:(NSArray*)locations:(GenericCallback)callback:(GenericErrorback)errorback {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    NSMutableArray *locationParams = [[NSMutableArray alloc] init];
    for(CLLocation *loc in locations) {
        NSMutableDictionary *locDict = [NSMutableDictionary dictionary];
        [locDict setObject:[NSNumber numberWithDouble:loc.coordinate.latitude] forKey:@"latitude"];
        [locDict setObject:[NSNumber numberWithDouble:loc.coordinate.longitude] forKey:@"longitude"];
        [locDict setObject:[NSNumber numberWithDouble:[loc.timestamp timeIntervalSince1970]] forKey:@"timestamp"];
        [locationParams addObject:locDict];
    }
    
    [params setObject:[locationParams JSONString] forKey:@"locations"];
    [self postPath:@"log_location" WithParameters:params callback:callback errorback:errorback];
}

-(void)updatePhotoHashes:(NSArray*)hashInfoList:(GenericCallback)callback:(GenericErrorback)errorback {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:[hashInfoList JSONString] forKey:@"hashes"];
    [self postPath:@"update_photo_hash" WithParameters:params callback:callback errorback:errorback];
}

-(void)reportMissingPhotos:(NSArray*)photos {
    NSMutableArray *photoIDs = [NSMutableArray array];
    for(Photo *photo in photos) {
        if(photo.id) {
            [photoIDs addObject:photo.id];
        }
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[photoIDs JSONString] forKey:@"pid"];
    [self postPath:@"report_missing_photo" WithParameters:params callback:^(id data) {
        [Logger track:@"reported_missing_fb_photo" :[NSDictionary dictionaryWithObject:photoIDs forKey:@"pid"]];
    } errorback:^(NSError *error) {
        NSString *message = [NSString stringWithFormat:@"error in reporting missing facebook photos: %@", photoIDs];
        [Logger logError:message :error];
    }];
}

-(void)sendShareNotification:(NSDictionary*)context:(GenericCallback)callback:(GenericErrorback)errorback {
    [self postPath:@"send_share_notification" WithParameters:context callback:callback errorback:errorback];
}

-(void)verifyPurchase:(NSString*)receipt:(GenericCallback)callback:(GenericErrorback)errorback {
    [self getPath:@"purchase" WithParameters:@{@"action": @"verify_purchase", @"receipt": receipt} callback:^(id data)
    {
        BOOL isValid = [[data valueForKey:@"valid"] boolValue];
        callback(@(isValid));
    } errorback:errorback];
}

-(void)parseSuccessOfRequest:(NSURLRequest *)request :(NSHTTPURLResponse *)response :(id)data :(GenericCallback)callback:(GenericErrorback)errorback
{
    //@TODO: check success
    [[SyncManager getInstance] syncWithServerChangeSet:data:^(){
        [[NotificationManager getInstance] updateWithServerNotifications:[data valueForKeyPath:@"notifications"]];
        [CreditsManager updateFromServerJSON:[data valueForKeyPath:@"credits"]];
        callback([data valueForKeyPath:@"data"]);
    }:errorback];
}
@end
