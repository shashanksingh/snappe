//
//  AlbumDownloadOperation.h
//  snappe
//
//  Created by Shashank on 02/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ExtendedOperation.h"
#import "ExtOp.h"
#import "AlbumAccessor.h"
#import "PhotoTransferOperation.h"

@interface AlbumDownloadOperation: PhotoTransferOperation
-(id)initWithAlbums:(Album*)srcAlbum:(NSObject*)destAlbum;
@end
