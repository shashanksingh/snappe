//
//  PhotoAddOperation.h
//  snappe
//
//  Created by Shashank on 07/04/13.
//
//

#import "PhotoTransferOperation.h"
#import "AlbumAccessor.h"

@interface PhotoAddOperation : PhotoTransferOperation
-(id)init:(Album*)album:(NSArray*)photos;
@end
