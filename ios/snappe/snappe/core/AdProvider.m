//
//  AdProvider.m
//  snappe
//
//  Created by Shashank on 12/01/13.
//
//

#import "AdProvider.h"
#import "LocationLogger.h"
#import "UIController.h"
#import "CreditsManager.h"
#import "DFPBannerView.h"
#import "AdDelegate.h"

#define ADMOB_SIMULATOR_TESTING_DEVICE_ID @"90BC6184-524A-5374-BAE6-FE65384DCD00"
#define ADMOB_AD_UNIT_ID @"e8254e409785419c"

@interface AdProvider()
@property (strong) DFPBannerView *adView;
@end

@implementation AdProvider

-(id)init {
    self = [super init];
    if(self) {
        self.adView = [[DFPBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        self.adView.delegate = [AdDelegate getInstance];
        self.adView.adUnitID = ADMOB_AD_UNIT_ID;
        self.adView.rootViewController = [[UIController getInstance] getUIControl:ROOT_VIEW_CONTROLLER];
    }
    return self;
}

+(AdProvider*) getInstance {
    static AdProvider *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AdProvider alloc] init];
    });
    return sharedInstance;
}

+(void)show:(UIView*)parent {
    if(![[CoreController getInstance] canShowAds]) {
        return;
    }
    
    GADRequest *adRequest = [self getAdRequest];
    [[self getInstance].adView loadRequest:adRequest];
    
    [parent addSubview:[self getInstance].adView];
    
    [self animatedShow];
}

+(void)hide {
    [[self getInstance].adView removeFromSuperview];
}

//private
+(void)animatedShow {
    CGRect startFrame = CGRectMake(0, -GAD_SIZE_320x50.height, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    
    CGRect endFrame = startFrame;
    endFrame.origin.y = 0;
    
    [self getInstance].adView.frame = startFrame;
    [UIView animateWithDuration:0.67 animations:^{
        [self getInstance].adView.frame = endFrame;
    }];
}

+(GADRequest*)getAdRequest {
    GADRequest *adRequest = [GADRequest request];
    adRequest.testDevices = [NSArray arrayWithObjects:ADMOB_SIMULATOR_TESTING_DEVICE_ID, nil];
    CLLocation *lastKnownLocation = [LocationLogger getInstance].lastKnownLocation;
    if(lastKnownLocation) {
        [adRequest setLocationWithLatitude:lastKnownLocation.coordinate.latitude longitude:lastKnownLocation.coordinate.longitude accuracy:lastKnownLocation.horizontalAccuracy];
    }
    return adRequest;
}

+(UIView*)getCustomAdView:(CGSize)size {
    GADAdSize customAdSize = GADAdSizeFromCGSize(size);
    DFPBannerView *adView = [[DFPBannerView alloc] initWithAdSize:customAdSize];
    adView.delegate = [AdDelegate getInstance];
    adView.adUnitID = ADMOB_AD_UNIT_ID;
    adView.rootViewController = [[UIController getInstance] getUIControl:ROOT_VIEW_CONTROLLER];
    
    GADRequest *adRequest = [self.class getAdRequest];
    [adView loadRequest:adRequest];
    return adView;
}
@end
