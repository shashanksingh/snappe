//
//  BaseExtOp.h
//  snappe
//
//  Created by Shashank on 14/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExtOp.h"
#import "ExtendedOperationAccessor.h"

@interface BaseExtOp : NSObject<ExtOp>
@property NSString *id;
@property ExtOpStatus status;
@property NSInteger completion;
@property (strong) NSString *error;
@property (strong) NSDate *queuedOn;
@property (strong) NSDate *finishedOn;
@property (strong) NSString *notificationId;

-(NSString*)getStatusText;
-(NSString*)getType;
-(void)fireUpdate;
-(NSString*)getId;
-(void)onFinish:(BOOL)success;
@end
