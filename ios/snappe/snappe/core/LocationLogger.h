//
//  LocationLogger.h
//  snappe
//
//  Created by Shashank on 13/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationLogger : NSObject<CLLocationManagerDelegate>
@property (strong) CLLocation *lastKnownLocation;
+(LocationLogger*) getInstance;
@end
