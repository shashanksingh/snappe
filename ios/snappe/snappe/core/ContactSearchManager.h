//
//  ContactSearchManager.h
//  snappe
//
//  Created by Shashank on 25/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactSearchManager : NSObject
+(ContactSearchManager *) getInstance;
-(NSArray*)search:(NSString*)query :(unsigned int)limit;
@end
