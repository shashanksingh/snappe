//
//  AlbumFBUploadOp.h
//  snappe
//
//  Created by Shashank on 05/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoTransferOperation.h"
#import "AlbumAccessor.h"

@interface AlbumPushOperation : PhotoTransferOperation
-(id)initWithAlbums:(Album*)srcAlbum:(NSObject*)destAlbum;
@end
