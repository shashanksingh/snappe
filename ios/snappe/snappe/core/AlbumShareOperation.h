//
//  AlbumShareOperation.h
//  snappe
//
//  Created by Shashank on 14/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoTransferOperation.h"
#import "AlbumAccessor.h"
#import "UserAccessor.h"
#import "Contact.h"

typedef enum ShareType {
    USER, CONTACT
} ShareType;

@interface AlbumShareOperation : PhotoTransferOperation
-(id)init:(Album*)srcAlbum:(NSDictionary*)changes:(ShareType)shareType;
@end
