//
//  AlbumCreationOperation.h
//  snappe
//
//  Created by Shashank on 27/04/13.
//
//

#import "PhotoTransferOperation.h"

@interface AlbumCreationOperation : PhotoTransferOperation
-(id)init:(NSString*)albumName:(NSArray*)photos:(BOOL)isFacebookAlbum;
@end
