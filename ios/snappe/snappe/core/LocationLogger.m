
//
//  LocationLogger.m
//  snappe
//
//  Created by Shashank on 13/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LocationLogger.h"
#import "SnappeHTTPService.h"
#import "EventManager.h"
#import "Util.h"
#import "Logger.h"
#import "SyncManager.h"

#define MAX_LOG_QUEUE 1000

#define LOCATION_LOGGER_PERSISTENT_FILE_NAME @"location_logger_persistent_data"
#define PERSISTENCE_LAST_KNOWN_LOCATION_KEY @"last_known_location"
#define PERSISTENCE_LOCATION_ARRAY_KEY @"location_array"

#define LOAD_DELAY_AFTER_ACTIVE 60
#define LOGGING_DELAY_AFTER_LOAD 120

@interface LocationLogger()
@property (strong) NSMutableArray *locationArray;
@property (strong) CLLocationManager *locationManager;
@property (strong) NSTimer *loggingTimer;
@property BOOL loggingInProgress;
@end

@implementation LocationLogger
@synthesize locationArray;
@synthesize locationManager;
@synthesize loggingTimer;
@synthesize lastKnownLocation;

-(id)init {
    self = [super init];
    if(self) {
        self.loggingInProgress = NO;
        self.locationArray = [NSMutableArray array];
        [self loadPersistedPendingLocations];
        
        [[EventManager getInstance] addEventListener:APPLICATION_BECAME_ACTIVE :^(va_list args) {
            __weak typeof (self) bself = self;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, LOAD_DELAY_AFTER_ACTIVE * NSEC_PER_SEC), dispatch_get_current_queue(), ^
            {
                [bself loadPersistedPendingLocations];
                if(bself.loggingTimer) {
                    [bself.loggingTimer invalidate];
                }
                bself.loggingTimer = [NSTimer scheduledTimerWithTimeInterval:LOGGING_DELAY_AFTER_LOAD target:bself selector:@selector(logLocationsOnServer) userInfo:nil repeats:YES];
            });
            
            
        }];
        [[EventManager getInstance] addEventListener:APPLICATION_ENTERED_BACKGROUND :^(va_list args) {
            [self.loggingTimer invalidate];
            self.loggingTimer = nil;
            
            [self persistPendingLocations];
        }];
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
    return self;
}

+(LocationLogger*) getInstance {
    static LocationLogger *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LocationLogger alloc] init];
    });
    return sharedInstance;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation 
{
    //no need for synchronization, always called from main thread
    if(oldLocation) {
        [self.locationArray addObject:oldLocation];
    }
    if(newLocation) {
        [self.locationArray addObject:newLocation];
        self.lastKnownLocation = newLocation;
    }
    
    if([self.locationArray count] > MAX_LOG_QUEUE) {
        NSRange rangeToRemove;
        rangeToRemove.location = 0;
        rangeToRemove.length = [self.locationArray count] - MAX_LOG_QUEUE;
        
        [self.locationArray removeObjectsInRange:rangeToRemove];
    }
    
    if([Util appIsInBackground]) {
        [self persistPendingLocations];
    }
}

-(void)logLocationsOnServer {
    if([self.locationArray count] == 0) {
        return;    
    }
    if(self.loggingInProgress) {
        return;
    }
    if([SyncManager getLastSyncTime] == 0) {
        return;
    }
    
    self.loggingInProgress = YES;
    
    NSMutableArray *locationsToLog = [NSMutableArray array];
    
    //no need for synchronization, always called from main thread
    [locationsToLog addObjectsFromArray:self.locationArray];
    
    
    DLog(@"logging %d locations on the server", [locationsToLog count]);
    
    [[SnappeHTTPService getInstance] logLocations:locationsToLog :^(id data) {
        
        //no need for synchronization, always called from main thread
        for(CLLocation *loggedLocation in locationsToLog) {
            [self.locationArray removeObject:loggedLocation];
        }
        
        self.loggingInProgress = NO;
        
        
    } :^(NSError *error) {
        [Logger logError:@"error in logging location on server" :error];
        for(CLLocation *loggedLocation in locationsToLog) {
            [self.locationArray removeObject:loggedLocation];
        }
        
        self.loggingInProgress = NO;
    }];
    
}

-(void)persistPendingLocations {
    NSMutableDictionary *persistentState = [NSMutableDictionary dictionary];
    [persistentState setObject:self.locationArray forKey:PERSISTENCE_LOCATION_ARRAY_KEY];
    if(self.lastKnownLocation) {
        [persistentState setObject:self.lastKnownLocation forKey:PERSISTENCE_LAST_KNOWN_LOCATION_KEY];
    }
    [Util saveObjectInFile:persistentState :LOCATION_LOGGER_PERSISTENT_FILE_NAME];
}

-(void)loadPersistedPendingLocations {
    NSDictionary *persistedState = (NSDictionary*)[Util readObjectFromFile:LOCATION_LOGGER_PERSISTENT_FILE_NAME];
    if(!persistedState || ![persistedState isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    if(!self.lastKnownLocation) {
        
        self.lastKnownLocation = [persistedState objectForKey:PERSISTENCE_LAST_KNOWN_LOCATION_KEY];
    }
    
    NSArray *persistedLocations = [persistedState objectForKey:PERSISTENCE_LOCATION_ARRAY_KEY];
    
    //although we don't need it right now, we still preserve order
    NSUInteger count = [persistedLocations count];
    for(NSInteger i = count - 1; i >= 0; --i) {
        CLLocation *persistedLocation = [persistedLocations objectAtIndex:i];
        if([self.locationArray indexOfObject:persistedLocation] == NSNotFound) {
            [self.locationArray addObject:persistedLocation];
        }
    }
}
@end
