//
//  MissingPhotoReporter.m
//  snappe
//
//  Created by Shashank on 06/01/13.
//
//

#import "MissingPhotoReporter.h"
#import "SnappeHTTPService.h"
#import "Util.h"

#define REPORTING_DELAY 30

@interface MissingPhotoReporter()
@property (strong) NSMutableArray *pendingQueue;
@property (strong) NSTimer *reportingTimer;
@end

@implementation MissingPhotoReporter

-(id)init {
    self = [super init];
    if(self) {
        self.pendingQueue = [NSMutableArray array];
    }
    return self;
}

+(MissingPhotoReporter*) getInstance {
    static MissingPhotoReporter *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MissingPhotoReporter alloc] init];
    });
    return sharedInstance;
}

-(void)reportMissingPhoto:(Photo*)photo {
    @synchronized(self) {
        [self.pendingQueue addObject:photo];
        if([self.pendingQueue count] == 1) {
            [Util executeBlockOnMainQueue:^{
                self.reportingTimer = [NSTimer scheduledTimerWithTimeInterval:REPORTING_DELAY target:self selector:@selector(flushQueue) userInfo:nil repeats:NO];
            }];
        }
    }
    
}

-(void)flushQueue {
    @synchronized(self) {
        if([self.pendingQueue count] == 0) {
            return;
        }
        
        NSArray *toReport = [self.pendingQueue copy];
        self.pendingQueue = [NSMutableArray array];
        
        DLog(@"reporting %d missing photos", [toReport count]);
        [[SnappeHTTPService getInstance] reportMissingPhotos:toReport];
    }
}

@end
