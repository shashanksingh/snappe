//
//  ContactSearchManager.m
//  snappe
//
//  Created by Shashank on 25/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ContactSearchManager.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "Macros.h"
#import "Contact.h"
#import "Util.h"

#define MAX_CONTACTS 2000

@interface ContactSearchManager()
@property (strong) NSArray *contacts;
@end

@implementation ContactSearchManager

-(id) init {
    self = [super init];
    if(self) {
        self.contacts = [self loadContacts];
    }
    return self;
}

+(ContactSearchManager *) getInstance {
    static ContactSearchManager *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ContactSearchManager alloc] init];
    });
    return sharedInstance;
}

-(NSArray*)search:(NSString*)query :(unsigned int)limit {
    if(!self.contacts) {
        DLog(@"address books contacts nil, cannot search");
        return nil;
    }
    
    NSMutableArray *rv = [[NSMutableArray alloc] init];
    for(Contact *contact in self.contacts) {
        if([contact matches:query]) {
            [rv addObject:contact];
        }
        
        if([rv count] == limit) {
            break;
        }
    }
    return rv;
}


-(NSArray*)loadContacts {
    ABAddressBookRef addressBook = ABAddressBookCreate();
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000    
    __block BOOL accessGranted = NO;
    if (ABAddressBookRequestAccessWithCompletion != NULL) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        dispatch_release(sema);   
    }
    else {
        accessGranted = YES;
    }
#else
    BOOL accessGranted = YES;
#endif
    
    if(!accessGranted) {
        NSString *message = @"You need to allow Snappe to access your addressbook to be able to search addresses. Please try again.";
        [Util alertError:nil :message];
        
        if(addressBook) {
            CFRelease(addressBook);
        }
        
        return nil;
    }

    NSArray *contactArray = (NSArray*)CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
    
    NSMutableArray *contacts = [[NSMutableArray alloc] init];
    if(contactArray) {
        for(unsigned int i=0; i<[contactArray count] && i < MAX_CONTACTS; ++i) {
            ABRecordRef contact = (__bridge ABRecordRef)([contactArray objectAtIndex:i]);
            Contact *c = [[Contact alloc] init];
            
            NSString *name = (NSString*)CFBridgingRelease(ABRecordCopyCompositeName(contact));
            c.name = name;
            
            NSMutableArray *emails = [NSMutableArray array];
            
            ABMutableMultiValueRef emailsRef = ABRecordCopyValue(contact, kABPersonEmailProperty);
            
            
            if(ABMultiValueGetCount(emailsRef) > 0) {
                
                for (CFIndex i = 0; i < ABMultiValueGetCount(emailsRef); i++) {
                    
                    CFStringRef emailRef = ABMultiValueCopyValueAtIndex(emailsRef, i);
                    
                    NSString *email = (NSString *)CFBridgingRelease(emailRef);
                    
                    if(email && email.length > 0 && [Util isValidEmail:email]) {
                        [emails addObject:email];
                    }
                }
            }
            c.emails = emails;
            
            if([self isValidContact:c]){
                [contacts addObject:c];
            }
            
            if(emailsRef) {
                CFRelease(emailsRef);    
            }
            
        }
    }
    
    if(addressBook) {
        CFRelease(addressBook);    
    }
    
    return contacts;
}

-(BOOL)isValidContact:(Contact*)c {
    return c.name && c.name.length > 0 && c.emails && c.emails.count > 0;
}


@end
