//
//  AlbumShareOperation.m
//  snappe
//
//  Created by Shashank on 14/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AlbumShareOperation.h"
#import "AlbumAccessor.h"
#import "UserAccessor.h"
#import "EventManager.h"
#import "Util.h"
#import "Logger.h"
#import "PhotoLibraryOrganizer.h"
#import "Macros.h"

#define SRC_ALBUM_ID_KEY @"src_album_id"
#define DEST_USER_ID_KEY @"dest_user_id"
#define DEST_USER_NAME_KEY @"dest_user_name"
#define PHOTO_ID_TO_STATUS_MAP_KEY @"photo_id_to_status"

@interface AlbumShareOperation()
@property ShareType shareType;
@property (retain) Album *src;
@property (retain) NSDictionary *changes;
@end

@implementation AlbumShareOperation

-(id)init:(Album*)srcAlbum:(NSDictionary*)changes:(ShareType)shareType {
    self = [super init];
    if(self) {
        self.shareType = shareType;
        self.src = srcAlbum;
        self.changes = changes;
        
        [self setUp];
    }
    return self;
}

-(id)initWithPersistedState:(NSDictionary*)persistedState {
    //NOT_IMPLEMENTED
    return nil;
}

-(NSArray*)getPhotos {
    return [self.src.photos array];
}

-(NSArray*)getEventTypes {
    return @[@(PHOTO_UPLOAD)];
}

-(NSDictionary*)getState {
    NSMutableDictionary *state = [[super getState] mutableCopy];
    [state setObject:NSStringFromClass(self.class) forKey:@"class"];
    [state setObject:self.src.id forKey:SRC_ALBUM_ID_KEY];
    
    [state setObject:self.changes.allKeys forKey:DEST_USER_ID_KEY];
    [state setObject:[self.changes.allValues valueForKeyPath:@"subject.name"] forKey:DEST_USER_NAME_KEY];
    return state;
}

-(void)start:(GenericCallback)callback {
    DLog(@"starting extop with id: %@", [self getNotification].id);
    self.status = STARTED;
    [self fireUpdate];
    
    GenericCallback onSuccess = ^(id data) {
        [Logger track:@"album_share" :@{@"album_id": self.src.id, @"dest_id": self.changes.allKeys}];
        
        self.status = SUCCESSFUL;
        self.completion = 100;
        self.finishedOn = [NSDate date];
        
        [[PhotoLibraryOrganizer getInstance] sync:^{
            DLog(@"successfully synced with server after album share operation");
            
            callback(@(self.status));
            
            [self onFinish:YES];
            
        } :^(NSError *error) {
            
            [Logger logError:@"error in syncing after AlbumShareOperation" :error];
            
            callback(@(FAILURE));
            
            [self onFinish:NO];
        }];
    };
    
    GenericErrorback onError = ^(NSError *error_) {
        [Logger logError:[NSString stringWithFormat:@"error in sharing album %@ with %@", self.src.id, self.changes.allKeys] :error_];
        //for credits error an alert is already shown
        if(!([error_.domain isEqualToString:SNAPPE_ERROR_DOMAIN] && error_.code==SNAPPE_ERROR_INSUFFICIENT_CREDITS))
        {
            [Util alertError:error_:[NSString stringWithFormat:@"There was an error in sharing album \"%@\". Please try again later", self.src.name]];
        }
        
        self.status = FAILURE;
        self.error = [error_ localizedDescription];
        self.finishedOn = [NSDate date];
        
        callback(@(self.status));
        
        [self onFinish:NO];
        
    };
    
    if(self.shareType == USER) {
        [self.src updateFriendShareState:self.changes.allValues :onSuccess :onError];
    }
    else {
        [self.src shareWithContacts:self.changes.allValues :onSuccess :onError];
    }


}

-(NSString*)getName {
    return [NSString stringWithFormat:@"Sharing album \"%@\" with %@", self.src.name, [self getRecipientsName]];
}

-(NSString*)getType {
    return @"EXTOP_ALBUM_SHARE";
}

-(NSString*)getStatusText {
    NSString *statusText = nil;
    if(self.status == NOT_STARTED) {
        statusText = @"waiting...";    
    }
    else if(self.status == STARTED) {
        statusText = [NSString stringWithFormat:@"upload %d%% completed", MIN(self.completion, 99)];
    }
    else if(self.status == SUCCESSFUL) {
        long secondsSinceDone = [self.finishedOn timeIntervalSince1970];
        statusText = [NSString stringWithFormat:@"successfully uploaded %@", [Util getRelativeTime:secondsSinceDone]];
    }
    else if(self.status == FAILURE) {
        statusText = @"failed, please try again";
    }
    else {
        DLog(@"warning: unknown value for ext op status: %d", self.status);
    }
    return statusText;
}

-(Notification*)getNotification {
    Notification *notification = [super getNotification];
    
    NSMutableDictionary *notificationData = notification.data;
    [notificationData setObject:self.src.id forKey:@"src_album_id"];
    //we need to provide album name so that we can show *some* info even
    //if the album is deleted afterwards
    [notificationData setObject:self.src.name forKey:@"src_album_name"];
    //[notificationData setObject:[self getRecipientsName] forKey:@"dest_user_name"];
    
    return notification;
}

-(NSString*)getRecipientsName {
    if(self.shareType == CONTACT) {
        return [Util getNameForMultipleContacts:[self.changes.allValues valueForKey:@"subject"]];
    }
    return [Util getNameForMultipleUsers:[self.changes.allValues valueForKey:@"subject"]:NO];
}

@end

