//
//  PhotoHashUpdater.m
//  snappe
//
//  Created by Shashank on 13/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoHashUpdater.h"
#import "SnappeHTTPService.h"
#import "Logger.h"
#import "Util.h"

#define PUSH_TIMER_TIMEOUT 300

@interface PhotoHashUpdater()
@property (strong) NSMutableArray *photosToUpdate;
@property (strong) NSTimer *pushTimer;
@end

@implementation PhotoHashUpdater
@synthesize photosToUpdate;
@synthesize pushTimer;

-(id)init {
    self = [super init];
    if(self) {
        self.photosToUpdate = [NSMutableArray array];
        self.pushTimer = nil;
    }
    return self;
}

+(PhotoHashUpdater*) getInstance {
    static PhotoHashUpdater *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PhotoHashUpdater alloc] init];
    });
    return sharedInstance;
}

-(void)add:(NSString*)photoID:(NSString*)hash:(NSString*)pHash {
    NSMutableDictionary *hashInfo = [NSMutableDictionary dictionary];
    hashInfo[@"id"] = photoID;
    if(hash) {
        hashInfo[@"hash"] = hash;
    }
    if(pHash) {
        hashInfo[@"pHash"] = pHash;
    }
    
    @synchronized(self) {
        [self.photosToUpdate addObject:hashInfo];
        if(self.pushTimer) {
            [self.pushTimer invalidate];
        }
        
        [Util executeBlockOnMainQueue:^{
            self.pushTimer = [NSTimer scheduledTimerWithTimeInterval:PUSH_TIMER_TIMEOUT target:self selector:@selector(onPushTimer) userInfo:nil repeats:NO];
        }];
    }

}

-(void)onPushTimer {
    
    @synchronized(self) {
        self.pushTimer = nil;
        
        NSArray *photos = nil;
        
        if(self.photosToUpdate.count == 0) {
            return;
        }
        
        photos = [NSArray arrayWithArray:self.photosToUpdate];
        [self.photosToUpdate removeAllObjects];
        
        //we don't care much if update fails for a few photos, we don't retry
        [[SnappeHTTPService getInstance] updatePhotoHashes:photos :^(id data) {
            [Logger track:@"photo_hash_update" :@{@"count" : @(photos.count)}];
        } :^(NSError *error) {
            [Logger logError:@"error in updating hashes of photos" :error];
        }];
        
    }
}
@end
