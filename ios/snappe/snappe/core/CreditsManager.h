//
//  CreditsManager.h
//  snappe
//
//  Created by Shashank on 12/01/13.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "Macros.h"

@interface CreditsManager : NSObject
+(CreditsManager*)getInstance;

+(NSSet*)getProductIDs;
+(NSSet*)getPurchasedProductIDs;

-(void)getProducts:(ArrayCallback)callback:(GenericErrorback)errorback;
-(void)fetchProducts;
+(void)updateFromServerJSON:(id)serverJSON;

-(void)buy:(SKProduct*)product;
-(void)restore;

+(void)setCreditsCount:(int)count;
+(int)getCreditsCount;
+(NSString*)getNextResetDay;
+(void)decrementCreditsCount;

+(int)getInfinityCreditsValue;

+(BOOL)isAdRemovalPurchased;
+(BOOL)isUnlimitedCreditsPurchased;
+(BOOL)isConsumableProduct:(NSString*)productID;

-(NSString*)getProductName:(NSString*)productID;
@end
