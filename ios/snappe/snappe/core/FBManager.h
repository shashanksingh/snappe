//
//  FBManager.h
//  snappe
//
//  Created by Shashank on 14/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"
#import "AlbumAccessor.h"
#import "PhotoAccessor.h"
#import "Facebook.h"

typedef void(^FBAuthSuccessCallback)(NSString *);
typedef void(^FBAuthFailureCallback)(NSError *);

@interface FBManager : NSObject<FBDialogDelegate>
+(FBManager*) getInstance;

-(BOOL) handleOpenURL:(NSURL *)url;
-(void) handleDidBecomeActive;
-(void) openSession:(BOOL)background;
-(void) authorize:(GenericCallback)callback;
-(NSString*) getAccessToken;
-(NSString*) getUserId;
-(FBGraphObject*)getUserInfo;
-(BOOL) isAuthorized;
-(BOOL) hasUserAuthorizedOnce;

-(void)authorizeWithPublishPermissions:(GenericCallback)callback;

-(void)createAlbum:(NSObject*)source:(GenericCallback)callback:(GenericErrorback)errorback;
-(void)uploadPhotoToAlbum:(Photo*)photo:(NSString*)albumObjectID:(NSInteger)allowedRetries:(GenericCallback)callback:(GenericErrorback)errorback;

-(void)sendUserToUserRequest:(NSArray*)recipientIDs:(NSString*)message:(NSDictionary*)data:(GenericCallback)callback;
-(void)getFriendIDsToInvite:(ArrayCallback)callback:(GenericErrorback)errorback;
-(void)inviteFriends:(NSArray*)friendIDs:(GenericCallback)callback;
@end
