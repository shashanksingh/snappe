//
//  SortManager.m
//  snappe
//
//  Created by Shashank on 21/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SortManager.h"
#import "AlbumAccessor.h"
#import "UserAccessor.h"
#import "SpatioTemporalLocation.h"
#import "Util.h"
#import "GraphManager.h"
#import "Logger.h"
#import "CoreController.h"

#define SAME_TIME_WINDOW_SECONDS 21600

#define SPATIO_TEMPORAL_DISTANCE_KEY @"spatio_temporal"
#define TEMPORAL_DISTANCE_KEY @"temporal"
#define SAME_PHOTO_COUNT_DISTANCE_KEY @"same_photo_count"
#define SIMILAR_PHOTO_COUNT_DISTANCE_KEY @"similar_photo_count"
#define NAME_EDIT_DISTANCE_KEY @"name_edit_distance"

#define SORTING_TYPE_PREF_KEY @"snappe_sorting_type"

//@TODO:clear caches on memory warning

@interface SortManager()
@property (strong) NSMutableDictionary *smartDistanceCache;
@property (strong) NSMutableDictionary *chronologicalDistanceCache;
@property (strong) NSMutableDictionary *averageAlbumTimestampCache;
//this cache is to be accessed from the background thread only
@property (strong) NSMutableDictionary *albumLocationsCache;
@end

@implementation SortManager

-(id)init {
    self = [super init];
    if(self) {
        self.smartDistanceCache = [NSMutableDictionary dictionary];
        self.chronologicalDistanceCache = [NSMutableDictionary dictionary];
        self.averageAlbumTimestampCache = [NSMutableDictionary dictionary];
        self.albumLocationsCache = [NSMutableDictionary dictionary];
    }
    return self;
}

+(SortManager*) getInstance {
    static SortManager *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SortManager alloc] init];
    });
    return sharedInstance;
}

-(void)sort:(NSArray*)friendsAndAlbums :(NSManagedObject*)sortCriterion:(SortingType)type:(BOOL)comparableOnly:(ArrayCallback)callback:(GenericErrorback)errorback
{
    if(type == SMART) {
        [self smartSort:friendsAndAlbums :sortCriterion :comparableOnly:callback:errorback];
    }
    else if(type == DATE) {
        NSArray *rv = [friendsAndAlbums sortedArrayUsingComparator:^NSComparisonResult(NSManagedObject *a, NSManagedObject *b)
        {
            
            NSTimeInterval t1 = 0, t2 = 0;
            if([a isKindOfClass:[Album class]]) {
                t1 = [self getAlbumAverageTimestamp:(Album*)a];
            }
            else {
                t1 = [self getUserLatestTime:(User*)a];
            }
            
            if([b isKindOfClass:[Album class]]) {
                t2 = [self getAlbumAverageTimestamp:(Album*)b];
            }
            else {
                t2 = [self getUserLatestTime:(User*)b];
            }
            
            return t2 - t1;//latest first
            
        }];
        callback(rv);
    }
    else if(type == ALPHABET) {
        NSArray *rv = [friendsAndAlbums sortedArrayUsingComparator:^NSComparisonResult(NSManagedObject *a, NSManagedObject *b) {
            
            NSString *name1 = [a valueForKey:@"name"];
            NSString *name2 = [b valueForKey:@"name"];
            
            if(!name1 && !name2) {
                return 0;
            }
            if(!name1) {
                return 1;
            }
            if(!name2) {
                return -1;
            }
            return [name1 compare:name2];
            
        }];
        callback(rv);
    }
    else {
        NSError *e = [NSError errorWithDomain:SNAPPE_ERROR_DOMAIN code:SNAPPE_ERROR_UNEXPECTED_VALUE userInfo:nil];
        NSString *message = [NSString stringWithFormat:@"warning: unknow sort type: %d", type];
        [Logger logError:message :e];
        errorback(e);
    }
}

-(void)smartSort:(NSArray*)friendsAndAlbums :(NSManagedObject*)sortCriterion:(BOOL)comparableOnly:(ArrayCallback)callback:(GenericErrorback)errorback
{
    
    dispatch_queue_t callerQ = dispatch_get_current_queue();
    
    NSMutableArray *albumIDs = [NSMutableArray array];
    NSMutableArray *userIDs = [NSMutableArray array];
    NSMutableDictionary *objectIDToObject = [NSMutableDictionary dictionary];
    
    for(NSManagedObject *albumOrUser in friendsAndAlbums) {
        NSManagedObjectID *objectID = albumOrUser.objectID;
        [objectIDToObject setObject:albumOrUser forKey:objectID];
        
        if([albumOrUser isKindOfClass:[Album class]]) {
            [albumIDs addObject:objectID];
        }
        else if([albumOrUser isKindOfClass:[User class]]) {
            [userIDs addObject:objectID];
        }
        else {
            [Logger logError:[NSString stringWithFormat:@"unknown managedobject type %@",[albumOrUser class]] :nil];
        }
    }

    [objectIDToObject setObject:sortCriterion forKey:sortCriterion.objectID];
    if([sortCriterion isKindOfClass:[Album class]]) {
        [albumIDs addObject:sortCriterion.objectID];
        
    }
    else if([sortCriterion isKindOfClass:[User class]]) {
        [userIDs addObject:sortCriterion.objectID];
    }
    else {
        [Logger logError:[NSString stringWithFormat:@"unknown sortCriterion type %@",[sortCriterion class]] :nil];
    }

    dispatch_async([CoreController getCoreDataBackgroundQueue], ^{
        
        NSArray *albums = [albumIDs count] == 0 ? @[] : [GraphManager changeContext:@"Album" :albumIDs :nil];
        NSArray *users = [userIDs count] == 0 ? @[] : [GraphManager changeContext:@"User" :userIDs :nil];
        
        DLog(@"avg ts cache start");NSDate *t1 = [NSDate date];
        [self cacheAlbumAverageTimestamps:albums];
        DLog(@"avg ts cache end; time: %f", [[NSDate date] timeIntervalSinceDate:t1]);
        
        DLog(@"location cache start");t1 = [NSDate date];
        [self cacheAlbumLocations:albums];
        DLog(@"location cache end; time: %f", [[NSDate date] timeIntervalSinceDate:t1]);
        
        NSMutableSet *uncomparables = [[NSMutableSet alloc] init];
        for(NSManagedObject *obj in friendsAndAlbums) {
            NSDictionary *dist = [self getSmartDistance:obj :sortCriterion];
            if([self isUncomparableDistance:dist]) {
                [uncomparables addObject:obj];
            }
        }
        
        NSArray *comparableFriendsAndAlbums = [friendsAndAlbums filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSManagedObject *o, NSDictionary *bindings) {
            return ![uncomparables containsObject:o];
        }]];
        
        NSMutableArray *rv = [[comparableFriendsAndAlbums sortedArrayUsingComparator:^NSComparisonResult(NSManagedObject *obj1, NSManagedObject *obj2)
                               {
                                   //these distances are already calculated and cached above
                                   NSDictionary *dist1 = [self getSmartDistance:obj1 :sortCriterion];
                                   NSDictionary *dist2 = [self getSmartDistance:obj2 :sortCriterion];
                                   
                                   return [self compareDistances:dist1 :dist2];
                                   
                               }] mutableCopy];
        
        if(!comparableOnly) {
            [rv addObjectsFromArray:[uncomparables allObjects]];
        }
        
        NSArray *sortedObjectIDs = [rv valueForKey:@"objectID"];
        dispatch_async(callerQ, ^{
            NSMutableArray *sortedResult = [NSMutableArray array];
            for(NSManagedObjectID *objectID in sortedObjectIDs) {
                NSManagedObject *obj = [objectIDToObject objectForKey:objectID];
                [sortedResult addObject:obj];
            }
            callback(sortedResult);
        });
        
    });
}

-(NSMutableDictionary*)getDefaultDistance {
    NSMutableDictionary *rv = [NSMutableDictionary dictionary];
    
    [rv setObject:[NSNumber numberWithDouble:INFINITY] forKey:SPATIO_TEMPORAL_DISTANCE_KEY];
    [rv setObject:[NSNumber numberWithDouble:INFINITY] forKey:TEMPORAL_DISTANCE_KEY];
    [rv setObject:[NSNumber numberWithDouble:INFINITY] forKey:SAME_PHOTO_COUNT_DISTANCE_KEY];
    [rv setObject:[NSNumber numberWithDouble:INFINITY] forKey:SIMILAR_PHOTO_COUNT_DISTANCE_KEY];
    [rv setObject:[NSNumber numberWithDouble:INFINITY] forKey:NAME_EDIT_DISTANCE_KEY];
    
    return rv;
}

-(NSInteger)compareDistances:(NSDictionary*)aDist:(NSDictionary*)bDist {
    double spatioTemporal1 = [[aDist objectForKey:SPATIO_TEMPORAL_DISTANCE_KEY] doubleValue];
    double spatioTemporal2 = [[bDist objectForKey:SPATIO_TEMPORAL_DISTANCE_KEY] doubleValue];
    if(spatioTemporal1 != INFINITY && spatioTemporal2 != INFINITY) {
        return spatioTemporal1 - spatioTemporal2;
    }
    if(spatioTemporal1 != INFINITY) {
        return -1;
    }
    if(spatioTemporal2 != INFINITY) {
        return 1;
    }
    
    double nameEditDist1 = [[aDist objectForKey:NAME_EDIT_DISTANCE_KEY] doubleValue];
    double nameEditDist2 = [[bDist objectForKey:NAME_EDIT_DISTANCE_KEY] doubleValue];
    if(nameEditDist1 != INFINITY && nameEditDist2 != INFINITY) {
        return nameEditDist1 - nameEditDist2;
    }
    if(nameEditDist1 != INFINITY) {
        return -1;
    }
    if(nameEditDist2 != INFINITY) {
        return 1;
    }
    
    NSTimeInterval temporal1 = [[aDist objectForKey:TEMPORAL_DISTANCE_KEY] doubleValue];
    NSTimeInterval temporal2 = [[bDist objectForKey:TEMPORAL_DISTANCE_KEY] doubleValue];
    if(temporal1 != INFINITY && temporal2 != INFINITY) {
        return temporal1 - temporal2;
    }
    if(temporal1 != INFINITY) {
        return -1;
    }
    if(temporal2 != INFINITY) {
        return 1;
    }
    
    double samePhotoDist1 = [[aDist objectForKey:SAME_PHOTO_COUNT_DISTANCE_KEY] doubleValue];
    double samePhotoDist2 = [[bDist objectForKey:SAME_PHOTO_COUNT_DISTANCE_KEY] doubleValue];
    if(samePhotoDist1 != INFINITY && samePhotoDist2 != INFINITY) {
        return samePhotoDist1 - samePhotoDist2;
    }
    if(samePhotoDist1 != INFINITY) {
        return -1;
    }
    if(samePhotoDist2 != INFINITY) {
        return 1;
    }
    
    double similarPhotoDist1 = [[aDist objectForKey:SIMILAR_PHOTO_COUNT_DISTANCE_KEY] doubleValue];
    double similarPhotoDist2 = [[bDist objectForKey:SIMILAR_PHOTO_COUNT_DISTANCE_KEY] doubleValue];
    if(similarPhotoDist1 != INFINITY && similarPhotoDist2 != INFINITY) {
        return similarPhotoDist1 - similarPhotoDist2;
    }
    if(similarPhotoDist1 != INFINITY) {
        return -1;
    }
    if(similarPhotoDist2 != INFINITY) {
        return 1;
    }
    
    return 0;
}

-(BOOL) isUncomparableDistance:(NSDictionary*)distance {
    for(NSString *key in distance) {
        if([[distance objectForKey:key] doubleValue] != INFINITY) {
            return NO;
        }
    }
    return YES;
}

-(NSDictionary*)getSmartDistance:(NSManagedObject*)a:(NSManagedObject*)b {
    NSString *cacheKey = [self getCacheKey:a :b];
    NSDictionary *cachedValue = [self.smartDistanceCache objectForKey:cacheKey];
    if(!cachedValue) {
        NSDictionary *rv = nil;
        if([a isKindOfClass:[User class]] && [b isKindOfClass:[Album class]]) {
            rv = [self getUserAlbumDistance:(User*)a:(Album*)b];
        }
        else if([a isKindOfClass:[Album class]] && [b isKindOfClass:[User class]]) {
            rv = [self getUserAlbumDistance:(User*)b:(Album*)a];
        }
        else if([a isKindOfClass:[Album class]] && [b isKindOfClass:[Album class]]) {
            rv = [self getAlbumAlbumDistance:(Album*)a:(Album*)b];
        }
        else {
            DLog(@"warning: unexpected combination of parameters:: a:%@, b:%@", [a class], [b class]);
        }
        
        cachedValue = rv;
        [self.smartDistanceCache setObject:cachedValue forKey:cacheKey];
    }
    
    return cachedValue;
    
}

-(NSDictionary*)getUserAlbumDistance:(User*)user:(Album*)album {
    NSArray *albumLocations = [self getAlbumLocations:album];
    NSArray *userTaggedLocations = [user getTaggedLocations];
    //@TODO: past current locations?
    Location *userDefaultLocation = user.currentLocation;
    
    double averageDistance = [self getAverageDistance:albumLocations :userTaggedLocations :userDefaultLocation];
    
    NSMutableDictionary *rv = [self getDefaultDistance];
    [rv setObject:[NSNumber numberWithDouble:averageDistance] forKey:SPATIO_TEMPORAL_DISTANCE_KEY];
    
    NSTimeInterval temporalDistance = [self getUserAlbumChronologicalDistance:user :album];
    [rv setObject:[NSNumber numberWithDouble:temporalDistance] forKey:TEMPORAL_DISTANCE_KEY];
    
    double editDistance = [self getEditDistance:user.name :album.name];
    [rv setObject:[NSNumber numberWithDouble:editDistance] forKey:NAME_EDIT_DISTANCE_KEY];
    
    return rv;
}

-(NSDictionary*)getAlbumAlbumDistance:(Album*)a :(Album*)b {
    BOOL pause = [a.name isEqualToString:b.name];
    NSArray *aLocations = [self getAlbumLocations:a];
    NSArray *bLocations = [self getAlbumLocations:b];
    
    BOOL aIsShorter = [aLocations count] <= [bLocations count];
    NSArray *shorterLocations = aIsShorter ? aLocations : bLocations;
    NSArray *longerLocations = aIsShorter ? bLocations : aLocations;
    
    double averageDistance = [self getAverageDistance:shorterLocations :longerLocations :nil];
    
    NSTimeInterval temporalDistance = [self getAlbumAlbumChronologicalDistance:a :b];
    
    //metric: total comparisons - total matches
    int totalPhotos = [a.photos count] + [b.photos count];
    
    int samePhotoCount = [a getSamePhotoCount:b];
    double samePhotoDistance = INFINITY;
    if(samePhotoCount >= 0) {
        samePhotoDistance = totalPhotos - samePhotoCount;
    }
    
    int similarPhotoCount = [a getSimilarPhotoCount:b];
    double similarPhotoDistance = INFINITY;
    if(similarPhotoCount >= 0) {
        similarPhotoDistance = totalPhotos - similarPhotoCount;
    }
    
    double editDistance = [self getEditDistance:a.name :b.name];
    
    NSMutableDictionary *rv = [self getDefaultDistance];
    [rv setObject:[NSNumber numberWithDouble:averageDistance] forKey:SPATIO_TEMPORAL_DISTANCE_KEY];
    [rv setObject:[NSNumber numberWithDouble:temporalDistance] forKey:TEMPORAL_DISTANCE_KEY];
    [rv setObject:[NSNumber numberWithDouble:samePhotoDistance] forKey:SAME_PHOTO_COUNT_DISTANCE_KEY];
    [rv setObject:[NSNumber numberWithDouble:similarPhotoDistance] forKey:SIMILAR_PHOTO_COUNT_DISTANCE_KEY];
    [rv setObject:[NSNumber numberWithDouble:editDistance] forKey:NAME_EDIT_DISTANCE_KEY];
    
    return rv;
}

-(SpatioTemporalLocation*)closestLocationInTime:(SpatioTemporalLocation*)needle :(NSArray*)sortedHaystack {
    unsigned int count = [sortedHaystack count];
    if(count == 0) {
        return nil;
    }
    
    NSUInteger closestIndex = [sortedHaystack indexOfObject:needle inSortedRange:NSMakeRange(0, count) options:NSBinarySearchingLastEqual | NSBinarySearchingInsertionIndex usingComparator:^NSComparisonResult(SpatioTemporalLocation *obj1, SpatioTemporalLocation *obj2) {
        return obj1.timestamp - obj2.timestamp;
    }];
    
    closestIndex = MIN(closestIndex, count - 1);
    return [sortedHaystack objectAtIndex:closestIndex];
}

-(double)getAverageDistance:(NSArray*)aLocations :(NSArray*)bLocations :(Location*)bDefault {
    bLocations = [bLocations sortedArrayUsingComparator:^NSComparisonResult(SpatioTemporalLocation *obj1, SpatioTemporalLocation *obj2) {
        return obj1.timestamp - obj2.timestamp;
    }];
    
    NSMutableArray *distances = [[NSMutableArray alloc] init];
    for(SpatioTemporalLocation *loc in aLocations) {
        SpatioTemporalLocation *closestInTime = [self closestLocationInTime:loc :bLocations];
        
        Location *closestInTimeLocation = nil;
        if(closestInTime) {
            if(abs(loc.timestamp - closestInTime.timestamp) < SAME_TIME_WINDOW_SECONDS) {
                closestInTimeLocation = closestInTime.location;
            }
        }
        
        if(!closestInTimeLocation) {
            closestInTimeLocation = bDefault;
        }
        
        if(closestInTimeLocation) {
            double distance = [loc.location distance:closestInTimeLocation];
            [distances addObject:[NSNumber numberWithDouble:distance]];    
        }
        
    }
    
    if([distances count] == 0) {
        return INFINITY;
    }
    
    double totalDistance = [[distances valueForKeyPath:@"@sum.self"] doubleValue];
    double averageDistance = totalDistance/[distances count];
    return averageDistance;
}
           
-(double)getEditDistance:(NSString*)a:(NSString*)b {
    if(!a || !b) {
        return INFINITY;
    }
    
    NSArray *aNGrams = [Util getNGrams:a :3];
    
    NSArray *bNGrams = [Util getNGrams:b :3];
    NSSet *bNGramSet = [NSSet setWithArray:bNGrams];
    
    int count = 0;
    for(NSString *aNGram in aNGrams) {
        if([bNGramSet containsObject:aNGram]) {
            count++;
        }
    }
    
    if(count == 0) {
        return INFINITY;
    }
    return MAX(0, (([aNGrams count] + [bNGrams count])/count) - 2);
}

-(NSArray*)chronologicalSort:(NSArray*)friendsAndAlbums :(NSManagedObject*)sortCriterion:(BOOL)comparableOnly 
{
    if(comparableOnly) {
        NSMutableSet *comparableIds = [[NSMutableSet alloc] init];
        for(NSManagedObject *o in friendsAndAlbums) {
            NSTimeInterval dist = [self getChronologicalDistance:o :sortCriterion];
            if(dist != INFINITY) {
                [comparableIds addObject:[o valueForKey:@"id"]];
            }
        }
        
        friendsAndAlbums = [friendsAndAlbums filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSManagedObject *o, NSDictionary *bindings) {
            return [comparableIds containsObject:[o valueForKey:@"id"]];
        }]];
    }
    
    
    
    return [friendsAndAlbums sortedArrayUsingComparator:^NSComparisonResult(NSManagedObject *a, NSManagedObject *b) 
    {
        NSTimeInterval dist1 = [self getChronologicalDistance:a :sortCriterion];
        NSTimeInterval dist2 = [self getChronologicalDistance:b :sortCriterion];
        
        return dist1 - dist2;
        
    }];
    
}

-(NSDictionary*) getAlbumAverageTimestamps:(NSArray*)albumIDs {
    NSPredicate *albumFilterPredicate = [NSPredicate predicateWithFormat:@"album.id IN %@", albumIDs];
    NSDictionary *avgTimestapByAlbumID = [GraphManager groupByAggregate:@"Photo" :@"average:" :@"clickedOn" :@"album.id" :albumFilterPredicate];
    
    NSMutableDictionary *rv = [NSMutableDictionary dictionary];
    for(NSString *albumID in albumIDs) {
        NSNumber *avgTimestamp = [avgTimestapByAlbumID objectForKey:albumID];
        if(!avgTimestamp) {
            avgTimestamp = [NSNumber numberWithDouble:INFINITY];
        }
        [rv setObject:avgTimestamp forKey:albumID];
    }
    return rv;
}

-(void) cacheAlbumAverageTimestamps:(NSArray*)albums {
    NSMutableArray *albumIDs = [NSMutableArray array];
    for(Album *album in albums) {
        if(![self.averageAlbumTimestampCache objectForKey:album.id]) {
            [albumIDs addObject:album.id];
        }
    }
    
    NSDictionary *albumIDToAvgTimestamp = [self getAlbumAverageTimestamps:albumIDs];
    [self.averageAlbumTimestampCache addEntriesFromDictionary:albumIDToAvgTimestamp];
    
}

-(NSTimeInterval)getAlbumAverageTimestamp:(Album*)a {
    NSNumber *avgTimestamp = [self.averageAlbumTimestampCache objectForKey:a.id];
    if(!avgTimestamp) {
        avgTimestamp = [[self getAlbumAverageTimestamps:@[a.id]] objectForKey:a.id];
        [self.averageAlbumTimestampCache setObject:avgTimestamp forKey:a.id];
    }
    if(a.modifiedOn) {
        return [a.modifiedOn timeIntervalSinceReferenceDate];
    }
    if(a.createdOn) {
        return [a.createdOn timeIntervalSinceReferenceDate];
    }
    return [avgTimestamp doubleValue];
}

-(NSDictionary*)getAlbumsLocations:(NSArray*)albumIDs {
    NSManagedObjectContext *context = [[GraphManager getInstance] getPersistentManagedObjectContext];
    
    NSPredicate *albumFilterPredicate = [NSPredicate predicateWithFormat:@"id IN %@", albumIDs];
    
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:[NSEntityDescription entityForName:@"Album" inManagedObjectContext:context]];
    [request setIncludesPropertyValues:YES];
    [request setPredicate:albumFilterPredicate];
    [request setRelationshipKeyPathsForPrefetching:@[@"photos", @"photos.location"]];
    
    NSError* error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if(error) {
        [Logger logError:@"error in getting groupByAggregate" :error];
        return nil;
    }
    
    NSMutableDictionary *rv = [NSMutableDictionary dictionary];
    for(Album *album in results) {
        NSString *albumID = album.id;
        NSMutableArray *albumLocations = [rv objectForKey:albumID];
        if(!albumLocations) {
            albumLocations = [NSMutableArray array];
            [rv setObject:albumLocations forKey:albumID];
        }
        
        for(Photo *photo in album.photos) {
            Location *location = photo.location ? photo.location : album.location;
            if(location) {
                SpatioTemporalLocation *stl = [[SpatioTemporalLocation alloc] init:location :photo.clickedOn];
                [albumLocations addObject:stl];
            }
        }
    }
    return rv;
}

-(void)cacheAlbumLocations:(NSArray*)albums {
    NSMutableArray *albumIDs = [NSMutableArray array];
    for(Album *album in albums) {
        if(![self.albumLocationsCache objectForKey:album.id]) {
            [albumIDs addObject:album.id];
        }
    }
    
    NSDictionary *albumIDToLocations = [self getAlbumsLocations:albumIDs];
    [self.albumLocationsCache addEntriesFromDictionary:albumIDToLocations];
}

-(NSArray*)getAlbumLocations:(Album*)a {
    NSArray *locations = [self.albumLocationsCache objectForKey:a.id];
    if(!locations) {
        locations = [[self getAlbumsLocations:@[a.id]] objectForKey:a.id];
        [self.albumLocationsCache setObject:locations forKey:a.id];
    }
    return locations;
}

-(NSTimeInterval)getChronologicalDistance:(NSManagedObject*)a:(NSManagedObject*)b {
    
    NSString *cacheKey = [self getCacheKey:a :b];
    NSNumber *cachedValue = [self.chronologicalDistanceCache objectForKey:cacheKey];
    if(!cachedValue) {
        NSTimeInterval rv = INFINITY;
        if([a isKindOfClass:[User class]] && [b isKindOfClass:[Album class]]) {
            rv = [self getUserAlbumChronologicalDistance:(User*)a:(Album*)b];
        }
        else if([a isKindOfClass:[Album class]] && [b isKindOfClass:[User class]]) {
            rv = [self getUserAlbumChronologicalDistance:(User*)b:(Album*)a];
        }
        else if([a isKindOfClass:[Album class]] && [b isKindOfClass:[Album class]]) {
            rv = [self getAlbumAlbumChronologicalDistance:(Album*)a:(Album*)b];
        }
        else {
            DLog(@"warning: unexpected combination of parameters:: a:%@, b:%@", [a class], [b class]);
        }
        
        cachedValue = [NSNumber numberWithDouble:rv];
        [self.chronologicalDistanceCache setObject:cachedValue forKey:cacheKey];
    }
    
    return [cachedValue doubleValue];
}

-(NSTimeInterval)getUserAlbumChronologicalDistance:(User*)user:(Album*)album {
    NSTimeInterval albumAverageTimestamp = [self getAlbumAverageTimestamp:album];
    if(albumAverageTimestamp == INFINITY) {
        return INFINITY;
    }
    
    NSTimeInterval minTimeDelta = INFINITY;
    for(Tag *tag in user.tag) {
        int absDelta = abs([tag.timestamp timeIntervalSince1970] - albumAverageTimestamp);
        if(absDelta < minTimeDelta) {
            minTimeDelta = absDelta;
        }
    }
    
    return minTimeDelta;
}

-(NSTimeInterval)getAlbumAlbumChronologicalDistance:(Album*)a1:(Album*)a2 {
    NSTimeInterval albumAverageTimestamp1 = [self getAlbumAverageTimestamp:a1];
    NSTimeInterval albumAverageTimestamp2 = [self getAlbumAverageTimestamp:a2];
    
    return abs(albumAverageTimestamp1 - albumAverageTimestamp2);
}

-(NSTimeInterval)getUserLatestTime:(User*)user {
    NSTimeInterval maxTime = 0;
    for(Tag *tag in user.tag) {
        int t = [tag.timestamp timeIntervalSince1970];
        if(t > maxTime) {
            maxTime = t;
        }
    }
    
    return maxTime;
}

-(NSString*)getCacheKey:(NSManagedObject*)a:(NSManagedObject*)b {
    NSArray *ids = [NSArray arrayWithObjects:[a valueForKey:@"id"], [b valueForKey:@"id"], nil];
    ids = [ids sortedArrayUsingSelector:@selector(compare:)];
    
    NSString *cacheKey = [ids componentsJoinedByString:@"-"];
    return cacheKey;
}

+(SortingType)getSortingTypePreference {
    NSString *pref = [Util getUserDefault:SORTING_TYPE_PREF_KEY];
    if(!pref) {
        return SMART; 
    }
    if([pref isEqualToString:@"SMART"]) {
        return SMART;
    }
    else if([pref isEqualToString:@"DATE"]) {
        return DATE;
    }
    else if([pref isEqualToString:@"ALPHABET"]) {
        return ALPHABET;
    }
    else {
        DLog(@"warn: unknow sorting type pref value: %@", pref);
    }
    return SMART;
}

+(void)setSortingTypePreference:(SortingType)sortingType {
    NSString *val = @"SMART";
    if(sortingType == DATE) {
        val = @"DATE";
    }
    else if(sortingType == ALPHABET) {
        val = @"ALPHABET";
    }
    else {
        DLog(@"warn: unknow sorting type value: %d", sortingType);
    }
    
    [Util setUserDefault:val forKey:SORTING_TYPE_PREF_KEY];
}

@end
