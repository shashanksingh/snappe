//
//  HawaiiAlbum.h
//  snappe
//
//  Created by Shashank on 15/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Album.h"

@interface BaseFakeAlbum : NSObject
@property (strong) NSString *id;
-(NSDictionary *) getInfo;
-(NSString *) getDescription;
-(NSString *) getName;
-(NSUInteger) getPhotoCount;
-(UIImage *) getPhotoAtIndex:(NSUInteger) index;
-(BOOL) isOwnedByCurrentUser;
@end


@interface HawaiiAlbum : BaseFakeAlbum
@end


@interface MaleAlbum : BaseFakeAlbum
@end


@interface FemaleAlbum : BaseFakeAlbum
@end

@interface CreationCandidateAlbum : BaseFakeAlbum
-(id)initWithName:(NSString*)name;
@end
