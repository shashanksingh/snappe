//
//  HawaiiAlbum.m
//  snappe
//
//  Created by Shashank on 15/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FakeAlbum.h"
#import "PhotoAccessor.h"

@interface BaseFakeAlbum ()
@property (strong) NSDictionary *info;
@end

@implementation BaseFakeAlbum
@synthesize info;

-(id) init {
    self = [super init];
    if(self) {
        
    }
    return self;
}

-(NSDictionary *) getInfo {
    return nil;
}

-(NSString *) getDescription {
    return [self.info objectForKey:@"description"];
}

-(NSString *) getName {
    return  [self.info objectForKey:@"name"];
}

-(NSUInteger) getPhotoCount {
    return [[self.info objectForKey:@"photos"] count];
}

-(UIImage *) getPhotoAtIndex:(NSUInteger) index {
    return [[self.info objectForKey:@"photos"] objectAtIndex:index];
}

-(BOOL) isOwnedByCurrentUser {
    return NO;
}

@end


@implementation HawaiiAlbum
-(NSDictionary *) getInfo {
    NSArray *photos = @[
        [Photo fromImageResource:@"hawaii-1.jpeg"],
        [Photo fromImageResource:@"hawaii-2.jpeg"],
        [Photo fromImageResource:@"hawaii-3.jpeg"]
    ];
    
    return @{
            @"name" : @"Awesome Hawaii",
            @"description": @"Awesome Trip to Hawaii. Breathtaking photos!",
            @"photos": photos,
    };
}
@end

@implementation MaleAlbum
-(NSDictionary *) getInfo {
    NSArray *photos = @[
        [Photo fromImageResource:@"male-1.jpeg"],
        [Photo fromImageResource:@"male-2.jpeg"],
        [Photo fromImageResource:@"male-3.jpeg"]
    ];
    return @{
            @"name": @"If Only I worked Out Regularly!",
            @"description": @"",
            @"photos": photos
    };
}
@end

@implementation FemaleAlbum
-(NSDictionary *) getInfo {
    NSArray *photos = @[
        [Photo fromImageResource:@"female-1.jpeg"],
        [Photo fromImageResource:@"female-2.jpeg"],
        [Photo fromImageResource:@"female-3.jpeg"]
    ];
    return @{
            @"name": @"Weekend!",
            @"description": @"",
            @"photos": photos
    };
}
@end

@interface CreationCandidateAlbum()
@property (strong) NSString *name;
@end

@implementation CreationCandidateAlbum

-(id)initWithName:(NSString*)name {
    self = [super init];
    if(self) {
        self.name = name;
    }
    return self;
}

-(NSDictionary *) getInfo {
    NSArray *photos = @[[Photo fromImageResource:@"create_new_album_icon.png"]];
    return @{
             @"name": self.name,
             @"description": @"",
             @"photos": photos
    };
}



@end
