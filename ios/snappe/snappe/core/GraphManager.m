//
//  GraphManager.m
//  snappe
//
//  Created by Shashank on 15/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GraphManager.h"
#import "FBManager.h"
#import "UserAccessor.h"
#import "AlbumAccessor.h"
#import "PhotoAccessor.h"
#import "Util.h"
#import "CoreController.h"
#import "PhotoLibraryReader.h"
#import "Logger.h"
#import "CoreController.h"
#import "NSManagedObjectContext+MagicalRecord.h"

#define MAX_BATCH_SIZE 950

static const NSString *SNAPPE_DISPATCH_QUEUE_COREDATA_CONTEXT_SYNCHRONIZATION_LOCK = @"snappe_dispatch_queue_coredata_context_lock";
static NSMutableArray *prefetchCache;

@interface GraphManager()
@property (strong) NSMutableDictionary *customQueueContext;
@end

@implementation GraphManager

-(id)init {
    self = [super init];
    if(self) {
        self.customQueueContext = [NSMutableDictionary dictionary];
    }
    return self;
}

+(GraphManager *) getInstance {
    static GraphManager *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[GraphManager alloc] init];
        prefetchCache = [NSMutableArray array];
    });
    return instance;
}


+(id) getGraphObjectById:(NSString*)id WithEntityName:(NSString*)entityName {
    NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:id, @"id", nil];
    NSArray *fetchedObjects = [self getGraphObjectsByProperties:properties :entityName];
    if([fetchedObjects count] == 0) return nil;
    return [fetchedObjects objectAtIndex:0];
}

+(NSDictionary*) getGraphObjectsById:(NSArray*)idList WithEntityName:(NSString*)entityName {
    NSManagedObjectContext *context = [[GraphManager getInstance] getPersistentManagedObjectContext];
    
    NSMutableDictionary *rv = [NSMutableDictionary dictionary];
    
    for(NSArray *idSubList in [Util splitArray:idList :MAX_BATCH_SIZE]) {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setResultType: NSManagedObjectResultType];
        [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
        [request setReturnsObjectsAsFaults:NO];
        //[request setPropertiesToFetch:@[@"id"]];
        
        NSMutableArray *predicateFormatFrags = [[NSMutableArray alloc] init];
        NSMutableArray *predicateFormatArgs = [[NSMutableArray alloc] init];
        for(NSString *idValue in idSubList) {
            [predicateFormatArgs addObject:idValue];
            [predicateFormatFrags addObject:@"id == %@"];
        }
        NSString *predicateFormat = [predicateFormatFrags componentsJoinedByString:@" OR "];
        
        if([predicateFormat length] > 0) {
            [request setPredicate:[NSPredicate predicateWithFormat:predicateFormat argumentArray:predicateFormatArgs]];
        }
        
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:request error:&error];
        if(fetchedObjects == nil) {
            [Logger logError:[NSString stringWithFormat:@"error getting %@ by id", entityName] :error];
            return nil;
        }
        
        
        for(NSManagedObject *fetchedObject in fetchedObjects) {
            [rv setObject:fetchedObject forKey:[fetchedObject valueForKey:@"id"]];
        }
    }
    
    
    return rv;
}

+(NSArray*) getGraphObjectsByProperties:(NSDictionary*)properties :(NSString*)entityName {
    //@TODO: check if the property is indexed
    //@TODO: use compound predicate
    NSManagedObjectContext *context = [[GraphManager getInstance] getPersistentManagedObjectContext];

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setResultType: NSManagedObjectResultType];
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    [request setReturnsObjectsAsFaults:NO];
    [request setIncludesPropertyValues:YES];
    
    NSMutableArray *predicateFormatFrags = [[NSMutableArray alloc] init];
    NSMutableArray *predicateFormatArgs = [[NSMutableArray alloc] init];
    for(NSString *key in properties) {
        id value = [properties valueForKey:key];
        [predicateFormatArgs addObject:key];
        [predicateFormatArgs addObject:value];
        [predicateFormatFrags addObject:@"%K == %@"];
    }
    NSString *predicateFormat = [predicateFormatFrags componentsJoinedByString:@" AND "];
    
    if([predicateFormat length] > 0) {
        [request setPredicate:[NSPredicate predicateWithFormat:predicateFormat argumentArray:predicateFormatArgs]];
    }

    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:request error:&error];
    if(fetchedObjects == nil) {
        [Logger logError:[NSString stringWithFormat:@"error getting %@ by %@", entityName, properties] :error];
        return nil;
    }
    
    return fetchedObjects;
}

-(NSArray*)searchGraphObjects:(NSDictionary*)ands :(NSString*)entityName {
    NSManagedObjectContext *context = [[GraphManager getInstance] getPersistentManagedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setResultType: NSManagedObjectResultType];
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    
    NSMutableArray *andPerdicates = [NSMutableArray array];
    for(NSString *value in ands) {
        NSMutableArray *ors = [NSMutableArray array];
        
        NSArray *properties = ands[value];
        for(NSString *property in properties) {
            NSPredicate *subPredicate = [NSPredicate predicateWithFormat:@"%K BEGINSWITH[cd] %@", property, value];
            [ors addObject:subPredicate];
        }
        
        NSPredicate *and = [NSCompoundPredicate orPredicateWithSubpredicates:ors];
        [andPerdicates addObject:and];
    }
    
    NSPredicate *filter = [NSCompoundPredicate andPredicateWithSubpredicates:andPerdicates];
    [request setPredicate:filter];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:request error:&error];
    if(fetchedObjects == nil) {
        [Logger logError:[NSString stringWithFormat:@"error getting %@ by %@", entityName, filter] :error];
        return nil;
    }
    
    return fetchedObjects;
}


+(BOOL) isManagedObjectTransient:(NSManagedObject*)managedObject {
    return managedObject.managedObjectContext == [[GraphManager getInstance] getTransientManagedObjectContext];
}

-(NSManagedObjectContext *)getPersistentManagedObjectContext {
    
    dispatch_queue_t q = dispatch_get_current_queue();
    
    const char *currentQueueLabel = dispatch_queue_get_label(q);
    
    if(currentQueueLabel) {
        
        NSString *label = [NSString stringWithUTF8String:currentQueueLabel];
        
        //if this is our background queue
        if([label rangeOfString:@"pe.snap"].location == 0) {
            
            @synchronized(SNAPPE_DISPATCH_QUEUE_COREDATA_CONTEXT_SYNCHRONIZATION_LOCK) {
                NSManagedObjectContext *context = self.customQueueContext[label];
                if(!context) {
                    DLog(@"no context found on queue %@, setting a new one", label);
                    context = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];
                    self.customQueueContext[label] = context;
                    DLog(@"no context found on queue %@, set a new one %@", label, context);
                }
            }
            
            return self.customQueueContext[label];
        }
    }

    return [NSManagedObjectContext MR_contextForCurrentThread];
}

-(NSManagedObjectContext *)getTransientManagedObjectContext {
    static NSManagedObjectContext *transientManagedObjectContext = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        transientManagedObjectContext = [[NSManagedObjectContext alloc] init];
        [transientManagedObjectContext setPersistentStoreCoordinator:[NSPersistentStoreCoordinator MR_defaultStoreCoordinator]];
    });
    return transientManagedObjectContext;
}

-(NSArray*) getUsers {
    return [self getEntity:@"User"];
}

-(NSArray*) getAlbums {
    return [self getEntity:@"Album"];
}

-(NSArray*) getDeviceAlbums {
    NSArray *rv = [self getEntity:@"Album"];
    return [rv filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(Album *album, NSDictionary *bindings)
    {
        return [album isCurrentDeviceAlbum];
    }]];
}

-(NSArray*) getFacebookAlbums {
    NSArray *rv = [self getEntity:@"Album"];
    return [rv filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(Album *album, NSDictionary *bindings)
    {
        return [album isFacebookAlbum];
    }]];
}

-(NSArray*) getFriends {
    NSString *userId = [[FBManager getInstance] getUserId];
    if(userId == nil) {
        return [[NSArray alloc] init];
    }
    
    User *currentUser = [GraphManager getGraphObjectById:userId WithEntityName:@"User"];
    return  [currentUser getFriends];
}

-(NSArray*) getEntity:(NSString*)type {
    return [self getEntity:type :nil];
}

-(NSArray*) getEntity:(NSString*)type :(NSPredicate*)predicate {
    NSManagedObjectContext *context = [self getPersistentManagedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:type inManagedObjectContext:context]];
    if(predicate != nil) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error = nil;
    NSArray *loadedEntities = [context executeFetchRequest:fetchRequest error:&error];
    if(error) {
        [Logger logError:[NSString stringWithFormat:@"error loading %@s from persistence store", type]:error];
        return nil;
    }
    return loadedEntities;
}

+(void) deleteObject:(NSManagedObject*)object {
    [[object managedObjectContext] deleteObject:object];
}

-(void) savePersistentContext {
    NSManagedObjectContext *context = [self getPersistentManagedObjectContext];
    [context MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if(!success && error) {
            [Logger logError:@"error is saving persistent context" :error];
        }
        else if(!success) {
            //no changes
        }
    }];
}

-(NSArray*)search:(NSString*)query:(NSUInteger)limit {
    //not searching for users, we stopped showing user photos like albums
    return [self search:query :limit:NO:YES:NO];
}

-(NSArray*)searchAlbums:(NSString*)query:(BOOL)facebookOnly:(NSUInteger)limit {
    return [self search:query :limit:NO:YES:facebookOnly];
}

-(NSArray*)searchUsers:(NSString*)query:(NSUInteger)limit {
    return [self search:query :limit:YES:NO:NO];
}


-(NSArray*)search:(NSString*)query:(NSUInteger)limit:(BOOL)searchUsers:(BOOL)searchAlbums:(BOOL)facebookOnly {
    //@TODO: ranking of search results
    NSMutableDictionary *entityQueryMap = [NSMutableDictionary dictionary];
    
    if(searchAlbums) {
        NSMutableDictionary *albumSearchParams = [NSMutableDictionary dictionary];
        [entityQueryMap setObject:albumSearchParams forKey:@"Album"];
        
        [albumSearchParams setObject:@[@"name", @"summary", @"location.name", @"location.city", @"owner.name" /*@"photos.location.name", @"photos.location.city",*/] forKey:query];
        if(facebookOnly) {
            [albumSearchParams setObject:@[@"deviceID"] forKey:@"facebook"];
        }
    }
    if(searchUsers) {
        NSMutableDictionary *userSearchParams = [NSMutableDictionary dictionary];
        [entityQueryMap setObject:userSearchParams forKey:@"User"];
        [userSearchParams setObject:@[@"name", @"currentLocation.name", @"currentLocation.city"] forKey:query];
    }
    
    NSString *currentUserId = [[FBManager getInstance] getUserId];
    
    NSMutableArray *rv = [[NSMutableArray alloc] init];
    for(NSString *entityName in entityQueryMap) {
        NSDictionary *entityQuery = [entityQueryMap objectForKey:entityName];
        NSArray *entityResults = [self searchGraphObjects:entityQuery :entityName];
        
        if(entityResults) {
            BOOL isUsers = [entityName isEqualToString:@"User"];
            for(NSManagedObject *obj in entityResults) {
                if(isUsers) {
                    User *u = (User*)obj;
                    if([u.id isEqualToString:currentUserId]) {
                        continue;
                    }
                }
                [rv addObject:obj];
            }
        }
    }
    
    NSRange range = NSMakeRange(0, MIN([rv count], limit));
    return [rv subarrayWithRange:range];
}

+(void) removeNonExistentLocalItemsFromGraph:(NSDictionary*)deleteDescriptor {
    
    NSMutableDictionary *serverDeleteDescriptor = [NSMutableDictionary dictionary];
    for(NSString *key in deleteDescriptor) {
        NSMutableArray *deletedObjectIDs = [[NSMutableArray alloc] init];
        [serverDeleteDescriptor setObject:deletedObjectIDs forKey:key];
        
        for(NSManagedObject *objectToDelete in [deleteDescriptor objectForKey:key]) {
            [deletedObjectIDs addObject:[objectToDelete valueForKey:@"id"]];
            [GraphManager deleteObject:objectToDelete];
        }
    }
    [[GraphManager getInstance] savePersistentContext];
}

+(void)prefetchObjects:(NSString*)entityName:(NSArray*)objects {
    if([objects count] == 0) {
        return;
    }
    
    NSManagedObjectContext * context = [[GraphManager getInstance] getPersistentManagedObjectContext];
     
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self IN %@", objects];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    [request setReturnsObjectsAsFaults:NO];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:request error:&error];
    if(fetchedObjects == nil) {
        [Logger logError:@"error in prefetching" :error];
    }
    else {
        for(NSManagedObject *ob in fetchedObjects) {
            [ob willAccessValueForKey:nil];
        }
    }
}

+(NSArray*)changeContext:(NSString*)entityName:(NSArray*)objectIDs:(NSManagedObjectContext*)destinationContext {
    if(!destinationContext) {
        destinationContext = [[GraphManager getInstance] getPersistentManagedObjectContext];
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF IN %@", objectIDs];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:destinationContext]];
    [request setReturnsObjectsAsFaults:NO];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [destinationContext executeFetchRequest:request error:&error];
    return fetchedObjects;
}

+(NSDictionary*)groupByAggregate:(NSString*)entityName:(NSString*)aggregationFunction:(NSString*)aggregationProperty:(NSString*)groupProperty:(NSPredicate*)filterPredicate
{
    NSManagedObjectContext *context = [[self getInstance] getPersistentManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    
    NSExpression *ex = [NSExpression expressionForFunction:aggregationFunction
                                                 arguments:[NSArray arrayWithObject:[NSExpression expressionForKeyPath:aggregationProperty]]];
    
    NSExpressionDescription *ed = [[NSExpressionDescription alloc] init];
    [ed setName:@"count"];
    [ed setExpression:ex];
    [ed setExpressionResultType:NSInteger64AttributeType];
    
    NSArray *properties = [NSArray arrayWithObjects:groupProperty, ed, nil];
    
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:entity];
    [request setPredicate:filterPredicate];
    [request setPropertiesToFetch:properties];
    [request setResultType:NSDictionaryResultType];
    [request setPropertiesToGroupBy:[NSArray arrayWithObject:groupProperty]];
    
    NSError* error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if(error) {
        [Logger logError:@"error in getting groupByAggregate" :error];
        return nil;
    }
    
    NSMutableDictionary *distByGroupProp = [NSMutableDictionary dictionary];
    for(NSDictionary *row in results) {
        id groupPropVal = [row objectForKey:groupProperty];
        NSNumber *aggregatedVal = [row objectForKey:@"count"];
        
        [distByGroupProp setObject:aggregatedVal forKey:groupPropVal];
    }

    return distByGroupProp;
}

+(int)getCount:(NSString*)entityName {
    NSManagedObjectContext * context = [[GraphManager getInstance] getPersistentManagedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    [request setIncludesSubentities:NO];
    
    NSError *error;
    NSUInteger count = [context countForFetchRequest:request error:&error];
    if(count == NSNotFound) {
        NSString *message = [NSString stringWithFormat:@"error in getting %@ count", entityName];
        [Logger logError:message :error];
        return -1;
    }
    return count;
}

+(NSArray*)getSorted:(NSString*)entityName:(NSString*)sortProperty:(int)offset:(int)limit {
    NSManagedObjectContext * context = [[GraphManager getInstance] getPersistentManagedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortProperty ascending:NO];
    [request setSortDescriptors:@[sortDescriptor]];
    
    request.fetchOffset = offset;
    request.fetchLimit = limit;
    
    NSError* error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if(error) {
        [Logger logError:@"error in getting groupByAggregate" :error];
        return nil;
    }
    
    return results;
}

@end
