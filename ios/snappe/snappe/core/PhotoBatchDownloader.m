//
//  PhotoBatchDownloader.m
//  snappe
//
//  Created by Shashank on 28/03/13.
//
//

#import "PhotoBatchDownloader.h"
#import "EventManager.h"
#import "Logger.h"
#import "SnappeHTTPService.h"
#import "PhotoAccessor.h"
#import "ExtendedOperationAccessor.h"
#import "ImageHasher.h"
#import "PhotoLibraryReader.h"
#import "PhotoHashUpdater.h"
#import "PhotoSaveManager.h"
#import "Util.h"

@implementation PhotoBatchDownloader

+(PhotoBatchDownloader*) getInstance {
    static PhotoBatchDownloader *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PhotoBatchDownloader alloc] init];
    });
    return sharedInstance;
}

+(void)download:(NSArray*)photos:(Album*)album:(NSSet*)existingHashes:(NSSet*)existingPHashes:(GenericCallback)callback:(GenericErrorback)errorback
{
    
    NSDictionary *context = @{
                              @"album" : album,
                              @"existingHashes" : existingHashes,
                              @"existingPHashes" : existingPHashes
                            };
    
    [[self getInstance] process:photos:context:callback:errorback];
}

-(void)process:(Photo*)photo:(id)context {
    
    Album *album = context[@"album"];
    NSSet *existingHashes = context[@"existingHashes"];
    NSSet *existingPHashes = context[@"existingPHashes"];
    
    NSString *url = photo.fullSrc ? photo.fullSrc : (photo.src ? photo.src : @"");
    
    [[EventManager getInstance] fireEvent:PHOTO_DOWNLOAD, photo.id, url, @(STARTED), nil];
    
    if(!photo.src) {
        
        [Logger track:@"photo_download_skipped" :@{@"reason" : @"no_url", @"hash" : photo.digest}];
        [[EventManager getInstance] fireEvent:PHOTO_DOWNLOAD, photo.id, url, @(SUCCESSFUL), nil];
        
        [self onPhotoDone:photo :nil];
        
        return;
    }
    
    if(photo.digest && [existingHashes containsObject:photo.digest]) {
        [Logger track:@"photo_download_skipped" :@{@"reason" : @"pre_download_dup_hash", @"hash" : photo.digest}];
        [[EventManager getInstance] fireEvent:PHOTO_DOWNLOAD, photo.id, url, @(SUCCESSFUL), nil];
        
        [self onPhotoDone:photo :nil];
        
        return;
    }
    
    if(photo.pHash) {
        
        NSString *similarPHash = [Util findSimilarPHash:photo.pHash :existingPHashes];
        
        if(similarPHash) {
            
            DLog(@"ignoring photo downloaded from %@ because of similar phashes: %@, %@", [photo getURL], similarPHash, photo.pHash);
            [Logger track:@"photo_download_skipped" :@{@"reason" : @"pre_download_dup_phash", @"new_phash" : photo.pHash, @"existing_phash": similarPHash}];
            
            [[EventManager getInstance] fireEvent:PHOTO_DOWNLOAD, photo.id, url, @(SUCCESSFUL), nil];
            
            [self onPhotoDone:photo :nil];
            
            return;
        }
    }
    
    [[SnappeHTTPService getInstance] downloadPhoto:photo :^(NSData *data) {
        
        //downloading time >>> hashing time, we calculate hashes upfront here
        NSString *hash = [PhotoLibraryReader getHashForPhotoData:data];
        NSString *pHash = [ImageHasher getHashForData:data];
        
        DLog(@"hash & phash for photo (%@) downloaded from url: %@, h: %@, ph: %@, length: %d", photo.id, photo.src, hash, pHash, data.length);
        
        
        if(!photo.digest || !photo.pHash || ![photo.digest isEqualToString:hash] ||  ![ImageHasher areSimilarImages:photo.pHash :pHash])
        {
            [[PhotoHashUpdater getInstance] add:photo.id:hash:pHash];
        }
        
        [photo setDigest:hash];
        [photo setPHash:pHash];
        
        if([existingHashes containsObject:hash]) {
            
            DLog(@"ignoring photo (%@) downloaded from %@ because of duplicate hash: %@", photo.id, [photo getURL], hash);
            [Logger track:@"photo_download_skipped" :@{@"reason" : @"post_download_dup_hash", @"hash" : photo.digest}];
            
            [[EventManager getInstance] fireEvent:PHOTO_DOWNLOAD, photo.id, url, @(SUCCESSFUL), nil];
            
            [self onPhotoDone:photo :nil];
            
            return;
        }
        
        
        NSString *similarPHash = [Util findSimilarPHash:pHash :existingPHashes];
        
        if(similarPHash) {
            
            DLog(@"ignoring photo(%@) downloaded from %@ because of similar phashes: %@, %@", photo.id, [photo getURL], similarPHash, pHash);
            [Logger track:@"photo_download_skipped" :@{@"reason" : @"post_download_dup_phash", @"phash" : photo.pHash}];
            
            
            [[EventManager getInstance] fireEvent:PHOTO_DOWNLOAD, photo.id, url, @(SUCCESSFUL), nil];
            
            [self onPhotoDone:photo :nil];
            
            return;
        }
        
        
        [[PhotoSaveManager getInstance] savePhoto:data :album.name :[photo getALAssetMetadata] :^(ALAsset *asset)
         {
             //hash of the photo might change after saving the raw data due to possible modifications
             //if a existing photo in a local album is being "realized" by downloading, we update the
             //hash, this check is to exclude the case where a photo from another album is downloaded
             //into this album
             //we also need to check if photo's album is not set (this not set for new photos until they
             //are downloaded)
             if(!photo.album || [photo.album.id isEqualToString:album.id]) {
                 
                 NSString *savedPhotoHash = [[PhotoLibraryReader getInstance] getAssetHash:asset];
                 NSString *savedPhotoPHash = [ImageHasher getHashForALAsset:asset];
                 
                 DLog(@"hash & phash for photo (%@) downloaded from url & saved to disk: %@, h: %@, ph: %@", photo.id, photo.src, savedPhotoHash, savedPhotoPHash);
                 
                 
                 if(!photo.digest || !photo.pHash || ![photo.digest isEqualToString:savedPhotoHash] || ![ImageHasher areSimilarImages:photo.pHash :savedPhotoPHash])
                 {
                     [[PhotoHashUpdater getInstance] add:photo.id:savedPhotoHash:savedPhotoPHash];
                 }
                 
                 [photo setDigest:savedPhotoHash];
                 [photo setPHash:savedPhotoPHash];
                 
             }
             
             [[EventManager getInstance] fireEvent:PHOTO_DOWNLOAD, photo.id, url, @(SUCCESSFUL), nil];
             
             [self onPhotoDone:photo :nil];
             
         } :^(NSError *error){
             
             [Logger logError:@"aborting album download due to error in saving photo":error];
             
             [self onPhotoDone:photo :error];
             
         }];
        
    } :^(NSError *error){
        
        [Logger logError:@"aborting album download due to error in downloading photo error":error];
        
        [self onPhotoDone:photo :error];
        
    }:^(long long done, long long toDo){
        
        double thisPhotoDoneFraction = ((double)done)/((double)toDo + 1);
        [[EventManager getInstance] fireEvent:PHOTO_DOWNLOAD, photo.id, photo.src ? photo.src : @"", @(ONGOING), @(thisPhotoDoneFraction), nil];
        
    }];
}
@end
