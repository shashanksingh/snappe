//
//  ExtendedOperation.m
//  snappe
//
//  Created by Shashank on 24/01/13.
//
//

#import "ExtendedOperation.h"


@implementation ExtendedOperation

@dynamic error;
@dynamic finishedOn;
@dynamic id;
@dynamic queuedOn;
@dynamic state;
@dynamic status;

@end
