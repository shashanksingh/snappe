//
//  ALAssetInfo.h
//  snappe
//
//  Created by Shashank on 14/01/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ALAssetInfo : NSObject
@property (strong) NSString *hash;
@property (strong) NSString *pHash;
@property (strong) NSString *url;
@property NSValue *size;
@property NSValue *thumbSize;
@property CLLocation *location;
@property NSDate *date;
@end
