//
//  User.h
//  snappe
//
//  Created by Shashank on 24/01/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Album, Location, Tag, User;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSNumber * isRegisteredUser;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * locale;
@property (nonatomic, retain) NSString * middleName;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * sex;
@property (nonatomic, retain) NSSet *canUpload;
@property (nonatomic, retain) Location *currentLocation;
@property (nonatomic, retain) NSSet *friendship;
@property (nonatomic, retain) NSSet *ownedAlbum;
@property (nonatomic, retain) NSSet *sharedOwner;
@property (nonatomic, retain) NSSet *tag;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addCanUploadObject:(Album *)value;
- (void)removeCanUploadObject:(Album *)value;
- (void)addCanUpload:(NSSet *)values;
- (void)removeCanUpload:(NSSet *)values;

- (void)addFriendshipObject:(User *)value;
- (void)removeFriendshipObject:(User *)value;
- (void)addFriendship:(NSSet *)values;
- (void)removeFriendship:(NSSet *)values;

- (void)addOwnedAlbumObject:(Album *)value;
- (void)removeOwnedAlbumObject:(Album *)value;
- (void)addOwnedAlbum:(NSSet *)values;
- (void)removeOwnedAlbum:(NSSet *)values;

- (void)addSharedOwnerObject:(Album *)value;
- (void)removeSharedOwnerObject:(Album *)value;
- (void)addSharedOwner:(NSSet *)values;
- (void)removeSharedOwner:(NSSet *)values;

- (void)addTagObject:(Tag *)value;
- (void)removeTagObject:(Tag *)value;
- (void)addTag:(NSSet *)values;
- (void)removeTag:(NSSet *)values;

@end
