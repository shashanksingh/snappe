//
//  ALAssetsGroupInfo.h
//  snappe
//
//  Created by Shashank on 14/01/13.
//
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAssetsGroupInfo : NSObject
@property (strong) NSString *name;
@property (strong) NSString *devicePersistentID;
@property (strong) NSString *url;
@property (strong) NSNumber *type;
+(ALAssetsGroupInfo*)fromAssetsGroup:(ALAssetsGroup*)group;
@end
