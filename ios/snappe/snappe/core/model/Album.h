//
//  Album.h
//  snappe
//
//  Created by Shashank on 20/03/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Album, Location, Photo, User;

@interface Album : NSManagedObject

@property (nonatomic, retain) NSDate * createdOn;
@property (nonatomic, retain) NSNumber * deviceAlbumType;
@property (nonatomic, retain) NSString * deviceID;
@property (nonatomic, retain) NSString * devicePersistentID;
@property (nonatomic, retain) NSString * fbObjectID;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSDate * modifiedOn;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * newPhotosCount;
@property (nonatomic, retain) NSSet *canUpload;
@property (nonatomic, retain) Location *location;
@property (nonatomic, retain) User *owner;
@property (nonatomic, retain) NSOrderedSet *photos;
@property (nonatomic, retain) NSSet *pulledInto;
@property (nonatomic, retain) Album *pulledIntoInverse;
@property (nonatomic, retain) NSSet *pushedTo;
@property (nonatomic, retain) Album *pushedToInverse;
@property (nonatomic, retain) NSSet *sharedWith;
@end

@interface Album (CoreDataGeneratedAccessors)

- (void)addCanUploadObject:(User *)value;
- (void)removeCanUploadObject:(User *)value;
- (void)addCanUpload:(NSSet *)values;
- (void)removeCanUpload:(NSSet *)values;

- (void)insertObject:(Photo *)value inPhotosAtIndex:(NSUInteger)idx;
- (void)removeObjectFromPhotosAtIndex:(NSUInteger)idx;
- (void)insertPhotos:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removePhotosAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInPhotosAtIndex:(NSUInteger)idx withObject:(Photo *)value;
- (void)replacePhotosAtIndexes:(NSIndexSet *)indexes withPhotos:(NSArray *)values;
- (void)addPhotosObject:(Photo *)value;
- (void)removePhotosObject:(Photo *)value;
- (void)addPhotos:(NSOrderedSet *)values;
- (void)removePhotos:(NSOrderedSet *)values;
- (void)addPulledIntoObject:(Album *)value;
- (void)removePulledIntoObject:(Album *)value;
- (void)addPulledInto:(NSSet *)values;
- (void)removePulledInto:(NSSet *)values;

- (void)addPushedToObject:(Album *)value;
- (void)removePushedToObject:(Album *)value;
- (void)addPushedTo:(NSSet *)values;
- (void)removePushedTo:(NSSet *)values;

- (void)addSharedWithObject:(User *)value;
- (void)removeSharedWithObject:(User *)value;
- (void)addSharedWith:(NSSet *)values;
- (void)removeSharedWith:(NSSet *)values;

@end
