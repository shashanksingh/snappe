//
//  Contact.h
//  snappe
//
//  Created by Shashank on 25/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject
@property (strong) NSString *name;
@property (strong) NSArray *emails;

-(NSString*)getBestEmail;
-(BOOL)matches:(NSString*)query;
-(UIImageView*)getThumb:(CGFloat)width:(CGFloat)height;
@end
