//
//  SpatioTemporaLocation.h
//  snappe
//
//  Created by Shashank on 21/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationAccessor.h"
#import "PhotoAccessor.h"
#import "TagAccessor.h"

@interface SpatioTemporalLocation : NSObject
@property (strong) Location *location;
@property NSTimeInterval timestamp;

-(id)init:(Location*)location:(NSDate*)date;
-(id)initWithTag:(Tag*)tag;
-(id)initWithPhoto:(Photo*)photo;
@end
