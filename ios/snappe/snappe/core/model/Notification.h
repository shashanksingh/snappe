//
//  Notification.h
//  snappe
//
//  Created by Shashank on 24/01/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Notification : NSManagedObject

@property (nonatomic, retain) id data;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSNumber * pending;
@property (nonatomic, retain) NSNumber * timestamp;
@property (nonatomic, retain) NSString * type;

@end
