//
//  ALAssetsGroupInfo.m
//  snappe
//
//  Created by Shashank on 14/01/13.
//
//

#import "ALAssetsGroupInfo.h"

@implementation ALAssetsGroupInfo

+(ALAssetsGroupInfo*)fromAssetsGroup:(ALAssetsGroup*)group {
    ALAssetsGroupInfo *rv = [ALAssetsGroupInfo new];
    
    rv.name = [group valueForProperty:ALAssetsGroupPropertyName],
    rv.devicePersistentID = [group valueForProperty:ALAssetsGroupPropertyPersistentID];
    rv.type = [group valueForProperty:ALAssetsGroupPropertyType];
    rv.url = [[group valueForProperty:ALAssetsGroupPropertyURL] absoluteString];
    
    return rv;
    
    
}
@end
