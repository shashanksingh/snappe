//
//  Contact.m
//  snappe
//
//  Created by Shashank on 25/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Contact.h"

@implementation Contact
@synthesize name;
@synthesize emails;

-(NSString*)getBestEmail {
    if(!self.emails || [self.emails count] == 0) {
        return @"";
    }
    return [emails objectAtIndex:0];
}

-(BOOL)matches:(NSString*)query {
    if(!query || [query isEqualToString:@""]) {
        return NO;
    }
    
    if([self.name rangeOfString:query].location != NSNotFound) {
        return YES;
    }
    
    for(NSString *email in emails) {
        if([email rangeOfString:query].location != NSNotFound) {
            return YES;
        }
    }
    
    return NO;
}

-(UIImageView*)getThumb:(CGFloat)width:(CGFloat)height {
    UIImageView *rv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    rv.image = [UIImage imageNamed:@"user_silhouette.png"];
    return rv;
}
@end
