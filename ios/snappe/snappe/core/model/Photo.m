//
//  Photo.m
//  snappe
//
//  Created by Shashank on 04/04/13.
//
//

#import "Photo.h"
#import "Album.h"
#import "Location.h"


@implementation Photo

@dynamic caption;
@dynamic clickedOn;
@dynamic cropCenterX;
@dynamic cropCenterY;
@dynamic digest;
@dynamic height;
@dynamic id;
@dynamic markedPrivate;
@dynamic pHash;
@dynamic src;
@dynamic thumbHeight;
@dynamic thumbSrc;
@dynamic thumbWidth;
@dynamic width;
@dynamic fullSrc;
@dynamic fullWidth;
@dynamic fullHeight;
@dynamic album;
@dynamic location;

@end
