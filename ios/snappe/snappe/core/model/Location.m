//
//  Location.m
//  snappe
//
//  Created by Shashank on 26/01/13.
//
//

#import "Location.h"
#import "Album.h"
#import "Photo.h"
#import "Tag.h"
#import "User.h"


@implementation Location

@dynamic city;
@dynamic country;
@dynamic id;
@dynamic latitude;
@dynamic longitude;
@dynamic name;
@dynamic state;
@dynamic zip;
@dynamic album;
@dynamic photos;
@dynamic tag;
@dynamic user;

@end
