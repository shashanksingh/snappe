//
//  Tag.m
//  snappe
//
//  Created by Shashank on 24/01/13.
//
//

#import "Tag.h"
#import "Location.h"
#import "User.h"


@implementation Tag

@dynamic id;
@dynamic timestamp;
@dynamic location;
@dynamic user;

@end
