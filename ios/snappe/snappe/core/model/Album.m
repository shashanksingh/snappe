//
//  Album.m
//  snappe
//
//  Created by Shashank on 20/03/13.
//
//

#import "Album.h"
#import "Album.h"
#import "Location.h"
#import "Photo.h"
#import "User.h"


@implementation Album

@dynamic createdOn;
@dynamic deviceAlbumType;
@dynamic deviceID;
@dynamic devicePersistentID;
@dynamic fbObjectID;
@dynamic id;
@dynamic modifiedOn;
@dynamic name;
@dynamic summary;
@dynamic type;
@dynamic newPhotosCount;
@dynamic canUpload;
@dynamic location;
@dynamic owner;
@dynamic photos;
@dynamic pulledInto;
@dynamic pulledIntoInverse;
@dynamic pushedTo;
@dynamic pushedToInverse;
@dynamic sharedWith;

@end
