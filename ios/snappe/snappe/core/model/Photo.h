//
//  Photo.h
//  snappe
//
//  Created by Shashank on 04/04/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Album, Location;

@interface Photo : NSManagedObject

@property (nonatomic, retain) NSString * caption;
@property (nonatomic, retain) NSDate * clickedOn;
@property (nonatomic, retain) NSNumber * cropCenterX;
@property (nonatomic, retain) NSNumber * cropCenterY;
@property (nonatomic, retain) NSString * digest;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSNumber * markedPrivate;
@property (nonatomic, retain) NSString * pHash;
@property (nonatomic, retain) NSString * src;
@property (nonatomic, retain) NSNumber * thumbHeight;
@property (nonatomic, retain) NSString * thumbSrc;
@property (nonatomic, retain) NSNumber * thumbWidth;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSString * fullSrc;
@property (nonatomic, retain) NSNumber * fullWidth;
@property (nonatomic, retain) NSNumber * fullHeight;
@property (nonatomic, retain) Album *album;
@property (nonatomic, retain) Location *location;

@end
