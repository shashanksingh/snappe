//
//  SpatioTemporaLocation.m
//  snappe
//
//  Created by Shashank on 21/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SpatioTemporalLocation.h"
#import "Util.h"

@implementation SpatioTemporalLocation
@synthesize location;
@synthesize timestamp;

-(id)init:(Location*)location:(NSDate*)date {
    self = [super init];
    if(self) {
        self.location = location;
        self.timestamp = [date timeIntervalSince1970];
    }
    return self;
}
-(id)initWithTag:(Tag*)tag {
    self = [super init];
    if(self) {
        self.location = tag.location;
        self.timestamp = [tag.timestamp timeIntervalSince1970];
    }
    return self;
}
-(id)initWithPhoto:(Photo*)photo {
    self = [super init];
    if(self) {
        self.location = [photo getSelfOrAlbumLocation];
        self.timestamp = [photo.clickedOn timeIntervalSince1970];
    }
    return self;
}

@end
