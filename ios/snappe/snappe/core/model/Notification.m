//
//  Notification.m
//  snappe
//
//  Created by Shashank on 24/01/13.
//
//

#import "Notification.h"


@implementation Notification

@dynamic data;
@dynamic id;
@dynamic pending;
@dynamic timestamp;
@dynamic type;

@end
