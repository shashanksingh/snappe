//
//  User.m
//  snappe
//
//  Created by Shashank on 24/01/13.
//
//

#import "User.h"
#import "Album.h"
#import "Location.h"
#import "Tag.h"
#import "User.h"


@implementation User

@dynamic firstName;
@dynamic id;
@dynamic isRegisteredUser;
@dynamic lastName;
@dynamic locale;
@dynamic middleName;
@dynamic name;
@dynamic sex;
@dynamic canUpload;
@dynamic currentLocation;
@dynamic friendship;
@dynamic ownedAlbum;
@dynamic sharedOwner;
@dynamic tag;

@end
