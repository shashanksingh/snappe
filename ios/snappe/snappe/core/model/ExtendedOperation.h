//
//  ExtendedOperation.h
//  snappe
//
//  Created by Shashank on 24/01/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ExtendedOperation : NSManagedObject

@property (nonatomic, retain) NSString * error;
@property (nonatomic, retain) NSDate * finishedOn;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSDate * queuedOn;
@property (nonatomic, retain) id state;
@property (nonatomic, retain) NSNumber * status;

@end
