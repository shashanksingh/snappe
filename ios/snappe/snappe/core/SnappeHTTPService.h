//
//  NetworkManager.h
//  snappe
//
//  Created by Shashank on 09/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPClient.h"
#import "Macros.h"
#import "AlbumAccessor.h"

@interface SnappeHTTPService : AFHTTPClient
+(SnappeHTTPService *) getInstance;

-(void)invalidateFBGraph;

-(void) applyLocalModifications:(NSDictionary*)localModDescriptor:(GenericCallback)callback:(GenericErrorback) errorback;
-(void) getPhotoHashesToUpload: (Album*)album :(ArrayCallback)callback :(GenericErrorback)errorback;
-(void)getPhotoHashesNeedingUpload:(NSSet*)hashes:(ArrayCallback)callback:(GenericErrorback)errorback;
-(void) uploadPhoto:(Photo*)photo :(GenericCallback)callback :(GenericErrorback)errorback:(ProgressCallback)progressCallback;

-(void)updateFriendShareState:(NSString *)albumId :(NSArray*)changeDescriptors :(NSString*)appRequestID :(GenericCallback)callback :(GenericErrorback)errorback;

-(void)shareAlbumWithContacts:(NSString *)albumId :(NSArray*)shareDescriptor :(GenericCallback)callback :(GenericErrorback)errorback;

-(void)pullAlbum:(NSString*)sourceAlbumID :(NSString*)destinationAlbumID:(GenericCallback)callback:(GenericErrorback)errorback;
-(void)pushAlbum:(NSString*)sourceAlbumID :(NSString*)destinationAlbumID:(GenericCallback)callback:(GenericErrorback)errorback;
-(void)syncAlbums:(NSString*)localAlbumID :(NSString*)remoteAlbumID:(GenericCallback)callback:(GenericErrorback)errorback;

-(void) downloadURL:(NSString*)url :(GenericCallback)callback :(GenericErrorback)errorback:(ProgressCallback)progressCallback;
-(void) downloadPhoto:(Photo*)photo :(GenericCallback)callback :(GenericErrorback)errorback:(ProgressCallback)progressCallback;
-(void) markNotificationsShown:(NSArray*)shownNotifications :(GenericCallback)callback :(GenericErrorback)errorback;

-(void) markPhotosPrivate:(NSArray*)photos :(VoidCallback)callback :(GenericErrorback)errorback;
-(void) markPhotosPublic:(NSArray*)photos :(VoidCallback)callback :(GenericErrorback)errorback;

-(void) updateAlbumName:(NSString*)name :(Album*)album :(GenericCallback)callback :(GenericErrorback)errorback;
-(void) updateAlbumDescription:(NSString*)description :(Album*)album :(GenericCallback)callback :(GenericErrorback)errorback;

-(void)addPhotosToAlbum:(Album*)album:(NSArray*)photos:(VoidCallback)callback:(GenericErrorback)errorback;

-(void) deleteObjects:(NSDictionary*)deleteDescriptor:(VoidCallback)callback:(GenericErrorback)errorback;
-(void)logLocations:(NSArray*)locations:(GenericCallback)callback:(GenericErrorback)errorback;
-(void)updatePhotoHashes:(NSArray*)hashInfoList:(GenericCallback)callback:(GenericErrorback)errorback;

-(void)reportMissingPhotos:(NSArray*)photos;

-(void)sendShareNotification:(NSDictionary*)context:(GenericCallback)callback:(GenericErrorback)errorback;

-(void)verifyPurchase:(NSString*)receipt:(GenericCallback)callback:(GenericErrorback)errorback;
@end
