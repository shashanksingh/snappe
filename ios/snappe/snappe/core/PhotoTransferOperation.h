//
//  PhotoTransferOperation.h
//  snappe
//
//  Created by Shashank on 02/04/13.
//
//

#import "BaseExtOp.h"

@interface PhotoTransferOperation : BaseExtOp
-(NSArray*)getPhotos;
-(NSArray*)getEventTypes;
-(void)setUp;
@end
