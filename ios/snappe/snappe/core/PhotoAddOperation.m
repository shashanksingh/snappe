//
//  PhotoAddOperation.m
//  snappe
//
//  Created by Shashank on 07/04/13.
//
//

#import "PhotoAddOperation.h"
#import "EventManager.h"
#import "Util.h"
#import "Logger.h"
#import "SnappeHTTPService.h"
#import "PhotoBatchUploader.h"
#import "PhotoLibraryOrganizer.h"
#import "PhotoLibraryReader.h"

#define PHOTOS_TO_UPLOAD_IDS_KEY @"photos_to_upload"
#define RECIPIENT_ALBUM_ID_KEY @"recipient_album_id"
#define RECIPIENT_ALBUM_NAME_KEY @"recipient_album_name"

@interface PhotoAddOperation()
@property (retain) Album *album;
@property (retain) NSArray *photos;
@end

@implementation PhotoAddOperation
-(id)init:(Album*)album:(NSArray*)photos {
    self = [super init];
    if(self) {
        self.album = album;
        self.photos = photos;
        
        [self setUp];
    }
    return self;
}
-(id)initWithPersistedState:(NSDictionary*)persistedState {
    //NOT_IMPLEMENTED
    return nil;
}

-(NSArray*)getPhotos {
    return self.photos;
}

-(NSArray*)getEventTypes {
    return @[@(PHOTO_UPLOAD), @(PHOTO_DOWNLOAD)];
}

-(NSString*)getName {
    return @"Adding Photos";
}

-(NSString*)getType {
    return @"EXTOP_ADD_PHOTOS";
}

-(NSDictionary*)getState {
    NSMutableDictionary *state = [[super getState] mutableCopy];
    [state setObject:NSStringFromClass(self.class) forKey:@"class"];
    [state setObject:[self.photos valueForKey:@"id"] forKey:PHOTOS_TO_UPLOAD_IDS_KEY];
    [state setObject:self.album.id forKey:RECIPIENT_ALBUM_ID_KEY];
    [state setObject:self.album.name forKey:RECIPIENT_ALBUM_NAME_KEY];
    return state;
}

-(NSString*)getStatusText {
    NSString *statusText = nil;
    if(self.status == NOT_STARTED) {
        statusText = @"waiting...";
    }
    else if(self.status == STARTED) {
        statusText = [NSString stringWithFormat:@"%d%% completed", MIN(self.completion, 99)];
    }
    else if(self.status == SUCCESSFUL) {
        long secondsSinceDone = [self.finishedOn timeIntervalSince1970];
        statusText = [NSString stringWithFormat:@"successfully added photos %@", [Util getRelativeTime:secondsSinceDone]];
    }
    else if(self.status == FAILURE) {
        statusText = @"failed, please try again";
    }
    else {
        DLog(@"warning: unknown value for ext op status: %d", self.status);
    }
    return statusText;
}

-(Notification*)getNotification {
    Notification *notification = [super getNotification];
    
    NSMutableDictionary *notificationData = notification.data;
    [notificationData setObject:[self.photos valueForKey:@"id"] forKey:PHOTOS_TO_UPLOAD_IDS_KEY];
    
    return notification;
}

-(void)start:(GenericCallback)callback {
    self.status = STARTED;
    [self fireUpdate];
    
    __block typeof (self) bself = self;
    
    GenericCallback onSuccess = ^(id data) {
        [Logger track:@"add_photo" :@{@"album_id": bself.album.id, @"album_name": bself.album.name, @"photo_count": @(bself.photos.count)}];
        
        bself.status = SUCCESSFUL;
        bself.completion = 100;
        bself.finishedOn = [NSDate date];
        
        [[PhotoLibraryOrganizer getInstance] sync:^{
            DLog(@"successfully synced with server after PhotoAddOperation");
            
            callback(@(bself.status));
            
            [bself onFinish:YES];
            
        } :^(NSError *error) {
            [Logger logError:@"error in syncing after PhotoAddOperation" :error];
            
            callback(@(FAILURE));
            
            [bself onFinish:NO];
            
        }];
    };
    
    GenericErrorback onError = ^(NSError *error_) {
        if(!([error_.domain isEqualToString:SNAPPE_ERROR_DOMAIN] && error_.code == SNAPPE_ERROR_INSUFFICIENT_CREDITS))
        {
            [Logger logError:[NSString stringWithFormat:@"error in adding %d new photos to %@", bself.photos.count, bself.album.name] :error_];
            
            //TODO: may be dial down the last sync time to allow repeat of the process without user
            //intervention
            NSString *userMessage = [NSString stringWithFormat:@"There was an error in adding new photos to %@. Please add them again and resync", bself.album.name];
            
            [Util alertError:error_:userMessage];
            
        }
        
        bself.status = FAILURE;
        bself.error = [error_ localizedDescription];
        bself.finishedOn = [NSDate date];
        
        callback(@(bself.status));
        
        [bself onFinish:NO];
        
    };
    
    NSMutableSet *hashSet = [NSMutableSet set];
    for(Photo *photo in self.photos) {
        if(photo.digest) {
            [hashSet addObject:photo.digest];
        }
    }
    
    //local albums do not need any photos to be uploaded, remote might
    if([self.album isCurrentDeviceAlbum]) {
        [[SnappeHTTPService getInstance] addPhotosToAlbum:self.album:self.photos:^{
            onSuccess(nil);
        }:^(NSError *error){
            onError(error);
        }];
    }
    else {
        [[SnappeHTTPService getInstance] getPhotoHashesNeedingUpload:hashSet :^(NSArray *data) {
            
            NSSet *hashesNeedingUpload = [NSSet setWithArray:data];
            
            NSMutableArray *photosToUpload = [NSMutableArray array];
            for(Photo *photo in bself.photos) {
                //there could be photos from FB with digest provided by another user but server won't have them
                //neither will this device, that's why we check if the photo hash is locally present
                if(photo.digest && [hashesNeedingUpload containsObject:photo.digest])
                {
                    if([[PhotoLibraryReader getInstance] assetExistsWithHash:photo.digest]) {
                        [photosToUpload addObject:photo];
                    }
                    else {
                        [Logger track:@"missing_photo_for_add" :@{@"id" : photo.id}];
                    }
                
                }
            }
            
            [PhotoBatchUploader upload:photosToUpload :^(id data) {
                [[SnappeHTTPService getInstance] addPhotosToAlbum:bself.album:bself.photos:^{
                    onSuccess(nil);
                }:^(NSError *error){
                    onError(error);
                }];
                
            } :^(NSError *error) {
                onError(error);
            }];
            
        } :^(NSError *error) {
            onError(error);
        }];
    }
    

}


@end
