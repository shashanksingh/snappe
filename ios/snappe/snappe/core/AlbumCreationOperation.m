//
//  AlbumCreationOperation.m
//  snappe
//
//  Created by Shashank on 27/04/13.
//
//

#import "AlbumCreationOperation.h"
#import "EventManager.h"
#import "Util.h"
#import "Logger.h"
#import "PhotoBatchFacebookUploader.h"
#import "PhotoLibraryReader.h"
#import "ModalAlertView.h"
#import "SnappeHTTPService.h"

#define ALBUM_NAME_KEY @"album_name"
#define PHOTO_IDS_KEY @"photo_ids"
#define IS_FACEBOOK_ALBUM_KEY @"is_facebook_album"

@interface AlbumCreationOperation()
@property (retain) NSString *albumName;
@property (retain) NSArray *photos;
@property BOOL isFacebookAlbum;
@end

@implementation AlbumCreationOperation

-(id)init:(NSString*)albumName:(NSArray*)photos:(BOOL)isFacebookAlbum {
    self = [super init];
    if(self) {
        self.albumName = albumName;
        self.photos = photos;
        self.isFacebookAlbum = isFacebookAlbum;
        
        [self setUp];
    }
    return self;
}
-(id)initWithPersistedState:(NSDictionary*)persistedState {
    //NOT_IMPLEMENTED
    return nil;
}

-(NSArray*)getPhotos {
    return self.photos;
}

-(NSArray*)getEventTypes {
    if(self.isFacebookAlbum) {
        return @[@(PHOTO_UPLOAD)];
    }
    else {
        return @[@(PHOTO_DOWNLOAD)];
    }
}

-(NSString*)getName {
    return @"Creating Albums";
}

-(NSString*)getType {
    return @"EXTOP_CREATE_ALBUM";
}

-(NSDictionary*)getState {
    NSMutableDictionary *state = [[super getState] mutableCopy];
    [state setObject:NSStringFromClass(self.class) forKey:@"class"];
    [state setObject:[self.photos valueForKey:@"id"] forKey:PHOTO_IDS_KEY];
    [state setObject:self.albumName forKey:ALBUM_NAME_KEY];
    [state setObject:@(self.isFacebookAlbum) forKey:IS_FACEBOOK_ALBUM_KEY];
    return state;
}

-(NSString*)getStatusText {
    NSString *statusText = nil;
    if(self.status == NOT_STARTED) {
        statusText = @"waiting...";
    }
    else if(self.status == STARTED) {
        statusText = [NSString stringWithFormat:@"%d%% completed", MIN(self.completion, 99)];
    }
    else if(self.status == SUCCESSFUL) {
        long secondsSinceDone = [self.finishedOn timeIntervalSince1970];
        statusText = [NSString stringWithFormat:@"successfully created album %@", [Util getRelativeTime:secondsSinceDone]];
    }
    else if(self.status == FAILURE) {
        statusText = @"failed, please try again";
    }
    else {
        DLog(@"warning: unknown value for ext op status: %d", self.status);
    }
    return statusText;
}

-(Notification*)getNotification {
    Notification *notification = [super getNotification];
    
    NSMutableDictionary *notificationData = notification.data;
    [notificationData setObject:[self.photos valueForKey:@"id"] forKey:PHOTO_IDS_KEY];
    
    return notification;
}

-(void)start:(GenericCallback)callback {
    self.status = STARTED;
    [self fireUpdate];
    
    __block typeof (self) bself = self;
    
    GenericCallback onSuccess = ^(id data) {
        [Logger track:@"create_album" :@{@"album_name": bself.albumName, @"photo_count": @(bself.photos.count)}];
        
        bself.status = SUCCESSFUL;
        bself.completion = 100;
        bself.finishedOn = [NSDate date];
        
        [[PhotoLibraryOrganizer getInstance] sync:^{
            DLog(@"successfully synced with server after AlbumCreationOperation");
            
            callback(@(bself.status));
            
            [bself onFinish:YES];
            
            [ModalAlertView showSuccess:[NSString stringWithFormat:@"Successfully created new album '%@'", bself.albumName]];
            
        } :^(NSError *error) {
            [Logger logError:@"error in syncing after AlbumCreationOperation" :error];
            
            callback(@(FAILURE));
            
            [bself onFinish:NO];
            
        }];
    };
    
    GenericErrorback onError = ^(NSError *error_) {
        if(!([error_.domain isEqualToString:SNAPPE_ERROR_DOMAIN] && error_.code == SNAPPE_ERROR_INSUFFICIENT_CREDITS))
        {
            [Logger logError:[NSString stringWithFormat:@"error in creating new album %@", bself.albumName] :error_];
            
            if([error_.domain isEqualToString:SNAPPE_ERROR_DOMAIN] && error_.userInfo[@"message"]) {
                [Util alertError:error_ :error_.userInfo[@"message"]];
            }
            else {
                //TODO: may be dial down the last sync time to allow repeat of the process without user
                //intervention
                NSString *userMessage = [NSString stringWithFormat:@"There was an error in creating new album %@. Please try again", bself.albumName];
                
                [Util alertError:error_:userMessage];
            }
        }
        
        bself.status = FAILURE;
        bself.error = [error_ localizedDescription];
        bself.finishedOn = [NSDate date];
        
        callback(@(bself.status));
        
        [bself onFinish:NO];
        
    };
    
    //local albums do not need any photos to be uploaded, remote might
    if(self.isFacebookAlbum) {
        [PhotoBatchFacebookUploader upload:self.photos :self.albumName :onSuccess :onError];
    }
    else {
        
        [[PhotoLibraryReader getInstance] getOrCreateLocalAssetsGroup:self.albumName :^(NSArray *groupData) {
            if(!bself) {
                return;
            }
            
            ALAssetsGroup *group = groupData[0];
            NSNumber *newlyCreated = groupData[1];
            if(!newlyCreated.boolValue) {
                NSString *m = [NSString stringWithFormat:@"An album with the name '%@' already exists", bself.albumName];
                [ModalAlertView showError:m];
                
                NSError *e = [NSError errorWithDomain:SNAPPE_ERROR_DOMAIN code:SNAPPE_ERROR_ALBUM_ALREADY_EXISTS_WITH_NAME userInfo:@{@"message": m}];
                onError(e);
                
            }
            else {
                NSString *albumDevicePersistentID = [group valueForProperty:ALAssetsGroupPropertyPersistentID];
                [[PhotoLibraryOrganizer getInstance] sync:^{
                    dispatch_async([CoreController getCoreDataBackgroundQueue], ^{
                        if(!bself) {
                            return;
                        }
                        
                        Album *newAlbum = [Album getByDevicePersistentId:albumDevicePersistentID];
                        
                        [[SnappeHTTPService getInstance] addPhotosToAlbum:newAlbum :bself.photos :^{
                            
                            DLog(@"successfully created a new album named %@ with %d photos", bself.albumName, bself.photos.count);
                            
                            [[PhotoLibraryOrganizer getInstance] sync:^{
                                DLog(@"successfully resynced after creating a new album named %@ with %d photos", bself.albumName, bself.photos.count);
                                onSuccess(@(bself.photos.count));
                            } :onError];
                            
                        } :onError];
                    });
                    
                } :onError];
            }
        }:onError];
    }
    
    
}

@end
