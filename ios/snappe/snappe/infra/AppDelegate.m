//
//  AppDelegate.m
//  snappe
//
//  Created by Shashank on 11/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"
#import "HelpOverlay.h"
#import "ImageHasher.h"
#import "UIController.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "EventManager.h"
#import "SnappeHTTPService.h"
#import "ExtOpQueue.h"
#import "LocationLogger.h"
#import "Logger.h"
#import "Util.h"
#import "CreditsManager.h"
#import "TestFlight.h"
#import <AdSupport/ASIdentifierManager.h>


@implementation AppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
        [TestFlight takeOff:@"8be4f173-9999-4017-ba83-45f2a73e44cd"];
    });
    
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"Snappe.sqlite"];
    
    [[FBManager getInstance] openSession:YES];
    
    [Logger start];
    
    [Logger track:@"app_init"];
    
    RootViewController *rootViewController =  [[RootViewController alloc] init];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = rootViewController;
    
    self.window.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"iphone_background.png"]];
    [self.window makeKeyAndVisible];
    
    HelpOverlay *helpOverlay = [HelpOverlay getInstance];
    [self.window addSubview:helpOverlay];
    [self.window bringSubviewToFront:helpOverlay];
    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    [[ExtOpQueue getInstance] start];
    
    //this will start the logger
    //[LocationLogger getInstance];
    
    [[CreditsManager getInstance] fetchProducts];
    
    if(launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]) {
        [Logger track:@"received_remote_notification" : @{@"state": @"stopped"}];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    __block UIBackgroundTaskIdentifier backgroundTaskIdentifier = [application beginBackgroundTaskWithExpirationHandler:^(void)
    {
        [application endBackgroundTask:backgroundTaskIdentifier];
        [[SnappeHTTPService getInstance].operationQueue cancelAllOperations];
    }];
    [[EventManager getInstance] fireEvent:APPLICATION_ENTERED_BACKGROUND, nil];
    [Logger track:@"app_entered_background"];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[EventManager getInstance] fireEvent:APPLICATION_ENTERED_FOREGROUND, nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[EventManager getInstance] fireEvent:APPLICATION_BECAME_ACTIVE, nil];
    [[FBManager getInstance] handleDidBecomeActive];
    [Logger track:@"app_became_active"];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[EventManager getInstance] fireEvent:APPLICATION_WILL_TERMINATE, nil];
    [MagicalRecord cleanUp];
    
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	NSString *apnsToken = [[[deviceToken description]
                            stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                           stringByReplacingOccurrencesOfString:@" "
                           withString:@""];
    
    [[CoreController getInstance] setAPNSToken:apnsToken];
    [Logger track:@"get_apns_token_succeeded" :@{@"token": apnsToken}];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	[[CoreController getInstance] setAPNSToken:nil];
    [Logger logError:@"error in getting apns token" :error];
    [Logger track:@"get_apns_token_failed"];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[PhotoLibraryOrganizer getInstance] sync:^{
        DLog(@"synced with server after album fb upload");
    } :^(NSError *error) {
        [Logger logError:@"error in syncing at startup" :error];
    }];
    
    if (application.applicationState == UIApplicationStateActive) {
        [Logger track:@"received_remote_notification" : @{@"state": @"foreground"}];
    }
    else {
        [Logger track:@"received_remote_notification" : @{@"state": @"background"}];
    }
    

}

// Pre iOS 4.2 support
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return [self handleURL:url];
}

// For iOS 4.2+ support
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [self handleURL:url];
}

-(BOOL)handleURL:(NSURL *)url {
    if([url.scheme isEqualToString:SNAPPE_URL_SCHEME]) {
        
        [Logger track:@"handle_url" :@{@"url": url.absoluteString}];
        
        RootViewController *rvc = [[UIController getInstance] getUIControl:ROOT_VIEW_CONTROLLER];
        [rvc showHome];
        [[EventManager getInstance] fireEvent:SNAPPE_URL_OPENED, url, nil];
        return YES;
    }
    return [[FBManager getInstance] handleOpenURL:url];
}
@end
