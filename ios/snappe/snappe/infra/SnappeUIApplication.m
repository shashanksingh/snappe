//
//  SnappeUIApplication.m
//  snappe
//
//  Created by Shashank on 27/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SnappeUIApplication.h"
#import "EventManager.h"

#define ACTIVE_TIMER_INTERVAL 1.0

@interface SnappeUIApplication()
@property NSTimer *activeTimer;
@end

@implementation SnappeUIApplication
@synthesize activeTimer;

- (void)sendEvent:(UIEvent *)event {
    [super sendEvent:event];
    
    // Only want to reset the timer on a Began touch or an Ended touch, to reduce the number of timer resets.
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0) {
        // allTouches count only ever seems to be 1, so anyObject works here.
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan || phase == UITouchPhaseEnded)
            [self fireTimer];
    }
}

- (void)fireTimer {
    if (activeTimer && [activeTimer isValid]) {
        return;
    }
    
    activeTimer = [NSTimer scheduledTimerWithTimeInterval:ACTIVE_TIMER_INTERVAL target:self selector:@selector(onActiveTimerFire) userInfo:nil repeats:NO];
}

- (void)onActiveTimerFire {
    [[EventManager getInstance] fireEvent:APPLICATION_ACTIVE, nil];
}
@end
