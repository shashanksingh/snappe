Notes for using the GoogleAdMobAdapter_MobFoxSDK_iOS_4.1.6.a

This static library contains MobFox SDK 4.1.6 and the Google Mediation Adaptor for MobFox to allow easy integration and should be used in conjunction with the Google AdMob Ads SDK 6.3.0 - https://developers.google.com/mobile-ads-sdk/download

Please refer to https://developers.google.com/mobile-ads-sdk/ for more details and samples.

Requirement:

- Admob Mediation Account
- Xcode 4.6.1 or later
- Target of iOS 4.3 or later


Instructions:

- Add the included binary (.a library) into your Xcode project.
  Note: The MobFox SDK is built in. A previously added MobFox.framework is no longer required and can be removed.
- Enable the Ad network in the Ad Network Mediation webpage.
- Make ad requests normally using the AdMob SDK using the "Mediation ID" for the placement rather than the "Publisher ID".


Additional setup instructions:

1) Add the following frameworks:

MediaPlayer.framework - For Video and Interstitial Ads please (if not yet included) add the MediaPlayer.framework to your project.

StoreKit.framework - Required for MRAID Ad functionality

AdSupport.framework - Required for MRAID Ad functionality

2) Add MRAID.bundle to your project
This is needed for the MRAID Ad functionality


Notes for using this Library:

1) "Full Screen" Video and Interstitial Ads:
Please note that Video and Interstitial Ads will be displayed in "Full Screen" mode where possible. It does this by hiding the Status Bar and resizing the view controller's view. However, if the superview of the Ad networks view controller is a Navigation Bar (for example) then the Interstitial Ads may not be able to be shown "Full Screen". Therefore it is best not to use Interstitial Ads with Navigation View Controllers. Please make sure to restore your display orientation after showing an ad as Video and Interstitial Ads can force an orientation change.

2) ARC - Automatic Reference Counting
The MobFox framework is set to use ARC. By enabling Automatic Reference Counting in your projects you will never need to type retain or release again, dramatically simplifying the development process, while reducing crashes and memory leaks. The compiler has a complete understanding of your objects, and releases each object the instant it is no longer used, so apps run as fast as ever, with predictable, smooth performance. During the migration process toward this new Apple LLVM compiler technology it is mandatory, if you include it in a non ARC project, to set Other Linker Flag to "-fobjc-arc". Note that the two main compiler switches when building your application with a third party library that is not ARC compliant and vice versa, are "-fno-objc-arc" and "-fobjc-arc". "-f" is the switch and "no-objc-arc" and "objc-arc" are the options that you are turning on. As evident from the names, the first one turns off ARC and the second turns on. For example, if your application is ARC enabled but a third party library is not, you use the first switch "-fno-objc-arc" to exclude the third party library. Conversely, if your application is not yet ARC enabled but the third party library you are integrating is, you use the second switch "-fobjc-arc". You add these flags to the project from the Build phases tab.

3) Location Information
Please note that you do not need to include the CoreLocation framework for the MobFox SDK. This is because Publishers may encounter issues with App Store approval if their App uses location information primarily for ads. When user’s location is queried using CoreLocation, a dialog will appear to ask for the user’s permission. If the App has no apparent reason to ask for location information, it may appear to be out of place for the user. Apple may reject the publisher’s App due to this reason. By using the Google Mediation Adapter It will automatically use any location information you pass to the Google Adapter. Please see google docs on how to set location there. If you do use CoreLocation in your own app, you can pass the current latitude and longitude values to the MobFox Ad Network and it will utilize those values to get "Location Aware Adverts". For more information please have a look into the header file MobFoxVideoInterstitialViewController.h within the MobFox.framework. 

4) Next to these notes please read the full Google Documentation at https://developers.google.com/mobile-ads-sdk/ for your implementation.
